package com;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.HuApplication;
import com.tourism.hu.entity.Author;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.Order;
import com.tourism.hu.entity.Region;
import com.tourism.hu.mapper.RegionMapper;
import com.tourism.hu.service.IAuthorService;
import com.tourism.hu.service.ICustomerInfoService;
import com.tourism.hu.service.IOrderService;
import com.tourism.hu.util.DateConverterUtil;
import com.tourism.hu.util.FileUploadUtil;
import com.tourism.hu.util.GlobalConfigUtil;
import com.tourism.hu.util.base64ToMultipartUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 17:30
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HuApplication.class)
public class testTest {

    private   Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IAuthorService iAuthorService;

    @Resource
    private RegionMapper    regionMapper;

    @Resource
    private IOrderService iOrderService;

    @Resource
    private ICustomerInfoService iCustomerInfoService;

    @Test
    public void testCustomer(){
        Integer customerId = 3;
        List<CustomerInfo> customerInfoList = iCustomerInfoService.list(new QueryWrapper<CustomerInfo>().lambda().eq(CustomerInfo::getCustomerId,customerId));
//        iCustomerInfoService.count()
        for (CustomerInfo customerInfo : customerInfoList) {
            System.out.println(customerInfo);
        }


    }


    @Test
    public void testOrder(){
        //查询支付时间距离今天0点大于等于15天的订单
        List<Order> orderList = iOrderService.list(new QueryWrapper<Order>().le("pay_time", DateConverterUtil.getTime(-15)));
        System.out.println("====="+orderList.size());
        for (Order order : orderList) {
            System.out.println(order);
        }

    }



    @org.junit.Test
    public void test(){
        Integer roleId = 14;
        List<Author> authors = iAuthorService.list(new QueryWrapper<Author>().eq("role_id", roleId));
        List<String> list = new ArrayList<>();
        for (Author author : authors) {
            list.add(author.getResourceId().toString());
        }
        System.out.println(list);
//        mv.addObject("au", list);


    }


    @Test
    public void test2() throws IOException {
        String base64 ="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC8AAABBCAYAAABB2f2GAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkU2NEMwNzE4RjE0RDExRTlCMEE0RDdCNzNDMDYyNzMwIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkU2NEMwNzE5RjE0RDExRTlCMEE0RDdCNzNDMDYyNzMwIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RTY0QzA3MTZGMTREMTFFOUIwQTREN0I3M0MwNjI3MzAiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RTY0QzA3MTdGMTREMTFFOUIwQTREN0I3M0MwNjI3MzAiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5P9J6pAAAE5ElEQVR42uxaWWgTQRje7ZGuR1uhaisqLWKxIirGeov64o0IiieKFdQHb0T0QdQHL/BJkSqieKMi4otYBREqXmA1ahUPUjQehdQexl7WtEn8fjNbxjFJu5tsugv54WNmZ3dmv3/2n9n55x9Z0ikbpqX3QrIOmA8MB9I1NuEF3gO3gCPFpQ3ftXKQdRKfguQakC3FRqqBGVDgpaHkQXwIkmdATym24gSGQgFfZyuk6HjJAYF4G/CYmUAz8DNCXarXi32xySyvSj4wB7hpCHn0eg6SBVzRZ2A6esup4wt2R3IBWMgVz9JCPknjO6cJprZHD3ES1KOvtAHwc8XDtLShlXyBcF0ajZFDgSokdVxRjpHkxca/xWCg8uQVI8krQs/5Y0D+B5fvZiR5I6SFy6dZjbxuMQN53aYnh5mD+yEZAfQFkrlba4BJ3PXqGJDfDIxi+V/AeuF+E1CO8fUhInmQnopkH/v7ySazkrfAbihxQy1I5ohvR3IZyDUhcZI+wJKxeWlKmct7r508iC9Ccsoi43QyFPgCBV7KIJ7G1ijZwo/jLHAfcAOtXTypTGfmnMrKaoFBtDBbKhAvB2bCttwm6m0HOpm47mfXWcBi0mqu8OBKkxFX5Rjwm7ueR+THcwWvQLzcjIYOXvVIyrgiexKby1V5bfLB+pHLZycJ6wmPycnzi7hU0ZPyaWkJg4h2DEYCvXUQqWF/znq9mujxYVUX7jCwFrBF0ZNetHUa6Q4o0WT4woz9F+4yF84WpRnY2FrmLtpV4tHzW4GJ6kVuXqq7X05K7X8Ts6OlwOsNJNtsss9uV96L993utiyXq1X1zCYA24CDRp";

        MultipartFile file = base64ToMultipartUtil.base64ToMultipart(base64);
        String realPath = "goodsssssssss";
        String url ="";
        try {
            url = upload(file, realPath);
            System.out.println(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        long size = file.getSize();
    }

    public String upload(MultipartFile file,String basePath) throws IOException {
        String realPath = getRealPath(basePath);
        logger.info("传入参数路径realPath===============>"+realPath);
        String url = FileUploadUtil.baseUpload(file,realPath);
        logger.info("生成图片路径url===============>"+url);
        url = basePath+url;
        logger.info("存入数据库路径returnUrl===============>"+url);
        return url;
    }


    public String getRealPath(String path) {
        if(path!=null && path.length()>0) {
            if(path.substring(0, 1).equals("/")) {
                path=path.substring(1,path.length());
            }
        }
        return GlobalConfigUtil.getUploadUrl()+path;
    }


}
