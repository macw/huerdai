package com.tourism.hu.service;

import com.tourism.hu.HuApplication;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.OrderDetail;
import com.tourism.hu.entity.ProfitFlowing;
import com.tourism.hu.entity.vo.OrderProfitFlowing;
import com.tourism.hu.mapper.ProfitFlowingMapper;
import com.tourism.hu.util.PageUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 16:31
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HuApplication.class)
public class profitTest {

    @Resource
    private IOrderDetailService iOrderDetailService;

    @Resource
    private IProfitFlowingService iProfitFlowingService;

    @Resource
    private ProfitFlowingMapper profitFlowingMapper;

    @Test
    public void test(){
        CustomerInfo customerInfo = new CustomerInfo();
        PageUtil.initAttr(customerInfo);
        customerInfo.setUserId(11);
        List<OrderProfitFlowing> orderProfit = profitFlowingMapper.getOrderProfit(1,5,11,null, null, "12019120411133043586035036403883");
        System.out.println(orderProfit.size());
        System.out.println(orderProfit);

    }
   /* @Test
    public void test2(){
        CustomerInfo customerInfo = new CustomerInfo();
        PageUtil.initAttr(customerInfo);
        customerInfo.setUserId(11);
        Long count = profitFlowingMapper.getCount(customerInfo);
        System.out.println(count);
    }*/


}
