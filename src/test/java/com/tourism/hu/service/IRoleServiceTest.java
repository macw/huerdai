package com.tourism.hu.service;

import com.tourism.hu.HuApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Set;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 11:11
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HuApplication.class)
public class IRoleServiceTest  {

    @Resource
    private IRoleService iRoleService;

    String username = "admin";

    @Test
    public void testRoel(){
        Set<String> allRolesByUsername = iRoleService.getAllRolesByUsername(username);
        for (String s : allRolesByUsername) {
            System.out.println("s=== " +s);
        }
    }

    @Test
    public void testPermission(){
        Set<String> permissions = iRoleService.getAllPermissionsByUsername(username);
        for (String permission : permissions) {
            System.out.println("permission=== " +permission);
        }
    }

}