package com;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.HuApplication;
import com.tourism.hu.constant.AuthorConstant;
import com.tourism.hu.constant.MenuConsant;
import com.tourism.hu.entity.Author;
import com.tourism.hu.entity.Menu;
import com.tourism.hu.entity.UserRole;
import com.tourism.hu.service.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 14:32
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HuApplication.class)
public class menuTest {


    @Resource
    private IMenuService iMenuService;

    @Resource
    private IUserRoleService iUserRoleService;

    @Resource
    private IUserService iUserService;

    @Resource
    private IRoleService iRoleService;

    @Resource
    private IAuthorService iAuthorService;

    @Test
    public void test(){
        Integer userId = 1;

        List<Menu> menuList = new ArrayList<>();
        //根据当前用户id 查询当前用户的角色
        List<UserRole> userRoleList = iUserRoleService.list(new QueryWrapper<UserRole>().eq("user_id", userId));
        for (UserRole userRole : userRoleList) {
            //根据用户角色信息查询出角色id
            Integer roleId = userRole.getRoleId();
            //根据角色id查询授权信息
            List<Author> authorList = iAuthorService.list(new QueryWrapper<Author>().eq("role_id", roleId).eq("resource_type", AuthorConstant.MENU_RESOURCE_TYPE));
            //根据授权信息查询资源（菜单）id等数据
            for (Author author : authorList) {
                //根据资源id查询出菜单对象
//                Menu menu = iMenuService.getOne(new QueryWrapper<Menu>().eq("menu_id", author.getResourceId()));
                Menu menu = iMenuService.getById(author.getResourceId());
                //将菜单对象添加到返回结果集中
                menuList.add(menu);
            }
        }

        //处理菜单返回结果集，封装一级 菜单集合
        List<Menu> menus = new ArrayList<>();
        for (Menu menu : menuList) {
            if (menu.getLevel()== MenuConsant.ONE_LEVEL){
                menus.add(menu);
            }
        }
        //处理二级结果集
        //遍历一级结果集
        for (Menu menu : menus) {
            //再次从总结果集中抽出资源
            List<Menu> list2 = new ArrayList<>();
            for (Menu menu1 : menuList) {
                if (menu1.getParentId()==menu.getMenuId()){
                    //将二级结果集放进去
                    list2.add(menu1);
                }
            }
            if (list2!=null && list2.size()>0){
                menu.setMenuList(list2);
            }
        }
        System.out.println(menus);
    }

}
