<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>物流编辑</title>
	<%@ include file="../../head.jsp" %>
</head>
<body>
 <form class="layui-form" style="margin-top: 20px;">
 	<input type="hidden" name="logisticsId" value="${dto.logisticsId}">
    <div class="layui-form-item">
       <div class="layui-inline">
	      <label class="layui-form-label">物流单号</label>
	      <div class="layui-input-inline">
	        <input type="text" name="expressNo" value="${dto.expressNo}" lay-verify="required" autocomplete="off" class="layui-input">
	      </div>
	  </div>
      <div class="layui-inline">
	      <label class="layui-form-label">收货人</label>
	      <div class="layui-input-inline">
	         <input type="tel" name="consigneeRealname" value="${dto.consigneeRealname}" lay-verify="required" autocomplete="off" class="layui-input">
	      </div>
      </div>
    </div>
    <div class="layui-form-item">
       <div class="layui-inline">
	      <label class="layui-form-label">联系电话</label>
	      <div class="layui-input-inline">
	         <input type="text" name="consigneeTelphone" value="${dto.consigneeTelphone}" lay-verify="required|phone" autocomplete="off" class="layui-input">
	      </div>
       </div>
        <div class="layui-inline">
	      <label class="layui-form-label">备用电话</label>
	      <div class="layui-input-inline">
	        <input type="text" name="consigneeTelphone2" value="${dto.consigneeTelphone2}"  autocomplete="off" class="layui-input">
	      </div>
        </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">地址</label>
      <div class="layui-input-inline">
        <input type="text" name="address"  value="${dto.address}" lay-verify="required" autocomplete="off" class="layui-input" style="width: 515px">
      </div>
    </div>
    <div class="layui-form-item">
	    <div class="layui-input-block">
	      <button type="submit" class="layui-btn" lay-submit="" lay-filter="btnSub">立即提交</button>
	    </div>
  </div>
 </form>
</body>
 
   

<script type="text/javascript">
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
          //监听提交
            form.on('submit(btnSub)', function(data){
           		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            	$.ajax({
                  	 url:'<%=basePath%>logistics/info/updateByLogisticsId',
                  	 data: data.field,
                  	 type: 'post',
                  	 dataType: 'json',
                  	 success:function(data){
                  		layer.msg('操作成功', { offset: '15px',icon: 6,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                  		});
                  	 },error:function(data){
                  		layer.msg('操作成功', { offset: '15px',icon: 5,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                  		});
                  	 }
                 });
            	 
                 return false;
            });
        });
        
        
    </script>
</html>

