<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>物流信息</title>
	<%@ include file="../../head.jsp" %>
	 <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>
</head>
<body>
<div class="layui-fluid">
    <div class="layui-card-body layui-form">
        <div style="padding-bottom: 10px;">
          <!--   <div class="layui-input-inline" style="width: 100px;">
                 <button type="button" class="layui-btn"  onclick="add()">添加</button>
            </div> -->
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="businessName" id="businessName" placeholder="商家名称" autocomplete="off" class="layui-input">
			</div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="expressNo" id="expressNo" placeholder="订单号/物流单号" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
	             <input type="text" name="consigneeTelphone" id="consigneeTelphone" placeholder="手机号码/备用手机号" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="consigneeRealname" id="consigneeRealname" placeholder="收件人" autocomplete="off" class="layui-input">
            </div>
              <div class="layui-input-inline" style="width: 200px;">
				<select name="reconciliationStatus" id="reconciliationStatus">
					<option value="">是否已对账</option>
					<option value="1">已对账</option>
					<option value="2">未对账</option>
				</select>
			</div>
              <div class="layui-input-inline" style="width: 200px;">
				<select name="state" id="state">
					<option value="">物流状态</option>
					<option value="0">无轨迹</option>
					<option value="1">已揽收</option>
					<option value="2">在途中</option>
					<option value="3">签收</option>
					<option value="4">问题件</option>
				</select>
			</div>
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>
        <table id="tab" lay-filter="tab"></table>
    </div>
</div>
</body>
 
  <script type="text/html" id="tool">
        <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">编辑</button>
        <button type="button" class="layui-btn layui-btn-danger" lay-event="del">删除</button>
  </script>

<script type="text/javascript">

		var active=null;
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            table.render({
                elem: '#tab',
                url: '<%=basePath%>logistics/info/selectData', //数据接口
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                cols: [[
                    {type: "checkbox"},
                    {title: "物流单号", field: "expressNo",align:"center",width :200},
                    {title: "物流商家", field: "businessName",align:"center",width :120},
                    {title: "订单号", field: "orderId",align:"center",width :200},
                    {title: "收货人姓名", field: "consigneeRealname",align:"center",width :100},
                    {title: "联系电话/备用电话", align:"center",width :210,
                    	templet: function(d){
                    		var phone = '';
                    		if(d.consigneeTelphone!=null){
	                    		phone=d.consigneeTelphone;
                    		}
                    		if(d.consigneeTelphone2!=null){
	                    		return phone+"/"+d.consigneeTelphone2;
                    		}
                    		return phone;
                    	}	
                    },
                    {title: "收货地址", field: "address", align:"center",width :300},
                    {title: "邮政编码", field: "consigneeZip",align:"center",width :90},
                    /* {title: "物流方式", field: "logisticsType",align:"center",width :120}, */
                    {title: "运费", field: "logisticsFee",align:"center",width :70},
                    {title: "运费成本", field: "deliveryAmount",align:"center",width :90},
                    {title: "快递代收货款费率", field: "agencyFee",align:"center",width :150},
                    {title: "物流结算状态", field: "logisticsSettlementStatus",align:"center",width :120},
                    {title: "物流描述", field: "logisticsResult",align:"center",width :300},
                    {title: "物流状态", align:"center",width :120,
                    	templet: function(d){
                    		 if(d.state == 0){
                    			return '无轨迹';
                    		}else if(d.state == 1){
                    			return '已揽收';
                    		}else if(d.state == 2){
                    			return '在途中';
                    		}else if(d.state == 3){
                    			return '已签收';
                    		}else{
                    			return '问题件';
                    		}
                    	}	
                    },
                    {title: "物流支付渠道", field: "payChannel",align:"center",width :120},
                    {title: "物流支付单号", field: "payNo",align:"center",width :120},
                    {title: "对账状态", field: "reconciliationStatus",width :100,align:"center",
                    	templet: function(d){
                    		if(d.reconciliationStatus == 1){
	                    		return '已对账';
                    		}else if(d.reconciliationStatus == 2){
                    			return '未对账';
                    		}
                    		return '';
                    	}	
                    },
                    {title: "对账时间",align:"center",
                    	templet: function(d){
                    		if(d.reconciliationTime!=null){
	                    		return layui.util.toDateString(d.reconciliationTime);
                    		}
                    		return '';
                    	},width :160	
                    },
                    {title: "发货时间",align:"center",
                    	templet: function(d){
                    		if(d.logisticsCreateTime!=null){
	                    		return layui.util.toDateString(d.logisticsCreateTime);
                    		}
                    		return '';
                    	},width :160	
                    },
                    {title: "物流更新时间",align:"center",
                    	templet: function(d){
                    		if(d.logisticsUpdateTime!=null){
	                    		return layui.util.toDateString(d.logisticsUpdateTime);
                    		}
                    		return '';
                    	},width :160	
                    },
                    {title: "物流结算时间",align:"center",
                    	templet: function(d){
                    		if(d.logisticsSettlementTime!=null){
	                    		return layui.util.toDateString(d.logisticsSettlementTime);
                    		}
                    		return '';
                    	},width :160	
                    },
                    {title: "操作",width :175, align:"center", templet: "#tool",fixed: 'right'}
                ]],done:function(res, curr){
        	    	  var brforeCurr = curr; // 获得当前页码
        	    	  var dataLength = res.data.length; // 获得当前页的记录数
        	    	  var count = res.count; // 获得总记录数
        	    	  if(dataLength == 0 && count != 0){ //如果当前页的记录数为0并且总记录数不为0
        	    		  table.reload("tab",{ // 刷新表格到上一页
        	    			  page:{
        	    				 curr:brforeCurr-1
        	    			  }
        	    		  });
        	    	  }
        	      }
            });
          	//监听行工具事件
            table.on('tool(tab)', function(obj){
              var data = obj.data;
              if(obj.event === 'del'){
                layer.confirm('真的删除这条物流信息吗?', function(index){
                	$.ajax({
                   	 url:'<%=basePath%>logistics/info/deleteById',
                   	 data: {"logisticsId":data.logisticsId},
                   	 type: 'post',
                   	 dataType: 'json',
                   	 success:function(data){
                   		table.reload('tab', {});
                   		layer.msg(data.msg, {time: 1000, icon:6});
                   	 },error:function(data){
                   		table.reload('tab', {});
                   		layer.msg(data.msg, {time: 1000, icon:5});
                   	 }
                    });
                });
              } else if(obj.event === 'edit'){
            	  layer.open({
		    		  type: 2,
		    		  title: '编辑物流消息',
		    		  shadeClose: true,
		    		  shade: 0.8,
		    		  area: ['679px', '350px'],
		    		  content: '<%=basePath%>logistics/info/editPage?logisticsId='+data.logisticsId
			     	});
              }
            });
            active = {
   		        add: function(){ //获取选中数据
			   		layer.open({
		    		  type: 2,
		    		  title: '编辑物流消息',
		    		  shadeClose: true,
		    		  shade: 0.8,
		    		  area: ['679px', '350px'],
		    		  content: '<%=basePath%>qapar/editPage'
			     	});
   		        },
   				doSearch: function(){
   					table.reload('tab', {
   	                    where: {"expressNo": $('#expressNo').val(),"consigneeTelphone": $('#consigneeTelphone').val(),"businessName": $('#businessName').val(),"consigneeRealname": $('#consigneeRealname').val(),"reconciliationStatus": $('#reconciliationStatus').val(),"state": $('#state').val()},
   	                    page: {
   	                        curr: 1 //重新从第 1 页开始
   	                      }
   	                });
   				},
   				tableReload:function(){
	   				 table.reload('tab', {});
   				}
           };
        });
        //搜索操作
        function doSearch() {
        	active.doSearch();
        };
        function tableReload(){
        	active.tableReload();
        };
        function add(){
        	active.add();
        };
        
        
        
    </script>
</html>

