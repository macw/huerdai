<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>钱包变动日志管理</title>
	<%@ include file="../head.jsp" %>
	 <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>
</head>
<body>
<div class="layui-fluid">
    <div class="layui-card-body">
        <div style="padding-bottom: 10px;">
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="customerName" id="customerName" placeholder="用户名" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
	             <input type="text" name="recordSn" id="recordSn" placeholder="流水号" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="createdName" id="createdName" placeholder="创建人" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>
        <table id="walletlog-tab" lay-filter="walletlog-tab"></table>
    </div>
</div>
</body>
 
  <script type="text/html" id="tool">
       <!--  <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">编辑</button> -->
        <button type="button" class="layui-btn layui-btn-danger" lay-event="del">删除</button>
  </script>

<script type="text/javascript">

        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            table.render({
                elem: '#walletlog-tab',
                url: '<%=basePath%>walletlog/selectData', //数据接口
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                cols: [[
                    {type: "checkbox"},
                    {title: "用户名", field: "customerName",align:"center",width:90},
                    {title: "流水号", field: "recordSn",align:"center",width:288},
                    {title: "变动", field: "changeMoney",align:"center",width:90},
                    {title: "变动后金额", field: "money",align:"center",width:120},
                    {title: "备注", field: "remark",align:"center"},
                    {title: "创建人", field: "createdName",align:"center",width:90},
                    {title: "创建时间",align:"center",
                    	templet: function(d){
                    		if(d.creationDate!=null){
	                    		return layui.util.toDateString(d.creationDate);
                    		}
                    		return '';
                    	},width:160	
                    },
                    /* {title: "最近修改人", field: "lastUpdateName",align:"center",width:100},
                    {title: "最近修改时间",align:"center",
                    	templet: function(d){
                    		if(d.lastUpdateDate!=null){
	                    		return layui.util.toDateString(d.lastUpdateDate);
                    		}
                    		return '';
                    	},width:160	
                    }, */
                    {title: "操作",width :100, align:"center", templet: "#tool"}
                ]],done:function(res, curr){
        	    	  var brforeCurr = curr; // 获得当前页码
        	    	  var dataLength = res.data.length; // 获得当前页的记录数
        	    	  var count = res.count; // 获得总记录数
        	    	  if(dataLength == 0 && count != 0){ //如果当前页的记录数为0并且总记录数不为0
        	    		  table.reload("walletlog-tab",{ // 刷新表格到上一页
        	    			  page:{
        	    				 curr:brforeCurr-1
        	    			  }
        	    		  });
        	    	  }
        	      }
            });
            
            
          	//监听行工具事件
            table.on('tool(walletlog-tab)', function(obj){
              var data = obj.data;
             if(obj.event === 'del'){
                layer.confirm('真的删除这条记录吗?', function(index){
                	$.ajax({
                   	 url:'<%=basePath%>walletlog/deleteByLogId',
                   	 data: {"logId":data.logId},
                   	 type: 'post',
                   	 dataType: 'json',
                   	 success:function(data){
                   		 tableReload();
                   		layer.msg("操作成功", {time: 1000, icon:6});
                   	 },error:function(data){
                   		 tableReload();
                   		layer.msg("操作失败", {time: 1000, icon:6});
                   	 }
                    });
                  	layer.close(index);
                });
              } else if(obj.event === 'edit'){
            	  layer.open({
             		  type: 2,
             		  title: '编辑商户管理',
             		  shadeClose: true,
             		  shade: 0.8,
             		  area: ['470px', '450px'],
             		  content: '<%=basePath%>walletlog/editPage?logId='+data.logId
              	});  
              }
            });
        });
        //搜索操作
        function doSearch() {
            //1.获取到输入框中输入的内容
            var customerName = $('#customerName').val();
            var recordSn = $('#recordSn').val();
            var createdName = $('#createdName').val();
            //发送请求，并且接收数据
            layui.use('table', function () {
                var table = layui.table;
                table.reload('walletlog-tab', {
                    where: {"customerName": customerName,"recordSn": recordSn,"createdName": createdName},
                    page: {
                        curr: 1 //重新从第 1 页开始
                      }
                });
            });
        };
        function tableReload(){
       	   layui.use('table', function () {
              var table = layui.table;
              table.reload('walletlog-tab', {
              });
           });
        };
        
    </script>
</html>

