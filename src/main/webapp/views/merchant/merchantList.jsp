<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>商户信息管理</title>
	<%@ include file="../head.jsp" %>
	 <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>
</head>
<body>
<div class="layui-fluid">
    <div class="layui-card-body">
        <div style="padding-bottom: 10px;">
            <div class="layui-input-inline" style="width: 100px;">
                 <button type="button" class="layui-btn addMerch" data-type="addMerch" onclick="addMerch()">添加</button>
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="merName" id="merName" placeholder="商户名称" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
	             <input type="text" name="phone" id="phone" placeholder="手机号" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="userName" id="userName" placeholder="创建人/修改人" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>
        <table id="tab" lay-filter="tab"></table>
    </div>
</div>
</body>
 
  <script type="text/html" id="tool">
        <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">编辑</button>
        <button type="button" class="layui-btn layui-btn-danger" lay-event="del">删除</button>
  </script>

<script type="text/javascript">

        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            table.render({
                elem: '#tab',
                url: '<%=basePath%>merchant/selectMerchantList', //数据接口
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                cols: [[
                    {type: "checkbox"},
                    {title: "商户名称", field: "merName",align:"center"},
                    {title: "经纬度",align:"center",
                    	templet: function(d){
                    		return d.lon+"/"+d.lat;
                    	}
                    },
                    {title: "手机号码", field: "phone",align:"center"},
                    {title: "地址", field: "address",align:"center"},
                    {title: "创建人", field: "createUser",align:"center"},
                    {title: "创建时间",align:"center",
                    	templet: function(d){
                    		if(d.createTime!=null){
	                    		return layui.util.toDateString(d.createTime);
                    		}
                    		return '';
                    	}	
                    },
                    {title: "最近修改人", field: "updateUser",align:"center"},
                    {title: "最近修改时间",align:"center",
                    	templet: function(d){
                    		if(d.updateTime!=null){
	                    		return layui.util.toDateString(d.updateTime);
                    		}
                    		return '';
                    	}	
                    },
                    {title: "状态", width :100,align:"center",
                    	templet: function(d){
	                    	if(d.status == 1){
	                    		return '<button type="button" class="layui-btn  layui-btn-normal"  lay-event="yn">启用</button>';
	                    	}else{
	                    		return '<button type="button" class="layui-btn  layui-btn-danger"  lay-event="yn">禁用</button>';
	                    	}
                   		}
                	},
                    {title: "操作",width :175, align:"center", templet: "#tool"}
                ]] ,done:function(res, curr){
        	    	  var brforeCurr = curr; // 获得当前页码
        	    	  var dataLength = res.data.length; // 获得当前页的记录数
        	    	  var count = res.count; // 获得总记录数
        	    	  if(dataLength == 0 && count != 0){ //如果当前页的记录数为0并且总记录数不为0
        	    		  table.reload("tab",{ // 刷新表格到上一页
        	    			  page:{
        	    				 curr:brforeCurr-1
        	    			  }
        	    		  });
        	    	  }
        	      }
            });
            
            
          	//监听行工具事件
            table.on('tool(tab)', function(obj){
              var data = obj.data;
              if(obj.event === 'yn'){
            	  var str = "启用";
            	  var status = 1;
            	  if(data.status == 1){
            		  str = "禁用";
            		  status = 0;
            	  }
                  layer.confirm('确认要['+str+']这个商户吗?', function(index){
                     $.ajax({
                    	 url:'<%=basePath%>merchant/update',
                    	 data: {"merId":data.merId,"status":status},
                    	 type: 'post',
                    	 dataType: 'json',
                    	 success:function(data){
                    		 tableReload();
                    		 layer.msg("操作成功", {time: 1000, icon:6});
                    	 },error:function(data){
                    		 tableReload();
                    		 layer.msg("操作失败", {time: 1000, icon:5});
                    	 }
                     });
                    	layer.close(index);
                  });
              }else if(obj.event === 'del'){
                layer.confirm('真的删除这个商户吗?', function(index){
                	$.ajax({
                   	 url:'<%=basePath%>merchant/del',
                   	 data: {"merId":data.merId},
                   	 type: 'post',
                   	 dataType: 'json',
                   	 success:function(data){
                   		 tableReload();
                   		layer.msg("操作成功", {time: 1000, icon:6});
                   	 },error:function(data){
                   		 tableReload();
                   		layer.msg("操作失败", {time: 1000, icon:5});
                   	 }
                    });
                  	layer.close(index);
                });
              } else if(obj.event === 'edit'){
            	  layer.open({
             		  type: 2,
             		  title: '编辑商户管理',
             		  shadeClose: true,
             		  shade: 0.8,
             		  area: ['470px', '450px'],
             		  content: '<%=basePath%>merchant/editPage?merId='+data.merId
              	});  
              }
            });
        });
        //搜索操作
        function doSearch() {
            //1.获取到输入框中输入的内容
            var merName = $('#merName').val();
            var phone = $('#phone').val();
            var createUser = $('#userName').val();
            //发送请求，并且接收数据
            layui.use('table', function () {
                var table = layui.table;
                table.reload('tab', {
                    where: {"merName": merName,"phone": phone,"createUser": createUser},
                    page: {
                        curr: 1 //重新从第 1 页开始
                      }
                });
            });
        };
        function tableReload(){
       	   layui.use('table', function () {
              var table = layui.table;
              table.reload('tab', {
              });
           });
        };
        function addMerch(){
        	layui.use('layer', function(){
        		layer.open({
             		  type: 2,
             		  title: '添加商户管理',
             		  shadeClose: true,
             		  shade: 0.8,
             		  area: ['30%', '50%'],
             		  content: '<%=basePath%>merchant/editPage'
              	});   
       		}); 
        };
        
        
        
    </script>
</html>

