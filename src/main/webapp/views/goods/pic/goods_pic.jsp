<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/9/20
  Time: 9:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../head.jsp" %>
<html>
<head>
    <title>商品信息详情管理</title>
</head>
<body>

<div id="LAY-goods-pic-layer">
    <div style="padding-bottom: 10px;" id="LAY_goods_pic_add">
        <button type="button" class="layui-btn layui-btn-danger" id="doPicMultiDelete">
            <i class="layui-icon layui-icon-delete"></i> 批量删除
        </button>
        <button class="layui-btn layuiadmin-btn-role " data-type="add" id="toPicOpenAddLayer">
            <i class="layui-icon layui-icon-add-circle-fine"></i> 添加
        </button>

    </div>

    <table id="Lay_goods_pic" lay-filter="Lay_goods_pic"></table>


</div>

<script>
    // 计算文件大小函数(保留两位小数),Size为字节大小
    // size：初始文件大小
    function getfilesize(size) {
        if (!size)
            return "";

        var num = 1024.00; //byte
        if (size < num)
            return size + "B";
        if (size < Math.pow(num, 2))
            return (size / num).toFixed(2) + "KB"; //kb
        if (size < Math.pow(num, 3))
            return (size / Math.pow(num, 2)).toFixed(2) + "MB"; //M
        if (size < Math.pow(num, 4))
            return (size / Math.pow(num, 3)).toFixed(2) + "GB"; //G
        return (size / Math.pow(num, 4)).toFixed(2) + "TB"; //T
    }
</script>

<script type="text/html" id="updateAndDeletes">
    <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">
        <i class="layui-icon layui-icon-edit"></i>修改
    </button>
    <button type="button" class="layui-btn layui-btn-danger" lay-event="del">
        <i class="layui-icon layui-icon-delete"></i> 删除
    </button>
</script>


<script type="text/javascript">
    layui.use(['table', 'upload', "layer", 'form'], function () {
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var upload = layui.upload;
        var $ = layui.jquery;

        var layTab =  table.render({
            elem: '#Lay_goods_pic',
            url: '${pageContext.request.contextPath}/goodspic/selectAllByGoodsId?goodsid=' + ${goodsId}, //数据接口
            page: true,
            limit: 10,
            limits: [10, 20, 30],
            cols: [[
                {type: "checkbox"},
                {title: "图片级别", field: "leavel", width: 100, align: "center"},
                {title: "作用类型", field: "type", width: 150, align: "center",
                    templet:function (d) {
                        if (d.type==1){
                            return "商品头图";
                        }else if(d.type==2){
                            return "详情介绍图";
                        }else {
                            return "";
                        }
                    }
                },
                {
                    title: "图片大小", field: "size", width: 200, align: "center", templet: function (d) {
                        return getfilesize(d.size);
                    }
                },
                {
                    title: "图片地址", field: "pictureurl", width: 200, align: "center", templet: function (d) {
                        return '<img src="' + d.pictureurl + '">'
                    }
                },
                {title: "备注", field: "memo", width: 200, align: "center"},
                {title: "创建人", field: "createUser", width: 200, align: "center"},
                {title: "创建时间", field: "createTime", width: 200, align: "center"},
                {title: "更新人员", field: "updateUser", width: 200, align: "center"},
                {title: "更新时间", field: "updateTime", width: 200, align: "center"},
                {title: "操作", templet: "#updateAndDeletes", width: 200, align: "center"}
            ]]
        });

        //刷新表格方法
        function tableReload() {
            layTab.reload('Lay_goods_pic', {});
        };

        //监听行工具事件
        table.on('tool(Lay_goods_pic)', function (obj) {
            var data = obj.data;
            if (obj.event === 'del'){
                layer.confirm('确定要删除吗？', {icon: 3, title: '确认删除'}, function (index) {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/goodspic/deleteOne",
                        data: "id=" + data.gpId,
                        success: function (data) {
                            layer.alert("删除" + data.msg, {time: 2000});
                            tableReload();
                            layer.close(index);
                        }
                    })
                });
            }else if (obj.event === 'edit'){
                var index = layer.open({
                    title: "修改",
                    content:"${pageContext.request.contextPath}/goodspic/toGoodsPicEdit?gpId="+data.gpId+"&goodsId="+${goodsId},
                    type: 2,
                    maxmin: true,
                    area: ['500px', '480px'],
                    end: function () {
                        window.location.reload();
                    }
                });

            }
        });
        //添加框
        $("#toPicOpenAddLayer").click(function () {
            layer.open({
                title: "添加商品图片",
                content: "${pageContext.request.contextPath}/goodspic/toGoodsPicEdit?goodsId="+${goodsId},
                type: 2,
                maxmin: true,
                area: ['500px', '480px'],
                maxmin: true,
                end: function () {
                    window.location.reload();
                }
            });
        });
        //图片的批量删除方法
        $("#doPicMultiDelete").click(function () {
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_goods_pic'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据?", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].gpId;
                    }
                    console.log("ids===" + ids);
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/goodspic/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert("删除" + data.msg, {time: 2000});
                            //刷新table
                            table.reload("Lay_goods_pic");
                        }
                    })
                });
            }
        })

    });

</script>

</body>
</html>
