<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/29
  Time: 14:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../head.jsp" %>
<html>
<head>
    <title>商品信息详情管理</title>
</head>
<body>
<%--弹出层--%>
<form id="add_Pic_Form" lay-filter="add_Pic_Form" class="layui-form" enctype="multipart/form-data">
    <div class="layui-form" lay-filter="layuiconfig-form-goods_pic" id="layuiconfig-form-goods_pic"
         style="padding: 20px 30px 0 0">
        <div class="layui-form-item">
            <label class="layui-form-label">作用类型</label>
            <div class="layui-input-block">
                <select name="type" id="type" lay-filter="type" >
                    <option value="">请选择图片作用类型</option>
                    <option value="1" <c:if test="${goods.type==1}">selected</c:if> >商品头图</option>
                    <option value="2" <c:if test="${goods.type==2}">selected</c:if> >详情介绍图</option>
                </select>

            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">备注</label>
            <div class="layui-input-block">
                <input name="memo" id="memo" value="${goods.memo}" class="layui-input">
                <input name="goods_id" id="goods_id" value="${goods.goodsId}" lay-type="hide" type="hidden"
                       class="layui-input">
                <input name="gpId" id="gpId" value="${goods.gpId}" lay-type="hide" type="hidden" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">图片级别(越小越靠前)</label>
            <div class="layui-input-block">
                <select name="leavel" id="leavel" lay-filter="two" >
                    <option value="">请选择图片级别</option>
                    <option value="1" <c:if test="${goods.leavel==1}">selected</c:if> >1</option>
                    <option value="2" <c:if test="${goods.leavel==2}">selected</c:if> >2</option>
                    <option value="3" <c:if test="${goods.leavel==3}">selected</c:if> >3</option>
                    <option value="4" <c:if test="${goods.leavel==4}">selected</c:if> >4</option>
                    <option value="5" <c:if test="${goods.leavel==5}">selected</c:if> >5</option>
                </select>
            </div>

        </div>


        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">选择图片</label>
                <div class="layui-input-block">
                    <button type="button" class="layui-btn" id="upload_customer_pic">
                        <i class="layui-icon">&#xe67c;</i>上传图片
                    </button>
                </div>
            </div>
            <div class="layui-inline">
                <img class="layui-upload-img" name="pictureurl" src="${goods.pictureurl}" id="wechatUrl"
                     style="max-width: 200px;max-height: 200px; padding-left: 40px;">
            </div>
        </div>


        <div class="layui-form-item">
            <div class="layui-input-block">
                <button type="submit" class="layui-btn" id="btnSub" lay-filter="btnSub" lay-submit>立即提交</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    layui.use(['table', 'upload', "layer", 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            var upload = layui.upload;

            upload.render({
                elem: '#upload_customer_pic', //绑定元素
                accept: "images",
                acceptMime: 'image/*',//打开文件选择框时,只显示图片文件
                auto: false,  //是否选完文件后自动上传。默认值：true
                // bindAction: '#btnSub',
                choose: function (obj) {
                    obj.preview(function (index, file, result) {
                        $('#wechatUrl').attr('src', result);

                    })
                }
            });

            //当点击提交按钮的时候，会进入到这个函数
            form.on("submit(btnSub)", function (data) {
                var formData = new FormData(document.getElementById("add_Pic_Form"));
                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                console.log(data.field);
                $.ajax({
                    url: '${pageContext.request.contextPath}/goodspic/addOrUpdate?goodsId=' +${goodsId},
                    data: formData,
                    type: 'post',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data.code == 200) {
                            layer.msg('操作成功', {
                                icon: 6,
                                time: 1000
                            }, function () {
                                parent.layer.close(index);
                                parent.layui.table.reload('Lay_back_table', {});
                            });
                        } else {
                            layer.msg('操作失败,请重新上传', {
                                icon: 5,
                                time: 1000
                            });
                        }
                    },
                    error: function (data) {
                        layer.msg('操作失败', {
                            icon: 5,
                            time: 1000
                        });
                    }
                });
                return false;//阻止跳转；
            })
        }
    )
</script>


</body>
</html>
