<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/9/23
  Time: 13:25
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../meta.jsp" %>
<html>
<head>
    <title>商品分类管理</title>
    <link rel="stylesheet" href="${basepath}layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="${basepath}layuiadmin/modules/treetable-lay/treetable.css">
    <script type="text/javascript" src="${basepath}js/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="${basepath}js/cookies.js"></script>
    <script type="text/javascript" src="${basepath}layuiadmin/layui/layui.js"></script>
    <script type="text/javascript" src="${basepath}layuiadmin/modules/treetable-lay/treetable.js"></script>
    <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>

</head>
<body>
<div class="layui-card-body">
    <div style="padding-bottom: 10px;" id="LAY_lay_add">
       <%-- <button type="button" class="layui-btn layui-btn-danger" id="doMultiDelete">
            <i class="layui-icon layui-icon-delete"></i> 批量删除
        </button>--%>
        <button class="layui-btn layuiadmin-btn-role " data-type="add" id="toOpenAddLayer">
            <i class="layui-icon layui-icon-add-circle-fine"></i> 添加
        </button>
        &nbsp;
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="search" id="Lay_toSearch_input" placeholder="请输入分类名称" autocomplete="off"
                   class="layui-input">
        </div>
        <div class="layui-input-inline" style="width: 100px;">
            <button type="button" class="layui-btn layui-btn-normal" id="btn-search"<%-- onclick="doSearch()"--%>>
                <i class="layui-icon layui-icon-search"></i> 搜索
            </button>
        </div>
        &nbsp;
        <div class="layui-btn-group">
            <button class="layui-btn" id="btn-expand">全部展开</button>
            <button class="layui-btn" id="btn-fold">全部折叠</button>
            <button class="layui-btn" id="btn-refresh">刷新表格</button>
        </div>
        <%--
                <button type="button" class="layui-btn layui-btn-normal  change-icon">随机更换小图标</button>
        --%>
    </div>

    <%--树形表格--%>
    <table class="layui-table" id="Lay_category_treeTable" lay-filter="Lay_category_treeTable"></table>

    <%--树形菜单--%>
    <div id="Lay_category_tree" lay-filter="Lay_category_tree"></div>

    <%--Table表格--%>
    <%-- <table id="Lay_back_table" lay-filter="Lay_back_table"></table>--%>


</div>

<script type="text/html" id="updateAndDelete">
    <button type="button" class="layui-btn  layui-btn-normal" lay-event="toOpenUpdateLayer">
        <i class="layui-icon layui-icon-edit"></i>修改
    </button>
    <button type="button" class="layui-btn layui-btn-danger" lay-event="doDelete">
        <i class="layui-icon layui-icon-delete"></i> 删除
    </button>
</script>

<%--弹出层--%>

<form id="addForm" class="layui-form">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0;display: none">
        <div class="layui-form-item">
            <label class="layui-form-label">分类名称</label>
            <div class="layui-input-block">
                <input name="className" id="className" class="layui-input">
                <input name="classId" id="classId" lay-type="hide" type="hidden" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">分类编码</label>
            <div class="layui-input-block">
                <input name="classCode" id="classCode" class="layui-input">
            </div>
        </div>


        <div class="layui-form-item" id="Lay_select_Level">
            <label class="layui-form-label">请选择分类级别</label>
            <div class="layui-input-block">
                <select name="classIdLevel" lay-verify="required" lay-filter="classIdLevel">
                    <option value=""></option>
                    <option value="1" selected>一级分类</option>
                    <option value="2">二级分类</option>
                    <option value="3">三级分类</option>
                </select>
            </div>
        </div>


        <div class="layui-form-item" style="display: none" id="Lay_One_Level">
            <label class="layui-form-label">请选择所属的一级分类</label>
            <div class="layui-input-block">
                <select name="parentId" id="parentId" lay-filter="parent_classIdLevel_One">
                    <option value=""></option>
                </select>
            </div>
        </div>

        <div class="layui-form-item" style="display: none" id="Lay_Two_Level">
            <label class="layui-form-label">请选择所属的二级分类</label>
            <div class="layui-input-block">
                <select name="parentId2" id="Two_parentId" lay-filter="parent_classIdLevel_Two">
                    <option value=""></option>
                </select>
            </div>
        </div>


        <div class="layui-form-item" style="text-align: right">
            <button class="layui-btn " lay-submit lay-filter="LAY-sysconfig-submit" id="LAY-sysconfig-submit">确认添加
            </button>
            <button lay-submit lay-filter="updateSubmitBtn" class="layui-btn" id="updateSubmitBtn">确认修改</button>

        </div>
    </div>
</form>


<script type="text/javascript">

    layui.config({
        base: '${pageContext.request.contextPath}/layuiadmin/modules/' //   资源所在路径
    }).extend({
        treetable: 'treetable-lay/treetable'
    }).use(['treetable', 'table', 'form', 'layer'], function () {
        var treetable = layui.treetable;
        var layer = layui.layer;
        var table = layui.table;
        var form = layui.form;
        var $ = layui.jquery;

        var re;

        // 渲染表格
        var renderTable = function () {
            layer.load(3);
            re = treetable.render({
                elem: '#Lay_category_treeTable',
                url: '${basepath}/goodscategory/selectTreeTable',
                treeColIndex: 1,          // 树形图标显示在第几列
                treeSpid: 0,             // 最上级的父级id
                treeIdName: 'classId',       // 	id字段的名称
                treePidName: 'parentId',    // 	pid字段的名称
                treeDefaultClose: true,     //是否默认折叠
                page: false,
                //treeLinkage: true,      //父级展开时是否自动展开所有子级
                cols: [[
                    {type: 'numbers'},
                    {title: "分类名称", field: "className", style: "text-align:left"},
                    {title: "分类编码", field: "classCode", align: "center"},
                    //  {title: "分类层级", field: "classIdLevel"},
                    {title: "创建人", field: "createUser", align: "center"},
                    {
                        title: "创建时间",
                        field: "createTime", align: "center",
                        templet: '<div>{{# if(d.createTime!=null){ }} {{ layui.util.toDateString(d.createTime,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'
                    },
                    {title: "更新人员", field: "updateUser", align: "center"},
                    {
                        title: "更新时间",
                        field: "updateTime", align: "center",
                        templet: '<div>{{# if(d.updateTime!=null){ }} {{ layui.util.toDateString(d.updateTime,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'
                    },
                    {
                        title: "分类状态", width: 100, align: "center",
                        templet: function (d) {
                            if (d.classIdStatus == 1) {
                                return '<button type="button" class="layui-btn  layui-btn-normal"  lay-event="yn">启用</button>';
                            }
                            else if (d.classIdStatus == 2){
                                return '<button type="button" class="layui-btn " >玩赚</button>';
                            }
                            else {
                                return '<button type="button" class="layui-btn  layui-btn-danger"  lay-event="yn">禁用</button>';
                            }
                        }
                    },
                    {title: "操作", templet: "#updateAndDelete", align: "center"}
                ]],
                done: function () {
                    layer.closeAll('loading');
                }

            })
        };

        //监听行工具事件
        table.on('tool(Lay_category_treeTable)', function (obj) {
            var data = obj.data;
            if (obj.event === 'yn') {
                var str = obj.data.classIdStatus == 0 ? "启用" : "禁用";
                var state = obj.data.classIdStatus == 1 ? 0 : 1;
                layer.confirm('确认要[' + str + ']这个分类吗?', function (index) {
                    $.ajax({
                        url: '${pageContext.request.contextPath}/goodscategory/updateclassIdStatus',
                        data: {"classId": data.classId, "classIdStatus": state},
                        type: 'post',
                        dataType: 'json',
                        success: function (data) {
                            renderTable();
                            layer.msg("操作" + data.msg, {time: 1000, icon: 6});
                        }, error: function (data) {
                            renderTable();
                            layer.msg("操作失败", {time: 1000, icon: 5});
                        }
                    });
                    layer.close(index);
                });
            } else if (obj.event === "toOpenUpdateLayer") {
                $.ajax({
                    url: "${pageContext.request.contextPath}/goodscategory/selectOne",
                    data: "aid=" + data.classId,
                    success: function (data) {
                        $("#className").val(data.className);
                        $("#classId").val(data.classId);
                        $("#classCode").val(data.classCode);

                    }
                });

                //2.把数据填充到修改弹出层中==>弹出层显示
                $("#Lay_select_Level").hide();
                var index = layer.open({
                    title: "修改配置",
                    content: $("#layuiconfig-form-role"),
                    type: 1,
                    maxmin: true,
                    area: ['500px', '480px'],
                    end: function () {
                        window.location.reload();
                    }
                });

                $("#LAY-sysconfig-submit").hide();
                $("#updateSubmitBtn").show();

                //3.提交表单
                form.on("submit(updateSubmitBtn)", function (data) {
                    // console.log(data);
                    $.ajax({
                        url: "${pageContext.request.contextPath}/goodscategory/updateGoods",
                        data: data.field,
                        type: "post",
                        //4.接收后台修改响应回来的数据；关闭弹出层、提示修改信息、刷新table
                        success: function (data) {

                            //提示
                            // layer.alert(data.msg, {time: 3500});
                            layer.msg("data.msg", {
                                icon: 5,
                                time: 2000
                            });
                            //确认框关闭掉
                            layer.close(index);
                            //刷新table
                            renderTable();
                        }
                    });
                    return false;//阻止跳转；
                });
            }
            else if (obj.event === "doDelete"){
                layer.confirm('确定要删除吗？', {icon: 3, title: '确认删除'}, function (index) {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/goodscategory/deletecl",
                        data: "aid=" + data.classId,
                        success: function (data) {
                            layer.alert("删除" + data.msg, {time: 2000});
                            // table.reload("Lay_back_table");
                            layer.close(index);
                            renderTable();
                        }
                    })
                });

            }
        });

        renderTable();
        //展开所有
        $('#btn-expand').click(function () {
            //alert(0)
            treetable.expandAll('#Lay_category_treeTable');
        });
        //折叠所有
        $('#btn-fold').click(function () {
            // alert(1)
            treetable.foldAll('#Lay_category_treeTable');
        });
        //刷新表格
        $('#btn-refresh').click(function () {
            renderTable();
        });


        $('#btn-search').click(function () {
            var keyword = $('#Lay_toSearch_input').val();
            //alert(keyword);
            // var searchName = $('#Lay_toSearch_input').val();
            var searchCount = 0;
            $('#Lay_category_treeTable').next('.treeTable').find('.layui-table-body tbody tr td').each(function () {
                $(this).css('background-color', 'transparent');
                var text = $(this).text();
                if (keyword != '' && text.indexOf(keyword) >= 0) {
                    $(this).css('background-color', 'rgba(250,230,160,0.5)');
                    if (searchCount == 0) {
                        treetable.expandAll('#Lay_category_treeTable');
                        $('html,body').stop(true);
                        $('html,body').animate({scrollTop: $(this).offset().top - 150}, 500);
                    }
                    searchCount++;
                }
            });
            if (keyword == '') {
                layer.msg("请输入搜索内容", {icon: 5});
            } else if (searchCount == 0) {
                layer.msg("没有匹配结果", {icon: 5});
            }
        });

        $("#toOpenAddLayer").click(function () {
            layer.open({
                title: "添加配置",
                content: $("#layuiconfig-form-role"),
                type: 1,
                maxmin: true,
                area: ['500px', '480px'],
                end: function () {
                    window.location.reload();
                }
            });

            $("#updateSubmitBtn").hide();
            $("#LAY-sysconfig-submit").show();

            form.on('select(classIdLevel)', function (data) {
                //console.log(data.elem); //得到select原始DOM对象
                console.log("data.value = " + data.value); //得到被选中的值
                //console.log(data.othis); //得到美化后的DOM对象
                if (data.value == 1) {
                    // alert(1);
                    $("#Lay_One_Level").hide();
                    $("#Lay_Two_Level").hide();
                }
                if (data.value == 2) {
                    // alert(2);
                    $("#Lay_One_Level").show();
                    $("#Lay_Two_Level").hide();
                    $.ajax({
                        url: '${basepath}goodscategory/selectOneLevel',
                        dataType: 'json',
                        type: 'post',
                        success: function (data) {
                            $.each(data, function (index, item) {
                                //console.log("000 " + index);
                                //console.log("111 " + item);
                                $('#parentId').append(new Option(item.className, item.classId));//往下拉菜单里添加元素
                            });
                            form.render();//菜单渲染 把内容加载进去
                        }
                    })
                }
                if (data.value == 3) {
                    // alert(3);
                    $("#Lay_One_Level").hide();
                    // form.on('select(parent_classIdLevel_One)', function (data2){
                    $("#Lay_Two_Level").show();
                    $.ajax({
                        url: '${basepath}goodscategory/selectTwoLevel',
                        dataType: 'json',
                        type: 'post',
                        success: function (data) {
                            $.each(data, function (index, item) {
                                //console.log("000 " + index);
                                //console.log("111 " + item);
                                $('#Two_parentId').append(new Option(item.className, item.classId));//往下拉菜单里添加元素
                            });
                            form.render();//菜单渲染 把内容加载进去
                        }
                    })
                    // })
                }
            });

            //当点击提交按钮的时候，会进入到这个函数
            form.on("submit(LAY-sysconfig-submit)", function (data) {
                console.log(data.field);
                $.ajax({
                    url: "${pageContext.request.contextPath}/goodscategory/addGoodsCategory",
                    data: data.field,
                    type: "post",
                    success: function (data) {
                        //2.提示添加成功
                        layer.alert("添加" + data.msg, {time: 3000});
                        //1.关闭掉添加弹出层
                        layer.closeAll('index');
                        //3.刷新table
                        renderTable();

                    }
                });
                return false;//阻止跳转；
            })
        });

        $("#doMultiDelete").click(function () {
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].id;
                    }
                    console.log("ids===" + ids);
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/prefixThird/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除提示
                            layer.alert("删除" + data.msg, {time: 2000});
                            //删除确认框关闭掉
                            layer.close(index);
                            //刷新table
                            renderTable();
                        }
                    })
                });
            }
        })


    });


    /*   //这是一棵树，
       layui.use(['table', 'tree', "layer", 'jquery', 'form'], function () {
           var table = layui.table;
           var layer = layui.layer;
           var form = layui.form;
           var tree = layui.tree;
           var $ = layui.jquery;

           $.ajax({
               url: "${pageContext.request.contextPath}/goodscategory/selectTree",
            success: function (data) {
                //console.log(data);
                //渲染
                var inst1 = tree.render({
                    elem: '#Lay_category_tree',  //绑定元素
                    data: data.data,
                    showCheckbox: true
                    // accordion:true
                });
            }
        });
    });*/


    /*   //搜索操作
       function doSearch() {
           //1.获取到输入框中输入的内容
           var searchName = $('#Lay_toSearch_input').val();
           //发送请求，并且接收数据
           layui.use('table', function () {
               var table = layui.table;
               table.reload('Lay_back_table', {
                   where: {"platform": searchName}
               });
           });
       }
   */





    function doMultiDelete() {
        //获取到选中的内容的id===》table模块中找方法
        layui.use(['layer', 'table'], function () {
            var table = layui.table;
            var layer = layui.layer;

        });
    }


</script>

</body>
</html>
