<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/2
  Time: 9:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../head.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>添加评论管理</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>
<body>

<form id="addForm" class="layui-form" enctype="multipart/form-data">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0;">

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">订单编号</label>
                <div class="layui-input-block">
                    <select name="orderId" id="orderId" lay-verify="required" lay-filter="orderId">
                        <option value="">请选择订单编号</option>
                        <c:forEach items="${ol}" var="l">
                            <option value="${l.orderId }"
                                    <c:if test="${cs.orderId == l.orderId}">selected</c:if> >${l.orderSn }</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="layui-input-inline" id="Lay_agenid" style="width: 300px">
                <label class="layui-form-label">商品名称</label>
                <div class="layui-input-block">
                    <select name="goodsId" id="goodsId" lay-filter="agenId">
                        <option value="">请选择商品</option>
                    </select>
                </div>
            </div>
        </div>


        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">用户昵称</label>
                <div class="layui-input-block">
                    <select name="customerId" id="customerId" lay-verify="required" lay-filter="customerId">
                        <option value="">请选择用户昵称</option>
                        <c:forEach items="${us}" var="l">
                            <option value="${l.customerId }"
                                    <c:if test="${cs.customerId == l.customerId}">selected</c:if> >${l.nickname }</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>


        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <div class="layui-form-item">
                    <label class="layui-form-label">评论标题</label>
                    <div class="layui-input-block">
                        <input name="title" value="${cs.title}" id="title" lay-verify="required"
                               class="layui-input">
                    </div>
                </div>
            </div>

        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">评论内容</label>
            <div class="layui-input-block">
                <textarea name="content" placeholder="请输入回复内容" class="layui-textarea"></textarea>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">评论图片</label>
                <div class="layui-input-block">
                    <button type="button" class="layui-btn" id="upload_customer_pic">
                        <i class="layui-icon">&#xe67c;</i>选择图片
                    </button>

                </div>
            </div>
            <div class="layui-inline" style="width: 300px">
                <div class="layui-upload-list"></div>
                <c:if test="${cs.pictureurl!=null}">
                    <img class="layui-upload-img" name="pictureurl"
                         src="${pageContext.request.contextPath}${cs.pictureurl}" id="pictureurl"
                         style="max-width: 200px;max-height: 200px; padding-left: 40px;">
                </c:if>

            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">商品评分</label>
                <div class="layui-input-block">
                    <input name="goodsScore" value="${cs.goodsScore}" lay-verify="required" id="goodsScore"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">服务评分</label>
                <div class="layui-input-block">
                    <input name="serviceScore" value="${cs.serviceScore}" id="serviceScore" class="layui-input">
                </div>
            </div>
        </div>

    </div>


    <div class="layui-form-item" style="width: 600px">
        <div class="layui-input-block" style="text-align: right">
            <button type="submit" class="layui-btn" id="btnSub" lay-filter="btnSub" lay-submit>立即提交</button>
        </div>
    </div>
</form>


<script type="text/javascript">
    layui.use(['form', 'upload', 'layer'], function () {
        var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        var layer = layui.layer;
        var $ = layui.jquery;
        var upload = layui.upload;
        form.render();
        $("#Lay_agenid").hide();
        form.on('select(orderId)', function (data) {
            if (data.value != null && data.value != "") {
                $("#Lay_agenid").show();
                $.ajax({
                    type: 'POST',
                    url: "${pageContext.request.contextPath}/goodscomment/selectProductByOrderSn",
                    data: {"orderId": data.value},
                    dataType: 'json',
                    success: function (e) {
                        console.log(e.data);
                        //empty() 方法从被选元素移除所有内容
                        $("select[name='goodsId']").empty();
                        var html = "<option value=''>请选择商品</option>";
                        $(e.data).each(function (v, k) {
                            html += "<option value='" + k.goodsId + "'>" + k.goodsName + "</option>";
                        });
                        //把遍历的数据放到select表里面
                        $("select[name='goodsId']").append(html);
                        //从新刷新了一下下拉框
                        form.render('select');      //重新渲染
                    }
                });
            } else {
                $("#Lay_agenid").hide();
            }
        });


        //执行实例
        var uploadInst = upload.render({
            elem: '#upload_customer_pic', //绑定元素
            accept: "images",
            acceptMime: 'image/*',//打开文件选择框时,只显示图片文件
            auto: false,  //是否选完文件后自动上传。默认值：true
            bindAction: '#btnSub',
            size: 5120,	//设置文件最大可允许上传的大小，单位 KB
            multiple: false,	//是否允许多文件上传
            drag: true,	//是否接受拖拽的文件上传
            choose: function (obj) {
                //将每次选择的文件追加到文件队列
                var files = obj.pushFile();
                //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
                obj.preview(function (index, file, result) {
                    $(".layui-upload-list").append('<img src="' + result + '" id="remove_' + index + '" title="双击删除该图片" style="width:200px;height:auto;">')
                    $('#remove_' + index).bind('dblclick', function () {//双击删除指定预上传图片
                        delete files[index];//删除指定图片
                        $(this).remove();
                    });
                    // console.log(index); //得到文件索引
                    //  console.log(file); //得到文件对象
                    //  console.log(result); //得到文件base64编码，比如图片
                });
            },
            done: function (res) {
                //上传完毕回调
                //1.关闭掉添加弹出层
                layer.close(index);
                //2.提示添加成功
                layer.alert("添加成功", {time: 3000});
                //3.刷新table
                table.reload("Lay_goods_pic");
            }
            , error: function () {
                //请求异常回调
                layer.alert("添加失败", {time: 3000});
            }
        });
        //监听提交
        form.on('submit(btnSub)', function (data) {
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            var formData = new FormData(document.getElementById("addForm"));
            console.log(data.field);
            $.ajax({
                url: '${pageContext.request.contextPath}/goodscomment/addgoodscomment',
                data: formData,
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    layer.msg('操作成功', {
                        offset: '15px',
                        icon: 6,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                },
                error: function (data) {
                    layer.msg('操作失败', {
                        offset: '15px',
                        icon: 5,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                }
            });
            return false;
        });
    });

</script>
</body>
</html>

