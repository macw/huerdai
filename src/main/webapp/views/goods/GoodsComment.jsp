<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../head.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>商品评论管理管理</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>
<body>

<div class="layui-card-body">
    <div style="padding-bottom: 10px;" id="LAY_lay_add">
        <button type="button" class="layui-btn layui-btn-danger" onclick="doMultiDelete()">
            <i class="layui-icon layui-icon-delete"></i> 批量删除
        </button>
        <button class="layui-btn layuiadmin-btn-role " data-type="add" id="add">
            <i class="layui-icon layui-icon-add-circle-fine"></i> 添加评论
        </button>
        &nbsp;
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="search" id="Lay_toSearch_GoodsName" placeholder="请输入商品名称" autocomplete="off"
                   class="layui-input">
        </div>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="search" id="Lay_toSearch_OrderSn" placeholder="请输入订单编号" autocomplete="off"
                   class="layui-input">
        </div>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="search" id="Lay_toSearch_CustomerName" placeholder="请输入用户名称" autocomplete="off"
                   class="layui-input">
        </div>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="search" id="Lay_toSearch_Title" placeholder="请输入评论标题" autocomplete="off"
                   class="layui-input">
        </div>


        <div class="layui-input-inline" style="width: 100px;">
            <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                <i class="layui-icon layui-icon-search"></i> 搜索
            </button>
        </div>
    </div>


    <table id="Lay_back_table" lay-filter="Lay_back_table"></table>


</div>



<%--弹出层--%>

<form id="addForm" class="layui-form">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0;display: none">
        <div class="layui-form-item">
            <label class="layui-form-label">请输入回复内容</label>
            <div class="layui-input-block">
                <textarea name="content" placeholder="请输入回复内容" class="layui-textarea"></textarea>
            </div>
        </div>

        <div class="layui-form-item"  style="text-align: right">
            <button lay-submit  lay-filter="LAY-submit" class="layui-btn" id="updateSubmitBtn">回复</button>
        </div>
    </div>
</form>

<script type="text/html" id="updateAndDelete">


    <button type="button" class="layui-btn" lay-event="reply">
        <i class="layui-icon layui-icon-reply-fill"></i>添加/查看回复
    </button>

  <%-- <button type="button" class="layui-btn  layui-btn-warm" lay-event="details">
        </i>查看回复
    </button>--%>
    <button type="button" class="layui-btn layui-btn-danger" lay-event="del">
        <i class="layui-icon layui-icon-delete"></i>删除
    </button>

</script>


<script type="text/javascript">
    layui.use(['table', "layer", 'form'], function () {
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var $ = layui.jquery;

        var layTab = table.render({
            elem: '#Lay_back_table',
            url: '${pageContext.request.contextPath}/goodscomment/selectData', //数据接口
            page: true,
            limit: 5,
            limits: [5,10, 20, 30],
            // width: 'auto',
            //toolbar: "#LAY_lay_add",
            cols: [[
                {type: "checkbox"},
                {title: "订单编号", field: "orderSn", align: "center",sort:true, width: "150"},
                {title: "商品名称", field: "goodsName", align: "center", width: "130"},
                {title: "用户昵称", field: "customerName", align: "center", width: "100"},
                {title: "评论标题", field: "title", align: "center", width: "100"},
                {title: "评论内容", field: "content", align: "center", width: "300"},
                {
                    title: "评论图片",
                    field: "pictureurl",
                    width: "180",
                    align: "center",
                    event: "to_big_pic",
                    templet: function (d) {
                        return '<img src="${pageContext.request.contextPath}' + d.pictureurl + '">'
                    }
                },
                {title: "评分(星级)", field: "goodsScore", align: "center", width: "130",
                    templet: function (d) {
                        var str = '';
                        if (d.goodsScore !=null){
                            str+= "商品评分:"+d.goodsScore+"</br>";
                        }
                        if(d.serviceScore !=null){
                            str+="服务评分:"+d.serviceScore;
                        }
                        return str;
                    }
                },
                {
                    title: "时间",
                    field: "auditTime", align: "center", width: "200",
                    templet: function (d) {
                        var str = '';
                        if (d.auditTime !=null){
                            str+= "评论时间:"+layui.util.toDateString(d.auditTime)+"</br>";
                        }
                        if(d.modifiedTime !=null){
                            str+="修改时间:"+layui.util.toDateString(d.modifiedTime);
                        }
                        return str;
                    }
                        
                },
                {
                    title: "审核状态", width: 130, align: "center",
                    templet: function (d) {
                        if (d.auditStatus == 1) {
                            return '<button type="button" class="layui-btn  layui-btn-normal"  lay-event="yn">已审核</button>';
                        } else {
                            return '<button type="button" class="layui-btn  layui-btn-danger"  lay-event="yn">未审核</button>';
                        }
                    }
                },
                {title: "操作", templet: "#updateAndDelete", align: "center", width: "280"}
            ]]
        });

        //刷新表格方法
        function tableReload() {
            layTab.reload('Lay_back_table', {});
        };

        //监听行工具事件
        table.on('tool(Lay_back_table)', function (obj) {
            var data = obj.data;
            if (obj.event === 'yn') {
                var str = obj.data.auditStatus == 0 ? "已审核" : "未审核";
                var state = obj.data.auditStatus == 1 ? 0 : 1;
                layer.confirm('确认要修改这个评论状态为[' + str + ']吗?', function (index) {
                    $.ajax({
                        url: '${pageContext.request.contextPath}/goodscomment/updateStatus',
                        data: {"commentId": data.commentId, "auditStatus": state},
                        type: 'post',
                        dataType: 'json',
                        success: function (data) {
                            tableReload();
                            layer.msg("操作成功", {time: 1000, icon: 6});
                        }, error: function (data) {
                            tableReload();
                            layer.msg("操作失败", {time: 1000, icon: 5});
                        }
                    });
                    layer.close(index);
                });
            } else if (obj.event === 'reply') {
                var cid = data.commentId;
                $.ajax({
                    url:"${pageContext.request.contextPath}/goodscomment/testReply?commentId="+data.commentId,
                    type:"post",
                    success:function(data){
                      //
                        if (data.code==400){
                            layer.open({
                                title: "回复评论",
                                content: $("#layuiconfig-form-role"),
                                type: 1,
                                maxmin: true,
                                area: ['500px', '280px'],
                                end: function () {
                                    window.location.reload();
                                }
                            });
                        }else {
                            layer.open({
                                title: "查看回复",
                                content: "${pageContext.request.contextPath}/goodscomment/toSeeDetail?commentId="+cid,
                                type: 2,
                                maxmin: true,
                                area: ['500px', '280px'],
                                end: function () {
                                    window.location.reload();
                                }
                            });
                        }
                    }
                });

                //当点击提交按钮的时候，会进入到这个函数
                form.on("submit(LAY-submit)",function(data){
                    // console.log(data);
                    $.ajax({
                        url:"${pageContext.request.contextPath}/goodscomment/toReply?cid="+cid,
                        data:data.field,
                        type:"post",
                        success:function(data){
                            //1.关闭掉添加弹出层
                            layer.closeAll('page');
                            //2.提示添加成功
                            layer.alert("添加"+data.msg,{time:3000});
                            //3.刷新table
                            table.reload("sysconfig_table");
                        }
                    });
                    return false;//阻止跳转；
                })
            } else if (obj.event === "to_big_pic") {
                var url = data.pictureurl;
                var path = "${pageContext.request.contextPath}" + url;
                layer.open({
                    type: 1,
                    title: "查看大图",
                    skin: 'layui-layer-rim', //加上边框
                    area: ['500', '480'], //宽高
                    // shadeClose: true, //开启遮罩关闭
                    end: function (index, layero) {
                        return false;
                    },
                    content: '<div style="text-align:center"><img src="' + path + '"/></div>'
                });
            }else if (obj.event === "details"){
                layer.open({
                    title: "查看回复",
                    content: "${pageContext.request.contextPath}/goodscomment/toSeeDetail?commentId="+data.commentId,
                    type: 2,
                    maxmin: true,
                    area: ['500px', '280px'],
                    end: function () {
                        window.location.reload();
                    }
                });
            }else if (obj.event === "del"){
                layer.confirm('确定要删除吗？', {icon: 3, title: '确认删除'}, function (index) {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/goodscomment/deletecl",
                        data: "aid=" + data.commentId,
                        success: function (data) {
                            layer.alert("删除" + data.msg, {time: 2000});
                            table.reload("Lay_back_table");
                        },
                        error: function (data) {
                            table.reload("Lay_back_table");
                            layer.msg("操作失败", {time: 1000, icon: 5});
                        }
                    });
                    layer.close(index);
                });
            }

        });
        $("#add").click(function () {
            layer.open({
                title: "添加评论",
                // content: $("#layuiconfig-form-role"),
                content: "${pageContext.request.contextPath}/goodscomment/toEditComment",
                type: 2,
                maxmin: true,
                area: ['700px', '520px'],
                end: function () {
                    window.location.reload();
                }
            });
        });

    });



    //搜索操作
    function doSearch() {
        //1.获取到输入框中输入的内容
        var GoodsName = $('#Lay_toSearch_GoodsName').val();
        var OrderSn = $('#Lay_toSearch_OrderSn').val();
        var CustomerName = $('#Lay_toSearch_CustomerName').val();
        var Title = $('#Lay_toSearch_Title').val();
        //发送请求，并且接收数据
        layui.use('table', function () {
            var table = layui.table;
            table.reload('Lay_back_table', {
                where: {"goodsName": GoodsName, "customerName": CustomerName, "orderSn": OrderSn, "title": Title}
            });
        });
    }


    function doMultiDelete() {
        //获取到选中的内容的id===》table模块中找方法
        layui.use(['layer', 'table'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var $ = layui.jquery;
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].commentId;
                    }
                    console.log("ids===" + ids);
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/goodscomment/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert("删除" + data.msg, {time: 2000});
                            //刷新table
                            table.reload("Lay_back_table");
                        }
                    })
                });
            }
        });
    }

</script>
</body>
</html>

