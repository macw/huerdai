<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/18
  Time: 16:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../head.jsp" %>

<html>
<head>
    <title>商品编辑</title>
</head>
<body>


<form id="addForm" class="layui-form">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0">

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">商品名称</label>
                <div class="layui-input-block">
                    <input name="goodsId" id="goodsId" value="${go.goodsId}" lay-type="hide" type="hidden" class="layui-input">
                    <input name="goodsname" id="goodsname" lay-verify="required" value="${go.goodsname}" class="layui-input">
                </div>
            </div>
          <%--  <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">商品规格</label>
                <div class="layui-input-block">
                    <input name="goodstype" id="goodstype" value="${go.goodstype}"  class="layui-input">
                </div>
            </div>--%>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">所属商户名称</label>
                <div class="layui-input-block">
                    <select name="merchantId" id="merchantId" lay-filter="merchantId" lay-search>
                        <option value="">一级分类</option>
                        <c:forEach items="${ml}" var="p">
                            <option value="${p.merId}"
                                    <c:if test="${go.merchantId == p.merId}">selected</c:if> >${p.merName}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

        </div>

       <%-- <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">商品编码</label>
                <div class="layui-input-block">
                    <input name="goodsCode" id="goodsCode" value="${go.goodsCode}"  class="layui-input">
                    <input name="goodsId" id="goodsId" value="${go.goodsId}" lay-type="hide" type="hidden" class="layui-input">
                </div>
            </div>
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">拼音码</label>
                <div class="layui-input-block">
                    <input name="opcode" id="opcode" value="${go.opcode}" class="layui-input">
                </div>
            </div>
        </div>--%>

        <div class="layui-form-item">
          <%--  <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">商品型号</label>
                <div class="layui-input-block">
                    <input name="goodsmodel" id="goodsmodel" value="${go.goodsmodel}" class="layui-input">
                </div>
            </div>--%>
              <div class="layui-input-inline" style="width: 300px">
                  <label class="layui-form-label">出厂价/低价</label>
                  <div class="layui-input-block">
                      <input name="exitPrice" id="exitPrice" value="${go.exitPrice}" lay-verify="required" class="layui-input">
                  </div>
              </div>
        </div>
        <div class="layui-form-item">

            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">商品单价</label>
                <div class="layui-input-block">
                    <input name="price" id="price" value="${go.price}" lay-verify="required" class="layui-input">
                </div>
            </div>
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">活动价</label>
                <div class="layui-input-block">
                    <input name="activityPrice" value="${go.activityPrice}" lay-verify="required"  id="activityPrice" class="layui-input">
                </div>
            </div>
        </div>

        <div class="layui-form-item">

            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">一级分类</label>
                <div class="layui-input-block">
                    <select name="goodsclassid" id="one" lay-verify="required" lay-filter="one" lay-search>
                        <option value="">一级分类</option>
                        <c:forEach items="${one}" var="p">
                            <option value="${p.classId}"
                                    <c:if test="${go.goodsclassid == p.classId}">selected</c:if> >${p.className}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">二级分类</label>
                <div class="layui-input-block">
                    <select name="goodsclassid" id="two" lay-filter="two" lay-search>
                        <option value="">二级分类</option>
                        <c:forEach items="${two}" var="p">
                            <option value="${p.classId}"
                                    <c:if test="${go.goodsclassid == p.classId}">selected</c:if> >${p.className}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">三级分类</label>
                <div class="layui-input-block">
                    <select name="goodsclassid" id="three" lay-filter="three" lay-search>
                        <option value="">三级分类</option>
                        <c:forEach items="${three}" var="p">
                            <option value="${p.classId}"
                                    <c:if test="${go.goodsclassid == p.classId}">selected</c:if> >${p.className}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

           <%-- <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">优惠额度</label>
                <div class="layui-input-block">
                    <input name="preferentialQuota" value="${go.preferentialQuota}" id="preferentialQuota" class="layui-input">
                </div>
            </div>--%>
        </div>

        <div class="layui-form-item">
           <%-- <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">限价标志</label>
                <div class="layui-input-block">
                    <input name="priceflag" value="${go.priceflag}" id="priceflag" class="layui-input">
                </div>
            </div>--%>
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">库存</label>
                <div class="layui-input-block">
                    <input name="stupperlimit" value="${go.stupperlimit}" lay-verify="required" id="stupperlimit" class="layui-input">
                </div>
            </div>


        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">库存下限</label>
                <div class="layui-input-block">
                    <input name="stlowerlimit" value="${go.stlowerlimit}" id="stlowerlimit" class="layui-input">
                </div>
            </div>
           <%-- <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">商品条码</label>
                <div class="layui-input-block">
                    <input name="barcode" id="barcode" value="${go.barcode}" class="layui-input">
                </div>
            </div>--%>


        </div>

      <%--  <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">进项税</label>
                <div class="layui-input-block">
                    <input name="invat" id="invat" value="${go.invat}" class="layui-input">
                </div>
            </div>
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">销项税</label>
                <div class="layui-input-block">
                    <input name="outvat" id="outvat" value="${go.outvat}" class="layui-input">
                </div>
            </div>
        </div>
--%>
       <%-- <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">一级利润</label>
                <div class="layui-input-block">
                    <input name="primaryProfit" id="primaryProfit" value="${go.primaryProfit}" class="layui-input">
                </div>
            </div>
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">二级利润</label>
                <div class="layui-input-block">
                    <input name="secondaryProfit" id="secondaryProfit" value="${go.secondaryProfit}" class="layui-input">
                </div>
            </div>
        </div>
--%>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">商品头图</label>
                <div class="layui-input-block">
                    <button type="button" class="layui-btn" id="upload_customer_pic">
                        <i class="layui-icon">&#xe67c;</i>选择图片
                    </button>

                </div>
            </div>
            <div class="layui-inline">
                <img class="layui-upload-img" name="pictureUrl" src="${go.pictureUrl}" id="pictureUrl" style="max-width: 200px;max-height: 200px; padding-left: 40px;">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">商品描述</label>
            <div class="layui-input-block">
                <textarea name="descript" id="descript"  class="layui-textarea">${go.descript}</textarea>
            </div>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button type="submit" class="layui-btn" id="btnSub" lay-filter="btnSub" lay-submit>立即提交</button>
        </div>
    </div>
</form>


<script type="text/javascript">
    layui.use(['form','upload', 'layer'], function () {
        var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        var layer = layui.layer;
        var upload = layui.upload;
        var $ = layui.jquery;

        form.on('select(one)', function (data) {
            $("#three").empty();
            $("#two").empty();
            var str="<option value=''>三级分类</option>";
            $("#district").append(str);
            str="<option value=''>二级分类</option>";
            if (data.value.length <= 0) {
                $("#two").append(str);
                form.render('select');
            } else {
                $.ajax({
                    url: '<%=basePath%>goodscategory/selectTwoLevel?classId=' + data.value,
                    type: 'post',
                    dataType: 'json',
                    success: function (d) {
                        if(d.data !=null){
                            $(d.data).each(function (i, t) {
                                str += "<option value='" + t.classId + "'>" + t.className + "</option>"
                            });
                        }
                        $("#two").append(str);
                        form.render('select');
                    }
                });
            }
        });
        form.on('select(two)', function (data) {
            var str="<option value=''>三级分类</option>";
            $.ajax({
                url: '<%=basePath%>goodscategory/selectTwoLevel?classId=' + data.value,
                type: 'post',
                dataType: 'json',
                success: function (d) {
                    $("#three").empty();

                    if(d.data !=null) {
                        $(d.data).each(function (i, t) {
                            str += "<option value='" + t.classId + "'>" + t.className + "</option>"
                        });
                    }
                    $("#three").append(str);
                    form.render('select');
                }
            });
        });

        upload.render({
            elem: '#upload_customer_pic', //绑定元素
            accept: "images",
            acceptMime: 'image/*',//打开文件选择框时,只显示图片文件
            auto: false,  //是否选完文件后自动上传。默认值：true
            // bindAction: '#btnSub',
            choose : function(obj) {
                obj.preview(function(index, file, result) {
                    $('#pictureUrl').attr('src', result);

                })
            }
        });

        //监听提交
        form.on('submit(btnSub)', function (data) {
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            var formData = new FormData(document.getElementById("addForm"));
            var id = '';
            if ($("#one option:selected").val() != null && $("#one option:selected").val() != "") {
                id = $("#one option:selected").val();
            }
            if ($("#two option:selected").val() != null && $("#two option:selected").val() != "") {
                id = $("#two option:selected").val();
            }
            if ($("#three option:selected").val() != null && $("#three option:selected").val() != "") {
                id = $("#three option:selected").val();
            }
            console.log(data.field);
            // alert(id);
            $.ajax({
                url: '${pageContext.request.contextPath}/goods/addOrUpdate?classId='+id,
                data: formData,
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    layer.msg('操作成功', {
                        offset: '15px',
                        icon: 6,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                },
                error: function (data) {
                    layer.msg('操作失败', {
                        offset: '15px',
                        icon: 5,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                }
            });
            return false;
        });
    });

</script>

</body>
</html>
