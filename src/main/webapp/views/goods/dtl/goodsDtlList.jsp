<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/9/18
  Time: 17:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../head.jsp" %>
<html>
<head>
    <title>商品规格信息管理</title>
</head>
<body>
<div class="layui-card-body">
    <div class="layui-form">
        <div style="padding-bottom: 10px;" id="LAY_lay_add">
            <button type="button" class="layui-btn layui-btn-danger" <%--onclick="doMultiDelete()"--%>
                    id="doMultiDelete">
                <i class="layui-icon layui-icon-delete"></i> 批量删除
            </button>
            <button class="layui-btn layuiadmin-btn-role " data-type="add" id="add">
                <i class="layui-icon layui-icon-add-circle-fine"></i> 添加
            </button>
            &nbsp;
           <%-- <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="goodsname" id="goodsname" placeholder="请输入商品名称" autocomplete="off"
                       class="layui-input">
            </div>



            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" lay-filter="search" lay-submit>
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>--%>

            <%--  <button class="layui-btn layui-btn-warm" data-type="toSeeDetails" id="toSeeDetails">
                  <i class="layui-icon layui-icon-app"></i> 查看详情
              </button>--%>
        </div>
    </div>

    <table id="Lay_back_table" lay-filter="Lay_back_table"></table>


</div>

<script type="text/html" id="updateAndDelete">
    <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">
        <i class="layui-icon layui-icon-edit"></i>修改
    </button>
    <button type="button" class="layui-btn layui-btn-danger" lay-event="del">
        <i class="layui-icon layui-icon-delete"></i> 删除
    </button>
</script>

<%--弹出层--%>


<script type="text/javascript">
    layui.use(['table', "layer", 'form'], function () {
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var $ = layui.jquery;

        var layTab = table.render({
            elem: '#Lay_back_table',
            url: '${pageContext.request.contextPath}/goodsDtl/selectAll?goodsId='+${goodsId}, //数据接口
            page: true,
            limit: 5,
            limits: [5,10, 20, 30],
            // width: '3000',
            //toolbar: "#LAY_lay_add",
            cols: [[
                {type: "checkbox"},
                {title: "商品名称", field: "goodsname", width: 200, align: "center"},
                {title: "规格类型", field: "type", width: 150, align: "center",
                    templet:function (d) {
                        if (d.type==1){
                            return "颜色";
                        }else if (d.type == 2){
                            return "尺码";
                        }
                    }
                },
                {title: "规格名称", field: "spec", width: 100, align: "center"},
                {title: "规格加价", field: "dtlPrice", width: 150, align: "center"},
                {
                    title: "规格图片", field: "classimgAdress", width: 150, align: "center", templet: function (d) {
                        return '<img src="' + d.classimgAdress + '">'
                    }
                },
                {title: "创建人", field: "createUser",align:"center"},
                {title: "创建时间",align:"center",
                    templet: function(d){
                        if(d.createTime!=null){
                            return layui.util.toDateString(d.createTime);
                        }
                        return '';
                    }
                },
                {title: "最近修改人", field: "updateUser",align:"center"},
                {title: "最近修改时间",align:"center",
                    templet: function(d){
                        if(d.updateTime!=null){
                            return layui.util.toDateString(d.updateTime);
                        }
                        return '';
                    }
                },

                {title: "操作", templet: "#updateAndDelete", width: 300, align: "center"}
            ]]
        });

        //刷新表格方法
        function tableReload() {
            layTab.reload('Lay_back_table', {});
        };


        //监听行工具事件
        table.on('tool(Lay_back_table)', function (obj) {
            var data = obj.data;
            if (obj.event === 'del') {
                layer.confirm('确定要删除吗？', {icon: 3, title: '删除'}, function (index) {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/goodsDtl/deleteOne",
                        data: "id=" + data.goodsdtlId,
                        success: function (data) {
                            layer.alert("删除" + data.msg, {time: 2000});
                            tableReload();
                            layer.close(index);
                        }
                    })
                });
            } else if (obj.event === 'edit') {
                // alert(data.goodsdtlId);
                layer.open({
                    title: "修改",
                    content: "${pageContext.request.contextPath}/goodsDtl/togoodsDtlEdit?goodsId=" + ${goodsId}+"&goodsdtlId="+data.goodsdtlId,
                    type: 2,
                    maxmin: true,
                    area: ['690px', '700px'],
                    end: function () {
                        window.location.reload();
                    }
                });
            }
        });


        $("#add").click(function () {
            layer.open({
                title: "添加商品规格",
                content: "${pageContext.request.contextPath}/goodsDtl/togoodsDtlEdit?goodsId=" + ${goodsId},
                type: 2,
                maxmin: true,
                area: ['690px', '700px'],
                end: function () {
                    window.location.reload();
                }
            });
        });


        //执行删除多条数据，批量删除操作
        $("#doMultiDelete").click(function () {
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            console.log("111~~~~" + checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据!");
            } else {
                layer.confirm("确定要删除选中的所有数据?", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].goodsdtlId;
                    }
                    console.log("ids===" + ids);
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/goodsDtl/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert("删除" + data.msg, {time: 2000});
                            //刷新table
                            table.reload("Lay_back_table");
                        }
                    })
                });
            }
        })

    });

</script>

</body>
</html>
