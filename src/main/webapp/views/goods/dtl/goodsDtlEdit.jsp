<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/18
  Time: 16:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../head.jsp" %>

<html>
<head>
    <title>商品规格编辑</title>
</head>
<body>


<form id="addForm" class="layui-form">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0">

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">规格类型</label>
                <div class="layui-input-block">
                    <select name="type" id="type" lay-filter="type" lay-search>
                        <option value="">请选择规格类型</option>
                        <option value="1" <c:if test="${dtl.type == 1}">selected</c:if> >颜色</option>
                        <option value="2" <c:if test="${dtl.type == 2}">selected</c:if> >尺码</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">规格名称</label>
                <div class="layui-input-block">
                    <input name="spec" id="spec" value="${dtl.spec}" class="layui-input">
                    <input type="hidden" name="goodsId" value="${goodsId}">
                    <input type="hidden" name="goodsdtlId" value="${dtl.goodsdtlId}">
                </div>
            </div>
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">规格加价</label>
                <div class="layui-input-block">
                    <input name="dtlPrice" id="dtlPrice" value="${dtl.dtlPrice}" class="layui-input">
                </div>
            </div>
        </div>


        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">规格图片</label>
                <div class="layui-input-block">
                    <button type="button" class="layui-btn" id="upload_customer_pic">
                        <i class="layui-icon">&#xe67c;</i>选择图片
                    </button>
                </div>
            </div>
            <div class="layui-inline">
                <img class="layui-upload-img" name="classimgAdress" src="${dtl.classimgAdress}" id="pictureUrl"
                     style="max-width: 200px;max-height: 200px; padding-left: 40px;">
            </div>
        </div>


    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button type="submit" class="layui-btn" id="btnSub" lay-filter="btnSub" lay-submit>立即提交</button>
        </div>
    </div>
</form>


<script type="text/javascript">
    layui.use(['form', 'upload', 'layer'], function () {
        var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        var layer = layui.layer;
        var upload = layui.upload;
        var $ = layui.jquery;

        form.render('select');

        upload.render({
            elem: '#upload_customer_pic', //绑定元素
            accept: "images",
            acceptMime: 'image/*',//打开文件选择框时,只显示图片文件
            auto: false,  //是否选完文件后自动上传。默认值：true
            // bindAction: '#btnSub',
            choose: function (obj) {
                obj.preview(function (index, file, result) {
                    $('#pictureUrl').attr('src', result);
                })
            }
        });

        //监听提交
        form.on('submit(btnSub)', function (data) {
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            var formData = new FormData(document.getElementById("addForm"));
            console.log(data.field);
            $.ajax({
                url: '${pageContext.request.contextPath}/goodsDtl/addOrUpdate?goodsId=' + ${goodsId},
                data: formData,
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    layer.msg('操作成功', {
                        offset: '15px',
                        icon: 6,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                },
                error: function (data) {
                    layer.msg('操作失败', {
                        offset: '15px',
                        icon: 5,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                }
            });
            return false;
        });
    });

</script>

</body>
</html>
