<%@ page language="java" import="java.util.*"
	trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>支付渠道</title>
<%@ include file="../../head.jsp"%>
<style type="text/css">
.layui-table-cell {
	height: auto;
	line-height: 30px;
}
</style>
</head>
<body>
<div class="layui-fluid">
		<div class="layui-card">
			<div class="layui-form">
				<div style="padding-bottom: 10px;">
					<div class="layui-input-inline" style="width: 100px;">
						<button type="button" class="layui-btn" id="add">添加</button>
					</div>
					<div class="layui-inline">
						<select name="merchantId" id="merchantId" lay-search>
							<option value="">选择公司</option>
							<c:forEach items="${merchantList}" var="merchant">
								<option value="${merchant.mId}">${merchant.mName}</option>
							</c:forEach>
						</select>
					</div>
					<div class="layui-input-inline" style="width: 200px;">
						<select name="channelName" id="channelName" lay-search>
							<option value="">选择支付方式</option>
							<c:forEach items="${payMethods}" var="payMethod">
								<option value="${payMethod}">${payMethod}</option>
							</c:forEach>
						</select>
					</div>
					<div class="layui-input-inline" style="width: 200px;">
						<input type="text" name="userName" id="userName"
							placeholder="创建人/修改人" autocomplete="off" class="layui-input">
					</div>
					<div class="layui-input-inline" style="width: 100px;">
						<button type="button" class="layui-btn layui-btn-normal"
							lay-filter="search" lay-submit>
							<i class="layui-icon layui-icon-search"></i>搜索
						</button>
					</div>
				</div>
			</div>
			<div class="layui-card-body">
				<table id="tab" lay-filter="tab"></table>
			</div>
		</div>
	</div>
</body>
<script type="text/html" id="tool">
        <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">编辑</button>
        <button type="button" class="layui-btn layui-btn-danger" lay-event="del">删除</button>
  </script>
  

<script type="text/javascript">
        layui.use(['table', 'form'], function () {
        	var merchantId ='${dto.merchantId }',channelName = '${dto.channelName}',userName= '${dto.userName}';
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            var layTab = table.render({
                elem: '#tab',
                url: '<%=basePath%>payment/channel/selectData', //数据接口
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                defaultToolbar: [],
                where: {merchantId: merchantId, channelName: channelName,userName:userName},
                cols: [[
                    {title: "商户名称", field: "merchantName",align:"center",width:120},
                    {title: "渠道名称", field: "channelName",align:"center",width:120},
                    {title: "同步回调地址", field: "syncUrl",align:"center",width:200},
                    {title: "异步回调地址", field: "asynUrl",align:"center",width:200},
                    {title: "公匙", field: "publicKey",align:"center",width:300},
                    {title: "秘匙", field: "privateKey",align:"center",width:300},
                    //{title: "乐观锁", field: "revision",align:"center"},
                    {title: "创建人", field: "createdBy",align:"center",width:90},
                    {title: "创建时间",align:"center",
                    	templet: function(d){
                    		if(d.createdTime!=null){
	                    		return layui.util.toDateString(d.createdTime);
                    		}
                    		return '';
                    	},width:160	
                    },
                    {title: "最近修改人", field: "updatedBy",align:"center",width:90},
                    {title: "最近修改时间",align:"center",
                    	templet: function(d){
                    		if(d.updatedTime!=null){
	                    		return layui.util.toDateString(d.updatedTime);
                    		}
                    		return '';
                    	},width:160	
                    },
                    {title: "状态", width :100,align:"center",
                    	templet: function(d){
	                    	if(d.channelState == 0){
	                    		return '<button type="button" class="layui-btn  layui-btn-normal"  lay-event="yn">启用</button>';
	                    	}else{
	                    		return '<button type="button" class="layui-btn  layui-btn-danger"  lay-event="yn">禁用</button>';
	                    	}
                   		},width:100
                	},
                    {title: "操作",width :175, align:"center", templet: "#tool"}
                ]] ,done:function(res, curr){
      	    	  var brforeCurr = curr; // 获得当前页码
    	    	  var dataLength = res.data.length; // 获得当前页的记录数
    	    	  var count = res.count; // 获得总记录数
    	    	  if(dataLength == 0 && count != 0){ //如果当前页的记录数为0并且总记录数不为0
    	    		  table.reload("tab",{ // 刷新表格到上一页
    	    			  page:{
    	    				 curr:brforeCurr-1
    	    			  }
    	    		  });
    	    	  }
    	      }
            });
        	//监听搜索
            form.on('submit(search)', function(data){
              var field = data.field;
              //执行重载
              layTab.reload({
                where: field,
                page: {
                    curr: 1 //重新从第 1 页开始
                  }
              });
            });
        	
          //监听事件
          table.on('tool(tab)', function(obj){
        	  var data = obj.data; //获得当前行数据
        	  var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        	  var tr = obj.tr; //获得当前行 tr 的 DOM 对象（如果有的话）
        	  switch(obj.event){
              case 'del':
            	  layer.confirm('确认要删除这条渠道信息?', function(index){
            	      $.ajax({
            	    	  url:'<%=basePath%>payment/channel/delByChannelId?channelId='+obj.data.channelId,
            	    	  type:'get',
            	    	  datatype:'json',
            	    	  success:function(data){
                     		 tableReload();
                     		 layer.msg("操作成功", {time: 1000, icon:6});
                     	 },error:function(data){
                     		 tableReload();
                     		 layer.msg("操作失败", {time: 1000, icon:5});
                     	 }
            	      });
        	    	});
              break;
              case 'edit':
            	  layer.open({
             		  type: 2,
             		  title: '添加支付渠道',
             		  shadeClose: true,
             		  shade: 0.8,
             		  area: ['50%', '60%'],
             		  content: '<%=basePath%>payment/channel/editPage?channelId='+obj.data.channelId,
              		}); 
              break;
              case 'yn':
                  var str= obj.data.channelState==1?"启用":"禁用";
            	  var channelState = obj.data.channelState==1?0:1;
            	  layer.confirm(str+'这条渠道信息?', function(index){
            	      $.ajax({
            	    	  url:'<%=basePath%>payment/channel/updateOrSave',
            	    	  data:{channelId:obj.data.channelId,channelState},
            	    	  type:'post',
            	    	  datatype:'json',
            	    	  success:function(data){
                     		 tableReload();
                     		 layer.msg("操作成功", {time: 1000, icon:6});
                     	 },error:function(data){
                     		 tableReload();
                     		 layer.msg("操作失败", {time: 1000, icon:5});
                     	 }
            	      });
        	    	});
                break;
            };
          });
          function tableReload(){
                table.reload('tab', {
                });
          };
          $("#add").click(function(){
        	  layer.open({
         		  type: 2,
         		  title: '添加支付渠道',
         		  shadeClose: true,
         		  shade: 0.8,
         		  area: ['50%', '60%'],
         		  content: '<%=basePath%>payment/channel/editPage'
          	});  
          });
        });
</script>
</html>

