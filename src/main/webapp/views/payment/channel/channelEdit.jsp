<%@ page language="java" import="java.util.*"
	trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>商户信息管理</title>
<%@ include file="../../head.jsp"%>
<style type="text/css">
.width100{
 	width: 100px;
};

.width550{
	width: 550px;
}

</style>
</head>
<body>
	<form class="layui-form" style="margin-top: 20px;">
		<input type="hidden" name="channelId" value="${channel.channelId}">
		<div class="layui-form-item">
			<div class="layui-inline">
		      <label class="layui-form-label width100">选择商户</label>
		      <div class="layui-input-inline">
		        <select name="merchantId" id="merchantId" lay-verify="required" lay-search >
					<option value=""></option>
					<c:forEach items="${merchantList}" var="merchant">
						<option value="${merchant.mId}"
							<c:if test="${channel.merchantId == merchant.mId }">selected</c:if>>${merchant.mName}</option>
					</c:forEach>
				</select>
		      </div>
		    </div>
		    <div class="layui-inline">
		      <label class="layui-form-label width100">支付名称</label>
		      <div class="layui-input-inline">
		        <input type="text" name="channelName" value="${channel.channelName}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
		    </div>
		</div>
		
		<div class="layui-form-item">
				<label class="layui-form-label width100">同步回调地址</label>
				<div class="layui-input-block">
					<input type="text" name="syncUrl" value="${channel.syncUrl}" lay-verify="required" autocomplete="off" class="layui-input" style="width: 535px;">
				</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label width100">异步回调地址</label>
			<div class="layui-input-block">
				<input type="text" name="asynUrl" value="${channel.asynUrl}" lay-verify="required" autocomplete="off" class="layui-input" style="width: 535px;">
			</div>
		</div>
		<div class="layui-form-item layui-form-text">
			<label class="layui-form-label width100">公钥</label>
			<div class="layui-input-block">
		       <textarea placeholder="请输入内容" class="layui-textarea" name="publicKey" lay-verify="required" autocomplete="off" style="width: 535px;">${channel.publicKey}</textarea>
		    </div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label width100">私钥</label>
			<div class="layui-input-block">
		       <textarea placeholder="请输入内容" class="layui-textarea" name="privateKey" lay-verify="required" autocomplete="off" style="width: 535px;">${channel.privateKey}</textarea>
		    </div>
		</div>
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button type="submit" class="layui-btn" lay-filter="btnSub" lay-submit>立即提交</button>
			</div>
		</div>
	</form>
</body>



<script type="text/javascript">
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
          //监听提交
            form.on('submit(btnSub)', function(data){
           		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            	$.ajax({
                url:'<%=basePath%>payment/channel/updateOrSave',
				data : data.field,
				type : 'post',
				dataType : 'json',
				success : function(data) {
					layer.msg('操作成功', {
						offset : '15px',
						icon : 6,
						time : 1000
					}, function() {
						parent.layer.close(index);
						parent.layui.table.reload('tab',{
							
						});
					});
				},
				error : function(data) {
					layer.msg('操作失败', {
						offset : '15px',
						icon : 5,
						time : 1000
					}, function() {
						parent.layer.close(index);
						parent.layui.table.reload('tab',{
							
						});
					});
				}
			});
			return false;
		});
	});
</script>
</html>

