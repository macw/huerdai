<%@ page language="java" import="java.util.*"
	trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>支付渠道</title>
<%@ include file="../../head.jsp"%>
<style type="text/css">
.layui-table-cell {
	height: auto;
	line-height: 30px;
}
</style>
</head>
<body>
<div class="layui-fluid">
		<div class="layui-card">
			<div class="layui-form">
				<div style="padding-bottom: 10px;">
					<div class="layui-inline">
						<select name="merchantId" id="merchantId" lay-search>
							<option value="">选择公司</option>
							<c:forEach items="${merchantList}" var="merchant">
								<option value="${merchant.mId}">${merchant.mName}</option>
							</c:forEach>
						</select>
					</div>
					<div class="layui-input-inline" style="width: 200px;">
						<select name="channelName" id="channelName" lay-search>
							<option value="">选择支付方式</option>
							<c:forEach items="${payMethods}" var="payMethod">
								<option value="${payMethod}">${payMethod}</option>
							</c:forEach>
						</select>
					</div>
					<div class="layui-input-inline" style="width: 200px;">
						<input type="text" name="createdBy" id="createdBy"
							placeholder="创建人" autocomplete="off" class="layui-input">
					</div>
					<div class="layui-input-inline" style="width: 100px;">
						<button type="button" class="layui-btn layui-btn-normal"
							lay-filter="search" lay-submit>
							<i class="layui-icon layui-icon-search"></i>搜索
						</button>
					</div>
				</div>
			</div>
			<div class="layui-card-body">
				<table id="tab" lay-filter="tab"></table>
			</div>
		</div>
	</div>
</body>
<script type="text/html" id="tool">
        <button type="button" class="layui-btn layui-btn-danger" lay-event="del">删除</button>
  </script>

<script type="text/javascript">
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            var layTab = table.render({
                elem: '#tab',
                url: '<%=basePath%>payment/transLog/selectData', //数据接口
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                defaultToolbar: [],
                cols: [[
                    {title: "商户名称", field: "merchantName",align:"center"},
                    {title: "支付渠道", field: "channelName",align:"center"},
                    {title: "订单号", field: "orderId",align:"center"},
                    {title: "交易金额", field: "payAmount",align:"center"},
                    {title: "描述", field: "commodityName",align:"center"},
                    {title: "同步回调日志", field: "synchLog",align:"center"},
                    {title: "异步回调日志", field: "asyncLog",align:"center"},
                    {title: "创建人", field: "createdBy",align:"center"},
                    {title: "创建时间",align:"center",
                    	templet: function(d){
                    		if(d.createdTime!=null){
	                    		return layui.util.toDateString(d.createdTime);
                    		}
                    		return '';
                    	}	
                    },
                    {title: "操作", align:"center", templet: "#tool"}
                    
                ]] ,done:function(res, curr){
      	    	  var brforeCurr = curr; // 获得当前页码
    	    	  var dataLength = res.data.length; // 获得当前页的记录数
    	    	  var count = res.count; // 获得总记录数
    	    	  if(dataLength == 0 && count != 0){ //如果当前页的记录数为0并且总记录数不为0
    	    		  table.reload("tab",{ // 刷新表格到上一页
    	    			  page:{
    	    				 curr:brforeCurr-1
    	    			  }
    	    		  });
    	    	  }
    	      }
            });
          //监听事件
            table.on('tool(tab)', function(obj){
          	  var data = obj.data; //获得当前行数据
          	  var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
          	  var tr = obj.tr; //获得当前行 tr 的 DOM 对象（如果有的话）
          	  switch(obj.event){
                case 'del':
              	  layer.confirm('确认要删除这条交易日志?', function(index){
              	      $.ajax({
              	    	  url:'<%=basePath%>payment/transLog/deleteById?id='+obj.data.id,
              	    	  type:'post',
              	    	  datatype:'json',
              	    	  success:function(data){
                       		 tableReload();
                       		 layer.msg(data.msg, {time: 1000, icon:6});
                       	 },error:function(data){
                       		 tableReload();
                       		 layer.msg(data.msg, {time: 1000, icon:5});
                       	 }
              	      });
          	    	});
                break;
              };
            });
        	//监听搜索
            form.on('submit(search)', function(data){
              var field = data.field;
              //执行重载
              layTab.reload({
                where: field,
                page: {
                    curr: 1 //重新从第 1 页开始
                  }
              });
            });
        	
            function tableReload(){
                table.reload('tab', {
                });
          	};
        });
</script>
</html>

