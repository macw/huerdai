<%@ page language="java" import="java.util.*"
	trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>优惠券管理</title>
<%@ include file="../head.jsp"%>
</head>
<body>
	<form class="layui-form" style="margin-top: 20px;">
		<input type="hidden" name="couponId" value="${dto.couponId}">
		<div class="layui-form-item">
			<div class="layui-inline">
				<label class="layui-form-label">名称</label>
				<div class="layui-input-inline">
					<input type="text" name="couponName" value="${dto.couponName}"
						lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">数量</label>
				<div class="layui-input-inline">
					<input type="text" name="count" value="${dto.count}"
						lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-inline">
				<label class="layui-form-label">使用条件</label>
				<div class="layui-input-inline">
					<input type="text" name="conditionsPrice"
						value="${dto.conditionsPrice}" lay-verify="required"
						autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">抵扣金额</label>
				<div class="layui-input-inline">
					<input type="text" name="price" value="${dto.price}"
						lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-inline">
				<label class="layui-form-label">类型</label>
				<div class="layui-input-inline">
					<select name="type" id="type" lay-verify="required" lay-search>
						<option value="">类型</option>
						<option value="1" <c:if test="${dto.type == 1 }">selected</c:if>>红包</option>
						<option value="2" <c:if test="${dto.type == 2 }">selected</c:if>>代金券</option>
					</select>
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">状态</label>
				<div class="layui-input-inline">
					<select name="status" id="status" lay-verify="required" lay-search>
						<option value="">状态</option>
						<option value="1" <c:if test="${dto.status == 1 }">selected</c:if>>已发布</option>
						<option value="2" <c:if test="${dto.status == 2 }">selected</c:if>>未发布</option>
						<option value="3" <c:if test="${dto.status == 3 }">selected</c:if>>发布中</option>
						<option value="4" <c:if test="${dto.status == 4 }">selected</c:if>>已下架</option>
						<option value="5" <c:if test="${dto.status == 5 }">selected</c:if>>过期</option>
					</select>
				</div>
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-inline">
				<label class="layui-form-label">适用范围</label>
				<div class="layui-input-inline">
					<input type="text" name="couponRange" value="${dto.couponRange}"
						lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">数量预警</label>
				<div class="layui-input-inline">
					<input type="text" name="countWarning" value="${dto.countWarning}"
						lay-verify="required" autocomplete="off" class="layui-input">
				</div>
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-inline">
				<label class="layui-form-label">发放类型</label>
				<div class="layui-input-inline">
					<select name="dsseminationType" id="dsseminationType"
						lay-verify="required" lay-search>
						<option value="">发放类型</option>
						<option value="1"
							<c:if test="${dto.dsseminationType == 1 }">selected</c:if>>自动分配</option>
						<option value="2"
							<c:if test="${dto.dsseminationType == 2 }">selected</c:if>>手动领取</option>
					</select>
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">使用类型</label>
				<div class="layui-input-inline">
					<select name="useType" id="useType" lay-verify="required"
						lay-search>
						<option value="">使用类型</option>
						<option value="1"
							<c:if test="${dto.useType == 1 }">selected</c:if>>全部可用</option>
						<option value="2"
							<c:if test="${dto.useType == 2 }">selected</c:if>>绑定线路可用</option>
					</select>
				</div>
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-inline">
				<label class="layui-form-label">重复领取</label>
				<div class="layui-input-inline">
					<select name="isRepeatedCollection" id="isRepeatedCollection"
						lay-verify="required" lay-search>
						<option value="">使用类型</option>
						<option value="1"
							<c:if test="${dto.isRepeatedCollection == 1 }">selected</c:if>>允许</option>
						<option value="2"
							<c:if test="${dto.isRepeatedCollection == 2 }">selected</c:if>>不允许</option>
					</select>
				</div>
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-inline">
				<label class="layui-form-label">有效期时间</label>
				<div class="layui-input-inline">
					<input type="text" class="layui-input" name="expiresStartDate"
						id="expiresStartDate" lay-verify="required" placeholder="开始时间" placeholder="yyyy-MM-dd"
						value="${dto.expiresStartDate }">
				</div>
			</div>
			<div class="layui-inline">
				<label class="layui-form-label">有效期时间</label>
				<div class="layui-input-inline">
					<input type="text" class="layui-input" name="expiresEndDate"
						id="expiresEndDate" lay-verify="required" placeholder="结束时间" placeholder="yyyy-MM-dd"
						value="${dto.expiresEndDate }">
				</div>
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button type="submit" class="layui-btn" lay-submit=""
					lay-filter="btnSub">立即提交</button>
			</div>
		</div>
	</form>
</body>



<script type="text/javascript">
		layui.use(['table', 'form','laydate'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            var laydate = layui.laydate;
            
        	//日期时间选择器
            laydate.render({
              elem: '#expiresStartDate'

            });
          //日期时间选择器
            laydate.render({
                elem: '#expiresEndDate'
              });
          //监听提交
            form.on('submit(btnSub)', function(data){
           		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            	$.ajax({
               	url:'<%=basePath%>coupon/saveOrUpdate',
				data : data.field,
				type : 'post',
				dataType : 'json',
				success : function(data) {
	            	if(data.se){
	              		parent.layer.msg(data.msg, { offset: '15px',icon: 6,time: 1000},function(){
	              			parent.layer.close(index);
	    	            	parent.location.reload();
	              		});
	            	}else{
	              		parent.layer.msg(data.msg, { offset: '15px',icon: 5,time: 1000},function(){
	              			parent.layer.close(index);
	    	            	parent.location.reload();
	              		});
	            	}
				},
				error : function(data) {
					parent.layer.close(index);
	            	parent.location.reload();
	            	if(data.msg==null || data.msg.length<=0){
	              		parent.layer.msg('操作失败', { offset: '15px',icon: 5,time: 1000});
	            	}else{
	              		parent.layer.msg(data.msg, { offset: '15px',icon: 5,time: 1000});
	            	}
				}
			});

			return false;
		});
	});
</script>
</html>

