<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>优惠券管理管理</title>
	<%@ include file="../head.jsp" %>
	 <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>
</head>
<body>
<div class="layui-fluid">
    <div class="layui-card-body layui-form">
        <div style="padding-bottom: 10px;">
            <div class="layui-input-inline" style="width: 100px;">
                 <button type="button" class="layui-btn add" data-type="add" onclick="add()">添加</button>
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="couponName" id="couponName" placeholder="红包、代金券名称" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
	             <select name="status" id="status" lay-search>
					<option value="">状态</option>
					<option value="1">已发布</option>
					<option value="2">未发布</option>
					<option value="3">发布中</option>
					<option value="4">已下架</option>
					<option value="5">过期</option>
				</select>
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="createdName" id="createdName" placeholder="创建人/修改人" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>
        <table id="coupon-tab" lay-filter="coupon-tab"></table>
    </div>
</div>
</body>
 
  <script type="text/html" id="tool">
        <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">编辑</button>
        <button type="button" class="layui-btn layui-btn-danger" lay-event="del">删除</button>
  </script>

<script type="text/javascript">

        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            table.render({
                elem: '#coupon-tab',
                url: '<%=basePath%>coupon/selectData', //数据接口
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                cols: [[
                    {type: "checkbox"},
                    {title: "红包、代金券名称", field: "couponName",align:"center",width:200},
                    {title: "数量", field: "count",align:"center",width:80},
                    {title: "抵扣值", field: "price",align:"center",width:80},
                    {title: "类型" ,align:"center",
                    	templet: function(d){
                    		if(d.type!=null && d.type ==1){
	                    		return '红包';
                    		}else if(d.type!=null && d.type ==2){
                    			return '代金券';
                    		}
                    		return '';
                    	},width:80	
                    },
                    {title: "状态", align:"center",
                    	templet: function(d){
                    		if(d.status!=null && d.type ==1){
	                    		return '已发布';
                    		}else if(d.status!=null && d.type ==2){
                    			return '未发布';
                    		}else if(d.status!=null && d.type ==3){
                    			return '发布中';
                    		}else if(d.status!=null && d.type ==4){
                    			return '已下架';
                    		}else if(d.status!=null && d.type ==5){
                    			return '过期';
                    		}else{
	                    		return '';
                    		}
                    	},width:90	
                    },
                    {title: "适用范围", field: "couponRange",align:"center",width:150},
                    {title: "使用条件", field: "conditionsPrice",align:"center",width:100},
                    {title: "数量预警", field: "countWarning",align:"center",width:90},
                    {title: "发放类型", field: "createUser",align:"center",
                    	templet: function(d){
                    		if(d.dsseminationType!=null && d.dsseminationType == 1){
	                    		return '自动分配';
                    		}else if(d.dsseminationType!=null && d.dsseminationType == 2){
                    			return '手动分配';
                    		}
                    		return '';
                    	},width:90	
                    	
                    },
                    {title: "使用类型", field: "useType",align:"center",
                    	templet: function(d){
                    		if(d.useType!=null && d.useType == 1){
	                    		return '全部可用';
                    		}else if(d.useType!=null && d.useType == 2){
                    			return '绑定线路可用';
                    		}
                    		return '';
                    	},width:110	
                    	
                    },
                    {title: "允许重复领取", align:"center",
                    	templet: function(d){
                    		if(d.isRepeatedCollection!=null && d.isRepeatedCollection==1){
	                    		return '允许';
                    		}else if(d.isRepeatedCollection!=null && d.isRepeatedCollection==2){
                    			return '不允许';
                    		}
                    		return '';
                    	},width:120	
                    	
                    },
                    {title: "有效期", align:"center",
                    	templet: function(d){
                    		if(d.validityStartdate!=null && d.validityEnddate!=null){
                    			return layui.util.toDateString(d.validityStartdate, 'yyyy-MM-dd')+" 至 "+layui.util.toDateString(d.validityEnddate, 'yyyy-MM-dd');
                    			//return util.toDateString(d.validityStartdate, 'yyyy-MM-dd')+"至"+util.toDateString(d.validityEnddate, 'yyyy-MM-dd');
                    		}
                    		return '';
                    	},width:250	
                    },
                    {title: "创建人", field: "createdName",align:"center",width:90},
                    {title: "创建时间",align:"center",
                    	templet: function(d){
                    		if(d.creationDate!=null){
	                    		return layui.util.toDateString(d.creationDate);
                    		}
                    		return '';
                    	},width:160	
                    },
                    {title: "最近修改人", field: "lastUpdateName",align:"center",width:110},
                    {title: "最近修改时间",align:"center",
                    	templet: function(d){
                    		if(d.lastUpdateDate!=null){
	                    		return layui.util.toDateString(d.lastUpdateDate);
                    		}
                    		return '';
                    	},width:160	
                    },
                    {title: "操作",width :175, align:"center", templet: "#tool"}
                ]],done:function(res, curr){
        	    	  var brforeCurr = curr; // 获得当前页码
        	    	  var dataLength = res.data.length; // 获得当前页的记录数
        	    	  var count = res.count; // 获得总记录数
        	    	  if(dataLength == 0 && count != 0){ //如果当前页的记录数为0并且总记录数不为0
        	    		  table.reload("coupon-tab",{ // 刷新表格到上一页
        	    			  page:{
        	    				 curr:brforeCurr-1
        	    			  }
        	    		  });
        	    	  }
        	      }
            });
            
            
          	//监听行工具事件
            table.on('tool(coupon-tab)', function(obj){
              var data = obj.data;
              if(obj.event === 'del'){
                layer.confirm('真的删除这条记录吗?', function(index){
                	$.ajax({
                   	 url:'<%=basePath%>coupon/deleteByCouponId',
                   	 data: {"couponId":data.couponId},
                   	 type: 'post',
                   	 dataType: 'json',
                   	 success:function(data){
                   		 tableReload();
                   		 if(data.se){
	                   		layer.msg(data.msg, {time: 1000, icon:6});
                   		 }else{
	                   		layer.msg(data.msg, {time: 1000, icon:5});
                   		 }
                   	 },error:function(data){
                   		 tableReload();
                   		layer.msg("操作失败", {time: 1000, icon:6});
                   	 }
                    });
                  	layer.close(index);
                });
              } else if(obj.event === 'edit'){
            	  layer.open({
             		  type: 2,
             		  title: '红包优惠券编辑',
             		  shadeClose: true,
             		  shade: 0.8,
             		  area: ['710px', '530px'],
             		  content: '<%=basePath%>coupon/editPage?couponId='+data.couponId
              	});  
              }
            });
        });
        //搜索操作
        function doSearch() {
            //发送请求，并且接收数据
            layui.use('table', function () {
                var table = layui.table;
                table.reload('coupon-tab', {
                    where: {"couponName": $('#couponName').val(),"status": $('#status').val(),"createdName": $('#createdName').val()},
                    page: {
                        curr: 1 //重新从第 1 页开始
                      }
                });
            });
        };
        function tableReload(){
       	   layui.use('table', function () {
              var table = layui.table;
              table.reload('coupon-tab', {
              });
           });
        };
        function add(){
        	layui.use('layer', function(){
        		layer.open({
             		  type: 2,
             		  title: '红包优惠券添加',
             		  shadeClose: true,
             		  shade: 0.8,
             		  area: ['710px', '530px'],
             		  content: '<%=basePath%>coupon/editPage'
              	});   
       		}); 
        };
        
        
        
    </script>
</html>

