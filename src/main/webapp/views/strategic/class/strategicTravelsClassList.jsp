<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/23
  Time: 9:38
  To change this template use File | Settings | File Templates.
--%>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../head.jsp" %>
<html>
<head>
    <title>商品分类管理</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/layuiadmin/modules/treetable-lay/treetable.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/layuiadmin/modules/treetable-lay/treetable.js"></script>

</head>
<body>
<div class="layui-card-body">
    <div style="padding-bottom: 10px;" id="LAY_lay_add">
        <button type="button" class="layui-btn layui-btn-danger" id="doMultiDelete">
            <i class="layui-icon layui-icon-delete"></i> 批量删除
        </button>
        <button class="layui-btn layuiadmin-btn-role " data-type="add" id="toOpenAddLayer">
            <i class="layui-icon layui-icon-add-circle-fine"></i> 添加
        </button>
        &nbsp;
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="search" id="Lay_toSearch_input" placeholder="请输入分类名称" autocomplete="off"
                   class="layui-input">
        </div>
        <div class="layui-input-inline" style="width: 100px;">
            <button type="button" class="layui-btn layui-btn-normal" id="btn-search">
                <i class="layui-icon layui-icon-search"></i> 搜索
            </button>
        </div>
        &nbsp;
        <div class="layui-btn-group">
            <button class="layui-btn" id="btn-expand">全部展开</button>
            <button class="layui-btn" id="btn-fold">全部折叠</button>
            <button class="layui-btn" id="btn-refresh">刷新表格</button>
        </div>
    </div>

    <%--树形表格--%>
    <table class="layui-table" id="Lay_category_treeTable" lay-filter="Lay_category_treeTable"></table>

</div>

<script type="text/html" id="updateAndDelete">
    <button type="button" class="layui-btn  layui-btn-normal" lay-event="toOpenUpdateLayer">
        <i class="layui-icon layui-icon-edit"></i>修改
    </button>
    <button type="button" class="layui-btn layui-btn-danger" lay-event="doDelete">
        <i class="layui-icon layui-icon-delete"></i> 删除
    </button>
</script>


<script type="text/javascript">

    layui.config({
        base: '${pageContext.request.contextPath}/layuiadmin/modules/' //   资源所在路径
    }).extend({
        treetable: 'treetable-lay/treetable'
    }).use(['treetable', 'table', 'layer'], function () {
        var treetable = layui.treetable;
        var layer = layui.layer;
        var table = layui.table;
        var $ = layui.jquery;

        var re;

        // 渲染表格
        var renderTable = function () {
            layer.load(3);
            re = treetable.render({
                elem: '#Lay_category_treeTable',
                url: '${pageContext.request.contextPath}/strategicTravelsClass/selectTreeTable',
                treeColIndex: 1,          // 树形图标显示在第几列
                treeSpid: 0,             // 最上级的父级id
                treeIdName: 'travelsClassId',       // 	id字段的名称
                treePidName: 'travelsClassParentid',    // 	pid字段的名称
                treeDefaultClose: true,     //是否默认折叠
                page: false,
                //treeLinkage: true,      //父级展开时是否自动展开所有子级
                cols: [[
                    {type: 'numbers'},
                    {title: "分类名称", field: "travelsClassName", style: "text-align:left"},
                    {title: "备注", field: "memo", align: "center"},
                    {title: "创建人", field: "createdName", align: "center"},
                    {
                        title: "创建时间",
                        field: "creationDate", align: "center",
                        templet: '<div>{{# if(d.creationDate!=null){ }} {{ layui.util.toDateString(d.creationDate,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'
                    },
                    {title: "更新人员", field: "lastUpdateName", align: "center"},
                    {
                        title: "更新时间",
                        field: "lastUpdateDate", align: "center",
                        templet: '<div>{{# if(d.lastUpdateDate!=null){ }} {{ layui.util.toDateString(d.lastUpdateDate,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'
                    },
                    {
                        title: "分类状态", width: 100, align: "center",
                        templet: function (d) {
                            if (d.status == 1) {
                                return '<button type="button" class="layui-btn  layui-btn-normal"  lay-event="yn">启用</button>';
                            } else {
                                return '<button type="button" class="layui-btn  layui-btn-danger"  lay-event="yn">禁用</button>';
                            }
                        }
                    },
                    {title: "操作", templet: "#updateAndDelete", align: "center"}
                ]],
                done: function () {
                    layer.closeAll('loading');
                }

            })
        };

        //监听行工具事件
        table.on('tool(Lay_category_treeTable)', function (obj) {
            var data = obj.data;
            if (obj.event === 'yn') {
                var str = obj.data.status == 0 ? "启用" : "禁用";
                var state = obj.data.status == 1 ? 0 : 1;
                layer.confirm('确认要[' + str + ']这个分类吗?', function (index) {
                    $.ajax({
                        url: '${pageContext.request.contextPath}/strategicTravelsClass/updateclassIdStatus',
                        data: {"travelsClassId": data.classId, "status": state},
                        type: 'post',
                        dataType: 'json',
                        success: function (data) {
                            renderTable();
                            layer.msg("操作" + data.msg, {time: 1000, icon: 6});
                        }, error: function (data) {
                            renderTable();
                            layer.msg("操作失败", {time: 1000, icon: 5});
                        }
                    });
                    layer.close(index);
                });
            } else if (obj.event === 'toOpenUpdateLayer') {//编辑
                layer.open({
                    title: "编辑分类信息",
                    content: "${pageContext.request.contextPath}/strategicTravelsClass/toStrategicTravelsClassEdit?travelsClassId=" + data.travelsClassId,
                    type: 2,
                    maxmin: true,
                    area: ['500px', '480px'],
                    end: function () {
                        window.location.reload();
                    }
                });
            } else if (obj.event === 'doDelete') {//删除
                //确认；如果点击确认删除；否则不删除
                layer.confirm('确定要删除吗？', {icon: 3, title: '确认删除'}, function (index) {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/goodscategory/deletecl",
                        data: "aid=" + classId,
                        success: function (data) {
                            layer.alert("删除" + data.msg, {time: 2000});
                            renderTable();
                            layer.close(index);
                        }
                    })
                });
            }
        });

        renderTable();
        //展开所有
        $('#btn-expand').click(function () {
            treetable.expandAll('#Lay_category_treeTable');
        });
        //折叠所有
        $('#btn-fold').click(function () {
            treetable.foldAll('#Lay_category_treeTable');
        });
        //刷新表格
        $('#btn-refresh').click(function () {
            renderTable();
        });


        $('#btn-search').click(function () {
            var keyword = $('#Lay_toSearch_input').val();
            //alert(keyword);
            // var searchName = $('#Lay_toSearch_input').val();
            var searchCount = 0;
            $('#Lay_category_treeTable').next('.treeTable').find('.layui-table-body tbody tr td').each(function () {
                $(this).css('background-color', 'transparent');
                var text = $(this).text();
                if (keyword != '' && text.indexOf(keyword) >= 0) {
                    $(this).css('background-color', 'rgba(250,230,160,0.5)');
                    if (searchCount == 0) {
                        treetable.expandAll('#Lay_category_treeTable');
                        $('html,body').stop(true);
                        $('html,body').animate({scrollTop: $(this).offset().top - 150}, 500);
                    }
                    searchCount++;
                }
            });
            if (keyword == '') {
                layer.msg("请输入搜索内容", {icon: 5});
            } else if (searchCount == 0) {
                layer.msg("没有匹配结果", {icon: 5});
            }
        });

        $("#toOpenAddLayer").click(function () {
            layer.open({
                title: "添加分类信息",
                content: "${pageContext.request.contextPath}/strategicTravelsClass/toStrategicTravelsClassEdit",
                type: 2,
                // maxmin: true,
                area: ['500px', '480px'],
                end: function () {
                    window.location.reload();
                }
            });
        });


    });



    function doMultiDelete() {
        //获取到选中的内容的id===》table模块中找方法
        layui.use(['layer', 'table'], function () {
            var table = layui.table;
            var layer = layui.layer;
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].id;
                    }
                    console.log("ids===" + ids);
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/prefixThird/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert("删除" + data.msg + "，请点击右上角刷新表格后生效！", {time: 2000});
                            //刷新table
                            // table.reload("Lay_back_table");
                            // renderTable();
                        }
                    })
                });
            }
        });
    }


</script>

</body>
</html>
