<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/23
  Time: 9:38
  To change this template use File | Settings | File Templates.
--%>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../head.jsp" %>
<html>
<head>
    <title>商品分类管理</title>
</head>
<body>


<%--弹出层--%>
<form id="addForm" class="layui-form">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0;">
        <div class="layui-form-item">
            <label class="layui-form-label">分类名称</label>
            <div class="layui-input-block">
                <input name="travelsClassName" id="travelsClassName" value="${tc.travelsClassName}" class="layui-input">
                <input name="travelsClassId" id="travelsClassId" value="${tc.travelsClassId}" lay-type="hide" type="hidden" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">备注</label>
            <div class="layui-input-block">
                <input name="memo" id="memo" value="${tc.memo}" class="layui-input">
            </div>
        </div>


        <div class="layui-form-item" id="Lay_select_Level">
            <label class="layui-form-label">请选择分类级别</label>
            <div class="layui-input-block">
                <select lay-verify="required" lay-filter="classIdLevel">
                    <option value=""></option>
                    <option value="1" selected>一级分类</option>
                    <option value="2">二级分类</option>
                </select>
            </div>
        </div>


        <div class="layui-form-item" style="display: none" id="Lay_One_Level">
            <label class="layui-form-label">请选择所属的一级分类</label>
            <div class="layui-input-block">
                <select name="travelsClassParentid" id="travelsClassParentid"  lay-filter="parent_classIdLevel_One">
                    <option value=""></option>
                </select>
            </div>
        </div>

        <div class="layui-form-item" style="width: 600px">
            <div class="layui-input-block" style="text-align: right">
                <button type="submit" class="layui-btn" id="btnSub" lay-filter="btnSub" lay-submit>立即提交</button>
            </div>
        </div>

    </div>
</form>

<script type="text/javascript">

    layui.use(['form', 'layer'], function () {
        var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        var layer = layui.layer;
        var $ = layui.jquery;

        if ($("#travelsClassId").val()!=null && $("#travelsClassId").val()!=""){
            $("#Lay_select_Level").hide();
        }

        form.on('select(classIdLevel)', function (data) {
            console.log("data.value = "+data.value); //得到被选中的值
            if(data.value == 1){
                $("#Lay_One_Level").hide();
            }
            if (data.value == 2) {
                $("#Lay_One_Level").show();
                $.ajax({
                    url: '${pageContext.request.contextPath}/strategicTravelsClass/selectTwoLevel',
                    dataType: 'json',
                    type: 'post',
                    success: function (data) {
                        $.each(data, function (index, item) {
                            //console.log("000 " + index);
                            //console.log("111 " + item);
                            $('#parentId').append(new Option(item.travelsClassName, item.travelsClassId));//往下拉菜单里添加元素
                        });
                        form.render();//菜单渲染 把内容加载进去
                    }
                })
            }

        });


        //监听提交
        form.on('submit(btnSub)', function (data) {
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            var formData = new FormData(document.getElementById("addForm"));
            console.log(data.field);
            $.ajax({
                url: '${pageContext.request.contextPath}/api/wechatRegionCode/saveOrUpdate',
                data: formData,
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    layer.msg('操作成功', {
                        offset: '15px',
                        icon: 6,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                },
                error: function (data) {
                    layer.msg('操作失败', {
                        offset: '15px',
                        icon: 5,
                        time: 3000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                }
            });
            return false;
        });
    });




    //执行添加
    function toOpenAddLayer() {
        layui.use(["form", "layer", "table"], function () {
            var form = layui.form;
            var layer = layui.layer;
            var table = layui.table;

            layer.open({
                title: "添加配置",
                content: $("#layuiconfig-form-role"),
                type: 1,
                maxmin: true,
                area: ['500px', '480px'],
                end: function () {
                    window.location.reload();
                }
            });

            $("#updateSubmitBtn").hide();
            $("#LAY-sysconfig-submit").show();

            form.on('select(classIdLevel)', function (data) {
                //console.log(data.elem); //得到select原始DOM对象
                console.log("data.value = "+data.value); //得到被选中的值
                //console.log(data.othis); //得到美化后的DOM对象
                if(data.value == 1){
                    // alert(1);
                    $("#Lay_One_Level").hide();
                    $("#Lay_Two_Level").hide();
                }
                if (data.value == 2) {
                    // alert(2);
                    $("#Lay_One_Level").show();
                    $("#Lay_Two_Level").hide();
                    $.ajax({
                        url: '${basepath}goodscategory/selectOneLevel',
                        dataType: 'json',
                        type: 'post',
                        success: function (data) {
                            $.each(data, function (index, item) {
                                //console.log("000 " + index);
                                console.log("111 " + item);
                                $('#parentId').append(new Option(item.className, item.classId));//往下拉菜单里添加元素
                            });
                            form.render();//菜单渲染 把内容加载进去
                        }
                    })
                }
                if (data.value == 3) {
                    // alert(3);
                    $("#Lay_One_Level").hide();
                    // form.on('select(parent_classIdLevel_One)', function (data2){
                    $("#Lay_Two_Level").show();
                    $.ajax({
                        url: '${basepath}goodscategory/selectTwoLevel',
                        dataType: 'json',
                        type: 'post',
                        success: function (data) {
                            $.each(data, function (index, item) {
                                //console.log("000 " + index);
                                //console.log("111 " + item);
                                $('#Two_parentId').append(new Option(item.className, item.classId));//往下拉菜单里添加元素
                            });
                            form.render();//菜单渲染 把内容加载进去
                        }
                    })
                    // })
                }
            });

            //当点击提交按钮的时候，会进入到这个函数
            form.on("submit(LAY-sysconfig-submit)", function (data) {
                console.log(data.field);
                $.ajax({
                    url: "${pageContext.request.contextPath}/goodscategory/addGoodsCategory",
                    data: data.field,
                    type: "post",
                    success: function (data) {
                        //1.关闭掉添加弹出层
                        layer.closeAll('page');
                        //2.提示添加成功
                        layer.alert("添加" + data.msg, {time: 3000});
                        //3.刷新table
                        table.reload("Lay_back_table");

                    }
                });
                return false;//阻止跳转；
            })
        })
    }



</script>

</body>
</html>
