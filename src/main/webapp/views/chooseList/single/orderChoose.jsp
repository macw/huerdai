<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>选择订单</title>
	<%@ include file="../../head.jsp" %>
	 <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>
</head>
<body>
<div class="layui-fluid">
    <div class="layui-card-body layui-form">
        <div style="padding-bottom: 10px;">
            <div class="layui-input-inline" style="width: 200px;">
	             <input type="text" name="orderId" id="orderId" placeholder="订单号" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="shippingUser" id="shippingUser" placeholder="收件人" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>
        <table id="tab" lay-filter="tab"></table>
    </div>
</div>
</body>


<script type="text/javascript">
		var active=null;
		
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            table.render({
                elem: '#tab',
                url: '<%=basePath%>choose/order/selectData', //数据接口
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                where : {
                	orderType : '${dto.orderType}'
                },
                cols: [[
                    {type: "checkbox"},
                    {title: "订单号", field: "orderId",align:"center"},
                    {title: "收件人", field: "createdName",align:"center"},
                    {title: "支付方式", field: "paymentMethod",align:"center"},
                    {title: "支付金额", field: "paymentMethod",align:"center"},
                    {title: "联系方式", field: "paymentMethod",align:"center"},
                    {title: "创建时间",align:"center",
                    	templet: function(d){
                    		if(d.creationDate!=null){
	                    		return layui.util.toDateString(d.creationDate);
                    		}
                    		return '';
                    	},width :160	
                    },
                    {title: "最后更新人", field: "lastUpdateName",align:"center"},
                    {title: "最后更新时间",align:"center",
                    	templet: function(d){
                    		if(d.lastUpdateDate!=null){
	                    		return layui.util.toDateString(d.lastUpdateDate);
                    		}
                    		return '';
                    	},width :160	
                    }
                ]],done:function(res, curr){
        	    	  var brforeCurr = curr; // 获得当前页码
        	    	  var dataLength = res.data.length; // 获得当前页的记录数
        	    	  var count = res.count; // 获得总记录数
        	    	  if(dataLength == 0 && count != 0){ //如果当前页的记录数为0并且总记录数不为0
        	    		  table.reload("tab",{ // 刷新表格到上一页
        	    			  page:{
        	    				 curr:brforeCurr-1
        	    			  }
        	    		  });
        	    	  }
        	      }
            });
            
          	 
            active={
        	    doSearch:function(){
             		table.reload('tab', {
                      where: {"orderId": $('#orderId').val(),"shippingUser": $('#shippingUser').val()},
                      page: { curr: 1 }
                    });
             	}
              };
        });
        //搜索操作
        function doSearch() {
        	active.doSearch();
        };
         
        function add(){
        	active.add();
        };
        
        
        
    </script>
</html>

