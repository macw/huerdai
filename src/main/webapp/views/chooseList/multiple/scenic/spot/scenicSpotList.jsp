<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>风景地点</title>
	<%@ include file="../../../../head.jsp" %>
	 <style type="text/css">
		.layui-table-cell {
			font-size:14px;
		    padding:0 5px;
		    height:auto;
		    overflow:visible;
		    text-overflow:inherit;
		    white-space:normal;
		    word-break: break-all;
		    }
    </style>
</head>
<body>
<div class="layui-fluid">
    <div class="layui-card-body layui-form">
        <div style="padding-bottom: 10px;">
            <div class="layui-input-inline" style="width: 200px;">
				<select name="tId" id="tId" lay-search>
					<option value="">选择旅游公司</option>
					<c:forEach items="${tourOperatorList}" var="t">
						<option value="${t.tId }">${t.tName }</option>
					</c:forEach>
				</select>
			</div>
			<div class="layui-input-inline" style="width: 160px;">
		        <select name="province" id="province" lay-filter="province" lay-search >
					<option value="">省</option>
					<c:forEach items="${provinceList}" var="p">
						<option value="${p.id}" <c:if test="${dto.province == p.id}">selected</c:if> >${p.name}</option>
					</c:forEach>
				</select>
		     </div>
   			 <div class="layui-input-inline" style="width: 160px;">
		        <select name="city" id="city"  lay-filter="city" lay-search >
					<option value="">市</option>
			        <c:forEach items="${cityList}" var="c">
							<option value="${c.id}" <c:if test="${dto.city == c.id}">selected</c:if> >${c.name}</option>
					</c:forEach>
				</select>
		      </div>
   			 <div class="layui-input-inline" style="width: 160px;">
		        <select name="district" id="district" lay-search >
					<option value="">区</option>
					 <c:forEach items="${districtList}" var="d">
							<option value="${d.id}" <c:if test="${dto.district == d.id}">selected</c:if> >${d.name}</option>
					</c:forEach>
				</select>
		     </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="spotName" id="spotName" placeholder="景点名称/地址" autocomplete="off" class="layui-input">
            </div>
          <!--   <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="createdName" id="createdName" placeholder="创建人/修改人" autocomplete="off" class="layui-input">
            </div> -->
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>
        <table id="tab" lay-filter="tab"></table>
        <button type="submit" class="layui-btn" lay-submit="" lay-filter="btnSub" id="subBtn">立即提交</button>
    </div>
</div>
</body>

<script type="text/javascript">

		var active=null;
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            table.render({
                elem: '#tab',
                url: '<%=basePath%>scenic/spot/selectData',  //数据接口
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                where : {
                	status : 1
                },
                cols: [[
                    {type: "checkbox"},
                    {title: "所属旅游公司", field: "tName",align:"center",width :130},
                    {title: "景点名称", field: "spotName",align:"center",width :200},
                    {title: "景点图片", align:"center",width :150,
                    	templet: function(d){
                    		if(d.pictureurl!=null){
                    			return '<img src='+d.pictureurl+'></img>';
                    		}
                    		return '';
                    	}	
                    }
                    ,{title: "位置", field: "fulladdress",align:"center"}
                    ,{title: "优先级", field: "leavel",align:"center",width :80}
                    ,{title: "价格", field: "price",align:"center",width :200,
                    	templet: function(d){
                    		var str='';
                    		if(d.price!=null){
                    			str='门票价格 : '+d.price+'<br/>';
                    		}
                    		if(d.activityPrice!=null){
                    			str+='活动价格 : '+d.activityPrice+'<br/>';
                    		}
                    		if(d.exitPrice!=null){
                    			str+='出厂价格 : '+d.exitPrice+'<br/>';
                    		}
                    		if(d.preferentialQuota!=null){
                    			str+='利       润 : '+d.preferentialQuota+'<br/>';
                    		}
                    		return str;
                    	}	
                    }
                   ,{title: "利润",align:"center",width :200,
                    	templet: function(d){
                    		var str='';
                    		if(d.primaryProfit!=null){
                    			str ='一级利润 : 返点'+d.primaryProfit+'%<br/>';
                    		}
                    		if(d.secondaryProfit!=null){
                    			str+='二级利润 : 返点'+d.secondaryProfit+'%';
                    		}
                    		return str;
                    	}	
                    	
                    }
                    ,{title: "备注", field: "memo",align:"center",width :150}
                ]],done:function(res, curr){
                	  var data = res.data;
        	    	  var brforeCurr = curr; // 获得当前页码
       	    		  var array = getCookieSpotIds();
        	    	  var dataLength = res.data.length; // 获得当前页的记录数
        	    	  if(array.length>0){ //内容相当于判断spotIds=""、spotIds=null、spotIds = undefined、spotIds=0
        	    		  $(data).each(function(i,item){
        	    			   $(array).each(function(j,arr){
        	    				    if(parseInt(arr)==item.spotId){
        	    				    	 //找到对应数据改变勾选样式，呈现出选中效果
        	    				    	var index = item.LAY_TABLE_INDEX;
				                        $('tr[data-index=' + index + '] input[type="checkbox"]').prop('checked', true);
				                        $('tr[data-index=' + index + '] input[type="checkbox"]').next().addClass('layui-form-checked');
        	    				    }
        	    			   })
        	    		  })
        	    	  }
        	    	  var count = res.count; // 获得总记录数
        	    	  if(dataLength == 0 && count != 0){ //如果当前页的记录数为0并且总记录数不为0
        	    		  table.reload("tab",{ // 刷新表格到上一页
        	    			  page:{
        	    				 curr:brforeCurr-1
        	    			  }
        	    		  });
        	    	  }
        	      }
            });
            
            table.on('checkbox(tab)', function(obj){
            	 var isChecked=obj.checked; //当前是否选中状态
            	 var array = getCookieSpotIds();
            	 if(isChecked){
            		 array.push(obj.data.spotId);
            	 }else{
            		 removeArrayBySpotId(array,obj.data.spotId);
            	 }
            	 var str = arrayToCookieString(array);
            	 $.cookie("spotIds",str,{expires:1,path:'/'});
            });
          //监听提交
            form.on('submit(btnSub)', function(data){
            	var spotIds = getCookieSpotIds();
           		if (spotIds.length <= 0) {
					layer.msg('请选择一个景点');
				} else {
					parent.setData(spotIds);
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
				}
            });
            form.on('select(province)', function(data){
           		$("#district").empty();
           		$("#city").empty();
        		var str="<option value=''>区</option>";
           		$("#district").append(str);
        		str="<option value=''>市</option>";
        		if(data.value.length <= 0){
        			$("#city").append(str);
        			form.render('select');
        		}else{
	           		$.ajax({
		           		 url:'<%=basePath%>region/ajaxRegionList?pid='+data.value,
		              	 type: 'post',
		              	 dataType: 'json',
	           			 success:function(d){
	           				$("#city").empty();
	           				if(d.data.length>0){
		           				$(d.data).each(function(i,t){
		           					str+="<option value='"+t.id+"'>"+t.name+"</option>"
		           				})
	           				}
	           				$("#city").append(str);
			           		form.render('select');
	           			 }
	           		});
        		}
           	});
           	form.on('select(city)', function(data){
           		var str="<option value=''>区</option>";
           		$.ajax({
	           		 url:'<%=basePath%>region/ajaxRegionList?pid='+data.value,
	              	 type: 'post',
	              	 dataType: 'json',
          			 success:function(d){
          				$("#district").empty();
          				if(d.data.length>0){
	           				$(d.data).each(function(i,t){
	           					str+="<option value='"+t.id+"'>"+t.name+"</option>"
	           				})
          				}
          				$("#district").append(str);
          				form.render('select');
          			 }
          		});
           	});
            
            active = {
   				doSearch: function(){
   					table.reload('tab', {
   	                    where: {"tId": $('#tId').val(),"spotName": $('#spotName').val(),"createdName": $('#createdName').val(),"province": $('#province').val(),"city": $('#city').val(),"district": $('#district').val()},
   	                    page: {
   	                        curr: 1 //重新从第 1 页开始
   	                      }
   	                });
   				},
   				tableReload:function(){
	   				 table.reload('tab', {});
   				}
           };
            
           
        });
        //搜索操作
        function doSearch() {
        	active.doSearch();
        };
        
        function getCookieSpotIds(){
	        var spotIds = $.cookie('spotIds');
	        var array = [];
		  	if(!spotIds){ //内容相当于判断spotIds=""、spotIds=null、spotIds = undefined、spotIds=0
		  	}else{
		  	  array = spotIds.substring(0,spotIds.length-1).split("-");
		  	}
		  	return array;
        }
        
        function removeArrayBySpotId(array,spodId){
        	if(array.length>0){
        		array.splice(jQuery.inArray(spodId),1); 
        	};
        	return array;
        };
        
        function arrayToCookieString(array){
        	var str="";
        	$(array).each(function(i,item){
        		str+=item+"-";
        	})
        	return str;
        }
	        
    </script>
</html>

