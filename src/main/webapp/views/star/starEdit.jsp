<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/12/9
  Time: 17:02
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../head.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>达人/大咖管理</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
</head>
<body>

<form id="addForm" class="layui-form" enctype="multipart/form-data">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0;">


        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">达人姓名</label>
                <div class="layui-input-block">
                    <input name="starName" value="${st.starName}" id="starName"
                           class="layui-input">
                    <input name="starId" value="${st.starId}" id="starId" type="hidden"
                           class="layui-input">
                </div>
            </div>

        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 600px">
                <div class="layui-form-item">
                    <label class="layui-form-label">个性签名</label>
                    <div class="layui-input-block">
                        <input name="starSign" value="${st.starSign}" id="starSign"
                               class="layui-input">
                    </div>
                </div>
            </div>
        </div>



        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">达人头图</label>
                <div class="layui-input-block">
                    <button type="button" class="layui-btn" id="upload_customer_pic_starTitleUrl">
                        <i class="layui-icon">&#xe67c;</i>选择图片
                    </button>

                </div>
            </div>
            <div class="layui-inline">
                <img class="layui-upload-starTitleUrl" name="starTitleUrl" src="${st.starTitleUrl}" id="starTitleUrl" style="max-width: 200px;max-height: 200px; padding-left: 40px;">
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">达人头像</label>
                <div class="layui-input-block">
                    <button type="button" class="layui-btn" id="upload_customer_pic_starImageUrl">
                        <i class="layui-icon">&#xe67c;</i>选择图片
                    </button>
                </div>
            </div>
            <div class="layui-inline">
                <img class="layui-upload-starImageUrl" name="starImageUrl" src="${st.starImageUrl}" id="starImageUrl" style="max-width: 200px;max-height: 200px; padding-left: 40px;">
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">视频名称</label>
                <div class="layui-input-block">
                    <input name="starVideoTitle" value="${st.starVideoTitle}" id="starVideoTitle"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">排序号</label>
                <div class="layui-input-block">
                    <input name="starSort" value="${st.starSort}" id="starSort"
                           class="layui-input">
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">视频预览图</label>
                <div class="layui-input-block">
                    <button type="button" class="layui-btn" id="upload_customer_pic_starVideoIcon">
                        <i class="layui-icon">&#xe67c;</i>选择图片
                    </button>
                </div>
            </div>
            <div class="layui-inline">
                <img class="layui-upload-starVideoIcon" name="starVideoIcon" src="${st.starVideoIcon}" id="starVideoIcon" style="max-width: 200px;max-height: 200px; padding-left: 40px;">
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">视频</label>
                <div class="layui-input-block">
                    <button type="button" class="layui-btn" id="upload_customer_pic_starVideoUrl">
                        <i class="layui-icon">&#xe6ed;</i>选择视频
                    </button>
                </div>
            </div>
            <div class="layui-inline">
                <video class="layui-upload-starVideoUrl" name="starVideoUrl" src="${st.starVideoUrl}" id="starVideoUrl" style="max-width: 200px;max-height: 200px; padding-left: 40px;">
                </video>
            </div>
        </div>

        <div class="layui-form-item" style="width: 600px">
            <div class="layui-input-block" style="text-align: right">
                <button type="submit" class="layui-btn" id="btnSub" lay-filter="btnSub" lay-submit>立即提交</button>
            </div>
        </div>
    </div>
</form>


<script type="text/javascript">
    layui.use(['form', 'upload', 'layer'], function () {
        var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        var layer = layui.layer;
        var $ = layui.jquery;
        var upload = layui.upload;

        //达人头图
        upload.render({
            elem: '#upload_customer_pic_starTitleUrl', //绑定元素
            accept: "images",
            field:"starTitle",
            acceptMime: 'image/*',//打开文件选择框时,只显示图片文件
            auto: false,  //是否选完文件后自动上传。默认值：true
            choose : function(obj) {
                obj.preview(function(index, file, result) {
                    $('#starTitleUrl').attr('src', result);

                })
            }
        });

        //达人头像
        upload.render({
            elem: '#upload_customer_pic_starImageUrl', //绑定元素
            accept: "images",
            field:"starImage",
            acceptMime: 'image/*',//打开文件选择框时,只显示图片文件
            auto: false,  //是否选完文件后自动上传。默认值：true
            // bindAction: '#btnSub',
            choose : function(obj) {
                obj.preview(function(index, file, result) {
                    $('#starImageUrl').attr('src', result);

                })
            }
        });

        //视频预览图
        upload.render({
            elem: '#upload_customer_pic_starVideoIcon', //绑定元素
            accept: "images",
            field:"starVo",
            acceptMime: 'image/*',//打开文件选择框时,只显示图片文件
            auto: false,  //是否选完文件后自动上传。默认值：true
            choose : function(obj) {
                obj.preview(function(index, file, result) {
                    $('#starVideoIcon').attr('src', result);

                })
            }
        });

        //视频路径地址
        upload.render({
            elem: '#upload_customer_pic_starVideoUrl', //绑定元素
            accept: 'video',
            field:"starVideo",
            auto: false,  //是否选完文件后自动上传。默认值：true
            acceptMime: 'video/*',//打开文件选择框时,只显示图片文件
            choose : function(obj) {
                obj.preview(function(index, file, result) {
                    $('#starVideoUrl').attr('src', result);
                })
            }
        });



        //监听提交
        form.on('submit(btnSub)', function (data) {
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            var formData = new FormData(document.getElementById("addForm"));
            console.log(data.field);
            $.ajax({
                url: '${pageContext.request.contextPath}/star/addOrUpdate',
                data: formData,
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data.code==200){
                        layer.msg('操作成功', {
                            icon: 6,
                            time: 1000
                        }, function () {
                            parent.layer.close(index);
                            parent.layui.table.reload('Lay_back_table', {});
                        });
                    }else {
                        layer.msg('上传失败', {
                            icon: 5,
                            time: 1000
                        })
                    }

                },
                error: function (data) {
                    layer.msg('操作失败', {
                        icon: 5,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                }
            });
            return false;
        });
    });

</script>
</body>
</html>

