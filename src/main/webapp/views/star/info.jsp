<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/12/10
  Time: 9:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>达人简介</title>
    <%@ include file="../head.jsp" %>
    <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }
    </style>
</head>
<body>
<div class="layui-fluid">
    <div class="layui-elem-quote" style="margin-bottom: 0px;background: #fff">
        <p>达人简介</p>
    </div>
    <div class="layui-card">
        <div class="layui-form layui-row-form" style="padding: 10px;">
            <input type="hidden" name="starId" value="${sta.starId }">
            <textarea id="demo" style="display: none;">${sta.starInfo}</textarea>
            <div class="layui-input-block" style="text-align: center; margin: 0; padding: 10px;">
                <button class="layui-btn layui-btn-normal" lay-submit lay-filter="form">保存</button>
                <button class="layui-btn layui-btn-primary" type="button" onclick="backPage()">取消</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    layui.use( ['form', 'layedit'], function() {
        var layedit = layui.layedit;
        var form = layui.form;
        var layer = layui.layer;
        layedit.set({
            uploadImage : {
                url : '<%=basePath%>saveStarImg'
            }
        });
        var edit = layedit.build('demo',{
            height: 650 //设置编辑器高度
        }); //建立编辑器

        form.on('submit(form)', function(data) {
            var starclassInfo = layedit.getContent(edit);
            var formData = {};
            formData.starId = data.field.starId;
            formData.starInfo = starclassInfo;
            $.ajax({
                url:'<%=basePath%>star/addOrUpdate',
                data:formData,
                type: 'post',
                dataType: 'json',
                success:function(data){
                    if(data.code==200){
                        layer.msg("操作成功", { offset: '15px',icon: 6,time: 1000}, function(){
                            window.history.back(-1);
                        });
                    }else{
                        layer.msg("操作失败", { offset: '15px',icon: 5,time: 1000}, function(){
                            window.history.back(-1);
                        });
                    }
                }
            });
            return false;
        });
    });
    function backPage() {
        window.history.go(-1);
    }
</script>
</body>

</html>


