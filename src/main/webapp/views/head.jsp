<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="basePath.jsp" %>
<meta charset="utf-8">
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="<%=basePath%>layuiadmin/layui/css/layui.css" media="all">
<link rel="stylesheet" href="<%=basePath%>layuiadmin/style/admin.css" media="all">
<script type="text/javascript" src="<%=basePath %>js/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="<%=basePath %>js/jquery.cookie.js"></script>
<script src="<%=basePath%>layuiadmin/layui/layui.js"></script>
<style type="text/css">
    .layui-table-cell {
        height: auto;
        line-height: 30px;
    }

</style>