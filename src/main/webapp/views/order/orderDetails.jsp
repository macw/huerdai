<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/8
  Time: 17:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../head.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>订单详情页面</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
</head>
<body>


<div class="layui-card-body">
    <div class="layui-form">
        <div style="padding-bottom: 10px;" id="LAY_lay_add">
            <button type="button" class="layui-btn layui-btn-danger" onclick="doMultiDelete()">
                <i class="layui-icon layui-icon-delete"></i> 批量删除
            </button>



            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="customerName" id="Lay_toSearch_CustomerName" placeholder="请输入商品名称"
                       autocomplete="off"
                       class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" lay-filter="search" lay-submit>
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>

    </div>
    <table id="Lay_back_table" lay-filter="Lay_back_table"></table>

</div>

<script type="text/html" id="updateAndDelete">

    <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">
        <i class="layui-icon layui-icon-edit"></i>修改
    </button>
    <button type="button" class="layui-btn layui-btn-danger" onclick="dodel({{d.userId}})">
        <i class="layui-icon layui-icon-delete"></i>删除
    </button>

</script>

<script type="text/javascript">
    layui.use(['table', "layer", 'form'], function () {
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var $ = layui.jquery;

        var layTab = table.render({
            elem: '#Lay_back_table',
            url: '${pageContext.request.contextPath}/orderdetail/selectAll?orderId='+${orderId}, //数据接口
            page: true,
            limit: 10,
            limits: [10, 20, 30],
            width: 'auto',
            autoSort: true,

            //toolbar: "#LAY_lay_add",
            cols: [[
                {type: "checkbox"},
                {title: "商品名称", field: "goodsName", align: "center", width: "100"},
                {title: "商品编码", field: "goodsCode", align: "center", width: "100"},
                {title: "购买商品数量", field: "productCnt", align: "center", width: "100"},
                {title: "购买商品单价", field: "productPrice", align: "center", width: "100"},
                {title: "平均成本价格", field: "averageCost", align: "center", width: "100"},
                {title: "含税金额", field: "saleamount", align: "center", width: "100"},
                {title: "不含税金额", field: "notaxamount", align: "center", width: "100"},
                {title: "优惠分摊金额", field: "feeMoney", align: "center", width: "100"},
                {title: "备注", field: "memo", align: "center", width: "100"},
                {title: "创建人", field: "createUser", align: "center", width: "100"},
                {title: "备注", field: "memo", align: "center", width: "100"},


             /*   {
                    title: "查看详情", align: "center", width: "180", templet: function (d) {
                        return '<button type="button" class="layui-btn layui-btn-warm"  lay-event="details"> <i class="layui-icon">&#xe653;</i>查看订单详情</button>';
                    }
                },*/
                {title: "创建人", field: "createUser", align: "center", width: "100"},
                {
                    title: "创建时间",
                    field: "createTime", align: "center", width: "200",
                    templet: '<div>{{# if(d.createTime!=null){ }} {{ layui.util.toDateString(d.createTime,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'
                },
                {title: "更新人员", field: "updateUser", align: "center", width: "100"},
                {
                    title: "更新时间",
                    field: "updateTime", align: "center", width: "200",
                    templet: '<div>{{# if(d.updateTime!=null){ }} {{ layui.util.toDateString(d.updateTime,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'
                },
                {title: "操作", templet: "#updateAndDelete", align: "center", width: "220"}
            ]]
        });

        //刷新表格方法
        function tableReload() {
            table.reload('Lay_back_table', {});
        };


        //监听行工具事件
        table.on('tool(Lay_back_table)', function (obj) {
            var data = obj.data;
            if (obj.event === 'edit') {
                layer.open({
                    title: "修改配置",
                    content: "${basepath}user/toEditUser?userId=" + data.userId,
                    type: 2,
                    maxmin: true,
                    area: ['500px', '480px'],
                    end: function () {
                        window.location.reload();
                    }
                });
            } else if (obj.event === "to_big_pic") {
                var url = data.pictureurl;
                var path = "${pageContext.request.contextPath}" + url;
                layer.open({
                    type: 1,
                    title: "查看大图",
                    skin: 'layui-layer-rim', //加上边框
                    area: ['500', '480'], //宽高
                    // shadeClose: true, //开启遮罩关闭
                    end: function (index, layero) {
                        return false;
                    },
                    content: '<div style="text-align:center"><img src="' + path + '"/></div>'
                });
            }

        });

        //监听搜索
        form.on('submit(search)', function (data) {
            var field = data.field;
            // console.log(field);
            //执行重载
            layTab.reload({
                where: field
            });
        });
        //打开添加窗口
        $("#add").click(function () {
            layer.open({
                title: "添加评论",
                // content: $("#layuiconfig-form-role"),
                content: "${basepath}user/toEditUser",
                type: 2,
                // maxmin: true,
                area: ['500px', '480px'],
                end: function () {
                    window.location.reload();
                }

            });
        });

    });


    function dodel(userId) {
        layui.use(['layer', 'table'], function () {
            var layer = layui.layer;
            var table = layui.table;
            var $ = layui.jquery;
            layer.confirm('确定要删除吗？', {icon: 3, title: '确认删除'}, function (index) {
                $.ajax({
                    url: "${pageContext.request.contextPath}/user/deleteOne",
                    data: "aid=" + userId,
                    success: function (data) {
                        layer.alert("删除" + data.msg, {time: 2000});
                        table.reload("Lay_back_table");
                    },
                    error: function (data) {
                        table.reload("Lay_back_table");
                        layer.msg("操作失败", {time: 1000, icon: 5});
                    }
                });
                layer.close(index);
            });
        });
    }

    function doMultiDelete() {
        //获取到选中的内容的id===》table模块中找方法
        layui.use(['layer', 'table'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var $ = layui.jquery;
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].userId;
                    }
                    console.log("ids===" + ids);
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/user/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert("删除" + data.msg, {time: 2000});
                            //刷新table
                            table.reload("Lay_back_table");
                        }
                    })
                });
            }
        });
    }

</script>
</body>
</html>
