<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/8
  Time: 11:21
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../head.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>订单管理</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>
<body>

<div class="layui-card-body">
    <div class="layui-form">
        <div style="padding-bottom: 10px;" id="LAY_lay_add">
            <%--<button type="button" class="layui-btn layui-btn-danger" onclick="doMultiDelete()">
                <i class="layui-icon layui-icon-delete"></i> 批量删除
            </button>--%>
           <%-- <button class="layui-btn layuiadmin-btn-role " data-type="add" id="add">
                <i class="layui-icon layui-icon-add-circle-fine"></i> 添加订单
            </button>--%>
            &nbsp;

            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="orderSn" id="Lay_toSearch_OrderSn" placeholder="请输入订单编号" autocomplete="off"
                       class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <select name="paymentMethod" lay-verify="" lay-search>
                    <option value="">请选择支付方式</option>
                    <<%--option value="1">现金</option>--%>
                    <option value="2">余额</option>
                    <option value="3">网银</option>
                    <option value="4">支付宝</option>
                    <option value="5">微信</option>
                </select>
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <select name="orderStatus" lay-verify="" lay-search>
                    <option value="">请选择订单状态</option>
                    <option value="1">待付款</option>
                    <option value="2">待发货</option>
                    <option value="21">待使用</option>
                    <option value="3">待收货</option>
                    <option value="4">订单完成，待评价</option>
                    <option value="5">退款中</option>
                    <option value="6">退款完成</option>
                </select>
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="customerName" id="Lay_toSearch_CustomerName" placeholder="请输入下单人名称"
                       autocomplete="off"
                       class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" lay-filter="search" lay-submit>
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>

    </div>
    <table id="Lay_back_table" lay-filter="Lay_back_table"></table>

</div>

<script type="text/html" id="updateAndDelete">

    <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">
        <i class="layui-icon layui-icon-edit"></i>修改
    </button>
    <button type="button" class="layui-btn layui-btn-danger" onclick="dodel({{d.userId}})">
        <i class="layui-icon layui-icon-delete"></i>删除
    </button>

</script>

<script type="text/javascript">
    layui.use(['table', "layer", 'form'], function () {
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var $ = layui.jquery;

        var layTab = table.render({
            elem: '#Lay_back_table',
            url: '${pageContext.request.contextPath}/order/selectAll', //数据接口
            page: true,
            limit: 10,
            limits: [10, 20, 30],
            width: 'auto',
            autoSort: true,
            initSort: {
                field: 'orderTime' //排序字段，对应 cols 设定的各字段名
                , type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
            },
            //toolbar: "#LAY_lay_add",
            cols: [[
                {type: "checkbox"},
                {title: "订单编号", field: "orderSn", align: "center", width: "280"},
                {title: "下单人名称", field: "customerName", align: "center", width: "200"},
                {title: "收货人姓名", field: "shippingUser", align: "center", width: "100"},
                {
                    title: "支付方式", field: "paymentMethod", align: "center", width: "100",
                    templet: function (d) {
                        if (d.paymentMethod == 1) {
                            return '<div class="layui-bg-gray">现金</div>';
                        } else if (d.paymentMethod == 2) {
                            return '<div class="layui-bg-red">余额</div>';
                        } else if (d.paymentMethod == 3) {
                            return '<div class="layui-bg-orange">网银</div>';
                        } else if (d.paymentMethod == 4) {
                            return '<div class="layui-bg-blue">支付宝</div>';
                        } else if (d.paymentMethod == 5) {
                            return '<div class="layui-bg-green">微信</div>';
                        }else {
                            return "";
                        }
                    }
                },
                {title: "订单总金额", field: "orderMoney", align: "center", width: "100"},
                {title: "优惠金额", field: "districtMoney", align: "center", width: "100"},
                {title: "支付金额", field: "paymentMoney", align: "center", width: "100"},
                {title: "订单积分", field: "orderPoint", align: "center", width: "100"},
                {title: "发票抬头", field: "invoiceTime", align: "center", width: "100"},
                {title: "备注", field: "memo", align: "center", width: "100"},
                {
                    title: "订单状态", width: 130, align: "center",
                    templet: function (d) {
                        if (d.orderStatus == 1) {
                            return '<button type="button" class="layui-btn  layui-btn-normal"  lay-event="yn">待付款</button>';
                        } else if (d.orderStatus == 2) {
                            return '<button type="button" class="layui-btn  "  lay-event="yn">待发货</button>';
                        } else if (d.orderStatus == 21) {
                            return '<button type="button" class="layui-btn "  lay-event="yn">待使用</button>';
                        }else if (d.orderStatus == 3) {
                            return '<button type="button" class="layui-btn layui-btn-warm"  lay-event="yn">待收货</button>';
                        }else if (d.orderStatus == 4) {
                            return '<button type="button" class="layui-btn "  lay-event="yn">待评价</button>';
                        }else if (d.orderStatus == 5) {
                            return '<button type="button" class="layui-btn layui-btn-danger"  lay-event="yn">退款中</button>';
                        }else if (d.orderStatus == 6) {
                            return '<button type="button" class="layui-btn "  lay-event="yn">退款完成</button>';
                        }else{
                            return "";
                        }
                    }
                },
                {
                    title: "查看详情", align: "center", width: "180", templet: function (d) {
                        return '<button type="button" class="layui-btn layui-btn-warm"  lay-event="details"> <i class="layui-icon">&#xe653;</i>查看订单详情</button>';
                    }
                },
                {title: "创建人", field: "createUser", align: "center", width: "100"},
                {
                    title: "创建时间",
                    field: "createTime", align: "center", width: "200",
                    templet: '<div>{{# if(d.createTime!=null){ }} {{ layui.util.toDateString(d.createTime,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'
                },
                {title: "更新人员", field: "updateUser", align: "center", width: "100"},
                {
                    title: "更新时间",
                    field: "updateTime", align: "center", width: "200",
                    templet: '<div>{{# if(d.updateTime!=null){ }} {{ layui.util.toDateString(d.updateTime,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'
                },
                {
                    title: "下单时间",
                    field: "orderTime", align: "center", width: "200",
                    templet: '<div>{{# if(d.orderTime!=null){ }} {{ layui.util.toDateString(d.orderTime,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'
                },
                {
                    title: "支付时间",
                    field: "payTime", align: "center", width: "200",
                    templet: '<div>{{# if(d.payTime!=null){ }} {{ layui.util.toDateString(d.payTime,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'
                }/*,
                {title: "操作", templet: "#updateAndDelete", align: "center", width: "350"}*/
            ]]
        });

        //刷新表格方法
        function tableReload() {
            table.reload('Lay_back_table', {});
        };


        //监听行工具事件
        table.on('tool(Lay_back_table)', function (obj) {
            var data = obj.data;
             if (obj.event === 'edit') {
                layer.open({
                    title: "修改配置",
                    content: "${basepath}user/toEditUser?userId=" + data.userId,
                    type: 2,
                    maxmin: true,
                    area: ['500px', '480px'],
                    end: function () {
                        window.location.reload();
                    }
                });
            } else if (obj.event === "to_big_pic") {
                var url = data.pictureurl;
                var path = "${pageContext.request.contextPath}" + url;
                layer.open({
                    type: 1,
                    title: "查看大图",
                    skin: 'layui-layer-rim', //加上边框
                    area: ['500', '480'], //宽高
                    // shadeClose: true, //开启遮罩关闭
                    end: function (index, layero) {
                        return false;
                    },
                    content: '<div style="text-align:center"><img src="' + path + '"/></div>'
                });
            }else if (obj.event === "details"){
                 // alert(data.orderId);
                 layer.open({
                     title:"查看订单详情",
                     type:2,
                     content:"${pageContext.request.contextPath}/orderdetail/toorderdetail?orderId="+data.orderId,
                     maxmin: true,
                     area: ['1500px', '600px'],
                     success: function(layero, index){
                         var body = layer.getChildFrame('body', index);
                         var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                         // console.log(body.html()); //得到iframe页的body内容
                         // body.find('#goods_id').val(ids);
                     },
                     end: function () {
                         window.location.reload();
                     }
                 })
             }

        });

        //监听搜索
        form.on('submit(search)', function (data) {
            var field = data.field;
            // console.log(field);
            //执行重载
            layTab.reload({
                where: field
            });
        });
        //打开添加窗口
        $("#add").click(function () {
            layer.open({
                title: "添加评论",
                // content: $("#layuiconfig-form-role"),
                content: "${basepath}user/toEditUser",
                type: 2,
                // maxmin: true,
                area: ['500px', '480px'],
                end: function () {
                    window.location.reload();
                }

            });
        });

    });


    function dodel(userId) {
        layui.use(['layer', 'table'], function () {
            var layer = layui.layer;
            var table = layui.table;
            var $ = layui.jquery;
            layer.confirm('确定要删除吗？', {icon: 3, title: '确认删除'}, function (index) {
                $.ajax({
                    url: "${pageContext.request.contextPath}/user/deleteOne",
                    data: "aid=" + userId,
                    success: function (data) {
                        layer.alert("删除" + data.msg, {time: 2000});
                        table.reload("Lay_back_table");
                    },
                    error: function (data) {
                        table.reload("Lay_back_table");
                        layer.msg("操作失败", {time: 1000, icon: 5});
                    }
                });
                layer.close(index);
            });
        });
    }

    function doMultiDelete() {
        //获取到选中的内容的id===》table模块中找方法
        layui.use(['layer', 'table'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var $ = layui.jquery;
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].userId;
                    }
                    console.log("ids===" + ids);
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/user/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert("删除" + data.msg, {time: 2000});
                            //刷新table
                            table.reload("Lay_back_table");
                        }
                    })
                });
            }
        });
    }

</script>
</body>
</html>

