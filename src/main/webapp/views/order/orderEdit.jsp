<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/8
  Time: 16:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../head.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>订单编辑页面</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
</head>
<body>
<form class="layui-form" style="margin-top: 20px;">
    <input type="hidden" name="channelId" value="${channel.channelId}">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label width100">选择下单用户</label>
            <div class="layui-input-inline">
                <select name="merchantId" id="merchantId" lay-verify="required" lay-search >
                    <option value=""></option>
                    <c:forEach items="${merchantList}" var="merchant">
                        <option value="${merchant.mId}"
                                <c:if test="${channel.merchantId == merchant.mId }">selected</c:if>>${merchant.mName}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label width100">支付名称</label>
            <div class="layui-input-inline">
                <input type="text" name="channelName" value="${channel.channelName}" lay-verify="required" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label width100">同步回调地址</label>
        <div class="layui-input-block">
            <input type="text" name="syncUrl" value="${channel.syncUrl}" lay-verify="required" autocomplete="off" class="layui-input" style="width: 535px;">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label width100">异步回调地址</label>
        <div class="layui-input-block">
            <input type="text" name="asynUrl" value="${channel.asynUrl}" lay-verify="required" autocomplete="off" class="layui-input" style="width: 535px;">
        </div>
    </div>
    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label width100">公钥</label>
        <div class="layui-input-block">
            <textarea placeholder="请输入内容" class="layui-textarea" name="publicKey" lay-verify="required" autocomplete="off" style="width: 535px;">${channel.publicKey}</textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label width100">私钥</label>
        <div class="layui-input-block">
            <textarea placeholder="请输入内容" class="layui-textarea" name="privateKey" lay-verify="required" autocomplete="off" style="width: 535px;">${channel.privateKey}</textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button type="submit" class="layui-btn" lay-filter="btnSub" lay-submit>立即提交</button>
        </div>
    </div>
</form>


<script type="text/javascript">
    layui.use(['table', 'form'], function () {
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        //监听提交
        form.on('submit(btnSub)', function(data){
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            $.ajax({
                url:'<%=basePath%>payment/channel/updateOrSave',
                data : data.field,
                type : 'post',
                dataType : 'json',
                success : function(data) {
                    layer.msg('操作成功', {
                        offset : '15px',
                        icon : 6,
                        time : 1000
                    }, function() {
                        parent.layer.close(index);
                        parent.layui.table.reload('tab',{

                        });
                    });
                },
                error : function(data) {
                    layer.msg('操作失败', {
                        offset : '15px',
                        icon : 5,
                        time : 1000
                    }, function() {
                        parent.layer.close(index);
                        parent.layui.table.reload('tab',{

                        });
                    });
                }
            });
            return false;
        });
    });
</script>
</body>
</html>
