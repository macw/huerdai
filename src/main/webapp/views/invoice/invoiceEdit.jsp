<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>商户信息管理</title>
	<%@ include file="../head.jsp" %>
</head>
<body>
 <form class="layui-form" style="margin-top: 20px;">
 	<input type="hidden" name="invoiceId" value="${dto.invoiceId}">
 	<div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">来源</label>
              <!-- <button type="button" class="layui-btn layui-btn-normal" id="chooseSource" style=" float: left; margin-right: 20px;">选择货源</button> -->
		      <div class="layui-input-inline">
		       	  <input type="text" name="source" id="source" value="${dto.source}" readonly="readonly" lay-verify="required" autocomplete="off" class="layui-input" style="width: 515px;">
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">发票编号</label>
		      <div class="layui-input-inline">
		        <input type="text" name="invoiceNum" value="${dto.invoiceNum}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    	<div class="layui-inline">
		      <label class="layui-form-label">付款币种</label>
		      <div class="layui-input-inline">
		        <select name="paymentCurrencyCode" id="paymentCurrencyCode" lay-verify="required" lay-search >
					<option value="">付款币种</option>
					<option value="CNY" <c:if test="${dto.paymentCurrencyCode == 'CNY'}">selected</c:if> >CNY</option>
				</select>
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">发票摘要</label>
		      <div class="layui-input-inline">
		        <input type="text" name="description" value="${dto.description}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    	<div class="layui-inline">
		      <label class="layui-form-label">发票类型</label>
		      <div class="layui-input-inline">
		        <select name="invoiceTypeLookupCode" id="invoiceTypeLookupCode" lay-verify="required" lay-search >
					<option value="">发票类型</option>
					<option value="1" <c:if test="${dto.invoiceTypeLookupCode == 1}">selected</c:if> >信用发票</option>
					<option value="2" <c:if test="${dto.invoiceTypeLookupCode == 2}">selected</c:if> >借方发票</option>
					<option value="3" <c:if test="${dto.invoiceTypeLookupCode == 3}">selected</c:if> >费用发票</option>
					<option value="4" <c:if test="${dto.invoiceTypeLookupCode == 4}">selected</c:if> >混合发票</option>
					<option value="5" <c:if test="${dto.invoiceTypeLookupCode == 5}">selected</c:if> >先付发票</option>
					<option value="6" <c:if test="${dto.invoiceTypeLookupCode == 6}">selected</c:if> >标准发票</option>
				</select>
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">凭证编号</label>
		      <div class="layui-input-inline">
		        <input type="text" name="docSequenceValue" value="${dto.docSequenceValue}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    	<div class="layui-inline">
		      <label class="layui-form-label">付款状态</label>
		      <div class="layui-input-inline">
		        <select name="paymentStatusFlag" id="paymentStatusFlag" lay-verify="required" lay-search >
					<option value="">付款状态</option>
					<option value="1" <c:if test="${dto.paymentStatusFlag == 1}">selected</c:if> >全额付款</option>
					<option value="2" <c:if test="${dto.paymentStatusFlag == 2}">selected</c:if> >未付款</option>
					<option value="3" <c:if test="${dto.paymentStatusFlag == 3}">selected</c:if> >部分付款</option>
				</select>
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">发票金额</label>
		      <div class="layui-input-inline">
		        <input type="text" name="invoiceAmount" id="invoiceAmount" value="${dto.invoiceAmount}"  lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    	<div class="layui-inline">
		        <label class="layui-form-label">已付金额</label>
		      <div class="layui-input-inline">
		        <input type="text" name="amountPaid"  id="amountPaid" value="${dto.amountPaid}"  lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">发票日期</label>
		      <div class="layui-input-inline">
		        <input type="text" class="layui-input" name="invoiceDate" id="invoiceDate" placeholder="yyyy-MM-dd HH:mm:ss" lay-verify="required" value="${dto.invoiceDateString}">
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
	    <div class="layui-input-block">
	      <button type="submit" class="layui-btn" lay-submit="" lay-filter="btnSub">立即提交</button>
	    </div>
  </div>
 </form>
</body>
 
   

<script type="text/javascript">
        layui.use(['table', 'form','laydate'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            var laydate = layui.laydate;
            
          //日期时间选择器
            laydate.render({
              elem: '#invoiceDate'
              ,type: 'datetime'
            });
          //监听提交
            form.on('submit(btnSub)', function(data){
           		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            	$.ajax({
                  	 url:'<%=basePath%>invoice/saveOrUpdate',
                  	 data: data.field,
                  	 type: 'post',
                  	 dataType: 'json',
                  	 success:function(data){
                  		 if(data.se){
	                  		layer.msg(data.msg, { offset: '15px',icon: 6,time: 1000}, function(){
		                  		parent.layer.close(index);
				            	parent.location.reload();
	                  		});
                  		 }else{
	                  		layer.msg(data.msg, { offset: '15px',icon: 5,time: 1000}, function(){
		                  		parent.layer.close(index);
				            	parent.location.reload();
	                  		});
                  		 }
                  	 },error:function(data){
                  		layer.msg('操作失败', { offset: '15px',icon: 5,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                  		});
                  	 }
                 });
            	 
                 return false;
            });
          
            $("#chooseSource").click(function(){
            	$("#invoiceAmount").val(45);
            	$("#amountPaid").val(45);
            	$("#source").val("123");
            	//去查找订单返回订单的支付金额
            	<%-- layer.open({
                    type:2,
                    closeBtn:0,
                    title:"选择订单",
                    content:"<%=basePath%>order/selectData",
                    area: ['800px', '500px']
            	}); --%>
            	
            	layer.msg('操作成功');
            });
        });
        
        
    </script>
</html>

