<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>商户信息管理</title>
	<%@ include file="../head.jsp" %>
	 <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>
</head>
<body>
<div class="layui-fluid">
    <div class="layui-card-body layui-form">
        <div style="padding-bottom: 10px;">
           <!--  <div class="layui-input-inline" style="width: 100px;">
                 <button type="button" class="layui-btn add" data-type="add" onclick="add()">添加</button>
            </div> -->
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="invoiceNum" id="invoiceNum" placeholder="发票编号/订单号" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
				<select name="invoiceTypeLookupCode" id="invoiceTypeLookupCode" lay-search>
					<option value="">发票类型</option>
					<option value="1">信用发票</option>
					<option value="2">借方发票</option>
					<option value="3">费用发票</option>
					<option value="4">混合发票</option>
					<option value="5">先付发票</option>
					<option value="6">标准发票</option>
				</select>
			</div>
            <div class="layui-input-inline" style="width: 200px;">
				<select name="paymentStatusFlag" id="paymentStatusFlag" lay-search>
					<option value="">付款状态</option>
					<option value="1">全额付款</option>
					<option value="2">未付款</option>
					<option value="3">部分付款</option>
				</select>
			</div>
            <div class="layui-input-inline" style="width: 200px;">
				<select name="approvalStatus" id="approvalStatus" lay-search>
					<option value="">审批状态</option>
					<option value="1">已保存</option>
					<option value="2">已提交</option>
					<option value="3">审核成功</option>
					<option value="4">审核失败</option>
				</select>
			</div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="createdName" id="createdName" placeholder="创建人" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>
        <table id="tab" lay-filter="tab"></table>
    </div>
</div>
</body>
 
  <script type="text/html" id="tool">
        <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">编辑</button>
        <button type="button" class="layui-btn layui-btn-danger" lay-event="del">删除</button>
  </script>

<script type="text/javascript">

        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            table.render({
                elem: '#tab',
                url: '<%=basePath%>invoice/selectData', //数据接口
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                cols: [[
                    {title: "发票编号", field: "invoiceNum",align:"center",width :120},
                    {title: "付款币种", field: "paymentCurrencyCode",align:"center",width :90},
                    {title: "发票金额", field: "invoiceAmount",align:"center",width :90},
                    {title: "已付金额", field: "amountPaid",align:"center",width :90},
 					{title: "创建人", field: "createdName",align:"center",width :90},
                    {title: "来源", field: "source",align:"center",width :120},
                    {title: "发票类型",align:"center",width :90,
                    	templet: function(d){
                    		if(d.invoiceTypeLookupCode!=null){
                    			var codeType = d.invoiceTypeLookupCode;
                    			if(codeType == 1){
                    				return '信用发票';
                    			}else if(codeType == 2){
                    				return '借方发票';
                    			}else if(codeType == 3){
                    				return '费用发票';
                    			}else if(codeType == 4){
                    				return '混合发票';
                    			}else if(codeType == 5){
                    				return '先付发票';
                    			}else if(codeType == 6){
                    				return '标准发票';
                    			}
                    		}
                    		return '';
                    	}	
                    },
                    {title: "发票摘要", field: "description",align:"center",width :150},
                    {title: "付款状态",align:"center",width :90,
                    	templet: function(d){
                    		if(d.paymentStatusFlag!=null){
                    			var paymentStatus = d.paymentStatusFlag;
                    			if(paymentStatus == 1){
                    				return '全额付款';
                    			}else if(paymentStatus == 2){
                    				return '未付款';
                    			}else if(paymentStatus == 3){
                    				return '部分付款';
                    			}
                    		}
                    		return '';
                    	}	
                    },
                    {title: "凭证编号", field: "docSequenceValue",align:"center",width :120},
                    {title: "发票时间",align:"center",
                    	templet: function(d){
                    		if(d.invoiceDate!=null){
	                    		return layui.util.toDateString(d.invoiceDate);
                    		}
                    		return '';
                    	},width:160	
                    },
                    {title: "创建时间",align:"center",
                    	templet: function(d){
                    		if(d.creationDate!=null){
	                    		return layui.util.toDateString(d.creationDate);
                    		}
                    		return '';
                    	},width:160		
                    },
                    {title: "最后更新人", field: "lastUpdateName",align:"center",width :100},
                    {title: "最后更新日期",align:"center",
                    	templet: function(d){
                    		if(d.lastUpdateDate!=null){
	                    		return layui.util.toDateString(d.lastUpdateDate);
                    		}
                    		return '';
                    	},width:160		
                    },
                    {title: "取消人", field: "cancelledName",align:"center",width :90},
                    {title: "取消金额", field: "cancelledAmount",align:"center",width :120},
                    {title: "取消日期",align:"center",
                    	templet: function(d){
                    		if(d.cancelledDate!=null){
	                    		return layui.util.toDateString(d.cancelledDate);
                    		}
                    		return '';
                    	},width:160		
                    },
                    {title: "审批",width :175, align:"center",
						templet :function(d){
							var str="";
							if(d.approvalStatus!=null){
	                    		if(d.approvalStatus==1){
	                    			str =  "<button type='button' class='layui-btn  layui-btn-warm' lay-event='sub'>提交</button>"
	                    		}else if(d.approvalStatus==2){
	                    			str =  "<button type='button' class='layui-btn'  lay-event='ok'>通过</button>"
	                    				  +"<button type='button' class='layui-btn  layui-btn-danger' lay-event='fail'>未通过</button>"
	                    		}else if(d.approvalStatus==3){ 
	                    			str =  "<span style='color: #0ebb25;'><i class='layui-icon layui-icon-ok'></i>审核成功 </span>"
	                    		}else if(d.approvalStatus==4){
	                    			str =  "<span style='color: #e20d0b;'><i class='layui-icon layui-icon-close'></i>审核失败 </span>"
	                    		}
                    		}
							return str;
						}
                    },
                    {title: "审批状态", field: "approvalStatus",align:"center",width :90,
                    	templet: function(d){
                    		if(d.approvalStatus!=null){
	                    		if(d.approvalStatus==1){
	                    			return '已保存'
	                    		}else if(d.approvalStatus==2){
	                    			return '已提交'
	                    		}else if(d.approvalStatus==3){
	                    			return '审核成功'
	                    		}else if(d.approvalStatus==4){
	                    			return '审核失败'
	                    		}
                    		}
                    		return '';
                    	},fixed:"right"	
                    },
                    {title: "操作",width :175, align:"center", templet: "#tool",fixed:"right"	}
                ]],done:function(res, curr){
      	    	  var brforeCurr = curr; // 获得当前页码
    	    	  var dataLength = res.data.length; // 获得当前页的记录数
    	    	  var count = res.count; // 获得总记录数
    	    	  if(dataLength == 0 && count != 0){ //如果当前页的记录数为0并且总记录数不为0
    	    		  table.reload("tab",{ // 刷新表格到上一页
    	    			  page:{
    	    				 curr:brforeCurr-1
    	    			  }
    	    		  });
    	    	  }
    	      }
            });
            
            
          	//监听行工具事件
            table.on('tool(tab)', function(obj){
              var data = obj.data;
              if(obj.event === 'del'){
                layer.confirm('确定要删除这条发票信息吗?', function(index){
                	$.ajax({
                   	 url:'<%=basePath%>invoice/deleteById',
                   	 data: {"invoiceId":data.invoiceId},
                   	 type: 'post',
                   	 dataType: 'json',
                   	 success:function(data){
                   		 tableReload();
                   		layer.msg("操作成功", {time: 1000, icon:6});
                   	 },error:function(data){
                   		 tableReload();
                   		layer.msg("操作失败", {time: 1000, icon:5});
                   	 }
                    });
                  	layer.close(index);
                });
              } else if(obj.event === 'ok'){
            	  updateApprovalStatus(data.invoiceId,3);
              } else if(obj.event === 'fail'){
            	  updateApprovalStatus(data.invoiceId,4);
              } else if(obj.event === 'sub'){
            	  updateApprovalStatus(data.invoiceId,2);
              } else if(obj.event === 'edit'){
            	  layer.open({
             		  type: 2,
             		  title: '编辑发票管理',
             		  shadeClose: true,
             		  shade: 0.8,
             		  area: ['50%', '70%'],
             		  content: '<%=basePath%>invoice/editPage?invoiceId='+data.invoiceId
              	});  
              }
            });
        });
        //搜索操作
        function doSearch() {
            //1.获取到输入框中输入的内容
            var invoiceNum = $('#invoiceNum').val();
            var invoiceTypeLookupCode = $('#invoiceTypeLookupCode').val();
            var paymentStatusFlag = $('#paymentStatusFlag').val();
            var createdName = $('#createdName').val();
            var approvalStatus = $('#approvalStatus').val();
            //发送请求，并且接收数据
            layui.use('table', function () {
                var table = layui.table;
                table.reload('tab', {
                    where: {"approvalStatus": approvalStatus,"invoiceNum": invoiceNum,"invoiceTypeLookupCode": invoiceTypeLookupCode,"paymentStatusFlag": paymentStatusFlag,"createdName": createdName},
                    page: {
                        curr: 1 //重新从第 1 页开始
                      }
                });
            });
        };
        function tableReload(){
       	   layui.use('table', function () {
              var table = layui.table;
              table.reload('tab', {
              });
           });
        };
        function add(){
        	layui.use('layer', function(){
        		layer.open({
             		  type: 2,
             		  title: '添加发票管理',
             		  shadeClose: true,
             		  shade: 0.8,
             		  area: ['50%', '70%'],
             		  content: '<%=basePath%>invoice/editPage'
              	});   
       		}); 
        };
        
        function updateApprovalStatus(invoiceId,state){
        	 $.ajax({
            	 url:'<%=basePath%>invoice/saveOrUpdate',
            	 data: {"invoiceId":invoiceId,"approvalStatus":state},
            	 type: 'post',
            	 dataType: 'json',
            	 success:function(data){
            		layer.msg('操作成功', { offset: '15px',icon: 6,time: 1000}, function(){
            			tableReload();
            		});
            	 },error:function(data){
            		layer.msg('操作失败', { offset: '15px',icon: 5,time: 1000}, function(){
            			tableReload();
            		});
            	 }
           });
        }
        
        
        
    </script>
</html>

