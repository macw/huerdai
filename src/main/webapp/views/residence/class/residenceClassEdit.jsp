<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../../head.jsp" %>
</head>
<body>
 <form id="form" class="layui-form" style="margin-top: 20px;">
 	<input type="hidden" name="residenceclassId" value="${dto.residenceclassId}">
 	  <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label" style="width: 100px;">民宿分类名称</label>
		      <div class="layui-input-inline">
		        <input type="text" name="residenceclassName" value="${dto.residenceclassName}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label" style="width: 100px;">备注</label>
      <div class="layui-input-inline">
         <input type="tel" name="memo" value="${dto.memo}" lay-verify="required" autocomplete="off" class="layui-input">
      </div>
    </div>
    <div class="layui-form-item">
	    <div class="layui-input-block">
	      <button type="submit" class="layui-btn" lay-submit="" lay-filter="btnSub" id="subBtn">立即提交</button>
	    </div>
  </div>
 </form>
</body>
 
   

<script type="text/javascript">
		var activce=null;
        layui.use(['table', 'form'], function () {
            var layer = layui.layer;
            var table = layui.table;
            var form = layui.form;
           
          //监听提交
            form.on('submit(btnSub)', function(data){
            	var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            	$.ajax({
                  	 url:'<%=basePath%>residence/class/saveOrUpdate',
                  	 data: data.field,
                  	 type: 'post',
                  	 dataType: 'json',
                  	 success:function(data){
                  		if(data.se){
                  			layer.msg(data.msg, { offset: '15px',icon: 6,time: 1000}, function(){
    	                  		parent.layer.close(index);
    			            	parent.location.reload();
                      		});
                  		}else{
                  			layer.msg(data.msg, { offset: '15px',icon: 5,time: 1000}, function(){
    	                  		parent.layer.close(index);
    			            	parent.location.reload();
                      		});
                  		}
                  	 },error:function(data){
                  		layer.msg('操作失败', { offset: '15px',icon: 5,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                  		});
                  	 }
                 });
            	 
                 return false;
            });
          
        });
    </script>
</html>

