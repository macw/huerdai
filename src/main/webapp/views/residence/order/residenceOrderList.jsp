<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>民宿管理</title>
	<%@ include file="../../head.jsp" %>
	 <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }
    </style>
</head>
<body>
<div class="layui-fluid">
    <div class="layui-card-body layui-form">
        <div style="padding-bottom: 10px;">
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="residenceOrderId" id="residenceOrderId" placeholder="订单号" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="orderName" id="orderName" placeholder="用户名/手机号/地址" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>
        <table id="tab" lay-filter="tab"></table>
    </div>
</div>
</body>
 
  <script type="text/html" id="tool">
       <!-- <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">编辑</button> -->
        <button type="button" class="layui-btn layui-btn-danger" lay-event="del">删除</button>
  </script>

<script type="text/javascript">

		var active=null;
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            table.render({
                elem: '#tab',
                url: '<%=basePath%>residence/order/selectData',  //数据接口
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                cols: [[
                    {type: "checkbox"},
                    {title: "订单号", field: "residenceOrderId",align:"center",width :200},
                    {title: "用户名", field: "orderName",align:"center",width :90},
                    {title: "手机号", field: "orderPhone",align:"center",width :120},
                    {title: "地址", field: "orderAddress",align:"center",width :150},
                    {title: "民宿信息", align:"center",
                    	templet: function(d){
                    		return d.residenceName + '/'+d.residenceroomName;
                    	},width :160
                    },
                    {title: "居住时间", align:"center",
                    	templet: function(d){
                    		return layui.util.toDateString(d.liveBegintime,'yyyy-MM-dd')+'-'+layui.util.toDateString(d.liveEndtime,'yyyy-MM-dd');
                    	},width :190
                    },
                    {title: "支付类型", align:"center",
                    	templet: function(d){
                    		if(d.paymentMethod == 1){
                    			return '现金';
                    		}else if(d.paymentMethod == 2){
                    			return '余额';
                    		}else if(d.paymentMethod == 3){
                    			return '网银';
                    		}else if(d.paymentMethod == 4){
                    			return '支付宝';
                    		}else if(d.paymentMethod == 5){
                    			return '微信';
                    		}else{
                    			return '';
                    		}
                    	},width :90
                    },
                    {title: "订单金额", field: "orderMoney",align:"center",width :90},
                    {title: "优惠金额", field: "districtMoney",align:"center",width :90},
                    {title: "支付金额", field: "paymentMoney",align:"center",width :90},
                    {title: "下单时间", field: "paymentMoney",align:"center",
                    	templet: function(d){
                    		if(d.orderTime!=null){
	                    		return layui.util.toDateString(d.orderTime);
                    		}
                    		return '';
                    	},width :160	
                    		
                    },
                    {title: "支付时间", field: "paymentMoney",align:"center",
                    	templet: function(d){
                    		if(d.payTime!=null){
	                    		return layui.util.toDateString(d.payTime);
                    		}
                    		return '';
                    	},width :160	
                    		
                    },
                    {title: "订单状态",align:"center",
                    	templet: function(d){
                    		if(d.orderStatus==1){
	                    		return '已支付';
                    		}else if(d.orderStatus==2){
	                    		return '未支付';
                    		}else if(d.orderStatus==3){
                    			return '已完成';
                    		}
                    		return '';
                    	},width :90		
                    },
                    {title: "发票抬头", field: "invoiceTitle",align:"center",width :150},
                    {title: "备注", field: "memo",align:"center",width :200},
                    {title: "保险标题", field: "insName",align:"center",width :150},
                    {title: "创建人", field: "createdName",align:"center",width :90},
                    {title: "创建日期",align:"center",
                    	templet: function(d){
                    		if(d.creationDate!=null){
	                    		return layui.util.toDateString(d.creationDate);
                    		}
                    		return '';
                    	},width :160	
                    }
                   /* , {title: "更新人", field: "lastUpdateName",align:"center",width :90},
                    {title: "更新日期",align:"center",
                    	templet: function(d){
                    		if(d.lastUpdateDate!=null){
	                    		return layui.util.toDateString(d.lastUpdateDate);
                    		}
                    		return '';
                    	},width :160	
                    },
                    {title: "状态",align:"center",width :100,
                    	templet: function(d){
                    		if(d.status!=null && d.status == 1){
	                    		return '<button type="button" class="layui-btn layui-btn-normal" lay-event="yn">启用</button>';
                    		}else if(d.status!=null && d.status == 2){
	                    		return '<button type="button" class="layui-btn layui-btn-danger" lay-event="yn">禁用</button>';
                    		}
                    		return '';
                    	}
                    },
                    {title: "操作",width :175, align:"center", templet: "#tool"} */
                    ,{title: "操作",width :100, align:"center", templet: "#tool"} 
                ]],done:function(res, curr){
        	    	  var brforeCurr = curr; // 获得当前页码
        	    	  var dataLength = res.data.length; // 获得当前页的记录数
        	    	  var count = res.count; // 获得总记录数
        	    	  if(dataLength == 0 && count != 0){ //如果当前页的记录数为0并且总记录数不为0
        	    		  table.reload("tab",{ // 刷新表格到上一页
        	    			  page:{
        	    				 curr:brforeCurr-1
        	    			  }
        	    		  });
        	    	  }
        	      }
            });
          	//监听行工具事件
            table.on('tool(tab)', function(obj){
              var data = obj.data;
              if(obj.event === 'yn'){
            	  var str = "启用";
            	  if(data.status == 1){
            		  str = "禁用";
            	  }
                  layer.confirm('确认要['+str+']这个房间吗?', function(index){
                     $.ajax({
                    	 url:'<%=basePath%>residence/room/updateYnByResidenceRoomId',
                    	 data: {"residenceroomId":data.residenceroomId},
                    	 type: 'post',
                    	 dataType: 'json',
                    	 success:function(d){
                    		 table.reload('tab', {});
                    		 if(d.se){
	                    		 layer.msg(d.msg, {time: 1000, icon:6});
                    		 }else{
	                    		 layer.msg(d.msg, {time: 1000, icon:5});
                    		 }
                    	 },error:function(d){
                    		 table.reload('tab', {});
                    		 layer.msg(d.msg, {time: 1000, icon:5});
                    	 }
                     });
                    	layer.close(index);
                  });
              }else if(obj.event === 'del'){
                layer.confirm('真的删除这条订单记录吗?', function(index){
                	$.ajax({
                   	 url:'<%=basePath%>residence/order/deleteByResidenceOrderId',
                   	 data: {"residenceOrderId":data.residenceOrderId},
                   	 type: 'post',
                   	 dataType: 'json',
                   	 success:function(data){
                   		table.reload('tab', {});
                   		if(data.se){
	                   		layer.msg(data.msg, {time: 1000, icon:6});
                   		}else{
	                   		layer.msg(data.msg, {time: 1000, icon:5});
                   		}
                   	 },error:function(data){
                   		table.reload('tab', {});
                   		layer.msg(data.msg, {time: 1000, icon:5});
                   	 }
                    });
                });
              } else if(obj.event === 'edit'){
            	  layer.open({
		    		  type: 2,
		    		  title: '民宿管理',
		    		  shadeClose: true,
		    		  shade: 0.8,
		    		  area: ['451px', '420px'],
		    		  content: '<%=basePath%>residence/room/editPage?residenceroomId='+data.residenceroomId
		     	});
              }else if(obj.event === 'roomInfo'){
            	  window.location.href = '<%=basePath%>residence/room/roomInfo?residenceroomId='+data.residenceroomId;
              }
            });
            form.on('select(province)', function(data){
           		$("#district").empty();
           		$("#city").empty();
        		var str="<option value=''>区</option>";
           		$("#district").append(str);
        		str="<option value=''>市</option>";
        		if(data.value.length <= 0){
        			$("#city").append(str);
        			form.render('select');
        		}else{
	           		$.ajax({
		           		 url:'<%=basePath%>region/ajaxRegionList?pid='+data.value,
		              	 type: 'post',
		              	 dataType: 'json',
	           			 success:function(d){
	           				$("#city").empty();
	           				if(d.data.length>0){
		           				$(d.data).each(function(i,t){
		           					str+="<option value='"+t.id+"'>"+t.name+"</option>"
		           				})
	           				}
	           				$("#city").append(str);
			           		form.render('select');
	           			 }
	           		});
        		}
           	});
           	form.on('select(city)', function(data){
           		var str="<option value=''>区</option>";
           		$.ajax({
	           		 url:'<%=basePath%>region/ajaxRegionList?pid='+data.value,
	              	 type: 'post',
	              	 dataType: 'json',
          			 success:function(d){
          				$("#district").empty();
          				if(d.data.length>0){
	           				$(d.data).each(function(i,t){
	           					str+="<option value='"+t.id+"'>"+t.name+"</option>"
	           				})
          				}
          				$("#district").append(str);
          				form.render('select');
          			 }
          		});
           	});
            active = {
   		        add: function(){ //获取选中数据
   					layer.open({
			    		  type: 2,
			    		  title: '民宿添加',
			    		  shadeClose: true,
			    		  shade: 0.8,
			    		  area: ['451px', '420px'],
			    		  content: '<%=basePath%>residence/room/editPage'
			     	}); 
   		        },
   				doSearch: function(){
   					table.reload('tab', {
   	                    where: {"residenceOrderId": $('#residenceOrderId').val(),"orderName": $('#orderName').val()},
   	                    page: {
   	                        curr: 1 //重新从第 1 页开始
   	                      }
   	                });
   				},
   				tableReload:function(){
	   				 table.reload('tab', {});
   				}
           };
        });
        //搜索操作
        function doSearch() {
        	active.doSearch();
        };
        function add(){
        	active.add();
        };
        
        
        
    </script>
</html>

