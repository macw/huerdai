<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../../head.jsp" %>
</head>
<body>
<form id="form" class="layui-form" enctype="multipart/form-data" style="margin-top: 20px;">
 	<input type="hidden" name="commentParent" value="${dto.commentParent}">
    <div class="layui-form-item">
   		<div class="layui-inline">
   			<label class="layui-form-label">回复</label>
   			<div class="layui-input-inline">
	       		<textarea placeholder="回复" class="layui-textarea" name="content" lay-verify="required" autocomplete="off" style="width: 300px;">${dto.content}</textarea>
		    </div>
   		</div>
    </div>
   
    <div class="layui-form-item">
   		 <div class="layui-inline">
			<label class="layui-form-label">上传图片</label>
			<div class="layui-input-inline" style="width: 80px;">
				<button type="button" class="layui-btn" id="file">
					<i class="layui-icon"></i>上传文件
				</button>
			</div>
			<div class="layui-input-inline">
				 <img class="layui-upload-img" name="pictureurl" src="${dto.pictureurl}" id="pictureurl" style="max-width: 100px;max-height: 100px; padding-left: 40px;">
			</div>
		</div>
		
    </div>
   
    <div class="layui-form-item">
	    <div class="layui-elem-field site-demo-button">
	    	<center> 
	       <button type="submit" class="layui-btn" lay-submit="" lay-filter="btnSub" id="subBtn">立即提交</button>
	       <c:if test="${isDel ==true }">
		       <button type="button" class="layui-btn layui-btn-danger" id="delBtn">删除评论</button>
	       </c:if>
	       </center>
	    </div>
  </div>
 </form>
</body>
 
   

<script type="text/javascript">
		var activce=null;
		var layer=null;
		var index = null;
        layui.use(['table', 'form','upload'], function () {
            var table = layui.table;
            layer = layui.layer;
            var form = layui.form;
            var upload = layui.upload;
            index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            upload.render({
				elem : '#file',
				auto : false,
				accept: "images",
                acceptMime: 'image/*',//打开文件选择框时,只显示图片文件
                auto: false,  //是否选完文件后自动上传。默认值：true
                bindAction : '#test9',
               // bindAction: '#subBtn',
				choose : function(obj) {
					obj.preview(function(index, file, result) {
						$('#pictureurl').attr('src', result);
					})
				}
			});    
          //监听提交
            form.on('submit(btnSub)', function(data){
            	index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            	var formData = new FormData(document.getElementById("form"));
            	$.ajax({
                  	 url:'<%=basePath%>residence/comment/saveReply',
                  	 data: formData,
                  	 type: 'post',
                  	 dataType: 'json',
                  	 processData : false,
					 contentType : false,
                  	 success:function(data){
                  		if(data.se){
                  			layer.msg(data.msg, { offset: '15px',icon: 6,time: 1000}, function(){
    	                  		parent.layer.close(index);
    			            	parent.location.reload();
                      		});
                  		}else{
                  			layer.msg(data.msg, { offset: '15px',icon: 5,time: 1000}, function(){
    	                  		parent.layer.close(index);
    			            	parent.location.reload();
                      		});
                  		}
                  	 },error:function(data){
                  		layer.msg('操作失败', { offset: '15px',icon: 5,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                  		});
                  	 }
                 });
                 return false;
            });
          
        });
        
        $("#delBtn").click(function(){
        	var commentId ='${dto.commentId}';
        	$.ajax({
             	 url:'<%=basePath%>residence/comment/deleteByResidenceCommentId?commentId='+commentId,
             	 type: 'post',
             	 dataType: 'json',
             	 success:function(data){
             		 if(data.se){
             			layer.msg(data.msg, { offset: '15px',icon: 6,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                 		});
             		 }else{
             			layer.msg(data.msg, { offset: '15px',icon: 5,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                 		});
             		 }
             		
             	 },error:function(data){
             		layer.msg('操作失败', { offset: '15px',icon: 5,time: 1000}, function(){
                 		parent.layer.close(index);
		            	parent.location.reload();
             		});
             	 }
            });
            return false;
        });
    </script>
</html>

