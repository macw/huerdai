<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>民宿评论</title>
	<%@ include file="../../head.jsp" %>
	 <style type="text/css">
		.layui-table-cell {
			font-size:14px;
		    padding:0 5px;
		    height:auto;
		    overflow:visible;
		    text-overflow:inherit;
		    white-space:normal;
		    word-break: break-all;
		    }
    </style>
</head>
<body>
<div class="layui-fluid">
    <div class="layui-card-body layui-form">
        <div style="padding-bottom: 10px;">
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="title" id="title" placeholder="民宿名称/评论人" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="residenceOrderId" id="residenceOrderId" placeholder="订单号" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="content" id="content" placeholder="评论标题/内容" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 190px;">
		        <select name="auditStatus" id="auditStatus">
					<option value="">审核状态</option>
					<option value="1">通过</option>
					<option value="0">未通过</option>
				</select>
		     </div>
            
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>
        <table id="tab" lay-filter="tab"></table>
    </div>
</div>
</body>
 
  <script type="text/html" id="tool">
        <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">回复</button>
        <button type="button" class="layui-btn layui-btn-danger" lay-event="del">删除</button>
  </script>

<script type="text/javascript">

		var active=null;
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            table.render({
                elem: '#tab',
                url: '<%=basePath%>residence/comment/selectData',  //数据接口
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                cols: [[
                    {type: "checkbox"},
                    {title: "民宿名称", field: "title",align:"center",width :150},
                    {title: "订单号", field: "residenceOrderId",align:"center",width :250},
                    {title: "评论名", field: "customerName",align:"center",width :90},
                    {title: "评论内容", field: "content",align:"center"},
                    {title: "评论图片",align:"center",
                    	templet: function(d){
                    		if(d.pictureurl!=null){
	                    		return '<img src="'+d.pictureurl+'"/>';
                    		}
                    		return '';
                    	},width :120		
                    },
                    {title: "民宿评分", field: "goodsScore",align:"center",width :90},
                    {title: "服务评分", field: "serviceScore",align:"center",width :90},
                    {title: "评价日期",align:"center",
                    	templet: function(d){
                    		if(d.auditTime!=null){
	                    		return layui.util.toDateString(d.auditTime);
                    		}
                    		return '';
                    	},width :160	
                    },
                    {title: "更新日期",align:"center",
                    	templet: function(d){
                    		if(d.modifiedTime!=null){
	                    		return layui.util.toDateString(d.modifiedTime);
                    		}
                    		return '';
                    	},width :160	
                    },
                    {title: "审核状态", align:"center",
                    	templet: function(d){
                    		if(d.auditStatus == 0){
                    			return '<button type="button" class="layui-btn layui-btn-danger" lay-event="yn">未通过</button>';
                    		}else if(d.auditStatus == 1){
                    			return '<button type="button" class="layui-btn layui-btn-normal" lay-event="yn">通过</button>';
                    		}else{
	                    		return '';
                    		}
                    	},width :90
                    },
                    {title: "操作",width :175, align:"center", templet: "#tool"}
                ]],done:function(res, curr){
        	    	  var brforeCurr = curr; // 获得当前页码
        	    	  var dataLength = res.data.length; // 获得当前页的记录数
        	    	  var count = res.count; // 获得总记录数
        	    	  if(dataLength == 0 && count != 0){ //如果当前页的记录数为0并且总记录数不为0
        	    		  table.reload("tab",{ // 刷新表格到上一页
        	    			  page:{
        	    				 curr:brforeCurr-1
        	    			  }
        	    		  });
        	    	  }
        	      }
            });
          	//监听行工具事件
            table.on('tool(tab)', function(obj){
              var data = obj.data;
              if(obj.event === 'yn'){
            	  var str = "审核不通过";
            	  if(data.auditStatus == 0){
            		  str = "审核通过";
            	  }
                  layer.confirm('将该评论设置为'+str+'吗?', function(index){
                     $.ajax({
                    	 url:'<%=basePath%>residence/comment/updateYnByResidenceCommentId',
                    	 data: {"commentId":data.commentId},
                    	 type: 'post',
                    	 dataType: 'json',
                    	 success:function(d){
                    		 table.reload('tab', {});
                    		 if(d.se){
	                    		 layer.msg(d.msg, {time: 1000, icon:6});
                    		 }else{
	                    		 layer.msg(d.msg, {time: 1000, icon:5});
                    		 }
                    	 },error:function(d){
                    		 table.reload('tab', {});
                    		 layer.msg(d.msg, {time: 1000, icon:5});
                    	 }
                     });
                    	layer.close(index);
                  });
              }else if(obj.event === 'del'){
                layer.confirm('真的删除这个房间吗?', function(index){
                	$.ajax({
                   	 url:'<%=basePath%>residence/room/deleteByResidenceRoomId',
                   	 data: {"commentId":data.commentId},
                   	 type: 'post',
                   	 dataType: 'json',
                   	 success:function(data){
                   		table.reload('tab', {});
                   		if(data.se){
	                   		layer.msg(data.msg, {time: 1000, icon:6});
                   		}else{
	                   		layer.msg(data.msg, {time: 1000, icon:5});
                   		}
                   	 },error:function(data){
                   		table.reload('tab', {});
                   		layer.msg(data.msg, {time: 1000, icon:5});
                   	 }
                    });
                });
              } else if(obj.event === 'edit'){
            	  layer.open({
		    		  type: 2,
		    		  title: '回复',
		    		  shadeClose: true,
		    		  shade: 0.8,
		    		  area: ['500px', '420px'],
		    		  content: '<%=basePath%>residence/comment/editPage?commentParent='+data.commentId
		     	});
              }
            });
            active = {
   				doSearch: function(){
   					table.reload('tab', {
   	                    where: {"title": $('#title').val(),"residenceOrderId": $('#residenceOrderId').val(),"content": $('#content').val(),"auditStatus": $('#auditStatus').val()},
   	                    page: {
   	                        curr: 1 //重新从第 1 页开始
   	                      }
   	                });
   				},
   				tableReload:function(){
	   				 table.reload('tab', {});
   				}
           };
        });
        //搜索操作
        function doSearch() {
        	active.doSearch();
        };
    </script>
</html>

