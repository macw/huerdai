<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../../head.jsp" %>
</head>
<body>
 <form id="form" class="layui-form" style="margin-top: 20px;">
 	<input type="hidden" name="residenceId" value="${dto.residenceId}">
 	  <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">民宿名称</label>
		      <div class="layui-input-inline">
		        <input type="text" name="residenceName" value="${dto.residenceName}" lay-verify="required" autocomplete="off" class="layui-input" style="width: 515px;">
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
   		<div class="layui-inline">
   			<label class="layui-form-label">地址</label>
   			 <div class="layui-input-inline" style="width: 160px;">
		        <select name="province" id="province" lay-filter="province" lay-verify="required" lay-search >
					<option value="">省</option>
					<c:forEach items="${provinceList}" var="p">
						<option value="${p.id}" <c:if test="${dto.province == p.id}">selected</c:if> >${p.name}</option>
					</c:forEach>
				</select>
		     </div>
   			 <div class="layui-input-inline" style="width: 160px;">
		        <select name="city" id="city"  lay-filter="city" lay-verify="required" lay-search >
					<option value="">市</option>
			        <c:forEach items="${cityList}" var="c">
							<option value="${c.id}" <c:if test="${dto.city == c.id}">selected</c:if> >${c.name}</option>
					</c:forEach>
				</select>
		      </div>
   			 <div class="layui-input-inline" style="width: 175px;">
		        <select name="district" id="district" lay-verify="required" lay-search >
					<option value="">区</option>
					 <c:forEach items="${districtList}" var="d">
							<option value="${d.id}" <c:if test="${dto.district == d.id}">selected</c:if> >${d.name}</option>
					</c:forEach>
				</select>
		      </div>
   		</div>
    </div>
    <div class="layui-form-item">
   		<div class="layui-inline">
		      <label class="layui-form-label">详情位置</label>
    		 <div class="layui-input-inline">
			      <div class="layui-input-inline">
			        <input type="text" name="address" value="${dto.address}" lay-verify="required" autocomplete="off" class="layui-input" style="width: 515px;">
			      </div>
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">备注</label>
      <div class="layui-input-inline">
         <input type="tel" name="memo" value="${dto.memo}"  autocomplete="off" class="layui-input"  style="width: 515px;">
      </div>
    </div>
    <div class="layui-form-item">
	    <div class="layui-input-block">
	      <button type="submit" class="layui-btn" lay-submit="" lay-filter="btnSub" id="subBtn">立即提交</button>
	    </div>
  </div>
 </form>
</body>
 
   

<script type="text/javascript">
		var activce=null;
        layui.use(['table', 'form'], function () {
            var layer = layui.layer;
            var table = layui.table;
            var form = layui.form;
            
            form.on('select(province)', function(data){
           		$("#district").empty();
        		var str="<option value=''>区</option>";
           		$("#district").append(str);
        		str="<option value=''>市</option>";
           		$.ajax({
	           		 url:'<%=basePath%>region/ajaxRegionList?pid='+data.value,
	              	 type: 'post',
	              	 dataType: 'json',
           			 success:function(d){
           				$("#city").empty();
           				if(d.data.length>0){
	           				$(d.data).each(function(i,t){
	           					str+="<option value='"+t.id+"'>"+t.name+"</option>"
	           				})
           				}
           				$("#city").append(str);
		           		form.render('select');
           			 }
           		});
           	});
            
           	form.on('select(city)', function(data){
           		var str="<option value=''>区</option>";
           		$.ajax({
	           		 url:'<%=basePath%>region/ajaxRegionList?pid='+data.value,
	              	 type: 'post',
	              	 dataType: 'json',
          			 success:function(d){
          				$("#district").empty();
          				if(d.data.length>0){
	           				$(d.data).each(function(i,t){
	           					str+="<option value='"+t.id+"'>"+t.name+"</option>"
	           				})
          				}
          				$("#district").append(str);
          				form.render('select');
          			 }
          		});
           	});
          //监听提交
            form.on('submit(btnSub)', function(data){
            	var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            	$.ajax({
                  	 url:'<%=basePath%>residence/saveOrUpdate',
                  	 data: data.field,
                  	 type: 'post',
                  	 dataType: 'json',
                  	 success:function(data){
                  		if(data.se){
                  			layer.msg(data.msg, { offset: '15px',icon: 6,time: 1000}, function(){
    	                  		parent.layer.close(index);
    			            	parent.location.reload();
                      		});
                  		}else{
                  			layer.msg(data.msg, { offset: '15px',icon: 5,time: 1000}, function(){
    	                  		parent.layer.close(index);
    			            	parent.location.reload();
                      		});
                  		}
                  	 },error:function(data){
                  		layer.msg('操作失败', { offset: '15px',icon: 5,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                  		});
                  	 }
                 });
            	 
                 return false;
            });
          
        });
    </script>
</html>

