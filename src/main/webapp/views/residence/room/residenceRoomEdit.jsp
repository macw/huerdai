<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../../head.jsp" %>
</head>
<body>
 <form id="form" class="layui-form" style="margin-top: 20px;">
 	<input type="hidden" name="residenceroomId" value="${dto.residenceroomId}">
    <div class="layui-form-item">
   		<div class="layui-inline">
   			<label class="layui-form-label">选择民宿</label>
   			<div class="layui-input-inline" style="width: 190px;">
		        <select name="residenceId" id="residenceId" lay-filter="province" lay-verify="required" lay-search >
					<option value="">选择民宿</option>
					<c:forEach items="${residenceList}" var="r">
						<option value="${r.residenceId}" <c:if test="${dto.residenceId == r.residenceId}">selected</c:if> >${r.residenceName}</option>
					</c:forEach>
				</select>
		     </div>
   		</div>
    </div>
    <div class="layui-form-item">
   		 <div class="layui-inline">
		      <label class="layui-form-label">房间名称</label>
		      <div class="layui-input-inline">
		        <input type="text" name="residenceroomName" value="${dto.residenceroomName}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
   		<div class="layui-inline">
		      <label class="layui-form-label">房间价格</label>
		      <div class="layui-input-inline">
		        <input type="text" name="price" value="${dto.price}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">备注</label>
      <div class="layui-input-inline">
	       <textarea placeholder="备注" class="layui-textarea" name="memo" lay-verify="required" autocomplete="off" style="width: 190px;">${dto.memo}</textarea>
      </div>
    </div>
    <div class="layui-form-item">
	    <div class="layui-input-block">
	      <button type="submit" class="layui-btn" lay-submit="" lay-filter="btnSub" id="subBtn">立即提交</button>
	    </div>
  </div>
 </form>
</body>
 
   

<script type="text/javascript">
		var activce=null;
        layui.use(['table', 'form'], function () {
            var layer = layui.layer;
            var table = layui.table;
            var form = layui.form;
            
          //监听提交
            form.on('submit(btnSub)', function(data){
            	var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            	$.ajax({
                  	 url:'<%=basePath%>residence/room/saveOrUpdate',
                  	 data: data.field,
                  	 type: 'post',
                  	 dataType: 'json',
                  	 success:function(data){
                  		if(data.se){
                  			layer.msg(data.msg, { offset: '15px',icon: 6,time: 1000}, function(){
    	                  		parent.layer.close(index);
    			            	parent.location.reload();
                      		});
                  		}else{
                  			layer.msg(data.msg, { offset: '15px',icon: 5,time: 1000}, function(){
    	                  		parent.layer.close(index);
    			            	parent.location.reload();
                      		});
                  		}
                  	 },error:function(data){
                  		layer.msg('操作失败', { offset: '15px',icon: 5,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                  		});
                  	 }
                 });
            	 
                 return false;
            });
          
        });
    </script>
</html>

