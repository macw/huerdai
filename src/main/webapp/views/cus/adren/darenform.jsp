<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ include file="../../meta.jsp" %>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>添加用户</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="${basepath}statics/layuiadmin/layui/css/layui.css" media="all">
  <script type="text/javascript"  src="${basepath}statics/cookies.js"></script>
</head>
<body>

  <div class="layui-form" lay-filter="layuiadmin-form-useradmin" id="layuiadmin-form-useradmin" style="padding: 100px 80px;">
   
  
    <div class="layui-form-item">
      <label class="layui-form-label">达人姓名</label>
      <div class="layui-input-inline">
        <input type="text" name="darenTitle" lay-verify="required"  autocomplete="off" class="layui-input">
      </div>
    </div>
       <div class="layui-form-item">
	      <label class="layui-form-label">是否启用</label>
	      <div class="layui-input-block">
	       		 <select id="isPublish" name="isPublish" lay-filter="LAY-user-adminrole-type">
	            	 <option value="1">启用</option>
	              	 <option value="2">禁言</option>
	             </select>
	      </div>`
  	  </div>
    
    
    <div class="layui-form-item">
      <label class="layui-form-label">达人备注</label>
      <div class="layui-input-inline">
        <input type="text" name="reason" lay-verify="required"  autocomplete="off" class="layui-input">
      </div>
    </div>
    
    
    
    
    <div class="layui-form-item layui-hide">
      <input type="button" lay-submit lay-filter="LAY-user-front-submit" id="LAY-user-front-submit" value="确认">
    </div>
  </div>

  <script type="text/javascript"  src="${basepath}statics/layuiadmin/layui/layui.js"></script>  
  <script>
 
  
  layui.config({
	  base: '${pageContext.request.contextPath}/statics/layuiadmin/' //静态资源所在路径,
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index', 'form', 'upload','laydate'], function(){
    var $ = layui.$
    ,form = layui.form
    ,upload = layui.upload ;
    var laydate = layui.laydate;
   
    
  //常规用法
    laydate.render({
      elem: '#membirthdate'
    });
    
   /*  upload.render({
      elem: '#layuiadmin-upload-useradmin'
      ,url: layui.setter.base + 'json/upload/demo.js'
      ,accept: 'images'
      ,method: 'get'
      ,acceptMime: 'image/*'
      ,done: function(res){
        $(this.item).prev("div").children("input").val(res.data.src)
      }
    }); */
  })
  </script>
</body>
</html>