<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/25
  Time: 15:00
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../../head.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>秒杀管理</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>
<body>

<form id="addForm" class="layui-form" enctype="multipart/form-data">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0;">


        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">秒杀名称</label>
                <div class="layui-input-block">
                    <input name="seckillName" value="${sk.seckillName}" lay-verify="required" id="seckillName" placeholder="秒杀名称"
                           class="layui-input">
                    <input type="hidden" name="seckillId" id="seckillId"  value="${sk.seckillId}">
                </div>
            </div>
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">秒杀价</label>
                <div class="layui-input-block">
                    <input name="seckillPrice" value="${sk.seckillPrice}" lay-verify="required" id="seckillPrice" placeholder="秒杀价"
                           class="layui-input">
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <div class="layui-form-item">
                    <label class="layui-form-label">秒杀开始时间</label>
                    <div class="layui-input-block">
                        <input class="layui-input" name="seckillBegintime" autocomplete="off"
                               id="seckillBegintime" lay-verify="required" placeholder="开始时间"
                               value="${sk.begintime }">
                    </div>
                </div>
            </div>
            <div class="layui-input-inline" style="width: 300px">
                <div class="layui-form-item">
                    <label class="layui-form-label">秒杀结束时间</label>
                    <div class="layui-input-block">
                        <input class="layui-input" name="seckillEndtime" lay-verify="required" autocomplete="off" value="${sk.endtime}"
                               id="seckillEndtime" placeholder="结束时间">
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">类型</label>
                <div class="layui-input-block">
                    <select name="type" id="type" lay-verify="required" lay-filter="type">
                        <option value="">类型</option>
                        <option value="1" <c:if test="${sk.type==1}">selected</c:if>>商品</option>
                        <option value="0" <c:if test="${sk.type==0}">selected</c:if>>景点</option>
                    </select>
                </div>
            </div>
            <div class="layui-input-inline" id="Lay-goodsName" style="width: 300px;display: none">
                <label class="layui-form-label">商品名称</label>
                <div class="layui-input-block">
                    <select name="goodsId" id="goodsId" lay-filter="goodsId">
                        <option value="">请选择商品名称</option>
                        <c:forEach items="${goods}" var="l">
                            <option value="${l.goodsId }"
                                    <c:if test="${sk.goodsId == l.goodsId}">selected</c:if> >${l.goodsname }</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="layui-input-inline" id="Lay-spotName" style="width: 300px;display: none">
                <label class="layui-form-label">景点名称</label>
                <div class="layui-input-block">
                    <select name="goodsId" id="spotId" lay-filter="spotId">
                        <option value="">请选择景点名称</option>
                        <c:forEach items="${spot}" var="l">
                            <option value="${l.spotId }"
                                    <c:if test="${sk.goodsId == l.spotId}">selected</c:if> >${l.spotName }</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 600px;">
                <div class="layui-form-item">
                    <label class="layui-form-label">备注</label>
                    <div class="layui-input-block">
                        <input class="layui-input" name="memo" autocomplete="off"
                               id="memo"  placeholder="备注"
                               value="${sk.memo }">
                    </div>
                </div>
            </div>

        </div>

        <div class="layui-form-item" style="width: 600px">
            <div class="layui-input-block" style="text-align: right">
                <button type="submit" class="layui-btn" id="btnSub" lay-filter="btnSub" lay-submit>立即提交</button>
            </div>
        </div>
    </div>
</form>


<script type="text/javascript">
    layui.use(['form', 'layer', 'laydate'], function () {
        var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        var layer = layui.layer;
        var $ = layui.jquery;
        var laydate = layui.laydate;

        //日期时间选择器
        laydate.render({
            elem: '#seckillBegintime',
            type: 'datetime',
            format: "yyyy-MM-dd HH:mm:ss"
        });
        //日期时间选择器
        laydate.render({
            elem: '#seckillEndtime',
            type: 'datetime',
            format: "yyyy-MM-dd HH:mm:ss"
        });
        if (${sk.type==1}){
            $("#Lay-spotName").hide();
            $("#Lay-goodsName").show();
        }else if (${sk.type==0}) {
            $("#Lay-spotName").show();
            $("#Lay-goodsName").hide();
        }else {
            $("#Lay-goodsName").hide();
            $("#Lay-spotName").hide();
        }

        var seckillId = $("#seckillId").val();
        form.on('select(type)', function (data) {
            if (data.value == 0) {
                $("#Lay-spotName").show();
                $("#Lay-goodsName").hide();
            } else {
                $("#Lay-spotName").hide();
                $("#Lay-goodsName").show();
            }
        });


        //监听提交
        form.on('submit(btnSub)', function (data) {
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            var formData = new FormData(document.getElementById("addForm"));
            console.log(data.field);
            var spotId = $("#spotId").val();
            console.log(formData.spotId+"---"+spotId);
            $.ajax({
                url: '${pageContext.request.contextPath}/seckill/addOrUpdate?spotId='+spotId,
                data: formData,
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data.code == 200) {
                        layer.msg('操作成功', {
                            icon: 6,
                            time: 1000
                        }, function () {
                            parent.layer.close(index);
                            parent.layui.table.reload('Lay_back_table', {});
                        });
                    } else {
                        layer.msg('上传失败', {
                            icon: 5,
                            time: 1000
                        })
                    }

                },
                error: function (data) {
                    layer.msg('操作失败', {
                        icon: 5,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                }
            });
            return false;
        });
    });

</script>
</body>
</html>


