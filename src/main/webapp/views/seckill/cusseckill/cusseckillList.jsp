<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/29
  Time: 9:20
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../../head.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>查看拼团人数详情</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
</head>
<body>

<div class="layui-card-body">
    <%--  <div class="layui-form">
          <div style="padding-bottom: 10px;" id="LAY_lay_add">
            &lt;%&ndash;  <button type="button" class="layui-btn layui-btn-danger" id="doMultiDelete">
                  <i class="layui-icon layui-icon-delete"></i> 批量删除
              </button>
              <button class="layui-btn layuiadmin-btn-role " data-type="add" id="add">
                  <i class="layui-icon layui-icon-add-circle-fine"></i> 添加
              </button>&ndash;%&gt;

              <div class="layui-input-inline" style="width: 200px;">
                  <input type="text" name="seckillName"  placeholder="请输入秒杀名称" autocomplete="off"
                         class="layui-input">
              </div>

              <div class="layui-input-inline" style="width: 100px;">
                  <button type="button" class="layui-btn layui-btn-normal" lay-filter="search" lay-submit>
                      <i class="layui-icon layui-icon-search"></i> 搜索
                  </button>
              </div>
          </div>

      </div>--%>
    <table id="Lay_back_table" lay-filter="Lay_back_table"></table>

</div>

<script type="text/html" id="updateAndDelete">

    <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">
        <i class="layui-icon layui-icon-edit"></i>修改
    </button>
    <button type="button" class="layui-btn layui-btn-danger" lay-event="del">
        <i class="layui-icon layui-icon-delete"></i>删除
    </button>

</script>

<script type="text/javascript">
    layui.use(['table', "layer", 'form'], function () {
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var $ = layui.jquery;

        var layTab = table.render({
            elem: '#Lay_back_table',
            url: '${pageContext.request.contextPath}/cusseckill/selectAll?seckillId=' +${seckillId}, //数据接口
            page: true,
            limit: 5,
            limits: [5, 10, 20, 30],
            // width: 'auto',
            autoSort: true,
            initSort: {
                field: 'creationDate' //排序字段，对应 cols 设定的各字段名
                , type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
            },
            //toolbar: "#LAY_lay_add",
            cols: [[
                {type: "checkbox"},
                {
                    title: "团组", field: "seckillgroup", sort: true, width: "150", align: "center"
                    /* ,
                     templet:function (d) {
                         if (d.seckillgroup!=null){
                             return "拼团组";
                         }
                     }*/
                },
                {title: "拼团名称", field: "seckillName", sort: true, width: "100", align: "center"},
                {title: "该团用户昵称", field: "nickname", sort: true, width: "150", align: "center"},

                {
                    title: "备注", field: "memo", sort: true, width: "100", align: "center",
                    templet: function (d) {
                        if (d.memo == "1") {
                            return "团长";
                        } else if (d.memo == "0") {
                            return "团员";
                        } else if (d.memo == "2") {
                            return "已拼满";
                        } else {
                            return "";
                        }
                    }
                },
                {title: "创建人", field: "createdName", width: "100", align: "center"},
                {
                    title: "创建时间",
                    field: "creationDate", align: "center", sort: true, width: "125",
                    templet: '<div>{{# if(d.creationDate!=null){ }} {{ layui.util.toDateString(d.creationDate,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'
                },
                {title: "更新人", field: "lastUpdateName", width: "100", align: "center"},
                {
                    title: "更新时间",
                    field: "lastUpdateDate", align: "center", width: "125", sort: true,
                    templet: '<div>{{# if(d.lastUpdateDate!=null){ }} {{ layui.util.toDateString(d.lastUpdateDate,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'
                },
                {
                    title: "状态", align: "center", width: 100,
                    templet: function (d) {
                        if (d.status != null && d.status == 1) {
                            return '<button type="button" class="layui-btn layui-btn-normal" lay-event="yn">启用</button>';
                        } else if (d.status != null && d.status == 0) {
                            return '<button type="button" class="layui-btn layui-btn-danger" lay-event="yn">禁用</button>';
                        }
                        return '';
                    }
                }/*,
                {title: "操作", templet: "#updateAndDelete",width: "220", align: "center"}*/
            ]]
            /* ,
             //合并列方法
             done: function (res, curr, count) {
             merge(res, curr, count);
         }*/
        });


        /**
         * 合并单元格,有问题，暂时不用
         * @param res 表格数据
         * @param curr 当前页
         * @param count 总数
         */
        function merge(res, curr, count) {
            var data = res.data;
            var mergeIndex = 0;//定位需要添加合并属性的行数
            var mark = 1; //这里涉及到简单的运算，mark是计算每次需要合并的格子数
            var columsName = ['seckillgroup'];//需要合并的列名称
            var columsIndex = [1];//需要合并的列索引值

            for (var k = 0; k < columsName.length; k++)//这里循环所有要合并的列
            {
                var trArr = $(".layui-table-body>.layui-table").find("tr");//所有行
                for (var i = 1; i < res.data.length; i++) { //这里循环表格当前的数据
                    var tdCurArr = trArr.eq(i).find("td").eq(columsIndex[k]);//获取当前行的当前列
                    var tdPreArr = trArr.eq(mergeIndex).find("td").eq(columsIndex[k]);//获取相同列的第一列

                    if (data[i][columsName[k]] === data[i - 1][columsName[k]]) { //后一行的值与前一行的值做比较，相同就需要合并
                        mark += 1;
                        tdPreArr.each(function () {//相同列的第一列增加rowspan属性
                            $(this).attr("rowspan", mark);
                        });
                        tdCurArr.each(function () {//当前行隐藏
                            $(this).css("display", "none");
                        });
                    } else {
                        mergeIndex = i;
                        mark = 1;//一旦前后两行的值不一样了，那么需要合并的格子数mark就需要重新计算
                    }
                }
            }
        }


        //刷新表格方法
        function tableReload() {
            table.reload('Lay_back_table', {});
        };

        //监听行工具事件
        table.on('tool(Lay_back_table)', function (obj) {
            var data = obj.data;
            if (obj.event === 'edit') {
                layer.open({
                    title: "修改",
                    content: "${pageContext.request.contextPath}/seckill/toseckillEdit?aid=" + data.seckillId,
                    type: 2,
                    maxmin: true,
                    area: ['700px', '600px'],
                    end: function () {
                        window.location.reload();
                    }
                });
            }
            /* else if (obj.event === 'yn') {
                 var str = obj.data.status == 0 ? "启用" : "禁用";
                 var state = obj.data.status == 1 ? 0 : 1;
                 layer.confirm('确认要[' + str + ']这个秒杀吗?', function (index) {
                     $.ajax({
                         url: '${pageContext.request.contextPath}/seckill/addOrUpdate',
                        data: {"seckillId": data.seckillId, "status": state},
                        type: 'post',
                        dataType: 'json',
                        success: function (data) {
                            tableReload();
                            layer.msg("操作成功", {time: 1000, icon: 6});
                        }, error: function (data) {
                            tableReload();
                            layer.msg("操作失败", {time: 1000, icon: 5});
                        }
                    });
                    layer.close(index);
                });
            }*/

            else if (obj.event === "del") {
                alert(data.seckillId);
                layer.confirm('确定要删除吗？', {icon: 3, title: '确认删除'}, function (index) {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/seckill/deleteOne",
                        data: "aid=" + data.seckillId,
                        success: function (data) {
                            layer.alert(data.msg, {time: 3000});
                            table.reload("Lay_back_table");
                        },
                        error: function (data) {
                            table.reload("Lay_back_table");
                            layer.msg("操作失败", {time: 1000, icon: 5});
                        }
                    });
                });
            }
        });

        //监听搜索
        form.on('submit(search)', function (data) {
            var field = data.field;
            // console.log(field);
            //执行重载
            layTab.reload({
                where: field
            });
        });
        //打开添加窗口
        $("#add").click(function () {
            layer.open({
                title: "添加秒杀信息",
                content: "${pageContext.request.contextPath}/seckill/toseckillEdit",
                type: 2,
                // maxmin: true,
                area: ['700px', '600px'],
                end: function () {
                    window.location.reload();
                }

            });
        });
        //批量删除方法
        $("#doMultiDelete").click(function () {
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据?", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].seckillId;
                    }
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/seckill/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert(data.msg, {time: 3500});
                            //刷新table
                            table.reload("Lay_back_table");
                        }
                    })
                });
            }
        })

    });


</script>
</body>
</html>




