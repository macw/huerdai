<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>利润分成管理</title>
	<%@ include file="../head.jsp" %>
	 <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>
</head>
<body>
<div class="layui-fluid">
    <div class="layui-card-body layui-form">
        <div  style="padding-bottom: 10px;">
            <div class="layui-input-inline" style="width: 100px;">
                 <button type="button" class="layui-btn addMerch" data-type="addMerch" onclick="addMerch()">添加</button>
            </div>
            <div class="layui-input-inline" style="width: 200px;">
				<select name="type" id="type">
					<option value="">用户类型</option>
					<option value="1">普通用户</option>
					<option value="2">会员</option>
				</select>
			</div>
            <div class="layui-input-inline" style="width: 200px;">
				<select name="status" id="status">
					<option value="">商品类型</option>
					<option value="1">旅游</option>
					<option value="2">商品</option>
				</select>
			</div>
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>
        <table id="profitsaring-tab" lay-filter="profitsaring-tab"></table>
    </div>
</div>
</body>
 
  <script type="text/html" id="tool">
        <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">编辑</button>
        <button type="button" class="layui-btn layui-btn-danger" lay-event="del">删除</button>
  </script>

<script type="text/javascript">

        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            table.render({
                elem: '#profitsaring-tab',
                url: '<%=basePath%>profitsaring/selectData', //数据接口
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                cols: [[
                    {type: "checkbox"},
                    {title: "用户类型",align:"center",width:120,
                    	templet: function(d){
                    		if(d.type!=null && d.type==1){
	                    		return '普通用户';
                    		}else if(d.type!=null && d.type==2){
                    			return '会员';
                    		}
                    		return '';
                    	}
                    },
                    {title: "商品类型",align:"center",width:120,
                    	templet: function(d){
                    		if(d.status!=null && d.status==1){
	                    		return '旅游商品';
                    		}else if(d.status!=null && d.status==2){
                    			return '商品';
                    		}
                    		return '';
                    	}
                    },
                    {title: "代理商利润分成百分比", field: "agentPercentage",align:"center"},
                    {title: "平台利润分成百分比", field: "platformPercentage",align:"center"},
                    {title: "用户利润分成百分比", field: "pPercentage",align:"center"},
                    {title: "上级利润分成百分比", field: "parentPPercentage",align:"center"},
                   /*  {title: "创建人", field: "createdName",align:"center",width:90},
                    {title: "创建时间",align:"center",
                    	templet: function(d){
                    		if(d.creationDate!=null){
	                    		return layui.util.toDateString(d.creationDate);
                    		}
                    		return '';
                    	}
                    },
                    {title: "修改人", field: "lastUpdateName",align:"center",width:90},
                    {title: "最近修改时间",align:"center",
                    	templet: function(d){
                    		if(d.lastUpdateDate!=null){
	                    		return layui.util.toDateString(d.lastUpdateDate);
                    		}
                    		return '';
                    	}
                    }, 
                    {title: "状态", width :100,align:"center",
                    	templet: function(d){
	                    	if(d.status == 1){
	                    		return '<button type="button" class="layui-btn  layui-btn-normal"  lay-event="yn">启用</button>';
	                    	}else{
	                    		return '<button type="button" class="layui-btn  layui-btn-danger"  lay-event="yn">禁用</button>';
	                    	}
                   		}
                	},*/
                    {title: "操作",width :175, align:"center", templet: "#tool"}
                ]]
            });
            
            
          	//监听行工具事件
            table.on('tool(profitsaring-tab)', function(obj){
              var data = obj.data;
              if(obj.event === 'yn'){
            	  var str = "启用";
            	  if(data.status == 1){
            		  str = "禁用";
            	  }
                  layer.confirm('确认要['+str+']这条利润分成吗?', function(index){
                     $.ajax({
                    	 url:'<%=basePath%>profitsaring/updateYnByPsId',
                    	 data: {"psId":data.psId},
                    	 type: 'post',
                    	 dataType: 'json',
                    	 success:function(data){
                    		 tableReload();
                    		 if(data.se){
	                    		 layer.msg(data.msg, {time: 1000, icon:6});
                    		 }else{
	                    		 layer.msg(data.msg, {time: 1000, icon:5});
                    		 }
                    	 },error:function(data){
                    		 tableReload();
                    		 layer.msg("操作失败", {time: 1000, icon:5});
                    	 }
                     });
                    	layer.close(index);
                  });
              }else if(obj.event === 'del'){
                layer.confirm('真的删除这条记录吗?', function(index){
                	$.ajax({
                   	 url:'<%=basePath%>profitsaring/deleteBySId',
                   	 data: {"sId":data.sid},
                   	 type: 'post',
                   	 dataType: 'json',
                   	 success:function(data){
                   		 tableReload();
                   		 if(data.se){
                    		 layer.msg(data.msg, {time: 1000, icon:6});
                		 }else{
                    		 layer.msg(data.msg, {time: 1000, icon:5});
                		 }
                   	 },error:function(data){
                   		 tableReload();
                   		layer.msg("操作失败", {time: 1000, icon:5});
                   	 }
                    });
                  	layer.close(index);
                });
              } else if(obj.event === 'edit'){
            	  layer.open({
             		  type: 2,
             		  title: '编辑利润分成',
             		  shadeClose: true,
             		  shade: 0.8,
             		  area: ['800px', '450px'],
             		  content: '<%=basePath%>profitsaring/editPage?psId='+data.psId
              	});  
              }
            });
        });
        //搜索操作
        function doSearch() {
            //发送请求，并且接收数据
            layui.use('table', function () {
                var table = layui.table;
                table.reload('profitsaring-tab', {
                    where: {"type": $("#type").val(),"status": $("#status").val()},
                    page: {
                        curr: 1 //重新从第 1 页开始
                      }
                });
            });
        };
        function tableReload(){
       	   layui.use('table', function () {
              var table = layui.table;
              table.reload('profitsaring-tab', {
              });
           });
        };
        function addMerch(){
        	layui.use('layer', function(){
        		layer.open({
             		  type: 2,
             		  title: '添加利润分成',
             		  shadeClose: true,
             		  shade: 0.8,
             		  area: ['800px', '450px'],
             		  content: '<%=basePath%>profitsaring/editPage'
              	});   
       		}); 
        };
        
        
        
    </script>
</html>

