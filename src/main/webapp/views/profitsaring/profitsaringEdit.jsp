<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>利润分成管理</title>
	<%@ include file="../head.jsp" %>
</head>
<body>
 <form class="layui-form" style="margin-top: 20px;">
 	<input type="hidden" name="psId" value="${dto.psId}">
    <div class="layui-form-item">
	    <div class="layui-inline">
		      <label class="layui-form-label">用户类型</label>
		      <div class="layui-input-inline" style="width: 200px;">
				<select name="type" id="type">
					<option value="">用户类型</option>
					<option value="1" <c:if test="${dto.type ==1}">selected</c:if>>普通用户</option>
					<option value="2" <c:if test="${dto.type ==2}">selected</c:if>>会员</option>
				</select>
			</div>
	    </div>
	    <div class="layui-inline">
	    	  <label class="layui-form-label">商品类型</label>
		      <div class="layui-input-inline" style="width: 200px;">
				<select name="status" id="status">
					<option value="">商品类型</option>
					<option value="1" <c:if test="${dto.status ==1}">selected</c:if>>旅游</option>
					<option value="2" <c:if test="${dto.status ==2}">selected</c:if>>商品</option>
				</select>
			</div>
	    </div>
    </div>
    <div class="layui-form-item">
	    <div class="layui-inline">
		      <label class="layui-form-label">代理商利润分成百分比</label>
		      <div class="layui-input-inline">
		        <input type="text" name="agentPercentage" value="${dto.agentPercentage}" lay-verify="required" autocomplete="off" class="layui-input">
			  </div>
	    </div>
	    <div class="layui-inline">
		      <label class="layui-form-label">平台利润分成百分比</label>
		      <div class="layui-input-inline">
		        <input type="text" name="platformPercentage" value="${dto.platformPercentage}" lay-verify="required" autocomplete="off" class="layui-input">
			  </div>
	    </div>
    </div>
    <div class="layui-form-item">
	    <div class="layui-inline">
		      <label class="layui-form-label">用户利润分成百分比</label>
		      <div class="layui-input-inline">
		        <input type="text" name="pPercentage" value="${dto.pPercentage}" lay-verify="required" autocomplete="off" class="layui-input">
			  </div>
	    </div>
	    <div class="layui-inline">
		      <label class="layui-form-label">上级利润分成百分比</label>
		      <div class="layui-input-inline">
		        <input type="text" name="parentPPercentage" value="${dto.parentPPercentage}" lay-verify="required" autocomplete="off" class="layui-input">
			  </div>
	    </div>
    </div>
    
    <div class="layui-form-item">
	    <div class="layui-input-block">
	      <button type="submit" class="layui-btn" lay-submit="" lay-filter="btnSub">立即提交</button>
	    </div>
  </div>
 </form>
</body>
 
   

<script type="text/javascript">
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
          //监听提交
            form.on('submit(btnSub)', function(data){
           		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            	$.ajax({
                  	 url:'<%=basePath%>profitsaring/saveOrUpdate',
                  	 data: data.field,
                  	 type: 'post',
                  	 dataType: 'json',
                  	 success:function(data){
                  		 if(data.se){
                  			layer.msg(data.msg, { offset: '15px',icon: 6,time: 1000}, function(){
                  				parent.layer.close(index);
        		            	parent.location.reload();
                  			});
                  		 }else{
                  			layer.msg(data.msg, { offset: '15px',icon: 5,time: 1000}, function(){
                  				parent.layer.close(index);
        		            	parent.location.reload();
                  			});
                  		 }
                  		
                  	 },error:function(data){
                  		layer.msg('操作失败', { offset: '15px',icon: 5,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                  		});
                  	 }
                 });
            	 
                 return false;
            });
        });
        
        
    </script>
</html>

