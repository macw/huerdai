<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/11/25
  Time: 11:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../head.jsp" %>
<html>
<head>
    <title>编辑角色信息</title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <%-- <script type="text/javascript" src="${basepath}eleTree/layui/layui.js"></script>
     <link rel="stylesheet" href="${basepath}eleTree/eleTree.css" media="all">
     <script type="text/javascript" src="${basepath}/eleTree/layui/lay/mymodules/eleTree.js"></script>
 --%>
</head>
<body>

<%--弹出层--%>

<form id="addForm" class="layui-form">
    <div class="layui-form" lay-filter="layuiadmin-form-role" id="layuiadmin-form-role"
         style="padding: 20px 30px 0 0">
        <div class="layui-form-item" style="width: 350px">
            <label class="layui-form-label">角色</label>
            <div class="layui-input-block">
                <input type="hidden" name="roleId" id="roleId" value="${role.roleId}">
                <input name="roleName" id="roleName" value="${role.roleName}" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">权限范围</label>
            <%--树形菜单--%>
            <%-- <input id="hmfunid" name="hmfunid" hidden="hidden">
             <div class="layui-inline eleTree ele5" lay-filter="data5" id="roleTree" name="roleTree"></div>
 --%>
            <div class="layui-input-block">
                <c:forEach items="${menu}" var="m">
                    <input type="checkbox" name="list" class="${m.menuId}" value="${m.menuId}"
                           lay-filter="allChoose"
                           title="${m.menuName}">
                    <div class="layui-input-block">
                        <c:forEach items="${m.menuList}" var="l">
                            <input type="checkbox" name="list" class="${m.menuId}"
                                   lay-filter="childChoose"
                                   value="${l.menuId}" title="${l.menuName}">
                        </c:forEach>
                    </div>
                    <br/>
                </c:forEach>
            </div>
        </div>
        <div class="layui-form-item" style="width: 500px">
            <label class="layui-form-label">具体描述</label>
            <div class="layui-input-block">
            <textarea type="text" name="remark" id="remark" lay-verify="required" autocomplete="off"
                      class="layui-textarea">${role.remark}</textarea>
            </div>
        </div>

        <div class="layui-form-item" style="width: 500px">
            <div class="layui-input-block" style="text-align: right">
                <button type="submit" class="layui-btn" id="btnSub" lay-filter="btnSub" lay-submit>立即提交</button>
            </div>
        </div>

    </div>

</form>

<script>
    $(function () {
        var boxObj = $("input:checkbox[name='list']");  //获取所有的复选框
        var expresslist = '${au}'; //用el表达式获取在控制层存放的复选框的值为字符串类型
        var express = expresslist.replace("[", "").replace("]", "").split(','); //去掉它们之间的分割符“，”
        for (var i = 0; i < boxObj.length; i++) {
            for (var j = 0; j < express.length; j++) {
                if (boxObj[i].value == parseInt(express[j]))  //如果值与修改前的值相等
                {
                    boxObj[i].checked = true;
                    break;
                }
            }
        }
    })
</script>

<script type="text/javascript">
    layui.config({
        base: '${pageContext.request.contextPath}/layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'useradmin', 'table', 'jquery', 'form'], function () {
        var form = layui.form;
        var table = layui.table;
        var $ = layui.jquery;
        var form = layui.form;
        var layer = layui.layer;

        //点击 父级的 全选, 勾选
        form.on('checkbox(allChoose)', function (data) {
            var pc = data.elem.classList; //获取选中的checkbox的class属性
            var dd = data.elem.id;//获取选中的checkbox的id属性
            var child = $("." + pc);
            child.each(function (index, item) {
                item.checked = data.elem.checked;
            });
            form.render('checkbox');
        });

        //点击 子级 选中父级
        form.on('checkbox(childChoose)', function (data) {
            var pc = data.elem.classList; //获取选中的checkbox的class属性
            var child = $("." + pc);
            child.each(function (index, item) {
                if (index == 0) {
                    if (data.elem.checked==true){
                        item.checked = data.elem.checked;
                    }

                }
            });
            form.render('checkbox');
        });

        //监听提交
        form.on('submit(btnSub)', function (data) {
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            var formData = new FormData(document.getElementById("addForm"));
            console.log(data.field);

            //获取 复选框checkbox[name='list']的值
            var arr = new Array();
            $("input:checkbox[name='list']:checked").each(function (i) {
                arr[i] = $(this).val();
            });
            // alert("arr=="+arr.length);
            console.log(arr);
            console.log("formData==" + formData);
            var rid = $("#roleId").val();
            $.ajax({
                <%--url: '${pageContext.request.contextPath}/role/addOrUpdateRole?ids=' + arr,--%>
                url: '${pageContext.request.contextPath}/role/addOrUpdateRole',
                data: "ids=" + arr + "&roleId=" + rid + "&roleName=" + $("#roleName").val() + "&remark=" + $("#remark").val(),
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    if (data.code == 200) {
                        layer.msg('操作成功', {
                            icon: 6,
                            time: 1000
                        }, function () {
                            parent.layer.close(index);
                            parent.layui.table.reload('Lay_back_table', {});
                        });
                    } else {
                        layer.msg(data.msg, {
                            icon: 5,
                            time: 1000
                        })
                    }

                },
                error: function (data) {
                    layer.msg('操作失败', {
                        icon: 5,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                }
            });
            return false;
        });


    });


</script>


</body>
</html>
