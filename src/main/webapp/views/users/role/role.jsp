<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../../head.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>角色管理</title>
   <%-- <script type="text/javascript" src="${basepath}eleTree/layui/layui.js"></script>
    <link rel="stylesheet" href="${basepath}eleTree/eleTree.css" media="all">
    <script type="text/javascript" src="${basepath}/eleTree/layui/lay/mymodules/eleTree.js"></script>
--%>

</head>
<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-form layui-card-header layuiadmin-card-header-auto">
            <div style="padding-bottom: 10px;" id="LAY_lay_add">
                <button type="button" class="layui-btn layui-btn-danger" id="doMultiDelete">
                    <i class="layui-icon layui-icon-delete"></i> 批量删除
                </button>
                <button class="layui-btn layuiadmin-btn-role " <%--data-type="add"--%> id="toOpenAddLayer">
                    <i class="layui-icon layui-icon-add-circle-fine"></i> 添加
                </button>
                <div class="layui-input-inline" style="width: 200px;">
                    <input type="text" name="search" id="Lay_toSearch_input" placeholder="请输入角色名" autocomplete="off"
                           class="layui-input">
                </div>
                <div class="layui-input-inline" style="width: 100px;">
                    <button type="button" class="layui-btn layui-btn-normal" id="doSearch">
                        <i class="layui-icon layui-icon-search"></i> 搜索
                    </button>
                </div>
            </div>
        </div>
        <div class="layui-card-body">

            <table id="LAY-user-back-role" lay-filter="LAY-user-back-role"></table>


        </div>
    </div>
</div>

<script type="text/html" id="updateAndDelete">

    <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">
        <i class="layui-icon layui-icon-edit"></i>编辑权限
    </button>
    <button type="button" class="layui-btn layui-btn-danger" lay-event="del">
        <i class="layui-icon layui-icon-delete"></i>删除角色
    </button>

</script>
<script type="text/javascript">
    function writeObj(obj) {
        var description = "";
        for (var i in obj) {
            var property = obj[i];
            description += i + " = " + property + "\n";
        }
        alert(description);
    }

    layui.use(['table', 'form', ], function () {
        var form = layui.form;
        var table = layui.table;
        var $ = layui.jquery;
        var layer = layui.layer;

        table.render({
            elem: '#LAY-user-back-role',
            url: '${pageContext.request.contextPath}/role/selectAll', //数据接口
            page: true,
            limit: 10,
            limits: [10, 20, 30],
            cols: [[
                {type: "checkbox"},
                {title: "角色名", field: "roleName",align:"center"},
                {title: "描述", field: "remark",align:"center"},
                {title: "操作", templet: "#updateAndDelete", align: "center"}
            ]]
        });

        //监听行工具事件
        table.on('tool(LAY-user-back-role)', function (obj) {
            var data = obj.data;
            // alert(data.roleId);
            if (obj.event === 'edit') {
                layer.open({
                    title: "修改",
                    content: "${pageContext.request.contextPath}/role/toRoleEdit?roleId="+data.roleId,
                    type: 2,
                    maxmin: true,
                    area: ['700px', '700px'],
                    end: function () {
                        window.location.reload();
                    }
                });
            }

            else if (obj.event === "del"){
                layer.confirm('确定要删除吗？', {icon: 3, title: '确认删除'}, function (index) {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/role/deleteRole",
                        data: "roleId=" + data.roleId,
                        success: function (data) {
                            layer.alert(data.msg, {time: 3000});
                            table.reload("LAY-user-back-role");
                            layer.close(index);
                        }
                    })
                });
            }
        });

        $("#toOpenAddLayer").click(function () {
            layer.open({
                title: "添加管理员",
                content: "${pageContext.request.contextPath}/role/toRoleEdit",
                type: 2,
                maxmin: true,
                area: ['700px', '700px'],
                end: function () {
                    window.location.reload();
                }
            });
        });
        $("#doMultiDelete").click(function () {
            //获取到选中的数据
            var checkStatus = table.checkStatus('LAY-user-back-role'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].roleId;
                    }
                    console.log("ids==="+ids);
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/role/deleteMany",
                        data: "roleId=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert(data.msg, {time: 3500});
                            //刷新table
                            table.reload("LAY-user-back-role");
                        }
                    })
                });
            }
        })

    });




    //搜索角色
    function doSearch() {
        //1.获取到输入框中输入的内容
        var searchRoleName = $('#Lay_toSearch_input').val();
        //发送请求，并且接收数据
        layui.use('table', function () {
            var table = layui.table;
            table.reload('LAY-user-back-role', {
                where: {"roleName": searchRoleName}
            });
        });
    }



</script>
</body>
</html>

