<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/9/29
  Time: 13:25
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../meta.jsp" %>
<html>
<head>
    <title>功能资源管理</title>
    <link rel="stylesheet" href="${basepath}layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="${basepath}layuiadmin/modules/treetable-lay/treetable.css">
    <script type="text/javascript" src="${basepath}js/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="${basepath}js/cookies.js"></script>
    <script type="text/javascript" src="${basepath}layuiadmin/layui/layui.js"></script>
    <script type="text/javascript" src="${basepath}layuiadmin/modules/treetable-lay/treetable.js"></script>
    <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>

</head>
<body>
<div class="layui-card-body">
    <div style="padding-bottom: 10px;" id="LAY_lay_add">
       <%-- <button type="button" class="layui-btn layui-btn-danger" id="delMany" &lt;%&ndash;onclick="doMultiDelete()"&ndash;%&gt;>
            <i class="layui-icon layui-icon-delete"></i> 批量删除
        </button>--%>
        <button class="layui-btn layuiadmin-btn-role " data-type="add" id="add" <%--onclick="toOpenAddLayer()"--%>>
            <i class="layui-icon layui-icon-add-circle-fine"></i> 添加
        </button>
        &nbsp;
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="search" id="Lay_toSearch_input" placeholder="请输入分类名称" autocomplete="off"
                   class="layui-input">
        </div>
        <div class="layui-input-inline" style="width: 100px;">
            <button type="button" class="layui-btn layui-btn-normal" id="btn-search"<%-- onclick="doSearch()"--%>>
                <i class="layui-icon layui-icon-search"></i> 搜索
            </button>
        </div>
        &nbsp;
        <div class="layui-btn-group">
            <button class="layui-btn" id="btn-expand">全部展开</button>
            <button class="layui-btn" id="btn-fold">全部折叠</button>
            <button class="layui-btn" id="btn-refresh">刷新表格</button>
        </div>

    </div>

    <%--树形表格--%>
    <table class="layui-table" id="Lay_category_treeTable" lay-filter="Lay_category_treeTable"></table>


</div>

<script type="text/html" id="updateAndDelete">
    <button type="button" class="layui-btn  layui-btn-normal"
            lay-event="edit" <%--onclick="toOpenUpdateLayer('{{d.classId}}')"--%>>
        <i class="layui-icon layui-icon-edit"></i>修改
    </button>
    <button type="button" class="layui-btn layui-btn-danger"
            lay-event="del" <%--onclick="doDelete('{{d.classId}}')"--%>>
        <i class="layui-icon layui-icon-delete"></i> 删除
    </button>
</script>

<%--弹出层--%>

<form id="addForm" class="layui-form">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0;display: none">
        <div class="layui-form-item">
            <label class="layui-form-label">菜单名称</label>
            <div class="layui-input-block">
                <input name="menuName" id="menuName" class="layui-input">
                <input name="menuId" id="menuId" lay-type="hide" type="hidden" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">菜单url</label>
            <div class="layui-input-block">
                <input name="menuUrl" id="menuUrl" class="layui-input">
            </div>
        </div>


        <div class="layui-form-item" id="Lay_select_Level">
            <label class="layui-form-label">请选择菜单级别</label>
            <div class="layui-input-block">
                <select name="level" lay-verify="required" lay-filter="classIdLevel">
                    <option value=""></option>
                    <option value="1" selected>一级菜单</option>
                    <option value="2">二级分类</option>
                </select>
            </div>
        </div>


        <div class="layui-form-item" style="display: none" id="Lay_One_Level">
            <label class="layui-form-label">请选择所属的一级菜单列表</label>
            <div class="layui-input-block">
                <select name="parentId" id="parentId" lay-filter="parent_classIdLevel_One">
                    <option value=""></option>
                </select>
            </div>
        </div>


        <div class="layui-form-item" style="text-align: right">
            <button class="layui-btn " lay-submit lay-filter="LAY-sysconfig-submit" id="LAY-sysconfig-submit">确认添加
            </button>
            <button lay-submit lay-filter="updateSubmitBtn" class="layui-btn" id="updateSubmitBtn">确认修改</button>

        </div>
    </div>
</form>


<script type="text/javascript">

    layui.config({
        base: '${pageContext.request.contextPath}/layuiadmin/modules/' //   资源所在路径
    }).extend({
        treetable: 'treetable-lay/treetable'
    }).use(['treetable', 'table', 'form', 'layer'], function () {
        var treetable = layui.treetable;
        var layer = layui.layer;
        var table = layui.table;
        var form = layui.form;
        var $ = layui.jquery;

        var re;

        // 渲染表格
        var renderTable = function () {
            layer.load(3);
            re = treetable.render({
                elem: '#Lay_category_treeTable',
                url: '${basepath}menu/selectAllList',
                treeColIndex: 1,          // 树形图标显示在第几列
                treeSpid: 0,             // 最上级的父级id
                treeIdName: 'menuId',       // 	id字段的名称
                treePidName: 'parentId',    // 	pid字段的名称
                treeDefaultClose: true,     //是否默认折叠
                page: false,
                //treeLinkage: true,      //父级展开时是否自动展开所有子级
                cols: [[
                    {type: 'numbers'},
                    {title: "菜单名称", field: "menuName", style: "text-align:left"},
                    {title: "菜单url", field: "menuUrl",align:"center"},
                    //  {title: "菜单级别", field: "level"},
                    {
                        title: "按钮",align:"center", templet: function (d) {
                            // alert(d.level);
                            if (d.level == 2) {
                                return '<button type="button" class="layui-btn  layui-btn-radius"  lay-event="detailOperation">  <i class="layui-icon">&#xe857;</i>查看/编辑 按钮</button>';
                            } else {
                                return '';
                            }
                        }
                    },
                    {
                        title: "菜单状态", width: 100, align: "center",
                        templet: function (d) {
                            if (d.menuStatus == 1) {
                                return '<button type="button" class="layui-btn  layui-btn-normal"  lay-event="yn">启用</button>';
                            } else {
                                return '<button type="button" class="layui-btn  layui-btn-danger"  lay-event="yn">禁用</button>';
                            }
                        }
                    },
                    {title: "操作", templet: "#updateAndDelete",align:"center"}
                ]],
                done: function () {
                    layer.closeAll('loading');
                }

            })
        };

        //监听行工具事件
        table.on('tool(Lay_category_treeTable)', function (obj) {
            var data = obj.data;
            if (obj.event === 'yn') {
                var str = obj.data.menuStatus == 0 ? "启用" : "禁用";
                var state = obj.data.menuStatus == 1 ? 0 : 1;
                layer.confirm('确认要[' + str + ']这个菜单吗?', function (index) {
                    $.ajax({
                        url: '${basepath}menu/updateStatus',
                        data: {"menuId": data.menuId, "menuStatus": state},
                        type: 'post',
                        dataType: 'json',
                        success: function (data) {
                            renderTable();
                            layer.msg("操作" + data.msg, {time: 3000, icon: 6});
                        }, error: function (data) {
                            renderTable();
                            layer.msg("操作失败", {time: 3000, icon: 5});
                        }
                    });
                    layer.close(index);
                });
                //执行编辑修改操作
            } else if (obj.event === 'edit') {
                // alert(data.menuId);
                $.ajax({
                    url: "${basepath}menu/selectOne",
                    data: "aid=" + data.menuId,
                    success: function (data) {
                        $("#menuId").val(data.menuId);
                        $("#menuName").val(data.menuName);
                        $("#menuUrl").val(data.menuUrl);
                    }
                });
                $("#Lay_select_Level").hide();
                layer.open({
                    title: "修改配置",
                    content: $("#layuiconfig-form-role"),
                    type: 1,
                    maxmin: true,
                    area: ['350px', '280px'],
                    end: function () {
                        window.location.reload();
                    }
                });

                $("#LAY-sysconfig-submit").hide();
                $("#updateSubmitBtn").show();

                //3.提交表单
                form.on("submit(updateSubmitBtn)", function (data) {
                    // console.log(data);
                    $.ajax({
                        url: "${basepath}menu/updateMenu",
                        data: data.field,
                        type: "post",
                        //4.接收后台修改响应回来的数据；关闭弹出层、提示修改信息、刷新table
                        success: function (data) {
                            //1.关闭掉添加弹出层
                            layer.closeAll('page');
                            //2.提示修改成功
                            layer.alert("修改" + data.msg, {time: 3000});
                            //刷新table
                            //table.reload("Lay_category_treeTable");
                            renderTable();
                        },
                        error: function (data) {
                            renderTable();
                            layer.msg("操作失败", {time: 2000, icon: 5});
                        }
                    });
                    return false;//阻止跳转；
                })
            }
            //执行删除单个方法
            else if (obj.event === 'del') {
                layer.confirm('确定要删除吗？', {icon: 3, title: '确认删除'}, function (index) {
                    $.ajax({
                        url: "${basepath}menu/deleteOne",
                        data: "aid=" + data.menuId,
                        success: function (data) {
                            layer.alert("删除" + data.msg, {time: 2000});
                            // table.reload("Lay_back_table");
                            renderTable();
                            layer.close(index);
                        },
                        error: function (data) {
                            renderTable();
                            layer.msg("操作失败", {time: 1000, icon: 5});
                        }
                    })
                });
            } else if (obj.event === 'detailOperation') {
                //alert(data.menuId);
                layer.open({
                    title: "查看/编辑按钮资源",
                    // content: $("#LAY-goods-pic-layer"),
                    // content: "goods_pic.jsp?ids="+ids,
                    content: "${basepath}operation/tooperation?menuId="+data.menuId,
                    type: 2,
                    maxmin: true,
                    area: ['1500px', '600px'],
                    success: function(layero, index){
                        var body = layer.getChildFrame('body', index);
                        var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                        // console.log(body.html()); //得到iframe页的body内容
                        // body.find('#goods_id').val(ids);

                    },
                    end: function () {
                        window.location.reload();
                    }
                });
            }
        });

        renderTable();
        //展开所有
        $('#btn-expand').click(function () {
            //alert(0)
            treetable.expandAll('#Lay_category_treeTable');
        });
        //折叠所有
        $('#btn-fold').click(function () {
            // alert(1)
            treetable.foldAll('#Lay_category_treeTable');
        });
        //刷新表格
        $('#btn-refresh').click(function () {
            renderTable();
        });

        //执行搜索
        $('#btn-search').click(function () {
            var keyword = $('#Lay_toSearch_input').val();
            //alert(keyword);
            // var searchName = $('#Lay_toSearch_input').val();
            var searchCount = 0;
            $('#Lay_category_treeTable').next('.treeTable').find('.layui-table-body tbody tr td').each(function () {
                $(this).css('background-color', 'transparent');
                var text = $(this).text();
                if (keyword != '' && text.indexOf(keyword) >= 0) {
                    $(this).css('background-color', 'rgba(250,230,160,0.5)');
                    if (searchCount == 0) {
                        treetable.expandAll('#Lay_category_treeTable');
                        $('html,body').stop(true);
                        $('html,body').animate({scrollTop: $(this).offset().top - 150}, 500);
                    }
                    searchCount++;
                }
            });
            if (keyword == '') {
                layer.msg("请输入搜索内容", {icon: 5});
            } else if (searchCount == 0) {
                layer.msg("没有匹配结果", {icon: 5});
            }
        });

        $("#add").click(function () {
            layer.open({
                title: "添加配置",
                content: $("#layuiconfig-form-role"),
                type: 1,
                maxmin: true,
                area: ['500px', '480px'],
                end: function () {
                    window.location.reload();
                }
            });

            $("#updateSubmitBtn").hide();
            $("#LAY-sysconfig-submit").show();

            form.on('select(classIdLevel)', function (data) {
                //console.log(data.elem); //得到select原始DOM对象
                console.log("data.value = " + data.value); //得到被选中的值
                //console.log(data.othis); //得到美化后的DOM对象
                if (data.value == 1) {
                    // alert(1);
                    $("#Lay_One_Level").hide();
                    $("#Lay_Two_Level").hide();
                }
                if (data.value == 2) {
                    // alert(2);
                    $("#Lay_One_Level").show();
                    $("#Lay_Two_Level").hide();
                    $.ajax({
                        url: '${basepath}menu/selectOneLevel',
                        dataType: 'json',
                        type: 'post',
                        success: function (data) {
                            $.each(data, function (index, item) {
                                //console.log("000 " + index);
                                //console.log("111 " + item);
                                $('#parentId').append(new Option(item.menuName, item.menuId));//往下拉菜单里添加元素
                            });
                            form.render();//菜单渲染 把内容加载进去
                        }
                    })
                }

            });

            //当点击提交按钮的时候，会进入到这个函数
            form.on("submit(LAY-sysconfig-submit)", function (data) {
                console.log(data.field);
                $.ajax({
                    url: "${basepath}menu/addMenu",
                    data: data.field,
                    type: "post",
                    success: function (data) {
                        //1.关闭掉添加弹出层
                        layer.closeAll('page');
                        //2.提示添加成功
                        layer.alert("添加" + data.msg, {time: 3000});
                        //3.刷新table
                        //table.reload("Lay_back_table");
                        renderTable();
                    },
                    error: function (data) {
                        layer.msg("添加失败！", {time: 3000, icon: 5});
                    }
                });
                return false;//阻止跳转；
            })
        });
        //执行批量删除
        $("#delMany").click(function () {
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].menuId;
                    }
                    console.log("ids===" + ids);
                    //执行删除操作
                    $.ajax({
                        url: "${basepath}menu/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert("删除" + data.msg, {time: 2000});
                            //刷新table
                            // table.reload("Lay_back_table");
                            renderTable();
                        },
                        error: function (data) {
                            layer.msg("添加失败！", {time: 3000, icon: 5});
                        }
                    })
                });
            }
        })
    });

</script>
</body>
</html>

