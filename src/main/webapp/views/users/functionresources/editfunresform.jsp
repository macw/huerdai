<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/9/30
  Time: 13:25
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../meta.jsp" %>
<html>
<head>
    <title>按钮资源管理</title>
    <link rel="stylesheet" href="${basepath}layuiadmin/layui/css/layui.css" media="all">
    <script type="text/javascript" src="${basepath}js/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="${basepath}js/cookies.js"></script>
    <script type="text/javascript" src="${basepath}layuiadmin/layui/layui.js"></script>
    <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>

</head>
<body>
<div class="layui-card-body">
    <div style="padding-bottom: 10px;" id="LAY_lay_add">
        <button type="button" class="layui-btn layui-btn-danger" id="delMany" <%--&lt;%&ndash;onclick="doMultiDelete()"--%>
                &ndash;%&gt;>
            <i class="layui-icon layui-icon-delete"></i> 批量删除
        </button>
        <button class="layui-btn layuiadmin-btn-role " data-type="add" id="add" <%--onclick="toOpenAddLayer()"--%>>
            <i class="layui-icon layui-icon-add-circle-fine"></i> 添加
        </button>
        &nbsp;
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="search" id="Lay_toSearch_input" placeholder="请输入按钮名称" autocomplete="off"
                   class="layui-input">
        </div>
        <div class="layui-input-inline" style="width: 100px;">
            <button type="button" class="layui-btn layui-btn-normal" id="btn-search" <%-- onclick="doSearch()"--%>>
                <i class="layui-icon layui-icon-search"></i> 搜索
            </button>
        </div>


    </div>


    <table id="Lay_back_table" lay-filter="Lay_back_table"></table>

</div>

<script type="text/html" id="updateAndDelete">
    <button type="button" class="layui-btn  layui-btn-normal"
            lay-event="edit" <%--onclick="toOpenUpdateLayer('{{d.classId}}')"--%>>
        <i class="layui-icon layui-icon-edit"></i>修改
    </button>
    <button type="button" class="layui-btn layui-btn-danger"
            lay-event="del" <%--onclick="doDelete('{{d.classId}}')"--%>>
        <i class="layui-icon layui-icon-delete"></i> 删除
    </button>
</script>

<%--弹出层--%>

<form id="addForm" class="layui-form">
    <div class="layui-form" lay-filter="layuiconfig-form-operation" id="layuiconfig-form-operation"
         style="padding: 20px 30px 0 0;display: none">
        <div class="layui-form-item">
            <label class="layui-form-label">按钮名称</label>
            <div class="layui-input-block">
                <input name="btnName" id="btnName" class="layui-input">
                <input name="btnId" id="btnId"  lay-type="hide" type="hidden" class="layui-input">
                <input name="menuId" id="menuId" value="${mid}"  lay-type="hide" type="hidden" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">按钮编号</label>
            <div class="layui-input-block">
                <input name="btnCode" id="btnCode" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">按钮标题</label>
            <div class="layui-input-block">
                <input name="btnTitle" id="btnTitle" class="layui-input">
            </div>
        </div>


        <div class="layui-form-item" style="text-align: right">
            <button class="layui-btn " lay-submit lay-filter="LAY-sysconfig-submit" id="LAY-sysconfig-submit">确认添加
            </button>
            <button lay-submit lay-filter="updateSubmitBtn" class="layui-btn" id="updateSubmitBtn">确认修改</button>

        </div>
    </div>
</form>


<script type="text/javascript">
    layui.use(['table', "layer", 'form'], function () {
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var $ = layui.jquery;

        table.render({
            elem: '#Lay_back_table',
            url: '${basepath}operation/selectAll?menuId='+${mid}, //数据接口
            page: true,
            limit: 10,
            limits: [10, 20, 30],
            // width: 'auto',
            //toolbar: "#LAY_lay_add",
            cols: [[
                {type: "checkbox"},
                {title: "按钮名称", field: "btnName",align:"center"},
                {title: "按钮编号", field: "btnCode",align:"center"},
                {title: "按钮标题", field: "btnTitle",align:"center"},
                {title: "操作", templet: "#updateAndDelete",align:"center"}
            ]]

        });

        //刷新表格方法
        function renderTable() {
            table.reload('Lay_back_table', {});
        };

        //监听行工具事件
        table.on('tool(Lay_back_table)', function (obj) {
            var data = obj.data;
            if (obj.event === 'edit') {
               // layer.msg(data.btnId);
                $.ajax({
                    url: "${basepath}operation/selectOne",
                    data: "aid=" + data.btnId,
                    success: function (data) {
                        $("#btnId").val(data.btnId);
                        $("#btnName").val(data.btnName);
                        $("#btnCode").val(data.btnCode);
                        $("#btnTitle").val(data.btnTitle);
                    }
                });

                layer.open({
                    title: "修改按钮信息",
                    content: $("#layuiconfig-form-operation"),
                    type: 1,
                    maxmin: true,
                    area: ['400px', '280px'],
                    end: function () {
                        window.location.reload();
                    }
                });

                $("#LAY-sysconfig-submit").hide();
                $("#updateSubmitBtn").show();

                //3.提交表单
                form.on("submit(updateSubmitBtn)", function (data) {
                     console.log(data.field);
                    $.ajax({
                        url: "${basepath}operation/update",
                        data: data.field,
                        type: "post",
                        //4.接收后台修改响应回来的数据；关闭弹出层、提示修改信息、刷新table
                        success: function (data) {
                            //1.关闭掉添加弹出层
                            layer.closeAll('page');
                            //2.提示修改成功
                            layer.alert("修改" + data.msg, {time: 3000});
                            //刷新table
                            //table.reload("Lay_category_treeTable");
                            renderTable();
                        },
                        error: function (data) {
                            renderTable();
                            layer.msg("操作失败", {time: 2000, icon: 5});
                        }
                    });
                    return false;//阻止跳转；
                });


            }else if (obj.event === 'del'){
                layer.confirm('确定要删除吗？', {icon: 3, title: '确认删除'}, function (index) {
                    $.ajax({
                        url: "${basepath}operation/delete",
                        data: "aid=" + data.btnId,
                        success: function (data) {
                            layer.alert("删除" + data.msg, {time: 2000});
                            table.reload("Lay_back_table");
                        },
                        error: function (data) {
                            table.reload("Lay_back_table");
                            layer.msg("操作失败", {time: 1000, icon: 5});
                        }
                    });
                    layer.close(index);
                });
            }
        });

        $("#add").click(function () {
            layer.open({
                title: "添加按钮",
                content: $("#layuiconfig-form-operation"),
                type: 1,
                // maxmin: true,
                area: ['500px', '480px'],
                end: function () {
                    window.location.reload();
                }
            });
            $("#updateSubmitBtn").hide();
            $("#LAY-sysconfig-submit").show();

            //当点击提交按钮的时候，会进入到这个函数
            form.on("submit(LAY-sysconfig-submit)", function (data) {
                console.log(data.field);
                $.ajax({
                    url: "${basepath}operation/addoperation",
                    data: data.field,
                    type: "post",
                    success: function (data) {
                        //1.关闭掉添加弹出层
                        layer.closeAll('page');
                        //2.提示添加成功
                        layer.alert("添加" + data.msg, {time: 3000});
                        //3.刷新table
                        //table.reload("Lay_back_table");
                        renderTable();
                    },
                    error: function (data) {
                        layer.msg("添加失败！", {time: 3000, icon: 5});
                    }
                });
                return false;//阻止跳转；
            })
        });
        //搜索操作
        $("#btn-search").click(function () {
            //1.获取到输入框中输入的内容
            var searchName = $('#Lay_toSearch_input').val();
            //发送请求，并且接收数据
            layui.use('table', function () {
                var table = layui.table;
                table.reload('Lay_back_table', {
                    where: {"name": searchName}
                });
            });
        })
        $("#delMany").click(function () {
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据吗？", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].btnId;
                    }
                    console.log("ids===" + ids);
                    //执行删除操作
                    $.ajax({
                        url: "${basepath}operation/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert("删除" + data.msg, {time: 2000});
                            //刷新table
                            table.reload("Lay_back_table");
                        },
                        error: function (data) {
                            layer.msg("操作失败！", {time: 3000, icon: 5});
                        }
                    })
                });
            }
        })

    });





    function doMultiDelete() {
        //获取到选中的内容的id===》table模块中找方法
        layui.use(['layer', 'table'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var $ = layui.jquery;
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].userId;
                    }
                    console.log("ids===" + ids);
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/user/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert("删除" + data.msg, {time: 2000});
                            //刷新table
                            table.reload("Lay_back_table");
                        }
                    })
                });
            }
        });
    }

</script>
</body>
</html>

