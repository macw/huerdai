<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ include file="../../meta.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title>后台用户管理</title>
    <link rel="stylesheet" href="${basepath}layuiadmin/layui/css/layui.css" media="all">
    <script type="text/javascript" src="${basepath}js/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="${basepath}js/cookies.js"></script>
    <script type="text/javascript" src="${basepath}layuiadmin/layui/layui.js"></script>

</head>
<body>
<%--弹出层--%>

<form id="addForm" class="layui-form">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0;">
        <div class="layui-form-item" id="demo0">
            <label class="layui-form-label">用户名称</label>
            <div class="layui-input-block">
                <input name="name" id="name" value="${userBack.name}" class="layui-input">
                <input name="userId" id="userId" value="${userBack.userId}" lay-type="hide" type="hidden" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">登录账号</label>
            <div class="layui-input-block">
                <input name="loginName" value="${userBack.loginName}" id="loginName" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">登录密码</label>
            <div class="layui-input-block">
                <input name="password" value="${userBack.password}" id="password" class="layui-input">
            </div>
        </div>


        <div class="layui-form-item">
            <label class="layui-form-label">备注</label>
            <div class="layui-input-block">
                <input name="remark" value="${userBack.remark}" id="remark" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item" >
            <label class="layui-form-label">请选择所属角色</label>
            <div class="layui-input-block">
                <select name="roleName" id="Lay_roleName" lay-filter="Lay_roleName">
                    <option value=""></option>
                    <c:forEach items="${roleList}" var="l">
                        <option value="${l.roleName}"
                                <c:if test="${userBack.roleName==l.roleName}">selected</c:if>>${l.remark}</option>
                    </c:forEach>
                </select>
            </div>
        </div>


        <div class="layui-form-item">
            <div class="layui-input-block" >
                <button type="submit" class="layui-btn" lay-filter="btnSub" lay-submit s>立即提交</button>
            </div>
        </div>
    </div>
</form>

  <script type="text/javascript">
      layui.use(['form','layer'], function(){
          var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
          var layer = layui.layer;
          var $ = layui.jquery;
          form.render();

          //监听提交
          form.on('submit(btnSub)', function(data) {
              var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
              console.log(data.field);
              $.ajax({
                  url: '${basepath}user/updateOrSave',
                  data: data.field,
                  type: 'post',
                  dataType: 'json',
                  success: function (data) {
                      layer.msg('操作成功', {
                          offset: '15px',
                          icon: 6,
                          time: 1000
                      }, function () {
                          parent.layer.close(index);
                          parent.layui.table.reload('Lay_back_table', {});
                      });
                  },
                  error: function (data) {
                      layer.msg('操作失败', {
                          offset: '15px',
                          icon: 5,
                          time: 1000
                      }, function () {
                          parent.layer.close(index);
                          parent.layui.table.reload('Lay_back_table', {});
                      });
                  }
              });
              return false;
          });
      });

  </script>
</body>
</html>