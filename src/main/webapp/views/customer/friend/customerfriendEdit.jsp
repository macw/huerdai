<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/12
  Time: 14:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../../head.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>会员好友管理</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>
<body>

<form id="addForm" class="layui-form" enctype="multipart/form-data">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0;">

        <div class="layui-form-item">
            <input name="id" id="id" value="${fr.id}" lay-type="hide" type="hidden"
                   class="layui-input">
            <label class="layui-form-label">请选择会员</label>
            <div class="layui-input-block">
                <select name="customerInfId1" id="customerInfId1" lay-filter="customerInfId1">
                    <option value="">请选择会员</option>
                    <c:forEach items="${cs}" var="l">
                        <option value="${l.customerId }"
                                <c:if test="${fr.customerInfId1 == l.customerId}">selected</c:if> >${l.nickname }</option>
                    </c:forEach>
                </select>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">请选择关系类型</label>
            <div class="layui-input-block">
                <select name="relationshipType" id="relationshipType" lay-filter="relationshipType">
                    <option value="">请选择关系类型</option>
                    <option value="1" <c:if test="${fr.relationshipType == 1}">selected</c:if>>关注</option>
                    <option value="2" <c:if test="${fr.relationshipType == 2}">selected</c:if>>好友</option>
                    <option value="3" <c:if test="${fr.relationshipType == 3}">selected</c:if>>粉丝</option>
                </select>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">请选择对象关系昵称</label>
            <div class="layui-input-block">
                <select name="customerInfId2" id="customerInfId2" lay-filter="customerInfId2">
                    <option value="">请选择会员</option>
                    <c:forEach items="${cs}" var="l">
                        <option value="${l.customerId }"
                                <c:if test="${fr.customerInfId2 == l.customerId}">selected</c:if> >${l.nickname }</option>
                    </c:forEach>
                </select>
            </div>
        </div>
    </div>


    <div class="layui-form-item">
        <div class="layui-input-block" >
            <button type="submit" class="layui-btn" id="btnSub" lay-filter="btnSub" lay-submit>立即提交</button>
        </div>
    </div>
    </div>
</form>


<script type="text/javascript">
    layui.use(['form', 'layer'], function () {
        var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        var layer = layui.layer;
        var $ = layui.jquery;

        //监听提交
        form.on('submit(btnSub)', function (data) {
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            var formData = new FormData(document.getElementById("addForm"));
            console.log(data.field);
            $.ajax({
                url: '${pageContext.request.contextPath}/customerfriend/addOrUpdate',
                data: formData,
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    layer.msg('操作成功', {
                        offset: '15px',
                        icon: 6,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                },
                error: function (data) {
                    layer.msg('操作失败', {
                        offset: '15px',
                        icon: 5,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                }
            });
            return false;
        });
    });

</script>
</body>
</html>



