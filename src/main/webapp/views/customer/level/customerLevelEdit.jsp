<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/10
  Time: 16:56
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../../head.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>等级管理</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>
<body>

<form id="addForm" class="layui-form" enctype="multipart/form-data">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0;">

        <div class="layui-form-item" id="demo0" style="width: 700px">
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">等级名称</label>
                <div class="layui-input-block">
                    <input name="levelName" id="levelName" value="${cs.levelName}" class="layui-input">
                    <input name="levelId" id="levelId" value="${cs.levelId}" lay-type="hide" type="hidden"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-input-inline">
                <div class="layui-form-item" style="width: 300px">
                    <label class="layui-form-label">折扣</label>
                    <div class="layui-input-block">
                        <input name="discount" value="${cs.discount}" id="discount" lay-verify="required"
                               class="layui-input">
                    </div>
                </div>
            </div>
        </div>


        <div class="layui-form-item" style="width: 600px">
            <div class="layui-input-block" style="text-align: right">
                <button type="submit" class="layui-btn" id="btnSub" lay-filter="btnSub" lay-submit>立即提交</button>
            </div>
        </div>
    </div>
</form>


<script type="text/javascript">
    layui.use(['form','layer'], function () {
        var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        var layer = layui.layer;
        var $ = layui.jquery;

        //监听提交
        form.on('submit(btnSub)', function (data) {
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            var formData = new FormData(document.getElementById("addForm"));
            console.log(data.field);
            $.ajax({
                url: '${pageContext.request.contextPath}/customerlevel/addOrUpdate',
                data: formData,
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    layer.msg('操作成功', {
                        offset: '15px',
                        icon: 6,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                },
                error: function (data) {
                    layer.msg('操作失败', {
                        offset: '15px',
                        icon: 5,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                }
            });
            return false;
        });
    });

</script>
</body>
</html>


