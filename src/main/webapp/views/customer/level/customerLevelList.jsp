<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/10
  Time: 16:16
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../../head.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>会员管理</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
</head>
<body>

<div class="layui-card-body">
    <div class="layui-form">
        <div style="padding-bottom: 10px;" id="LAY_lay_add">
            <button type="button" class="layui-btn layui-btn-danger" id="doMultiDelete">
                <i class="layui-icon layui-icon-delete"></i> 批量删除
            </button>
            <button class="layui-btn layuiadmin-btn-role " data-type="add" id="add">
                <i class="layui-icon layui-icon-add-circle-fine"></i> 添加
            </button>

            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="levelName"  placeholder="请输入等级名称" autocomplete="off"
                       class="layui-input">
            </div>

            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" lay-filter="search" lay-submit>
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>

    </div>
    <table id="Lay_back_table" lay-filter="Lay_back_table"></table>

</div>

<script type="text/html" id="updateAndDelete">

    <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">
        <i class="layui-icon layui-icon-edit"></i>修改
    </button>
    <button type="button" class="layui-btn layui-btn-danger" lay-event="del">
        <i class="layui-icon layui-icon-delete"></i>删除
    </button>

</script>

<script type="text/javascript">
    layui.use(['table', "layer", 'form'], function () {
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var $ = layui.jquery;

        var layTab = table.render({
            elem: '#Lay_back_table',
            url: '${pageContext.request.contextPath}/customerlevel/selectAll', //数据接口
            page: true,
            limit: 10,
            limits: [10, 20, 30],
            // width: 'auto',
            autoSort: true,
            initSort: {
                field: 'orderTime' //排序字段，对应 cols 设定的各字段名
                , type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
            },
            //toolbar: "#LAY_lay_add",
            cols: [[
                {type: "checkbox"},
                {title: "等级名称", field: "levelName", align: "center"},
                {title: "折扣:%百分比", field: "discount", align: "center"},
                {title: "创建人", field: "createUser", align: "center"},
                {
                    title: "创建时间",
                    field: "createTime", align: "center",
                    templet: '<div>{{# if(d.createTime!=null){ }} {{ layui.util.toDateString(d.createTime,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'
                },
                {title: "更新人员", field: "updateUser", align: "center"},
                {
                    title: "更新时间",
                    field: "updateTime", align: "center",
                    templet: '<div>{{# if(d.updateTime!=null){ }} {{ layui.util.toDateString(d.updateTime,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'
                },
                {title: "操作", templet: "#updateAndDelete", align: "center"}
            ]]
        });

        //刷新表格方法
        function tableReload() {
            table.reload('Lay_back_table', {});
        };

        //监听行工具事件
        table.on('tool(Lay_back_table)', function (obj) {
            var data = obj.data;
            if (obj.event === 'edit') {
                layer.open({
                    title: "修改",
                    content: "${pageContext.request.contextPath}/customerlevel/selectOne?aid="+data.levelId,
                    type: 2,
                    maxmin: true,
                    area: ['750px', '200px'],
                    end: function () {
                        window.location.reload();
                    }
                });
            }else if (obj.event === "del"){
                layer.confirm('确定要删除吗？', {icon: 3, title: '确认删除'}, function (index) {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/customerlevel/deleteOne",
                        data: "aid=" + data.levelId,
                        success: function (data) {
                            layer.alert(data.msg, {time: 3000});
                            table.reload("Lay_back_table");
                        },
                        error: function (data) {
                            table.reload("Lay_back_table");
                            layer.msg("操作失败", {time: 1000, icon: 5});
                        }
                    });
                });
            }
        });

        //监听搜索
        form.on('submit(search)', function (data) {
            var field = data.field;
            // console.log(field);
            //执行重载
            layTab.reload({
                where: field
            });
        });
        //打开添加窗口
        $("#add").click(function () {
            layer.open({
                title: "添加等级",
                content: "${pageContext.request.contextPath}/customerlevel/tocustomerLevelEdit",
                type: 2,
                // maxmin: true,
                area: ['750px', '200px'],
                end: function () {
                    window.location.reload();
                }

            });
        });
        //批量删除方法
        $("#doMultiDelete").click(function () {
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据?", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].levelId;
                    }
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/customerlevel/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert(data.msg, {time: 3500});
                            //刷新table
                            table.reload("Lay_back_table");
                        }
                    })
                });
            }
        })

    });


</script>
</body>
</html>


