<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>登陆日志列表</title>
	<%@ include file="../head.jsp" %>
    <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card-body">
        <div style="padding-bottom: 10px;">
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="search" id="Lay_toSearch_input" placeholder="请输入用户名" autocomplete="off"
                       class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>
        <table id="tab" lay-filter="tab"></table>
    </div>
</div>
</body>
    <script type="text/html" id="tool">
        <button type="button" class="layui-btn layui-btn-danger" lay-event="del">删除</button>
    </script>
    <script type="text/javascript">
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            table.render({
                elem: '#tab',
                url: '<%= basePath%>customerloginlog/selectList',
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                cols: [[
                    {type: "checkbox"},
                    {title: "用户", field: "customerName",align:"center"},
                    {title: "登陆时间", field: "loginTime",
                    	templet: function(d){
                    		return layui.util.toDateString(d.loginTime);
                    	},align:"center"
                    },
                    {title: "IP地址", field: "loginIp",align:"center"},
                    {title: "登陆是否成功", field: "loginType",
                    	templet: function(d){
	                    	if(d.loginType == '1'){ 
	                    		return '登陆成功'
	                    	} else {
	                    		return '登陆失败'
	                    	}
                    	},align:"center"
                    },
                    {title: "操作", templet: "#tool",align:"center"}
                ]] ,done:function(res, curr){
        	    	  var brforeCurr = curr; // 获得当前页码
        	    	  var dataLength = res.data.length; // 获得当前页的记录数
        	    	  var count = res.count; // 获得总记录数
        	    	  if(dataLength == 0 && count != 0){ //如果当前页的记录数为0并且总记录数不为0
        	    		  table.reload("tab",{ // 刷新表格到上一页
        	    			  page:{
        	    				 curr:brforeCurr-1
        	    			  }
        	    		  });
        	    	  }
        	      }

            });
            
            table.on('tool(tab)', function(obj){
                var data = obj.data;
                if(obj.event === 'del'){
                	layer.confirm('确认要[删除]这条记录吗?', function(index){
                		var icon = 5;
                		var str = "操作失败";
                        $.ajax({
                       	 url:'<%=basePath%>customerloginlog/delCustomerLoginLog/api',
                       	 data: {"loginId":data.loginId},
                       	 type: 'post',
                       	 dataType: 'json',
                       	 async:false,
                       	 success:function(data){
                       		icon=6;
                       		str="操作成功";
                       	 }
                        });
                        table.reload('tab', {
                        	
                        });
                        layer.msg(str, {time: 1000, icon: icon});
                     });
                }
            });
        });
        //搜索操作
        function doSearch() {
            //1.获取到输入框中输入的内容
            var customerName = $('#Lay_toSearch_input').val();
            //发送请求，并且接收数据
            layui.use('table', function () {
                var table = layui.table;
                table.reload('tab', {
                    where: {"customerName": customerName},
                    page: {
                        curr: 1 //重新从第 1 页开始
                      }
                });
            });
        }
    </script>
</html>

