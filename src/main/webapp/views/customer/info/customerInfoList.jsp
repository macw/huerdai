<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/9
  Time: 9:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../../head.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>会员管理</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>
<body>

<div class="layui-card-body">
    <div class="layui-form">
        <div style="padding-bottom: 10px;" id="LAY_lay_add">
            <button type="button" class="layui-btn layui-btn-danger" id="doMultiDelete">
                <i class="layui-icon layui-icon-delete"></i> 批量删除
            </button>
            <button class="layui-btn layuiadmin-btn-role " data-type="add" id="add">
                <i class="layui-icon layui-icon-add-circle-fine"></i> 添加
            </button>
            &nbsp;

            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="customerName"  placeholder="请输入真实姓名" autocomplete="off"
                       class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="nickname" placeholder="请输入用户昵称" autocomplete="off"
                       class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="loginName" placeholder="请输入用户名" autocomplete="off"
                       class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="mobilePhone"  placeholder="请输入手机号"
                       autocomplete="off"
                       class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <select name="regtype" lay-verify="" lay-search>
                    <option value="">请选择注册类型</option>
                    <option value="1">邀请注册链接</option>
                    <option value="2">网页注册</option>
                    <option value="3">扫码注册</option>
                    <option value="4">微信群管理注册</option>
                </select>
            </div>


            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" lay-filter="search" lay-submit>
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>

    </div>
    <table id="Lay_back_table" lay-filter="Lay_back_table"></table>

</div>

<script type="text/html" id="updateAndDelete">

    <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">
        <i class="layui-icon layui-icon-edit"></i>修改
    </button>
    <button type="button" class="layui-btn layui-btn-danger" lay-event="del">
        <i class="layui-icon layui-icon-delete"></i>删除
    </button>

</script>

<script type="text/javascript">
    layui.use(['table', "layer", 'form'], function () {
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var $ = layui.jquery;

        var layTab = table.render({
            elem: '#Lay_back_table',
            url: '${pageContext.request.contextPath}/customerinfo/selectAll', //数据接口
            page: true,
            limit: 10,
            limits: [10, 20, 30],
            width: 'auto',
            autoSort: true,
            initSort: {
                field: 'orderTime' //排序字段，对应 cols 设定的各字段名
                , type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
            },
            //toolbar: "#LAY_lay_add",
            cols: [[
                {type: "checkbox"},
                {title: "用户头像", field: "customerIcon", align: "center", width: "130",event:"to_big_pic",
                    templet: function (d) {
                        if (d.customerIcon!=null){
                            return '<img width="50" src="'+ d.customerIcon + '">'
                        }else {
                            return "";
                        }
                    }
                },
                {title: "用户昵称", field: "nickname", align: "center", width: "150"},
                {title: "手机号", field: "mobilePhone", align: "center", width: "200"},
                {title: "用户真实姓名", field: "customerName", sort: true, align: "center", width: "100"},
                {title: "用户登录名", field: "loginName", align: "center", width: "100"},
                {title: "邀请码", field: "invitationcode", align: "center", width: "100"},
                {title: "邮箱", field: "customerEmail", align: "center", width: "100"},
               /* {
                    title: "注册类型", field: "regtype", align: "center", width: "150",
                    templet: function (d) {
                        if (d.regtype == 1) {
                            return '<div>邀请注册链接</div>';
                        } else if (d.regtype == 2) {
                            return '<div>网页注册</div>';
                        } else if (d.regtype == 3) {
                            return '<div>扫码注册 </div>';
                        }else if (d.regtype == 4){
                            return '<div>微信群管理注册 </div>'
                        }
                        else {
                            return '';
                        }
                    }
                },
                {
                    title: "证件类型", field: "identityCardType", align: "center", width: "100",
                    templet: function (d) {
                        if (d.identityCardType == 1) {
                            return '<div >身份证</div>';
                        } else if (d.identityCardType == 2) {
                            return '<div>军官证</div>';
                        } else if (d.identityCardType == 3) {
                            return '<div>护照 </div>';
                        }else{
                            return '';
                        }
                    }
                },
                {title: "证件号码", field: "identityCardNo", align: "center", width: "100"},*/
                {title: "性别", field: "gender", align: "center", width: "100",
                    templet: function (d) {
                        if (d.gender == "1") {
                            return '<div >男</div>';
                        } else if (d.gender == "2"){
                            return '<div>女</div>';
                        }else {
                            return "未知";
                        }
                    }
                },
                {title: "用户积分", field: "userPoint", sort: true, align: "center", width: "100",
                    templet: function (d) {
                        return '<button type="button" class="layui-btn layui-btn-radius layui-btn-warm"  lay-event="point">'+d.userPoint+'</button>';
                    }
                },
             /*   {title: "会员级别", field: "customerLevelName", sort: true, align: "center", width: "100"},
                {title: "关注数量", field: "followcount",  sort: true,align: "center", width: "100"},
                {title: "粉丝数量", field: "fanscount", sort: true, align: "center", width: "100"},*/
                {title: "达人描述", field: "darenMemo", align: "center", width: "200",},
               /* {
                    title: "会员生日",
                    field: "birthday", align: "center", width: "200",
                    templet: '<div>{{# if(d.birthday!=null){ }} {{ layui.util.toDateString(d.birthday,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'
                },*/
                {
                    title: "注册时间",
                    field: "registerTime", align: "center", width: "200",
                    templet: function (d) {
                        var str = '';
                        if (d.registerTime !=null){
                            str+= layui.util.toDateString(d.registerTime)+"</br>";
                        }
                      /*  if(d.modifiedTime !=null){
                            str+="修改时间:"+layui.util.toDateString(d.modifiedTime);
                        }*/
                        return str;
                    }

                },

                {
                    title: "是否会员", width: 130, align: "center",
                    templet: function (d) {
                        if (d.isCuslevel == 1) {
                            return '<button type="button" class="layui-btn  layui-btn-danger"  lay-event="agent">是</button>';
                        } else {
                            return '<button type="button" class="layui-btn  "  lay-event="agent">否</button>';
                        }
                    }
                },

                {
                    title: "用户状态", width: 130, align: "center",
                    templet: function (d) {
                        if (d.userStats == 1) {
                            return '<button type="button" class="layui-btn  layui-btn-normal"  lay-event="yn">启用</button>';
                        } else {
                            return '<button type="button" class="layui-btn  layui-btn-danger"  lay-event="yn">禁用</button>';
                        }
                    }
                },



                {title: "操作", templet: "#updateAndDelete", align: "center", width: "220"}
            ]]
        });

        //刷新表格方法
        function tableReload() {
            table.reload('Lay_back_table', {});
        };

        //监听行工具事件
        table.on('tool(Lay_back_table)', function (obj) {
            var data = obj.data;
            if (obj.event === 'edit') {
                layer.open({
                    title: "修改配置",
                    content: "${pageContext.request.contextPath}/customerinfo/selectOne?aid=" + data.customerId,
                    type: 2,
                    maxmin: true,
                    area: ['750px', '630px'],
                    end: function () {
                        window.location.reload();
                    }
                });
            }else if (obj.event === 'yn') {
                var str = obj.data.userStats == 0 ? "启用" : "禁用";
                var state = obj.data.userStats == 1 ? 0 : 1;
                layer.confirm('确认要[' + str + ']这个会员吗?', function (index) {
                    $.ajax({
                        url: '${pageContext.request.contextPath}/customerinfo/updatecustomerInfo',
                        data: {"customerId": data.customerId, "userStats": state},
                        type: 'post',
                        dataType: 'json',
                        success: function (data) {
                            tableReload();
                            layer.msg("操作成功", {time: 1000, icon: 6});
                        }, error: function (data) {
                            tableReload();
                            layer.msg("操作失败", {time: 1000, icon: 5});
                        }
                    });
                    layer.close(index);
                });
            }
            else if (obj.event === 'point') {
                layer.open({
                    title: "查看积分明细",
                    content: "${pageContext.request.contextPath}/scoreaccount/toAccount?id="+data.customerId,
                    type: 2,
                    maxmin: true,
                    area: ['1400px', '700px'],
                    end: function () {
                        window.location.reload();
                    }

                });
            }
            else if (obj.event === "to_big_pic") {
                var url = data.customerIcon;
                var path = "${pageContext.request.contextPath}" + url;
                layer.open({
                    type: 1,
                    title: "查看大图",
                    skin: 'layui-layer-rim', //加上边框
                    area: ['500', '480'], //宽高
                    content: '<div style="text-align:center"><img src="' + path + '"/></div>',
                    // shadeClose: true, //开启遮罩关闭
                    end: function (index, layero) {
                        return false;
                    }
                });
            }else if (obj.event === "del"){
                layer.confirm('确定要删除吗？', {icon: 3, title: '确认删除'}, function (index) {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/customerinfo/deletecustomerInfo",
                        data: "aid=" + data.customerId,
                        success: function (data) {
                            layer.alert("删除" + data.msg, {time: 2000});
                            table.reload("Lay_back_table");
                        },
                        error: function (data) {
                            table.reload("Lay_back_table");
                            layer.msg("操作失败", {time: 1000, icon: 5});
                        }
                    });
                });
            }
        });

        //监听搜索
        form.on('submit(search)', function (data) {
            var field = data.field;
            // console.log(field);
            //执行重载
            layTab.reload({
                where: field
            });
        });
        //打开添加窗口
        $("#add").click(function () {
            layer.open({
                title: "添加用户",
                // content: $("#layuiconfig-form-role"),
                content: "${pageContext.request.contextPath}/customerinfo/tocustomerInfoEdit",
                type: 2,
                // maxmin: true,
                area: ['750px', '630px'],
                end: function () {
                    window.location.reload();
                }

            });
        });
        //批量删除方法
        $("#doMultiDelete").click(function () {
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据?", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].customerId;
                    }
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/customerinfo/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert("删除" + data.msg, {time: 2000});
                            //刷新table
                            table.reload("Lay_back_table");
                        }
                    })
                });
            }
        })

    });


</script>
</body>
</html>

