<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/9
  Time: 9:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../../head.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>会员管理</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>
<body>

<form id="addForm" class="layui-form" enctype="multipart/form-data">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0;">

        <div class="layui-form-item" id="demo0" style="width: 700px">
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">用户真实姓名</label>
                <div class="layui-input-block">
                    <input name="customerName" id="customerName" value="${cs.customerName}" class="layui-input">
                    <input name="customerId" id="customerId" value="${cs.customerId}" lay-type="hide" type="hidden"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-input-inline">
                <div class="layui-form-item" style="width: 300px">
                    <label class="layui-form-label">昵称</label>
                    <div class="layui-input-block">
                        <input name="nickname" value="${cs.nickname}" id="nickname" lay-verify="required"
                               class="layui-input">
                    </div>
                </div>
            </div>

        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">上传头像</label>
                <div class="layui-input-block">
                    <button type="button" class="layui-btn" id="upload_customer_pic">
                        <i class="layui-icon">&#xe67c;</i>选择图片
                    </button>

                </div>
            </div>
            <div class="layui-inline" style="width: 300px">
                <div class="layui-upload-list"></div>
                <c:if test="${cs.customerIcon!=null}">
                    <img class="layui-upload-img" name="customerIcon"
                         src="${pageContext.request.contextPath}${cs.customerIcon}" id="customerIcon"
                         style="max-width: 200px;max-height: 200px; padding-left: 40px;">
                </c:if>

            </div>
        </div>
            <div class="layui-form-item">
                <div class="layui-input-inline" style="width: 300px;">
                    <div class="layui-form-item">
                        <label class="layui-form-label">用户登录名</label>
                        <div class="layui-input-block">
                            <input name="loginName" value="${cs.loginName}" id="loginName" lay-verify="required"
                                   class="layui-input">
                        </div>
                    </div>
                </div>
                <div class="layui-input-inline" style="width: 300px">
                    <div class="layui-form-item">
                        <label class="layui-form-label">登录密码</label>
                        <div class="layui-input-block">
                            <input name="password" value="${cs.password}" id="password" lay-verify="required"
                                   class="layui-input">
                        </div>
                    </div>

                </div>

            </div>


            <div class="layui-form-item">
                <div class="layui-input-inline" style="width: 300px">
                    <label class="layui-form-label">请选择证件类型</label>
                    <div class="layui-input-block">
                        <select name="identityCardType" id="identityCardType" lay-filter="identityCardType">
                            <option value="">请选择证件类型</option>
                            <option value="1" <c:if test="${cs.identityCardType == 1}">selected</c:if>>身份证</option>
                            <option value="2" <c:if test="${cs.identityCardType == 2}">selected</c:if>>军官证</option>
                            <option value="3" <c:if test="${cs.identityCardType == 3}">selected</c:if>>护照</option>
                        </select>
                    </div>
                </div>
                <div class="layui-input-inline" style="width: 300px">
                    <label class="layui-form-label">证件号码</label>
                    <div class="layui-input-block">
                        <input name="identityCardNo" value="${cs.identityCardNo}" id="identityCardNo"
                               class="layui-input">
                    </div>
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-inline" style="width: 300px">
                    <label class="layui-form-label">手机号</label>
                    <div class="layui-input-block">
                        <input name="mobilePhone" value="${cs.mobilePhone}" lay-verify="required" id="mobilePhone"
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-input-inline" style="width: 300px">
                    <label class="layui-form-label">邮箱</label>
                    <div class="layui-input-block">
                        <input name="customerEmail" value="${cs.customerEmail}" id="customerEmail" class="layui-input">
                    </div>
                </div>
            </div>


            <div class="layui-form-item">
                <div style="width: 300px" class="layui-input-inline">
                    <label class="layui-form-label">请选择性别</label>
                    <div class="layui-input-block">
                        <select name="gender" id="gender" lay-filter="gender">
                            <option value="">请选择性别</option>
                            <option value="1" <c:if test="${cs.gender == 1}">selected</c:if>>男</option>
                            <option value="0" <c:if test="${cs.gender == 0}">selected</c:if>>女</option>
                        </select>
                    </div>
                </div>

                <div style="width: 300px" class="layui-input-inline">
                    <label class="layui-form-label">请选择会员级别</label>
                    <div class="layui-input-block">
                        <select name="customerLevelId" id="customerLevelId" lay-verify="required" lay-search>
                            <option value="">请选择会员级别</option>
                            <c:forEach items="${cll}" var="l">
                                <option value="${l.levelId }"
                                        <c:if test="${cs.customerLevelId == l.levelId}">selected</c:if> >${l.levelName }</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">是否代理商</label>
                <div class="layui-input-block">
                    <select name="isAgent" id="isAgent" lay-filter="isAgent">
                        <option value="">请选择是否代理商</option>
                        <option value="1" <c:if test="${cs.isAgent == 1}">selected</c:if>>是</option>
                        <option value="2" <c:if test="${cs.isAgent == 2}">selected</c:if>>否</option>
                    </select>
                </div>
            </div>
            <div class="layui-input-inline" id="Lay_agenid"  style="width: 300px">
                <label class="layui-form-label">请选择代理商</label>
                <div class="layui-input-block">
                    <select name="agenId" id="agenId" lay-filter="agenId">
                        <option value="">请选择代理商</option>
                        <c:forEach items="${ag}" var="l">
                            <option value="${l.agenId }"
                                    <c:if test="${cs.agenId == l.agenId}">selected</c:if> >${l.agenName }</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">是否旅游公司</label>
                <div class="layui-input-block">
                    <select name="isOperator" id="isOperator" lay-filter="isOperator">
                        <option value="">请选择是否旅游公司</option>
                        <option value="1" <c:if test="${cs.isOperator == 1}">selected</c:if>>是</option>
                        <option value="2" <c:if test="${cs.isOperator == 2}">selected</c:if>>否</option>
                    </select>
                </div>
            </div>
            <div class="layui-input-inline" id="Lay_operator" style="width: 300px;" >
                <label class="layui-form-label">请选择旅游公司</label>
                <div class="layui-input-block">
                    <select name="tId" id="tId" lay-filter="tId">
                        <option value="">请选择是否旅游公司</option>
                        <c:forEach items="${op}" var="l">
                            <option value="${l.tId }"
                                    <c:if test="${cs.tId == l.tId}">selected</c:if> >${l.tName }</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>

        <div class="layui-form-item" style="width: 600px">
            <div class="layui-input-inline" style="width: 600px">
                <label class="layui-form-label">达人描述</label>
                <div class="layui-input-block">
                    <input name="darenMemo" value="${cs.darenMemo}" id="darenMemo" class="layui-input">
                </div>
            </div>
        </div>


        <div class="layui-form-item" style="width: 600px">
            <div class="layui-input-block" style="text-align: right">
                <button type="submit" class="layui-btn" id="btnSub" lay-filter="btnSub" lay-submit>立即提交</button>
            </div>
        </div>
    </div>
</form>


<script type="text/javascript">
    layui.use(['form', 'upload', 'layer'], function () {
        var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        var layer = layui.layer;
        var $ = layui.jquery;
        var upload = layui.upload;
        form.render();
        $("#Lay_agenid").hide();
        $("#Lay_operator").hide();
        if(${cs.isOperator == 1}){
            $("#Lay_operator").show();
        }
        if (${cs.isAgent == 1}){
            $("#Lay_agenid").show();
        }
        form.on('select(isAgent)', function(data){
            if (data.value==1){
                $("#Lay_agenid").show();
            }else{
                $("#Lay_agenid").hide();
            }
        });
        form.on('select(isOperator)', function(data){
            if (data.value==1){
                $("#Lay_operator").show();
            }else{
                $("#Lay_operator").hide();
            }
        });

        //执行实例
        var uploadInst = upload.render({
            elem: '#upload_customer_pic', //绑定元素
            accept: "images",
            acceptMime: 'image/*',//打开文件选择框时,只显示图片文件
            auto: false,  //是否选完文件后自动上传。默认值：true
            bindAction: '#btnSub',
            size: 5120,	//设置文件最大可允许上传的大小，单位 KB
            multiple: false,	//是否允许多文件上传
            drag: true,	//是否接受拖拽的文件上传
            choose: function (obj) {
                //将每次选择的文件追加到文件队列
                var files = obj.pushFile();
                //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
                obj.preview(function (index, file, result) {
                    $(".layui-upload-list").append('<img src="' + result + '" id="remove_' + index + '" title="双击删除该图片" style="width:200px;height:auto;">')
                    $('#remove_' + index).bind('dblclick', function () {//双击删除指定预上传图片
                        delete files[index];//删除指定图片
                        $(this).remove();
                    });
                    // console.log(index); //得到文件索引
                    //  console.log(file); //得到文件对象
                    //  console.log(result); //得到文件base64编码，比如图片
                });
            },
            done: function (res) {
                //上传完毕回调
                //1.关闭掉添加弹出层
                layer.close(index);
                //2.提示添加成功
                layer.alert("添加成功", {time: 3000});
                //3.刷新table
                table.reload("Lay_goods_pic");
            }
            , error: function () {
                //请求异常回调
                layer.alert("添加失败", {time: 3000});
            }
        });
        //监听提交
        form.on('submit(btnSub)', function (data) {
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            var formData = new FormData(document.getElementById("addForm"));
            console.log(data.field);
            $.ajax({
                url: '${pageContext.request.contextPath}/customerinfo/addOrUpdateCustomerInfo',
                data: formData,
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    layer.msg('操作成功', {
                        offset: '15px',
                        icon: 6,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                },
                error: function (data) {
                    layer.msg('操作失败', {
                        offset: '15px',
                        icon: 5,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                }
            });
            return false;
        });
    });

</script>
</body>
</html>

