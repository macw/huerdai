<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/14
  Time: 15:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../../head.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>会员好友管理</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>
<body>

<form id="addForm" class="layui-form" enctype="multipart/form-data">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0;">

        <div class="layui-form-item" style="width: 300px">
            <input name="addressId" id="addressId" value="${ad.addressId}" lay-type="hide" type="hidden"
                   class="layui-input">
            <label class="layui-form-label">请选择会员</label>
            <div class="layui-input-block">
                <select name="customerId" id="customerId" lay-filter="customerInfId1">
                    <option value="">请选择会员</option>
                    <c:forEach items="${cs}" var="l">
                        <option value="${l.customerId }"
                                <c:if test="${ad.customerId == l.customerId}">selected</c:if> >${l.nickname }</option>
                    </c:forEach>
                </select>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">收货人姓名</label>
                <div class="layui-input-block">
                    <input name="consignee" value="${ad.consignee}" lay-verify="required"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">邮政编码</label>
                <div class="layui-input-block">
                    <input name="zipcode" value="${ad.zipcode}"  class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">电话</label>
                <div class="layui-input-block">
                    <input name="telephone" value="${ad.telephone}" lay-verify="required"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">移动电话</label>
                <div class="layui-input-block">
                    <input name="mobile" value="${ad.mobile}"  class="layui-input">
                </div>
            </div>
        </div>


        <div class="layui-form-item" style="width: 680px">
                <label class="layui-form-label">地址</label>
                <div class="layui-input-inline" style="width: 160px;">
                    <select name="province" id="province" lay-filter="province" lay-verify="required" lay-search >
                        <option value="">省</option>
                        <c:forEach items="${provinceList}" var="p">
                            <option value="${p.id}" <c:if test="${ad.province == p.id}">selected</c:if> >${p.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="layui-input-inline" style="width: 160px;">
                    <select name="city" id="city"  lay-filter="city" lay-verify="required" lay-search >
                        <option value="">市</option>
                        <c:forEach items="${cityList}" var="c">
                            <option value="${c.id}" <c:if test="${ad.city == c.id}">selected</c:if> >${c.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="layui-input-inline">
                    <select name="district" id="district" lay-verify="required" lay-search >
                        <option value="">区</option>
                        <c:forEach items="${districtList}" var="d">
                            <option value="${d.id}" <c:if test="${ad.district == d.id}">selected</c:if> >${d.name}</option>
                        </c:forEach>
                    </select>
                </div>
        </div>
        <div class="layui-form-item" style="width: 690px">
            <div class="layui-inline">
                <label class="layui-form-label">街道地址</label>
                <div class="layui-input-inline" >
                    <div class="layui-input-inline">
                        <input type="text" name="street" value="${ad.street}" lay-verify="required" autocomplete="off" class="layui-input" style="width: 515px;">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="layui-form-item">
        <div class="layui-input-block" >
            <button type="submit" class="layui-btn" id="btnSub" lay-filter="btnSub" lay-submit>立即提交</button>
        </div>
    </div>
</form>


<script type="text/javascript">
    layui.use(['form', 'layer'], function () {
        var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        var layer = layui.layer;
        var $ = layui.jquery;

        form.on('select(province)', function(data){
            $("#district").empty();
            var str="<option value=''>区</option>";
            $("#district").append(str);
            str="<option value=''>市</option>";
            $.ajax({
                url:'<%=basePath%>region/ajaxRegionList?pid='+data.value,
                type: 'post',
                dataType: 'json',
                success:function(d){
                    $("#city").empty();
                    if(d.data.length>0){
                        $(d.data).each(function(i,t){
                            str+="<option value='"+t.id+"'>"+t.name+"</option>"
                        })
                    }
                    $("#city").append(str);
                    form.render('select');
                }
            });
        });
        form.on('select(city)', function(data){
            var str="<option value=''>区</option>";
            $.ajax({
                url:'<%=basePath%>region/ajaxRegionList?pid='+data.value,
                type: 'post',
                dataType: 'json',
                success:function(d){
                    $("#district").empty();
                    if(d.data.length>0){
                        $(d.data).each(function(i,t){
                            str+="<option value='"+t.id+"'>"+t.name+"</option>"
                        })
                    }
                    $("#district").append(str);
                    form.render('select');
                }
            });
        });

        //监听提交
        form.on('submit(btnSub)', function (data) {
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            var formData = new FormData(document.getElementById("addForm"));
            console.log(data.field);
            $.ajax({
                url: '${pageContext.request.contextPath}/address/addOrUpdate',
                data: formData,
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    layer.msg('操作成功', {
                        offset: '15px',
                        icon: 6,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                },
                error: function (data) {
                    layer.msg('操作失败', {
                        offset: '15px',
                        icon: 5,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                }
            });
            return false;
        });
    });

</script>
</body>
</html>




