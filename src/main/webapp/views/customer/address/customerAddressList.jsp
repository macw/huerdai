<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/14
  Time: 15:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../../head.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>会员地址管理</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
</head>
<body>

<div class="layui-card-body">
    <div class="layui-form">
        <div style="padding-bottom: 10px;" id="LAY_lay_add">
            <button type="button" class="layui-btn layui-btn-danger" id="doMultiDelete">
                <i class="layui-icon layui-icon-delete"></i> 批量删除
            </button>
            <button class="layui-btn layuiadmin-btn-role " data-type="add" id="add">
                <i class="layui-icon layui-icon-add-circle-fine"></i> 添加
            </button>

            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="nickname"  placeholder="请输入用户昵称" autocomplete="off"
                       class="layui-input">
            </div>

            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" lay-filter="search" lay-submit>
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>
    </div>
    <table id="Lay_back_table" lay-filter="Lay_back_table"></table>

</div>

<script type="text/html" id="updateAndDelete">

    <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">
        <i class="layui-icon layui-icon-edit"></i>修改
    </button>
    <button type="button" class="layui-btn layui-btn-danger" lay-event="del">
        <i class="layui-icon layui-icon-delete"></i>删除
    </button>

</script>

<script type="text/javascript">
    layui.use(['table', "layer", 'form'], function () {
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var $ = layui.jquery;

        var layTab = table.render({
            elem: '#Lay_back_table',
            url: '${pageContext.request.contextPath}/address/selectAll', //数据接口
            page: true,
            limit: 10,
            limits: [10, 20, 30],
            autoSort: true,

            cols: [[
                {type: "checkbox"},
                {title: "用户昵称", field: "nickname",sort:true,  align: "center"},
                {title: "收货人姓名", field: "consignee",sort:true,  align: "center"},

                {title: "邮政编码", field: "zipcode", align: "center"},
                {title: "电话", field: "telephone", align: "center"},
                {title: "移动电话", field: "mobile", align: "center"},
                {title: "准确地址", field: "address", align: "center",width: 500},
                {
                    title: "状态", width: 130, align: "center",
                    templet: function (d) {
                        if (d.astatus == 1) {
                            return '<button type="button" class="layui-btn  layui-btn-normal"  lay-event="yn">启用</button>';
                        } else {
                            return '<button type="button" class="layui-btn  layui-btn-danger"  lay-event="yn">禁用</button>';
                        }
                    }
                },

                {title: "操作", templet: "#updateAndDelete",width:300, align: "center"}
            ]]
        });

        //刷新表格方法
        function tableReload() {
            table.reload('Lay_back_table', {});
        };

        //监听行工具事件
        table.on('tool(Lay_back_table)', function (obj) {
            var data = obj.data;
            // alert(data.addressId);
            if (obj.event === 'edit') {
                layer.open({
                    title: "修改",
                    content: "${pageContext.request.contextPath}/address/selectOne?aid="+data.addressId,
                    type: 2,
                    maxmin: true,
                    area: ['690px', '400px'],
                    end: function () {
                        window.location.reload();
                    }
                });
            }
            else if (obj.event === 'yn') {
                var str = obj.data.astatus == 0 ? "启用" : "禁用";
                var state = obj.data.astatus == 1 ? 0 : 1;
                layer.confirm('确认要[' + str + ']这个地址吗?', function (index) {
                    $.ajax({
                        url: '${pageContext.request.contextPath}/address/updateStatus',
                        data: {"addressId": data.addressId, "astatus": state},
                        type: 'post',
                        dataType: 'json',
                        success: function (data) {
                            tableReload();
                            layer.msg("操作成功", {time: 1000, icon: 6});
                        }, error: function (data) {
                            tableReload();
                            layer.msg("操作失败", {time: 1000, icon: 5});
                        }
                    });
                    layer.close(index);
                });
            }
            else if (obj.event === "del"){
                layer.confirm('确定要删除吗？', {icon: 3, title: '确认删除'}, function (index) {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/address/deleteOne",
                        data: "aid=" + data.addressId,
                        success: function (data) {
                            layer.alert(data.msg, {time: 3000});
                            table.reload("Lay_back_table");
                        },
                        error: function (data) {
                            table.reload("Lay_back_table");
                            layer.msg("操作失败", {time: 1000, icon: 5});
                        }
                    });
                });
            }
        });

        //监听搜索
        form.on('submit(search)', function (data) {
            var field = data.field;
            // console.log(field);
            //执行重载
            layTab.reload({
                where: field
            });
        });
        //打开添加窗口
        $("#add").click(function () {
            layer.open({
                title: "添加会员地址",
                content: "${pageContext.request.contextPath}/address/tocustomerAddressEdit",
                type: 2,
                // maxmin: true,
                area: ['690px', '400px'],
                end: function () {
                    window.location.reload();
                }
            });
        });
        //批量删除方法
        $("#doMultiDelete").click(function () {
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据?", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].addressId;
                    }
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/address/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert(data.msg, {time: 3500});
                            //刷新table
                            table.reload("Lay_back_table");
                        }
                    })
                });
            }
        })
    });
</script>
</body>
</html>
