<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/9/25
  Time: 10:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../meta.jsp" %>
<html>
<head>
    <title>省市区管理</title>
    <link rel="stylesheet" href="${basepath}layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="${basepath}layuiadmin/modules/treetable-lay/treetable.css">
    <script type="text/javascript" src="${basepath}js/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="${basepath}js/cookies.js"></script>
    <script type="text/javascript" src="${basepath}layuiadmin/layui/layui.js"></script>
    <script type="text/javascript" src="${basepath}layuiadmin/modules/treetable-lay/treetable.js"></script>
    <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>

</head>
<body>
<div class="layui-card-body">
    <div style="padding-bottom: 10px;" id="LAY_lay_add">
        <div class="layui-btn-group">
            <button class="layui-btn" id="btn-expand">全部展开</button>
            <button class="layui-btn" id="btn-fold">全部折叠</button>
            <button class="layui-btn" id="btn-refresh">刷新表格</button>
        </div>

        &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="search" id="Lay_toSearch_input" placeholder="请输入地区名称" autocomplete="off"
                   class="layui-input">
        </div>
        <div class="layui-input-inline" style="width: 100px;">
            <button type="button" class="layui-btn layui-btn-normal" id="btn-search"<%-- onclick="doSearch()"--%>>
                <i class="layui-icon layui-icon-search"></i> 搜索
            </button>
        </div>
        &nbsp;

    </div>

    <%--树形表格--%>
    <table class="layui-table" id="Lay_treeTable" lay-filter="Lay_treeTable"></table>


</div>

<script type="text/html" id="updateAndDelete">
    <button type="button" class="layui-btn  layui-btn-normal" onclick="toOpenUpdateLayer('{{d.classId}}')">
        <i class="layui-icon layui-icon-edit"></i>修改
    </button>
    <button type="button" class="layui-btn layui-btn-danger" onclick="doDelete('{{d.classId}}')">
        <i class="layui-icon layui-icon-delete"></i> 删除
    </button>
</script>


<script type="text/javascript">

    layui.config({
        base: '${pageContext.request.contextPath}/layuiadmin/modules/' //静态资源所在路径
    }).extend({
        treetable: 'treetable-lay/treetable'
    }).use(['treetable', 'table', 'layer'], function () {
        var treetable = layui.treetable;
        var layer = layui.layer;
        var table = layui.table;
        var $ = layui.jquery;

        var re;
        // 渲染表格
        var reginrenderTable = function () {
            layer.load(3);
            re = treetable.render({
                elem: '#Lay_treeTable',
                url: '${basepath}region/selectTreeTable',
                treeColIndex: 1,          // 树形图标显示在第几列
                treeSpid: 0,             // 最上级的父级id
                treeIdName: 'id',       // 	id字段的名称
                treePidName: 'pid',    // 	pid字段的名称
                treeDefaultClose: true,     //是否默认折叠
                page: false,
                loading: true,    //是否显示加载条
                //treeLinkage: true,      //父级展开时是否自动展开所有子级
                cols: [[
                    {type: 'numbers'},
                    {title: "区域名称", field: "name",style:"text-align:left"},
                    {title: "地名简称", field: "sname",align:"center"},
                    {title: "区域编码", field: "citycode",align:"center"},
                    {title: "邮政编码", field: "yzcode",align:"center"},
                    {title: "组合名称", field: "mername",align:"center"},
                    {title: "经度", field: "lng",align:"center"},
                    {title: "维度", field: "lat",align:"center"},
                    {title: "拼音", field: "pinyin",align:"center"}
                    //{title: "操作", templet: "#updateAndDelete"}
                ]],
                done: function () {
                    layer.closeAll('loading');
                }

            })
        };

        reginrenderTable();
        //展开所有
        $('#btn-expand').click(function () {
            //alert(0)
            layer.load();
            treetable.expandAll('#Lay_treeTable');
        });
        //折叠所有
        $('#btn-fold').click(function () {
            // alert(1)
            treetable.foldAll('#Lay_treeTable');
        });
        //刷新表格
        $('#btn-refresh').click(function () {
            reginrenderTable();
        });

        $('#btn-search').click(function () {
            var keyword = $('#Lay_toSearch_input').val();
            //alert(keyword);
            // var searchName = $('#Lay_toSearch_input').val();
            var searchCount = 0;
            $('#Lay_treeTable').next('.treeTable').find('.layui-table-body tbody tr td').each(function () {
                $(this).css('background-color', 'transparent');
                var text = $(this).text();
                if (keyword != '' && text.indexOf(keyword) >= 0) {
                    $(this).css('background-color', 'rgba(250,230,160,0.5)');
                    if (searchCount == 0) {
                        treetable.expandAll('#Lay_treeTable');
                        $('html,body').stop(true);
                        $('html,body').animate({scrollTop: $(this).offset().top - 150}, 500);
                    }
                    searchCount++;
                }
            });
            if (keyword == '') {
                layer.msg("请输入搜索内容", {icon: 5});
            } else if (searchCount == 0) {
                layer.msg("没有匹配结果", {icon: 5});
            }
        });
    });




</script>

</body>
</html>
