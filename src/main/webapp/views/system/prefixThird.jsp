<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/9/17
  Time: 17:25
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../meta.jsp" %>
<html>
<head>
    <title>第三方登录管理</title>
    <link rel="stylesheet" href="${basepath}layuiadmin/layui/css/layui.css" media="all">
    <script type="text/javascript"  src="${basepath}js/jquery-1.12.0.min.js"></script>
    <script type="text/javascript"  src="${basepath}js/cookies.js"></script>
    <script type="text/javascript" src="${basepath}layuiadmin/layui/layui.js"></script>
    <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>

</head>
<body>
<div class="layui-card-body">
    <div style="padding-bottom: 10px;" id="LAY_lay_add">
        <button type="button" class="layui-btn layui-btn-danger" onclick="doMultiDelete()">
            <i class="layui-icon layui-icon-delete"></i> 批量删除
        </button>
        <button class="layui-btn layuiadmin-btn-role " data-type="add" onclick="toOpenAddLayer()">
            <i class="layui-icon layui-icon-add-circle-fine"></i> 添加
        </button>
        &nbsp;
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="search" id="Lay_toSearch_input" placeholder="请输入第三方应用名" autocomplete="off"
                   class="layui-input">
        </div>
        <div class="layui-input-inline" style="width: 100px;">
            <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                <i class="layui-icon layui-icon-search"></i> 搜索
            </button>
        </div>
    </div>


    <table id="Lay_back_table" lay-filter="Lay_back_table"></table>


</div>

<script type="text/html" id="updateAndDelete">
    <button type="button" class="layui-btn  layui-btn-normal" onclick="toOpenUpdateLayer('{{d.id}}')">
        <i class="layui-icon layui-icon-edit"></i>修改
    </button>
    <button type="button" class="layui-btn layui-btn-danger" onclick="doDelete('{{d.id}}')">
        <i class="layui-icon layui-icon-delete"></i> 删除
    </button>
</script>

<%--弹出层--%>

<form id="addForm" class="layui-form">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0;display: none">
        <div class="layui-form-item">
            <label class="layui-form-label">会员ID</label>
            <div class="layui-input-block">
                <input name="customerId" id="customerId" class="layui-input">
                <input name="id" id="id" lay-type="hide" type="hidden" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">第三方应用</label>
            <div class="layui-input-block">
                <input name="platform" id="platform" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">第三方唯一ID</label>
            <div class="layui-input-block">
                <input name="openid" id="openid" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">第三方会员昵称</label>
            <div class="layui-input-block">
                <input name="openname" id="openname" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">AccessToken</label>
            <div class="layui-input-block">
                <input name="accessToken" id="accessToken" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">RefreshToken</label>
            <div class="layui-input-block">
                <input name="refreshToken" id="refreshToken" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">有效期</label>
            <div class="layui-input-block">
                <input name="expiresIn" id="expiresIn" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item"  style="text-align: right">
            <button class="layui-btn " lay-submit lay-filter="LAY-sysconfig-submit" id="LAY-sysconfig-submit">添加</button>
            <button lay-submit  lay-filter="update" class="layui-btn" id="updateSubmitBtn">修改</button>

        </div>
    </div>
</form>

<script type="text/javascript">
    layui.use(['table',"layer", 'form'], function () {
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;

        table.render({
            elem: '#Lay_back_table',
            url: '${pageContext.request.contextPath}/prefixThird/selectAll', //数据接口
            page: true,
            limit: 10,
            limits: [10, 20, 30],
           // width: 'auto',
            //toolbar: "#LAY_lay_add",
            cols: [[
                {type: "checkbox"},
                {title: "会员ID", field: "customerId",align:"center"},
                {title: "第三方应用", field: "platform",align:"center"},
                {title: "第三方唯一ID", field: "openid",align:"center"},
                {title: "第三方会员昵称", field: "openname",align:"center",edit:"radio"},
                {title: "AccessToken", field: "accessToken",align:"center"},
                {title: "RefreshToken", field: "refreshToken",align:"center"},
                {title: "有效期", field: "expiresIn",align:"center"},
                {title: "创建时间", field: "createtime",align:"center",templet:'<div>{{# if(d.createtime!=null){ }} {{ layui.util.toDateString(d.createtime,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'},
                {title: "更新时间", field: "updatetime",align:"center",templet:'<div>{{# if(d.updatetime!=null){ }} {{ layui.util.toDateString(d.updatetime,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'},
                {title: "登录时间", field: "logintime",align:"center",templet:'<div>{{# if(d.logintime!=null){ }} {{ layui.util.toDateString(d.logintime,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'},
                {title: "过期时间", field: "expiretime",align:"center",templet:'<div>{{# if(d.expiretime!=null){ }} {{ layui.util.toDateString(d.expiretime,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'},
                {title: "操作", templet: "#updateAndDelete",align:"center"}
            ]]

        });
    });

    //搜索操作
    function doSearch() {
        //1.获取到输入框中输入的内容
        var searchName = $('#Lay_toSearch_input').val();
        //发送请求，并且接收数据
        layui.use('table', function () {
            var table = layui.table;
            table.reload('Lay_back_table', {
                where: {"platform": searchName}
            });
        });
    }

    //执行编辑修改
    function toOpenUpdateLayer(id) {
        //1.获取当前行数据===》发送ajax请求，获取当前行数据
        $.ajax({
            url:"${pageContext.request.contextPath}/prefixThird/selectOne",
            data:"id="+id,
            success:function(data){
                $("#customerId").val(data.customerId);
                $("#platform").val(data.platform);
                $("#openid").val(data.openid);
                $("#openname").val(data.openname);
                $("#accessToken").val(data.accessToken);
                $("#refreshToken").val(data.refreshToken);
                $("#expiresIn").val(data.expiresIn);
                $("#createtime").val(data.createtime);
                $("#updatetime").val(data.updatetime);
                $("#logintime").val(data.logintime);
                $("#expiretime").val(data.expiretime);
            }
        });

        //2.把数据填充到修改弹出层中==>弹出层显示
        layui.use(['layer','form','table'], function(){
            var form = layui.form;
            var layer = layui.layer;
            var table = layui.table;

            layer.open({
                title:"修改配置",
                content: $("#layuiconfig-form-role"),
                type:1,
                maxmin:true,
                area: ['500px', '480px']
            });

            $("#LAY-sysconfig-submit").hide();
            $("#updateSubmitBtn").show();


            //3.提交表单
            form.on("submit(update)",function(data){
                // console.log(data);
                $.ajax({
                    url:"${pageContext.request.contextPath}/dedesysconfig/update",
                    data:data.field,
                    type:"post",
                    //4.接收后台修改响应回来的数据；关闭弹出层、提示修改信息、刷新table
                    success:function(data){
                        //1.关闭掉添加弹出层
                        layer.closeAll('page');
                        //2.提示修改成功
                        layer.alert("修改"+data.msg,{time:3000});
                        //刷新table
                        table.reload("Lay_back_table");
                    }
                });
                return false;//阻止跳转；
            })
        });

    }

    //执行添加
    function toOpenAddLayer() {
        layui.use(["form","layer","table"],function () {
            var form = layui.form;
            var layer = layui.layer;
            var table = layui.table;


            layer.open({
                title: "添加配置",
                content: $("#layuiconfig-form-role"),
                type: 1,
                maxmin: true,
                area: ['500px', '480px'],
                end:function(){
                    window.location.reload();
                }

            });

            $("#updateSubmitBtn").hide();
            $("#LAY-sysconfig-submit").show();

            //当点击提交按钮的时候，会进入到这个函数
            form.on("submit(LAY-sysconfig-submit)",function(data){
                // console.log(data);
                $.ajax({
                    url:"${pageContext.request.contextPath}/prefixThird/add",
                    data:data.field,
                    type:"post",
                    success:function(data){
                        //1.关闭掉添加弹出层
                        layer.closeAll('page');
                        //2.提示添加成功
                        layer.alert("添加"+data.msg,{time:3000});
                        //3.刷新table
                        table.reload("Lay_back_table");
                    }
                });
                return false;//阻止跳转；
            })
        })
    }

    function doDelete(id) {
        //确认；如果点击确认删除；否则不删除
        layui.use(['layer','table'], function(){
            var table = layui.table;
            layer.confirm('确定要删除吗？', {icon: 3, title:'确认删除'}, function(index){
                $.ajax({
                    url:"${pageContext.request.contextPath}/prefixThird/deleteOne",
                    data:"id="+id,
                    success:function(data){
                        layer.alert("删除"+data.msg,{time:2000});
                        table.reload("Lay_back_table");
                        layer.close(index);
                    }
                })
            });
        });
    }

    function doMultiDelete() {
        //获取到选中的内容的id===》table模块中找方法
        layui.use(['layer', 'table'], function () {
            var table = layui.table;
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].id;
                    }
                    console.log("ids==="+ids);
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/prefixThird/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert("删除"+data.msg, {time: 2000});
                            //刷新table
                            table.reload("Lay_back_table");
                        }
                    })
                });
            }
        });
    }


</script>

</body>
</html>
