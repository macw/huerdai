<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/9/25
  Time: 15:39
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../meta.jsp" %>
<html>
<head>
    <title>短信模板管理</title>
    <link rel="stylesheet" href="${basepath}layuiadmin/layui/css/layui.css" media="all">
    <script type="text/javascript"  src="${basepath}js/jquery-1.12.0.min.js"></script>
    <script type="text/javascript"  src="${basepath}js/cookies.js"></script>
    <script type="text/javascript" src="${basepath}layuiadmin/layui/layui.js"></script>
    <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>

</head>
<body>
<div class="layui-card-body">
    <div style="padding-bottom: 10px;" id="LAY_lay_add">
        <button type="button" class="layui-btn layui-btn-danger" onclick="doMultiDelete()">
            <i class="layui-icon layui-icon-delete"></i> 批量删除
        </button>
        <button class="layui-btn layuiadmin-btn-role " data-type="add" onclick="toOpenAddLayer()">
            <i class="layui-icon layui-icon-add-circle-fine"></i> 添加
        </button>
        &nbsp;
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="search" id="Lay_toSearch_input" placeholder="请输入模板标题" autocomplete="off"
                   class="layui-input">
        </div>
        <div class="layui-input-inline" style="width: 100px;">
            <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                <i class="layui-icon layui-icon-search"></i> 搜索
            </button>
        </div>
    </div>


    <table id="Lay_back_table" lay-filter="Lay_back_table"></table>


</div>

<script type="text/html" id="updateAndDelete">
    <button type="button" class="layui-btn  layui-btn-normal" onclick="toOpenUpdateLayer('{{d.id}}')">
        <i class="layui-icon layui-icon-edit"></i>修改
    </button>
    <button type="button" class="layui-btn layui-btn-danger" onclick="doDelete('{{d.id}}')">
        <i class="layui-icon layui-icon-delete"></i> 删除
    </button>
</script>

<%--弹出层--%>

<form id="addForm" class="layui-form">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0;display: none">
        <div class="layui-form-item" id="demo0">
            <label class="layui-form-label">模板id</label>
            <div class="layui-input-block">
                <input name="id" id="id" class="layui-input">
               <%-- <input name="id" id="id" lay-type="hide" type="hidden" class="layui-input">--%>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">模板标题</label>
            <div class="layui-input-block">
                <input name="title" id="title" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">模板内容</label>
            <div class="layui-input-block">
                <input name="content" id="content" class="layui-input">
            </div>
        </div>


        <div class="layui-form-item">
            <label class="layui-form-label">备注</label>
            <div class="layui-input-block">
                <input name="memo" id="memo" class="layui-input">
            </div>
        </div>


        <div class="layui-form-item"  style="text-align: right">
            <button class="layui-btn " lay-submit lay-filter="LAY-sysconfig-submit" id="LAY-sysconfig-submit">添加</button>
            <button lay-submit  lay-filter="update" class="layui-btn" id="updateSubmitBtn">修改</button>

        </div>
    </div>
</form>

<script type="text/javascript">
    layui.use(['table',"layer", 'form'], function () {
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;

        table.render({
            elem: '#Lay_back_table',
            url: '${pageContext.request.contextPath}/megtemplate/selectAll', //数据接口
            page: true,
            limit: 10,
            limits: [10, 20, 30],
            // width: 'auto',
            //toolbar: "#LAY_lay_add",
            cols: [[
                {type: "checkbox"},
                {title: "模板id", field: "id",align:"center"},
                {title: "模板标题", field: "title",align:"center"},
                {title: "模板内容", field: "content",align:"center"},
               // {title: "状态", field: "status",edit:"radio"},
                {title: "备注", field: "memo",align:"center"},
                {title: "相关时间",align:"center", field: "relevantTime",templet:'<div>{{# if(d.relevantTime!=null){ }} {{ layui.util.toDateString(d.relevantTime,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'},
                {title: "创建者名称", field: "createdName",align:"center"},
                {title: "创建时间", field: "creationDate",align:"center",templet:'<div>{{# if(d.creationDate!=null){ }} {{ layui.util.toDateString(d.creationDate,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'},
                {title: "最后更新人名称", field: "lastUpdateName",align:"center"},
                {title: "最后更新日期", field: "lastUpdateDate",align:"center",templet:'<div>{{# if(d.lastUpdateDate!=null){ }} {{ layui.util.toDateString(d.lastUpdateDate,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'},
                {title: "操作", templet: "#updateAndDelete",align:"center"}
            ]]

        });
    });

    //搜索操作
    function doSearch() {
        //1.获取到输入框中输入的内容
        var searchName = $('#Lay_toSearch_input').val();
        //发送请求，并且接收数据
        layui.use('table', function () {
            var table = layui.table;
            table.reload('Lay_back_table', {
                where: {"title": searchName}
            });
        });
    }

    //执行编辑修改
    function toOpenUpdateLayer(id) {
        //1.获取当前行数据===》发送ajax请求，获取当前行数据
        $.ajax({
            url:"${pageContext.request.contextPath}/megtemplate/selectOne",
            data:"aid="+id,
            success:function(data){
                $("#id").val(data.id);
                $("#demo0").hide();
                $("#title").val(data.title);
                $("#content").val(data.content);
                //$("#status").val(data.status);
                $("#memo").val(data.memo);

            }
        });


        //2.把数据填充到修改弹出层中==>弹出层显示
        layui.use(['layer','form','table'], function(){
            var form = layui.form;
            var layer = layui.layer;
            var table = layui.table;

            layer.open({
                title:"修改配置",
                content: $("#layuiconfig-form-role"),
                type:1,
                maxmin:true,
                area: ['500px', '480px']
            });

            $("#LAY-sysconfig-submit").hide();
            $("#updateSubmitBtn").show();


            //3.提交表单
            form.on("submit(update)",function(data){
                // console.log(data);
                $.ajax({
                    url:"${pageContext.request.contextPath}/megtemplate/update",
                    data:data.field,
                    type:"post",
                    //4.接收后台修改响应回来的数据；关闭弹出层、提示修改信息、刷新table
                    success:function(data){
                        //1.关闭掉添加弹出层
                        layer.closeAll('page');
                        //2.提示修改成功
                        layer.alert("修改"+data.msg,{time:3000});
                        //刷新table
                        table.reload("Lay_back_table");
                    }
                });
                return false;//阻止跳转；
            })
        });

    }

    //执行添加
    function toOpenAddLayer() {
        layui.use(["form","layer","table"],function () {
            var form = layui.form;
            var layer = layui.layer;
            var table = layui.table;


            layer.open({
                title: "添加配置",
                content: $("#layuiconfig-form-role"),
                type: 1,
                maxmin: true,
                area: ['500px', '480px'],
                end:function(){
                    window.location.reload();
                }

            });

            $("#updateSubmitBtn").hide();
            $("#LAY-sysconfig-submit").show();

            //当点击提交按钮的时候，会进入到这个函数
            form.on("submit(LAY-sysconfig-submit)",function(data){
                // console.log(data);
                $.ajax({
                    url:"${pageContext.request.contextPath}/megtemplate/addmegtemplate",
                    data:data.field,
                    type:"post",
                    success:function(data){
                        //1.关闭掉添加弹出层
                        layer.closeAll('page');
                        //2.提示添加成功
                        layer.alert("添加"+data.msg,{time:3000});
                        //3.刷新table
                        table.reload("Lay_back_table");
                    }
                });
                return false;//阻止跳转；
            })
        })
    }

    function doDelete(id) {
        //确认；如果点击确认删除；否则不删除
        alert(id);
        layui.use(['layer','table'], function(){
            var table = layui.table;
            var layer = layui.layer;

            layer.confirm('确定要删除吗？', {icon: 3, title:'确认删除'}, function(index){
                $.ajax({
                    url:"${pageContext.request.contextPath}/megtemplate/deleteOne",
                    data:"aid="+id,
                    success:function(data){
                        layer.alert("删除"+data.msg,{time:2000});
                        table.reload("Lay_back_table");
                        layer.close(index);
                    }
                })
            });
        });
    }

    function doMultiDelete() {
        //获取到选中的内容的id===》table模块中找方法
        layui.use(['layer', 'table'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var $ = layui.jquery;
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].id;
                    }
                    console.log("ids==="+ids);
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/megtemplate/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert("删除"+data.msg, {time: 2000});
                            //刷新table
                            table.reload("Lay_back_table");
                        }
                    })
                });
            }
        });
    }

</script>
</body>
</html>

