<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/9/29
  Time: 9:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../meta.jsp" %>
<html>
<head>
    <title>黑名单关键字管理</title>
    <link rel="stylesheet" href="${basepath}layuiadmin/layui/css/layui.css" media="all">
    <script type="text/javascript" src="${basepath}js/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="${basepath}js/cookies.js"></script>
    <script type="text/javascript" src="${basepath}layuiadmin/layui/layui.js"></script>
    <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>

</head>
<body>
<div class="layui-card-body">
    <div style="padding-bottom: 10px;" id="LAY_lay_add">
        <button type="button" class="layui-btn layui-btn-danger" onclick="doMultiDelete()">
            <i class="layui-icon layui-icon-delete"></i> 批量删除
        </button>
        <button class="layui-btn layuiadmin-btn-role " data-type="add" id="add">
            <i class="layui-icon layui-icon-add-circle-fine"></i> 添加
        </button>
        &nbsp;
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="search" id="Lay_toSearch_input" placeholder="请输入关键字" autocomplete="off"
                   class="layui-input">
        </div>
        <div class="layui-input-inline" style="width: 100px;">
            <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                <i class="layui-icon layui-icon-search"></i> 搜索
            </button>
        </div>
    </div>


    <table id="Lay_back_table" lay-filter="Lay_back_table"></table>

</div>

<script type="text/html" id="updateAndDelete">
    <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">
        <i class="layui-icon layui-icon-edit"></i>修改
    </button>
    <button type="button" class="layui-btn layui-btn-danger" onclick="dodel({{d.id}})">
        <i class="layui-icon layui-icon-delete"></i>删除
    </button>
</script

    <%--弹出层--%>

<form id="addForm" class="layui-form">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0;display: none">
        <div class="layui-form-item">
            <label class="layui-form-label">关键字</label>
            <div class="layui-input-block">
                <input name="code" id="code" class="layui-input">
                <input name="id" id="id" lay-type="hide" type="hidden" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">类型</label>
            <div class="layui-input-block" >
                <input type="radio" name="type" value="1" title="评论">
                <input type="radio" name="type" value="2" title="描述" >
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">备注</label>
            <div class="layui-input-block">
                <input name="memo" id="memo" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item"  style="text-align: right">
            <button class="layui-btn " lay-submit lay-filter="LAY-sysconfig-submit" id="LAY-sysconfig-submit">添加</button>
            <button lay-submit  lay-filter="update" class="layui-btn" id="updateSubmitBtn">修改</button>

        </div>
    </div>
</form>


<script type="text/html" id="typeTpl">
    {{#  if(d.type == 1){ }}
    <div class="layui-bg-green">评论</div>
    {{#  } else { }}
    <div class="layui-bg-blue">描述</div>
    {{#  } }}
</script>

<script type="text/javascript">
    layui.use(['table', "layer", 'form'], function () {
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var $ = layui.jquery;

        table.render({
            elem: '#Lay_back_table',
            url: '${pageContext.request.contextPath}/blacklistcode/selectAll', //数据接口
            page: true,
            limit: 10,
            limits: [10, 20, 30],
            // width: 'auto',
            //toolbar: "#LAY_lay_add",
            cols: [[
                {type: "checkbox"},
                {title: "关键字", field: "code",align:"center"},
                {title: "类型", field: "type",align:"center",templet:"#typeTpl"},
                {title: "备注", field: "memo",align:"center"},
                {title: "创建人", field: "createdName",align:"center"},
                {
                    title: "创建时间",
                    field: "creationDate",align:"center",
                    templet: '<div>{{# if(d.creationDate!=null){ }} {{ layui.util.toDateString(d.creationDate,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'
                },
                {title: "最后更新人员", field: "lastUpdateName",align:"center"},
                {
                    title: "更新时间",
                    field: "lastUpdateDate",align:"center",
                    templet: '<div>{{# if(d.lastUpdateDate!=null){ }} {{ layui.util.toDateString(d.lastUpdateDate,\'yyyy-MM-dd HH:mm:ss\')  }} {{# } }}</div>'
                },
                {
                    title: "状态", width: 100, align: "center",
                    templet: function (d) {
                        if (d.status == 1) {
                            return '<button type="button" class="layui-btn  layui-btn-normal"  lay-event="yn">启用</button>';
                        } else {
                            return '<button type="button" class="layui-btn  layui-btn-danger"  lay-event="yn">禁用</button>';
                        }
                    }
                },
                {title: "操作", templet: "#updateAndDelete",align:"center"}
            ]]

        });

        //刷新表格方法
        function tableReload() {
            table.reload('Lay_back_table', {});
        };

        //监听行工具事件
        table.on('tool(Lay_back_table)', function (obj) {
            var data = obj.data;
            if (obj.event === 'yn') {
                var str = obj.data.status !=1 ? "启用" : "禁用";
                var state = obj.data.status == 1 ? 0 : 1;
                layer.confirm('确认要[' + str + ']这个关键字吗?', function (index) {
                    $.ajax({
                        url: '${pageContext.request.contextPath}/blacklistcode/updateStatus',
                        data: {"id": data.id, "status": state},
                        type: 'post',
                        dataType: 'json',
                        success: function (data) {
                            tableReload();
                            layer.msg("操作"+data.msg, {time: 1000, icon: 6});
                        }, error: function (data) {
                            tableReload();
                            layer.msg("操作失败", {time: 1000, icon: 5});
                        }
                    });
                    layer.close(index);
                });
            } else if (obj.event === 'edit') {
                $.ajax({
                    url:"${pageContext.request.contextPath}/blacklistcode/selectOne",
                    data:"aid="+data.id,
                    success:function(data){
                        $("#id").val(data.id);
                        $("#code").val(data.code);
                        $("#memo").val(data.memo);
                        $("input[type=radio][name=type][value="+data.type+"]").attr("checked",true);
                        form.render();
                    }
                });

                layer.open({
                    title: "编辑关键字信息",
                    content: $("#layuiconfig-form-role"),
                    type: 1,
                    maxmin: true,
                    area: ['500px', '480px'],
                    end: function () {
                        window.location.reload();
                    }
                });

                $("#LAY-sysconfig-submit").hide();
                $("#updateSubmitBtn").show();
                //3.提交表单
                form.on("submit(update)",function(data){
                    // console.log(data);
                    $.ajax({
                        url:"${pageContext.request.contextPath}/blacklistcode/update",
                        data:data.field,
                        type:"post",
                        //4.接收后台修改响应回来的数据；关闭弹出层、提示修改信息、刷新table
                        success:function(data){
                            //1.关闭掉添加弹出层
                            layer.closeAll('page');
                            //2.提示修改成功
                            layer.alert("修改"+data.msg,{time:3000});
                            //刷新table
                            table.reload("Lay_back_table");
                        }
                    });
                    return false;//阻止跳转；
                })

            }
        });
        $("#add").click(function () {
            layer.open({
                title: "添加黑名单关键字",
                content: $("#layuiconfig-form-role"),
                type: 1,
                // maxmin: true,
                area: ['500px', '480px'],
                end: function () {
                    window.location.reload();
                }
            });
            $("#updateSubmitBtn").hide();
            $("#LAY-sysconfig-submit").show();
            //当点击提交按钮的时候，会进入到这个函数
            form.on("submit(LAY-sysconfig-submit)",function(data){
                // console.log(data);
                $.ajax({
                    url:"${pageContext.request.contextPath}/blacklistcode/add",
                    data:data.field,
                    type:"post",
                    success:function(data){
                        //1.关闭掉添加弹出层
                        layer.closeAll('page');
                        //2.提示添加成功
                        layer.alert("添加"+data.msg,{time:3000});
                        //3.刷新table
                        table.reload("Lay_back_table");
                    }
                });
                return false;//阻止跳转；
            })
        });
    });


    function dodel(id) {
        layui.use(['layer','table'], function () {
            var layer = layui.layer;
            var table = layui.table;
            var $ = layui.jquery;
            layer.confirm('确定要删除吗？', {icon: 3, title: '确认删除'}, function (index) {
                $.ajax({
                    url: "${pageContext.request.contextPath}/blacklistcode/deleteOne",
                    data: "aid=" + id,
                    success: function (data) {
                        layer.alert("删除" + data.msg, {time: 2000});
                        table.reload("Lay_back_table");
                    },
                    error: function (data) {
                        table.reload("Lay_back_table");
                        layer.msg("操作失败", {time: 1000, icon: 5});
                    }
                });
                layer.close(index);
            });
        });
    }

    //搜索操作
    function doSearch() {
        //1.获取到输入框中输入的内容
        var searchName = $('#Lay_toSearch_input').val();
        //发送请求，并且接收数据
        layui.use('table', function () {
            var table = layui.table;
            table.reload('Lay_back_table', {
                where: {"code": searchName}
            });
        });
    }


    function doMultiDelete() {
        //获取到选中的内容的id===》table模块中找方法
        layui.use(['layer', 'table'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var $ = layui.jquery;
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].id;
                    }
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/blacklistcode/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert("删除" + data.msg, {time: 2000});
                            //刷新table
                            table.reload("Lay_back_table");
                        }
                    })
                });
            }
        });
    }

</script>
</body>
</html>

