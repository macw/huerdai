<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../head.jsp" %>
</head>
<body>
 <form id="form" class="layui-form" style="margin-top: 20px;">
 	<input type="hidden" name="insId" value="${dto.insId}">
 	  <div class="layui-form-item">
    	<div class="layui-inline">
		     <label class="layui-form-label">保险名称</label>
   			  <div class="layui-input-inline">
		        <input type="text" name="insName" value="${dto.insName}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    	<div class="layui-inline">
		      <label class="layui-form-label">保险价格</label>
		      <div class="layui-input-inline">
		        <input type="text" name="money" value="${dto.money}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">保险标题</label>
		      <div class="layui-input-inline">
		        <input type="text" name="title" value="${dto.title}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
	    <div class="layui-inline">
		      <label class="layui-form-label">保额</label>
		      <div class="layui-input-inline">
		        <input type="text" name="insuredAmount" value="${dto.insuredAmount}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
			<label class="layui-form-label">标题内容</label>
			<div class="layui-input-block">
		       <textarea placeholder="请输入内容" class="layui-textarea" name="content" lay-verify="required" autocomplete="off" style="width: 515px;">${dto.content}</textarea>
		    </div>
	 </div>
	 <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">三方保险接口id</label>
		      <div class="layui-input-inline">
		        <input type="text" name="openid" value="${dto.openid}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
	    <div class="layui-inline">
		      <label class="layui-form-label">三方保险接口名称</label>
		      <div class="layui-input-inline">
		        <input type="text" name="platform" value="${dto.platform}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    </div>
	 <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">AccessToken</label>
		      <div class="layui-input-inline">
		        <input type="text" name="accessToken" value="${dto.accessToken}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
	    <div class="layui-inline">
		      <label class="layui-form-label">RefreshToken</label>
		      <div class="layui-input-inline">
		        <input type="text" name="refreshToken" value="${dto.refreshToken}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    </div>
     <div class="layui-form-item">
      <div class="layui-inline">
          <label class="layui-form-label">有效期时间</label>
	      <div class="layui-input-inline">
	        <input type="text" class="layui-input" name="expiresBegindate" id="expiresBegindate" lay-verify="required" placeholder="开始时间" value="${dto.expiresStartDate }">
	      </div>
      </div>
      <div class="layui-inline">
          <label class="layui-form-label">有效期时间</label>
	      <div class="layui-input-inline">
	        <input type="text" class="layui-input" name="expiresEnddate" id="expiresEnddate" lay-verify="required" placeholder="结束时间" value="${dto.expiresEndDate }">
	      </div>
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">备注</label>
      <div class="layui-input-inline">
         <input type="tel" name="memo" value="${dto.memo}" lay-verify="required" autocomplete="off" class="layui-input"  style="width: 515px;">
      </div>
    </div>
    <div class="layui-form-item">
	    <div class="layui-input-block">
	      <button type="submit" class="layui-btn" lay-submit="" lay-filter="btnSub" id="subBtn">立即提交</button>
	    </div>
  </div>
 </form>
</body>
 
   

<script type="text/javascript">
		var activce=null;
        layui.use(['table', 'form','laydate'], function () {
            var layer = layui.layer;
            var table = layui.table;
            var form = layui.form;
            var laydate = layui.laydate;
           
          	//日期时间选择器
            laydate.render({
              elem: '#expiresEnddate'
              ,type: 'datetime'
            });
          //日期时间选择器
            laydate.render({
                elem: '#expiresBegindate'
                ,type: 'datetime'
              });
          //监听提交
            form.on('submit(btnSub)', function(data){
            	var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            	$.ajax({
                  	 url:'<%=basePath%>insurance/saveOrUpdate',
                  	 data: data.field,
                  	 type: 'post',
                  	 dataType: 'json',
                  	 success:function(data){
                  		if(data.se){
                  			layer.msg(data.msg, { offset: '15px',icon: 6,time: 1000}, function(){
    	                  		parent.layer.close(index);
    			            	parent.location.reload();
                      		});
                  		}else{
                  			layer.msg(data.msg, { offset: '15px',icon: 5,time: 1000}, function(){
    	                  		parent.layer.close(index);
    			            	parent.location.reload();
                      		});
                  		}
                  	 },error:function(data){
                  		layer.msg('操作失败', { offset: '15px',icon: 5,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                  		});
                  	 }
                 });
            	 
                 return false;
            });
            activce = {
           	   chooseSpot:function(){
	           		layer.open({
	           			  type: 2,
			    		  title: '选择景点',
			    		  shadeClose: true,
			    		  shade: 0.8,
			    		  area: ['100%', '100%'],
			    		  content: '<%=basePath%>scenic/spot/multiple/choosePage'
	           		});
           	   }
            		
            }
        });
        
        $("#chooseSpot").click(function(){
        	activce.chooseSpot();
        });
         
    </script>
</html>

