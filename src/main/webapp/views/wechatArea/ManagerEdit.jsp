<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/19
  Time: 10:31
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../head.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>微信群管理</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>
<body>

<form id="addForm" class="layui-form" enctype="multipart/form-data">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0;">


        <div class="layui-form-item" style="width: 680px">
            <label class="layui-form-label">地址</label>
            <div class="layui-input-inline" style="width: 160px;">
                <select name="regionId" id="province" lay-filter="province" lay-verify="required" lay-search >
                    <option value="">省</option>
                    <c:forEach items="${provinceList}" var="p">
                        <option value="${p.id}" <c:if test="${wr.regionId == p.id}">selected</c:if> >${p.name}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="layui-input-inline" style="width: 160px;">
                <select name="regionCityId" id="city"  lay-filter="city" lay-verify="required" lay-search >
                    <option value="">市</option>
                    <c:forEach items="${cityList}" var="c">
                        <option value="${c.id}" <c:if test="${wr.regionCityId == c.id}">selected</c:if> >${c.name}</option>
                    </c:forEach>
                </select>
            </div>

        </div>



        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">地区群二维码</label>
                <div class="layui-input-block">
                    <button type="button" class="layui-btn" id="upload_customer_pic">
                        <i class="layui-icon">&#xe67c;</i>选择图片
                    </button>
                </div>
            </div>
            <div class="layui-inline">
                <img class="layui-upload-img" name="wechatUrl" src="${wr.wechatUrl}" id="wechatUrl" style="max-width: 200px;max-height: 200px; padding-left: 40px;">
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">群名</label>
                <div class="layui-input-block">
                    <input name="groupName" value="${wr.groupName}" id="groupName"
                           class="layui-input">
                    <input type="hidden" name="id" value="${wr.id}">
                </div>
            </div>

        </div>

        <div class="layui-form-item" style="width: 600px">
            <div class="layui-input-block" style="text-align: right">
                <button type="submit" class="layui-btn" id="btnSub" lay-filter="btnSub" lay-submit>立即提交</button>
            </div>
        </div>
    </div>
</form>


<script type="text/javascript">
    var iscanClick=true;
    layui.use(['form', 'upload', 'layer'], function () {
        var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        var layer = layui.layer;
        var $ = layui.jquery;
        var upload = layui.upload;

        form.on('select(province)', function(data){
            $("#district").empty();
            var str="<option value=''>区</option>";
            $("#district").append(str);
            str="<option value=''>市</option>";
            $.ajax({
                url:'<%=basePath%>region/ajaxRegionList?pid='+data.value,
                type: 'post',
                dataType: 'json',
                success:function(d){
                    $("#city").empty();
                    if(d.data.length>0){
                        $(d.data).each(function(i,t){
                            str+="<option value='"+t.id+"'>"+t.name+"</option>"
                        })
                    }
                    $("#city").append(str);
                    form.render('select');
                }
            });
        });


        upload.render({
            elem: '#upload_customer_pic', //绑定元素
            accept: "images",
            acceptMime: 'image/*',//打开文件选择框时,只显示图片文件
            auto: false,  //是否选完文件后自动上传。默认值：true
            // bindAction: '#btnSub',
            choose : function(obj) {
                obj.preview(function(index, file, result) {
                    $('#wechatUrl').attr('src', result);

                })
            }
        });

         /*   //获取当前网址，如： https://localhost:8083/uimcardprj/share/meun.jsp
            var curWwwPath=window.document.location.href;
            alert(curWwwPath)
            //获取主机地址之后的目录，如： uimcardprj/share/meun.jsp
            var pathName=window.document.location.pathname;
            alert(pathName)
            var pos=curWwwPath.indexOf(pathName);
            //获取主机地址，如： https://localhost:8083
            var localhostPaht=curWwwPath.substring(0,pos);
            alert(localhostPaht)
            //获取带"/"的项目名，如：/uimcardprj
            var projectName=pathName.substring(0,pathName.substr(1).indexOf('/')+1);
            alert(projectName);*/

        //监听提交
        form.on('submit(btnSub)', function (data) {
            if(iscanClick){
                distCanClick();
                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                var formData = new FormData(document.getElementById("addForm"));
                console.log(data.field);
                $.ajax({
                    url: '${pageContext.request.contextPath}/api/wechatRegionCode/saveOrUpdate',
                    data: formData,
                    type: 'post',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data.code==200) {
                            layer.msg('操作成功', {
                                icon: 6,
                                time: 1000
                            }, function () {
                                parent.layer.close(index);
                                parent.layui.table.reload('Lay_back_table', {});
                            });
                        }else {
                            layer.msg('操作失败,请重新上传', {
                                icon: 5,
                                time: 1000
                            }/*, function () {
                                parent.layer.close(index);
                                parent.layui.table.reload('Lay_back_table', {});
                            }*/);
                        }
                    },
                    error: function (data) {
                        layer.msg('操作失败', {
                            icon: 5,
                            time: 1000
                        }/*, function () {
                            parent.layer.close(index);
                            parent.layui.table.reload('Lay_back_table', {});
                        }*/);
                    }
                });
            };

            return false;
        });
    });

    //定时器，三秒内只能点一次
    function distCanClick(){
        iscanClick=false;
        setTimeout(function(){
            iscanClick=true;

        }, 3000)
    }
</script>
</body>
</html>


