<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/19
  Time: 10:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../head.jsp" %>

<html>
<head>
    <title>注册</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/form.css"/>

    <meta
             charset="UTF-8"  />
            
    <meta
             name="viewport"
             content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"  />
            
    <meta
             http-equiv="X-UA-Compatible"  content="ie=edge"  />
            
    <meta
             name="HandheldFriendly"  content="true">
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        body {
            background: #fff;
            max-width: 640px;
            min-width: 320px;
            margin: 0 auto;
            position: relative;
            font-family: '微软雅黑';
            color: #333;
        }

        html {
            font-size: 16px;
        }

        ul, ol {
            list-style: none;
        }

        a {
            text-decoration: none;
            color: #666;
            outline: none;
            -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
        }

        .section {
            padding: 10px;
            margin: 0 auto;
        }

        .section h6 {
            margin: 10px 0;
            font-size: 15px;
        }

        .section h6 input {
            width: 70%;
            height: 30px;
            line-height: 30px;
            border-radius: 5px;
            border: 1px solid #eee;
        }

        .section button {
            background: #6faaf5;
            color: #fff;
            width: 70%;
            height: 30px;
            line-height: 30px;
            border: none;
            border-radius: 5px;
        }

        .section .bottom {
            text-align: center;
            margin: 10px 0;
        }
    </style>
</head>
<body>
<form id="addForm" class="layui-form" enctype="multipart/form-data">
    <div class="section">
        <div class=" bottom">
            <h3>当前地区还没有户二代户外群，填写信息申请创建群吧</h3>
        </div>
        <br/><br/>

        <div class=" bottom">
            <h1>群主申请</h1>
        </div>


        <span class="span1">* 表示必填项</span>
        <div class="uId">
            <label for="uId">登录名称：</label> <input id="uId" type="text"
                                                  name="loginName" class="required" placeholder="6-20位的字母、数字组合"/>
        </div>
        <div class="pwd">
            <label for="pwd">输入密码：</label> <input id="pwd" type="password"
                                                  name="password" class="required" placeholder="6位以上的字母、数字、符号组合"/>
        </div>
        <div class="conPwd">
            <label for="conPwd">确认密码：</label> <input id="conPwd"
                                                     type="password" name="password2" class="required"
                                                     placeholder="确认密码一致哟"/>
        </div>
        <div class="E-mail">
            <label for="mobilePhone">手机号码：</label> <input id="mobilePhone" type="text"
                                                          name="mobilePhone" class="required"
                                                          lay-verify="required|phone" placeholder="如：1234567@qq.com"/>
        </div>
        <div class="trueName">
            <label for="customerName">真实姓名：</label> <input id="customerName"
                                                           type="text" name="customerName" class="required"
                                                           placeholder="如：张三"/>
        </div>
        <div class="trueName">
            <label for="customerName">会员昵称：</label> <input id="nickname"
                                                           type="text" name="nickname" class="required"
                                                           placeholder="请输入您的昵称"/>
        </div>

        <h6>注册完后请联系管理员微信号：qq28323456、cj66188</h6>

        <h6>申请指引：</h6>
        <h6>一、添加管理员审核通过注册</h6>
        <h6>二、创建户二代微信群</h6>
        <h6>三、上传群二维码</h6>
        <div class="bottom">

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button type="submit" class="layui-btn" id="btnSub" lay-filter="btnSub" lay-submit>提交</button>
                </div>
            </div>
        </div>

    </div>


</form>


<script type="text/javascript">
    var iscanClick = true;
    layui.use(['form', 'layer'], function () {
        var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        var layer = layui.layer;
        var $ = layui.jquery;

        //监听提交
        form.on('submit(btnSub)', function (data) {
            if (iscanClick) {
                distCanClick();
                $("form :input.required").trigger('blur');
                var numError = $('form .onError').length;
                if (numError) {
                    return false;
                }
                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                var formData = new FormData(document.getElementById("addForm"));
                console.log(data.field);
                var field = data.field;
                $.ajax({
                    type: "get",
                    async: false,
                    url: '${pageContext.request.contextPath}/api/customerInfoApi/wechatGroupReg222',
                    data: field,
                    dataType: "jsonp",//数据类型为jsonp
                    jsonp: "jsonpCallback",//服务端用于接收callback调用的function名的参数
                    success: function (data) {
                        console.log("Result:" + data.result)
                        if (data.result == 1) {
                            layer.msg('注册成功！', {
                                offset: '15px',
                                icon: 6,
                                time: 1000
                            }, function () {
                                window.location = "${pageContext.request.contextPath}/views/wechatArea/agentLogin.jsp";
                            });
                        } else {
                            layer.msg('注册失败', {
                                offset: '15px',
                                icon: 5,
                                time: 3000
                            });
                        }
                    },
                    error: function () {
                        layer.msg('操作失败', {
                            offset: '15px',
                            icon: 5,
                            time: 3000
                        });
                    }
                });
            }
            return false;
        });
    });

    //定时器，三秒内只能点一次
    function distCanClick() {
        iscanClick = false;
        setTimeout(function () {
            iscanClick = true
        }, 3000)
    }


    $(function () {

        $("form :input.required").each(function () {
            var $required = $("<strong class='high'>*</strong>");
            $(this).parent().append($required);
        })


        $("form :input")
            .blur(
                function () {
                    var $parent = $(this).parent();
                    $parent.find(".onError").remove();
                    //验证用户名
                    if ($(this).is("#uId")) {
                        var uPattern = /^[a-zA-Z0-9_-]{6,20}$/;
                        if (this.value == ""
                            || (this.value != "" && !uPattern
                                .test(this.value))) {
                            var errorMsg = '请确认输入的为6到20位的字母和数字组合';
                            $parent
                                .append('<span class="formtips onError">'
                                    + errorMsg + '</span>');
                        } else {
                            $.ajax({
                                url: '${pageContext.request.contextPath}/api/customerInfoApi/valCustomerName?loginname=' + this.value,
                                dataType: 'json',
                                type: 'post',
                                success: function (data) {
                                    if (data.code == 1) {
                                        var Msg = "该用户名已被占用！"
                                        $parent
                                            .append('<span class="formtips onError">'
                                                + Msg + '</span>');
                                    }
                                }
                            });

                            var okMsg = "输入正确！"
                            $parent
                                .append("<span class='formtips onSuccess'>"
                                    + okMsg + "</span>");
                        }
                    }
                    //校验手机号
                    if ($(this).is("#mobilePhone")) {
                        var uPattern = /^1(3|4|5|7|8)\d{9}$/;
                        if (this.value == ""
                            || (this.value != "" && !uPattern
                                .test(this.value))) {
                            var errorMsg = '手机号码不合法';
                            $parent
                                .append('<span class="formtips onError">'
                                    + errorMsg + '</span>');
                        } else {
                            $.ajax({
                                url: '${pageContext.request.contextPath}/api/customerInfoApi/valCustomerName?mobilePhone=' + this.value,
                                dataType: 'json',
                                type: 'post',
                                success: function (data) {
                                    if (data.code == 1) {
                                        var Msg = "该手机号已被占用！";
                                        $parent
                                            .append('<span class="formtips onError">'
                                                + Msg + '</span>');
                                    }
                                }
                            });

                            var okMsg = "输入正确！"
                            $parent
                                .append("<span class='formtips onSuccess'>"
                                    + okMsg + "</span>");
                        }
                    }


                    //验证密码
                    if ($(this).is("#pwd")) {
                        var pPattern = /^(?![a-zA-z]+$)(?!\d+$)(?![!@#$%^&*]+$)[a-zA-Z\d!@#$%^&*]+$/;
                        //var pPattern = /^[a-zA-Z0-9_-]{6,8}$/;
                        if (this.value == ""
                            || (this.vlaue != "" && !pPattern
                                .test(this.value))) {
                            var errorMsg = '请输入6位以上的字母、数字、符号组合';
                            $parent
                                .append('<span class="formtips onError">'
                                    + errorMsg + '</span>');
                        } else {
                            var okMsg = "输入正确！"
                            $parent
                                .append("<span class='formtips onSuccess'>"
                                    + okMsg + "</span>");
                        }
                    }
                    //确认密码
                    if ($(this).is("#conPwd")) {
                        var pwd1 = $("#pwd").val();
                        var pwd2 = $("#conPwd").val();
                        if (this.value == "" || pwd1 != pwd2) {
                            var errorMsg = '密码不一致或未填写哟！';
                            $parent
                                .append('<span class="formtips onError">'
                                    + errorMsg + '</span>');
                        } else {
                            var okMsg = "输入正确！"
                            $parent
                                .append("<span class='formtips onSuccess'>"
                                    + okMsg + "</span>");
                        }
                    }

                    //验证姓名
                    if ($(this).is("#trueName")) {
                        var nPattern = /^[\u4E00-\u9FA5]{2,4}$/;
                        if (this.value == ''
                            || (this.value != '' && !nPattern
                                .test(this.value))) {
                            var errorMsg = '请确认输入的为真实的姓名';
                            $parent
                                .append('<span class="formtips onError">'
                                    + errorMsg + '</span>');
                        } else {
                            var okMsg = '输入正确！';
                            $parent
                                .append('<span class="formtips onSuccess">'
                                    + okMsg + '</span>');
                        }
                    }
                })


    })

</script>

</body>
</html>
