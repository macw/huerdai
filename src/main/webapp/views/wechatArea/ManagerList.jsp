<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/22
  Time: 10:11
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../head.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>微信群管理</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
</head>
<body>

<div class="layui-card-body">
    <div class="layui-form">
        <div style="padding-bottom: 10px;" id="LAY_lay_add">
            <button type="button" class="layui-btn layui-btn-danger" id="doMultiDelete">
                <i class="layui-icon layui-icon-delete"></i> 批量删除
            </button>
            <button class="layui-btn layuiadmin-btn-role " data-type="add" id="add">
                <i class="layui-icon layui-icon-add-circle-fine"></i> 添加
            </button>

        </div>

    </div>
    <table id="Lay_back_table" lay-filter="Lay_back_table"></table>

</div>

<script type="text/html" id="updateAndDelete">

    <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">
        <i class="layui-icon layui-icon-edit"></i>修改
    </button>
    <button type="button" class="layui-btn layui-btn-danger" lay-event="del">
        <i class="layui-icon layui-icon-delete"></i>删除
    </button>

</script>

<script type="text/javascript">
    layui.use(['table', "layer", 'form'], function () {
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var $ = layui.jquery;

        var layTab = table.render({
            elem: '#Lay_back_table',
            url: '${pageContext.request.contextPath}/api/wechatRegionCode/selectAllByCustomer', //数据接口
            page: true,
            limit: 5,
            limits: [5,10, 20, 30],
            // width: 'auto',
            autoSort: true,
           /* initSort: {
                field: 'createTime' //排序字段，对应 cols 设定的各字段名
                , type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
            },*/
            //text:"您所代理的地区暂无数据，请点击左上角按钮添加群二维码",
            //toolbar: "#LAY_lay_add",
            cols: [[
                {type: "checkbox"},
                {title: "地区", field: "provinceName", align: "center",sort:true,
                    templet: function (d) {
                        if (d.cityName!=null){
                            return '<div>'+d.provinceName+"-"+d.cityName+'</div>';
                        }else {
                            return '<div>'+d.provinceName+'</div>';
                        }
                    }
                },
                {title: "群名", field: "groupName", align: "center"},
                {title: "群二维码", field: "wechatUrl", align: "center",
                    templet: function (d) {
                        return '<img src="' + d.wechatUrl + '">'
                    }
                },
                {title: "代理人", field: "customerName", align: "center"},

                {title: "操作", templet: "#updateAndDelete", align: "center",width:220}
            ]]
        });

        //刷新表格方法
        function tableReload() {
            table.reload('Lay_back_table', {});
        };

        //监听行工具事件
        table.on('tool(Lay_back_table)', function (obj) {
            var data = obj.data;
            if (obj.event === 'edit') {
                layer.open({
                    title: "修改",
                    content: "${pageContext.request.contextPath}/api/wechatRegionCode/toManagerEdit?id="+data.id,
                    type: 2,
                    maxmin: true,
                    area: ['750px', '600px'],
                    end: function () {
                        window.location.reload();
                    }
                });
            }else if (obj.event === "del"){
                layer.confirm('确定要删除吗？', {icon: 3, title: '确认删除'}, function (index) {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/api/wechatRegionCode/deleteById",
                        data: "id=" + data.id,
                        success: function (data) {
                            layer.alert(data.msg, {time: 3000});
                            table.reload("Lay_back_table");
                        },
                        error: function (data) {
                            table.reload("Lay_back_table");
                            layer.msg("操作失败", {time: 1000, icon: 5});
                        }
                    });
                });
            }
        });

        //监听搜索
        form.on('submit(search)', function (data) {
            var field = data.field;
            // console.log(field);
            //执行重载
            layTab.reload({
                where: field
            });
        });
        //打开添加窗口
        $("#add").click(function () {
            layer.open({
                title: "添加区域群信息",
                content: "${pageContext.request.contextPath}/api/wechatRegionCode/toManagerEdit",
                type: 2,
                // maxmin: true,
                area: ['750px', '600px'],
                end: function () {
                    window.location.reload();
                }
            });
        });
        //批量删除方法
        $("#doMultiDelete").click(function () {
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据?", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].id;
                    }
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/api/wechatRegionCode/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert(data.msg, {time: 3500});
                            //刷新table
                            table.reload("Lay_back_table");
                        }
                    })
                });
            }
        })

    });


</script>
</body>
</html>


