<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/10/19
  Time: 10:31
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="../head.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>微信群管理</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>
<body>

<form id="addForm" class="layui-form" enctype="multipart/form-data">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0;">

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px">
                <label class="layui-form-label">区域省份</label>
                <div class="layui-input-block">
                    <select name="regionId" id="province" lay-filter="province"  lay-search >
                        <option value="">省</option>
                        <c:forEach items="${provinceList}" var="p">
                            <option value="${p.id}" <c:if test="${wr.regionId == p.id}">selected</c:if> >${p.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="layui-input-inline" id="Lay_agenid" style="width: 300px">
                <label class="layui-form-label">地级市</label>
                <div class="layui-input-block">
                    <select name="regionCityId" id="city"  lay-filter="city"  lay-search >
                        <option value="">市</option>
                        <c:forEach items="${cityList}" var="c">
                            <option value="${c.id}" <c:if test="${wr.regionCityId == c.id}">selected</c:if> >${c.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>


        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">代理人昵称</label>
                <div class="layui-input-block">
                    <select name="customerId" id="customerId" lay-verify="required" lay-filter="customerId">
                        <option value="">请选择代理人昵称</option>
                        <c:forEach items="${us}" var="l">
                            <option value="${l.customerId }"
                                    <c:if test="${wr.customerId == l.customerId}">selected</c:if> >${l.nickname }</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="layui-input-inline" style="width: 300px">
                <div class="layui-form-item">
                    <label class="layui-form-label">代理人微信号</label>
                    <div class="layui-input-block">
                        <input name="wechatNumber" value="${wr.wechatNumber}" id="wechatNumber"
                               class="layui-input">
                    </div>
                </div>
            </div>
        </div>


        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">地区群二维码</label>
                <div class="layui-input-block">
                    <button type="button" class="layui-btn" id="upload_customer_pic">
                        <i class="layui-icon">&#xe67c;</i>选择图片
                    </button>

                </div>
            </div>
            <div class="layui-inline">
                <img class="layui-upload-img" name="wechatUrl" src="${wr.wechatUrl}" id="wechatUrl" style="max-width: 200px;max-height: 200px; padding-left: 40px;">
            </div>

        </div>

        <div class="layui-form-item">
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">群名</label>
                <div class="layui-input-block">
                    <input name="groupName" value="${wr.groupName}" id="groupName"
                           class="layui-input">
                    <input type="hidden" name="id" value="${wr.id}">
                </div>
            </div>
            <div class="layui-input-inline" style="width: 300px;">
                <label class="layui-form-label">代理人类型</label>
                <div class="layui-input-block">
                    <select name="type" id="type" lay-verify="required" lay-filter="type">
                        <option value="">请选择代理人类型</option>
                        <option value="1" <c:if test="${wr.type==1}">selected</c:if>>区域代理</option>
                        <option value="0" <c:if test="${wr.type==0}">selected</c:if>>省级管理</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="layui-form-item" style="width: 600px">
            <div class="layui-input-block" style="text-align: right">
                <button type="submit" class="layui-btn" id="btnSub" lay-filter="btnSub" lay-submit>立即提交</button>
            </div>
        </div>
    </div>
</form>


<script type="text/javascript">
    layui.use(['form', 'upload', 'layer'], function () {
        var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        var layer = layui.layer;
        var $ = layui.jquery;
        var upload = layui.upload;

        form.on('select(province)', function(data){
            $("#district").empty();
            $("#city").empty();
            var str="<option value=''>区</option>";
            $("#district").append(str);
            str="<option value=''>市</option>";
            if(data.value.length <= 0){
                $("#city").append(str);
                form.render('select');
            }else{
                $.ajax({
                    url:'<%=basePath%>region/ajaxRegionList?pid='+data.value,
                    type: 'post',
                    dataType: 'json',
                    success:function(d){
                        if(d.data.length>0){
                            $(d.data).each(function(i,t){
                                str+="<option value='"+t.id+"'>"+t.name+"</option>"
                            })
                        }
                        $("#city").append(str);
                        form.render('select');
                    }
                });
            }
        });
      
        form.on('select(city)', function(data){
            var str="<option value=''>区</option>";
            $.ajax({
                url:'<%=basePath%>region/ajaxRegionList?pid='+data.value,
                type: 'post',
                dataType: 'json',
                success:function(d){
                    $("#district").empty();
                    if(d.data.length>0){
                        $(d.data).each(function(i,t){
                            str+="<option value='"+t.id+"'>"+t.name+"</option>"
                        })
                    }
                    $("#district").append(str);
                    form.render('select');
                }
            });
        });

        upload.render({
            elem: '#upload_customer_pic', //绑定元素
            accept: "images",
            acceptMime: 'image/*',//打开文件选择框时,只显示图片文件
            auto: false,  //是否选完文件后自动上传。默认值：true
            // bindAction: '#btnSub',
            choose : function(obj) {
                obj.preview(function(index, file, result) {
                    $('#wechatUrl').attr('src', result);

                })
            }
        });



        //监听提交
        form.on('submit(btnSub)', function (data) {
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            var formData = new FormData(document.getElementById("addForm"));
            console.log(data.field);
            $.ajax({
                url: '${pageContext.request.contextPath}/wechatRegionCode/saveOrUpdate',
                data: formData,
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data.code==0){
                        layer.msg('操作成功', {
                            icon: 6,
                            time: 1000
                        }, function () {
                            parent.layer.close(index);
                            parent.layui.table.reload('Lay_back_table', {});
                        });
                    }else {
                        layer.msg('上传失败', {
                            icon: 5,
                            time: 1000
                        })
                    }

                },
                error: function (data) {
                    layer.msg('操作失败', {
                        icon: 5,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                }
            });
            return false;
        });
    });

</script>
</body>
</html>


