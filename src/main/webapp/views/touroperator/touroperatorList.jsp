<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>旅游公司信息</title>
	<%@ include file="../head.jsp" %>
	 <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>
</head>
<body>
<div class="layui-fluid">
    <div class="layui-card-body">
        <div style="padding-bottom: 10px;">
            <div class="layui-input-inline" style="width: 100px;">
                 <button type="button" class="layui-btn add" data-type="add" onclick="add()">添加</button>
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="tName" id="tName" placeholder="商户名称" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="phone" id="phone" placeholder="联系方式" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="createdName" id="createdName" placeholder="创建人/修改人" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>
        <table id="tab" lay-filter="tab"></table>
    </div>
</div>
</body>
 
  <script type="text/html" id="tool">
        <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">编辑</button>
        <button type="button" class="layui-btn layui-btn-danger" lay-event="del">删除</button>
  </script>

<script type="text/javascript">
        var active=null;
        
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            
            table.render({
                elem: '#tab',
                url: '<%=basePath%>touroperator/selectData', //数据接口
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                cols: [[
                    {type: "checkbox"},
                    {title: "商户名称", field: "tName",align:"center",width:200},
                    {title: "联系方式", field: "phone",align:"center",width:120},
                    {title: "地址",align:"center",
                    	templet: function(d){
                    		return d.provinceName+d.cityName+d.districtName+d.address;
                    	}
                    },
                    {title: "创建人", field: "createdName",align:"center",width:120},
                    {title: "创建时间",align:"center",
                    	templet: function(d){
                    		if(d.creationDate!=null){
	                    		return layui.util.toDateString(d.creationDate);
                    		}
                    		return '';
                    	},width:160	
                    },
                    {title: "最近修改人", field: "lastUpdateName",align:"center",width:120},
                    {title: "最近修改时间",align:"center",
                    	templet: function(d){
                    		if(d.lastUpdateDate!=null){
	                    		return layui.util.toDateString(d.lastUpdateDate);
                    		}
                    		return '';
                    	},width:160	
                    },
                    {title: "状态", width :100,align:"center",
                    	templet: function(d){
	                    	if(d.status == 1){
	                    		return '<button type="button" class="layui-btn  layui-btn-normal"  lay-event="yn">启用</button>';
	                    	}else{
	                    		return '<button type="button" class="layui-btn  layui-btn-danger"  lay-event="yn">禁用</button>';
	                    	}
                   		}
                	},
                    {title: "操作",width :175, align:"center", templet: "#tool"}
                ]],done:function(res, curr){
      	    	  var brforeCurr = curr; // 获得当前页码
    	    	  var dataLength = res.data.length; // 获得当前页的记录数
    	    	  var count = res.count; // 获得总记录数
    	    	  if(dataLength == 0 && count != 0){ //如果当前页的记录数为0并且总记录数不为0
    	    		  table.reload("tab",{ // 刷新表格到上一页
    	    			  page:{
    	    				 curr:brforeCurr-1
    	    			  }
    	    		  });
    	    	  }
    	      }
            });
            
          	//监听行工具事件
            table.on('tool(tab)', function(obj){
              var data = obj.data;
              if(obj.event === 'yn'){
            	  var str = "启用";
            	  var status = 1;
            	  if(data.status == 1){
            		  str = "禁用";
            		  status = 0;
            	  }
                  layer.confirm('确认要['+str+']这个旅游公司吗?', function(index){
                     $.ajax({
                    	 url:'<%=basePath%>touroperator/saveOrUpdate',
                    	 data: {"tId":data.tId,"status":status},
                    	 type: 'post',
                    	 dataType: 'json',
                    	 success:function(data){
                    		 tableReload();
                    		 if(data.se){
	                    		 layer.msg(data.msg, {time: 1000, icon:6});
                    		 }else{
	                    		 layer.msg(data.msg, {time: 1000, icon:5});
                    		 }
                    	 },error:function(data){
                    		 tableReload();
                    		 layer.msg("操作失败", {time: 1000, icon:5});
                    	 }
                     });
                    	layer.close(index);
                  });
              }else if(obj.event === 'del'){
                layer.confirm('真的删除这个商户吗?', function(index){
                	$.ajax({
                   	 url:'<%=basePath%>touroperator/deleteById',
                   	 data: {"tId":data.tId},
                   	 type: 'post',
                   	 dataType: 'json',
                   	 success:function(data){
                   		 tableReload();
                   		 if(data.se){
                   		 	 layer.msg(data.msg, {time: 1000, icon:6});
	               		 }else{
	                   		 layer.msg(data.msg, {time: 1000, icon:5});
	               		 }
                   	 },error:function(data){
                   		 tableReload();
                   		layer.msg("操作失败", {time: 1000, icon:5});
                   	 }
                    });
                  	layer.close(index);
                });
              } else if(obj.event === 'edit'){
            	  layer.open({
             		  type: 2,
             		  title: '编辑旅游公司',
             		  shadeClose: true,
             		  shade: 0.8,
             		  area: ['680px', '500px'],
             		  content: '<%=basePath%>touroperator/editPage?tId='+data.tId
              	});  
              }
            });
            active = {
   		        add: function(){ //获取选中数据
		   			layer.open({
		       		  type: 2,
		       		  title: '添加旅游公司',
		       		  shadeClose: true,
		       		  shade: 0.8,
		       		  area: ['680px', '500px'],
		       		  content: '<%=basePath%>touroperator/editPage'
		        	});   
   		        },
   				doSearch: function(){
   					table.reload('tab', {
   	                    where: {"tName": $('#tName').val(),"createdName": $('#createdName').val(),"phone": $('#phone').val()},
   	                    page: {
   	                        curr: 1 //重新从第 1 页开始
   	                      }
   	                });
   				},
       			tableReload:function(){
    	   			 table.reload('tab', {});
       			}
             };
        });
        //搜索操作
        function doSearch() {
        	active.doSearch();
        };
        function tableReload(){
       	   layui.use('table', function () {
              var table = layui.table;
              table.reload('tab', {
              });
           });
        };
        function add(){
        	active.add();
        };
        
        
        
    </script>
</html>

