<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>旅游公司管理</title>
	<%@ include file="../head.jsp" %>
</head>
<body>
 <form class="layui-form" style="margin-top: 20px;">
 	<input type="hidden" name="tId" value="${dto.tId}">
    <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">商户名称</label>
		      <div class="layui-input-inline">
		        <input type="text" name="tName" value="${dto.tName}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
     	 </div>
     	 <div class="layui-inline">
     	 	  <label class="layui-form-label">联系方式</label>
		      <div class="layui-input-inline">
		         <input type="tel" name="phone" value="${dto.phone}" lay-verify="required" autocoxmplete="off" class="layui-input">
		      </div>
     	 </div>
    </div>
     <div class="layui-form-item">
   		<div class="layui-inline">
   			<label class="layui-form-label">所在城市</label>
   			 <div class="layui-input-inline" style="width: 160px;">
		        <select name="province" id="province" lay-filter="province" lay-verify="required" lay-search >
					<option value="">省</option>
					<c:forEach items="${provinceList}" var="p">
						<option value="${p.id}" <c:if test="${dto.province == p.id}">selected</c:if> >${p.name}</option>
					</c:forEach>
				</select>
		     </div>
   			 <div class="layui-input-inline" style="width: 160px;">
		        <select name="city" id="city"  lay-filter="city" lay-verify="required" lay-search >
					<option value="">市</option>
			        <c:forEach items="${cityList}" var="c">
							<option value="${c.id}" <c:if test="${dto.city == c.id}">selected</c:if> >${c.name}</option>
					</c:forEach>
				</select>
		      </div>
   			 <div class="layui-input-inline" style="width: 160px;">
		        <select name="district" id="district" lay-verify="required" lay-search >
					<option value="">区</option>
					 <c:forEach items="${districtList}" var="d">
							<option value="${d.id}" <c:if test="${dto.district == d.id}">selected</c:if> >${d.name}</option>
					</c:forEach>
				</select>
		      </div>
   		</div>
    </div>
    <div class="layui-form-item">
   		<div class="layui-inline">
		      <label class="layui-form-label">详情位置</label>
    		 <div class="layui-input-inline">
			      <div class="layui-input-inline">
			        <input type="text" name="address" value="${dto.address}" lay-verify="required" autocomplete="off" class="layui-input" style="width: 515px;">
			      </div>
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
	    <div class="layui-input-block">
	      <button type="submit" class="layui-btn" lay-submit="" lay-filter="btnSub">立即提交</button>
	    </div>
  </div>
 </form>
</body>
 
   

<script type="text/javascript">
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
          //监听提交
            form.on('submit(btnSub)', function(data){
           		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            	$.ajax({
            		 url:'<%=basePath%>touroperator/saveOrUpdate',
                  	 data: data.field,
                  	 type: 'post',
                  	 dataType: 'json',
                  	 success:function(data){
                  		 if(data.se){
	                  		layer.msg(data.msg, { offset: '15px',icon: 6,time: 1000}, function(){
		                  		parent.layer.close(index);
				            	parent.location.reload();
	                  		});
                  		 }else{
                  			layer.msg(data.msg, { offset: '15px',icon: 5,time: 1000}, function(){
		                  		parent.layer.close(index);
				            	parent.location.reload();
	                  		});
                  		 }
                  	 },error:function(data){
                  		layer.msg(data.msg, { offset: '15px',icon: 5,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                  		});
                  	 }
                 });
            	 
                 return false;
            });
          
            form.on('select(province)', function(data){
           		$("#district").empty();
        		var str="<option value=''>区</option>";
           		$("#district").append(str);
        		str="<option value=''>市</option>";
           		$.ajax({
	           		 url:'<%=basePath%>region/ajaxRegionList?pid='+data.value,
	              	 type: 'post',
	              	 dataType: 'json',
           			 success:function(d){
           				$("#city").empty();
           				if(d.data.length>0){
	           				$(d.data).each(function(i,t){
	           					str+="<option value='"+t.id+"'>"+t.name+"</option>"
	           				})
           				}
           				$("#city").append(str);
		           		form.render('select');
           			 }
           		});
           	});
           	form.on('select(city)', function(data){
           		var str="<option value=''>区</option>";
           		$.ajax({
	           		 url:'<%=basePath%>region/ajaxRegionList?pid='+data.value,
	              	 type: 'post',
	              	 dataType: 'json',
          			 success:function(d){
          				$("#district").empty();
          				if(d.data.length>0){
	           				$(d.data).each(function(i,t){
	           					str+="<option value='"+t.id+"'>"+t.name+"</option>"
	           				})
          				}
          				$("#district").append(str);
          				form.render('select');
          			 }
          		});
           	});
          
        });
        
        
    </script>
</html>

