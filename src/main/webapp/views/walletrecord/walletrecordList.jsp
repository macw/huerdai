<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>钱包信息管理</title>
	<%@ include file="../head.jsp" %>
	 <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>
</head>
<body>
<div class="layui-fluid layui-form">
    <div class="layui-card-body">
        <div style="padding-bottom: 10px;">
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="createdName" id="createdName" placeholder="支付方/接收方/创建人/修改人" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
	             <input type="text" name="recordSn" id="recordSn" placeholder="流水号" autocomplete="off" class="layui-input">
            </div>
             <div class="layui-input-inline" style="width: 200px;">
				<select name="type" id="type" lay-search>
					<option value="">交易类型</option>
					<option value="1">充值</option>
					<option value="2">提现</option>
					<option value="3">交易</option>
				</select>
			</div>
            <div class="layui-input-inline" style="width: 200px;">
				<select name="payStatus" id="payStatus" lay-search>
					<option value="">支付状态</option>
					<option value="0">待支付</option>
					<option value="1">支付成功</option>
					<option value="-1">支付失败</option>
				</select>
			</div>
            <div class="layui-input-inline" style="width: 200px;">
				<select name="checkStatus" id="checkStatus" lay-search>
					<option value="">对账状态</option>
					<option value="0">未对账</option>
					<option value="1">已对账</option>
				</select>
			</div>
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>
        <table id="walletrecord-tab" lay-filter="walletrecord-tab"></table>
    </div>
</div>
</body>
 
  <script type="text/html" id="tool">
       <!-- <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">编辑</button> -->
        <button type="button" class="layui-btn layui-btn-danger" lay-event="del">删除</button>
  </script>

<script type="text/javascript">
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            table.render({
                elem: '#walletrecord-tab',
                url: '<%=basePath%>walletRecord/selectData', //数据接口
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                cols: [[
                    {type: "checkbox"},
                    {title: "支付方", field: "fromUserName",align:"center",width:90},
                    {title: "接收方", field: "toUserName",align:"center",width:90},
                    {title: "流水号", field: "recordSn",align:"center",width:288},
                    {title: "交易类型",align:"center",width:87,
                    	templet: function(d){
                    		if(d.type == 1){
                    			return '充值';
                    		}else if(d.type == 2){
                    			return '提现';
                    		}else if(d.type == 3){
                    			return '交易';
                    		}else{
                    			return '';
                    		}
                    	}
                    },
                    {title: "交易金额", field: "money",align:"center",width:87},
                    {title: "支付方式",align:"center",width:87,
                    	templet: function(d){
                    		if(d.payType == 1){
                    			return '支付宝';
                    		}else if(d.payType == 2){
                    			return '微信';
                    		}else if(d.payType == 2){
                    			return '交易';
                    		}else if(d.payType == 3){
                    			return '银行卡';
                    		}else if(d.payType == 4){
                    			return '余额';
                    		}else if(d.payType == 0){
                    			return '待定';
                    		}else{
                    			return '';
                    		}
                    	}
                    },
                    {title: "备注", field: "remark",align:"center",width:200},
                    {title: "支付状态",align:"center",width:87,
                    	templet: function(d){
                    		if(d.payStatus == 0){
                    			return '待支付';
                    		}else if(d.payStatus == -1){
                    			return '支付失败';
                    		}else if(d.payStatus == 1){
                    			return '支付成功';
                    		}else{
                    			return '';
                    		}
                    	}
                    },
                    {title: "交易时间",align:"center",width:160,
                    	templet: function(d){
                    		if(d.payTime!=null){
	                    		return layui.util.toDateString(d.payTime);
                    		}
                    		return '';
                    	}	
                    },
                    {title: "收款状态",align:"center",width:87,
                    	templet: function(d){
                    		if(d.fetchStatus == 0){
                    			return '待收款';
                    		}else if(d.fetchStatus == -1){
                    			return '收款失败';
                    		}else if(d.fetchStatus == 1){
                    			return '收款成功';
                    		}else{
                    			return '';
                    		}
                    	}
                    },
                    {title: "收款时间",align:"center",width:160,
                    	templet: function(d){
                    		if(d.fetchTime!=null){
	                    		return layui.util.toDateString(d.fetchTime);
                    		}
                    		return '';
                    	}	
                    },
                    {title: "对账状态",align:"center",width:87,
                    	templet: function(d){
                    		if(d.checkStatus == 0){
                    			return '未对账';
                    		}else if(d.checkStatus == 1){
                    			return '已对账';
                    		}else{
                    			return '';
                    		}
                    	}
                    },
                    {title: "创建人", field: "createdName",align:"center",width:87},
                    {title: "创建时间",align:"center",width:160,
                    	templet: function(d){
                    		if(d.creationDate!=null){
	                    		return layui.util.toDateString(d.creationDate);
                    		}
                    		return '';
                    	}	
                    }
                    /* ,{title: "最近修改人", field: "lastUpdateName",align:"center",width:100},
                    {title: "最近修改时间",align:"center",width:160,
                    	templet: function(d){
                    		if(d.lastUpdateDate!=null){
	                    		return layui.util.toDateString(d.lastUpdateDate);
                    		}
                    		return '';
                    	}	
                    }*/
                    ,{title: "操作",width :100, align:"center", templet: "#tool"} 
                ]],done:function(res, curr){
        	    	  var brforeCurr = curr; // 获得当前页码
        	    	  var dataLength = res.data.length; // 获得当前页的记录数
        	    	  var count = res.count; // 获得总记录数
        	    	  if(dataLength == 0 && count != 0){ //如果当前页的记录数为0并且总记录数不为0
        	    		  table.reload("walletrecord-tab",{ // 刷新表格到上一页
        	    			  page:{
        	    				 curr:brforeCurr-1
        	    			  }
        	    		  });
        	    	  }
        	      }
            });
            
            
          	//监听行工具事件
            table.on('tool(walletrecord-tab)', function(obj){
              var data = obj.data;
              if(obj.event === 'del'){
                layer.confirm('真的删除这条记录吗?', function(index){
                	$.ajax({
                   	 url:'<%=basePath%>walletRecord/deleteByRecordId',
                   	 data: {"recordId":data.recordId},
                   	 type: 'post',
                   	 dataType: 'json',
                   	 success:function(data){
                   		 tableReload();
                   		 if(data.se){
	                   		layer.msg(data.msg, {time: 1000, icon:6});
                   		 }else{
	                   		layer.msg(data.msg, {time: 1000, icon:5});
                   		 }
                   	 },error:function(data){
                   		 tableReload();
                   		layer.msg("操作失败", {time: 1000, icon:6});
                   	 }
                    });
                  	layer.close(index);
                });
              } else if(obj.event === 'edit'){
            	  layer.open({
             		  type: 2,
             		  title: '编辑商户管理',
             		  shadeClose: true,
             		  shade: 0.8,
             		  area: ['470px', '450px'],
             		  content: '<%=basePath%>merchant/editPage?mId='+data.mId
              	});  
              }
            });
        });
        //搜索操作
        function doSearch() {
            //1.获取到输入框中输入的内容
            var createdName = $('#createdName').val();
            var recordSn = $('#recordSn').val();
            var type = $('#type').val();
            var payStatus = $('#payStatus').val();
            var checkStatus = $('#checkStatus').val();
            //发送请求，并且接收数据
            layui.use('table', function () {
                var table = layui.table;
                table.reload('walletrecord-tab', {
                    where: {"createdName": createdName,"recordSn": recordSn,"type": type,"payStatus": payStatus,"checkStatus": checkStatus},
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
            });
        };
        function tableReload(){
       	   layui.use('table', function () {
              var table = layui.table;
              table.reload('walletrecord-tab', {
              });
           });
        };
        function add(){
        	layui.use('layer', function(){
        		layer.open({
             		  type: 2,
             		  title: '添加商户管理',
             		  shadeClose: true,
             		  shade: 0.8,
             		  area: ['30%', '50%'],
             		  content: '<%=basePath%>merchant/editPage'
              	});   
       		}); 
        };
        
        
        
    </script>
</html>

