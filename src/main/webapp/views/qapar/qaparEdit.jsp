<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>商户信息管理</title>
	<%@ include file="../head.jsp" %>
</head>
<body>
 <form class="layui-form" style="margin-top: 20px;">
 	<input type="hidden" name="qaparId" value="${dto.qaparId}">
    <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">公司段</label>
		      <div class="layui-input-inline">
		        <input type="text" name="orgTitle" value="${dto.orgTitle}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">发票类型</label>
		      <div class="layui-input-inline">
		        <select name="invRecFlag" id="invRecFlag" lay-verify="required" lay-search >
					<option value="">发票类型</option>
					<option value="1" <c:if test="${dto.invRecFlag == 1}">selected</c:if> >发票凭证</option>
					<option value="2" <c:if test="${dto.invRecFlag == 2}">selected</c:if> >其他凭证</option>
				</select>
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">期间</label>
		      <div class="layui-input-inline">
		        <input type="text" name="accountingDate" value="${dto.accountingDate}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
	    <div class="layui-input-block">
	      <button type="submit" class="layui-btn" lay-submit="" lay-filter="btnSub">立即提交</button>
	    </div>
  </div>
 </form>
</body>
 
   

<script type="text/javascript">
        layui.use(['table', 'form','laydate'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            var laydate = layui.laydate;
            
          //日期时间选择器
            laydate.render({
              elem: '#invoiceDate'
              ,type: 'datetime'
            });
          //监听提交
            form.on('submit(btnSub)', function(data){
           		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            	$.ajax({
                  	 url:'<%=basePath%>qapar/saveOrUpdate',
                  	 data: data.field,
                  	 type: 'post',
                  	 dataType: 'json',
                  	 success:function(data){
                  		layer.msg('操作成功', { offset: '15px',icon: 6,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                  		});
                  	 },error:function(data){
                  		layer.msg('操作失败', { offset: '15px',icon: 5,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                  		});
                  	 }
                 });
            	 
                 return false;
            });
           
        });
        
        
    </script>
</html>

