<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>物流管理</title>
	<%@ include file="../head.jsp" %>
	 <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>
</head>
<body>
<div class="layui-fluid">
    <div class="layui-card-body layui-form">
        <div style="padding-bottom: 10px;">
            <div class="layui-input-inline" style="width: 100px;">
                 <button type="button" class="layui-btn" id="add">添加</button>
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="orgTitle" id="orgTitle" placeholder="公司段" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
				<select name="invRecFlag" id="invRecFlag" lay-search>
					<option value="">凭证类型</option>
					<option value="1">发票凭证</option>
					<option value="2">其他凭证</option>
				</select>
			</div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="createdName" id="createdName" placeholder="创建人/修改人" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" id="search">
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>
        <table id="tab" lay-filter="tab"></table>
    </div>
</div>
</body>
 
  <script type="text/html" id="toolbar">
       <button type="button" class="layui-btn" id="add">添加</button>
  </script>
  <script type="text/html" id="tool">
        <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">编辑</button>
        <button type="button" class="layui-btn layui-btn-danger" lay-event="del">删除</button>
  </script>

<script type="text/javascript">
		var actity=null;
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            table.render({
                elem: '#tab',
                url: '<%=basePath%>qapar/selectData', //数据接口
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                /* toolbar: '#toolbar', */
                cols: [[
                    {type: "checkbox"},
                    {title: "公司段", field: "orgTitle",align:"center"},
                    {title: "凭证类型",align:"invRecFlag",align:"center",
                    	templet: function(d){
                    		if(d.invRecFlag==1){
	                    		return '发票凭证';
                    		}else if(d.invRecFlag==2){
                    			return '其他凭证';
                    		}
                    		return '';
                    	}
                    },
                    {title: "期间", field: "accountingDate",align:"center"},
                    {title: "创建人", field: "createdName",align:"center"},
                    {title: "创建时间",align:"creationDate",
                    	templet: function(d){
                    		if(d.creationDate!=null){
	                    		return layui.util.toDateString(d.creationDate);
                    		}
                    		return '';
                    	}	
                    },
                    {title: "最近修改人", field: "lastUpdateName",align:"center"},
                    {title: "最近修改时间",align:"center",
                    	templet: function(d){
                    		if(d.lastUpdateDate!=null){
	                    		return layui.util.toDateString(d.lastUpdateDate);
                    		}
                    		return '';
                    	}	
                    },
                    {title: "操作",width :175, align:"center", templet: "#tool"}
                ]],done:function(res, curr){
      	    	  var brforeCurr = curr; // 获得当前页码
    	    	  var dataLength = res.data.length; // 获得当前页的记录数
    	    	  var count = res.count; // 获得总记录数
    	    	  if(dataLength == 0 && count != 0){ //如果当前页的记录数为0并且总记录数不为0
    	    		  table.reload("tab",{ // 刷新表格到上一页
    	    			  page:{
    	    				 curr:brforeCurr-1
    	    			  }
    	    		  });
    	    	  }
    	      }
            });
            
          	//监听行工具事件
            table.on('tool(tab)', function(obj){
              var data = obj.data;
              if(obj.event === 'del'){
                layer.confirm('真的删除该凭证吗?', function(index){
                	$.ajax({
                   	 url:'<%=basePath%>qapar/deleteById',
                   	 data: {"qaparId":data.qaparId},
                   	 type: 'post',
                   	 dataType: 'json',
                   	 success:function(data){
                   		active.tableReload();
                   		layer.msg("操作成功", {time: 1000, icon:6});
                   	 },error:function(data){
                   		active.tableReload();
                   		layer.msg("操作失败", {time: 1000, icon:5});
                   	 }
                    });
                  	layer.close(index);
                });
              } else if(obj.event === 'edit'){
            	  layer.open({
             		  type: 2,
             		  title: '编辑凭证管理',
             		  shadeClose: true,
             		  shade: 0.8,
             		  area: ['460px', '350px'],
             		  content: '<%=basePath%>qapar/editPage?qaparId='+data.qaparId
              	});  
              }
            });
          	
            active = {
   		        add: function(){ //获取选中数据
			   		 layer.open({
			    		  type: 2,
			    		  title: '编辑凭证管理',
			    		  shadeClose: true,
			    		  shade: 0.8,
			    		  area: ['460px', '350px'],
			    		  content: '<%=basePath%>qapar/editPage'
			     	});  
   		        },
   				doSearch: function(){
   					var createdName =$("#createdName").val();
   					var orgTitle =$("#orgTitle").val();
   					var invRecFlag =$("#invRecFlag").val();
   	                table.reload('tab', {
   	                    where: {"createdName": $("#createdName").val(),"orgTitle": $("#orgTitle").val(),"invRecFlag": $("#invRecFlag").val()},
   	                    page: {
   	                        curr: 1 //重新从第 1 页开始
   	                      }
   	                });
   				},
   				tableReload:function(){
	   				 table.reload('tab', {});
   				}
            }
        });
		$("#search").click(function(){
			active.doSearch();
		});
		$("#add").click(function(){
			active.add();
		})
         
    </script>
</html>

