<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../../head.jsp" %>
	 <style type="text/css">
         .info {
            position: relative;
            background-color: black;
            color: white;
            filter: alpha(Opacity=80);
            -moz-opacity: 0.5;
            opacity: 0.3;
            width: 135px;
            height: 25px;
            text-align: center;
            margin-left: 3px;
        }
 
         .handle {
            position: relative;
            background-color: black;
            color: white;
            filter: alpha(Opacity=80);
            -moz-opacity: 0.5;
            opacity: 0.3;
            width: 135px;
            text-align: right;
            height: 18px;
            margin-bottom: -21px;
		    margin-left: 3px;
        }
         .dataHandle {
            position: relative;
            background-color: black;
            color: white;
            filter: alpha(Opacity=80);
            -moz-opacity: 0.5;
            opacity: 0.3;
            width: 135px;
            text-align: right;
            height: 18px;
            margin-bottom: -21px;
		    margin-left: 3px;
        }
 
        .handle i {
            
        }
 
       .handle i:hover {
             cursor: pointer;
        }
 		.dataHandle i:hover {
             cursor: pointer;
        }
        .file-iteme {
        	max-width:139px;
            padding: 1px;
            float: left;
        }
    </style>
</head>
<body>
 <form id="form" class="layui-form" enctype="multipart/form-data" style="margin-top: 20px;">
 	<input type="hidden" name="spotId" value="${dto.spotId}" id="spotId">
 	  <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">景点名称</label>
		      <div class="layui-input-inline">
		        <input type="text" name="spotName" value="${dto.spotName}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
	    <div class="layui-inline">
  	     <label class="layui-form-label">旅游公司</label>
	      <div class="layui-input-inline">
	         <select name="tId" id="tId" lay-verify="required" lay-search >
				<option value="">所属公司</option>
				<c:forEach items="${tourOperatorList}" var="t">
					<option value="${t.tId }"  <c:if test="${dto.tId == t.tId}">selected</c:if> >${t.tName }</option>
				</c:forEach>
			</select>
	      </div>
	  </div>
    	<div class="layui-inline">
		      <label class="layui-form-label">优先级</label>
		      <div class="layui-input-inline">
		        <input type="text" name="leavel" value="${dto.leavel}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
    	<div class="layui-inline">
			<label class="layui-form-label">景点主图</label>
			<div class="layui-input-inline">
				<button type="button" class="layui-btn" id="file">
					</i>上传景点主图
				</button>
			</div>
		</div>
		<div class="layui-inline">
				<img class="layui-upload-img" name="pictureurl" src="${dto.pictureurl}" id="pictureurl" style="max-width: 200px;max-height: 200px; padding-left: 40px;">
	    </div>
	    <div class="layui-inline">
			<label class="layui-form-label">景点缩略图</label>
			<div class="layui-input-inline">
				<button type="button" class="layui-btn" id="files">上传文件</button>
			</div>
		</div>
	</div>
  <!--   <div class="layui-form-item">
    	
		 
	</div> -->
    <div class="layui-form-item">
    	<div class="layui-input-block" style="margin-left: 0px">
			 <blockquote class="layui-elem-quote layui-quote-nm" style="margin-top: 10px;margin-left: 23px; width: 1150px;float: left;">
			    缩略图预览：
			 <div class="layui-upload-list" id="filesUrl">
			 
			 </div>
		</div>
		 
	</div>
   <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">出厂价</label>
		      <div class="layui-input-inline">
		        <input type="text" name="floorPrice" value="${dto.floorPrice}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    	<div class="layui-inline">
		      <label class="layui-form-label">门票价格</label>
		      <div class="layui-input-inline">
		        <input type="text" name="sellingPrice" value="${dto.sellingPrice}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
	    <%-- <div class="layui-inline">
		      <label class="layui-form-label">一级利润</label>
		      <div class="layui-input-inline">
		        <input type="text" name="primaryProfit" value="${dto.primaryProfit}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    	<div class="layui-inline">
		      <label class="layui-form-label">二级利润</label>
		      <div class="layui-input-inline">
		        <input type="text" name="secondaryProfit" value="${dto.secondaryProfit}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div> --%>
    </div>
   <div class="layui-form-item">
   		<div class="layui-inline">
   			<label class="layui-form-label">所在城市</label>
   			 <div class="layui-input-inline" style="width: 160px;">
		        <select name="province" id="province" lay-filter="province" lay-verify="required" lay-search >
					<option value="">省</option>
					<c:forEach items="${provinceList}" var="p">
						<option value="${p.id}" <c:if test="${dto.province == p.id}">selected</c:if> >${p.name}</option>
					</c:forEach>
				</select>
		     </div>
   			 <div class="layui-input-inline" style="width: 160px;">
		        <select name="city" id="city"  lay-filter="city" lay-verify="required" lay-search >
					<option value="">市</option>
			        <c:forEach items="${cityList}" var="c">
							<option value="${c.id}" <c:if test="${dto.city == c.id}">selected</c:if> >${c.name}</option>
					</c:forEach>
				</select>
		      </div>
   			 <div class="layui-input-inline" style="width: 175px;">
		        <select name="district" id="district" lay-verify="required" lay-search >
					<option value="">区</option>
					 <c:forEach items="${districtList}" var="d">
							<option value="${d.id}" <c:if test="${dto.district == d.id}">selected</c:if> >${d.name}</option>
					</c:forEach>
				</select>
		      </div>
		   	<div class="layui-inline">
	    		 <div class="layui-input-inline">
				      <div class="layui-input-inline">
				        <input type="text" name="address" value="${dto.address}" lay-verify="required" placeholder="位置详情" autocomplete="off" class="layui-input" style="width: 515px;">
				      </div>
			      </div>
	    	</div>
   		</div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">备注</label>
      <div class="layui-input-inline">
         <textarea placeholder="备注" cols="4" rows="3" style="width: 515px" class="layui-textarea" name="memo"  autocomplete="off" >${dto.memo}</textarea>
      </div>
    </div>
    <div class="layui-form-item">
	    <div class="layui-input-block">
	      <button type="submit" class="layui-btn" lay-submit="" lay-filter="btnSub" id="subBtn">立即提交</button>
	    </div>
  </div>
 </form>
</body>
 
   

<script type="text/javascript">
		var activce=null;
        layui.use(['table', 'form','upload'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            var upload = layui.upload;
            
            activce = {
            	deleteBySpotImgId:function(spotImgId){
            		/* layer.confirm('这条记录为数据库中的记录确定要删除吗?', function(index){ */
            		var result = false;
                     	$.ajax({
                        	 url:'<%=basePath%>scenicSpotImg/deleteBySpotImgId',
                        	 data: {"spotImgId":spotImgId},
                        	 type: 'post',
                        	 dataType: 'json',
                        	 async:false,
                        	 success:function(data){
                        		 result = true;
                        	 }
                         });
            			return result;
                   /*   }); */
            	}	
            },
            upload.render({
				 elem : '#file'
				,auto : false
				,accept: "images"
                ,acceptMime: 'image/jpg,image/jpeg,image/png'//打开文件选择框时,只显示图片文件
                ,auto: false  //是否选完文件后自动上传。默认值：true
				,choose : function(obj) {
					obj.preview(function(index, file, result) {
						$('#pictureurl').attr('src', result);
					})
				}
			});
            
           var uploadListIns = upload.render({
				 elem : '#files'
				,auto : false //是否选完文件后自动上传。默认值：true
				,accept: "images"
                ,acceptMime: 'image/jpg,image/jpeg,image/png'//打开文件选择框时,只显示图片文件
                ,multiple: true //多文件上传
                ,number:'8'
                ,field:'files'
                ,choose: function(obj){
                	var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
                    var uploadSize = $(".file-iteme").length;
                    obj.preview(function(index, file, result){
                    	if(uploadSize<8){
	                		//预读本地文件示例，不支持ie8
		                      $('#filesUrl').append(
		                    		  '<div class="file-iteme">'+
		                    		  '<div class="handle"><i class="layui-icon layui-icon-delete"></i></div>' +
		                    		  '<img src="'+ result +'"class="layui-upload-img" style="max-width:135px;max-height:100px; margin:3px;">'+
		                    		  '<div class="info">' + file.name + '</div>' +
		                    		  '</div>'
		                    );
	                	}else{
	                		layer.msg('最多上传8张图片');
	                	}
	                	// 删除图片
	                    $(document).on("click", ".file-iteme .handle", function(event){
	                    	 delete files[index]; //删除对应的文件
	                         $(this).parent().remove();  
	                    });
               		});
                    
				}
            });
            
           	form.on('select(province)', function(data){
           		$("#district").empty();
        		var str="<option value=''>区</option>";
           		$("#district").append(str);
        		str="<option value=''>市</option>";
           		$.ajax({
	           		 url:'<%=basePath%>region/ajaxRegionList?pid='+data.value,
	              	 type: 'post',
	              	 dataType: 'json',
           			 success:function(d){
           				$("#city").empty();
           				if(d.data.length>0){
	           				$(d.data).each(function(i,t){
	           					str+="<option value='"+t.id+"'>"+t.name+"</option>"
	           				})
           				}
           				$("#city").append(str);
		           		form.render('select');
           			 }
           		});
           	});
           	form.on('select(city)', function(data){
           		var str="<option value=''>区</option>";
           		$.ajax({
	           		 url:'<%=basePath%>region/ajaxRegionList?pid='+data.value,
	              	 type: 'post',
	              	 dataType: 'json',
          			 success:function(d){
          				$("#district").empty();
          				if(d.data.length>0){
	           				$(d.data).each(function(i,t){
	           					str+="<option value='"+t.id+"'>"+t.name+"</option>"
	           				})
          				}
          				$("#district").append(str);
          				form.render('select');
          			 }
          		});
           	});
          //监听提交
            form.on('submit(btnSub)', function(data){
            	var src= $("#pictureurl").attr("src");
            	if(src == null || src.length <=0){
            		layer.msg('请上传图片', { offset: '15px',icon: 5,time: 1000});
            		return false;
            	}
           		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            	var formData = new FormData(document.getElementById("form"));
            	$.ajax({
                  	 url:'<%=basePath%>scenic/spot/saveOrUpdate',
                  	 data: formData,
                  	 type: 'post',
                  	 dataType: 'json',
                  	 processData : false,
					 contentType : false,
                  	 success:function(data){
                  		 if(data.se){
                  			layer.msg(data.msg, { offset: '15px',icon: 6,time: 1000}, function(){
    	                  		parent.layer.close(index);
    			            	parent.location.reload();
                      		});
                  		 }else{
                  			layer.msg(data.msg, { offset: '15px',icon: 5,time: 1000}, function(){
    	                  		parent.layer.close(index);
    			            	parent.location.reload();
                      		});
                  		 }
                  		
                  	 },error:function(data){
                  		layer.msg('操作失败', { offset: '15px',icon: 5,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                  		});
                  	 }
                 });
                 return false;
            });
        });
        
        /* $(document).on("mouseenter mouseleave", ".file-iteme", function(event){
            if(event.type === "mouseenter"){
                //鼠标悬浮
                $(this).children(".info").fadeIn("fast");
                $(this).children(".handle").fadeIn("fast");
            }else if(event.type === "mouseleave") {
                //鼠标离开
                $(this).children(".info").hide();
                $(this).children(".handle").hide();
            }
        }); */
        
     	
        
        $(document).on("click", ".file-iteme .dataHandle", function(event){
        	var spotImgId = $(this).attr("id");
        	var result = activce.deleteBySpotImgId(spotImgId);
        	if(result){
	            $(this).parent().remove();  
        	}
        });
        window.onload=function(){
        	var spotId = $("#spotId").val();
        	if(spotId!=null && spotId>0){
        		$.ajax({
                 	 url:'<%=basePath%>scenicSpotImg/selectBySpotId',
                 	 data: {"spotId":spotId},
                 	 type: 'post',
                 	 dataType: 'json',
                 	 success:function(data){
                 		 if(data.se){
                 			$(data.data).each(function(i,item){
                 				  $('#filesUrl').append(
        	                    		  '<div class="file-iteme">'+
        	                    		  '<div class="dataHandle" id="'+ item.spotImgId +'"><i class="layui-icon layui-icon-delete"></i></div>' +
        	                    		  '<img src="'+ item.imgUrl +'"class="layui-upload-img" style="max-width:135px;max-height:100px; margin:3px;">'+
        	                    		  '</div>'
        	                      )
                 			});
                     	  }
                 	 }
                });
        	}
        }
        
    </script>
</html>

