<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../../head.jsp" %>
	<link rel="stylesheet" href="<%= basePath%>css/timeStyle.css" >
	
  <!--图标库-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous" />

<!--字体-->
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&display=swap" rel="stylesheet" />

<title>CalenderApp</title>

</head>
<body>
	<div id="current-day-info" class="color" hidden>
		<h1 id="app-name-landscape" class="off-color center default-cursor">
			日历表
		</h1>

		<div>
			<h2 id="current-year" class="center default-cursor">2019</h2>
		</div>

		<div>
			<h1 id="cur-day" class="current-day-heading center default-cursor">
				星期一
			</h1>
			<h1 id="cur-month" class="current-month-heading center default-cursor">
				六月
			</h1>
			<h1 id="cur-date" class="current-date-heading center default-cursor">
				7
			</h1>
		</div>

		<div class="time">
			<span> 22 </span>: <span> 55 </span>:
			<span> 23 </span>
		</div>

		<button id="theme-landscape" class="font btn">更改主题</button>
	</div>

	<div class="layui-fluid">
   		<div class="layui-card-body layui-form">
   			<input type="hidden" id="spotId" value="${dto.spotId}">
			<div id="calender">
				<h1 id="app-name-portrait" class="center">日历表</h1>
				<!-- h1 'off-color' class was removed -->
				<table>
					<thead class="color">
						<tr>
							<th colspan="7" class="border-color">
								<h4 id="cal-year" contenteditable="true">2018</h4>
								<div>
									<i class="fas fa-caret-left icon" onclick="prevMonth()"> </i>
									<h3 id="cal-month">july</h3>
									<i class="fas fa-caret-right icon" onclick="nextMonth()"> </i>
								</div>
							</th>
						</tr>
		
						<tr>
							<th class="weekday border-color">日</th>
							<th class="weekday border-color">一</th>
							<th class="weekday border-color">二</th>
							<th class="weekday border-color">三</th>
							<th class="weekday border-color">四</th>
							<th class="weekday border-color">五</th>
							<th class="weekday border-color">六</th>
						</tr>
					</thead>
		
					<tbody id="table-body">
						<tr>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
						</tr>
						<tr>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
						</tr>
						<tr>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
						</tr>
						<tr>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
						</tr>
						<tr>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
						</tr>
						<tr>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
						</tr>
					</tbody>
				</table>
				<button id="theme-portrait" class="font btn">更改主题</button>
			</div>
		</div>
	</div>
	<div class="modal">
		<div id="fav-color" hidden>
			<div class="popup">
				<h4>你最喜欢什么颜色？</h4>

				<div id="color-options">
					<div class="color-option">
						<div class="color-preview" id="blue" style="background-color: #1B19CD;">
							<!-- <i class="fas fa-check checkmark"></i> -->
						</div>
						<h5>Blue</h5>
					</div>

					<div class="color-option">
						<div class="color-preview" id="red" style="background-color: #D01212;"></div>
						<h5>Red</h5>
					</div>

					<div class="color-option">
						<div class="color-preview" id="purple" style="background-color: #721D89;"></div>
						<h5>Purple</h5>
					</div>

					<div class="color-option">
						<div class="color-preview" id="green" style="background-color: #158348;"></div>
						<h5>Green</h5>
					</div>

					<div class="color-option">
						<div class="color-preview" id="orange" style="background-color: #EE742D;"></div>
						<h5>Orange</h5>
					</div>

					<div class="color-option">
						<div class="color-preview" id="deep-orange" style="background-color: #F13C26;"></div>
						<h5>Deep Orange</h5>
					</div>

					<div class="color-option">
						<div class="color-preview" id="baby-blue" style="background-color: #31B2FC;"></div>
						<h5>Baby Blue</h5>
					</div>

					<div class="color-option">
						<div class="color-preview" id="cerise" style="background-color: #EA3D69;"></div>
						<h5>Cerise</h5>
					</div>

					<div class="color-option">
						<div class="color-preview" id="lime" style="background-color: #36C945;"></div>
						<h5>Lime</h5>
					</div>

					<div class="color-option">
						<div class="color-preview" id="teal" style="background-color: #2FCCB9;"></div>
						<h5>Teal</h5>
					</div>

					<div class="color-option">
						<div class="color-preview" id="pink" style="background-color: #F50D7A;"></div>
						<h5>Pink</h5>
					</div>

					<div class="color-option">
						<div class="color-preview" id="black" style="background-color: #212524;"></div>
						<h5>Black</h5>
					</div>
				</div>

				<button id="update-theme-button" class="font btn color">
					更新主题
				</button>
			</div>
		</div>

		<div id="make-note" hidden>
			<div class="popup">
				<h4> <span class="verb"></span> note on <span id="noteDate">2019 12 5</span></h4>
				<input class="note-title" type="text" name="title" placeholder="note title ..." />
				<textarea class="note-content" id="edit-post-it" name="post-it"
					placeholder="note description ..."></textarea>
				<span style="color:red;" id="warning"></span>
				<div>
					<button class="btn font post-it-button" id="add-post-it">
						保存
					</button>
					<button class="btn font post-it-button" id="delete-button">
						删除
					</button>
				</div>
			</div>
		</div>
	</div>

  <script type="text/javascript" src="<%= basePath%>js/timeScript.js"></script>
  
  <script type="text/javascript">
  	layui.use(['table', 'form'], function () {
	    var table = layui.table;
	    var layer = layui.layer;
	    var form = layui.form;
	  	document.body.addEventListener("click", e => {
	  		let noteDate;
	  		let noteId;
	  		let note;
	  		let verbWord;
	  		if (e.target.parentElement.parentElement.id == "table-body") {
	  			var spotId = $("#spotId").val();
	  			if (e.target.classList.contains("currMonth")) {
	  				noteId = e.target.id;
	  				noteDate = e.target.innerHTML;
	  				var month = state.todayMonth + 1;
	  				if(month<10){
	  					month="0"+month;
	  				}
	  				if(noteDate.split("<br>")[0]<10){
	  					noteDate="0"+noteDate[0];
	  				}
	  				noteDate = state.todayYear +"-" +month+"-" +noteDate;
	  				date=noteDate;
	  				layer.open({
			    		  type: 2,
			    		  title: '编辑景点价格',
			    		  shadeClose: true,
			    		  shade: 0.8,
			    		  area: ['700px', '400px'],
			    		  content: '<%=basePath%>scenicroutesprice/editBySpotIdPrice?spotId='+spotId+'&date='+noteDate
			     	});
	  			} else {
	  				console.log("Not Applicable for previous and next month");
	  			}
	  			noteDateInPopup.innerHTML = noteDate;
	  		} else if (e.target.classList.contains("fa-sticky-note")) {
	  			/* verbWord = "Edit";
	  			// deleteBtnInPopup.style.display = "display";
	
	  			console.log("edit note (sticky)");
	  			noteId = e.target.parentElement.parentElement.id;
	  			if (noteId == "current-day") {
	  				noteDate =
	  					state.todayYear + " " + (state.todayMonth + 1) + " " + state.todayDate;
	  				note = notes.filter(n => n.date == noteDate);
	  			} else {
	  				note = notes.filter(n => n.id == noteId);
	  			}
	  			// note = notes.filter(n => n.id == noteId);
	  			noteDate = note[0].date;
	  			console.log("note:", note);
	  			openModal(true);
	  			fillNotePopup(note[0]);
	  			noteDateInPopup.innerHTML = noteDate;
	  			verb.innerHTML = verbWord;
	
	  			console.log("xXx");
	  			addNote(noteDate.split(" ")[2], noteId); */
	  			console.log("xXx");
	  		}
	  	});
	    
	    
	});
  	
  	window.onload=function(){
  		loadCalendar();
  	};
  	
  	function loadCalendar(date){
  		var spotId = $("#spotId").val();
  		$.ajax({
			url:"<%= basePath%>scenicroutesprice/selectSpotPrice",
			type:"post",
			data:{"spotId":spotId,"date":date},
			dataType:"json",
			async:false,
			success:function(data){
					$(".currMonth").each(function(i,item){
						// data.msg 为路线默认价格
						var str="<br/><br/>￥:"+data.msg;
						$(data.data).each(function(j,dd){
							if(parseInt(dd.date)==$(item).html()){
								//商品有活动价格拿活动价格
								if(dd.activityPrice>0){
									str="<br/><br/>￥:"+dd.activityPrice;
								}else{
									str="<br/><br/>￥:"+dd.sellingPrice;
								}
								return false;
							}
						});
						$(item).append(str);
		  			});
			},error:function(data){
				
			}
		});
  	};
  	function nextMonth() {
  		state.todayMonth += 1;
  		if (state.todayMonth == 12) {
  			state.todayYear += 1;
  			currentFullYear = analyizYear(state.todayYear);
  			state.todayMonth = 0;
  		}
  		var month = state.todayMonth+1;
  		if(month<10){
  			month="0"+month;
  		}
  		var date = currentFullYear.year+"-"+month;
  		currentFullMonth = currentFullYear.months[monthsStr[state.todayMonth]];
  		showCalenderInfo();
  		loadCalendar(date);
  	};

  	function prevMonth() {
  		state.todayMonth -= 1;
  		if (state.todayMonth == 0) {
  			state.todayYear -= 1;
  			currentFullYear = analyizYear(state.todayYear);
  			state.todayMonth = 11;
  		}
  		var month = state.todayMonth+1;
  		if(month<10){
  			month="0"+month;
  		}
  		var date = currentFullYear.year+"-"+month;
  		currentFullMonth = currentFullYear.months[monthsStr[state.todayMonth]];
  		showCalenderInfo();
  		loadCalendar(date);
  	};
  	
  </script>
  </body>
</html>

