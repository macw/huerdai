<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../../head.jsp" %>
</head>
<body>
 <form id="form" class="layui-form"  style="margin-top: 20px;">
 	<input type="hidden" name="routespriceId" value="${dto.routespriceId}">
 	<input type="hidden" name="spotId" value="${vo.spotId}">
 	<input type="hidden" name="date" value="${vo.date}">
 	<input type="hidden" name="type" value="2">
    <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">底价</label>
		      <div class="layui-input-inline">
		        <input type="text" name="floorPrice" lay-verify="required" value="${dto.floorPrice}" autocomplete="off" class="layui-input">
		      </div>
	    </div>
	</div>
    <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">活动价</label>
		      <div class="layui-input-inline">
		        <input type="text" name="activityPrice" id="activityPrice" value="${dto.activityPrice}" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    	<div class="layui-inline">
		      <label class="layui-form-label">景点价格</label>
		      <div class="layui-input-inline">
		        <input type="text" name="sellingPrice" id="sellingPrice" value="${dto.sellingPrice}" autocomplete="off" class="layui-input">
		      </div>
	    </div>
	</div>
	  <div class="layui-form-item">
    	<div class="layui-inline">
			<label class="layui-form-label">一级利润</label>
			<div class="layui-input-inline">
		        <input type="text" name="primaryProfit" id="primaryProfit" value="${dto.primaryProfit}"  autocomplete="off" class="layui-input">
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label">二级利润</label>
			<div class="layui-input-inline">
		        <input type="text" name="secondaryProfit" id="secondaryProfit" value="${dto.secondaryProfit}"  autocomplete="off" class="layui-input">
			</div>
	    </div>
	</div>
    <div class="layui-form-item">
      <label class="layui-form-label">备注</label>
      <div class="layui-input-inline">
         <input type="tel" name="memo" value="${dto.memo}" autocomplete="off" class="layui-input"  style="width: 515px;">
      </div>
    </div>
    <div class="layui-form-item">
	    <div class="layui-input-block">
	      <button type="submit" class="layui-btn" lay-submit="" lay-filter="btnSub" id="subBtn">立即提交</button>
	    </div>
  </div>
 </form>
</body>
 
   

<script type="text/javascript">
		var activce=null;
        layui.use(['table', 'form'], function () {
            var layer = layui.layer;
            var table = layui.table;
            var form = layui.form;
           
          //监听提交
            form.on('submit(btnSub)', function(data){
            	var activityPrice = $("#activityPrice").val();
            	var sellingPrice = $("#sellingPrice").val();
            	if((activityPrice == null && activityPrice ==null) || (activityPrice.length<=0 && sellingPrice.length<=0)){
            		layer.msg("活动价格 或者 景点价格 必选一个");
            		return false;
            	}
            	var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            	$.ajax({
                  	 url:'<%=basePath%>scenicroutesprice/saveOrUpdate',
                  	 data: data.field,
                  	 type: 'post',
                  	 dataType: 'json',
                  	 success:function(data){
                  		if(data.se){
                  			layer.msg(data.msg, { offset: '15px',icon: 6,time: 1000}, function(){
    	                  		parent.layer.close(index);
    			            	parent.location.reload();
    			            	parent.loadCalendar();
                      		});
                  		}else{
                  			layer.msg(data.msg, { offset: '15px',icon: 5,time: 1000}, function(){
    	                  		parent.layer.close(index);
    			            	parent.location.reload();
    			            	parent.loadCalendar();
                      		});
                  		}
                  	 },error:function(data){
                  		layer.msg('操作失败', { offset: '15px',icon: 5,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
			            	parent.loadCalendar();
                  		});
                  	 }
                 });
            	 
                 return false;
            });
        });
        
        function nextMonth() {
        	state.todayMonth += 1;
        	if (state.todayMonth == 12) {
        		state.todayYear += 1;
        		currentFullYear = analyizYear(state.todayYear);
        		state.todayMonth = 0;
        	}
        	currentFullMonth = currentFullYear.months[monthsStr[state.todayMonth]];
        	showCalenderInfo();
        }

        function prevMonth() {
        	state.todayMonth -= 1;
        	if (state.todayMonth == 0) {
        		state.todayYear -= 1;
        		currentFullYear = analyizYear(state.todayYear);
        		state.todayMonth = 11;
        	}
        	currentFullMonth = currentFullYear.months[monthsStr[state.todayMonth]];
        	showCalenderInfo();
        }
    </script>
</html>

