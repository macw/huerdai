<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>景点详情</title>
	<%@ include file="../../head.jsp" %>
	 <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }
    </style>
</head>
<body>
	<div class="layui-fluid">
		<div class="layui-elem-quote" style="margin-bottom: 0px;background: #fff">
			<p>景点详情</p>
		</div>
		<div class="layui-card">
			<div class="layui-form layui-row-form" style="padding: 10px;">
				<input type="hidden"  id="spotId"  name="spotId" value="${dto.spotId }">
				<textarea id="demo" style="display: none;">${dto.spotInfo}</textarea>
				<div class="layui-input-block" style="text-align: center; margin: 0; padding: 10px;">
					<button class="layui-btn layui-btn-normal" lay-submit lay-filter="form">保存</button>
					<button class="layui-btn layui-btn-primary" type="button" onclick="backPage()">取消</button>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		layui.use( ['form', 'layedit'], function() {
			var layedit = layui.layedit;
			var form = layui.form;
			var layer = layui.layer;
			var spotId = $("#spotId").val();
			layedit.set({
				uploadImage : {
					url : '<%=basePath%>saveSpotInfoImg?'+'spotId='+spotId
				}
			});
			var edit = layedit.build('demo',{
				height: 650 //设置编辑器高度
			}); //建立编辑器
			
			form.on('submit(form)', function(data) {
				var spotInfo = layedit.getContent(edit);
				var formData = {};
				formData.spotId = data.field.spotId;
				formData.spotInfo = spotInfo;
				$.ajax({
	           		 url:'<%=basePath%>scenic/spot/saveSpotInfo',
	           		 data:formData,
	              	 type: 'post',
	              	 dataType: 'json',
          			 success:function(d){
          				if(d.se){
                  			layer.msg("操作成功", { offset: '15px',icon: 6,time: 1000}, function(){
                  				window.history.back(-1);
                      		});
                  		}else{
                  			layer.msg("操作失败", { offset: '15px',icon: 5,time: 1000}, function(){
                  				window.history.back(-1);
                      		});
                  		} 
          			 }
          		});
				return false;
			});
		});
		function backPage() {
			window.history.go(-1);
		}
	</script>
</body>
 
</html>

