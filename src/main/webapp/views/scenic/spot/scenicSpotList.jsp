<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>风景地点</title>
	<%@ include file="../../head.jsp" %>
	 <style type="text/css">
		.layui-table-cell {
			font-size:14px;
		    padding:0 5px;
		    height:auto;
		    overflow:visible;
		    text-overflow:inherit;
		    white-space:normal;
		    word-break: break-all;
		    }
    </style>
</head>
<body>
<div class="layui-fluid">
    <div class="layui-card-body layui-form">
        <div style="padding-bottom: 10px;">
            <div class="layui-input-inline" style="width: 100px;">
                 <button type="button" class="layui-btn"  onclick="add()">添加</button>
            </div>
            <div class="layui-input-inline" style="width: 200px;">
				<select name="tId" id="tId" lay-search>
					<option value="">选择旅游公司</option>
					<c:forEach items="${tourOperatorList}" var="t">
						<option value="${t.tId }">${t.tName }</option>
					</c:forEach>
				</select>
			</div>
			<div class="layui-input-inline" style="width: 160px;">
		        <select name="province" id="province" lay-filter="province"  lay-search >
					<option value="">省</option>
					<c:forEach items="${provinceList}" var="p">
						<option value="${p.id}" <c:if test="${dto.province == p.id}">selected</c:if> >${p.name}</option>
					</c:forEach>
				</select>
		     </div>
   			 <div class="layui-input-inline" style="width: 160px;">
		        <select name="city" id="city"  lay-filter="city"  lay-search >
					<option value="">市</option>
			        <c:forEach items="${cityList}" var="c">
							<option value="${c.id}" <c:if test="${dto.city == c.id}">selected</c:if> >${c.name}</option>
					</c:forEach>
				</select>
		      </div>
   			 <div class="layui-input-inline" style="width: 160px;">
		        <select name="district" id="district"  lay-search >
					<option value="">区</option>
					 <c:forEach items="${districtList}" var="d">
							<option value="${d.id}" <c:if test="${dto.district == d.id}">selected</c:if> >${d.name}</option>
					</c:forEach>
				</select>
		     </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="spotName" id="spotName" placeholder="景点名称/位置" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="createdName" id="createdName" placeholder="创建人/修改人" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>
        <table id="tab" lay-filter="tab"></table>
    </div>
</div>
</body>
 
  <script type="text/html" id="tool">
        <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">编辑</button>
        <button type="button" class="layui-btn layui-btn-danger" lay-event="del">删除</button>
  </script>

<script type="text/javascript">

		var active=null;
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            table.render({
                elem: '#tab',
                url: '<%=basePath%>scenic/spot/selectData',  //数据接口
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                cols: [[
                    {type: "checkbox"},
                    {title: "景点名称", field: "spotName",align:"center",width :200},
                    {title: "景点主图", align:"center",width :150,
                    	templet: function(d){
                    		if(d.pictureurl!=null){
                    			return '<img src='+d.pictureurl+'></img>';
                    		}
                    		return '';
                    	}	
                    },
                    {title: "景点详情", align:"center",
                    	templet: function(d){
                    		return '<button type="button" class="layui-btn" lay-event="spotInfo"> <i class="layui-icon">&#xe642;</i>景点详情</button>';
                    	},width :130
                    },
                    {title: "优先级", field: "leavel",align:"center",width :80},
                    {title: "价格",align:"center",width :200,
                    	templet: function(d){
                    		var str='';
                    		if(d.floorPrice!=null){
                    			str+='出厂价格 : '+d.floorPrice+'<br/>';
                    		}
                    		if(d.activityPrice!=null){
                    			str+='活动价格 : '+d.activityPrice+'<br/>';
                    		}
                    		if(d.sellingPrice!=null){
                    			str+='门票价格 : '+d.sellingPrice+'<br/>';
                    		}
                    		if(d.preferentialQuota!=null){
                    			str+='利       润 : '+d.preferentialQuota+'<br/>';
                    		}
                    		return str;
                    	}	
                    },
                    {title: "设置景点日期价格",align:"center",width :200,
                    	templet: function(d){
                    		return '<button type="button" class="layui-btn layui-btn-normal" lay-event="editPrice">设置价格</button>';
                    	}	
                    },
                    {title: "利润",align:"center",width :200,
                    	templet: function(d){
                    		var str='';
                    		if(d.primaryProfit!=null){
                    			str ='一级利润 : 返点'+d.primaryProfit+'%<br/>';
                    		}
                    		if(d.secondaryProfit!=null){
                    			str+='二级利润 : 返点'+d.secondaryProfit+'%';
                    		}
                    		return str;
                    	}	
                    	
                    },
                    {title: "备注", field: "memo",align:"center",width :150},
                    {title: "位置", field: "fulladdress",align:"center",width :300},
                    {title: "所属旅游公司", field: "tName",align:"center",width :150},
                    {title: "创建人", field: "createdName",align:"center",width :120},
                    {title: "创建日期",align:"center",
                    	templet: function(d){
                    		if(d.creationDate!=null){
	                    		return layui.util.toDateString(d.creationDate);
                    		}
                    		return '';
                    	},width :160	
                    },
                    {title: "更新人", field: "lastUpdateName",align:"center",width :120},
                    {title: "更新日期",align:"center",
                    	templet: function(d){
                    		if(d.lastUpdateDate!=null){
	                    		return layui.util.toDateString(d.lastUpdateDate);
                    		}
                    		return '';
                    	},width :160	
                    },
                    {title: "状态",align:"center",width :100,
                    	templet: function(d){
                    		if(d.status!=null && d.status == 1){
	                    		return '<button type="button" class="layui-btn layui-btn-normal" lay-event="yn">启用</button>';
                    		}else if(d.status!=null && d.status == 2){
	                    		return '<button type="button" class="layui-btn layui-btn-danger" lay-event="yn">禁用</button>';
                    		}
                    		return '';
                    	}
                    },
                    {title: "操作",width :175, align:"center", templet: "#tool"}
                ]],done:function(res, curr){
        	    	  var brforeCurr = curr; // 获得当前页码
        	    	  var dataLength = res.data.length; // 获得当前页的记录数
        	    	  var count = res.count; // 获得总记录数
        	    	  if(dataLength == 0 && count != 0){ //如果当前页的记录数为0并且总记录数不为0
        	    		  table.reload("tab",{ // 刷新表格到上一页
        	    			  page:{
        	    				 curr:brforeCurr-1
        	    			  }
        	    		  });
        	    	  }
        	      }
            });
          	//监听行工具事件
            table.on('tool(tab)', function(obj){
              var data = obj.data;
              if(obj.event === 'yn'){
            	  var str = "启用";
            	  var status = 1;
            	  if(data.status == 1){
            		  str = "禁用";
            		  status = 2;
            	  }
                  layer.confirm('确认要['+str+']这个景点信息吗?', function(index){
                     $.ajax({
                    	 url:'<%=basePath%>scenic/spot/saveOrUpdate',
                    	 data: {"spotId":data.spotId,"status":status},
                    	 type: 'post',
                    	 dataType: 'json',
                    	 success:function(d){
                    		 table.reload('tab', {});
                    		 layer.msg(d.msg, {time: 1000, icon:6});
                    	 },error:function(d){
                    		 table.reload('tab', {});
                    		 layer.msg(d.msg, {time: 1000, icon:5});
                    	 }
                     });
                    	layer.close(index);
                  });
              }else if(obj.event === 'del'){
                layer.confirm('真的删除这条景点信息吗?', function(index){
                	$.ajax({
                   	 url:'<%=basePath%>/scenic/spot/deleteById',
                   	 data: {"spotId":data.spotId},
                   	 type: 'post',
                   	 dataType: 'json',
                   	 success:function(data){
                   		table.reload('tab', {});
                   		layer.msg(data.msg, {time: 1000, icon:6});
                   	 },error:function(data){
                   		table.reload('tab', {});
                   		layer.msg(data.msg, {time: 1000, icon:5});
                   	 }
                    });
                });
              } else if(obj.event === 'edit'){
            	  layer.open({
		    		  type: 2,
		    		  title: '景点编辑',
		    		  shadeClose: true,
		    		  shade: 0.8,
		    		  area: ['100%', '100%'],
		    		  content: '<%=basePath%>scenic/spot/editPage?spotId='+data.spotId
		     	});
              }else if(obj.event === 'spotInfo'){
            	  window.location.href = '<%=basePath%>scenic/spot/spotInfo?spotId='+data.spotId;
              }else if(obj.event === 'editPrice'){
            	  layer.open({
		    		  type: 2,
		    		  title: '景点编辑价格',
		    		  shadeClose: true,
		    		  shade: 0.8,
		    		  area: ['100%', '100%'],
		    		  content: '<%=basePath%>scenic/spot/editSpotCalendarPage?spotId='+data.spotId
		     	});
              }
            });
            form.on('select(province)', function(data){
           		$("#district").empty();
           		$("#city").empty();
        		var str="<option value=''>区</option>";
           		$("#district").append(str);
        		str="<option value=''>市</option>";
        		if(data.value.length <= 0){
        			$("#city").append(str);
        			form.render('select');
        		}else{
        			$.ajax({
		           		 url:'<%=basePath%>region/ajaxRegionList?pid='+data.value,
		              	 type: 'post',
		              	 dataType: 'json',
	           			 success:function(d){
	           				if(d.data.length>0){
		           				$(d.data).each(function(i,t){
		           					str+="<option value='"+t.id+"'>"+t.name+"</option>"
		           				})
	           				}
	           				$("#city").append(str);
			           		form.render('select');
	           			 }
	           		});
        		}
           	});
           	form.on('select(city)', function(data){
           		var str="<option value=''>区</option>";
           		$.ajax({
	           		 url:'<%=basePath%>region/ajaxRegionList?pid='+data.value,
	              	 type: 'post',
	              	 dataType: 'json',
          			 success:function(d){
          				$("#district").empty();
          				if(d.data.length>0){
	           				$(d.data).each(function(i,t){
	           					str+="<option value='"+t.id+"'>"+t.name+"</option>"
	           				})
          				}
          				$("#district").append(str);
          				form.render('select');
          			 }
          		});
           	});
            active = {
   		        add: function(){ //获取选中数据
			   		layer.open({
			    		  type: 2,
			    		  title: '添加景点',
			    		  shadeClose: true,
			    		  shade: 0.8,
			    		  area: ['100%', '100%'],
			    		  content: '<%=basePath%>scenic/spot/editPage'
			     	}); 
   		        },
   				doSearch: function(){
   					table.reload('tab', {
   	                    where: {"tId": $('#tId').val(),"spotName": $('#spotName').val(),"createdName": $('#createdName').val(),"province": $('#province').val(),"city": $('#city').val(),"district": $('#district').val()},
   	                    page: {
   	                        curr: 1 //重新从第 1 页开始
   	                      }
   	                });
   				},
   				tableReload:function(){
	   				 table.reload('tab', {});
   				}
           };
        });
        //搜索操作
        function doSearch() {
        	active.doSearch();
        };
        function add(){
        	active.add();
        };
        
        
        
    </script>
</html>

