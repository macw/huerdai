<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../../head.jsp" %>
</head>
<body>
 <form id="form" class="layui-form" enctype="multipart/form-data" style="margin-top: 20px;">
 	<input type="hidden" name="routesId" value="${dto.routesId}">
 	  <div class="layui-form-item">
    	<div class="layui-inline">
		     <label class="layui-form-label">旅游公司</label>
   			 <div class="layui-input-inline" style="width: 190px;">
		        <select name="tId" id="tId" lay-verify="required"  lay-search>
					<option value="">选择旅游公司</option>
					<c:forEach items="${tourOperatorList}" var="t">
						<option value="${t.tId }" <c:if test="${t.tId == dto.tId}"> selected </c:if> >${t.tName } </option>
					</c:forEach>
				</select>
		     </div>
	    </div>
    	<div class="layui-inline">
		      <label class="layui-form-label">路线名称</label>
		      <div class="layui-input-inline">
		        <input type="text" name="routesName" value="${dto.routesName}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
    	<div class="layui-inline">
			<label class="layui-form-label">景点名称</label>
			<div class="layui-input-inline">
		        <input type="text" name="spotNames" id="spotNames" value="${dto.spotNames}" lay-verify="required"  autocomplete="off" class="layui-input" readonly="readonly" style="background-color: #c2c2c2;">
			</div>
	        <input type="hidden" name="spotId" id="spotId" value="${dto.spotId}">
		</div>
		<div class="layui-inline">
			<div class="layui-input-inline">
				<button type="button" class="layui-btn" id="chooseSpot">选择景点</button>
			</div>	
	    </div>
	</div>
    <div class="layui-form-item">
      <label class="layui-form-label">备注</label>
      <div class="layui-input-inline">
         <input type="tel" name="memo" value="${dto.memo}" lay-verify="required" autocomplete="off" class="layui-input"  style="width: 515px;">
      </div>
    </div>
    <div class="layui-form-item">
	    <div class="layui-input-block">
	      <button type="submit" class="layui-btn" lay-submit="" lay-filter="btnSub" id="subBtn">立即提交</button>
	    </div>
  </div>
 </form>
</body>
 
   

<script type="text/javascript">
		var activce=null;
        layui.use(['table', 'form'], function () {
            var layer = layui.layer;
            var table = layui.table;
            var form = layui.form;
           
          //监听提交
            form.on('submit(btnSub)', function(data){
            	var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            	$.ajax({
                  	 url:'<%=basePath%>scenic/routes/saveOrUpdate',
                  	 data: data.field,
                  	 type: 'post',
                  	 dataType: 'json',
                  	 success:function(data){
                  		if(data.se){
                  			layer.msg(data.msg, { offset: '15px',icon: 6,time: 1000}, function(){
    	                  		parent.layer.close(index);
    			            	parent.location.reload();
                      		});
                  		}else{
                  			layer.msg(data.msg, { offset: '15px',icon: 5,time: 1000}, function(){
    	                  		parent.layer.close(index);
    			            	parent.location.reload();
                      		});
                  		}
                  	 },error:function(data){
                  		layer.msg('操作失败', { offset: '15px',icon: 5,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                  		});
                  	 }
                 });
            	 
                 return false;
            });
            activce = {
           	   chooseSpot:function(){
	           		layer.open({
	           			  type: 2,
			    		  title: '选择景点',
			    		  shadeClose: true,
			    		  shade: 0.8,
			    		  area: ['100%', '100%'],
			    		  content: '<%=basePath%>scenic/spot/multiple/choosePage'
	           		});
           	   }
            		
            }
        });
        
        $("#chooseSpot").click(function(){
        	activce.chooseSpot();
        });
        function setData(data) {
        	var spotId="";
        	var spotNames="";
        	if(data.length>0){
        		$(data).each(function(i,item){
        			spotNames+=item.spotName+",";
        			spotId+=item.spotId+",";
        		});
        	}
        	if(spotNames.length>1){
        		spotNames = spotNames.substring(0,spotNames.length-1);
        		spotId = spotId.substring(0,spotId.length-1);
        	}
        	$("#spotId").val(spotId);
        	$("#spotNames").val(spotNames);
        }
    </script>
</html>

