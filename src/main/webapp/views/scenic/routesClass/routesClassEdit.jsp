<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="../../head.jsp" %>
<style type="text/css">
.layui-input-inline {
    float: left;
    width: 231px;
    margin-right: 10px;
}
</style>
</head>

<body>
 <form id="form" class="layui-form" enctype="multipart/form-data" style="margin-top: 20px;">
 	<input type="hidden" name="routesclassId" value="${dto.routesclassId}">
    <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">分类名称</label>
		      <div class="layui-input-inline">
		        <input type="text" name="routesclassName" value="${dto.routesclassName}" lay-verify="required" autocomplete="off" class="layui-input" style="width: 231px">
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">跳转URL</label>
		      <div class="layui-input-inline">
		        <input type="text" name="url" value="${dto.url}" lay-verify="required" autocomplete="off"  class="layui-input" style="width: 231px">
		      </div>
	    </div>
    </div>
    <div class="layui-form-item">
   		 <div class="layui-inline">
			<label class="layui-form-label">上传图片</label>
			<div class="layui-input-inline" style="width: 80px;">
				<button type="button" class="layui-btn" id="file">
					<i class="layui-icon"></i>上传文件
				</button>
			</div>
			<div class="layui-input-inline">
				 <img class="layui-upload-img" name="img" src="${dto.img}" id="img" style="max-width: 100px;max-height: 100px; padding-left: 40px; width: 231px">
			</div>
		</div>
		
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">备注</label>
      <div class="layui-input-inline">
         <textarea placeholder="备注" class="layui-textarea" name="memo" lay-verify="required" autocomplete="off"  style="width: 231px">${dto.memo}</textarea>
      </div>
    </div>
    <div class="layui-form-item">
	    <div class="layui-input-block">
	      <button type="submit" class="layui-btn" lay-submit="" lay-filter="btnSub" id="subBtn">立即提交</button>
	    </div>
  </div>
 </form>
</body>
 
   

<script type="text/javascript">
		var activce=null;
        layui.use(['table', 'form','upload'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            var upload = layui.upload;
            upload.render({
				elem : '#file',
				auto : false,
				accept: "images",
                acceptMime: 'image/*',//打开文件选择框时,只显示图片文件
                auto: false,  //是否选完文件后自动上传。默认值：true
                bindAction : '#test9',
               // bindAction: '#subBtn',
				choose : function(obj) {
					obj.preview(function(index, file, result) {
						$('#img').attr('src', result);
					})
				}
			});
          //监听提交
            form.on('submit(btnSub)', function(data){
            	var src= $("#img").attr("src");
            	if(src == null || src.length <=0){
            		layer.msg('请上传图片', { offset: '15px',icon: 5,time: 1000});
            		return false;
            	}
           		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            	var formData = new FormData(document.getElementById("form"));
            	$.ajax({
                  	 url:'<%=basePath%>scenic/routesClass/saveOrUpdate',
                  	 data: formData,
                  	 type: 'post',
                  	 dataType: 'json',
                  	 processData : false,
					 contentType : false,
                  	 success:function(data){
                  		 if(data.se){
                  			layer.msg(data.msg, { offset: '15px',icon: 6,time: 1000}, function(){
    	                  		parent.layer.close(index);
    			            	parent.location.reload();
                      		});
                  		 }else{
                  			layer.msg(data.msg, { offset: '15px',icon: 5,time: 1000}, function(){
    	                  		parent.layer.close(index);
    			            	parent.location.reload();
                      		});
                  		 }
                  		
                  	 },error:function(data){
                  		layer.msg('操作失败', { offset: '15px',icon: 5,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                  		});
                  	 }
                 });
                 return false;
            });
        });
        
    </script>
</html>

