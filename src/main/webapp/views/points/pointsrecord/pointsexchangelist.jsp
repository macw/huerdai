<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ include file="../../meta.jsp" %>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>客户信息</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="${basepath}statics/layuiadmin/layui/css/layui.css" media="all">
 <link rel="stylesheet" href="${basepath}statics/layuiadmin/style/admin.css" media="all">
   <script type="text/javascript"  src="${basepath}statics/cookies.js"></script>
</head>
<body>

  <div class="layui-fluid">
    <div class="layui-card">
      <div class="layui-form layui-card-header layuiadmin-card-header-auto">
        <div class="layui-form-item">
          <div class="layui-inline">
            <label class="layui-form-label">客户ID</label>
            <div class="layui-input-block">
              <input type="text" name="cuid" placeholder="请输入" autocomplete="off" class="layui-input">
            </div>
          </div>
          <div class="layui-inline">
            <label class="layui-form-label">客户名称</label>
            <div class="layui-input-block">
              <input type="text" name="showname" placeholder="请输入" autocomplete="off" class="layui-input">
            </div>
          </div>
             <div class="layui-inline">
            <label class="layui-form-label">手机号</label>
            <div class="layui-input-block">
              <input type="text" name="mobile" placeholder="请输入" autocomplete="off" class="layui-input">
            </div>
          </div>
        
         
          <div class="layui-inline">
            <button class="layui-btn layuiadmin-btn-useradmin" lay-submit lay-filter="LAY-user-front-search">
              <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
            </button>
          </div>
        </div>
      </div>
      
      <div class="layui-card-body">
        <div style="padding-bottom: 10px;">
          <button class="layui-btn layuiadmin-btn-useradmin" data-type="batchdel">删除</button>
          <button class="layui-btn layuiadmin-btn-useradmin" data-type="add">添加</button>
        </div>
        
        <table id="LAY-cus-manage" lay-filter="LAY-user-manage"></table>
        <script type="text/html" id="imgTpl"> 
          <img style="display: inline-block; width: 50%; height: 100%;" src= {{ d.avatar }}>
        </script> 
        <script type="text/html" id="table-useradmin-webuser">
          <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="edit"><i class="layui-icon layui-icon-edit"></i>编辑</a>
          <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"><i class="layui-icon layui-icon-delete"></i>删除</a>
        </script>
      </div>
    </div>
  </div>
   <script type="text/javascript"  src="${basepath}statics/layuiadmin/layui/layui.js"></script>  
   <script type="text/javascript"  src="${basepath}statics/points/pointsexchange.js"></script>
  <script>
  
  function writeObj(obj){ 
	  var description = ""; 
	  for(var i in obj){ 
	  var property=obj[i]; 
	  description+=i+" = "+property+"\n"; 
	  } 
	  alert(description); 
	 }
  layui.config({
	  base: '${pageContext.request.contextPath}/statics/layuiadmin/' //静态资源所在路径,
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index', 'useradmin', 'table'], function(){
    var $ = layui.$
    ,form = layui.form
    ,table = layui.table;
    
    //监听搜索
    form.on('submit(LAY-user-front-search)', function(data){
      var field = data.field;
      
      //执行重载
      table.reload('LAY-user-manage', {
        where: field
      });
    });
  
    //事件
    var active = {
      batchdel: function(){
        var checkStatus = table.checkStatus('LAY-user-manage')
        ,checkData = checkStatus.data; //得到选中的数据

        if(checkData.length === 0){
          return layer.msg('请选择数据');
        }
        
          layer.confirm('敏感操作,此角色下所有的配置都会删除？', function(index) {
            
        	  for ( var i = 0; i <checkData.length; i++){
          		
            		var da = JSON.stringify(checkData[i]);0
            		var page = eval("("+da+")");
            		$.ajax({ 
            			url: "delhmroleform?roleid="+page.roleid,
               		contentType: "application/x-www-form-urlencoded; charset=utf-8", 
               		success: function(data){
               			if(data.code == 100){ 
               				
               			}else{
               				table.reload('LAY-user-back-manage');
              	            layer.msg('删除失败');
               			}
               		}
               	});
         		 }
          	
          	layer.load(0, {
                  shade:  [0.4,'#fff'],
                  time: 2*1000,
                  end:function(){
                	   table.reload('LAY-user-manage');
                       layer.msg('已删除');
                  }
              }); 
         
          });
       
      }
      ,add: function(){
        layer.open({
          type: 2
          ,title: '添加用户'
          ,content: 'toaddrole'
          ,maxmin: true
          ,area: ['500px', '450px']
          ,btn: ['确定', '取消']
          ,yes: function(index, layero){
            var iframeWindow = window['layui-layer-iframe'+ index]
            ,submitID = 'LAY-user-front-submit'
            ,submit = layero.find('iframe').contents().find('#'+ submitID);

            //监听提交
            iframeWindow.layui.form.on('submit('+ submitID +')', function(data){
              var field = data.field; //获取提交的字段
              //提交 Ajax 成功后，静态更新表格中的数据
              //$.ajax({});
              var sessionid = getCookie("sessonid");
              $.ajax({ 
            		url: "addhmroleform?sessionid="+sessionid ,
            		contentType: "application/x-www-form-urlencoded; charset=utf-8", 
                    data: field,
            		success: function(data){
            			if(data.code == 100){
            				table.reload('LAY-user-manage'); //数据刷新
            	              layer.close(index); //关闭弹层
            			}else{
            				//location.href = '${basepath}login'; //后台主页
            			}
            		}
            	});
            });  
            submit.trigger('click');
          }
        }); 
      }
    };
    
    $('.layui-btn.layuiadmin-btn-useradmin').on('click', function(){
      var type = $(this).data('type');
      active[type] ? active[type].call(this) : '';
    });
  });
  </script>
</body>
</html>
