<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>钱包信息管理</title>
	<%@ include file="../head.jsp" %>
</head>
<body>
 <form class="layui-form" style="margin-top: 20px;">
 	<input type="hidden" name="id" value="${dto.id}">
     <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">用户</label>
		      <div class="layui-input-inline">
		        <select name="cusId" id="cusId" lay-verify="required" lay-search >
					<option value="">选择用户</option>
					<c:forEach items="${customerInfoList}" var="c">
						<option value="${c.customerId }" <c:if test="${c.customerId == dto.cusId}"> selected </c:if> >${c.customerName } </option>
					</c:forEach>
				</select>
		      </div>
	    </div>
    	<div class="layui-inline">
		      <label class="layui-form-label">金额</label>
		      <div class="layui-input-inline">
		        <input type="text" name="money" value="${dto.money}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    </div>
     <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">姓名</label>
		      <div class="layui-input-inline">
		        <input type="text" name="name" value="${dto.name}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    	<div class="layui-inline">
		      <label class="layui-form-label">身份证</label>
		      <div class="layui-input-inline">
		        <input type="text" name="idcard" value="${dto.idcard}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    </div>
     <div class="layui-form-item">
    	<div class="layui-inline">
		      <label class="layui-form-label">支付密码</label>
		      <div class="layui-input-inline">
		        <input type="text" name="payPassword" value="${dto.payPassword}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    	<div class="layui-inline">
		      <label class="layui-form-label">支付宝提现</label>
		      <div class="layui-input-inline">
		        <input type="text" name="txAlipay" value="${dto.txAlipay}" lay-verify="required" autocomplete="off" class="layui-input">
		      </div>
	    </div>
    </div>
    
    <div class="layui-form-item">
	    <div class="layui-input-block">
	      <button type="submit" class="layui-btn" lay-submit="" lay-filter="btnSub">立即提交</button>
	    </div>
  </div>
 </form>
</body>
 
   

<script type="text/javascript">
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
          //监听提交
            form.on('submit(btnSub)', function(data){
           		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            	$.ajax({
                  	 url:'<%=basePath%>wallet/saveOrUpdate',
                  	 data: data.field,
                  	 type: 'post',
                  	 dataType: 'json',
                  	 success:function(data){
                  		if(data.se){
	                  		layer.msg(data.msg, { offset: '15px',icon: 6,time: 1000}, function(){
		                  		parent.layer.close(index);
				            	parent.location.reload();
	                  		});
                  		 }else{
	                  		layer.msg(data.msg, { offset: '15px',icon: 5,time: 1000}, function(){
		                  		parent.layer.close(index);
				            	parent.location.reload();
	                  		});
                  		 }
                  	 },error:function(data){
                  		layer.msg('操作失败', { offset: '15px',icon: 5,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                  		});
                  	 }
                 });
            	 
                 return false;
            });
        });
        
        
    </script>
</html>

