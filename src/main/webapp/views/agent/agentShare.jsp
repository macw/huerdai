<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>代理商管理</title>
	<%@ include file="../head.jsp" %>
</head>
<body>
 <form class="layui-form" style="margin-top: 20px;">
 
 	<input type="hidden" id="copy">
 	<div class="layui-form-item">
    	<div class="layui-inline">
	       <label class="layui-form-label">分享code</label>
	       <div class="layui-input-block" style="margin-top: 11px;" >
	        	<span id="sharecode">${sharecode}</span> 
	        	<span style="margin-left: 100px; color: cornflowerblue;" onclick="copyLink('sharecode')">复制</span>
	       </div>
	    </div>
 	</div>
 	<div class="layui-form-item">
    	<div class="layui-inline">
	       <label class="layui-form-label">code链接</label>
	       <div class="layui-input-block" style="margin-top: 11px;" >
	       		<span id="sharecodeUrl">${sharecodeUrl}</span>
	       		<span style="margin-left: 100px; color: cornflowerblue;" onclick="copyLink('sharecodeUrl')">复制</span>
	       </div> 
	    </div>
 	</div>
    <div class="layui-form-item">
    	<div class="layui-inline">
	       <label class="layui-form-label">code二维码</label>
	       <div class="layui-input-block">
	         <img src="${shareQRCode}">
	       	<a href="${shareQRCode}" download="w3logo" style="margin-left: 100px; color: cornflowerblue;">下载</a>
	       </div>
	    </div>
    </div>
 </form>
</body>
 
   

<script type="text/javascript">
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
        });
        
        function copyLink(id){
        	$("#copy").val($("#"+id).html());
        	$("#copy").select();
        	document.execCommand("Copy")
        	layer.msg("复制成功");
        }
    </script>
</html>

