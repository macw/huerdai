<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/12/6
  Time: 15:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../head.jsp" %>
<html>
<head>
    <title>查询代理商下的订单信息</title>
</head>
<body>

<div class="layui-card-body">
    <div style="padding-bottom: 10px;" id="LAY_lay_add">
        &nbsp;
       <%-- <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="orderSn" id="orderSn"  placeholder="请输入订单编号" autocomplete="off"
                   class="layui-input">
        </div>--%>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="goodsname" id="goodsname"  placeholder="请输入商品名称" autocomplete="off"
                   class="layui-input">
        </div>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="nickname"  id="nickname" placeholder="请输入用户昵称" autocomplete="off"
                   class="layui-input">
        </div>
        <div class="layui-input-inline" style="width: 100px;">
            <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                <i class="layui-icon layui-icon-search"></i> 搜索
            </button>
        </div>
    </div>

    <table id="Lay_back_table" lay-filter="Lay_back_table"></table>

</div>


<script type="text/javascript">
    layui.use(['table', "layer", 'form'], function () {
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var $ = layui.jquery;

        table.render({
            elem: '#Lay_back_table',
            url: '${pageContext.request.contextPath}/userAgent/selOrder?userId='+${userId}, //数据接口
            page: true,
            limit: 10,
            limits: [10, 20, 30],
            cols: [[
                {type: "checkbox"},
                {title: "订单编号", field: "orderSn", align: "center"},
                {title: "商品名称", field: "goodsname", align: "center"},
                {title: "商品图片", field: "pictureUrl", align: "center",
                    templet: function (d) {
                        return '<img width="50" src="' + d.pictureUrl + '">'
                    }
                },
                {title: "用户昵称", field: "customerIcon", align: "center",
                    templet: function (d) {
                        if(d.customerIcon !=null) {
                            return '<img width="20" src="' + d.customerIcon + '">'+d.nickname
                        }else{
                            return d.nickname;
                        }
                    }
                },
                {title: "订单总金额", field: "orderPriceAll", align: "center"},
                {title: "优惠抵扣总金额", field: "subPriceAll", align: "center"},
                {title: "实际支付金额", field: "salePrice", align: "center"},
                {title: "购买数量", field: "number", align: "center"},
                {title: "用户返利金额", field: "cusMoney", align: "center"},
                {title: "上级用户返利金额", field: "parentcusMoney", align: "center"},
                {title: "代理商利润金额", field: "agentMoney", align: "center"},
                {
                    title: "订单创建时间",
                    field: "createTime", align: "center",
                    templet: function (d) {
                        if (d.creationDate != null) {
                            return layui.util.toDateString(d.creationDate);
                        }
                        return '';
                    }
                }
            ]]
        });

        //刷新表格方法
        function tableReload() {
            table.reload('Lay_back_table', {});
        };

        //监听行工具事件
        table.on('tool(Lay_back_table)', function (obj) {
            var data = obj.data;
            if (obj.event === 'yn') {
                var str = obj.data.userStatus == 0 ? "启用" : "禁用";
                var state = obj.data.userStatus == 1 ? 0 : 1;
                layer.confirm('确认要[' + str + ']这个后台用户吗?', function (index) {
                    $.ajax({
                        url: '${pageContext.request.contextPath}/user/updateStatus',
                        data: {"userId": data.userId, "userStatus": state},
                        type: 'post',
                        dataType: 'json',
                        success: function (data) {
                            tableReload();
                            layer.msg("操作成功", {time: 1000, icon: 6});
                        }, error: function (data) {
                            tableReload();
                            layer.msg("操作失败", {time: 1000, icon: 5});
                        }
                    });
                    layer.close(index);
                });
            }
            else if (obj.event === 'selOrder'){
                layer.open({
                    title: "查看订单",
                    content: "${pageContext.request.contextPath}/userAgent/toEditUserAgent?userIdAgent=" + data.userId,
                    type: 2,
                    maxmin: true,
                    area: ['500px', '480px'],
                    end: function () {
                        window.location.reload();
                    }
                });
            }

            else if (obj.event === 'edit') {
                // alert("userid==="+data.userId);
                layer.open({
                    title: "修改代理商信息",
                    content: "${pageContext.request.contextPath}/userAgent/toEditUserAgent?userIdAgent=" + data.userId,
                    type: 2,
                    maxmin: true,
                    area: ['500px', '480px'],
                    end: function () {
                        window.location.reload();
                    }
                });
            }

        });
        $("#add").click(function () {
            layer.open({
                title: "添加代理商",
                content: "${pageContext.request.contextPath}/userAgent/toEditUserAgent",
                type: 2,
                maxmin: true,
                area: ['500px', '480px'],
                end: function () {
                    window.location.reload();
                }

            });
        });

    });


    function dodel(userId) {
        layui.use(['layer', 'table'], function () {
            var layer = layui.layer;
            var table = layui.table;
            var $ = layui.jquery;
            layer.confirm('确定要删除吗？', {icon: 3, title: '确认删除'}, function (index) {
                $.ajax({
                    url: "${pageContext.request.contextPath}/user/deleteOne",
                    data: "aid=" + userId,
                    success: function (data) {
                        layer.alert("删除" + data.msg, {time: 2000});
                        table.reload("Lay_back_table");
                    },
                    error: function (data) {
                        table.reload("Lay_back_table");
                        layer.msg("操作失败", {time: 1000, icon: 5});
                    }
                });
                layer.close(index);
            });
        });
    }

    //搜索操作
    function doSearch() {
        //1.获取到输入框中输入的内容
        //发送请求，并且接收数据
        layui.use('table', function () {
            var table = layui.table;
            table.reload('Lay_back_table', {
                where: {"userId":${userId},"orderSn": $("#orderSn").val(),"nickname":$("#nickname").val(),"goodsname":$("#goodsname").val()}
            });
        });
    }


    function doMultiDelete() {
        //获取到选中的内容的id===》table模块中找方法
        layui.use(['layer', 'table'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var $ = layui.jquery;
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].userId;
                    }
                    console.log("ids===" + ids);
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/user/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert("删除" + data.msg, {time: 2000});
                            //刷新table
                            table.reload("Lay_back_table");
                        }
                    })
                });
            }
        });
    }

</script>
</body>
</html>
