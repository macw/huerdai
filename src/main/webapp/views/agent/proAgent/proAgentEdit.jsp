<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/12/5
  Time: 16:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../head.jsp" %>
<html>
<head>
    <title>编辑省代信息</title>
</head>
<body>
<%--弹出层--%>

<form id="addForm" class="layui-form">
    <div class="layui-form" lay-filter="layuiconfig-form-role" id="layuiconfig-form-role"
         style="padding: 20px 30px 0 0;">
        <div class="layui-form-item" id="demo0">
            <label class="layui-form-label">代理商名称</label>
            <div class="layui-input-block">
                <input name="name" id="name" value="${ua.name}" class="layui-input">
                <input name="userId" id="userId" value="${ua.userId}" lay-type="hide" type="hidden"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">登录账号</label>
            <div class="layui-input-block">
                <input name="loginName" value="${ua.loginName}" id="loginName" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">登录密码</label>
            <div class="layui-input-block">
                <input name="password" value="${ua.password}" id="password" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">备注</label>
            <div class="layui-input-block">
                <input name="remark" value="${ua.remark}" id="remark" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button type="submit" class="layui-btn" lay-filter="btnSub" lay-submit >立即提交</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    layui.use(['form', 'layer'], function () {
        var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
        var layer = layui.layer;
        var $ = layui.jquery;
        form.render();

        //监听提交
        form.on('submit(btnSub)', function (data) {
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            console.log(data.field);
            $.ajax({
                url: '${pageContext.request.contextPath}/userAgent/updateOrSaveAgent',
                data: data.field,
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    layer.msg('操作成功', {
                        offset: '15px',
                        icon: 6,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                },
                error: function (data) {
                    layer.msg('操作失败', {
                        offset: '15px',
                        icon: 5,
                        time: 1000
                    }, function () {
                        parent.layer.close(index);
                        parent.layui.table.reload('Lay_back_table', {});
                    });
                }
            });
            return false;
        });
    });

</script>
</body>
</html>