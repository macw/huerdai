<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/12/5
  Time: 16:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../head.jsp" %>
<html>
<head>
    <title>省代管理</title>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/md5.js"></script>
</head>
<body>

<div class="layui-card-body">
    <div style="padding-bottom: 10px;" id="LAY_lay_add">
        <button type="button" class="layui-btn layui-btn-danger" onclick="doMultiDelete()">
            <i class="layui-icon layui-icon-delete"></i> 批量删除
        </button>
        <button class="layui-btn layuiadmin-btn-role " data-type="add" id="add">
            <i class="layui-icon layui-icon-add-circle-fine"></i> 添加
        </button>
        &nbsp;
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="search" id="Lay_toSearch_input" placeholder="请输入代理商名称" autocomplete="off"
                   class="layui-input">
        </div>
        <div class="layui-input-inline" style="width: 100px;">
            <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                <i class="layui-icon layui-icon-search"></i> 搜索
            </button>
        </div>
    </div>


    <table id="Lay_back_table" lay-filter="Lay_back_table"></table>


</div>

<script type="text/html" id="updateAndDelete">
    <button type="button" class="layui-btn  layui-btn-normal" lay-event="share">查看分享</button>
    <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">
        <i class="layui-icon layui-icon-edit"></i>修改
    </button>
    <button type="button" class="layui-btn layui-btn-danger" onclick="dodel({{d.userId}})">
        <i class="layui-icon layui-icon-delete"></i>删除
    </button>
</script>

<script type="text/javascript">
    layui.use(['table', "layer", 'form'], function () {
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var $ = layui.jquery;

        table.render({
            elem: '#Lay_back_table',
            url: '${pageContext.request.contextPath}/userAgent/selectAll', //数据接口
            page: true,
            limit: 10,
            limits: [10, 20, 30],
            cols: [[
                {type: "checkbox"},
                {title: "代理商名称", field: "name", align: "center"},
                {title: "代理商账号", field: "loginName", align: "center"},
                {title: "备注", field: "remark", align: "center"},
                {
                    title: "查看订单信息", align: "center",
                    templet: function (d) {
                        return '<button type="button" class="layui-btn "  lay-event="selOrder">订单信息</button>';
                    }
                },
                {
                    title: "创建时间",
                    field: "createTime", align: "center",
                    templet: function (d) {
                        if (d.createTime != null) {
                            return layui.util.toDateString(d.createTime);
                        }
                        return '';
                    }
                },
                {
                    title: "代理商状态", width: 100, align: "center",
                    templet: function (d) {
                        if (d.userStatus == 1) {
                            return '<button type="button" class="layui-btn  layui-btn-normal"  lay-event="yn">启用</button>';
                        } else {
                            return '<button type="button" class="layui-btn  layui-btn-danger"  lay-event="yn">禁用</button>';
                        }
                    }
                },
                {title: "操作", templet: "#updateAndDelete", align: "center", width: 320}
            ]]

        });

        //刷新表格方法
        function tableReload() {
            table.reload('Lay_back_table', {});
        };

        //监听行工具事件
        table.on('tool(Lay_back_table)', function (obj) {
            var data = obj.data;
            if (obj.event === 'yn') {
                var str = obj.data.userStatus == 0 ? "启用" : "禁用";
                var state = obj.data.userStatus == 1 ? 0 : 1;
                layer.confirm('确认要[' + str + ']这个后台用户吗?', function (index) {
                    $.ajax({
                        url: '${pageContext.request.contextPath}/user/updateStatus',
                        data: {"userId": data.userId, "userStatus": state},
                        type: 'post',
                        dataType: 'json',
                        success: function (data) {
                            tableReload();
                            layer.msg("操作成功", {time: 1000, icon: 6});
                        }, error: function (data) {
                            tableReload();
                            layer.msg("操作失败", {time: 1000, icon: 5});
                        }
                    });
                    layer.close(index);
                });
            }
            else if (obj.event === 'selOrder'){
                // alert(data.userId)
                var index = layer.open({
                    title: "查看订单",
                    content: "${pageContext.request.contextPath}/userAgent/toselOrder?userId=" + data.userId,
                    type: 2,
                    maxmin: true,
                    area: ['1500px', '800px'],
                    end: function () {
                        window.location.reload();
                    }
                });
                layer.full(index);
            }

            else if (obj.event === 'edit') {
                // alert("userid==="+data.userId);
                layer.open({
                    title: "修改代理商信息",
                    content: "${pageContext.request.contextPath}/userAgent/toEditUserAgent?userIdAgent=" + data.userId,
                    type: 2,
                    maxmin: true,
                    area: ['500px', '480px'],
                    end: function () {
                        window.location.reload();
                    }
                });
            } else if(obj.event === 'share'){
          	  layer.open({
         		  type: 2,
         		  title: '查看分享信息',
         		  shadeClose: true,
         		  shade: 0.8,
         		  area: ['750px', '544px'],
         		  content: '<%=basePath%>userAgent/sharePage?userId='+data.userId
          	});  
          }

        });
        $("#add").click(function () {
            layer.open({
                title: "添加代理商",
                content: "${pageContext.request.contextPath}/userAgent/toEditUserAgent",
                type: 2,
                maxmin: true,
                area: ['500px', '480px'],
                end: function () {
                    window.location.reload();
                }

            });
        });

    });


    function dodel(userId) {
        layui.use(['layer', 'table'], function () {
            var layer = layui.layer;
            var table = layui.table;
            var $ = layui.jquery;
            layer.confirm('确定要删除吗？', {icon: 3, title: '确认删除'}, function (index) {
                $.ajax({
                    url: "${pageContext.request.contextPath}/user/deleteOne",
                    data: "aid=" + userId,
                    success: function (data) {
                        layer.alert("删除" + data.msg, {time: 2000});
                        table.reload("Lay_back_table");
                    },
                    error: function (data) {
                        table.reload("Lay_back_table");
                        layer.msg("操作失败", {time: 1000, icon: 5});
                    }
                });
                layer.close(index);
            });
        });
    }

    //搜索操作
    function doSearch() {
        //1.获取到输入框中输入的内容
        var searchName = $('#Lay_toSearch_input').val();
        //发送请求，并且接收数据
        layui.use('table', function () {
            var table = layui.table;
            table.reload('Lay_back_table', {
                where: {"name": searchName}
            });
        });
    }


    function doMultiDelete() {
        //获取到选中的内容的id===》table模块中找方法
        layui.use(['layer', 'table'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var $ = layui.jquery;
            //获取到选中的数据
            var checkStatus = table.checkStatus('Lay_back_table'); //idTest 即为基础参数 id 对应的值
            // console.log(checkStatus.data);//获取选中行的数据
            var data = checkStatus.data;

            if (data.length == 0) {
                layer.alert("请选中要删除的数据");
            } else {
                layer.confirm("确定要删除选中的所有数据", function (index) {
                    //把所有选中的数据的id封装到一个数组中
                    var ids = new Array(data.length);
                    for (var i = 0; i < ids.length; i++) {
                        ids[i] = data[i].userId;
                    }
                    console.log("ids===" + ids);
                    //执行删除操作
                    $.ajax({
                        url: "${pageContext.request.contextPath}/user/deleteMany",
                        data: "ids=" + ids,
                        success: function (data) {
                            //删除确认框关闭掉
                            layer.close(index);
                            //删除提示
                            layer.alert("删除" + data.msg, {time: 2000});
                            //刷新table
                            table.reload("Lay_back_table");
                        }
                    })
                });
            }
        });
    }

</script>
</body>
</html>
