<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>代理商信息</title>
	<%@ include file="../head.jsp" %>
	 <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>
</head>
<body>
<div class="layui-fluid">
    <div class="layui-card-body">
        <div style="padding-bottom: 10px;">
            <div class="layui-input-inline" style="width: 100px;">
                 <button type="button" class="layui-btn add" data-type="add" onclick="add()">添加</button>
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="agenName" id="agenName" placeholder="代理商名称" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
	             <input type="text" name="phone" id="phone" placeholder="手机号码" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="createdName" id="createdName" placeholder="创建人/修改人" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>
        <table id="agent-tab" lay-filter="agent-tab"></table>
    </div>
</div>
</body>
 
  <script type="text/html" id="tool">
		<button type="button" class="layui-btn  layui-btn-normal" lay-event="share">查看分享</button>
        <button type="button" class="layui-btn  layui-btn-normal" lay-event="edit">编辑</button>
        <button type="button" class="layui-btn layui-btn-danger" lay-event="del">删除</button>
  </script>

<script type="text/javascript">

        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            table.render({
                elem: '#agent-tab',
                url: '<%=basePath%>agent/selectData', //数据接口
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                cols: [[
                    {type: "checkbox"},
                    {title: "代理商名称", field: "agenName",align:"center",width :150},
                    {title: "联系方式", field: "phone",align:"center",width :120},
                    {title: "地址",align:"center",
                    	templet: function(d){
                    		return d.fullAddress;
                    	}
                    },
                    {title: "经纪人人数", field: "brokersNum",align:"center",width :100},
                    {title: "创建人", field: "createdName",align:"center",width :90},
                    {title: "创建时间",align:"center",
                    	templet: function(d){
                    		if(d.creationDate!=null){
	                    		return layui.util.toDateString(d.creationDate);
                    		}
                    		return '';
                    	},width :160	
                    },
                    {title: "状态", width :100,align:"center",
                    	templet: function(d){
	                    	if(d.status == 1){
	                    		return '<button type="button" class="layui-btn  layui-btn-normal"  lay-event="yn">启用</button>';
	                    	}else if(d.status == 2){
	                    		return '<button type="button" class="layui-btn  layui-btn-danger"  lay-event="yn">禁用</button>';
	                    	}else{
	                    		return '';
	                    	}
                   		}
                	},
                    {title: "操作",width :280, align:"center", templet: "#tool"}
                ]],done:function(res, curr){
      	    	  var brforeCurr = curr; // 获得当前页码
    	    	  var dataLength = res.data.length; // 获得当前页的记录数
    	    	  var count = res.count; // 获得总记录数
    	    	  if(dataLength == 0 && count != 0){ //如果当前页的记录数为0并且总记录数不为0
    	    		  table.reload("agent-tab",{ // 刷新表格到上一页
    	    			  page:{
    	    				 curr:brforeCurr-1
    	    			  }
    	    		  });
    	    	  }
    	      }
            });
            
            
          	//监听行工具事件
            table.on('tool(agent-tab)', function(obj){
              var data = obj.data;
              if(obj.event === 'yn'){
            	  var str = "启用";
            	  var status = 1;
            	  if(data.status == 1){
            		  str = "禁用";
            		  status = 2;
            	  }
                  layer.confirm('确认要['+str+']这个商户吗?', function(index){
                     $.ajax({
                    	 url:'<%=basePath%>agent/saveOrUpdate',
                    	 data: {"agenId":data.agenId,"status":status},
                    	 type: 'post',
                    	 dataType: 'json',
                    	 success:function(data){
                    		 tableReload();
                    		 if(data.se){
	                    		 layer.msg(data.msg, {time: 1000, icon:6});
                    		 }else{
	                    		 layer.msg(data.msg, {time: 1000, icon:5});
                    		 }
                    	 },error:function(data){
                    		 tableReload();
                    		 layer.msg("操作失败", {time: 1000, icon:5});
                    	 }
                     });
                    	layer.close(index);
                  });
              }else if(obj.event === 'del'){
                layer.confirm('真的删除这个商户吗?', function(index){
                	$.ajax({
                   	 url:'<%=basePath%>agent/deleteById',
                   	 data: {"agenId":data.agenId},
                   	 type: 'post',
                   	 dataType: 'json',
                   	 success:function(data){
                   		 tableReload();
                   		 if(data.se){
	                   		 layer.msg(data.msg, {time: 1000, icon:6});
                   		 }else{
	                   		 layer.msg(data.msg, {time: 1000, icon:5});
                   		 }
                   	 },error:function(data){
                   		 tableReload();
                   		 layer.msg(data.msg, {time: 1000, icon:5});
                   	 }
                    });
                  	layer.close(index);
                });
              } else if(obj.event === 'edit'){
            	  layer.open({
             		  type: 2,
             		  title: '代理商信息',
             		  shadeClose: true,
             		  shade: 0.8,
             		  area: ['680px', '500px'],
             		  content: '<%=basePath%>agent/editPage?agenId='+data.agenId
              	});  
              } else if(obj.event === 'share'){
            	  layer.open({
             		  type: 2,
             		  title: '查看分享信息',
             		  shadeClose: true,
             		  shade: 0.8,
             		  area: ['750px', '544px'],
             		  content: '<%=basePath%>agent/sharePage?agenId='+data.agenId
              	});  
              }
              
            });
        });
        //搜索操作
        function doSearch() {
            //1.获取到输入框中输入的内容
            var agenName = $('#agenName').val();
            var phone = $('#phone').val();
            var createdName = $('#createdName').val();
            //发送请求，并且接收数据
            layui.use('table', function () {
                var table = layui.table;
                table.reload('agent-tab', {
                    where: {"agenName": agenName,"phone": phone,"createdName": createdName},
                    page: {
                        curr: 1 //重新从第 1 页开始
                      }
                });
            });
        };
        function tableReload(){
       	   layui.use('table', function () {
              var table = layui.table;
              table.reload('agent-tab', {
              });
           });
        };
        function add(){
        	layui.use('layer', function(){
        		layer.open({
             		  type: 2,
             		  title: '添加商户管理',
             		  shadeClose: true,
             		  shade: 0.8,
             		  area: ['680px', '500px'],
             		  content: '<%=basePath%>agent/editPage'
              	});   
       		}); 
        };
        
        
        
    </script>
</html>

