<%@ page language="java" import="java.util.*" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>代理商管理</title>
	<%@ include file="../head.jsp" %>
</head>
<body>
 <form class="layui-form" style="margin-top: 20px;">
 	<input type="hidden" name="mId" value="${merch.mId}">
    <div class="layui-form-item">
      <label class="layui-form-label">商户名称</label>
      <div class="layui-input-inline">
        <input type="text" name="mName" value="${merch.mName}" lay-verify="required" autocomplete="off" class="layui-input">
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">手机号</label>
      <div class="layui-input-inline">
         <input type="tel" name="phone" value="${merch.phone}" lay-verify="required|phone" autocomplete="off" class="layui-input">
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">地址</label>
      <div class="layui-input-inline">
         <input type="text" name="address" value="${merch.address}" lay-verify="required" autocomplete="off" class="layui-input">
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">经度</label>
      <div class="layui-input-inline">
        <input type="text" name="lon" value="${merch.lon}" lay-verify="required|number" autocomplete="off" class="layui-input">
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">纬度</label>
      <div class="layui-input-inline">
        <input type="text" name="lat"  value="${merch.lat}" lay-verify="required|number" autocomplete="off" class="layui-input">
      </div>
    </div>
    <div class="layui-form-item">
	    <div class="layui-input-block">
	      <button type="submit" class="layui-btn" lay-submit="" lay-filter="btnSub">立即提交</button>
	    </div>
  </div>
 </form>
</body>
 
   

<script type="text/javascript">
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
          //监听提交
            form.on('submit(btnSub)', function(data){
           		var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            	$.ajax({
                  	 url:'<%=basePath%>merchant/update',
                  	 data: data.field,
                  	 type: 'post',
                  	 dataType: 'json',
                  	 success:function(data){
                  		layer.msg('操作成功', { offset: '15px',icon: 6,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                  		});
                  	 },error:function(data){
                  		layer.msg('操作成功', { offset: '15px',icon: 5,time: 1000}, function(){
	                  		parent.layer.close(index);
			            	parent.location.reload();
                  		});
                  	 }
                 });
            	 
                 return false;
            });
        });
        
        
    </script>
</html>

