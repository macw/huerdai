
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>layuiAdmin 角色管理 iframe 框</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="../../../layuiadmin/layui/css/layui.css" media="all">
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.12.4.js"></script>
  <script src="../../../layuiadmin/layui/layui.js"></script>
  <script type="text/javascript">
    $(function(){
      $.ajax({
        url:"${pageContext.request.contextPath}/menu/selectList",
        success:function(data){
          layui.use('tree', function(){
            var tree = layui.tree;
            //渲染
            var inst1 = tree.render({
              elem: '#roleTree',  //绑定元素
              data:data,
              showCheckbox:true,
              oncheck:function (obj) {
                console.log(obj.data); //得到当前点击的节点数据
                console.log(obj.checked); //得到当前节点的展开状态：open、close、normal
                console.log(obj.elem); //得到当前节点元素
              }
            });
          });
        }
      })

    })


  </script>

</head>
<body>

  <div class="layui-form" lay-filter="layuiadmin-form-role" id="layuiadmin-form-role" style="padding: 20px 30px 0 0;">
    <div class="layui-form-item">
      <label class="layui-form-label">角色</label>
      <div class="layui-input-block">
        <select name="rolename">
          <option value="0">管理员</option>
          <option value="1">超级管理员</option>
          <option value="2">纠错员</option>
          <option value="3">采购员</option>
          <option value="4">推销员</option>
          <option value="5">运营人员</option>
          <option value="6">编辑</option>
        </select>
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">权限范围</label>
     <%-- <div class="layui-input-block" id="authorName">
        <input type="checkbox" name="limits[]" lay-skin="primary" title="内容系统">
        <input type="checkbox" name="limits[]" lay-skin="primary" title="社区系统">
        <input type="checkbox" name="limits[]" lay-skin="primary" title="用户">
        <input type="checkbox" name="limits[]" lay-skin="primary" title="角色">
        <input type="checkbox" name="limits[]" lay-skin="primary" title="评论审核">
        <input type="checkbox" name="limits[]" lay-skin="primary" title="发货">
      </div>--%>

      <div class="layui-inline" id="roleTree"></div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">具体描述</label>
      <div class="layui-input-block">
        <textarea type="text" name="descr" lay-verify="required" autocomplete="off" class="layui-textarea"></textarea>
      </div>
    </div>
    <div class="layui-form-item layui-hide">
      <button class="layui-btn" lay-submit lay-filter="LAY-user-role-submit" id="LAY-user-role-submit">提交</button>
    </div>
  </div>

  <script src="../../../layuiadmin/layui/layui.js"></script>  
  <script>
  layui.config({
    base: '../../../layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index', 'form'], function(){
    var $ = layui.$
    ,form = layui.form ;
  })
  </script>
</body>
</html>