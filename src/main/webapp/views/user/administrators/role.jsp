<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layuiAdmin 角色管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="../../../layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="../../../layuiadmin/style/admin.css" media="all">
    <script src="../../../layuiadmin/layui/layui.js"></script>

    <c:set var="basepath" value="${pageContext.request.contextPath}/"/>
    <script type="text/javascript" src="${basepath}js/jquery-1.12.0.min.js"></script>
    <style type="text/css">
        .layui-table-cell {
            height: auto;
            line-height: 30px;
        }

    </style>
    <script type="text/javascript">
        layui.config({
            base: '../../../layuiadmin/' //静态资源所在路径
        }).extend({
            index: 'lib/index' //主入口模块
        }).use(['index', 'useradmin', 'table', 'form'], function () {

            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;

            table.render({
                elem: '#LAY-user-back-role',
                url: '${pageContext.request.contextPath}/role/selectAll', //数据接口
                page: true,
                limit: 10,
                limits: [10, 20, 30],
                //toolbar: "#LAY_lay_add",
                cols: [[
                    {type: "checkbox"},
                    {title: "角色名", field: "roleName"},
                    {title: "描述", field: "remark"},
                    {title: "操作", templet: "#updateAndDelete"}
                ]]

            });
        });

        //更新，编辑权限
        function toOpenUpdateLayer(roleName) {
            //  console.log(layuiadmin-form-role);
            //1.获取当前行数据===》发送ajax请求，获取当前行数据
            //2.把数据填充到修改弹出层中==>弹出层显示
            //3.提交表单
            //4.接收后台修改响应回来的数据；关闭弹出层、提示修改信息、刷新table
            //1.
            $("#roleName").val(roleName);

            $.ajax({
                url: "${pageContext.request.contextPath}/menu/selectTreeByRoleName?roleName=" + roleName,
                success: function (data) {
                    layui.use('tree', function () {
                        var tree = layui.tree;
                        //渲染
                        var inst1 = tree.render({
                            elem: '#roleTree',  //绑定元素
                            data: data.treeNodeList,
                            showCheckbox: true,
                            oncheck: function (obj) {
                                console.log(obj.data); //得到当前点击的节点数据
                                console.log(obj.checked); //得到当前节点的展开状态：open、close、normal
                                console.log(obj.elem); //得到当前节点元素
                            }
                        });

                        var inst2 = tree.render({
                            elem: '#roleTree',  //绑定元素
                            data: data.treeNodeList,
                            showCheckbox: true,
                            oncheck: function (obj) {
                                console.log(obj.data); //得到当前点击的节点数据
                               // console.log(obj.checked); //得到当前节点的展开状态：open、close、normal
                                console.log(obj.elem); //得到当前节点元素
                            }
                        });


                    });
                }
            });


            $("#updateSubmitBtn").show();
            //2.
            layui.use('layer', function () {
                layer.open({
                    title: "编辑权限",
                    content: $("#layuiadmin-form-role"),
                    type: 1,
                    maxmin: true,
                    area: ['500px', '480px']
                })
            });
        }

        //搜索操作
        function doSearch() {
            //1.获取到输入框中输入的内容
            var searchRoleName = $('#Lay_toSearch_input').val();
            //发送请求，并且接收数据
            layui.use('table', function () {
                var table = layui.table;

                table.reload('LAY-user-back-role', {
                    where: {"roleName": searchRoleName}
                });
            });
        }

        //添加弹出层
        function toOpenAddLayer() {
            var roleName = $('#roleName').val('');
            console.log(roleName);
            $.ajax({
                url: "${pageContext.request.contextPath}/menu/selectTreeByRoleName?roleName=" + '',
                success: function (data) {
                    layui.use('tree', function () {
                        var tree = layui.tree;
                        //渲染
                        var inst1 = tree.render({
                            elem: '#roleTree',  //绑定元素
                            data: data.treeNodeList,
                            showCheckbox: true,
                            id: 'treeRoleid', //定义索引
                            oncheck: function (obj) {
                                console.log(obj.data); //得到当前点击的节点数据
                                //console.log(obj.checked); //得到当前节点的展开状态：open、close、normal
                               // console.log(obj.elem); //得到当前节点元素
                            }
                        });
                    })
                }
            });
            function writeObj(obj){
                var description = "";
                for(var i in obj){
                    var property=obj[i];
                    description+=i+" = "+property+"\n";
                }
                alert(description);
            }


            //点击提交按钮执行添加操作
            layui.use(["form","tree","layer","table"],function () {
                var form = layui.form;
                var layer = layui.layer;
                var table = layui.table;
                var tree  = layui.tree;

                layer.open({
                    title: "添加管理员",
                    content: $("#layuiadmin-form-role"),
                    type: 1,
                    maxmin: true,
                    area: ['500px', '480px']
                });

                //当点击提交按钮的时候，会进入到这个函数
                //当所有的验证都通过时，才会进入到这个函数
                form.on("submit(LAY-user-role-submit)",function(data){
                    console.log(data);

                    //获得选中的树节点
                    var checkData = tree.getChecked('treeRoleid');
                    writeObj(checkData[0]);
                    console.log("0000000000"+checkData[0]);
                    console.log(data.field);
                    var checkData = JSON.stringify(checkData);
                    console.log(checkData.id);

                    $.ajax({
                        url:"${pageContext.request.contextPath}/role/addRole?checkData="+checkData,
                        // data:data.field,
                        type:"post",
                        // data:{"role": data.field,"tree":checkData},
                        // data: {checkData:checkData},
                         contentType : 'application/json;charset=UTF-8',
                        success:function(data){
                            //console.log(data)
                            //1.关闭掉添加弹出层
                            layer.closeAll('page');
                            //2.提示添加成功
                            /* if(data.isLAY-user-role-submit){
                                 layer.alert("添加成功",{time:3000});
                             }else{
                                 layer.alert("添加失败",{time:3000});
                             }*/

                            //3.刷新table
                            table.reload("LAY-user-back-role");
                        }
                    });
                    return false;//阻止跳转；
                })
            })

        }




    </script>
    <script type="text/html" id="updateAndDelete">
        <button type="button" class="layui-btn  layui-btn-normal" onclick="toOpenUpdateLayer('{{d.roleName}}')">修改
        </button>
        <button type="button" class="layui-btn layui-btn-danger" onclick="doDelete('{{d.id}}')">删除</button>
    </script>

</head>
<body>


<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-form layui-card-header layuiadmin-card-header-auto">
            <div class="layui-form-item">
                <div class="layui-inline">
                    角色管理
                </div>

            </div>
        </div>
    </div>
    <div class="layui-card-body">
        <div style="padding-bottom: 10px;" id="LAY_lay_add">
            <button class="layui-btn layuiadmin-btn-role" data-type="batchdel">批量删除</button>
            <button class="layui-btn layuiadmin-btn-role" data-type="add" onclick="toOpenAddLayer()">添加</button>
            &nbsp;
            <div class="layui-input-inline" style="width: 200px;">
                <input type="text" name="search" id="Lay_toSearch_input" placeholder="请输入角色名" autocomplete="off"
                       class="layui-input">
            </div>
            <div class="layui-input-inline" style="width: 100px;">
                <button type="button" class="layui-btn layui-btn-normal" onclick="doSearch()">
                    <i class="layui-icon layui-icon-search"></i> 搜索
                </button>
            </div>
        </div>


        <table id="LAY-user-back-role" lay-filter="LAY-user-back-role"></table>


    </div>
</div>
</div>

<%--弹出层--%>
<form id="addForm" class="layui-form">
    <div class="layui-form" lay-filter="layuiadmin-form-role" id="layuiadmin-form-role"
         style="padding: 20px 30px 0 0;display: none">
        <div class="layui-form-item">
            <label class="layui-form-label">角色</label>
            <div class="layui-input-block">
               <%-- <input type="hidden" name="roleId" value="roleId">--%>
                <input name="roleName" id="roleName" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">权限范围</label>
            <%--树形菜单--%>
            <div class="layui-inline" id="roleTree" name="roleTree"></div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">具体描述</label>
            <div class="layui-input-block">
            <textarea type="text" name="remark" lay-verify="required" autocomplete="off"
                      class="layui-textarea"></textarea>
            </div>
        </div>
        <div class="layui-form-item layui-col-md9"  style="text-align: right">
            <button class="layui-btn " lay-submit lay-filter="LAY-user-role-submit" id="LAY-user-role-submit">提交</button>

        </div>
    </div>
</form>

</body>
</html>

