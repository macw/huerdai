<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="./meta.jsp" %>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Simple SSM </title>
</head>
<body>
	<div class="app">
		<div class="back">
			<a href="${basepath}user"><-</a>
		</div>
		<div class="data">
			<form action="${basepath}user/adduserData" method="post">
			<table>
				<tbody>
					<tr>
						<td colspan="2">姓名</td>
						<td colspan="2"><input  name="name"/></td>
					</tr>
					<tr>
						<td colspan="2">年龄</td>
						<td colspan="2"><input name="age"/></td>
					</tr>
					<tr>
						<td colspan="2">Sex</td>
						<td colspan="2">
							男：<input name="sex" value="1" checked="checked" type="radio" value="男"/>
							女：<input name="sex" value="2" type="radio" value="女"/>
						</td>
					</tr>
					<tr>
						<td colspan="2">身份证</td>
						<td colspan="2"><input name="idcard"/></td>
					</tr>
				</tbody>
					<tfoot>
						<tr>
							<td colspan="5">
								<input  type="submit" value="Save"/>
								<span class="tip">${err.msg}</span>							
							</td>
						</tr>
					</tfoot>
			</table>
			</form>
		</div>
	 </div>
</body>
</html>