<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="./meta.jsp" %>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${basepath}statics/laypage/laypage.js"></script>
<title></title>
</head>
<body>
	<div class="app">
		<div class="search">
			<form action="${basepath}test">
				<input name="name" value="${searchVo.name}"> 
				<input type="submit" value="Search"/>
				<a href="${basepath}adduesrData">
					<input name="add"  type="button" value="Add"/>
				</a>
			</form>
		</div>
		<div class="data">
			<table>
				<thead>
					<tr>
						<td>编号</td>
						<td>名字</td>
						<td>年龄</td>
						<td>性别</td>
						<td>注册时间</td>
						<td>身份证</td>
						<td>Edit</td>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${fn:length(vo.list) > 0  }">
							<c:forEach items="${vo.list}" var="bean">
								<tr>
									<td>${bean.id }</td>
									<td>${bean.name }</td>
									<td>${bean.age }</td>
									<td>
									<c:if test="${bean.sex == 1 }">男 </c:if>
									<c:if test="${bean.sex != 1 }">女 </c:if>
									
									<td><fmt:formatDate value="${bean.starttime }" pattern="yyyy-MM-dd HH:mm:ss" /></td>
									<td>${bean.idcard }</td>
									<td><a href="${basepath}user/updateuserDataPre?id=${bean.id}">update</a>&nbsp;<a href="${basepath}user/deleteuserData?id=${bean.id}">Delete	</a></td>
								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<td colspan="5">
									没有相关数据...
								</td>
							</tr>
						</c:otherwise>
					</c:choose>
					
					
				</tbody>
				<c:if test="${vo.totalPage > 1 }">
					<tfoot>
						<tr>
							<td colspan="5">
							
										<div id="page_div"></div>
								
							</td>
						</tr>
					</tfoot>
				</c:if>
			</table>
		</div>
	 </div>
</body>
<script>
<c:if test="${vo.totalPage > 1}">
laypage({
    cont: $('#page_div'), //容器。值支持id名、原生dom对象，jquery对象,
    pages: ${vo.totalPage}, //总页数
    skip: false, //是否开启跳页
    skin: '#8a8a8a',
    curr: ${vo.pageNo},
    groups: 3, //连续显示分页数
    jump: function(e, first){
    	 if(!first){ //一定要加此判断，否则初始时会无限刷新
	    	var page = e.curr;
	    	window.location.href= '${basepath}user?pageNum='+page;
         }
    }
});
</c:if>
</script>
</html>