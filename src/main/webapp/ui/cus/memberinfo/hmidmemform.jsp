<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ include file="../../meta.jsp" %>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>添加独立会员</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="${basepath}statics/layuiadmin/layui/css/layui.css" media="all">
  <script type="text/javascript"  src="${basepath}statics/cookies.js"></script>
</head>
<body>

  <div class="layui-form" lay-filter="layuiadmin-form-useradmin" id="layuiadmin-form-useradmin" style="padding: 100px 80px;">
    <div class="layui-form-item">
      <label class="layui-form-label">独立会员名称</label>
      <div class="layui-input-inline">
        <input type="text" name="imName" lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input">
      </div>
    </div>
  
       <div class="layui-form-item">
      <label class="layui-form-label">成为方式</label>
      <div class="layui-input-inline">
        	<select name="imType" lay-filter="LAY-user-adminrole-type">
              <option value="1">购买	</option>
              <option value="2">积分申请</option>
               <option value="3">到店成为</option>
            </select>
      </div>
    </div>
    
     <div class="layui-form-item">
      <label class="layui-form-label">收入</label>
      <div class="layui-input-inline">
        <input type="text" name="incomeMoney" lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input">
      </div>
    </div>
    
      <div class="layui-form-item">
      <label class="layui-form-label">有效性</label>
      <div class="layui-input-inline">
        	<select name="isEffect" lay-filter="LAY-user-adminrole-type">
              <option value="1">有效</option>
              <option value="2">无效</option>
            </select>
      </div>
    </div>
    
    <div class="layui-form-item">
      <label class="layui-form-label">删除状态</label>
      <div class="layui-input-inline">
        	<select name="isDelete" lay-filter="LAY-user-adminrole-type">
              <option value="1">未删除</option>
              <option value="2">已删除</option>
            </select>
      </div>
    </div>
    
     <div class="layui-form-item">
      <label class="layui-form-label">活跃量</label>
      <div class="layui-input-inline">
        <input type="text" name="activeQuantity" lay-verify="required" placeholder="请输入活跃量" autocomplete="off" class="layui-input">
      </div>
    </div>
    
    <div class="layui-form-item">
      <label class="layui-form-label">会员余额</label>
      <div class="layui-input-inline">
        <input type="text" name="money" lay-verify="required" placeholder="请输入会员余额" autocomplete="off" class="layui-input">
      </div>
    </div>
    
    <div class="layui-form-item">
      <label class="layui-form-label">返现金额</label>
      <div class="layui-input-inline">
        <input type="text" name="referralMoney" lay-verify="required" placeholder="请输入返现金额" autocomplete="off" class="layui-input">
      </div>
    </div>
    
     <div class="layui-form-item">
      <label class="layui-form-label">点评总数</label>
      <div class="layui-input-inline">
        <input type="text" name="dpCount" lay-verify="required" placeholder="请输入点评总数" autocomplete="off" class="layui-input">
      </div>
    </div>
    
    <div class="layui-form-item">
      <label class="layui-form-label">商城分享总数</label>
      <div class="layui-input-inline">
        <input type="text" name="insiteCount" lay-verify="required" placeholder="请输入商城分享总数" autocomplete="off" class="layui-input">
      </div>
    </div>
    
     <div class="layui-form-item">
      <label class="layui-form-label">达人级别</label>
      <div class="layui-input-inline">
        	 <select id="ssDarenId" name="ssDarenId" lay-filter="LAY-user-adminrole-type">
             <c:forEach items="${hds}" var="h">
              <option value="${h.id},${h.darenTitle}">${h.darenTitle}</option>
            </c:forEach>
            </select>
      </div>
    </div>
    
    <div class="layui-form-item layui-hide">
      <input type="button" lay-submit lay-filter="LAY-user-front-submit" id="LAY-user-front-submit" value="确认">
    </div>
  </div>

  <script type="text/javascript"  src="${basepath}statics/layuiadmin/layui/layui.js"></script>  
  <script>
  layui.config({
	  base: '${pageContext.request.contextPath}/statics/layuiadmin/' //静态资源所在路径,
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index', 'form', 'upload'], function(){
    var $ = layui.$
    ,form = layui.form
    ,upload = layui.upload ;
    
   /*  upload.render({
      elem: '#layuiadmin-upload-useradmin'
      ,url: layui.setter.base + 'json/upload/demo.js'
      ,accept: 'images'
      ,method: 'get'
      ,acceptMime: 'image/*'
      ,done: function(res){
        $(this.item).prev("div").children("input").val(res.data.src)
      }
    }); */
  })
  </script>
</body>
</html>