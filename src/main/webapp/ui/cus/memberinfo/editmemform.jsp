<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ include file="../../meta.jsp" %>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>添加用户</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="${basepath}statics/layuiadmin/layui/css/layui.css" media="all">
  <script type="text/javascript"  src="${basepath}statics/cookies.js"></script>
</head>
<body>

  <div class="layui-form" lay-filter="layuiadmin-form-useradmin" id="layuiadmin-form-useradmin" style="padding: 100px 80px;">
    	 <input id="mId" name="mId" value = "${hms.mId}" hidden="hidden">
    <div class="layui-form-item">
      <label class="layui-form-label">会员姓名</label>
      <div class="layui-input-inline">
        <input type="text" name="mName" value="${hms.mName}" lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input">
      </div>
    </div>
  
       <div class="layui-form-item">
      <label class="layui-form-label">会员分类</label>
      <div class="layui-input-block">
       	 <input id="smtypeId" name="smtypeId" value = "${hms.mtypeId}" hidden="hidden">
       	 
             <select id="mtypeId" name="mtypeId" lay-filter="LAY-user-adminrole-type">
             <c:forEach items="${hmts}" var="h">
              <option value="${h.mtId}">${h.mtName}</option>
            </c:forEach>
            </select>
      </div>`
    </div>
    
    
     <div class="layui-form-item">
      <label class="layui-form-label">会员状态</label>
      <div class="layui-input-inline">
     		 <input id="scardStatus" name="smcardStatus" value = "${hos.cardStatus}" hidden="hidden">
      
        	<select name="cardStatus" lay-filter="LAY-user-adminrole-type">
              <option value="1">正常</option>
            <option value="2">禁言</option>
            </select>
      </div>
    </div>
    
    
    <div class="layui-form-item">
      <label class="layui-form-label">会员等级</label>
      <div class="layui-input-inline">
        <input type="text" name="careGrade" value="${hms.careGrade}" lay-verify="required"  autocomplete="off" class="layui-input">
      </div>
    </div>
    
    
    <div class="layui-form-item">
      <label class="layui-form-label">出生日期</label>
      <div class="layui-input-inline">
        		<input type="text" name="birthdate" value='<fmt:formatDate  value='${hms.birthdate}' type='both' pattern='yyyy-MM-dd' /> ' class="layui-input" id="membirthdate" placeholder="yyyy-MM-dd">
        		
      </div>
    </div>
    
    <div class="layui-form-item">
      <label class="layui-form-label">证件类型</label>
      <div class="layui-input-inline">
      		 <input id="sidcardtype" name="sidcardtype" value = "${hos.idcardtype}" hidden="hidden">
        	<select name="idcardtype" lay-filter="LAY-user-adminrole-type">
              <option value="1">身份证</option>
            <option value="2">护照</option>
            </select>
      </div>
    </div>
    
      
    <div class="layui-form-item">
      <label class="layui-form-label">证件号码</label>
      <div class="layui-input-inline"> 
        <input type="text" name="idcarenumber" value="${hms.idcarenumber}" lay-verify="required" placeholder="请输入证件号码" autocomplete="off" class="layui-input">
      </div>
    </div>
    
    <div class="layui-form-item">
      <label class="layui-form-label">手机号码</label>
      <div class="layui-input-inline">
        <input type="text" name="telephone" value="${hms.telephone}" lay-verify="required" placeholder="请输入手机号码" autocomplete="off" class="layui-input">
      </div>
    </div>
    
    <div class="layui-form-item">
      <label class="layui-form-label">紧急联系人</label>
      <div class="layui-input-inline">
        <input type="text" name="emergencyphone" value="${hms.emergencyphone}" lay-verify="required" placeholder="请输入紧急联系人号码" autocomplete="off" class="layui-input">
      </div>
    </div>
    
    <div class="layui-form-item">
      <label class="layui-form-label">电子邮箱</label>
      <div class="layui-input-inline">
        <input type="text" name="email" value="${hms.email}" lay-verify="required" placeholder="请输入电子邮箱" autocomplete="off" class="layui-input">
      </div>
    </div>
    
     <div class="layui-form-item">
      <label class="layui-form-label">联系地址</label>
      <div class="layui-input-inline">
        <input type="text" name="address" value="${hms.address}" lay-verify="required" placeholder="请输入联系地址" autocomplete="off" class="layui-input">
      </div>
    </div>
    
    <div class="layui-form-item">
      <label class="layui-form-label">会员折扣</label>
      <div class="layui-input-inline">
        <input type="text" name="discount" value="${hms.discount}" lay-verify="required" placeholder="请输入会员折扣" autocomplete="off" class="layui-input">
      </div>
    </div>
    
    <div class="layui-form-item">
      <label class="layui-form-label">累计积分</label>
      <div class="layui-input-inline">
        <input type="text" name="totalpoints" value="${hms.totalpoints}" lay-verify="required" placeholder="请输入累计积分" autocomplete="off" class="layui-input">
      </div>
    </div>
    
      <div class="layui-form-item">
      <label class="layui-form-label">累计消费</label>
      <div class="layui-input-inline">
        <input type="text" name="totalconsumption" value="${hms.totalconsumption}" lay-verify="required" placeholder="请输入累计消费" autocomplete="off" class="layui-input">
      </div>
    </div>
    
     <div class="layui-form-item">
      <label class="layui-form-label">个人照片</label>
      <div class="layui-input-inline">
        <input type="text" name="portraint"  value="${hms.portraint}" lay-verify="required" placeholder="请输入个人照片" autocomplete="off" class="layui-input">
      </div>
    </div>
    
    <div class="layui-form-item">
      <label class="layui-form-label">个人资料</label>
      <div class="layui-input-inline">
        <input type="text" name="attachGuid" value="${hms.attachGuid}"  lay-verify="required" placeholder="请输入个人资料" autocomplete="off" class="layui-input">
      </div>
    </div>
    
    <div class="layui-form-item">
      <label class="layui-form-label">个人备注</label>
      <div class="layui-input-inline">
        <input type="text" name="note" value="${hms.note}"  lay-verify="required" placeholder="请输入备注" autocomplete="off" class="layui-input">
      </div>
    </div>  
            
     <div class="layui-form-item">
      <label class="layui-form-label">所在省份</label>
      <div class="layui-input-inline">
        <input type="text" name="province" value="${hms.province}"  lay-verify="required" placeholder="请输入省份" autocomplete="off" class="layui-input">
      </div>
    </div> 
    
     <div class="layui-form-item">
      <label class="layui-form-label">城市</label>
      <div class="layui-input-inline">
        <input type="text" name="city" value="${hms.city}" lay-verify="required" placeholder="请输入城市" autocomplete="off" class="layui-input">
      </div>
    </div> 
    
     <div class="layui-form-item">
      <label class="layui-form-label">行政区</label>
      <div class="layui-input-inline">
        <input type="text" name="district" value="${hms.district}" lay-verify="required" placeholder="请输入行政区" autocomplete="off" class="layui-input">
      </div>
    </div> 
    
    
    <div class="layui-form-item layui-hide">
      <input type="button" lay-submit lay-filter="LAY-user-front-submit" id="LAY-user-front-submit" value="确认">
    </div>
  </div>

  <script type="text/javascript"  src="${basepath}statics/layuiadmin/layui/layui.js"></script>  
  <script>
  window.onload=function(){
	 	 var smtypeId = $('#smtypeId').val();	
	 	 $("#mtypeId").val(smtypeId);
	 	 
	 	 var scardStatus = $('#scardStatus').val();	
	 	 $("#cardStatus").val(scardStatus);
	 	 
	 	 var sidcardtype = $('#sidcardtype').val();	
	 	 $("#idcardtype").val(sidcardtype);
	 	
}
  
  layui.config({
	  base: '${pageContext.request.contextPath}/statics/layuiadmin/' //静态资源所在路径,
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index', 'form', 'upload','laydate'], function(){
    var $ = layui.$
    ,form = layui.form
    ,upload = layui.upload ;
    var laydate = layui.laydate;
   
    
  //常规用法
    laydate.render({
      elem: '#membirthdate'
    });
    
   /*  upload.render({
      elem: '#layuiadmin-upload-useradmin'
      ,url: layui.setter.base + 'json/upload/demo.js'
      ,accept: 'images'
      ,method: 'get'
      ,acceptMime: 'image/*'
      ,done: function(res){
        $(this.item).prev("div").children("input").val(res.data.src)
      }
    }); */
  })
  </script>
</body>
</html>