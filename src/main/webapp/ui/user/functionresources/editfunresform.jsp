<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ include file="../../meta.jsp" %>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>添加功能资源</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
   <link rel="stylesheet" href="${basepath}statics/layuiadmin/layui/css/layui.css" media="all">
   <script type="text/javascript"  src="${basepath}statics/cookies.js"></script>
</head>
<body>

   <div class="layui-form" lay-filter="layuiadmin-form-admin" id="layuiadmin-form-admin" style="padding: 20px 30px 0 0;">
   
	 <div class="layui-form-item">
	      <label class="layui-form-label">功能名称</label>
	      <div class="layui-input-inline">
	        <input type="text" name="fname" value="${hfs.fname}" lay-verify="required" placeholder="请输入功能名称" autocomplete="off" class="layui-input">
	      </div>
	    </div>
	    <div class="layui-form-item">
	      <label class="layui-form-label">功能路径</label>
	      <div class="layui-input-inline">
	        <input type="text" name="funurl"  value="${hfs.furl}" placeholder="请输入功能路径" autocomplete="off" class="layui-input">
	      </div>
	    </div>
	 
   
    <div class="layui-form-item">
      <label class="layui-form-label">上级目录</label>
      <div class="layui-input-block">
       
        <select id="funforaddfun" name="funforaddfun" lay-filter="LAY-user-adminrole-type">
             <option value="0">默认目录</option>
            <c:forEach items="${hmfun}" var="h">
              <option value="${h.fid}">${h.fname}</option>
            </c:forEach>
            </select>
      </div>`
    </div>
    	<input id="hmfunresid" name="hmfunresid" value = "${hfs.fid}" hidden="hidden">
    	<input id="hmfparentid" name="fparentid" value = "${hfs.fparentid}" hidden="hidden">
    	
      <div class="layui-form-item">
	      <label class="layui-form-label">功能标识</label>
	      <div class="layui-input-inline">
	        <input type="text" name="funidentification" value="${hfs.identification}" placeholder="请输入功能标识" autocomplete="off" class="layui-input">
	      </div>
	    </div>
   
    <div class="layui-form-item layui-hide">
      <input type="button" lay-submit lay-filter="LAY-user-front-submit" id="LAY-user-front-submit" value="确认">
    </div>
    
  </div>

  <script type="text/javascript"  src="${basepath}statics/layuiadmin/layui/layui.js"></script>
  <script>
  window.onload=function(){
	 	 var hmfparentid = $('#hmfparentid').val();	
	 	 $("#funforaddfun").val(hmfparentid);
	 	
}
  layui.config({
	  base: '${pageContext.request.contextPath}/statics/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index', 'form'], function(){
    var $ = layui.$
    ,form = layui.form ;
  })
  </script>
</body>
</html>