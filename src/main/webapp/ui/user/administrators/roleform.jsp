<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ include file="../../meta.jsp" %>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>角色功能权限</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="${basepath}statics/layuiadmin/layui/css/layui.css" media="all">
  
   <script type="text/javascript"  src="${basepath}statics/cookies.js"></script>
</head>
<body>

  <div class="layui-form" lay-filter="layuiadmin-form-role" id="layuiadmin-form-role" style="padding: 20px 30px 0 0;">
    <div class="layui-form-item">
      <label class="layui-form-label">角色</label>
      <div class="layui-input-block">
       
         <select name="rolename" lay-filter="LAY-user-adminrole-type">
            <c:forEach items="${hmstf}" var="hf">
            
            <c:choose>
			   <c:when test="${Hmroles.roleid== hf.roleid }">  
			  		 <option   selected = "selected"  value="${hf.roleid}">${hf.rolename}</option>
			   </c:when>
			   <c:otherwise> 
			 		 <option    value="${hf.roleid}">${hf.rolename}</option> 
			   </c:otherwise>
			</c:choose>
            
            </c:forEach>
        </select>
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">权限范围</label>
      <div class="layui-input-block" style="margin-bottom: -350px;">
      
      	<input id="hmUserunction" value = "${hmUserfunction.applyLevel}" hidden="hidden">
      	<input id="hmconid" name="hmconid" value = "${hmcon.cid}" hidden="hidden">
      	
		<input id="hcid" name ="hcid" value = "${hmUserfunction.id}" hidden="hidden">	<!-- 用户功能 -->
		<input id="hmUserAuthRelationid" name ="hmUserAuthRelationid" value = "${hmUserAuthRelationid}" hidden="hidden">	<!-- 用户角色功能 -->
					   
				<input type="checkbox" id="ddxrfadd" 	name="ddxadd" 	value="1"	 lay-skin="primary" title="添加">
				<input type="checkbox" id="ddxrfdel"    name="ddxdel"	value="1"	 lay-skin="primary" title="删除">
				<input type="checkbox" id="ddxrfupd"	name="ddxuda" 	value="1"	 lay-skin="primary" title="编辑">
				<input type="checkbox" id="ddxrfsel"	name="ddxsel"	value="1"	 lay-skin="primary" title="查询">
							        
					 
		
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">具体描述</label>
      <div class="layui-input-block">
        <textarea type="text" name="descr" value="" lay-verify="required" autocomplete="off" class="layui-textarea">${hmUserfunction.remark}</textarea>
      </div>
    </div>
    <div class="layui-form-item layui-hide">
      <button class="layui-btn" lay-submit lay-filter="LAY-user-role-submit" id="LAY-user-role-submit">提交</button>
    </div>
  </div>

    <script type="text/javascript"  src="${basepath}statics/layuiadmin/layui/layui.js"></script>
  <script>
  window.onload=function(){
	  var hmUserunction = $('#hmUserunction').val();	
	  
	  var resultddx = hmUserunction.split(',');
	  
	  

	  for(var i=0;i<resultddx.length;i++){
		  if(resultddx[i] ==1 ){
			  $("#ddxrfadd").prop("checked", true);
		  }
		  if(resultddx[i] ==2 ){
			  $("#ddxrfdel").prop("checked", true);
		  }
		  if(resultddx[i] ==3 ){
			  $("#ddxrfupd").prop("checked", true);
		  }
		  if(resultddx[i] ==4 ){
			  $("#ddxrfsel").prop("checked", true);
		  }
	  }
	  
	}
  layui.config({
	  base: '${pageContext.request.contextPath}/statics/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index', 'form'], function(){
    var $ = layui.$
    ,form = layui.form ;
  })
  </script>
</body>
</html>