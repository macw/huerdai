<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ include file="../../meta.jsp" %>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>添加后台管理员</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
   <link rel="stylesheet" href="${basepath}statics/layuiadmin/layui/css/layui.css" media="all">
</head>
<body>

   <div class="layui-form" lay-filter="layuiadmin-form-admin" id="layuiadmin-form-admin" style="padding: 20px 30px 0 0;">
   
   
	 <div class="layui-form-item">
	      <label class="layui-form-label">登录名</label>
	      <div class="layui-input-inline">
	        <input type="text" name="loginname" lay-verify="required" placeholder="请输入用户名" autocomplete="off" class="layui-input">
	      </div>
	    </div>
	    <div class="layui-form-item">
	      <label class="layui-form-label">密码</label>
	      <div class="layui-input-inline">
	        <input type="text" name="pwd" lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">
	      </div>
	    </div>
	   
   
   
    <div class="layui-form-item">
      <label class="layui-form-label">角色</label>
      <div class="layui-input-block">
       
        <select name="roleforaddcon" lay-filter="LAY-user-adminrole-type">
             
            <c:forEach items="${hms}" var="h">
              <option value="${h.roleid}">${h.rolename}</option>
            </c:forEach>
            </select>
      </div>`
    </div>
   
    <div class="layui-form-item layui-hide">
       <input type="button" lay-submit lay-filter="LAY-user-back-submit" id="LAY-user-back-submit" value="确认">
    </div>
    
  </div>

  <script type="text/javascript"  src="${basepath}statics/layuiadmin/layui/layui.js"></script>
  <script>
  layui.config({
	  base: '${pageContext.request.contextPath}/statics/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
  }).use(['index', 'form'], function(){
    var $ = layui.$
    ,form = layui.form ;
  })
  </script>
</body>
</html>