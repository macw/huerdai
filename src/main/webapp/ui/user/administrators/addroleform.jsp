<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ include file="../../meta.jsp" %>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>添加用户角色功能</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
 <link rel="stylesheet" href="${basepath}statics/layuiadmin/layui/css/layui.css" media="all">
 
    <link rel="stylesheet" href="${basepath}statics/eleTree/eleTree.css" media="all">
  
</head>
<body>

  <div class="layui-form" lay-filter="layuiadmin-form-role" id="layuiadmin-form-role" style="padding: 20px 30px 0 0;">
    <div class="layui-form-item">
      <label class="layui-form-label">角色</label>
      <div class="layui-input-block">
      	<input id="selroleddxtext" name="selroleddxtext"  hidden="hidden">
        <select  lay-filter="selroleddx" id="selroleddx" name="rolename"  >
          		  <c:forEach items="${addhmstf}" var="hfa">
          		  		 <option   value="${hfa.roleid}">${hfa.rolename}</option> 
          		   </c:forEach>
        </select>
      </div>
    </div>
  <!--   <ul id="demo"></ul> -->
   
	    <div class="layui-form-item">
	        <label class="layui-form-label">功能选择</label>
	        <div class="layui-input-block">
	        	<input id="hmfunid" name="hmfunid"  hidden="hidden">
	            <input type="text" name="title" required="" lay-verify="required" placeholder="请输入标题" readonly="" autocomplete="off" class="layui-input">
	            <div class="eleTree ele5" lay-filter="data5"></div>
	        </div>
	    </div>
	
    
    <div class="layui-form-item" >
      <label class="layui-form-label">权限范围</label>
      <div class="layui-input-block" style="margin-bottom: -350px;">
      
 			   	<input type="checkbox" id="ddxrfadd" 	name="ddxadd" 	value="1"	 lay-skin="primary" title="添加">
				<input type="checkbox" id="ddxrfdel"    name="ddxdel"	value="1"	 lay-skin="primary" title="删除">
				<input type="checkbox" id="ddxrfupd"	name="ddxuda" 	value="1"	 lay-skin="primary" title="编辑">
				<input type="checkbox" id="ddxrfsel"	name="ddxsel"	value="1"	 lay-skin="primary" title="查询">
      </div>
    </div>
    <div class="layui-form-item">
      <label class="layui-form-label">具体描述</label>
      <div class="layui-input-block">
        <textarea type="text" name="descr" lay-verify="required" autocomplete="off" class="layui-textarea"></textarea>
      </div>
    </div>
    <div class="layui-form-item layui-hide">
      <button class="layui-btn" lay-submit lay-filter="LAY-user-role-submit" id="LAY-user-role-submit">提交</button>
    </div>
  </div>

 <script type="text/javascript"  src="${basepath}statics/eleTree/layui/layui.js"></script>
 
 
  <script>
  

 	var selroleddxtext =  $('#selroleddx option:selected').text();
  	$('#selroleddxtext').val(selroleddxtext);
  	
  layui.config({
// base: '${pageContext.request.contextPath}/statics/layuiadmin/' //静态资源所在路径
		  base: "${pageContext.request.contextPath}/statics/eleTree/layui/lay/mymodules/"
  }).use(['jquery','table','eleTree','code','form','slider'], function(){
	  var $ = layui.jquery;
      var eleTree = layui.eleTree;
      var table = layui.table;
      var code = layui.code;
      var form = layui.form;
      var slider = layui.slider;
     
      form.on('select(selroleddx)', function(data){

    	  var selroleddxtext =  $('#selroleddx option:selected').text();
    	  	$('#selroleddxtext').val(selroleddxtext);

    	})
      $.ajax({ 
  		url: "addtogetallfun", 
  		success: function(data){
  			
  			var el5;
  	      $("[name='title']").on("click",function (e) {
  	          e.stopPropagation();
  	          if(!el5){
  	              el5=eleTree.render({
  	                  elem: '.ele5',
  	                  data: data,
  	                  // url: "../eleTree/tree.json",
  	                  defaultExpandAll: true,
  	               	  //showCheckbox: true,
  	                  expandOnClickNode: false,
  	                  highlightCurrent: true
  	              });
  	          }
  	          $(".ele5").toggle();
  	      })
  	      eleTree.on("nodeClick(data5)",function(d) {
  	          $("[name='title']").val(d.data.currentData.label);
  	          
  	        $("#hmfunid").val(d.data.currentData.id);
  	        
  	          $(".ele5").hide();
  	      }) 
  	      $(document).on("click",function() {
  	          $(".ele5").hide();
  	      })
  			
  		}
  			   
  	});
      
  })
  
  
	 
	  
	 
  </script>
</body>
</html>