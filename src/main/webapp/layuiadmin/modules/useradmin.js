/**

  用户管理 管理员管理 角色管理
    
 */

function getCookie(name) {
    var arr = document.cookie.match(new RegExp("(^| )" + name + "=([^;]*)(;|$)"));
    if (arr != null) return unescape(arr[2]); return null;
};

layui.define(['table', 'form'], function(exports){
  var $ = layui.$
  ,table = layui.table
  ,form = layui.form;

  
  //功能资源管理
  table.render({
    elem: '#LAY-funres-manage'
    ,url: 'getallfunres' //模拟接口
    
    ,cols: [[
      {type: 'checkbox', fixed: 'left'}
      ,{field: 'fid', width: 100, title: 'fid', sort: true}
      ,{field: 'fname',align:'center', title: '资源名称', width: 200}
      ,{field: 'identification',align:'center', title: '标识', width: 150}
      ,{field: 'type', align:'center',title: '类型', width: 100}
      ,{field: 'furl', align:'center',title: '功能路径', width: 220,}
      ,{field: 'creatorName',align:'center', title: '创建人', width: 160,}
      ,{field: 'createTime', width: 170, align:'center',title: '创建时间', sort: true,templet:function (d) {                   
    	    return showTime(d.createTime);
      }}
      ,{field: 'modifier', title: '修改人', width: 160,}
      ,{field: 'updateTime', width: 170, align:'center',title: '修改时间', sort: true,templet:function (d) {                   
    	    return showTime(d.updateTime);
      }}
      ,{title: '操作', width: 250, align:'center', fixed: 'right', toolbar: '#table-funres-webuser'}
    ]]
  ,page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
      layout: ['count', 'prev', 'page', 'next', 'limit', 'refresh', 'skip']//自定义分页布局 
      ,limits:[5,10,15]
      ,first: false //不显示首页
      ,last: false //不显示尾页
    }
    ,text: '对不起，加载出现异常！'
    ,done: function () {	
    	        $("[data-field='fid']").css('display','none');
    	     
    	         $("[data-field='type']").children().each(function(){
    	        		var text = $(this).text().replace('1', '目录');
    	        		text = text.replace('2', '菜单');
    	        		$(this).text(text);
    	          
    	        });
    	    }
  });
  
  //监听工具条
  table.on('tool(LAY-funres-manage)', function(obj){
    var data = obj.data;
    if(obj.event === 'del'){

    	 layer.confirm('敏感操作,此功能下所有的配置将会删除.确定删除吗？', function(index) {
        	$.ajax({ 
        		url: "delhmfunform?fid="+data.fid, 
        		contentType: "application/x-www-form-urlencoded; charset=utf-8", 
        		success: function(data){
        			if(data.code == 100){
        				
        			}else{
        				table.reload('LAY-funres-manage'); //数据刷新
                        layer.msg('删除失败');
     		            layer.close(index); //关闭弹层
        			}
        		}
        	});
        	layer.load(0, {
                shade:  [0.4,'#fff'],
                time: 2*1000,
                end:function(){
                	table.reload('LAY-funres-manage'); //数据刷新
                     layer.msg('已删除');
  		            layer.close(index); //关闭弹层
                }
            }); 
        });
    
    } else if(obj.event === 'edit'){
      var tr = $(obj.tr);
      layer.open({
        type: 2
        ,title: '编辑用户'
        ,content: 'toeditfun?funid='+data.fid
        ,maxmin: true
        ,area: ['500px', '450px']
        ,btn: ['确定', '取消']
        ,yes: function(index, layero){
          var iframeWindow = window['layui-layer-iframe'+ index]
          ,submitID = 'LAY-user-front-submit'
          ,submit = layero.find('iframe').contents().find('#'+ submitID);

          //监听提交
          iframeWindow.layui.form.on('submit('+ submitID +')', function(data){
            var field = data.field; //获取提交的字段
            //提交 Ajax 成功后，静态更新表格中的数据
            //$.ajax({});
            var sessionid = getCookie("sessonid");
            table.reload('LAY-funres-manage'); //数据刷新
            layer.close(index); //关闭弹层
            $.ajax({ 
        		url: "editrole?sessionid="+sessionid, 
        		contentType: 'application/x-www-form-urlencoded;charset=utf-8',
                data: field,
        		success: function(data){
        			if(data.code == 100){
        				table.reload('LAY-funres-manage'); //数据刷新
        	            layer.close(index); //关闭弹层
        			}else{
        				//location.href = '${basepath}login'; //后台主页
        			}
        			 
        		}
        			   
        	});
         
          });  
          
          submit.trigger('click');
        }
        ,success: function(layero, index){
          
        }
      });
    }
  });

  
  
  //角色管理
  table.render({
    elem: '#LAY-user-manage'
    ,url: 'getallrole' //模拟接口
    
    ,cols: [[
      {type: 'checkbox', fixed: 'left'}
      ,{field: 'roleid', width: 100, title: 'roleid', sort: true}
      ,{field: 'rolename', title: '角色名称', width: 100}
      ,{field: 'roleSub', title: '角色说明', width: 150,}
      ,{field: 'creatorName', title: '创建人', width: 100,}
      ,{field: 'createTime',  align:'center',title: '创建时间', sort: true,templet:function (d) {                   
    	    return showTime(d.createTime);
      }}
      ,{field: 'modifier', title: '修改人', width: 100,}
      ,{field: 'updateTime',  align:'center',title: '修改时间', sort: true,templet:function (d) {                   
    	    return showTime(d.updateTime);
      }}
      ,{title: '操作', width: 150, align:'center', fixed: 'right', toolbar: '#table-useradmin-webuser'}
    ]]
    ,page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
        layout: ['count', 'prev', 'page', 'next', 'limit', 'refresh', 'skip']//自定义分页布局 
    ,limits:[5,10,15]
    ,first: false //不显示首页
    ,last: false //不显示尾页
  }
    ,text: '对不起，加载出现异常！'
  });
  
  //监听工具条
  table.on('tool(LAY-user-manage)', function(obj){
    var data = obj.data;
    if(obj.event === 'del'){
    
        
        layer.confirm('敏感操作,此角色下所有的配置都会删除？', function(index){
        	$.ajax({ 
        		url: "delhmroleform?roleid="+data.roleid, 
        		contentType: "application/x-www-form-urlencoded; charset=utf-8", 
        		success: function(data){
        			if(data.code == 100){
        					
        			}else{
        				//location.href = '${basepath}login'; //后台主页
        			}
        		}
        	});
        	layer.load(0, {
                shade:  [0.4,'#fff'],
                time: 2*1000,
                end:function(){
              	   table.reload('LAY-user-manage'); //数据刷新
                     layer.msg('已删除');
  		            layer.close(index); //关闭弹层
                }
            }); 
        	
        });
    
    } else if(obj.event === 'edit'){
      var tr = $(obj.tr);
      
      layer.open({
        type: 2
        ,title: '编辑用户'
        ,content: 'toeditrole?roleid='+data.roleid
        ,maxmin: true
        ,area: ['500px', '450px']
        ,btn: ['确定', '取消']
        ,yes: function(index, layero){
          var iframeWindow = window['layui-layer-iframe'+ index]
          ,submitID = 'LAY-user-front-submit'
          ,submit = layero.find('iframe').contents().find('#'+ submitID);

          //监听提交
          iframeWindow.layui.form.on('submit('+ submitID +')', function(data){
            var field = data.field; //获取提交的字段
            //提交 Ajax 成功后，静态更新表格中的数据
            //$.ajax({});
            var sessionid = getCookie("sessonid");
            $.ajax({ 
        		url: "editrole?sessionid="+sessionid, 
        		contentType: 'application/x-www-form-urlencoded;charset=utf-8',
                data: field,
        		success: function(data){
        			if(data.code == 100){
        				table.reload('LAY-user-manage'); //数据刷新
        	            layer.close(index); //关闭弹层
        			}else{
        				//location.href = '${basepath}login'; //后台主页
        			}
        			 
        		}
        			   
        	});
         
          });  
          
          submit.trigger('click');
        }
        ,success: function(layero, index){
          
        }
      });
    }
  });

  //管理员管理
  table.render({
    elem: '#LAY-user-back-manage'
    ,url: 'getallcon' //模拟接口
    ,cols: [[
      {type: 'checkbox', fixed: 'left'}
      ,{field: 'cid', width: 80, title: 'ID', sort: true}
      ,{field: 'cname', title: '登录名'}
      ,{field: 'rolename', title: '角色'}
      ,{field: 'createTime', title: '创建时间', sort: true,templet:function (d) {                   
    	    return showTime(d.createTime);
      }}
      /*,{field: 'check', title:'审核状态', templet: '#buttonTpl', minWidth: 80, align: 'center'}*/
      ,{title: '操作', width: 150, align: 'center', fixed: 'right', toolbar: '#table-useradmin-admin'}
    ]]
    ,text: '敏感操作,此功能下所有的配置将会删除？'
    ,page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
    		      layout: ['count', 'prev', 'page', 'next', 'limit', 'refresh', 'skip']//自定义分页布局 
    		     ,limits:[5,10,15]
    		     ,first: false //不显示首页
    		     ,last: false //不显示尾页
    		}
     ,done: function () {	
    		        $("[data-field='cid']").css('display','none');
    		     
    		    }
  });
  
  //监听工具条
  table.on('tool(LAY-user-back-manage)', function(obj){
    var data = obj.data;
    if(obj.event === 'del'){
    	layer.confirm('敏感操作,此管理员下所有的配置将会删除', function(index){
          console.log(obj)
          	$.ajax({ 
        		url: "delhmconform?cid="+data.cid, 
        		contentType: "application/x-www-form-urlencoded; charset=utf-8", 
        		success: function(data){
        			if(data.code == 100){
        				 
        		            layer.load(0, {
        		                shade:  [0.4,'#fff'],
        		                time: 2*1000,
        		                end:function(){
        		                	 table.reload('LAY-user-back-manage'); //数据刷新
        	        		            layer.close(index); //关闭弹层
        		                }
        		            }); 
        			}else{
        				//location.href = '${basepath}login'; //后台主页
        			}
        		}
        	});
          
          /*obj.del();
          layer.close(index);*/
     });
     
    }else if(obj.event === 'edit'){
      var tr = $(obj.tr);
     
      layer.open({
        type: 2
        ,title: '编辑管理员'
        ,content: 'toeditcon?cid='+data.cid
        ,area: ['420px', '420px']
        ,btn: ['确定', '取消']
        ,yes: function(index, layero){
          var iframeWindow = window['layui-layer-iframe'+ index]
          ,submitID = 'LAY-user-back-submit'
          ,submit = layero.find('iframe').contents().find('#'+ submitID);

          //监听提交
          iframeWindow.layui.form.on('submit('+ submitID +')', function(data){
            var field = data.field; //获取提交的字段
            
            //提交 Ajax 成功后，静态更新表格中的数据
            //$.ajax({});
            
            $.ajax({ 
        		url: "editcon", 
        		contentType: 'application/x-www-form-urlencoded;charset=utf-8',
                data: field,
        		success: function(data){
        			if(data.code == 100){
        				  table.reload('LAY-user-back-manage'); //数据刷新
        		            layer.close(index); //关闭弹层
        			}else{
        				//location.href = '${basepath}login'; //后台主页
        			}
        			 
        		}
        			   
        	});
            
          
          });  
          
          submit.trigger('click');
        }
        ,success: function(layero, index){           
          
        }
      })
    }
  });

  function showTime(tempDate)
  {
	 
	  if(tempDate == undefined){
		 return '0-0-0-0 0:0:0';
	  }
        var d = new Date(tempDate); 
        var year = d.getFullYear();
        var month = d.getMonth();
        month++;
        var day = d.getDate();
        var hours = d.getHours();
        
        var minutes = d.getMinutes();
        var seconds = d.getSeconds();
        month = month<10 ? "0"+month:month;
        day = day<10 ? "0"+day:day;
        hours = hours<10 ? "0"+hours:hours;
        minutes = minutes<10 ? "0"+minutes:minutes;
        seconds = seconds<10 ? "0"+seconds:seconds;
        
        var time = year+"-"+month+"-"+day+" "+hours+":"+minutes+":"+seconds;
        return time;
  }
  var sessionid = getCookie("sessonid");
  //角色管理
  table.render({
    elem: '#LAY-user-back-role'
    ,url: "logingetalluser?sessionid="+sessionid //模拟接口
    
    ,cols: [[
      {type: 'checkbox', fixed: 'left'}
      
      ,{field: 'id', title: '功能权限id', sort: true}
      ,{field: 'cid',  title: '操作员id'}
      ,{field: 'hmUserAuthRelationid', title: '用户角色id'}
      ,{field: 'cname', width: 150, title: '用户名'}
      ,{field: 'detail', width: 200, title: '角色名称'}
      ,{field: 'secondName', width: 200,title: '功能名称'}
      ,{field: 'applyLevel',width: 200, title: '拥有权限'}
      ,{field: 'remark', width: 200,title: '具体描述'}
      ,{field: 'createdTime',width: 230, sort: true ,title: '创建时间',templet:function (d) {                   
    	    return showTime(d.createdTime);
      }}
      ,{title: '操作', width: 400, align: 'center', fixed: 'right', toolbar: '#table-useradmin-admin'}
    ]]
    ,text: '对不起，加载出现异常！'
    ,page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
            layout: ['count', 'prev', 'page', 'next', 'limit', 'refresh', 'skip']//自定义分页布局 
            ,limits:[5,10,15]
            ,first: false //不显示首页
            ,last: false //不显示尾页
          }
    ,done: function () {	
        $("[data-field='id']").css('display','none');
        $("[data-field='cid']").css('display','none');
        $("[data-field='hmUserAuthRelationid']").css('display','none');
        
         $("[data-field='applyLevel']").children().each(function(){
        		var text = $(this).text().replace('1', '添加');
        		text = text.replace('2', '删除');
        		text = text.replace('3', '修改');
        		text = text.replace('4', '查询');
        		$(this).text(text);
          
        });
    }
  });

  function writeObj(obj){ 
	  var description = ""; 
	  for(var i in obj){ 
	  var property=obj[i]; 
	  description+=i+" = "+property+"\n"; 
	  } 
	  alert(description); 
	 }

  //监听工具条
  table.on('tool(LAY-user-back-role)', function(obj){
    var data = obj.data;
    //writeObj(data);
    
    //alert(data.hmUserAuthRelationid);
    if(obj.event === 'del'){
      layer.confirm('确定删除此角色？', function(index){
    	  
    	  $.ajax({ 
        		url: "delereroreloform?id="+data.id+"&hmUserAuthRelationid="+data.hmUserAuthRelationid, 
        		contentType: "application/x-www-form-urlencoded; charset=utf-8", 
             
        		success: function(data){
        			if(data.code == 100){
        				  table.reload('LAY-user-back-role'); //数据刷新
        		            layer.close(index); //关闭弹层
        			}else{
        				location.href = '${basepath}login'; //后台主页
        			}
        			 
        		}
        			   
        	});

     /*   obj.del();
        layer.close(index);*/
      });
    }else if(obj.event === 'edit'){
      var tr = $(obj.tr);
      layer.open({
        type: 2
        ,title: '编辑角色'
        ,content: 'toreloform?id='+data.id+"&cid="+data.cid+"&hmUserAuthRelationid="+data.hmUserAuthRelationid
        ,area: ['520px', '350px']
        ,offset:"auto"
        ,btn: ['确定', '取消']
        ,yes: function(index, layero){
        	
          var iframeWindow = window['layui-layer-iframe'+ index]
          ,submit = layero.find('iframe').contents().find("#LAY-user-role-submit");
         
          
          //监听提交
          iframeWindow.layui.form.on('submit(LAY-user-role-submit)', function(data){
            var field = data.field; //获取提交的字段
            //writeObj(field);
            
            //提交 Ajax 成功后，静态更新表格中的数据
            //$.ajax({});
            $.ajax({ 
        		url: "updatetoreloform", 
        		contentType: 'application/x-www-form-urlencoded;charset=utf-8',
                data: field,
        		success: function(data){
        			if(data.code == 100){
        				  table.reload('LAY-user-back-role'); //数据刷新
        		            layer.close(index); //关闭弹层
        			}else{
        				location.href = '${basepath}login'; //后台主页
        			}
        			 
        		}
        			   
        	});
          
          });  
          
          submit.trigger('click');
        }
        ,success: function(layero, index){
        	
        }
      })
    }
  });

  exports('useradmin', {})
});
