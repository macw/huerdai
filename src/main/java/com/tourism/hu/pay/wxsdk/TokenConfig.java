package com.tourism.hu.pay.wxsdk;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tourism.hu.util.HttpClientsUtil;

/**
 * API配置类，项目中请保证其为单例 实现观察者模式，用于监控token变化
 *
 * @author peiyu
 * @since 1.2
 */
public class TokenConfig implements Serializable {

	private static final Logger LOG = LoggerFactory.getLogger(TokenConfig.class);
	/**
	 * 这里定义token正在刷新的标识，想要达到的目标是当有一个请求来获取token，发现token已经过期（我这里的过期逻辑是比官方提供的早100秒），然后开始刷新token
	 * 在刷新的过程里，如果又继续来获取token，会先把旧的token返回，直到刷新结束，之后再来的请求，将获取到新的token
	 * 利用AtomicBoolean实现原理：
	 * 当请求来的时候，检查token是否已经过期（7100秒）以及标识是否已经是true（表示已经在刷新了，还没刷新完），过期则将此标识设为true，并开始刷新token
	 * 在刷新结束前再次进来的请求，由于标识一直是true，而会直接拿到旧的token，由于我们的过期逻辑比官方的早100秒，所以旧的还可以继续用
	 * 无论刷新token正在结束还是出现异常，都在最后将标识改回false，表示刷新工作已经结束
	 */

	private static String accessToken;
	private static String jsApiTicket;
	private static long jsTokenTime = System.currentTimeMillis();
	private static long tokenTime= System.currentTimeMillis();

	
	 private static void initToken(long refreshTime) {
	        LOG.debug("开始初始化access_token........");
	        //记住原本的时间，用于出错回滚
	        final long oldTime = tokenTime;
	        System.out.println("initToken="+oldTime+":"+refreshTime);
	        tokenTime = refreshTime;
	        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + WeixinConfig.appId + "&secret=" + WeixinConfig.appSecret;
	        try {
	        	Map<String,Object> map= HttpClientsUtil.doGetToMap(url);
	        	if(map!=null && map.containsKey("expires_in")) {
	        		tokenTime=System.currentTimeMillis()+(6900*1000);
	        		accessToken = map.get("access_token").toString();
	        	}
			} catch (Exception e) {
				e.printStackTrace();
			}
	    }
	
	public static String getAccessToken(){
		long time = System.currentTimeMillis();
		if(time>tokenTime) {
			initToken(time);
		}
		System.out.println("accessToken ===" + accessToken);
		return accessToken;
	}
	
	

	public static String getJsApiTicket() {
		long now = System.currentTimeMillis();
			System.out.println("getJsApiTicket=" + now + ":" + jsTokenTime);
			// 官方给出的超时时间是7200秒，这里用7100秒来做，防止出现已经过期的情况
			if (now > jsTokenTime) {
				getAccessToken();
				initJSToken(now);
			}
			System.out.println("jsApiTicket ===" + jsApiTicket);
		return jsApiTicket;
	}


	 public static void main(String[] args) {
		 String timestamp1 = String.valueOf(new Date().getTime()).substring(0, 10);
		 System.out.println(timestamp1);
	}
	
	/**
	 * 初始化微信JS-SDK，获取JS-SDK token
	 *
	 * @param refreshTime 刷新时间
	 */
	private static void initJSToken(final long refreshTime) {
		LOG.debug("初始化 jsapi_ticket........");
		final long oldTime = jsTokenTime;
		System.out.println("initJSToken=" + oldTime + ":" + refreshTime);
		jsTokenTime = refreshTime;
		String url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=" + accessToken + "&type=jsapi";
		Map<String, Object> map = HttpClientsUtil.doGetToMap(url);
		double errcode = (Double) map.get("errcode");
		if(errcode==0) {
			jsApiTicket =  map.get("ticket").toString();
			jsTokenTime =  System.currentTimeMillis()+(6900*1000); 
		}
	}

}
