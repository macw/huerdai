package com.tourism.hu.pay.wxsdk;

import lombok.Data;

@Data
public class NotifyModel {

	/**
	 * 返回状态码
	 */
	private String return_code;
	/**
	 * 返回信息
	 */
	private String return_msg;
	 /**
	  * 程序应用Id
	  */
	private String appid;
	/**
	 *  商户Id
	 */
	private String mch_id;
	/**
	 * 设备号
	 */
	private String device_info;
	/**
	 * 随机字符串
	 */
	private String nonce_str;
	/**
	 * 签名
	 */
	private String sign;
	/**
	 * 业务结果    SUCCESS/FAIL
	 */
	private String result_code;
	/**
	 * 错误代码
	 */
	private String err_code;
	/**
	 * 错误代码描述   
	 */
	private String err_code_des;
	/**
	 * 用户在商户appid下的唯一标识
	 */
	private String openid;
	/**
	 * 用户是否关注公众账号，Y-关注，N-未关注
	 */
	private String is_subscribe;
	/**
	 * 交易类型   APP 
	 */
	private String trade_type;
	/**
	 * 付款银行
	 */
	private String bank_type;
	/**
	 * 订单总金额  单位 为分
	 */
	private Integer total_fee;
	/**
	 * 货币种类  CNY
	 */
	private String fee_type;
	 
	
	 /**
	  * 微信支付订单号
	  */
	private String transaction_id;
	/**
	 * 商户订单号
	 */
	private String out_trade_no;
	/**
	 * 支付完成时间  支付完成时间，格式为yyyyMMddHHmmss，如2009年12月25日9点10分10秒表示为20091225091010。
	 */
	private String time_end;
	
	
	
	
}
