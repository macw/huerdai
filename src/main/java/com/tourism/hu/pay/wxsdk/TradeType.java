package com.tourism.hu.pay.wxsdk;

public enum TradeType {
	  JSAPI("JSAPI"),
	  Native("Native"),
	  APP("APP");
	 
	private String tradeType;
	
	TradeType(String tradeType){
		this.tradeType=tradeType;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	
	
	
}
