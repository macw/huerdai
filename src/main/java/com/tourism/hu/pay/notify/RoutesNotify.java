package com.tourism.hu.pay.notify;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.OrderConstant;
import com.tourism.hu.entity.*;
import com.tourism.hu.pay.wxsdk.WXPayUtil;
import com.tourism.hu.pay.wxsdk.WeixinConfig;
import com.tourism.hu.service.*;
import com.tourism.hu.util.DateConverterUtil;
import com.tourism.hu.util.RequestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@RequestMapping("notify")
@Controller
public class RoutesNotify {

    private static Logger logger = LoggerFactory.getLogger(RoutesNotify.class);

    @Autowired
    private IProfitFlowingService flowingService;

    @Autowired
    private IScenicOrderService routesOrderService;

    @Resource
    private IOrderService iOrderService;

    @Resource
    private IOrderDetailService iOrderDetailService;

    @Resource
    private IGoodsService iGoodsService;

    @Resource
    private IGoodsCategoryService iGoodsCategoryService;

    @Resource
    private ICustomerInfoService iCustomerInfoService;

    /**
     * @Title: routesOrder
     * @Description: (线路订单回调)
     * @date 2019年11月21日 下午2:38:32
     * @author 董兴隆
     */
    @RequestMapping("/routesOrder")
    public void routesOrder(HttpServletRequest request, HttpServletResponse resp) throws Exception {

        Map<String, String> map = getNotifyMap(request);
        String msg = "FAIL";
        if ("200".equals(map.get("code"))) {
            String orderNo = map.get("orderNo");
            String serialNumber = map.get("serialNumber");
            ProfitFlowing pf = flowingService.getOne(new QueryWrapper<ProfitFlowing>().eq("order_sn", orderNo), false);
            if (pf == null) {
                return;
            }
            pf.setIsPay(1);
            pf.setSerialNumber(serialNumber);
            ScenicOrder scenicOrder = routesOrderService.getOne(new QueryWrapper<ScenicOrder>().eq("order_sn", orderNo));
            if (scenicOrder == null) {
                resp.getWriter().print(msg);
                return;
            }
            scenicOrder.setPayTime(LocalDateTime.now());
            scenicOrder.setOrderStatus(1);
            scenicOrder.setOrderPoint(10);//10积分
            routesOrderService.updateById(scenicOrder);
            flowingService.updateById(pf);
            msg = "SUCCESS";
        }
        resp.getWriter().print(msg);


    }


    /**
     * 商城 支付成功后执行 这个回调
     * 更新订单表支付状态 和  流水表支付状态
     */
    @RequestMapping("/successPay")
    public void successPay(HttpServletRequest request, HttpServletResponse resp) throws Exception {
        logger.debug("----------------商品订单回调方法--------------");
        Map<String, String> map = getNotifyMap(request);

        logger.debug("-------------getNotifyMap(request)===" + map);
        String msg = "FAIL";
        try {
            if ("200".equals(map.get("code"))) {
                logger.debug("--------------map.get(\"code\")=====" + map.get("code"));
                String orderNo = map.get("orderNo");
                String serialNumber = map.get("serialNumber");
                logger.debug("---------map.get(\"orderNo\") ====  " + map.get("orderNo"));
                Order order = iOrderService.getOne(new QueryWrapper<Order>().eq("order_sn", orderNo));
                logger.debug("__________order================="+order);
                if (order == null) {
                    resp.getWriter().print(msg);
                    return;
                }
                OrderDetail orderDetail = iOrderDetailService.getOne(new QueryWrapper<OrderDetail>().eq("order_sn", orderNo));
                GoodsCategory goodsCategory = iGoodsCategoryService.getById(iGoodsService.getById(orderDetail.getGoodsId()).getGoodsclassid());
                //如果该商品在玩赚下的分类有值，则说明这是该商品是玩赚，则把购买用户设为会员
                if (goodsCategory != null) {
                    logger.debug("goodsCategory.getClassIdStatus()======"+goodsCategory.getClassIdStatus());
                    //判断是不是玩赚商品
                    if (2 == goodsCategory.getClassIdStatus() ) {
                        CustomerInfo customerInfo = iCustomerInfoService.getOne(new QueryWrapper<CustomerInfo>().eq("id", order.getCustomerId()));
                        logger.debug("customerInfo==========="+customerInfo);
                        LocalDateTime localDateTime = null;
                        if (BaseConstant.USE_STATUS!=customerInfo.getIsCuslevel()) {
                            //说明该用户不是会员
                            //将当期用户设为会员,并且设置到期时间
                            localDateTime = DateConverterUtil.getTime(399 * (orderDetail.getProductCnt()));
                        }else {
                            //说明用户已经购买过会员，要再次续期，
                            localDateTime = DateConverterUtil.getTime(customerInfo.getEndTime(), 399 * (orderDetail.getProductCnt()));
                        }
                        logger.debug("localDateTime==========="+localDateTime);
                        customerInfo.setEndTime(localDateTime);
                        customerInfo.setIsCuslevel(BaseConstant.USE_STATUS);
                        iCustomerInfoService.updateById(customerInfo);
                    }
                }

                logger.debug("--------------------------------------");
                ProfitFlowing profitFlowing = flowingService.getOne(new QueryWrapper<ProfitFlowing>().eq("order_sn", orderNo));
                if (profitFlowing == null) {
                    resp.getWriter().print(msg);
                    return;
                }
                logger.debug("------------订单，流水信息，查询成功-0---------------");
                //订单表状态设为已支付
                order.setOrderStatus(OrderConstant.TWO_YES_PAY);
                order.setPayTime(LocalDateTime.now());
                //流水表设为已支付
                profitFlowing.setIsPay(BaseConstant.USE_STATUS);
                boolean b = flowingService.updateById(profitFlowing);
                boolean b1 = iOrderService.updateById(order);
                logger.debug("----------------商品回调状态更新------------");
                if (b && b1) {
                    logger.debug("--------------状态更新成功-----------");
                    msg = "SUCCESS";
                } else {
                    logger.debug("--------状态更新失败----------");
                    resp.getWriter().print(msg);
                    return;
                }
                logger.debug("-----bbbbbbbbbbbbbbbb--" + b + "--------b1-------------" + b1);
            }
        } catch (
                Exception e) {
            e.printStackTrace();
            logger.debug("Exception===" + e.getMessage());
        }
        resp.getWriter().

                print(msg);

    }


    /**
     * @param request
     * @return {"code":"200","orderNo":"订单号","serialNumber":"微信订单流水号"}   200表示成功
     * @Title: getNotifyMap
     * @Description: (通过request 获得 微信返回的信息)
     * @date 2019年11月27日 下午6:00:09
     * @author 董兴隆
     */
    public static Map<String, String> getNotifyMap(HttpServletRequest request) {
        logger.info("=====开始处理支付回调通知");
        //1 获取微信支付异步回调结果
        String xmlResult = RequestUtil.getPostStr(request);
        logger.info("xmlResult=============>" + xmlResult);
        Map<String, String> resultMap = new HashMap<String, String>();
        int code = 400;
        try {
            //将结果转成map
            resultMap = WXPayUtil.xmlToMap(xmlResult);
            logger.info("resultMap=============>" + resultMap);
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        //订单号
        String sign = resultMap.get("sign");
        //去掉sign和利用微信回调回来的信息重新加密
        resultMap.remove("sign");
        logger.info("sign=============>" + sign);
        String sign1 = "";
        try {
            //重新加密 获取加密的签名
            sign1 = WXPayUtil.generateSignature(resultMap, WeixinConfig.partnerKey); //签名
            logger.info("sign1=============>" + sign1);
        } catch (Exception e) {

        }
        if (sign.equals(sign1)) {
            code = 200;
        }
        resultMap.put("code", code + "");
        resultMap.put("orderNo", resultMap.get("out_trade_no"));
        resultMap.put("serialNumber", resultMap.get("transaction_id"));
        logger.debug("------------code=== " + code);
        return resultMap;
    }


}
