package com.tourism.hu.constant;

public class MenuConsant {


    /**
     * 一级 菜单
     */
    public static final Integer ONE_LEVEL = 1;
    /**
     * 二级菜单
     */
    public static final Integer TWO_LEVEL = 2;
    /**
     * 三级菜单
     */
    public static final int THREE_LEVEL = 3;

}
