package com.tourism.hu.constant;

public class AuthorConstant {

    /**
     * 资源类型， 0，代表是 菜单
     */
    public static final int MENU_RESOURCE_TYPE = 0;

    /**
     * 资源类型 ， 1， 代表是按钮
     */
    public static final int BTN_RESOURCE_TYPE = 1;

}
