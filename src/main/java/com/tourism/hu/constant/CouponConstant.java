package com.tourism.hu.constant;

public class CouponConstant {

    /**
     * 红包，优惠卷 状态 1已发布 2未发布 3发布中 4已下架 5过期
     */
    public static final int ONE_STATUS = 1;
    public static final int TWO_STATUS = 2;
    public static final int THREE_STATUS = 3;
    public static final int FOUR_STATUS = 4;
    public static final int FIVE_STATUS = 5;

}
