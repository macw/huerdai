package com.tourism.hu.constant;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 10:59
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
public class WalletRecordConstant {
    /**
     * 订单收益
     */
    public static final int ORDER_PROFIT = 1;

    /**
     * 提现
     */
    public static final int CASH_PROFIT = 2;

    /**
     * 商品消费
     */
    public static final int GOODS_CONSUME = 3;

}
