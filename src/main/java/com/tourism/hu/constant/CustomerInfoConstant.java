package com.tourism.hu.constant;

public class CustomerInfoConstant {

    /**
     * 注册类型 1邀请注册链接 2网页注册 3扫码注册
     */
    public static final Integer INVITE_REG = 1;
    public static final Integer WEB_REG = 2;
    public static final Integer SM_REG = 3;


    /**
     * 微信扫码注册，（群管理代理商）
     */
    public static final Integer WECHAT_REG = 4;

    /**
     * 验证码注册
     */
    public static final Integer MB_REG = 5;


    public static final Integer YES_CUSLEVEL = 1;
    public static final Integer NO_CUSLEVEL = 2;




}
