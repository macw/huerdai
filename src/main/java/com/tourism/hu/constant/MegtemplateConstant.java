package com.tourism.hu.constant;

public class MegtemplateConstant {

    /**
     * 模板状态， 1，启用
     */
    public static final String USE_STATUE = "1";

    /**
     * 模板状态  2， 禁用
     */
    public static final String DISUSE_STATUS = "2";
}
