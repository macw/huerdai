package com.tourism.hu.constant;

public class BaseConstant {

    /**
     * 通用状态，1，代表启用
     */
    public static final int USE_STATUS = 1;

    /**
     * 通用状态，0 ，代表 禁用
     */
    public static final int DISUSE_STATUS = 0;


    /**
     * 分成 ，成员类型
     */
    public static final Integer AGENT_TYPE = 1;
    public static final Integer USER_TYPE = 2;
    public static final Integer PLAT_TYPE = 3;
    public static final Integer PARE_TYPE = 4;


    /**
     * 服务器文件路径存放地址
     */
    public static final String FILE_PATH = "C:\\apache-tomcat-8.5.47\\apache-tomcat-8.5.47\\webapps\\img\\";

    /**
     * 服务器文件路径 请求地址
     */
    public static final String GET_FILE_PATH = "http://static.huerdai.net/img/";

    /**
     * 系统内部调用地址:
     */
    public static final String OS_PATH = "http://api.huerdai.net/hu/";


}
