package com.tourism.hu.constant;

public class CustomerFriendConstant {

    /**
     * 关系：  1关注 2好友 3粉丝
     */
    public static final int FOLLOW_tYPE = 1;
    public static final int FRIEND_tYPE = 2;
    public static final int FANS_tYPE = 3;

}
