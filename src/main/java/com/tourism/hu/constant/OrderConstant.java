package com.tourism.hu.constant;

public class OrderConstant {

    /**
     * 取消订单
     */
    public static final int NO_USE = 0;

    /**
     * 订单状态 1未支付,代付款
     */
    public static final int ONE_NO_PAY = 1;
    /**
     * 2.已支付，未发货，待发货
     */
    public static final int TWO_YES_PAY = 2;


    /**
     * 3，已发货 ，，未收获，待收获
     */
    public static final int THREE_SHIPPING = 3;
    /**
     * 4，已收货，未评价，待评价
     */
    public static final int FOUR_DONE = 4;

    /**
     * 评价完成
     */
    public static final int FIVE_DONE = 5;


    /**
     * 5, 订单退款
     */
    public static final int TEN_EXIT = 10;
    /**
     * 6，退款完成
     */
    public static final int ELEVEN_DONE = 11;

    /**
     * 删除订单
     */
    public static final int DELETE_STATE = 500;

    /**
     * 待使用
     */
    public static final int TWOONE_NO_USE = 21;



}
