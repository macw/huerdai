package com.tourism.hu.constant;

public class ProfitSaringConstant {

    /**
     * 利润分规则：1普通用户  2会员
     */
    public static final int ORD_TYPE = 1;
    /**
     * 利润分规则：1普通用户  2会员
     */
    public static final int MEM_TYPE = 2;
    /**
     * 利润规则状态：状态 1 旅游 2 商品
     */
    public static final int TRIP_STATUS = 1;
    /**
     * 利润规则状态：状态 1 旅游 2 商品
     */
    public static final int GOODS_STATUS = 2;


}
