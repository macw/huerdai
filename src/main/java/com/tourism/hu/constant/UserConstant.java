package com.tourism.hu.constant;

public class UserConstant {


    /**
     * 部门（或者叫类型）部门id (1系统账户，2省级代理商，3普通代理商)
     */
    public static final int DEP_ONE = 1;
    public static final int DEP_TWO = 2;
    public static final int DEP_THREE = 3;


}
