package com.tourism.hu.constant;

public class GoodsPicConstant {

    /**
     * 商品logo展示图
     */
    public static final int LOGO_TYPE = 1;
    /**
     * 商品详情图
     */
    public static final int DESC_TYPE = 2;

    /**
     * 图片级别， 1 2 3 4 5
     */
    public static final int ONE_LEVEL = 1;
    public static final int TWO_LEVEL = 2;
    public static final int THREE_LEVEL = 3;
    public static final int FOUR_LEVEL = 4;
    public static final int FIVE_LEVEL = 5;




}
