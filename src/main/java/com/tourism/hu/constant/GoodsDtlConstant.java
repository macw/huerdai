package com.tourism.hu.constant;

public class GoodsDtlConstant {

    /**
     * 商品规格类型，1，颜色
     */
    public static final int COLOR_TYPE = 1;
    /**
     * 商品规格类型，2，型号
     */
    public static final int MODEL_TYPE = 2;

}
