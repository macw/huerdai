package com.tourism.hu.constant;

public class GoodsCategoryConstant {

    /**
     * 一级分类
     */
    public static final int ONE_LEVEL = 1;
    /**
     * 二级分类
     */
    public static final int TWO_LEVEL = 2;
    /**
     * 三级分类
     */
    public static final int THREE_LEVEL = 3;


    /**
     * 玩赚id
     */
    public static final int WANZID = 27;



}
