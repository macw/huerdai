package com.tourism.hu.constant;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 18:23
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
public class WXLoginConstant {
    /**
     * 微信登录 APPID
     */
    public static final String APPID = "wxfc5d809b1ae7ff17";
    /**
     * 微信授权ID 秘钥
     */
    public static final String Auth_APP_ID = "wx8dd06324a13cdbc5";     //填写自己的APPID
    
    /**
     *secret
     */
    public static final String SECRET = "819fd886b364faf1ea4b11aadaf60be4";
    /**
     * redirect_uri
     */
    public static final String REDIRECT_URI = "http://api.huerdai.net/hu/wxLogin/callback";

    /**
     * response_type
     */
    public static final String RESPONSE_TYPE = "code";

    /**
     * scope
     */
    public static final String SCOPE = "snsapi_login";


    public static final String Auth_APP_SECRET = "f88a2f9d5b7820b54a286e3abdf46e4f";   //填写自己的APPSECRET






}
