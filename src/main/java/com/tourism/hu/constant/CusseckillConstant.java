package com.tourism.hu.constant;

public class CusseckillConstant {

    /**
     * 拼团， 1为团长，0为团员
     */
    public static final String TUZHANG = "1";
    /**
     * 拼团， 1为团长，0为团员
     */
    public static final String TUYUAN = "0";

    /**
     * 成团，去付款
     */
    public static final String CETUAN = "2";

}
