package com.tourism.hu.constant;

public class WechatRegionCodeConstant {
    /**
     * 代理类型，1，区域代理 ，  0， 省级管理
     */
    public static final int AREA_TYPE = 1;
    public static final int MANAGER_TYPE = 0;
}
