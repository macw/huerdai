package com.tourism.hu.mapper;

import com.tourism.hu.entity.vo.CusseckillVo;
import com.tourism.hu.entity.Cusseckill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 会员秒杀表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-29
 */
public interface CusseckillMapper extends BaseMapper<Cusseckill> {

    List<CusseckillVo> selectCusseckillVo(Integer spotId);

}
