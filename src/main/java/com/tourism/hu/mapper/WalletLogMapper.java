package com.tourism.hu.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.WalletLog;
import com.tourism.hu.entity.dto.WalletLogDto;

/**
 * <p>
 * 钱包变动日志 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-24
 */
public interface WalletLogMapper extends BaseMapper<WalletLog> {
	
	Long getCount(WalletLogDto dto);

	List<WalletLogDto> selectPageDataList(WalletLogDto dto);
}
