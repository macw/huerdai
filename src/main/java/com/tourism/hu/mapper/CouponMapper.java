package com.tourism.hu.mapper;

import com.tourism.hu.entity.Coupon;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-24
 */
public interface CouponMapper extends BaseMapper<Coupon> {

}
