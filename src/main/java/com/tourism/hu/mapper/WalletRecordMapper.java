package com.tourism.hu.mapper;

import com.tourism.hu.entity.WalletRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 钱包交易记录表 Mapper 接口
 * </p>
 *
 * @author hu
 * @since 2019-12-13
 */
public interface WalletRecordMapper extends BaseMapper<WalletRecord> {

}
