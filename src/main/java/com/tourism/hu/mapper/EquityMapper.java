package com.tourism.hu.mapper;

import com.tourism.hu.entity.Equity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-15
 */
public interface EquityMapper extends BaseMapper<Equity> {

}
