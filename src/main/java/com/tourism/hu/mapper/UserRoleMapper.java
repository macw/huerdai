package com.tourism.hu.mapper;

import com.tourism.hu.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-06
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

    /**
     * 根据用户名，查询该用户属于的角色
     * @param username
     * @return
     */
    UserRole selectRoleByUserName(String username);

}
