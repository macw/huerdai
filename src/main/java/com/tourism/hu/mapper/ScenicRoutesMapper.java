package com.tourism.hu.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.ScenicRoutes;
import com.tourism.hu.entity.dto.ScenicRoutesDto;

/**
 * <p>
 * 旅游线路表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface ScenicRoutesMapper extends BaseMapper<ScenicRoutes> {
	
	Long  getCount(ScenicRoutesDto dto);
	
	List<ScenicRoutesDto> selectPageListData(ScenicRoutesDto dto);
}
