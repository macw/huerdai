package com.tourism.hu.mapper;

import com.tourism.hu.entity.OrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单详情表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {


}
