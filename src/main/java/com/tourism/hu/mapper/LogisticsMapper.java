package com.tourism.hu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.Logistics;
import com.tourism.hu.entity.dto.LogisticsDto;

/**
 * <p>
 * 物流信息表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface LogisticsMapper extends BaseMapper<Logistics> {

	
	Long getCount(LogisticsDto dto);
	
	List<LogisticsDto> selectPageDataList(LogisticsDto dto);
	
	//List<LogisticsDto> selectDataList(Page<Logistics> page,@Param(Constants.WRAPPER) Wrapper<LogisticsDto> wrapper);
}
