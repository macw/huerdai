package com.tourism.hu.mapper;

import com.tourism.hu.entity.StrategicComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 攻略评论表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface StrategicCommentMapper extends BaseMapper<StrategicComment> {

}
