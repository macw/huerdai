package com.tourism.hu.mapper;

import com.tourism.hu.entity.Wallet;
import com.tourism.hu.entity.dto.WalletDto;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户钱包 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-20
 */
public interface WalletMapper extends BaseMapper<Wallet> {

	Long getCount(WalletDto dto);
	
	List<WalletDto> selectPageDataList(WalletDto dto);
}
