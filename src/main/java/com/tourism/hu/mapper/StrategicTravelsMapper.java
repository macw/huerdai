package com.tourism.hu.mapper;

import com.tourism.hu.entity.StrategicTravels;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 攻略游记表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface StrategicTravelsMapper extends BaseMapper<StrategicTravels> {

}
