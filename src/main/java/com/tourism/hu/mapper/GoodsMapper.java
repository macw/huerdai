package com.tourism.hu.mapper;

import com.tourism.hu.entity.vo.GoodsDetailsVo;
import com.tourism.hu.entity.Goods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.vo.GoodsVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 商品信息表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface GoodsMapper extends BaseMapper<Goods> {

    /**
     * 查询单个商品的商品详情信息
     * @return
     */
    GoodsDetailsVo selectGoodsDetails(Integer goodsId);




}
