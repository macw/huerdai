package com.tourism.hu.mapper;

import com.tourism.hu.entity.DedeSysconfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统参数配置表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface DedeSysconfigMapper extends BaseMapper<DedeSysconfig> {

}
