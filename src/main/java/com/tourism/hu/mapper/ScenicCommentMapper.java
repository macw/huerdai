package com.tourism.hu.mapper;

import com.tourism.hu.entity.ScenicComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 线路评论表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-05
 */
public interface ScenicCommentMapper extends BaseMapper<ScenicComment> {

}
