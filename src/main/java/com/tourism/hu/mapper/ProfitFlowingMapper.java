package com.tourism.hu.mapper;

import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.ProfitFlowing;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.vo.OrderProfitFlowing;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 利润分成表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-11
 */
public interface ProfitFlowingMapper extends BaseMapper<ProfitFlowing> {

    List<OrderProfitFlowing> getOrderProfit(@Param("start") Integer start, @Param("limit") Integer limit, @Param("userId") Integer userId,@Param("nickname") String nickname,@Param("goodsname") String goodsname,@Param("orderSn") String orderSn);

    Long getCount(@Param("start") Integer start, @Param("limit") Integer limit, @Param("userId") Integer userId,@Param("nickname") String nickname,@Param("goodsname") String goodsname,@Param("orderSn") String orderSn);

}
