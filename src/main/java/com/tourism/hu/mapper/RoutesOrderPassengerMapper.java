package com.tourism.hu.mapper;

import com.tourism.hu.entity.RoutesOrderPassenger;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 旅游订单乘车人信息表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-12
 */
public interface RoutesOrderPassengerMapper extends BaseMapper<RoutesOrderPassenger> {

}
