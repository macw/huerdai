package com.tourism.hu.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.CustomerLoginLog;
import com.tourism.hu.entity.dto.CustomerLoginLogDto;

/**
 * <p>
 * 用户登陆日志表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface CustomerLoginLogMapper extends BaseMapper<CustomerLoginLog> {

	List<CustomerLoginLogDto> selectCustomerLoginLogDto(CustomerLoginLogDto dto);

	Long selectCustomerLoginLogCount(CustomerLoginLogDto dto);
}
