package com.tourism.hu.mapper;

import com.tourism.hu.entity.StrategicTravelsClass;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 攻略游记分类表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface StrategicTravelsClassMapper extends BaseMapper<StrategicTravelsClass> {

}
