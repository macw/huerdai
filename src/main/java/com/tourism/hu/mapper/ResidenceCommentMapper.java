package com.tourism.hu.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.ResidenceComment;
import com.tourism.hu.entity.dto.ResidenceCommentDto;

/**
 * <p>
 * 民宿评论表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface ResidenceCommentMapper extends BaseMapper<ResidenceComment> {
	
	Long getCount(ResidenceCommentDto dto);

	List<ResidenceCommentDto> selectPageDataList(ResidenceCommentDto dto);
}
