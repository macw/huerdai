package com.tourism.hu.mapper;

import com.tourism.hu.entity.Merchant;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商户表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface MerchantMapper extends BaseMapper<Merchant> {

}
