package com.tourism.hu.mapper;

import com.tourism.hu.entity.Blacklistcode;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-29
 */
public interface BlacklistcodeMapper extends BaseMapper<Blacklistcode> {

}
