package com.tourism.hu.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.PaymentTransactionLog;
import com.tourism.hu.entity.dto.PaymentTransactionLogDto;

/**
 * <p>
 * 支付交易日志表  Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface PaymentTransactionLogMapper extends BaseMapper<PaymentTransactionLog> {

	
	Long getCount(PaymentTransactionLogDto dto);
	
	List<PaymentTransactionLogDto> selectData(PaymentTransactionLogDto dto);
}
