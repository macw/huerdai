package com.tourism.hu.mapper;

import com.tourism.hu.entity.Star;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hu
 * @since 2019-12-09
 */
public interface StarMapper extends BaseMapper<Star> {

}
