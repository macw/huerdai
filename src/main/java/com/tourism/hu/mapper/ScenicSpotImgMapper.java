package com.tourism.hu.mapper;

import com.tourism.hu.entity.ScenicSpotImg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 旅游景点图片表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-31
 */
public interface ScenicSpotImgMapper extends BaseMapper<ScenicSpotImg> {

}
