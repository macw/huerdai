package com.tourism.hu.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.ResidenceOrder;
import com.tourism.hu.entity.dto.ResidenceOrderDto;

/**
 * <p>
 * 民宿订单表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface ResidenceOrderMapper extends BaseMapper<ResidenceOrder> {

	Long getCount(ResidenceOrderDto dto);

	List<ResidenceOrderDto> selectPageDataList(ResidenceOrderDto dto);
}
