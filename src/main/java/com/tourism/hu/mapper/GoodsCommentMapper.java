package com.tourism.hu.mapper;

import com.tourism.hu.entity.GoodsComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.dto.GoodsCommentDto;
import com.tourism.hu.entity.dto.PaymentChannelDto;

import java.util.List;

/**
 * <p>
 * 商品评论表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface GoodsCommentMapper extends BaseMapper<GoodsComment> {

    /**
     * 查询 商品评论表总条数
     * @param dto
     * @return
     */
    Long getGoodsCommentCount(GoodsCommentDto dto);

    /**
     * 查询 商品评论表 所有数据
     * @param dto
     * @return
     */
    List<GoodsCommentDto> selectGoodsCommentDto(GoodsCommentDto dto);

}
