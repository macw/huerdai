package com.tourism.hu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.vo.GoodsCategoryVo;
import com.tourism.hu.entity.GoodsCategory;
import com.tourism.hu.entity.TreeNode;

import java.util.List;

/**
 * <p>
 * 商品分类表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface GoodsCategoryMapper extends BaseMapper<GoodsCategory> {

    /**
     * 查询， 商品分类表的， 一级 二级目录
     * @return
     */
    List<TreeNode> selectAll();

    /**
     * 接口,
     * 查询秒杀商品列表
     * @return
     */
    List<GoodsCategoryVo> selectGoodsCategoryList();

    /**
     * 查询商品的分类
     * @return
     */
    List<GoodsCategoryVo> selectCategoryList();





}
