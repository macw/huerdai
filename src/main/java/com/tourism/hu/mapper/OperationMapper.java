package com.tourism.hu.mapper;

import com.tourism.hu.entity.Operation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-06
 */
public interface OperationMapper extends BaseMapper<Operation> {

}
