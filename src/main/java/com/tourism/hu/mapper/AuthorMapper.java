package com.tourism.hu.mapper;

import com.tourism.hu.entity.Author;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-06
 */
public interface AuthorMapper extends BaseMapper<Author> {

}
