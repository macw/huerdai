package com.tourism.hu.mapper;

import com.tourism.hu.entity.OrderCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 购物车表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface OrderCartMapper extends BaseMapper<OrderCart> {

}
