package com.tourism.hu.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.ScenicRoutesprice;
import com.tourism.hu.entity.dto.ScenicRoutespriceDto;

/**
 * <p>
 * 旅游线路表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-19
 */
public interface ScenicRoutespriceMapper extends BaseMapper<ScenicRoutesprice> {

	ScenicRoutespriceDto  getByDayAndRoutesId(ScenicRoutespriceDto dto);
	
	ScenicRoutespriceDto  getByDayAndSpotId(ScenicRoutespriceDto dto);
	
	List<ScenicRoutespriceDto> selectByMonthAndRoutesId(ScenicRoutespriceDto dto);
	
	List<ScenicRoutespriceDto> selectByMonthAndSpotId(ScenicRoutespriceDto dto);
	
}
