package com.tourism.hu.mapper;

import com.tourism.hu.entity.CustomerInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface CustomerInfoMapper extends BaseMapper<CustomerInfo> {

}
