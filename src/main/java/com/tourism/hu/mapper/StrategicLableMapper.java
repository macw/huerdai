package com.tourism.hu.mapper;

import com.tourism.hu.entity.StrategicLable;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hu
 * @since 2019-11-28
 */
public interface StrategicLableMapper extends BaseMapper<StrategicLable> {

}
