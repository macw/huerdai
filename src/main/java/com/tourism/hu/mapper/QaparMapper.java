package com.tourism.hu.mapper;

import com.tourism.hu.entity.Qapar;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 凭证表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface QaparMapper extends BaseMapper<Qapar> {

}
