package com.tourism.hu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.HmFunctionstree;
import com.tourism.hu.entity.Menu;
import com.tourism.hu.entity.TreeNode;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-04
 */
public interface MenuMapper  extends BaseMapper<Menu>{


    /**
     * 查询一级，二级菜单，并将结果封装到MenuAndBtn对象中
     * @return
     */
    List<TreeNode> selectAllTree();

    /**
     * 查询一级，二级菜单，将结果封装到eleTre的HmFunctionstree对象中
     * @return
     */
    List<HmFunctionstree> selectAlleleTree();


    /**
     * 根据角色id，查询该角色所拥有的菜单
     * @param id
     * @return
     */
    Menu selectAllMenuByRoleId(@Param("menu") String menu,@Param("id") Integer id);

    /**
     * 根据角色id ， 查询菜单权限，返回树
     * @param id
     * @return
     */
    TreeNode selectTreeByRoleName(@Param("id") Integer id);



}
