package com.tourism.hu.mapper;

import com.tourism.hu.entity.ProfitSaring;
import com.tourism.hu.entity.Residence;
import com.tourism.hu.entity.dto.ProfitSaringDto;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 民宿表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface ResidenceMapper extends BaseMapper<Residence> {
	
	Long getCount(Residence dto);

	List<ProfitSaringDto> selectPageDataList(ProfitSaring dto);
}
