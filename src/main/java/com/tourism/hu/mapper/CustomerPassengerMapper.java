package com.tourism.hu.mapper;

import com.tourism.hu.entity.CustomerPassenger;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户乘车人信息表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-12
 */
public interface CustomerPassengerMapper extends BaseMapper<CustomerPassenger> {

}
