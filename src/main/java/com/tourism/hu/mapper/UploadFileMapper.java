package com.tourism.hu.mapper;

import com.tourism.hu.entity.UploadFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 上传文件记录表 Mapper 接口
 * </p>
 *
 * @author hu
 * @since 2019-12-04
 */
public interface UploadFileMapper extends BaseMapper<UploadFile> {

}
