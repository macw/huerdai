package com.tourism.hu.mapper;

import com.tourism.hu.entity.Advertisement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-19
 */
public interface AdvertisementMapper extends BaseMapper<Advertisement> {

}
