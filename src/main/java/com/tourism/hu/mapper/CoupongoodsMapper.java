package com.tourism.hu.mapper;

import com.tourism.hu.entity.Coupongoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 优惠券商品表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-30
 */
public interface CoupongoodsMapper extends BaseMapper<Coupongoods> {

}
