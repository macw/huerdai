package com.tourism.hu.mapper;

import com.tourism.hu.entity.ResidenceClass;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 民宿分类表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface ResidenceClassMapper extends BaseMapper<ResidenceClass> {

}
