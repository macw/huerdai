package com.tourism.hu.mapper;

import com.tourism.hu.entity.ScenicRoutesClass;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 旅游线路分类表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-24
 */
public interface ScenicRoutesClassMapper extends BaseMapper<ScenicRoutesClass> {

}
