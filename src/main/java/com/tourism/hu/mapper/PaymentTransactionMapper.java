package com.tourism.hu.mapper;

import com.tourism.hu.entity.PaymentTransaction;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付交易 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface PaymentTransactionMapper extends BaseMapper<PaymentTransaction> {

}
