package com.tourism.hu.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.ProfitSaring;
import com.tourism.hu.entity.dto.ProfitSaringDto;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface ProfitSaringMapper extends BaseMapper<ProfitSaring> {

	Long getCount(ProfitSaring dto);

	List<ProfitSaringDto> selectPageDataList(ProfitSaring dto);

}
