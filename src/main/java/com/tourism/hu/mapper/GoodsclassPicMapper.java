package com.tourism.hu.mapper;

import com.tourism.hu.entity.GoodsclassPic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品商品分类图片表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface GoodsclassPicMapper extends BaseMapper<GoodsclassPic> {

}
