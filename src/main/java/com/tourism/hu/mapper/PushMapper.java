package com.tourism.hu.mapper;

import com.tourism.hu.entity.Push;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface PushMapper extends BaseMapper<Push> {

}
