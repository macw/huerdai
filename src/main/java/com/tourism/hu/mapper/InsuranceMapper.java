package com.tourism.hu.mapper;

import com.tourism.hu.entity.Insurance;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 线路保险表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface InsuranceMapper extends BaseMapper<Insurance> {

}
