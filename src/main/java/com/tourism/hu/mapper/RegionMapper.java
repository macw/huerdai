package com.tourism.hu.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.tourism.hu.entity.Region;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.vo.RegionVo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface RegionMapper extends BaseMapper<Region> {

    List<RegionVo> selectRegionByPinyin();
}
