package com.tourism.hu.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.PaymentChannel;
import com.tourism.hu.entity.dto.PaymentChannelDto;

/**
 * <p>
 * 支付渠道  Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface PaymentChannelMapper extends BaseMapper<PaymentChannel> {

	/**
	 * 
	 * @Title: selectPayMethods 
	 * @Description: (渠道所有支付类型) 
	 * @return  
	 * @date 2019年9月17日 下午5:18:12
	 * @author 董兴隆
	 */
	List<String> selectPayMethods();
	
	/**
	 * 
	 * @Title: getCount 
	 * @Description: (搜索条件判断条数) 
	 * @param dto
	 * @return  
	 * @date 2019年9月17日 下午5:20:09
	 * @author 董兴隆
	 */
	Long getPaymentChannelCount(PaymentChannelDto dto);
	
	/**
	 * 
	 * @Title: selectPaymentChannel 
	 * @Description: (搜索条件获取数据) 
	 * @param dto
	 * @return  
	 * @date 2019年9月17日 下午5:20:56
	 * @author 董兴隆
	 */
	List<PaymentChannelDto> selectPaymentChannel(PaymentChannelDto dto);
	
}
