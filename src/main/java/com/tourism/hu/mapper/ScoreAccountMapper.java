package com.tourism.hu.mapper;

import com.tourism.hu.entity.ScoreAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员积分账户表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface ScoreAccountMapper extends BaseMapper<ScoreAccount> {

}
