package com.tourism.hu.mapper;

import com.tourism.hu.entity.ScenicRoutesspot;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 线路景点关联表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-25
 */
public interface ScenicRoutesspotMapper extends BaseMapper<ScenicRoutesspot> {

}
