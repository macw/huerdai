package com.tourism.hu.mapper;

import com.tourism.hu.entity.ScenicOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 旅游线路订单表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface ScenicOrderMapper extends BaseMapper<ScenicOrder> {

}
