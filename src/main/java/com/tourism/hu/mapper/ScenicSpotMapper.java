package com.tourism.hu.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.ScenicSpot;
import com.tourism.hu.entity.dto.ScenicSpotDto;

/**
 * <p>
 * 旅游景点表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface ScenicSpotMapper extends BaseMapper<ScenicSpot> {

		 Long getCount(ScenicSpotDto dto);
		 
		 List<ScenicSpotDto> selectPageListData(ScenicSpotDto dto);
}
