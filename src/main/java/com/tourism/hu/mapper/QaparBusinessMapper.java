package com.tourism.hu.mapper;

import com.tourism.hu.entity.QaparBusiness;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付交易日志表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface QaparBusinessMapper extends BaseMapper<QaparBusiness> {

}
