package com.tourism.hu.mapper;

import com.tourism.hu.entity.Invoice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 发票表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface InvoiceMapper extends BaseMapper<Invoice> {

}
