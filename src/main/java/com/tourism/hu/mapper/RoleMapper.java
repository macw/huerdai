package com.tourism.hu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.Role;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-06
 */
public interface RoleMapper extends BaseMapper<Role> {



}
