package com.tourism.hu.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.Agent;
import com.tourism.hu.entity.dto.AgentDto;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface AgentMapper extends BaseMapper<Agent> {

	Long getCount(AgentDto dto);
	
	List<AgentDto> selectPageDataList(AgentDto dto);
}
