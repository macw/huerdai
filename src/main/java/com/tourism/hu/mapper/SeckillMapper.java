package com.tourism.hu.mapper;

import com.tourism.hu.entity.Seckill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.dto.SeckillDto;

import java.util.List;

/**
 * <p>
 * 秒杀表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface SeckillMapper extends BaseMapper<Seckill> {


    /**
     * 查询 秒杀列表
     * @return
     */
    List<SeckillDto> selectSeckillDto();


}
