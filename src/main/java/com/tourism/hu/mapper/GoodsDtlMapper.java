package com.tourism.hu.mapper;

import com.tourism.hu.entity.GoodsDtl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 规格描述表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-05
 */
public interface GoodsDtlMapper extends BaseMapper<GoodsDtl> {

}
