package com.tourism.hu.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.TourOperator;
import com.tourism.hu.entity.dto.TourOperatorDto;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface TourOperatorMapper extends BaseMapper<TourOperator> {

	
	  List<TourOperatorDto> selectPageListData(TourOperatorDto dto);
	  
	  Long getCount(TourOperatorDto dto);
}
