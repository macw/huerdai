package com.tourism.hu.mapper;

import com.tourism.hu.entity.GoodsDtlImg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 规格描述表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-14
 */
public interface GoodsDtlImgMapper extends BaseMapper<GoodsDtlImg> {

}
