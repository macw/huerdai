package com.tourism.hu.mapper;

import com.tourism.hu.entity.Megtemplate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface MegtemplateMapper extends BaseMapper<Megtemplate> {

}
