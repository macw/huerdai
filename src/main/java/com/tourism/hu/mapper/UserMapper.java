package com.tourism.hu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tourism.hu.entity.User;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-05
 */
public interface UserMapper extends BaseMapper<User> {




}
