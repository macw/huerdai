package com.tourism.hu.mapper;

import com.tourism.hu.entity.WechatRegionCode;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-19
 */
public interface WechatRegionCodeMapper extends BaseMapper<WechatRegionCode> {

}
