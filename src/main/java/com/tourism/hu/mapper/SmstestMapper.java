package com.tourism.hu.mapper;

import com.tourism.hu.entity.Smstest;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 验证码登录表 Mapper 接口
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface SmstestMapper extends BaseMapper<Smstest> {

}
