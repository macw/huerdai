package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.config.Log;
import com.tourism.hu.constant.*;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.*;
import com.tourism.hu.entity.vo.*;
import com.tourism.hu.mapper.GoodsCategoryMapper;
import com.tourism.hu.mapper.GoodsMapper;
import com.tourism.hu.service.*;
import com.tourism.hu.util.GlobalConfigUtil;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 董兴隆
 * @ClassName: GoodsApiController
 * @Description: ()
 * @date 2019年11月11日 下午5:02:06
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/api/goodsApi")
public class GoodsApiController extends BaseController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private GoodsMapper goodsMapper;

    @Resource
    private IGoodsService iGoodsService;

    @Resource
    private GoodsCategoryMapper goodsCategoryMapper;

    @Resource
    private IGoodsCategoryService iGoodsCategoryService;

    @Resource
    private IGoodsEquityService iGoodsEquityService;

    @Resource
    private IEquityService iEquityService;

    @Resource
    private ISeckillService iSeckillService;

    @Resource
    private IGoodsDtlService iGoodsDtlService;

    @Resource
    private IGoodsDtlImgService iGoodsDtlImgService;

    @Resource
    private IGoodsPicService iGoodsPicService;

    @Resource
    private IBaseGoods iBaseGoods;

    @Resource
    private IBaseService iBaseService;

    /**
     * 商品分类列表展示接口 （暂时不用）
     * <p>
     * 预留优惠券数据
     * 预留商品返利，返利价格显示
     * 预留正在拼团数量数据显示
     * 预留拼团人数头像数据显示
     */
    @RequestMapping("/GoodsCategoryList")
    public void GoodsCategoryList() {
        try {
            List<GoodsCategoryVo> GoodsCategoryVos = goodsCategoryMapper.selectGoodsCategoryList();
            for (GoodsCategoryVo GoodsCategoryVo : GoodsCategoryVos) {
                List<GoodsVo> GoodsVos = GoodsCategoryVo.getGoodsLists();
                for (GoodsVo goodsList : GoodsVos) {
                    //计算用户的省钱数值
                    BigDecimal percentage = getPercentage(goodsList.getPrice(), goodsList.getExitPrice(), goodsList.getActivityPrice());
                    //赋值,并四色五入只保留两位小数，)     四舍五入如，2.35变成2.4
                    goodsList.setSavePrice(percentage);
                    goodsList.setExitPrice(BigDecimal.ZERO);
                }

            }

            ResponseResult<List<GoodsCategoryVo>> result = new ResponseResult<>();
            result.setData(GoodsCategoryVos);
            result.setMsg("查询成功！");
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("GoodsCategoryList Exception==="+e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
        }
    }

    /**
     * 查询商品分类
     */
    @RequestMapping("/selectGoodsCategory")
    public void selectGoodsCategory() {
        try {
            List<GoodsCategoryVo> goodsCategoryVoList = goodsCategoryMapper.selectCategoryList();
            for (GoodsCategoryVo goodsCategoryVo : goodsCategoryVoList) {
                goodsCategoryVo.setLink(BaseConstant.OS_PATH + "api/goodsApi/selectGoodsListByCategory/" + goodsCategoryVo.getClassId());
            }
            ResponseResult<List<GoodsCategoryVo>> result = new ResponseResult<>();
            result.setData(goodsCategoryVoList);
            result.setMsg("查询成功！");
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug(e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
        }
    }

    /**
     * 根据分类id查询，该分类下的商品
     * @param classId
     */
    @RequestMapping("/selectGoodsListByCategory/{classId}")
    public void selectGoodsListByCategory(@PathVariable Integer classId,Integer page,Integer limit) {
        try {
            Page<Goods> goodsPage = new Page<>(page,limit);
            QueryWrapper<Goods> queryWrapper = new QueryWrapper<>();
            queryWrapper.select("goods_id goodsId","goodsname","price", "exit_price exitPrice", "activity_price activityPrice",
                    "goodsclassid ","goodsclassid","descript","picture_url pictureUrl")
                    .eq("status", BaseConstant.USE_STATUS)
                    .eq("goodsclassid", classId);
            IPage<Map<String, Object>> mapIPage = iGoodsService.pageMaps(goodsPage, queryWrapper);
            List<Map<String, Object>> records = mapIPage.getRecords();
            for (Map<String, Object> record : records) {
                //计算用户的省钱数值
                BigDecimal percentage = getPercentage((BigDecimal)record.get("price"), (BigDecimal)record.get("exitPrice"),(BigDecimal) record.get("activityPrice"));
                //赋值,并四色五入只保留两位小数，)     四舍五入如，2.35变成2.4
                record.put("savePrice", percentage);
                //将出厂价设为0
                record.put("exitPrice",0);
            }
            ResponseResult<List<Map<String, Object>>> result = new ResponseResult<>();
            result.setData(records);
            result.setMsg("查询成功！");
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("classId===" + classId);
            logger.debug(e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("查询失败，请重试"));
        }
    }


    /**
     * 查询单个商品详情信息
     *
     * @param goodsId
     */
    @Log(value = "查询商品详情")
    @RequestMapping("/GoodsDetails")
    public void GoodsDetails(Integer goodsId) {
        try {
            GoodsDetailsVo goodsDetails = goodsMapper.selectGoodsDetails(goodsId);
            //商品规格list
            List<GoodsDtl> dtlList = iGoodsDtlService.list(new QueryWrapper<GoodsDtl>().eq("goods_id", goodsId));
            goodsDetails.setIsDtl(1);
            if(dtlList==null || dtlList.isEmpty()) {  //如果查询商品的规格为空 则返回是无规格商品
            	goodsDetails.setIsDtl(0);
            }
            //如果不是玩赚商品，则计算返利金额
            if (GoodsCategoryConstant.WANZID !=goodsDetails.getGoodsclassid()){
                //获取用户对于该商品的返利价格,   商品单价-出厂价
                BigDecimal percentage = getPercentage(goodsDetails.getPrice(), goodsDetails.getExitPrice(), goodsDetails.getActivityPrice());
                goodsDetails.setSavePrice(percentage);

                //上级利润金额
                BigDecimal servicePercentage = iBaseService.getPercentage(2, goodsDetails.getPrice(), goodsDetails.getExitPrice(), goodsDetails.getActivityPrice(), BaseConstant.PARE_TYPE);
                //分享赚
                goodsDetails.setSharePrice(servicePercentage);
            }
            //计算拼团节省价格
            Seckill seckill = iSeckillService.getOne(new QueryWrapper<Seckill>().ge("collage_count", 2).eq("type", SeckillConstant.GOODSID).eq("goods_id", goodsId).eq("status", BaseConstant.USE_STATUS));
            BigDecimal percentageGroup = null;
            if (seckill != null) {
                percentageGroup = getPercentage(goodsDetails.getPrice(), goodsDetails.getExitPrice(), seckill.getSeckillPrice());
            }
            //设置 商品信息的 权益
            List<Equity> list = new ArrayList<>();
            List<GoodsEquity> equityList = iGoodsEquityService.list(new QueryWrapper<GoodsEquity>().eq("goods_id", goodsId));
            for (GoodsEquity goodsEquity : equityList) {
                Equity equity = iEquityService.getOne(new QueryWrapper<Equity>().eq("equity_id", goodsEquity.getEquityId()).eq("status", BaseConstant.USE_STATUS));
                list.add(equity);
            }
            goodsDetails.setEquityList(list);
            goodsDetails.setSaveGroupPrice(percentageGroup);
            List<GoodsPic> goodsPicList = iGoodsPicService.list(new QueryWrapper<GoodsPic>().eq("goods_id", goodsId).eq("type", GoodsPicConstant.LOGO_TYPE).orderByDesc("leavel"));
            goodsDetails.setPictureTitelList(goodsPicList);
            ResponseResult result = new ResponseResult();
            result.setData(goodsDetails);
            result.setMsg("查询成功！");
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
        }
    }

    /**
     * 查询/选择
     * 商品的规格
     *
     * @param goodsId
     * @param dtlIdArray
     */
    @Log("查询商品规格")
    @RequestMapping("/selectGoodsDtlVo")//@RequestParam("dtlIdArray[]")
    public void selectGoodsDtlVo(Integer goodsId,@RequestParam(value = "dtlIdArray[]",required = false)List<Integer> dtlIdArray) {
        //创建返回实体
    	Map<String,Object> map = new HashMap<String,Object>();
    	ResponseResult<Map<String,Object>>  result =  ResponseResult.ERROR("商品信息不存在");
        GoodsDtlVo goodsDtlVo = new GoodsDtlVo();
        Goods goods = iGoodsService.getById(goodsId);
        if (goods == null) {
            ResponseResult.Step(result);
        }
        /**
         * 计算商品的实际价格
         */
        BigDecimal actualPrice = iBaseGoods.getActualPrice(goods.getPrice(), goods.getActivityPrice());
        //如果没选择规格
        if (dtlIdArray == null || dtlIdArray.isEmpty()){
            map.put("goodsDtlPrice", actualPrice);
            map.put("dtlPicUrl", goods.getPictureUrl());
            List<GoodsDtlType> goodsDtlTypeList = new ArrayList<>();
            List<GoodsDtl> dtlList = iGoodsDtlService.list(new QueryWrapper<GoodsDtl>().eq("goods_id", goodsId).groupBy("type"));
            for (GoodsDtl goodsDtl : dtlList) {
                goodsDtlTypeListAdd(goodsDtlTypeList, goodsDtl,GoodsDtlConstant.MODEL_TYPE,"型号");
            }
            for (GoodsDtl goodsDtl : dtlList) {
            	goodsDtlTypeListAdd(goodsDtlTypeList, goodsDtl,GoodsDtlConstant.COLOR_TYPE,"颜色");
            }
            map.put("goodsDtlTypes", goodsDtlTypeList);
        }else {
        	//选择了规格查询的
            map.put("dtlPicUrl", "");
            Collection<GoodsDtl> goodsList = iGoodsDtlService.listByIds(dtlIdArray);
            dtlSaveMap(goodsList, actualPrice, map);
        }
    	result = ResponseResult.SUCCESS("查询成功");
        result.setData(map);
        ResponseResult.Step(result);
    }

	private void dtlSaveMap(Collection<GoodsDtl> goodsList, BigDecimal actualPrice, Map<String, Object> map) {
		List<String> stringList = new ArrayList<String>();
		if(goodsList==null||goodsList.isEmpty()) {
			return ;
		}
		for (GoodsDtl goodsDtl : goodsList) {
			if(goodsDtl.getType()==1) {
				 GoodsDtlImg goodsDtlImg = iGoodsDtlImgService.getOne(new QueryWrapper<GoodsDtlImg>().eq("goodsdtl_id", goodsDtl.getGoodsdtlId()));
			}
			stringList.add(goodsDtl.getSpec());
			actualPrice=actualPrice.add(goodsDtl.getDtlPrice());
		}
		map.put("DtlStringList", stringList);
		map.put("actualPrice", actualPrice);
	}

	private void goodsDtlTypeListAdd(List<GoodsDtlType> goodsDtlTypeList, GoodsDtl goodsDtl,Integer type,String typeName) {
		if (goodsDtl.getType()==type){
		    GoodsDtlType goodsDtlType = new GoodsDtlType();
		    goodsDtlType.setName(typeName);
		    goodsDtlType.setType(type);
		    List<GoodsDtl> goodsType = iGoodsDtlService.list(new QueryWrapper<GoodsDtl>().eq("type",type));
		    for (GoodsDtl dtl : goodsType) {
		        GoodsDtlImg one = iGoodsDtlImgService.getOne(new QueryWrapper<GoodsDtlImg>().eq("goodsdtl_id", dtl.getGoodsdtlId()).eq("status", BaseConstant.USE_STATUS));
		        if (one!=null){
		            dtl.setClassimgAdress(one.getClassimgAdress());
		        }
		    }
		    goodsDtlType.setDtlVoList(goodsType);
		    goodsDtlTypeList.add(goodsDtlType);
		}
	}
}
