package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.config.Log;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.Wallet;
import com.tourism.hu.service.IWalletService;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description: 钱包表，个人收益记录
 * @date 10:46
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/api/walltApi")
public class WalltApiController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IWalletService iWalletService;


    @Log(value = "查询个人收益", type = "select")
    @RequestMapping("/selectWallt")
    public void selectWallt() {
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录"));
            return;
        }
        //如果当前用户没有钱包，则创建一个
        Wallet wallet = iWalletService.getOne(new QueryWrapper<Wallet>().eq("cus_id", customerInfo.getCustomerId()));


        if (wallet == null) {
            wallet = new Wallet();
            wallet.setCusId(customerInfo.getCustomerId());
            wallet.setCusId(customerInfo.getCustomerId());
            wallet.setCreatedName(customerInfo.getCustomerName());
            wallet.setCreationDate(LocalDateTime.now());
            iWalletService.save(wallet);
        }
        logger.debug("查询订单收益===" + wallet);
        //查询总订单数
        Wallet selectGoodsOrderProfit = null;
        try {
            selectGoodsOrderProfit = iWalletService.selectGoodsOrderProfit(customerInfo.getCustomerId());
            logger.debug("查询已有订单收益----------------"+selectGoodsOrderProfit);
            //如果库里的订单总数 和 查询出来的订单总数一致，则不往下查询，直接返回钱包对象
            if (wallet.getOrderNumber()!=null && selectGoodsOrderProfit.getOrderNumber()!=null){
                if (wallet.getOrderNumber()==selectGoodsOrderProfit.getOrderNumber()){
                    ResponseResult result = ResponseResult.SUCCESS("查询成功");
                    result.setData(wallet);
                    ResponseResult.Step(result);
                    return;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"+e.getMessage()));
        }
        logger.debug("11111111111111111111111111111111111111111111111111");
        try {
            //设置总订单数
            wallet.setOrderNumber(selectGoodsOrderProfit.getOrderNumber());
            //设置预估结算总收益
            wallet.setReadyProfit(selectGoodsOrderProfit.getReadyProfit());
            //设置本月消费预估收入
            BigDecimal localMonthConsume = iWalletService.getLocalMonthConsume(customerInfo.getCustomerId(), 0);
            logger.debug("localMonthConsume===" + localMonthConsume);
            wallet.setMonthProfit(localMonthConsume);
            //设置上月预估消费收入
            BigDecimal localMonthConsume1 = iWalletService.getLocalMonthConsume(customerInfo.getCustomerId(), -1);
            wallet.setFrontMonthProfit(localMonthConsume1);
            logger.debug("localMonthConsume1===" + localMonthConsume1);
            //设置本月预估结算收入
            //获取今天几号
            int day = LocalDateTime.now().getDayOfMonth();
            //如果小于等于20，则显示为0，否则显示为上月消费预估收益
            if (day <= 20) {
                wallet.setMonthEndProfit(BigDecimal.ZERO);
            } else {
                wallet.setMonthEndProfit(localMonthConsume1);
            }

            //设置上月预估消费结算收益
            //为上上月消费预估结算收益
            wallet.setFrontMonthEndProfit(iWalletService.getLocalMonthConsume(customerInfo.getCustomerId(), -2));
            logger.debug("------------------------setFrontMonthEndProfit");
            if (wallet.getEndProfit() == null) {
                wallet.setEndProfit(BigDecimal.ZERO);
            }
            boolean saveOrUpdate = iWalletService.saveOrUpdate(wallet);
            if (saveOrUpdate){
                ResponseResult result = ResponseResult.SUCCESS("查询成功！");
                result.setData(wallet);
                ResponseResult.Step(result);
                return;
            }else {
                ResponseResult.Step(ResponseResult.ERROR("查询失败，更新异常"));
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("查询异常错误！");
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"+e.getMessage()));
        }


    }


}
