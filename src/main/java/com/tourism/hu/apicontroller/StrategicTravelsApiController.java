package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.StrategicTravels;
import com.tourism.hu.service.ICustomerInfoService;
import com.tourism.hu.service.IStrategicTravelsService;
import com.tourism.hu.util.DateConverterUtil;
import com.tourism.hu.util.GlobalConfigUtil;
import com.tourism.hu.util.ResponseResult;
import com.tourism.hu.util.base64ToMultipartUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 15:32
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/api/strategicTravelsApi")
public class StrategicTravelsApiController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IStrategicTravelsService iStrategicTravelsService;

    @Resource
    private ICustomerInfoService iCustomerInfoService;

    @Resource
    private RedisTemplate redisTemplate;

    /**
     * 添加旅游攻略
     * 参数：
     * sTitle 标题
     * sContent 内容
     * @param strategicTravels
     */
    @RequestMapping("/addOrUpdateStrategicTravels")
//    public void addOrUpdateStrategicTravels(@RequestParam(value = "file",required = false)MultipartFile file, StrategicTravels strategicTravels) {
    public void addOrUpdateStrategicTravels(String base64, StrategicTravels strategicTravels,String sessionId) {
        logger.debug("base64==========="+base64);

        MultipartFile file = null;
        try {
            file = base64ToMultipartUtil.base64ToMultipart(base64);
        } catch (Exception e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR("图片上传失败！"));
            return;
        }

        CustomerInfo customerInfo = getCustomerInfo();
        if (sessionId!=null){
            Object obj = redisTemplate.opsForValue().get(sessionId);
            String customerId = "";
            if(obj!=null) {
                customerId=obj.toString();
            }
            customerInfo = iCustomerInfoService.getOne(new QueryWrapper<CustomerInfo>().eq("id", customerId));
        }

        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录！"));
            return;
        }
        logger.debug("发布攻略：strategicTravels===" + strategicTravels);
        if (file != null && file.getSize() > 0) {
            logger.debug("file====" + file);
            String basePath = "strategic/travels";
            String url = "";
            //获取上传文件的路径
            try {
            	url = aliOSSUpload(file, basePath);
            } catch (IOException e) {
                e.printStackTrace();
                logger.debug("IOException===" + e.getMessage());

            }
            strategicTravels.setStragePictureUrl(url);
        }
        strategicTravels.setCustomerId(customerInfo.getCustomerId());
        strategicTravels.setStatus(BaseConstant.USE_STATUS);

        if (strategicTravels.getStrageId() == null) {
            strategicTravels.setCreatedId(customerInfo.getCustomerId());
            strategicTravels.setCreationDate(LocalDateTime.now());
            strategicTravels.setCreatedName(customerInfo.getCustomerName());
            if (iStrategicTravelsService.save(strategicTravels)) {
                ResponseResult.Step(ResponseResult.SUCCESS("发布成功！"));
            } else {
                ResponseResult.Step(ResponseResult.ERROR("发布失败！"));
            }
        } else {
            strategicTravels.setLastUpdateDate(LocalDateTime.now());
            strategicTravels.setLastUpdatedId(customerInfo.getCustomerId());
            strategicTravels.setLastUpdateName(customerInfo.getNickname());
            if (iStrategicTravelsService.updateById(strategicTravels)) {
                ResponseResult.Step(ResponseResult.SUCCESS("更新成功！"));
            } else {
                ResponseResult.Step(ResponseResult.ERROR("更新失败！"));
            }
        }
    }

    /**
     * 添加，或者更新 攻略
     * 非跨域请求
     * @param base64q
     * @param strategicTravels
     * @param sessionId
     * @return
     */
    @RequestMapping("/addOrUpdateTravelsPost")
    public ResponseResult addOrUpdateTravelsPost(String base64, StrategicTravels strategicTravels,String sessionId) {
        logger.debug("base64==========="+base64);

        MultipartFile file = null;
        try {
            file = base64ToMultipartUtil.base64ToMultipart(base64);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseResult.ERROR("图片上传失败！");
        }

        CustomerInfo customerInfo = getCustomerInfo();
        if (sessionId!=null){
            Object obj = redisTemplate.opsForValue().get(sessionId);
            String customerId = "";
            if(obj!=null) {
                customerId=obj.toString();
            }
            customerInfo = iCustomerInfoService.getOne(new QueryWrapper<CustomerInfo>().eq("id", customerId));
        }

        if (customerInfo == null) {
            return ResponseResult.ERROR("获取用户信息失败，请重新登录！");
        }
        logger.debug("发布攻略：strategicTravels===" + strategicTravels);
        if (file != null && file.getSize() > 0) {
            logger.debug("file====" + file);
            //获取上传文件的路径
            String basePath = "strategic/travels";
            String url = "";
            try {
//                url = upload(file, realPath);
            	 url = aliOSSUpload(file, basePath);
                logger.debug("url===" + url);
            } catch (IOException e) {
                e.printStackTrace();
                logger.debug("IOException===" + e.getMessage());

            }
            strategicTravels.setStragePictureUrl(url);
        }
        strategicTravels.setCustomerId(customerInfo.getCustomerId());
        strategicTravels.setStatus(BaseConstant.USE_STATUS);

        if (strategicTravels.getStrageId() == null) {
            strategicTravels.setCreatedId(customerInfo.getCustomerId());
            strategicTravels.setCreationDate(LocalDateTime.now());
            strategicTravels.setCreatedName(customerInfo.getCustomerName());
            if (iStrategicTravelsService.save(strategicTravels)) {
                return ResponseResult.SUCCESS("发布成功！");
            } else {
                return ResponseResult.ERROR("发布失败！");
            }
        } else {
            strategicTravels.setLastUpdateDate(LocalDateTime.now());
            strategicTravels.setLastUpdatedId(customerInfo.getCustomerId());
            strategicTravels.setLastUpdateName(customerInfo.getNickname());
            if (iStrategicTravelsService.updateById(strategicTravels)) {
                return ResponseResult.SUCCESS("更新成功！");
            } else {
                return ResponseResult.ERROR("更新失败！");
            }
        }
    }




    /**
     * 查询所有的攻略
     */
    @RequestMapping("/selectStrategicTravels")
    public void selectStrategicTravels(Integer page,Integer limit) {
        try {
            ResponseResult<List<Map<String, Object>>> result = new ResponseResult<>();
            Page<StrategicTravels> page1 = new Page<>(page, limit);
            logger.debug("page===="+page+",---limit==="+limit);

            QueryWrapper<StrategicTravels> qw = new QueryWrapper<StrategicTravels>();
            qw.select("strage_id strageId", "strage_title strageTitle", "strage_picture_url stragePictureUrl", "likecount","customer_id customerId", "praisecount").eq("status", BaseConstant.USE_STATUS).orderByDesc("last_update_date", "creation_date");
//            List<Map<String, Object>> mapList = iStrategicTravelsService.listMaps(qw);

            IPage<Map<String, Object>> travelsIPage = iStrategicTravelsService.pageMaps(page1, qw);
            List<Map<String, Object>> mapList = travelsIPage.getRecords();
            System.out.println("+++count="+mapList.size()+"——————"+mapList);
            for (Map<String, Object> map : mapList) {
//                map.put("stragePictureUrl", GlobalConfigUtil.getServiceUrl() + isNullNotAppend(map.get("stragePictureUrl")));
                map.put("stragePictureUrl", isNullNotAppend(map.get("stragePictureUrl")));
                CustomerInfo customerInfo = iCustomerInfoService.getById((Serializable) map.get("customerId"));
                if (customerInfo!=null){
                    map.put("nickname", customerInfo.getNickname());
                    map.put("customerIcon", customerInfo.getCustomerIcon());
                }
            }
            result.setData(mapList);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Exception===" + e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
        }
    }

    /**
     * 查询单个攻略
     * 详情
     *
     * @param strageId
     */
    @RequestMapping("/getDetailTravels")
    public void getDetailTravels(Integer strageId) {
        ResponseResult<Map<String, Object>> result = ResponseResult.ERROR("未找到该攻略");
        Map<String, Object> map = new HashMap<>();
        StrategicTravels strategicTravels = iStrategicTravelsService.getOne(new QueryWrapper<StrategicTravels>().eq("strage_id", strageId));
        if (strategicTravels == null) {
            result.setData(map);
            ResponseResult.Step(result);
            return;
        }
        CustomerInfo customerInfo = iCustomerInfoService.getById(strategicTravels.getCustomerId());
        map.put("strageTitle", strategicTravels.getStrageTitle());
        map.put("creationDate", DateConverterUtil.dateFormat(strategicTravels.getCreationDate(), DateConverterUtil.date));
        map.put("praisecount", strategicTravels.getPraisecount());
        map.put("likecount", strategicTravels.getLikecount());
        map.put("readingcount", strategicTravels.getReadingcount());
        map.put("forwardcount", strategicTravels.getForwardcount());
        map.put("strageId", strategicTravels.getStrageId());
        map.put("nickname", "");
        map.put("customerIcon", "");
        if (customerInfo != null) {
            map.put("nickname", customerInfo.getNickname());
            map.put("customerIcon", customerInfo.getCustomerIcon());
        }
//        map.put("stragePictureUrl", GlobalConfigUtil.getServiceUrl() + isNullNotAppend(strategicTravels.getStragePictureUrl()));
        map.put("stragePictureUrl", isNullNotAppend(strategicTravels.getStragePictureUrl()));
        map.put("strageContent", strategicTravels.getStrageContent());
        result.setOkMsg("查询成功");
        result.setData(map);
        ResponseResult.Step(result);
    }


    /**
     * 点赞数增加或者减少
     *
     * @param strageId
     * @param type     1增加  0减少
     */
    @RequestMapping(value = "/stragePraise")
    public void increaseLikeCount(Integer strageId, Integer type) {
        StrategicTravels strategicTravels = iStrategicTravelsService.getById(strageId);
        String msg = null;
        if (type == 1) {
            msg = "点赞";
            strategicTravels.setPraisecount((strategicTravels.getPraisecount() + 1));
        } else if (type == 0) {
            msg = "取消点赞";
            strategicTravels.setPraisecount((strategicTravels.getPraisecount() - 1));
        }
        logger.debug("点赞量=="+strategicTravels.getPraisecount());
        if (iStrategicTravelsService.updateById(strategicTravels)) {
            ResponseResult result = ResponseResult.SUCCESS(msg+"成功");
            result.setData(strategicTravels.getPraisecount());
            ResponseResult.Step(result);
        } else {
            ResponseResult.Step(ResponseResult.ERROR("失败"));
            logger.debug("点赞文章id：strageId==="+strageId);
        }
    }

    //文章访问量数增加
    @RequestMapping(value = "/addReading")
    public void increaseViewCount(Integer strageId) {
        StrategicTravels strategicTravels = iStrategicTravelsService.getById(strageId);
        strategicTravels.setReadingcount((strategicTravels.getReadingcount() + 1));
        logger.debug("访问量："+strategicTravels.getReadingcount());
        if (iStrategicTravelsService.updateById(strategicTravels)) {
            ResponseResult result = ResponseResult.SUCCESS("成功");
            result.setData(strategicTravels.getReadingcount());
            ResponseResult.Step(result);
        } else {
            ResponseResult.Step(ResponseResult.ERROR("失败"));
            logger.debug("访问文章id：strageId==="+strageId);
        }
    }
}
