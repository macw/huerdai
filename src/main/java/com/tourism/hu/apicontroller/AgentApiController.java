package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.entity.Agent;
import com.tourism.hu.service.IAgentService;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 
 * @ClassName: AgentApiController 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:01:06 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/api/AgentApi")
public class AgentApiController {
	
	
    private  Logger logger = LoggerFactory.getLogger(getClass());


	@Autowired
	private IAgentService iAgentService;

    /**
     *
     * @param 
     */
	@RequestMapping("/selectAgent")
	public void selectAgent() {
		ResponseResult<List<Agent>> result = new ResponseResult<>();
		try {
			QueryWrapper<Agent> qw= new QueryWrapper<Agent>();
			
			List<Agent> list = iAgentService.list(qw);
			result.setOkMsg(list);
		}catch (Exception e) {
			result.setErrorMsg("查询异常");
		}
		ResponseResult.Step(result);
	}




}
