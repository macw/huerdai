package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.entity.ScoreAccount;
import com.tourism.hu.mapper.ScoreAccountMapper;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
/**
 * 
 * @ClassName: ScoreAccountApiController 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:03:29 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/api/ScoreAccountApi")
public class ScoreAccountApiController {
	
	
    private  Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private ScoreAccountMapper scoreAccountMapper;


    /**
     * @param
     */
    @RequestMapping("/selectScoreAccount")
    public void ScoreAccount() {
        try {
            List<ScoreAccount> goodsComments = scoreAccountMapper.selectList(new QueryWrapper<ScoreAccount>());
            ResponseResult<List<ScoreAccount>> result = new ResponseResult<>();
            result.setData(goodsComments);
            ResponseResult.Step(result);
        } catch (BeansException e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR);
        }

    }




}
