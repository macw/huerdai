package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.GoodsPicConstant;
import com.tourism.hu.entity.GoodsPic;
import com.tourism.hu.mapper.GoodsPicMapper;
import com.tourism.hu.util.GlobalConfigUtil;
import com.tourism.hu.util.ResponseResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
/**
 * 
 * @ClassName: GoodsPicApiController 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:02:17 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/api/goodsPicApi")
public class GoodsPicApiController {

    @Resource
    private GoodsPicMapper goodsPicMapper;

    /**
     * 商品详情页图片
     * @param goodsId
     */
    @RequestMapping("/selectGoodsPicList")
    public void selectGoodsPicList(Integer goodsId){
        try {
            List<GoodsPic> goodsPics = goodsPicMapper.selectList(new QueryWrapper<GoodsPic>().eq("goods_id", goodsId)
                    .eq("type", GoodsPicConstant.DESC_TYPE)
                    .orderByDesc("leavel"));
            ResponseResult<List<GoodsPic>> result = new ResponseResult<>();
            result.setData(goodsPics);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR);
        }

    }

}
