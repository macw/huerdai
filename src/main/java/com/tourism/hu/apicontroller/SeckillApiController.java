package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.CusseckillConstant;
import com.tourism.hu.constant.SeckillConstant;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.Cusseckill;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.Seckill;
import com.tourism.hu.entity.dto.SeckillDto;
import com.tourism.hu.entity.vo.CusseckillVo;
import com.tourism.hu.mapper.CusseckillMapper;
import com.tourism.hu.mapper.CustomerInfoMapper;
import com.tourism.hu.mapper.GoodsMapper;
import com.tourism.hu.mapper.SeckillMapper;
import com.tourism.hu.service.ICusseckillService;
import com.tourism.hu.service.ISeckillService;
import com.tourism.hu.util.DateConverterUtil;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 董兴隆
 * @ClassName: SeckillApiController
 * @Description: ()
 * @date 2019年11月11日 下午5:03:34
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/api/seckillApi")
public class SeckillApiController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());


    @Resource
    private SeckillMapper seckillMapper;

    @Resource
    private ISeckillService iSeckillService;

    @Resource
    private CusseckillMapper cusseckillMapper;

    @Resource
    private ICusseckillService iCusseckillService;

    @Resource
    private GoodsMapper goodsMapper;

    @Resource
    private CustomerInfoMapper customerInfoMapper;

    /**
     * 查询秒杀商品列表
     *
     * @return
     */
    @RequestMapping("/selectSeckillList")
    public void selectSeckillList() {
        try {
            List<SeckillDto> seckillDtoList = seckillMapper.selectSeckillDto();
            for (SeckillDto seckillDto : seckillDtoList) {
                if (seckillDto.getSeckillBegintime() != null) {
                    String validityStartdate = DateConverterUtil.dateFormat(seckillDto.getSeckillBegintime(), DateConverterUtil.dateTime);
                    seckillDto.setBegintime(validityStartdate);
                }
                if (seckillDto.getSeckillEndtime() != null) {
                    String validityEnddate = DateConverterUtil.dateFormat(seckillDto.getSeckillBegintime(), DateConverterUtil.dateTime);
                    seckillDto.setEndtime(validityEnddate);
                }
            }
            ResponseResult<List<SeckillDto>> result = new ResponseResult<>();
            result.setData(seckillDtoList);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR);
        }
    }


    /**
     * Cusseckill的status 代表的状态是是否正在参与拼团
     * 1，正在参与的拼团， 0代表参与过的拼团
     *
     * 发起拼团，需先选择商品规格，并付款后再调用创建拼团
     * 查看拼团列表，查看团长列表，和待拼团成功人数
     * 参与拼团 ， 参与成功将该团下的所有用户信息状态设为0
     *
     *
     */



    /**
     * 发起拼团
     * 在商品的团下，添加一条用户数据，cusseckill
     *
     * @param goodsId
     */
    @RequestMapping("/toAddSeckillGroup")
    public void toAddSeckillGroup(Integer goodsId) {
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录"));
        } else {
            try {
                /**
                 * 查询，拼团表，根据商品id，
                 * 拼团人数大于等于2，类型type为1商品
                 * 状态为启用
                 */
                Seckill seckill = seckillMapper.selectOne(new QueryWrapper<Seckill>().ge("collage_count", 2).eq("type", SeckillConstant.GOODSID).eq("goods_id", goodsId).eq("status", BaseConstant.USE_STATUS));
                if (seckill==null){
                    ResponseResult.Step(ResponseResult.ERROR("该商品下暂无拼团信息"));
                }
                Cusseckill cusseckill1 = cusseckillMapper.selectOne(new QueryWrapper<Cusseckill>().eq("cus_id", customerInfo.getCustomerId()).eq("spot_id", seckill.getSeckillId()).eq("status", BaseConstant.USE_STATUS));
                if (cusseckill1 != null) {
                    ResponseResult.Step(ResponseResult.ERROR("您已经参与过该商品的拼团，请勿重新参与"));
                } else {
                    Cusseckill cusseckill = new Cusseckill();
                    cusseckill.setSeckillgroup("" + goodsId + getUUID());
                    cusseckill.setCusId(customerInfo.getCustomerId());
                    cusseckill.setSpotId(seckill.getSeckillId());
                    cusseckill.setStatus(BaseConstant.USE_STATUS);
                    cusseckill.setCreatedId(customerInfo.getCustomerId());
                    cusseckill.setCreationDate(LocalDateTime.now());
                    cusseckill.setCreatedName(customerInfo.getNickname());
                    cusseckill.setMemo(CusseckillConstant.TUZHANG);
                    boolean save = iCusseckillService.save(cusseckill);
                    if (save){
                        ResponseResult.Step(ResponseResult.SUCCESS("创建拼团成功！"));
                        return;
                    }else {
                        ResponseResult.Step(ResponseResult.ERROR("创建拼团失败！"));
                        return;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                ResponseResult.Step(ResponseResult.ERROR("创建拼团失败，请重新操作"));
            }
        }

    }


    /**
     * 查询当前商品下的正在拼团人数详情
     *
     * @param goodsId
     */
    @RequestMapping("/selectSeckillGroupByGoodsId")
    public void selectSeckillGroupByGoodsId(Integer goodsId) {
        logger.debug("goodsID==="+goodsId);
       /* CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录"));
            return;
        }
        logger.debug("customer===="+customerInfo.toString());*/
        try {
            /**
             * 查询，拼团表，根据商品id，
             * 拼团人数大于等于2，类型type为1商品
             * 状态为启用
             */
            Seckill seckill = seckillMapper.selectOne(new QueryWrapper<Seckill>().ge("collage_count", 2).eq("type", SeckillConstant.GOODSID).eq("goods_id", goodsId).eq("status", BaseConstant.USE_STATUS));
            if (seckill==null){
                ResponseResult.Step(ResponseResult.ERROR("该商品下暂无拼团信息"));
                return;
            }
            //商品下的拼团列表
            List<Cusseckill> cusseckillVo = cusseckillMapper.selectList(new QueryWrapper<Cusseckill>().eq("memo", CusseckillConstant.TUZHANG).eq("status", BaseConstant.USE_STATUS).eq("spot_id", seckill.getSeckillId()));
            if (cusseckillVo==null){
                ResponseResult.Step(ResponseResult.ERROR("参与拼团人数为空"));
                return;
            }
            List<CusseckillVo> CusseckillVoList = new ArrayList<>();
            for (Cusseckill cusseckill : cusseckillVo) {
                //参与拼团的用户
                CustomerInfo customerInfo1 = customerInfoMapper.selectById(cusseckill.getCusId());
                //返回拼团列表对象
                CusseckillVo vo = new CusseckillVo();
                vo.setNickname(customerInfo1.getNickname());
                vo.setCustomerIcon(customerInfo1.getCustomerIcon());
                vo.setCustomerId(customerInfo1.getCustomerId());
                vo.setFzCusseckillId(seckill.getSeckillId());
                vo.setSeckillgroup(cusseckill.getSeckillgroup());
                //对拼团时间赋值
                if (seckill.getSeckillBegintime() != null) {
                    String validityStartdate = DateConverterUtil.dateFormat(seckill.getSeckillBegintime(), DateConverterUtil.dateTime);
                    vo.setBegintime(validityStartdate);
                }
                if (seckill.getSeckillEndtime() != null) {
                    String validityEnddate = DateConverterUtil.dateFormat(seckill.getSeckillBegintime(), DateConverterUtil.dateTime);
                    vo.setEndtime(validityEnddate);
                }
                logger.debug("seckill.getCollageCount()===" + seckill.getCollageCount());
                logger.debug("CusseckillVo.size()===" + cusseckillVo.size());
                /**
                 * 根据团长的拼团组，重新查询该团组下面的人数
                 * 要计算该团长下面的剩余数量
                 */
                List<Cusseckill> seckillgroup = iCusseckillService.list(new QueryWrapper<Cusseckill>().eq("seckillgroup", cusseckill.getSeckillgroup()));
                Integer number = (seckill.getCollageCount() - seckillgroup.size());
                vo.setNumber(number);
                CusseckillVoList.add(vo);
            }
            ResponseResult<List<CusseckillVo>> result = new ResponseResult<>();
            result.setData(CusseckillVoList);
            result.setMsg("查询成功！");
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("goodsId=="+goodsId);
            logger.debug("Exception=="+e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("系统异常，查询失败"));
        }
    }

    /**
     * 参与拼团接口
     *
     * @param seckillgroup   团长用户id
     * @param fzCusseckillId 拼图列表主键id
     */
    @RequestMapping("/toJoinSeckillGroup")
    public void toJoinSeckillGroup(String seckillgroup, Integer fzCusseckillId) {
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录"));
        } else {

                //判断登录用户是不是已拼团的团员
                //判断该登录使用是不是对一个商品已经参与过拼团
                /**
                 * 登录用户：customerInfo
                 * 对于一个商品的拼团，spotid，
                 */
                Cusseckill cusseckill2 = cusseckillMapper.selectOne(new QueryWrapper<Cusseckill>().eq("cus_id", customerInfo.getCustomerId()).eq("spot_id", fzCusseckillId).eq("status", BaseConstant.USE_STATUS));
                if (cusseckill2 != null) {
                    ResponseResult.Step(ResponseResult.ERROR("您已有团购，请勿重新参与"));
                    return;
                }

                //如果参与拼团的人数已经满了，则。。。返回团满不能重复参与
                List<Cusseckill> CusseckillVo = cusseckillMapper.selectList(new QueryWrapper<Cusseckill>().eq("seckillgroup", seckillgroup));
                Seckill seckill = seckillMapper.selectById(fzCusseckillId);
                logger.debug("getCollageCount===" + seckill.getCollageCount());
                logger.debug("CusseckillVo.size()===" + CusseckillVo.size());
                if ((seckill.getCollageCount() - CusseckillVo.size()) == 0) {
                    ResponseResult.Step(ResponseResult.ERROR("该团已满，不能再参加"));
                    return;
                }


            try {
                //创建拼团
                Cusseckill cusseckill1 = new Cusseckill();
                cusseckill1.setSeckillgroup(seckillgroup);
                cusseckill1.setMemo(CusseckillConstant.TUYUAN);
                cusseckill1.setStatus(BaseConstant.USE_STATUS);
                cusseckill1.setSpotId(fzCusseckillId);
                cusseckill1.setCusId(customerInfo.getCustomerId());
                //时间戳
                cusseckill1.setCreatedName(customerInfo.getNickname());
                cusseckill1.setCreationDate(LocalDateTime.now());
                cusseckill1.setCreatedId(customerInfo.getCustomerId());

                boolean save = iCusseckillService.save(cusseckill1);
                if (save){
                    //参与成功之后再次判断该团是否已满
                    List<Cusseckill> cusseckills = iCusseckillService.list(new QueryWrapper<Cusseckill>().eq("seckillgroup", seckillgroup));
                    //如果团满
                    if (cusseckills.size()==seckill.getCollageCount()){
                        //则将拼图状态设为不用,使用过的拼团
                        for (Cusseckill cusseckill : cusseckills) {
                            cusseckill.setStatus(BaseConstant.DISUSE_STATUS);
                            iCusseckillService.updateById(cusseckill);
                        }
                    }
                    ResponseResult.Step(ResponseResult.SUCCESS("参与成功！"));
                    return;
                }else {
                    ResponseResult.Step(ResponseResult.ERROR("参与失败！"));
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
                logger.debug("fzCusseckillId==="+fzCusseckillId);
                logger.debug("seckillgroup==="+seckillgroup);
                logger.debug("Exception==="+e.getMessage());
                ResponseResult.Step(ResponseResult.ERROR("参与失败，请重新操作！"));
            }
        }
    }

}
