package com.tourism.hu.apicontroller;

import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.StrategicLable;
import com.tourism.hu.service.IStrategicLableService;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 11:42
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/api/strategicLableApi")
public class StrategicLableApiController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IStrategicLableService iStrategicLableService;

    @RequestMapping("/addOrUpdateLable")
    public void addOrUpdateLable(StrategicLable strategicLable){
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo==null){
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录！"));
            return;
        }
        strategicLable.setCustomerId(customerInfo.getCustomerId());
        strategicLable.setStatus(BaseConstant.USE_STATUS);
        try {
            if (strategicLable.getTagId()==null){
                if (iStrategicLableService.save(strategicLable)){
                    ResponseResult.Step(ResponseResult.SUCCESS("添加标签成功！"));
                    return;
                }
            }else {
                if (iStrategicLableService.updateById(strategicLable)){
                    ResponseResult.Step(ResponseResult.SUCCESS("更新标签成功！"));
                    return;
                }
            }
            ResponseResult.Step(ResponseResult.ERROR("操作失败！"));
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("strategicLable==="+strategicLable);
            logger.debug("Exception==="+e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("异常错误，操作失败！"));
        }


    }


}
