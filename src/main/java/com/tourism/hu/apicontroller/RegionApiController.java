package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.Region;
import com.tourism.hu.entity.vo.RegionVo;
import com.tourism.hu.mapper.RegionMapper;
import com.tourism.hu.service.IRegionService;
import com.tourism.hu.util.ResponseResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 10:28
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/api/regionApi")
public class RegionApiController extends BaseController {

    @Resource
    private IRegionService iRegionService;

    @Resource
    private RegionMapper regionMapper;

    @RequestMapping("/selectRegionByPinyin")
    public void selectRegionByPinyin(){
        try {
            List<RegionVo> regionVos = regionMapper.selectRegionByPinyin();
            ResponseResult result = ResponseResult.SUCCESS("查询成功");
            result.setData(regionVos);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR("查询失败"+e.getMessage()));
        }

    }

}
