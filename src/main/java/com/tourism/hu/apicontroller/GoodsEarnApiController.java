package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.Goods;
import com.tourism.hu.entity.GoodsCategory;
import com.tourism.hu.entity.StrategicTravels;
import com.tourism.hu.entity.vo.GoodsVo;
import com.tourism.hu.mapper.GoodsCategoryMapper;
import com.tourism.hu.mapper.GoodsMapper;
import com.tourism.hu.service.*;
import com.tourism.hu.util.GlobalConfigUtil;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 15:44
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/api/goodsEarnApi")
public class GoodsEarnApiController extends BaseController {


    Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private GoodsMapper goodsMapper;

    @Resource
    private IGoodsService iGoodsService;

    @Resource
    private GoodsCategoryMapper goodsCategoryMapper;

    @Resource
    private IGoodsCategoryService iGoodsCategoryService;

    @Resource
    private IGoodsEquityService iGoodsEquityService;

    @Resource
    private IEquityService iEquityService;

    @Resource
    private ISeckillService iSeckillService;

    @Resource
    private IGoodsDtlService iGoodsDtlService;

    @Resource
    private IGoodsDtlImgService iGoodsDtlImgService;

    @Resource
    private IGoodsPicService iGoodsPicService;

    @Resource
    private IBaseGoods iBaseGoods;


    /**
     * 查询玩赚商品
     * @param page
     * @param limit
     */
    @RequestMapping("/selectGoodsEarn")
    public void selectGoodsEarn(Integer page,Integer limit){
        try {
            Page<Goods> page1 = new Page<>(page, limit);
            QueryWrapper<Goods> qw = new QueryWrapper<>();
            //玩赚，className必须等于玩赚，class_id_status必须等于2.
            GoodsCategory categoryServiceOne = iGoodsCategoryService.getOne(new QueryWrapper<GoodsCategory>().eq("class_name", "玩赚").eq("class_id_status", 2));
            if (categoryServiceOne==null){
                ResponseResult.Step(ResponseResult.ERROR("查询失败！玩赚分类id不存在"));
                logger.debug("玩赚分类id不存在");
                return;
            }
            qw.select("goods_id goodsId", "goodsname", "price","activity_price activityPrice", "picture_url pictureUrl").eq("goodsclassid", categoryServiceOne.getClassId()).eq("status", BaseConstant.USE_STATUS).orderByDesc("create_time");
            IPage<Map<String, Object>> mapIPage = iGoodsService.pageMaps(page1, qw);
            List<Map<String, Object>> mapList = mapIPage.getRecords();
            for (Map<String, Object> map : mapList) {
                map.put("pictureUrl", isNullNotAppend(map.get("pictureUrl")));
            }
            ResponseResult<List<Map<String, Object>>> result = new ResponseResult<>();
            result.setData(mapList);
            result.setMsg("查询成功！");
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("classId===" );
            logger.debug(e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("查询失败，请重试"));
        }

    }


}
