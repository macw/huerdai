package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.StrategicComment;
import com.tourism.hu.entity.StrategicTravels;
import com.tourism.hu.service.ICustomerInfoService;
import com.tourism.hu.service.IStrategicCommentService;
import com.tourism.hu.service.IStrategicTravelsService;
import com.tourism.hu.util.DateConverterUtil;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 10:43
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/api/strategicComment")
public class StrategicCommentApiController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IStrategicCommentService iStrategicCommentService;

    @Resource
    private IStrategicTravelsService iStrategicTravelsService;

    @Resource
    private ICustomerInfoService iCustomerInfoService;

    /**
     * 发布评论
     *
     * @param content     评论内容
     * @param strategicId 攻略主键id
     */
    @RequestMapping("/addComment")
    public void addComment(String content, Integer strategicId) {
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录"));
            return;
        }
        StrategicComment strategicComment = new StrategicComment();
        strategicComment.setContent(content);
        strategicComment.setStrategicId(strategicId);
        strategicComment.setCustomerId(customerInfo.getCustomerId());
        //默认已审核
        strategicComment.setAuditStatus(BaseConstant.USE_STATUS);
        if (iStrategicCommentService.save(strategicComment)) {
            ResponseResult.Step(ResponseResult.SUCCESS("评论成功！"));
        } else {
            ResponseResult.Step(ResponseResult.ERROR("评论失败！"));
            logger.debug("评论攻略：content===" + content);
            logger.debug("评论攻略：strategicId===" + strategicId);
        }
    }

    /**
     * 回复评论
     * 只有作者能 回复 攻略下面其他人的评论
     *
     * @param commentId
     * @param strategicId
     * @param content
     */
    @RequestMapping("/replyComment")
    public void replyComment(Integer commentId, Integer strategicId, String content) {
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录"));
            return;
        }
        StrategicTravels strategicTravels = iStrategicTravelsService.getById(strategicId);
        if (strategicTravels.getCustomerId() != customerInfo.getCustomerId()) {
            ResponseResult.Step(ResponseResult.ERROR("您不是该文章作者，无法回复该评论！"));
            return;
        }
        StrategicComment strategicComment = new StrategicComment();
        strategicComment.setCustomerId(customerInfo.getCustomerId());
        strategicComment.setStrategicId(strategicId);
        strategicComment.setContent(content);
        strategicComment.setCommentParent(commentId);
        boolean save = iStrategicCommentService.save(strategicComment);
        if (save) {
            ResponseResult.Step(ResponseResult.SUCCESS("回复评论成功！"));
        } else {
            ResponseResult.Step(ResponseResult.ERROR("回复评论失败！"));
            logger.debug("评论主键：commentId===" + commentId);
            logger.debug("回复评论攻略主键：strategicId===" + strategicId);
            logger.debug("回复评论内容：content===" + content);
        }
    }


    /**
     * 查询该篇攻略下的所有评论
     *
     * @param strategicId
     */
    @RequestMapping("/selectCommentBystrategicId")
    public void selectCommentBystrategicId(Integer strategicId) {
        try {
            List<StrategicComment> strategicComments = iStrategicCommentService.list(new QueryWrapper<StrategicComment>().eq("strategic_id", strategicId).eq("comment_parent", 0));
            if (strategicComments != null && strategicComments.size() > 0) {
                for (StrategicComment strategicComment : strategicComments) {
                    List<StrategicComment> commentList = iStrategicCommentService.list(new QueryWrapper<StrategicComment>().eq("comment_parent", strategicComment.getCommentId()));
                    if (commentList != null && commentList.size() > 0) {
                        for (StrategicComment comment : commentList) {
                            CustomerInfo customerInfo = iCustomerInfoService.getById(comment.getCustomerId());
                            if (customerInfo!=null){
                                comment.setNickname(customerInfo.getNickname());
                                comment.setCustomerIcon(customerInfo.getCustomerIcon());
                            }
                            comment.setCreateTime(DateConverterUtil.dateFormat(comment.getAuditTime(),  DateConverterUtil.dateTime));

                        }
                        strategicComment.setStrategicCommentList(commentList);
                    }
                    CustomerInfo customerInfo = iCustomerInfoService.getById(strategicComment.getCustomerId());
                    if (customerInfo!=null){
                        strategicComment.setNickname(customerInfo.getNickname());
                        strategicComment.setCustomerIcon(customerInfo.getCustomerIcon());
                    }
                    strategicComment.setCreateTime(DateConverterUtil.dateFormat(strategicComment.getAuditTime(),  DateConverterUtil.dateTime));
                }
            }
            ResponseResult result = ResponseResult.SUCCESS("查询成功！");
            result.setData(strategicComments);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("攻略，查询评论失败！strategicId===" + strategicId);
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
            logger.debug("攻略评论查询异常信息：===" + e.getMessage());
        }
    }

}
