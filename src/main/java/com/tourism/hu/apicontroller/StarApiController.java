package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.Star;
import com.tourism.hu.entity.StrategicTravels;
import com.tourism.hu.service.IStarService;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 11:25
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/api/starApi")
public class StarApiController extends BaseController {

    private  Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IStarService iStarService;

    /**
     * 查询所有达人列表
     */
    @RequestMapping("/selectStarList")
    public void selectStarList(Integer page,Integer limit){
        try {
            Page<Star> starPage = new Page<>(page, limit);
            QueryWrapper<Star> qw = new QueryWrapper<>();
            qw.select("star_id starId", "star_image_url starImageUrl","star_name starName", "star_sign starSign", "star_video_count starVideoCount","star_prise_count starPriseCount").orderByDesc("star_sort", "create_time");
            IPage<Map<String, Object>> mapIPage = iStarService.pageMaps(starPage, qw);
            List<Map<String, Object>> records = mapIPage.getRecords();
            ResponseResult result = ResponseResult.SUCCESS("查询成功！");
            result.setData(records);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"+e.getMessage()));
        }
    }

    /**
     * 查询达人详情
     * @param starId
     */
    @RequestMapping("/selectStarDetail")
    public void selectStarDetail(Integer starId){
        Star star = iStarService.getById(starId);
        ResponseResult result = ResponseResult.SUCCESS("查询成功！");
        result.setData(star);
        ResponseResult.Step(result);

    }

    /**
     * 增加观看数
     * @param starId
     */
    @RequestMapping("/addVideoing")
    public void addVideoing(Integer starId){
        Star star = iStarService.getById(starId);
        logger.debug("star==="+star);
        System.out.println(star);
        star.setStarVideoCount(star.getStarVideoCount() + 1);
        boolean b = iStarService.updateById(star);
        if (b){
            ResponseResult result = ResponseResult.SUCCESS("添加成功！");
            result.setData(star.getStarVideoCount());
            ResponseResult.Step(result);
        }else {
            ResponseResult.Step(ResponseResult.ERROR("添加失败！"+starId));
        }

    }

    /**
     * 增加点赞数
     * @param starId
     * @param type 1增加，0减少
     */
    @RequestMapping("/addPriseing")
    public void addPriseing(Integer starId,Integer type){
        Star star = iStarService.getById(starId);
        int count = 0;
        if (type==1){
            count = star.getStarPriseCount()+1;
        }else {
            count = star.getStarPriseCount()-1;
        }
        star.setStarPriseCount(count);
        boolean b = iStarService.updateById(star);
        if (b){
            ResponseResult result = ResponseResult.SUCCESS("添加成功！");
            result.setData(star.getStarPriseCount());
            ResponseResult.Step(result);
        }else {
            ResponseResult.Step(ResponseResult.ERROR("添加失败"+starId+"_"+type));
        }

    }




}
