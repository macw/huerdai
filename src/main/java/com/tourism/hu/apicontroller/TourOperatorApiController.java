package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.GoodsPicConstant;
import com.tourism.hu.entity.Agent;
import com.tourism.hu.entity.GoodsPic;
import com.tourism.hu.entity.TourOperator;
import com.tourism.hu.mapper.GoodsPicMapper;
import com.tourism.hu.service.IAgentService;
import com.tourism.hu.service.ITourOperatorService;
import com.tourism.hu.util.ResponseResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
/**
 * 
 * @ClassName: TourOperatorApiController 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:03:39 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/api/TourOperatorApi")
public class TourOperatorApiController {

	 
		@Autowired
		private ITourOperatorService iTourOperatorService;

	    
	    /**
	     * 
	     *
	     * @param 
	     */
		@GetMapping("/selectTourOperator")
		public void selectTourOperator() {
			ResponseResult<List<TourOperator>> result = new ResponseResult<>();
			try {
				QueryWrapper<TourOperator> qw= new QueryWrapper<TourOperator>();
				
				List<TourOperator> list = iTourOperatorService.list(qw);
				result.setOkMsg(list);
			}catch (Exception e) {
				result.setErrorMsg("查询异常");
			}
			ResponseResult.Step(result);
		}


}
