package com.tourism.hu.apicontroller;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.LinkedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.DedeSysconfig;
import com.tourism.hu.entity.ScenicOrder;
import com.tourism.hu.entity.ScenicRoutes;
import com.tourism.hu.entity.ScenicRoutesClass;
import com.tourism.hu.entity.ScenicRoutesprice;
import com.tourism.hu.entity.Seckill;
import com.tourism.hu.service.IDedeSysconfigService;
import com.tourism.hu.service.IScenicOrderService;
import com.tourism.hu.service.IScenicRoutesClassService;
import com.tourism.hu.service.IScenicRoutesService;
import com.tourism.hu.service.IScenicRoutespriceService;
import com.tourism.hu.service.ISeckillService;
import com.tourism.hu.util.DateConverterUtil;
import com.tourism.hu.util.GlobalConfigUtil;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResponseResult;
/**
 * 
 * @ClassName: ScenicRoutesIndexApiController 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:03:25 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RequestMapping("api/scenicRoutesApi")
@RestController
public class ScenicRoutesIndexApiController extends BaseController {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private IDedeSysconfigService dedeSysconfigService;
	
	@Autowired
	private ISeckillService seckillService;
	
	@Autowired
	private IScenicRoutesService routesService;
	
	@Autowired
	private IScenicOrderService orderService;
	
	@Autowired
	private IScenicRoutespriceService routespriceService;
	
	/**
	 * 线路分类service
	 */
	@Autowired
	private IScenicRoutesClassService routesClassService;

	/**
	 * 
	 * @Title: selectIndexBanner
	 * @Description: (获取线路首页Banner图)
	 * @date 2019年11月10日 上午11:38:39
	 * @author 董兴隆
	 */
	@RequestMapping("selectIndexBanner")
	public void selectIndexBanner() {
		ResponseResult<List<Map<String, Object>>> result = new ResponseResult<List<Map<String, Object>>>();
		List<Map<String, Object>> maps = new ArrayList<Map<String,Object>>();
		QueryWrapper<DedeSysconfig> qw= new QueryWrapper<DedeSysconfig>();
		qw.select("info img","value url").eq("type", 3);
		try {
			maps = dedeSysconfigService.listMaps(qw);
//			for (Map<String, Object> map : maps) {
//				map.put("img", GlobalConfigUtil.getServiceUrl()+map.get("img").toString());
//			}
			result.setOkMsg("查询成功");
		} catch (Exception e) {
			logger.error("线路首页banner 查询失败");
			result.setErrorMsg("查询失败");
		}
		result.setData(maps);
		ResponseResult.Step(result);
	}

	/**
	 * 
	 * @Title: selectRoutesIndexClassify 
	 * @Description: (线路首页展示分类)   默认取分类中前五条数据
	 * @date 2019年11月10日 下午1:35:41
	 * @author 董兴隆
	 */
	@RequestMapping("selectRoutesIndexClassify")
	public void selectRoutesIndexClassify() {
		ResponseResult<List<Map<String, Object>>> result = new ResponseResult<List<Map<String, Object>>>();
		QueryWrapper<ScenicRoutesClass> qw= new QueryWrapper<ScenicRoutesClass>();
		qw.select("routesclass_id routesclassId","routesclass_name routesclassName","memo","img","url").eq("type", 1).eq("status", 1).last("limit 5");
		List<Map<String, Object>> maps = routesClassService.listMaps(qw);
//		for (Map<String, Object> map : maps) {
//			map.put("img", GlobalConfigUtil.getServiceUrl()+map.get("img").toString());
//		}
		result.setData(maps);
		ResponseResult.Step(result);
	}
	
	/**
	 * 
	 * @Title: selectRoutesSeckill 
	 * @Description: (线路秒杀)   
	 * @date 2019年11月10日 下午1:43:06
	 * @author 董兴隆
	 */
	@RequestMapping("selectRoutesSeckill")
	public void selectRoutesSeckill(Integer pageNo,Integer pageNum) {
//		ResponseResult<Map<Integer,List<Map<String,Object>>>> result = new ResponseResult<>();
		ResponseResult<List<Map<String, Object>>> result = new ResponseResult<>();
		List<Map<String, Object>> data = new ArrayList<>();
		// 当天时间的秒杀线路
		LocalDateTime now = LocalDateTime.now();
		QueryWrapper<Seckill> qw = new QueryWrapper<Seckill>().lt("seckill_begintime", now).gt("seckill_endtime", now).isNull("collage_count").eq("status", 1);
		List<Seckill> seckillList = seckillService.list(qw);
		if(seckillList==null || seckillList.isEmpty()) {
			result.setOkMsg("未找到秒杀商品");
			result.setData(data);
			ResponseResult.Step(result);
			return ;
		}
		Map<Integer,List<Map<String,Object>>> dates = new HashMap<Integer, List<Map<String,Object>>>(); 
		//线路list集合
		for (Seckill seckill : seckillList) {
			//时间判断当前秒杀商品属于哪个时间
			int hour = seckill.getSeckillBegintime().getHour();
			List<Map<String, Object>> maps = new ArrayList<Map<String,Object>>();
			//判断时间集合是否存在 如果存在则使用时间集合map 不存在使用map 集合
			if(dates.containsKey(hour)) {
				maps = dates.get(hour);
			}
			// 找到线路信息 未找到则跳出循环
			ScenicRoutes routes = routesService.getById(seckill.getGoodsId());
			if(routes==null || routes.getRoutesId() == null) {
				continue;
			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("routesId", routes.getRoutesId());
//			map.put("routesImg", GlobalConfigUtil.getServiceUrl()+routes.getRoutesImg());
			map.put("routesImg", routes.getRoutesImg());
			map.put("sellingPrice", routes.getSellingPrice());
			map.put("activityPrice", seckill.getSeckillPrice());
			maps.add(map);
			dates.put(hour, maps);
		}
		
		Map<Integer, List<Map<String, Object>>> sort = new LinkedMap();
		List<Integer>  hourSort = new  ArrayList<>();
		if(dates!=null && dates.keySet().size()>0) {
			hourSort.addAll(dates.keySet());
			Collections.sort(hourSort);
			for (Integer integer : hourSort) {
				//sort.put(integer, dates.get(integer));
				Map<String, Object> obj = new HashMap<String, Object>();
				obj.put("hour", integer);
				obj.put("routesList",dates.get(integer));
				data.add(obj);
			}
			
		}
		result.setData(data);
		ResponseResult.Step(result);
	}
	
	/**
	 * 
	 * @Title: popularityRecommend 
	 * @Description: (线路人气推荐)   
	 * @date 2019年11月10日 下午4:25:46
	 * @author 董兴隆
	 */
	@RequestMapping("popularityRecommend")
	public void popularityRecommend() {
		//获得线路订单中已完成线路数量最多的线路做推荐
		ResponseResult<List<Map<String,Object>>> result = ResponseResult.ERROR("未找到推荐路线");
		List<Map<String,Object>> maps = orderService.listMaps(new QueryWrapper<ScenicOrder>().select("routes_id routesId","count(routes_id) num").eq("order_status",3).groupBy("routes_id").orderByDesc("num").last("limit 10"));
		if(maps.isEmpty()) {
			result.setOkMsg("当前没有推荐路线");
			ResponseResult.Step(result);
		}
		
		List<Map<String,Object>> data = new ArrayList<Map<String,Object>>();
		for (Map<String, Object> map : maps) {
			Map<String,Object> tempMap = new HashMap<String, Object>();
			Integer routesId = (Integer) map.get("routesId");
			ScenicRoutes routes = routesService.getById(routesId);
			if(routes == null) {
				continue;
			}
			tempMap.put("routesId", routes.getRoutesId());
			tempMap.put("routesName", routes.getRoutesName());
//			tempMap.put("routesImg", GlobalConfigUtil.getServiceUrl()+routes.getRoutesImg());
			tempMap.put("routesImg", routes.getRoutesImg());
			tempMap.put("sellingPrice", routes.getSellingPrice());
			data.add(tempMap);
		}
		result.setData(data);
		result.setOkMsg("查询成功");
		ResponseResult.Step(result);
	}
	
	
	 /**
	  * 
	  * @Title: guessYouLike 
	  * @Description: (猜你喜欢)   
	  * @date 2019年11月10日 下午5:11:03
	  * @author 董兴隆
	  */
	@RequestMapping("guessYouLike")
	public void  guessYouLike(PageUtil page) {
		ResponseResult<List<Map<String,Object>>> result = ResponseResult.ERROR("异常");
		List<Map<String,Object>> data = new ArrayList<>();
		CustomerInfo cust = getCustomerInfo();
		//未登录找订单销量最高的一个分类
		ScenicOrder order = null;
		if(cust == null) {
			order = orderService.getOne(new QueryWrapper<ScenicOrder>().select("routes_id routesId","count(routes_id) num").eq("order_status",3).groupBy("routes_id").orderByDesc("num").last("limit 1"));
		}else {
			order = orderService.getOne(new QueryWrapper<ScenicOrder>().eq("customer_id", cust.getCustomerId()).eq("order_status",3).orderByDesc("order_time").last("limit 1"));
		}
		if(order==null) {
			result.setOkMsg("未找到您喜欢的数据");
			ResponseResult.Step(result);
			return ;
		}
		ScenicRoutes routes = routesService.getById(order.getRoutesId());
		if(routes == null ) {
			result.setOkMsg("未找到您喜欢的数据");
			ResponseResult.Step(result);
			return ;
		}
		
		List<ScenicRoutes> list = routesService.list(new QueryWrapper<ScenicRoutes>().eq("routesclass_id", routes.getRoutesclassId()));
		if(list == null || list.isEmpty()) {
			result.setOkMsg("未找到您喜欢的数据");
			ResponseResult.Step(result);
			return ;
		}
		List<Integer> routesIds = new ArrayList<>();
		for (ScenicRoutes scenicRoutes : list) {
			routesIds.add(scenicRoutes.getRoutesId());
		}
		QueryWrapper<ScenicOrder> qw = new QueryWrapper<ScenicOrder>().select("routes_id routesId","count(routes_id) num").in("routes_id", routesIds).eq("order_status",3).groupBy("routes_id").orderByDesc("num");
		String today = DateConverterUtil.getDay(0);
		List<Map<String, Object>> scenicOrderMaps = orderService.listMaps(qw);
		for (Map<String, Object> map : scenicOrderMaps) {
			Integer routesId = (Integer) map.get("routesId");
			ScenicRoutes routesTemp = routesService.getById(routesId);
			if(routesTemp==null|| routesTemp.getRoutesId()==null) {
				continue;
			}
			Map<String,Object> m = new HashMap<String, Object>();
			m.put("routesId", routesTemp.getRoutesId());
//			m.put("routesImg", GlobalConfigUtil.getServiceUrl()+routesTemp.getRoutesImg());
			m.put("routesImg", routesTemp.getRoutesImg());
			int isRefund = 0;
			if(routesTemp.getType()!=null && routesTemp.getType()==1) {
				isRefund = 1;
			}
			m.put("isRefund", isRefund);
			int isDetermined = 0;
			if(routesTemp.getDetermined()!=null && routesTemp.getDetermined()==1) {
				isDetermined = 1;
			}
			m.put("isDetermined", isDetermined);
			m.put("memo", routesTemp.getMemo());
			m.put("routesName", routesTemp.getRoutesName());
			ScenicRoutesprice todayPrice = routespriceService.getOne(new QueryWrapper<ScenicRoutesprice>().eq("routes_id", routesId).apply("date_format(price_date,'%Y-%m-%d') = {0}", today),false);
			if(todayPrice==null || todayPrice.getRoutespriceId()==null) {
				m.put("sellingPrice", routesTemp.getSellingPrice());
			}else {
				if(todayPrice.getActivityPrice()!=null && todayPrice.getActivityPrice().compareTo(BigDecimal.ZERO)==1) {
					m.put("sellingPrice", todayPrice.getActivityPrice());
				}else if(todayPrice.getSellingPrice()!=null && todayPrice.getSellingPrice().compareTo(BigDecimal.ZERO)==1) {
					m.put("sellingPrice", todayPrice.getSellingPrice());
				}
			}
			data.add(m);
		}
		result.setData(data);
		result.setOkMsg("查询成功");
		ResponseResult.Step(result);
	}
	
	

}
