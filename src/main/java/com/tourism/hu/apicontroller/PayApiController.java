package com.tourism.hu.apicontroller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.PaymentTransaction;
import com.tourism.hu.service.IPaymentTransactionService;
import com.tourism.hu.util.IntegerUtil;
import com.tourism.hu.util.ResponseResult;

@RestController
@RequestMapping("api/payapi")
public class PayApiController extends BaseController{

	/**
	 * 支付记录日志
	 */
	@Autowired
	private IPaymentTransactionService paymentTransactionService;
	
	@RequestMapping("topay")
	public void toPay(String orderSn) {																																							
		ResponseResult<Map<String, Object>> result = ResponseResult.ERROR("查询失败");
		Map<String, Object> map = new HashMap<String, Object>();
		PaymentTransaction payment=paymentTransactionService.getOne(new QueryWrapper<PaymentTransaction>().eq("order_sn", orderSn),false);
		//微信支付
		if(payment!=null && IntegerUtil.isNotNullAndZero(payment.getPaymentMethod()) && payment.getPaymentMethod() == 5) {
			map.put("appId", payment.getAppId());
			map.put("nonceStr", payment.getNonceStr());
			map.put("orderSn", payment.getOrderSn());
			map.put("paySign", payment.getPaySign());
			map.put("prepayId", payment.getPrepayId());
			map.put("signType", payment.getSignType());
			map.put("timeStamp", payment.getTimeStamp());
			result = ResponseResult.SUCCESS("查询成功");
		}
		result.setData(map);
		ResponseResult.Step(result);
	}
	
	
}
