package com.tourism.hu.apicontroller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.WXLoginConstant;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.pay.wxsdk.TokenConfig;
import com.tourism.hu.pay.wxsdk.WeixinConfig;
import com.tourism.hu.service.ICustomerInfoService;
import com.tourism.hu.service.IRegisterService;
import com.tourism.hu.util.AuthUtil;
import com.tourism.hu.util.DateConverterUtil;
import com.tourism.hu.util.ResponseResult;

import net.sf.json.JSONObject;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 18:22
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@Controller
@RequestMapping("/wxLogin")
public class WXLogin extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private ICustomerInfoService iCustomerInfoService;
    
    
    @Autowired
    private IRegisterService registerService;

    @Resource
    private RedisTemplate redisTemplate;


    @RequestMapping("/getCode")
    public String getCode(HttpServletRequest request, HttpServletResponse response) {
        //拼接url
        StringBuilder url = new StringBuilder();
        url.append("https://open.weixin.qq.com/connect/qrconnect?");
        //appid
        url.append("appid=" + WXLoginConstant.APPID);
        //转码
        try {
        	//回调地址 ,回调地址要进行Encode转码
            String redirect_uri = URLEncoder.encode(WXLoginConstant.REDIRECT_URI, "utf-8");
            System.out.println("redirect_uri==" + redirect_uri);
            url.append("&redirect_uri=" + redirect_uri);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        url.append("&response_type=code");
        url.append("&scope=snsapi_login");
        url.append("&state=" + request.getSession().getId());
        url.append("#wechat_redirect");
        System.out.println("getId===" + request.getSession().getId());

        //  System.out.println("s==="+s);
        System.out.println("url===" + url.toString());
//
        return "redirect:" + url.toString();
    }

    /**
     * 微信 授权登录回调
     **/
    @RequestMapping("/callback")
    public void callback(String code, String state, HttpServletRequest request, HttpServletResponse response) throws Exception {
        System.out.println("====" + code + "===" + state + "====");
        logger.debug("code===" + code);
        logger.debug("state===" + state);
        if (StringUtils.isNotEmpty(code)) {
            logger.debug("sssssssss====" + code);
            StringBuilder url = new StringBuilder();
            url.append("https://api.weixin.qq.com/sns/oauth2/access_token?");
            url.append("appid=" + WXLoginConstant.Auth_APP_ID);
            url.append("&secret=" + WXLoginConstant.Auth_APP_SECRET);
            //这是微信回调给你的code
            url.append("&code=" + code);
            url.append("&grant_type=authorization_code");
            System.out.println("url.toString()===" + url.toString());
            logger.debug("url.toString()===" + url.toString());

            JSONObject jsonObject = AuthUtil.doGetJson(url.toString());
            //1.获取微信用户的openid
            String openid = jsonObject.getString("openid");
            //2.获取获取access_token
            String access_token = jsonObject.getString("access_token");
            String infoUrl = "https://api.weixin.qq.com/sns/userinfo?access_token=" + access_token + "&openid=" + openid
                    + "&lang=zh_CN";
            logger.debug("infoUrl===" + infoUrl);

            //3.获取微信用户信息
            JSONObject userInfo = AuthUtil.doGetJson(infoUrl);
            //至此拿到了微信用户的所有信息,剩下的就是业务逻辑处理部分了
            //保存openid和access_token到session
            request.getSession().setAttribute("openid", openid);
            request.getSession().setAttribute("access_token", access_token);
            logger.debug("openid===" + openid);
            logger.debug("access_token===" + access_token);
            //保存openid和access_token到session
            request.getSession().setAttribute("openid", openid);
            request.getSession().setAttribute("access_token", access_token);
            String sessionid = getRequest().getSession().getId();
            //去数据库查询有没有这个 openid
            CustomerInfo customerInfoServiceOne = iCustomerInfoService.getOne(new QueryWrapper<CustomerInfo>().eq("openid", openid));
            if (customerInfoServiceOne == null) {
                CustomerInfo customerInfo = new CustomerInfo();
                if (openid != null) {
                    customerInfo.setOpenid(openid);
                }
                //处理获取到的微信用户信息 到用户表
                logger.debug("userinfo===" + userInfo.get("nickname"));
                if (userInfo != null) {
                    String nickname = userInfo.getString("nickname");
                    if (nickname != null) {
                        customerInfo.setNickname(nickname);
                    }
                    logger.debug("userinfo222===" + userInfo.get("nickname"));

                    String headimgurl = userInfo.getString("headimgurl");
                    if (headimgurl != null) {
                        customerInfo.setCustomerIcon(headimgurl);
                    }
                    String sex = userInfo.getString("sex");
                    if (sex != null) {
                        customerInfo.setGender(sex);
                    }
                    customerInfo.setProvince(userInfo.getString("province"));
                    customerInfo.setCity(userInfo.getString("city"));
                    logger.debug("userinfo222===" + userInfo.get("city"));
                    customerInfo.setCountry(userInfo.getString("country"));
                    customerInfo.setUnionid(userInfo.getString("unionid"));
                    logger.debug("userinfo222===" + userInfo.get("unionid"));
                }
                boolean save = registerService.register(customerInfo);
                if (save) {
                    logger.debug("首次认证:http://m.huerdai.net");
                    redisTemplate.opsForValue().set(sessionid, customerInfoServiceOne.getCustomerId());
                    response.sendRedirect("http://m.huerdai.net/index.html");
                    return;
                } else {
                    logger.debug("认证失败！");
                    response.sendRedirect("http://m.huerdai.net/error.html");
                    return;
                }
            } else {
                //已经授权过，没有绑定手机号，也是直接跳转到首页
                redisTemplate.opsForValue().set(sessionid, customerInfoServiceOne.getCustomerId());
                if (customerInfoServiceOne.getMobilePhone() == null) {
                    logger.debug("已经授权过，没有绑定手机号，也是直接跳转到首页");
                    //并且将用户信息存到Redis中
                    response.sendRedirect("http://m.huerdai.net/index.html");
                    return;
                } else {
                    //已经授权过，并且已经绑定手机号
                    logger.debug("有openid的跳转http://m.huerdai.net222222");
                    response.sendRedirect("http://m.huerdai.net/index.html");
                    return;
                }

            }

        } else {
            logger.debug("code获取失败！====" + code);
            // return new ModelAndView("redirect:http://m.huerdai.net/error.html");
            response.sendRedirect("http://m.huerdai.net/error.html");
        }
    }




    public static String get(String uri) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet get = new HttpGet(uri);
        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(get);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                String result = EntityUtils.toString(entity, "UTF-8");
                return result;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping("/test")
    public ModelAndView test() {
//        return new ModelAndView("redirect:http://m.huerdai.net","",3);
        return new ModelAndView(new RedirectView("travel/index"));
    }

    @RequestMapping("/test2")
    public void test2(HttpServletResponse response) {
        try {
            response.sendRedirect("http://m.huerdai.net");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
