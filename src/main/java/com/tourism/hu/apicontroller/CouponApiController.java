package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.entity.Coupon;
import com.tourism.hu.entity.Coupongoods;
import com.tourism.hu.mapper.CouponMapper;
import com.tourism.hu.mapper.CoupongoodsMapper;
import com.tourism.hu.util.ResponseResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 红包，优惠劵接口
 */
/**
 * 
 * @ClassName: CouponApiController 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:01:19 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/api/CouponApi")
public class CouponApiController {

    @Resource
    private CouponMapper couponMapper;

    @Resource
    private CoupongoodsMapper coupongoodsMapper;

    /**
     * 根据商品id查询 该商品所拥有的优惠券信息
     * @param goodsId
     */
    @RequestMapping("/selectCouponByGoodsId")
    public void selectCouponByGoodsId(Integer goodsId){
        try {
            List<Coupongoods> coupongoodsList = coupongoodsMapper.selectList(new QueryWrapper<Coupongoods>().eq("goods_id", goodsId));
            List<Coupon> couponList = null;
            for (Coupongoods coupongoods : coupongoodsList) {
                couponList = couponMapper.selectList(new QueryWrapper<Coupon>().eq("coupon_id", coupongoods.getCouponId()));
            }
            ResponseResult<List<Coupon>> result = new ResponseResult<>();
            result.setData(couponList);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR);
        }
    }

}
