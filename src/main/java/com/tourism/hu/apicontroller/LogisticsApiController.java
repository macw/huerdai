package com.tourism.hu.apicontroller;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.entity.Goods;
import com.tourism.hu.entity.Logistics;
import com.tourism.hu.entity.OrderDetail;
import com.tourism.hu.mapper.CustomerFriendMapper;
import com.tourism.hu.service.IGoodsService;
import com.tourism.hu.service.ILogisticsService;
import com.tourism.hu.service.IOrderDetailService;
import com.tourism.hu.service.IOrderService;
import com.tourism.hu.util.GlobalConfigUtil;
import com.tourism.hu.util.KdniaoTrackQueryAPI;
import com.tourism.hu.util.ResponseResult;
/**
 * 
 * @ClassName: LogisticsApiController 
 * @Description: (物流接口) 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:02:26 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/api/logisticsApi")
public class LogisticsApiController {
	
	
    private  Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private CustomerFriendMapper customerFriendMapper;

	@Autowired
	private ILogisticsService logisticsService;
	
	@Autowired
	private IOrderDetailService orderDetailService;
	
	@Autowired
	private IGoodsService goodsService;
	
	
	
	
	/**
	 * 
	 * @Title: orderInfoShowLogistics 
	 * @Description: (订单详情显示物流最新一条记录) 
	 * @param orderSn
	 * @param expressNo  
	 * @date 2019年11月16日 下午4:19:47
	 * @author 董兴隆
	 */
	@RequestMapping("/selectLogisticsInfo")
	public void selectLogisticsInfo(String orderSn) {
		ResponseResult<Map<String,Object>> result = ResponseResult.ERROR("未找该订单的物流信息"); 
		Map <String,Object> map = new HashMap<String, Object>();
		OrderDetail details = orderDetailService.getOne(new QueryWrapper<OrderDetail>().eq("order_sn", orderSn),false);
		if(details==null) {
			result.setData(map);
			result.setErrorMsg("未找到订单数据");
			ResponseResult.Step(result);
			return ;
		}
		Logistics logistics = logisticsService.getOne(new QueryWrapper<Logistics>().eq("order_sn", orderSn),false);
		if(logistics==null) {
			result.setData(map);
			result.setErrorMsg("未找到物流信息");
			ResponseResult.Step(result);
			return ;
		}
		Goods goods = goodsService.getById(details.getGoodsId());
		if(goods==null) {
			result.setData(map);
			result.setErrorMsg("未找到商品信息");
			ResponseResult.Step(result);
			return ;
		}
		map.put("goodsName", goods.getGoodsname());
		map.put("pictureUrl",goods.getPictureUrl());
		map.put("companyName",logistics.getBusinessName());
		map.put("expressNo", logistics.getExpressNo());
		List<Map<String, Object>> logisticsList =selectLogisticsMaps(orderSn);
		if(logisticsList!=null && !logisticsList.isEmpty()) {
			result.setOkMsg("查询成功");
		}
		map.put("logistics", logisticsList);
		result.setData(map);
		ResponseResult.Step(result);
	}
	/**
	 * 
	 * @Title: orderInfoShowLogistics 
	 * @Description: (订单详情显示物流最新一条记录) 
	 * @param orderSn
	 * @param expressNo  
	 * @date 2019年11月16日 下午4:19:47
	 * @author 董兴隆
	 */
	@RequestMapping("/orderInfoShowLogistics")
	public void orderInfoShowLogistics(String orderSn) {
		ResponseResult<Map<String,Object>> result = ResponseResult.ERROR("未找该订单的物流信息"); 
		Map<String,Object> map = getLogisticsMap(orderSn);
		if(map!=null && !map.isEmpty()) {
			result.setOkMsg("查询成功");
		}
		result.setData(map);
		ResponseResult.Step(result);
	}
	/**
	 * 
	 * @Title: getLogistics 
	 * @Description: (获得物流信息) 
	 * @param orderSn  订单号
	 * @param isLastOne 是否获取最后一条物流信息  true  返回一条物流信息 false 返回 组合信息
	 * @return  
	 * @date 2019年11月16日 下午4:05:11
	 * @author 董兴隆
	 */
	public List<Map<String,Object>> selectLogisticsMaps(String orderSn) {
		List<Map<String,Object>> maps = new ArrayList<Map<String,Object>>();
		Logistics logistics = getLogistics(orderSn);
		if(logistics==null) {
			return null;
		}
		if(StringUtils.isBlank(logistics.getLogisticsResult())) {
			return null;
		}
		JSONArray array = JSONArray.parseArray(logistics.getLogisticsResult());
		for (int i = array.size()-1; i >=0; i--) {
			JSONObject obj = array.getJSONObject(i);
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("AcceptStation", obj.getString("AcceptStation"));
			map.put("AcceptTime", obj.getString("AcceptTime"));
			maps.add(map);
		}
		return maps;
	}
	/**
	 * 
	 * @Title: getLogistics 
	 * @Description: (获得物流信息) 
	 * @param orderSn  订单号
	 * @param isLastOne 是否获取最后一条物流信息  true  返回一条物流信息 false 返回 组合信息
	 * @return  
	 * @date 2019年11月16日 下午4:05:11
	 * @author 董兴隆
	 */
	public Map<String,Object> getLogisticsMap(String orderSn) {
		Map<String,Object> map = new HashMap<String, Object>();
		Logistics logistics = getLogistics(orderSn);
		if(logistics==null) {
			return null;
		}
		if(StringUtils.isBlank(logistics.getLogisticsResult())) {
			return null;
		}
		JSONArray array = JSONArray.parseArray(logistics.getLogisticsResult());
		JSONObject obj = array.getJSONObject(array.size()-1);
		map.put("AcceptStation", obj.getString("AcceptStation"));
		map.put("AcceptTime", obj.getString("AcceptTime"));
		return map;
	}

	
	/**
	 * 
	 * @Title: getLogistics 
	 * @Description: (通过订单号查看物流信息) 
	 * @param orderSn
	 * @return  
	 * @date 2019年11月18日 上午10:21:59
	 * @author 董兴隆
	 */
	public Logistics getLogistics(String orderSn) {
		Logistics logistics = logisticsService.getOne(new QueryWrapper<Logistics>().eq("order_sn", orderSn),false);
		if(logistics==null) {
			return null;
		}
		if(logistics.getState()!=3) {
			Integer	minutes = Integer.valueOf(GlobalConfigUtil.getKey("SearchTime"));
			//当前时间 减去 最后更新时间 判断是否大于 配置文件中的物流查询间隔时间  如果大于间隔时间则去查询一次物流接口 如果小于间隔时间则取数据库中的数据
			Duration duration  = Duration.between(logistics.getLastUpdateTime(),LocalDateTime.now());
			if(duration.toMinutes() >= minutes) {
				//查询时间大于间隔时间	
				try {
					JSONObject logisticsJson = KdniaoTrackQueryAPI.getOrderTracesByJson(logistics.getBusinessAlias(), logistics.getExpressNo());
					if(logisticsJson!=null && logisticsJson.getBooleanValue("Success")) {
						String logisticsResult = logisticsJson.getJSONArray("Traces").toJSONString();
						//更新物流信息
						logistics.setLogisticsResult(logisticsResult);
						//更新物流状态
						Integer state = logisticsJson.getInteger("State");
						logistics.setState(state);
						logistics.setLastUpdateTime(LocalDateTime.now());
						logisticsService.updateById(logistics);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		return logistics;
	}
}
