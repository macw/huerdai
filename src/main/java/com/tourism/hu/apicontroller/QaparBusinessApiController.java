package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.entity.QaparBusiness;
import com.tourism.hu.mapper.CustomerFriendMapper;
import com.tourism.hu.service.IQaparBusinessService;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
/**
 * 
 * @ClassName: QaparBusinessApiController 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:02:43 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/api/QaparBusinessApi")
public class QaparBusinessApiController {
	
	
    private  Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private CustomerFriendMapper customerFriendMapper;
	@Autowired
	private IQaparBusinessService iQaparBusinessService;

    
    /**
     * 
     *
     * @param 
     */
	@GetMapping("/selectQaparBusiness")
	public void selectQaparBusiness() {
		ResponseResult<List<QaparBusiness>> result = new ResponseResult<>();
		try {
			QueryWrapper<QaparBusiness> qw= new QueryWrapper<QaparBusiness>();
			
			List<QaparBusiness> list = iQaparBusinessService.list(qw);
			result.setOkMsg(list);
		}catch (Exception e) {
			result.setErrorMsg("查询异常");
		}
		ResponseResult.Step(result);
	}




}
