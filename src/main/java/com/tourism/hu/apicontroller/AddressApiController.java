package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.Address;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.Region;
import com.tourism.hu.service.IAddressService;
import com.tourism.hu.service.IRegionService;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
/**
 * 
 * @ClassName: AddressApiController 
 * @Description: 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:00:39 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/api/AddressApi")
public class AddressApiController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IAddressService iAddressService;

    @Resource
    private IRegionService iRegionService;

    /**
     * 查询用户的收货地址
     */

    @RequestMapping("/selectAddressByCustomerId")
    public void selectAddressByCustomerId() {
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录"));
        } else {
            try {
                List<Address> addressList = iAddressService.list(new QueryWrapper<Address>().eq("customer_id", customerInfo.getCustomerId()));
                ResponseResult<List<Address>> result = new ResponseResult<>();
                result.setData(addressList);
                result.setMsg("查询成功！");
                ResponseResult.Step(result);
            } catch (Exception e) {
                e.printStackTrace();
                logger.debug("Exception==="+e.getMessage());
                ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
            }
        }
    }

    /**
     * 将用户收货地址设为默认
     * @param addressId
     */
    @RequestMapping("/updateStatusAddress")
    public void updateStatusAddress(Integer addressId) {
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录"));
        } else {
            try {
                List<Address> addressList = iAddressService.list(new QueryWrapper<Address>().eq("customer_id", customerInfo.getCustomerId()));
                for (Address address : addressList) {
                    address.setAstatus(BaseConstant.DISUSE_STATUS);
                    boolean b = iAddressService.updateById(address);
                    if (!b){
                        ResponseResult.Step(ResponseResult.ERROR("操作失败，请重新操作"));
                    }
                }
                Address address = iAddressService.getById(addressId);
                address.setAstatus(BaseConstant.USE_STATUS);
                boolean b = iAddressService.updateById(address);
                if (!b){
                    ResponseResult.Step(ResponseResult.ERROR("状态更新失败！"));
                }
                ResponseResult.Step(ResponseResult.SUCCESS("操作成功"));
            } catch (Exception e) {
                e.printStackTrace();
                logger.debug("addressId==="+addressId);
                logger.debug("Exception==="+e.getMessage());
                ResponseResult.Step(ResponseResult.ERROR("操作失败！"));
            }
        }
    }


    /**
     * 查询省份
     */
    @RequestMapping("/selectProvince")
    public void selectProvince(){
        try {
            List<Region> provinceList = selectProvinceList();
            ResponseResult<List<Region>> result = new ResponseResult<>();
            result.setData(provinceList);
            result.setMsg("查询成功！");
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Exception==="+e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
        }
    }

    /**
     * 查询省市区
     * @param reg
     * pid = 100000  查询省份
     * pid = 省份id  查询该省份下的所有城市
     * pid = 城市id  查询该城市下的所有区县
     */
    @RequestMapping("/ajaxRegionList")
    public void AjaxRegionList(Region reg) {
        ResponseResult<List<Region>> result = new ResponseResult<>();
        try {
            List<Region> regionList = iRegionService.list(
                    new QueryWrapper<Region>().eq(reg.getPid() != null && reg.getPid() > 0, "pid", reg.getPid())
                            .eq(!StringUtils.isEmpty(reg.getCitycode()), "citycode", reg.getCitycode())
                            .eq(!StringUtils.isEmpty(reg.getYzcode()), "yzcode", reg.getYzcode()));
            result.setData(regionList);
            result.setMsg("查询成功！");
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("region==="+reg.toString());
            logger.debug("Exception==="+e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
        }
    }

    /**
     * 添加 或者更新 收货地址
     */
    @RequestMapping("/addAddressByCustmerInfo")
    public void addAddressByCustmerInfo(Address address){
        logger.debug("address==="+address.toString());
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录"));
            return;
        }
        try {
            String addr = "";
            Region province = iRegionService.getOne(new QueryWrapper<Region>().eq("id", address.getProvince()));
            if (province!=null){
                addr=addr+""+province.getName();
            }
            Region city = iRegionService.getOne(new QueryWrapper<Region>().eq("id", address.getCity()));
            if (city!=null){
                addr=addr+""+city.getName();
            }
            Region dist = iRegionService.getOne(new QueryWrapper<Region>().eq("id", address.getDistrict()));
            if (dist!=null){
                addr=addr+""+dist.getName();
            }
            addr=addr+""+ address.getStreet();
            address.setAddress(addr);
            address.setAstatus(BaseConstant.USE_STATUS);
            address.setCustomerId(customerInfo.getCustomerId());
            ResponseResult<Address> result = new ResponseResult<>();
            //如果id为空，则执行 添加收货地址方法
            if (address.getAddressId()==null) {
                if (iAddressService.save(address)) {
                    result.setMsg("添加成功！");
                } else {
                    result.setMsg("添加失败！");
                }
            }else{
                //如果有 地址id，则更新地址
                if (iAddressService.updateById(address)) {
                    result.setMsg("更新成功！");
                } else {
                    result.setMsg("更新失败！");
                }
            }
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("address==="+address.toString());
            logger.debug("Exception==="+e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("系统异常！"));

        }
    }


    /**
     * 通过地址id 查询收货地址
     */
    @RequestMapping("/selectAddressById")
    public void selectAddressById(Integer addressId) {
        Address address = iAddressService.getById(addressId);
        if (address==null){
            ResponseResult.Step(ResponseResult.ERROR("地址不存在！"));
            return;
        }
        try {
            address.setProvinceName(iRegionService.getById(address.getProvince()).getName());
            address.setCityName(iRegionService.getById(address.getCity()).getName());
            address.setDistrictName(iRegionService.getById(address.getDistrict()).getName());
            ResponseResult<Address> result = new ResponseResult<>();
            result.setMsg("查询成功");
            result.setCode(0);
            result.setData(address);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("adressid==="+addressId);
            logger.debug("exception==="+e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
        }
    }

    /**
     * 通过地址id 删除收货地址
     */
    @RequestMapping("/deleteAddressById")
    public void deleteAddressById(Integer addressId) {
        boolean remove = iAddressService.removeById(addressId);
        if (remove){
            ResponseResult.Step(ResponseResult.SUCCESS("删除成功！"));
            return;
        }
        ResponseResult.Step(ResponseResult.ERROR("删除失败！"));
    }




}
