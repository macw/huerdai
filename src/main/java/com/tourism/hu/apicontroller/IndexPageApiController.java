package com.tourism.hu.apicontroller;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.DedeSysconfig;
import com.tourism.hu.entity.ScenicComment;
import com.tourism.hu.entity.ScenicOrder;
import com.tourism.hu.entity.ScenicRoutes;
import com.tourism.hu.entity.ScenicRoutesClass;
import com.tourism.hu.entity.ScenicRoutesprice;
import com.tourism.hu.entity.Seckill;
import com.tourism.hu.service.IDedeSysconfigService;
import com.tourism.hu.service.IScenicCommentService;
import com.tourism.hu.service.IScenicOrderService;
import com.tourism.hu.service.IScenicRoutesClassService;
import com.tourism.hu.service.IScenicRoutesService;
import com.tourism.hu.service.IScenicRoutespriceService;
import com.tourism.hu.service.ISeckillService;
import com.tourism.hu.util.DateConverterUtil;
import com.tourism.hu.util.GlobalConfigUtil;
import com.tourism.hu.util.ResponseResult;

/**
 * 
 * @ClassName: IndexPageApiController
 * @Description: (首页数据)
 * @author 董兴隆
 * @date 2019年11月1日 下午2:42:23
 *
 */
@RestController
@RequestMapping("/api/indexPageApi")
public class IndexPageApiController extends BaseController {

	private Logger logger = LoggerFactory.getLogger(IndexPageApiController.class);
	
	/**
	 * 首页配置service
	 */
	@Autowired
	private IDedeSysconfigService dedeSysconfigService;

	/**
	 * 线路分类service
	 */
	@Autowired
	private IScenicRoutesClassService routesClassService;
	
	
	/**
	 * 线路评论service
	 */
	@Autowired
	private IScenicCommentService commentService;
	/**
	 * 旅游线路景点价格表service
	 */
	@Autowired
	private IScenicRoutespriceService scenicRoutespriceService;
	
	
	/**
	 * 线路service
	 */
	@Autowired
	private IScenicRoutesService scenicRoutesService;
	
	
	/**
	 * 线路订单service
	 */
	@Autowired
	private IScenicOrderService scenicOrderService;
	
	

	/**
	 * 
	 * @Title: selectIndexBanner
	 * @Description: (获取首页的Banner轮播图)
	 * @date 2019年11月1日 下午3:02:02
	 * @author 董兴隆
	 */
	@GetMapping("/selectIndexBanner")
	public void selectIndexBanner() {
		ResponseResult<List<Map<String, Object>>> result = new ResponseResult<>();
		try {
			QueryWrapper<DedeSysconfig> qw= new QueryWrapper<DedeSysconfig>();
			qw.select("info","value").eq("type", 1);
			List<Map<String, Object>> maps = dedeSysconfigService.listMaps(qw);
			result.setOkMsg(maps);
		}catch (Exception e) {
			result.setErrorMsg("查询异常");
		}
		ResponseResult.Step(result);
	}

	/**
	 * 
	 * @Title: selectIndexTitle
	 * @Description: (获取首页的分类标题 门票-->线路---)
	 * @date 2019年11月1日 下午3:19:35
	 * @author 董兴隆
	 */
	@GetMapping("/selectIndexTitle")
	public void selectIndexTitle() {
		ResponseResult<List<Map<String, Object>>> result = new ResponseResult<>();
		List<Map<String, Object>> maps = new ArrayList<Map<String,Object>>();
		try {
			QueryWrapper<DedeSysconfig> qw= new QueryWrapper<DedeSysconfig>();
			qw.select("varname","info","value").eq("type", 2);
			maps = dedeSysconfigService.listMaps(qw);
		}catch (Exception e) {
			result.setErrorMsg("查询失败");
		}
		result.setOkMsg(maps);
		ResponseResult.Step(result);
	}

	/**
	 * 
	 * @Title: selectRoutesClassify
	 * @Description: (获取首页旅游分类)
	 * @date 2019年11月1日 下午3:39:37
	 * @author 董兴隆
	 */
	@GetMapping("/selectRoutesClassify")
	public void selectRoutesClassify() {
		ResponseResult<List<Map<String, Object>>> result = new ResponseResult<>();
		List<Map<String, Object>> maps = new ArrayList<Map<String,Object>>();
		try {
			QueryWrapper<ScenicRoutesClass> qw= new QueryWrapper<ScenicRoutesClass>();
			qw.select("routesclass_id routesclassId","routesclass_name routesclassName","memo","url").eq("type", 1).eq("status", 1);
			maps = routesClassService.listMaps(qw);
		}catch (Exception e) {
			result.setErrorMsg("查询失败");
		}
		result.setOkMsg(maps);
		ResponseResult.Step(result);
	}

	/**
	 * 
	 * @Title: selectRoutesClass
	 * @Description: (获取首页推荐列表)
	 * @date 2019年11月1日 下午3:40:08
	 * @author 董兴隆
	 */
	@GetMapping("/selectIndexRecommend")
	public void selectIndexRecommend() {
		ResponseResult<List<Map<String, Object>>> result = new ResponseResult<>();
		List<Map<String, Object>> maps = new ArrayList<Map<String,Object>>();
		try {
			QueryWrapper<ScenicRoutesClass> qw= new QueryWrapper<ScenicRoutesClass>();
			qw.select("routesclass_id routesclassId","routesclass_name routesclassName","memo","url").eq("type", 2).eq("leavel", 1).eq("status", 1);
			maps= routesClassService.listMaps(qw);
		}catch (Exception e) {
			result.setErrorMsg("查询失败");
		}
		result.setOkMsg(maps);
		ResponseResult.Step(result);
	}
	/**
	 * 
	 * @Title: selectLineByRoutesClassId 
	 * @Description: (线路接口)   
	 * @date 2019年11月5日 下午1:16:57
	 * @author 董兴隆
	 */
	@GetMapping("/selectLineByRoutesClassId")
	public void selectLineByRoutesClassId(Integer routesclassId,Long pageNo, Long pageSize) {
		logger.info("线路 入参---------------》   routesclassId="+routesclassId);
		logger.info("线路 入参---------------》   pageNo="+pageNo);
		logger.info("线路 入参---------------》   pageSize="+pageSize);
		ResponseResult<List<Map<String, Object>>> result = new ResponseResult<>();
		List<Map<String, Object>> maps = new ArrayList<>();
		//当前时间   
		LocalDateTime date = LocalDateTime.now();
		logger.info("当前时间---------------》   date="+date);
		//当天
		String today = DateConverterUtil.dateFormat(date, DateConverterUtil.date);
		//当月第一天
		//下月第一天 转换成天 做 between   and  查询一个月的销售记录
		LocalDateTime firstday = date.with(TemporalAdjusters.firstDayOfMonth());
		LocalDateTime nextmonth = date.with(TemporalAdjusters.firstDayOfNextMonth());
		String thisMonth = DateConverterUtil.dateFormat(firstday, DateConverterUtil.date);
		String nextMonth = DateConverterUtil.dateFormat(nextmonth, DateConverterUtil.date);
		try {
			//获得线路信息  如果未传入 pageNo 及 pageSize 默认查询 第一页 5条数据
			maps = selectMapsByRoutesClassId(routesclassId,pageNo,pageSize);
			if(maps==null || maps.isEmpty()) {
				logger.info("线路信息为空---------------》   ="+maps);
				result.setOkMsg("当前分类没有添加线路");
				result.setData(maps);
				ResponseResult.Step(result);
				return ;
			}
			logger.info("获取线路信息---------------》   list="+maps.toString());
			for (Map<String, Object> scenicRoutes : maps) {
				Integer routesId = (Integer) scenicRoutes.get("routesId");
				String routesName = (String) scenicRoutes.get("routesName");
				//查询线路Id 一个月的销量
				int monthlySales = scenicOrderService.count(new QueryWrapper<ScenicOrder>().eq("routes_id", routesId).eq("order_status", 3).between("creation_date",thisMonth,nextMonth));
				logger.info(routesName+"线路"+thisMonth+"至"+nextMonth+"销量为"+monthlySales);
				scenicRoutes.put("monthlySales", monthlySales);
				//查看当天是否有活动价格
				BigDecimal sellingPrice = (BigDecimal) scenicRoutes.get("sellingPrice");
				ScenicRoutesprice routePrice = scenicRoutespriceService.getOne(new QueryWrapper<ScenicRoutesprice>().eq("routes_id", routesId).apply("date_format(price_date,'%Y-%m-%d') = {0}", today),false);
				if(routePrice!=null && routePrice.getRoutespriceId()!=null && routePrice.getRoutespriceId()>0) {
					if(routePrice.getActivityPrice()!= null && routePrice.getActivityPrice().compareTo(BigDecimal.ZERO)==1) {
						sellingPrice = routePrice.getActivityPrice();
					}else if(routePrice.getSellingPrice()!= null && routePrice.getSellingPrice().compareTo(BigDecimal.ZERO)==1) {
						sellingPrice = routePrice.getSellingPrice();
					}
				}
//				String now = DateConverterUtil.dateFormat(LocalDateTime.now(), DateConverterUtil.dateTime);
//				Seckill seckill = seckillService.getOne(new QueryWrapper<Seckill>().eq("goods_id", routesId).isNull("collage_count").apply("seckill_begintime<={0}", now).apply("seckill_endtime>{0}", now).last("limit 1"));
//				if(seckill!=null && seckill.getSeckillPrice()!=null && seckill.getSeckillPrice().compareTo(BigDecimal.ZERO)==1) {
//					sellingPrice = seckill.getSeckillPrice();
//				} 
				scenicRoutes.put("sellingPrice", sellingPrice);
				float routesScore = 5;
				//查询线路评分
				ScenicComment comment = commentService.getOne(new QueryWrapper<ScenicComment>().select("avg(goodsScore) goodsScore").eq("scenic_id", routesId),false);
				if(comment!=null && comment.getGoodsScore()!=null && comment.getGoodsScore()>0) {
					routesScore = comment.getGoodsScore();
				}
				scenicRoutes.put("routesScore", routesScore);
			}
			result.setOkMsg("查询成功");
		}catch (Exception e) {
			result.setErrorMsg("查询失败");
			logger.error("selectHotLine  热门线路异常  错误"+e.getMessage());
		}
		result.setData(maps);
		ResponseResult.Step(result);
	}
	
	
	/**
	 * 
	 * @Title: selectByRoutesclassId 
	 * @Description: (通过线路分类获得景点信息) 
	 * @param routesclassId
	 * @return  
	 * @date 2019年11月5日 下午3:12:53
	 * @author 董兴隆
	 */
	private  List<Map<String,Object>> selectMapsByRoutesClassId(Integer routesclassId,Long pageNo, Long PageSize){
		QueryWrapper<ScenicRoutes> qw= new QueryWrapper<ScenicRoutes>();
		qw.eq("routesclass_id", routesclassId);
		if(pageNo==null || pageNo<=0) {
			pageNo = 1l;
		}
		if(PageSize==null || PageSize<=0) {
			PageSize = 5l;
		}
		Page<ScenicRoutes> page = new Page<ScenicRoutes>(pageNo,PageSize);
		qw.select("travel_day travelDay","routesclass_id routesclassId","routes_img routesImg","selling_price sellingPrice","determined","type","city","memo","routes_id routesId","routes_name routesName")
		.eq("status", 1);
		IPage<Map<String, Object>> iPage = scenicRoutesService.pageMaps(page, qw);
		if(iPage!=null) {
			return iPage.getRecords();
		}
		return null;
	}
	
	

}
