package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.GoodsDtlConstant;
import com.tourism.hu.entity.Goods;
import com.tourism.hu.entity.GoodsDtl;
import com.tourism.hu.entity.GoodsDtlImg;
import com.tourism.hu.entity.vo.GoodsDtlType;
import com.tourism.hu.mapper.GoodsDtlMapper;
import com.tourism.hu.service.IGoodsDtlImgService;
import com.tourism.hu.service.IGoodsDtlService;
import com.tourism.hu.service.IGoodsService;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @ClassName: GoodsDtlApiController 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:02:14 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/api/GoodsDtlApi")
public class GoodsDtlApiController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IGoodsDtlService iGoodsDtlService;

    @Resource
    private IGoodsService iGoodsService;

    @Resource
    private IGoodsDtlImgService iGoodsDtlImgService;

    /**
     * 查询商品下的规格信息
     * @param goodsId
     */
    @RequestMapping("/selectGoodsDtlApi")
    public void selectGoodsDtlApi(Integer goodsId){
        try {
            List<GoodsDtl> goodsDtlList = iGoodsDtlService.list(new QueryWrapper<GoodsDtl>().eq("goods_id", goodsId));
            ResponseResult<List<GoodsDtl>> result = new ResponseResult<>();
            result.setData(goodsDtlList);
            ResponseResult.Step(result);

        } catch (Exception e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
        }
    }



    /**
     * 查询商品下的规格信息 ，
     * 集合封装处理
     * @param goodsId
     */
    @RequestMapping("/selectGoodsDtlType")
    public void selectGoodsDtlType(Integer goodsId){
        try {
            List<GoodsDtl> dtlList = iGoodsDtlService.list(new QueryWrapper<GoodsDtl>().eq("goods_id", goodsId).groupBy("type"));
            List<GoodsDtlType> goodsDtlTypeList = new ArrayList<>();

            for (GoodsDtl goodsDtl : dtlList) {
                if (goodsDtl.getType()==GoodsDtlConstant.COLOR_TYPE){
                    GoodsDtlType goodsDtlType = new GoodsDtlType();
                    goodsDtlType.setName("颜色");
                    goodsDtlType.setType(GoodsDtlConstant.COLOR_TYPE);
                    List<GoodsDtl> type = iGoodsDtlService.list(new QueryWrapper<GoodsDtl>().eq("type", GoodsDtlConstant.COLOR_TYPE));
                    for (GoodsDtl dtl : type) {
                        GoodsDtlImg one = iGoodsDtlImgService.getOne(new QueryWrapper<GoodsDtlImg>().eq("goodsdtl_id", dtl.getGoodsdtlId()).eq("status", BaseConstant.USE_STATUS));
                        if (one!=null){
                            dtl.setClassimgAdress(one.getClassimgAdress());
                        }
                    }
                    goodsDtlType.setDtlVoList(type);
                    goodsDtlTypeList.add(goodsDtlType);
                }
                if (goodsDtl.getType()==GoodsDtlConstant.MODEL_TYPE){
                    GoodsDtlType goodsDtlType = new GoodsDtlType();
                    goodsDtlType.setName("型号");
                    goodsDtlType.setType(GoodsDtlConstant.MODEL_TYPE);
                    List<GoodsDtl> type = iGoodsDtlService.list(new QueryWrapper<GoodsDtl>().eq("type", GoodsDtlConstant.MODEL_TYPE));
                    goodsDtlType.setDtlVoList(type);
                    goodsDtlTypeList.add(goodsDtlType);
                }
            }


            ResponseResult<List<GoodsDtlType>> result = new ResponseResult<>();
            result.setData(goodsDtlTypeList);
            result.setMsg("查询成功！");
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("goodsId==="+goodsId);
            logger.debug("Exception==="+e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
        }

    }






}
