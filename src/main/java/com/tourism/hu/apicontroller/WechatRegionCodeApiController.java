package com.tourism.hu.apicontroller;


import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.constant.WechatRegionCodeConstant;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.Region;
import com.tourism.hu.entity.WechatRegionCode;
import com.tourism.hu.mapper.RegionMapper;
import com.tourism.hu.mapper.WechatRegionCodeMapper;
import com.tourism.hu.pay.wxsdk.TokenConfig;
import com.tourism.hu.pay.wxsdk.WeixinConfig;
import com.tourism.hu.service.IRegionService;
import com.tourism.hu.service.IWechatRegionCodeService;
import com.tourism.hu.util.AddressUtils;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.IpAddressUtil;
import com.tourism.hu.util.MsgUtil;
import com.tourism.hu.util.ResponseResult;
import com.tourism.hu.util.ResultUtil;

import net.sf.json.JSONObject;

/**
 * 
 * @ClassName: WechatRegionCodeApiController 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:03:47 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/api/wechatRegionCode")
public class WechatRegionCodeApiController extends BaseController {
    private Logger logger = LoggerFactory.getLogger(getClass());


    @Autowired
    private IWechatRegionCodeService wechatRegionCodeService;

    @Autowired
    private IRegionService regionService;

    @Resource
    private WechatRegionCodeMapper wechatRegionCodeMapper;

    @Resource
    private RegionMapper regionMapper;

    
    /**
   	 * 获取签名
   	 */
   	@RequestMapping(value = "/sign")
   	public void sign(HttpServletRequest request, HttpServletResponse response) throws Exception {
   		ResponseResult<JSONObject> result = ResponseResult.ERROR("获取签名失败");
   		request.setCharacterEncoding("UTF-8");
   		response.setContentType("text/html;charset=utf-8");
   		System.out.println("获取sign");
   		String url = request.getParameter("url");
   		String ticket = TokenConfig.getJsApiTicket();
   		System.out.println(ticket);
   		String noncestr = getRandomString(16);
   		String timestamp = String.valueOf(new Date().getTime()).substring(0, 10);
   		String string = "jsapi_ticket=" + ticket + "&noncestr=" + noncestr + "&timestamp=" + timestamp + "&url=" + url;
   		System.out.println(string);
   		String sign = byteToString(DigestUtils.sha1(string));
   		System.out.println(sign);
   		JSONObject obj = new JSONObject();
   		obj.put("appid", WeixinConfig.appId);
   		obj.put("noncestr", noncestr);
   		obj.put("timestamp", timestamp);
   		obj.put("sign", sign);
   		result = ResponseResult.SUCCESS("查询成功");
   		result.setData(obj);
   		ResponseResult.Step(result);
   	}
       
   	/*
   	 * 获取随机字符串
   	 */
   	public static String getRandomString(int length) { // length表示生成字符串的长度
   		String base = "abcdefghijklmnopqrstuvwxyz0123456789";
   		Random random = new Random();
   		StringBuffer sb = new StringBuffer();
   		for (int i = 0; i < length; i++) {
   			int number = random.nextInt(base.length());
   			sb.append(base.charAt(number));
   		}
   		return sb.toString();
   	}
   	
   	/*
   	 * 字节数组装换成字符串
   	 */
   	private static String byteToString(byte[] digest) {
   		StringBuilder buf = new StringBuilder();
   		for (int i = 0; i < digest.length; i++) {
   			String tempStr = Integer.toHexString(digest[i] & 0xff);
   			if (tempStr.length() == 1) {
   				buf.append("0").append(tempStr);
   			} else {
   				buf.append(tempStr);
   			}
   		}
   		return buf.toString().toLowerCase();
   	}
    
    @RequestMapping("/getWechatRegCode")
    public ModelAndView getWechatRegCode(HttpServletRequest req) {
        ModelAndView mv = new ModelAndView("/wechatArea/showcode");
        ResultUtil<List<WechatRegionCode>> result = ResultUtil.error("查询失败");
        String ip = IpAddressUtil.getIpAddress(req);
        System.out.println("ip地址" + ip);
        logger.debug("ip地址" + ip);
        //ip = "171.15.127.166";
        String json_result = null;
        try {
            json_result = AddressUtils.getAddresses("ip=" + ip, "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject json = JSONObject.fromObject(json_result);
        System.out.println("json数据： " + json);
        logger.debug("阿里返回json数据： " + json);
        String province = JSONObject.fromObject(json.get("data")).get("region").toString();
        logger.debug("阿里返回json省级数据： " + province);
        String city = JSONObject.fromObject(json.get("data")).get("city").toString();
        logger.debug("阿里返回json市级数据： " + city);
        Region provinceInfo = regionService.getOne(new QueryWrapper<Region>().eq("sname", province).eq("level", 1),false);
        if(provinceInfo!=null && provinceInfo.getId()!=null && provinceInfo.getId()>0) {
        	Region cityInfo = regionService.getOne(new QueryWrapper<Region>().eq("sname", city).eq("level", 2).eq("pid", provinceInfo.getId()),false);
        	if (cityInfo != null) {
        		List<WechatRegionCode> list = wechatRegionCodeService.list(new QueryWrapper<WechatRegionCode>().eq("region_city_id", cityInfo.getId()).orderByDesc("update_time"));
        		if (list != null && !list.isEmpty()) {
        			result = ResultUtil.ok("查询成功", list);
        			result.setCode(200);
        			mv.addObject("list", list);
        		} else {
        			mv.setViewName("/wechatArea/agentRegister");
        		}
        	}
        }
        return mv;
    }

    @GetMapping("/toManagerList")
    public ModelAndView toManagerList() {
        ModelAndView mv = new ModelAndView("/wechatArea/ManagerList");
        return mv;
    }

    @GetMapping("/toManagerEdit")
    public ModelAndView toManagerEdit(Integer id) {
        ModelAndView mv = new ModelAndView("/wechatArea/ManagerEdit");
        WechatRegionCode wechatRegionCode = wechatRegionCodeMapper.selectOne(new QueryWrapper<WechatRegionCode>().eq("id", id));
        mv.addObject("wr", wechatRegionCode);
        List<Region> provinceList = selectProvinceList();
        mv.addObject("provinceList", provinceList);
        if (id!=null){
            if(wechatRegionCode.getRegionId()!=null && wechatRegionCode.getRegionId()>0){
                List<Region> cityList = selectByPidRegionList(wechatRegionCode.getRegionId());
                mv.addObject("cityList", cityList);
            }
        }
        return mv;
    }

    @GetMapping("/selectAllByCustomer")
    public DataUtil selectAllByCustomer(int page, int limit) {
        Page<WechatRegionCode> page1 = new Page<>(page, limit);
        CustomerInfo customerInfo = getCustomerInfo();
        IPage<WechatRegionCode> IPage = null;
        if (customerInfo != null) {
            IPage = wechatRegionCodeMapper.selectPage(page1, new QueryWrapper<WechatRegionCode>().eq("customer_id", customerInfo.getCustomerId()));
        } else {
            DataUtil<WechatRegionCode> wechatRegionCodeDataUtil = new DataUtil<>();
            wechatRegionCodeDataUtil.setMsg("获取用户信息失败，请重新登录！");
            return DataUtil.error();
        }
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<WechatRegionCode> List = IPage.getRecords();
        for (WechatRegionCode wechatRegionCode : List) {
            Region region = regionMapper.selectOne(new QueryWrapper<Region>().eq("id", wechatRegionCode.getRegionId()));
            Region region1 = regionMapper.selectOne(new QueryWrapper<Region>().eq("id", wechatRegionCode.getRegionCityId()));
            if (region != null) {
                wechatRegionCode.setProvinceName(region.getSname());
            }
            if (region1 != null) {
                wechatRegionCode.setCityName(region1.getSname());
            }
        }
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;

    }

    @RequestMapping("/saveOrUpdate")
    public MsgUtil saveOrUpdate(MultipartFile file, WechatRegionCode dto,HttpServletRequest request) {
        logger.debug(dto.toString());
        logger.debug("fileName=" + file.getOriginalFilename());
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            return MsgUtil.error("获取用户信息失效，请重新登录");
        }
        boolean b = true;
        //更新
        if (dto.getId() != null && dto.getId() > 0) {
            b = false;
            dto.setUpdateTime(LocalDateTime.now());
        } else {
            //添加
            dto.setCreateTime(LocalDateTime.now());
            dto.setType(WechatRegionCodeConstant.AREA_TYPE);
        }
        if (file != null && file.getSize() > 0) {
            String bastPath = "/wx/areacode";
            String url = "";
            try {
            	url = aliOSSUpload(file, bastPath);
			} catch (IOException e1) {
				e1.printStackTrace();
				return MsgUtil.error("保存图片失败");
			}
            dto.setWechatUrl(url);
             
        }
        if (customerInfo != null) {
            dto.setCustomerId(customerInfo.getCustomerId());
            dto.setCustomerName(customerInfo.getNickname());

        }
        int i;
        logger.debug(dto.toString());
        if (b) {
            i = wechatRegionCodeMapper.insert(dto);
        } else {
            i = wechatRegionCodeMapper.updateById(dto);
        }
        return MsgUtil.flag(i);

    }


    @RequestMapping("/deleteById")
    public ResultUtil<WechatRegionCode> deleteById(WechatRegionCode dto) {
        if (wechatRegionCodeService.removeById(dto.getId())) {
            return ResultUtil.ok("删除成功");
        } else {
            return ResultUtil.error("删除失败");
        }
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids) {
        List list = new ArrayList();
        for (Integer id : ids) {
            list.add(id);
        }
        int i = wechatRegionCodeMapper.deleteBatchIds(list);
        return MsgUtil.flag(i);
    }


}
