package com.tourism.hu.apicontroller;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.tourism.hu.util.base64ToMultipartUtil;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.GoodsComment;
import com.tourism.hu.entity.UploadFile;
import com.tourism.hu.mapper.CustomerInfoMapper;
import com.tourism.hu.mapper.GoodsCommentMapper;
import com.tourism.hu.service.IGoodsCommentService;
import com.tourism.hu.service.IUploadFileService;
import com.tourism.hu.util.DateConverterUtil;
import com.tourism.hu.util.ResponseResult;

/**
 * @author 董兴隆
 * @ClassName: GoodsCommentApiController
 * @Description: ()
 * @date 2019年11月11日 下午5:02:10
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/api/GoodsCommentApi")
public class GoodsCommentApiController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private GoodsCommentMapper goodsCommentMapper;

    @Resource
    private IGoodsCommentService iGoodsCommentService;

    @Resource
    private CustomerInfoMapper customerInfoMapper;

    @Resource
    private IUploadFileService iUploadFileService;


    /**
     * 添加评论
     *
     * @param cl
     */
    @RequestMapping("/addGoodsCommentByGoodsId")
//    public void addGoodsCommentByGoodsId(MultipartFile[] file, GoodsComment cl) {
    public void addGoodsCommentByGoodsId(@Param("base64[]") String base64[], GoodsComment cl) {
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录"));
            return;
        }

        MultipartFile[] file = new MultipartFile[base64.length];

        logger.debug("base========================"+base64.length);
        for (int i = 0; i < base64.length; i++) {
            logger.debug("_=-=------------=-=-=-=======---------=-=-=-=-=-"+i+"---"+base64[i]);
            file[i] = base64ToMultipartUtil.base64ToMultipart(base64[i]);
        }


        try {
            List<UploadFile> uploadFileList = new ArrayList<>();            		
            String uuid = getUUID();
            if (file != null) {
                String basePath = "goodsComm";
                for (MultipartFile multipartFile : file) {
                	if(multipartFile != null && multipartFile.getSize()>0) {
                		UploadFile uploadFile = new UploadFile();
                        String url;
                        if (cl.getGoodsId()==null) {
                            url = aliOSSUpload(multipartFile, basePath);
                        } else {
                            url = aliOSSUpload(multipartFile, basePath, cl.getGoodsId());
                        }
                        uploadFile.setUrl(url);
                		uploadFile.setGuid(uuid);
                		uploadFileList.add(uploadFile);
                	}
                }
                if(!uploadFileList.isEmpty()) {
                	iUploadFileService.saveBatch(uploadFileList);
                }
            }
            cl.setPictureurl(uuid);
            cl.setAuditStatus(BaseConstant.USE_STATUS);
            cl.setAuditTime(LocalDateTime.now());
            cl.setCustomerId(customerInfo.getCustomerId());
            boolean save = iGoodsCommentService.save(cl);
            if (save) {
                ResponseResult.Step(ResponseResult.SUCCESS("评论成功！"));
            } else {
                ResponseResult.Step(ResponseResult.ERROR("评论失败！"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Exception===" + e.getMessage());
            logger.debug("GoodsComment===" + cl);
            ResponseResult.Step(ResponseResult.ERROR("系统故障，评论失败！"));
        }
    }

    /**
     * 查询商品下的所有评论
     * 商品详情页，评论数据列表
     * @param goodsId
     * @param page 第几页
     * @param  limit 每页显示几条
     */
    @RequestMapping("/selectGoodsCommentByGoodsId")
    public void selectGoodsCommentByGoodsId(Integer goodsId,int page, int limit) {
        try {
            Page<GoodsComment> page1 = new Page<>(page, limit);
            IPage<GoodsComment> IPage = iGoodsCommentService.page(page1, new QueryWrapper<GoodsComment>().eq("goods_id", goodsId).eq("audit_status", BaseConstant.USE_STATUS).orderByDesc("audit_time"));
            List<GoodsComment> goodsComments = IPage.getRecords();
//            List<GoodsComment> goodsComments = goodsCommentMapper.selectList(new QueryWrapper<GoodsComment>().eq("goods_id", goodsId).eq("audit_status", GoodsCommentConstant.HAVE_AUDIT_STATUS));
            logger.debug("goodsComments===" + goodsComments);
            for (GoodsComment goodsComment : goodsComments) {
                //设置评论人头像和昵称
                if (goodsComment.getCustomerId() != null) {
                    CustomerInfo customerInfo = customerInfoMapper.selectById(goodsComment.getCustomerId());
                    if (customerInfo != null) {
                        goodsComment.setNickname(customerInfo.getNickname());
                        goodsComment.setCustomerIcon(customerInfo.getCustomerIcon());
                    }
                }
                //设置 评论的图片列表
                List<String> arrayList = new ArrayList<>();
                if (goodsComment.getPictureurl()!=null){
                    List<UploadFile> uploadFileList = iUploadFileService.list(new QueryWrapper<UploadFile>().eq("guid", goodsComment.getPictureurl()));
                    if (uploadFileList!=null && uploadFileList.size()>0){
                        for (UploadFile uploadFile : uploadFileList) {
                            arrayList.add(uploadFile.getUrl());
                        }
                    }
                }
                goodsComment.setPictureList(arrayList);
                //格式化创建时间
                goodsComment.setCreateTime(DateConverterUtil.dateFormat(goodsComment.getAuditTime(), DateConverterUtil.dateTime));
                    //设置订单 商家的回复
                    List<GoodsComment> goodsCommentList = iGoodsCommentService.list(new QueryWrapper<GoodsComment>().eq("comment_parent", goodsComment.getCommentId()).eq("audit_status", BaseConstant.USE_STATUS));
                    for (GoodsComment comment : goodsCommentList) {
                        //设置 评论的图片列表
                        List<String> arrayListList = new ArrayList<>();
                        if (comment.getPictureurl()!=null){
                            List<UploadFile> uploadFileList = iUploadFileService.list(new QueryWrapper<UploadFile>().eq("guid", comment.getPictureurl()));
                            if (uploadFileList!=null && uploadFileList.size()>0){
                                for (UploadFile uploadFile : uploadFileList) {
                                    arrayListList.add(uploadFile.getUrl());
                                }
                            }
                        }
                        comment.setPictureList(arrayListList);
                        //格式化创建时间
                        comment.setCreateTime(DateConverterUtil.dateFormat(comment.getAuditTime(), DateConverterUtil.dateTime));
                    }
                goodsComment.setGoodsCommentList(goodsCommentList);
            }
            logger.debug("goodsCommentList2222====" + goodsComments);

            ResponseResult<List<GoodsComment>> result = new ResponseResult<>();
            result.setData(goodsComments);
            ResponseResult.Step(result);
        } catch (BeansException e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR);
        }
    }
}
