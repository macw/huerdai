package com.tourism.hu.apicontroller;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.SmstestConstant;
import com.tourism.hu.entity.Smstest;
import com.tourism.hu.service.ISmstestService;
import com.tourism.hu.util.DateConverterUtil;
import com.tourism.hu.util.MemberRulesUtil;
import com.tourism.hu.util.MoblieMessageUtil;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 17:16
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/api/smsApi")
public class SmsApiController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private ISmstestService iSmstestService;

    /**
     * 发送验证码
     * @param phone
     */
    @RequestMapping("/sendSMS")
    public void sendSMS(String phone,Integer type) {

        String smsCode = MemberRulesUtil.createRandom(true, 6);
        SendSmsResponse sms = MoblieMessageUtil.sendSms(phone, smsCode,type);
        String code = sms.getCode();
        System.out.println(code);
        try {
            if (code.equals("OK")) {
                Smstest smstest = new Smstest();
                smstest.setCustomerId(null);
                smstest.setMobileNumber(phone);
                smstest.setValidateCode(smsCode);
                smstest.setUsable(BaseConstant.USE_STATUS);
                smstest.setSended(BaseConstant.USE_STATUS);
                smstest.setDeadLine(DateConverterUtil.getAfterFiveMinutes(1));
                boolean save = iSmstestService.save(smstest);
                if (save) {
                    ResponseResult.Step(ResponseResult.SUCCESS);
                    return;
                }
                ResponseResult.Step(ResponseResult.ERROR("系统错误！"));
            } else {
                logger.debug("phone===" + phone);
                ResponseResult.Step(ResponseResult.ERROR("请勿重复发送或系统故障！"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR("异常，发送失败！！！"));
        }
    }

}
