package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 * @ClassName: Smstest 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:11:46 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_smstest")
public class Smstest implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "msale_sms_id", type = IdType.AUTO)
    private Integer msaleSmsId;

    /**
     * 会员ID
     */
    private Integer customerId;

    /**
     * 手机号码
     */
    private String mobileNumber;

    /**
     * 短信验证码
     */
    private String validateCode;

    /**
     * 短信内容
     */
    private String sms;

    /**
     * 失效时间
     */
    private LocalDateTime deadLine;

    /**
     * 是否有效，1-无效，2-有效
     */
    private Integer usable;

    /**
     * 是否已发送，1-未发送，2-已发送
     */
    private Integer sended;


	public Integer getMsaleSmsId() {
		return msaleSmsId;
	}

	public void setMsaleSmsId(Integer msaleSmsId) {
		this.msaleSmsId = msaleSmsId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getValidateCode() {
		return validateCode;
	}

	public void setValidateCode(String validateCode) {
		this.validateCode = validateCode;
	}

	public String getSms() {
		return sms;
	}

	public void setSms(String sms) {
		this.sms = sms;
	}

	public LocalDateTime getDeadLine() {
		return deadLine;
	}

	public void setDeadLine(LocalDateTime deadLine) {
		this.deadLine = deadLine;
	}

	public Integer getUsable() {
		return usable;
	}

	public void setUsable(Integer usable) {
		this.usable = usable;
	}

	public Integer getSended() {
		return sended;
	}

	public void setSended(Integer sended) {
		this.sended = sended;
	}


}
