package com.tourism.hu.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tourism.hu.util.PageUtil;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @ClassName: Coupon 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:04:21 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_coupon")
public class Coupon extends PageUtil implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 红包、代金券ID
     */
    @TableId(type=IdType.AUTO,value="coupon_id")
    private Integer couponId;

    /**
     * 红包、代金券名称
     */
    private String couponName;

    /**
     * 数量
     */
    private Integer count;

    /**
     * 价值，抵扣金额
     */
    private BigDecimal price;

    /**
     * 类型 1红包 2代金券
     */
    private Integer type;

    /**
     * 状态 1已发布 2未发布 3发布中 4已下架 5过期
     */
    private Integer status;

    /**
     * 适用范围 即代金券只能在特定的产品或者渠道上使用
     */
    private String couponRange;

    /**
     * 使用条件 满足多少金额可用
     */
    private BigDecimal conditionsPrice;

    /**
     * 数量预警  达到多少开始预警
     */
    private Integer countWarning;

    /**
     * 发放类型  1自动分配 2手动领取
     */
    private Integer dsseminationType;

    /**
     * 使用类型  1可用 2绑定线路可用
     */
    private Integer useType;

    /**
     * 是否允许重复领取 1可以 2不可以
     */
    private Integer isRepeatedCollection;

    /**
     * 有效期开始日期
     */
    private LocalDateTime validityStartdate;

    /**
     * 有效期截止日期
     */
    private LocalDateTime validityEnddate;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建者名称
     */
    private String createdName;

    /**
     * 创建者id
     */
    private Integer createdId;

    /**
     * 最后更新者id
     */
    private Integer lastUpdatedId;

    /**
     * 最后更新日期
     */
    private LocalDateTime lastUpdateDate;

    /**
     * 最后更新人名称
     */
    private String lastUpdateName;

	public Integer getCouponId() {
		return couponId;
	}

	public void setCouponId(Integer couponId) {
		this.couponId = couponId;
	}

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCouponRange() {
		return couponRange;
	}

	public void setCouponRange(String couponRange) {
		this.couponRange = couponRange;
	}

	public BigDecimal getConditionsPrice() {
		return conditionsPrice;
	}

	public void setConditionsPrice(BigDecimal conditionsPrice) {
		this.conditionsPrice = conditionsPrice;
	}

	public Integer getCountWarning() {
		return countWarning;
	}

	public void setCountWarning(Integer countWarning) {
		this.countWarning = countWarning;
	}

	public Integer getDsseminationType() {
		return dsseminationType;
	}

	public void setDsseminationType(Integer dsseminationType) {
		this.dsseminationType = dsseminationType;
	}

	public Integer getUseType() {
		return useType;
	}

	public void setUseType(Integer useType) {
		this.useType = useType;
	}

	public Integer getIsRepeatedCollection() {
		return isRepeatedCollection;
	}

	public void setIsRepeatedCollection(Integer isRepeatedCollection) {
		this.isRepeatedCollection = isRepeatedCollection;
	}

	public LocalDateTime getValidityStartdate() {
		return validityStartdate;
	}

	public void setValidityStartdate(LocalDateTime validityStartdate) {
		this.validityStartdate = validityStartdate;
	}

	public LocalDateTime getValidityEnddate() {
		return validityEnddate;
	}

	public void setValidityEnddate(LocalDateTime validityEnddate) {
		this.validityEnddate = validityEnddate;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreatedName() {
		return createdName;
	}

	public void setCreatedName(String createdName) {
		this.createdName = createdName;
	}

	public Integer getCreatedId() {
		return createdId;
	}

	public void setCreatedId(Integer createdId) {
		this.createdId = createdId;
	}

	public Integer getLastUpdatedId() {
		return lastUpdatedId;
	}

	public void setLastUpdatedId(Integer lastUpdatedId) {
		this.lastUpdatedId = lastUpdatedId;
	}

	public LocalDateTime getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getLastUpdateName() {
		return lastUpdateName;
	}

	public void setLastUpdateName(String lastUpdateName) {
		this.lastUpdateName = lastUpdateName;
	}


}
