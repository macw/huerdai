package com.tourism.hu.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tourism.hu.util.PageUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @ClassName: Logistics 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:06:16 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_logistics")
public class Logistics  extends PageUtil implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "logistics_id", type = IdType.AUTO)
    private Integer logisticsId;

    /**
     * 订单编号 
     */
    private String orderSn;

    /**
     * 物流单号
     */
    private String expressNo;

    /**
     * 收货人姓名
     */
    private String consigneeRealname;

    /**
     * 联系电话
     */
    private String consigneeTelphone;

    /**
     * 备用联系电话 
     */
    private String consigneeTelphone2;

    /**
     * 收货地址 
     */
    private String address;

    /**
     * 邮政编码 
     */
    private String consigneeZip;

    /**
     *  物流方式
     */
    private String logisticsType;

    /**
     * 物流商家名称
     */
    private String businessName;
    
    /**
     * 物流商家别名
     */
    private String businessAlias;

    /**
     * 物流发货运费
     */
    private BigDecimal logisticsFee;

    /**
     * 快递代收货款费率
     */
    private BigDecimal agencyFee;

    /**
     * 物流成本金额
     */
    private BigDecimal deliveryAmount;

    /**
     * 物流状态
     */
    private Integer orderlogisticsStatus;

    /**
     * 0-无轨迹 1-已揽收 2-在途中 3-签收 4-问题件
     */
    private Integer state;
    /**
     * 物流结算状态 
     */
    private Integer logisticsSettlementStatus;

    /**
     * 物流最后状态描述
     */
    private String logisticsResultLast;

    /**
     * 物流描述
     */
    private String logisticsResult;

    /**
     * 发货时间
     */
    private LocalDateTime logisticsCreateTime;

    /**
     * 物流更新时间
     */
    private LocalDateTime logisticsUpdateTime;

    /**
     * 物流结算时间
     */
    private LocalDateTime logisticsSettlementTime;
    
    /**
     * 物流的最近更新时间
     */
    private LocalDateTime lastUpdateTime;

    /**
     * 物流支付渠道
     */
    private String payChannel;

    /**
     * 物流支付单号
     */
    private String payNo;

    /**
     * 物流公司已对账状态 1已对账 2未对账
     */
    private Integer reconciliationStatus;

    /**
     * 物流公司对账日期 
     */
    private LocalDateTime reconciliationTime;

	public Integer getLogisticsId() {
		return logisticsId;
	}

	public void setLogisticsId(Integer logisticsId) {
		this.logisticsId = logisticsId;
	}

	 
	public String getExpressNo() {
		return expressNo;
	}

	public void setExpressNo(String expressNo) {
		this.expressNo = expressNo;
	}

	public String getConsigneeRealname() {
		return consigneeRealname;
	}

	public void setConsigneeRealname(String consigneeRealname) {
		this.consigneeRealname = consigneeRealname;
	}

	public String getConsigneeTelphone() {
		return consigneeTelphone;
	}

	public void setConsigneeTelphone(String consigneeTelphone) {
		this.consigneeTelphone = consigneeTelphone;
	}

	public String getConsigneeTelphone2() {
		return consigneeTelphone2;
	}

	public void setConsigneeTelphone2(String consigneeTelphone2) {
		this.consigneeTelphone2 = consigneeTelphone2;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getConsigneeZip() {
		return consigneeZip;
	}

	public void setConsigneeZip(String consigneeZip) {
		this.consigneeZip = consigneeZip;
	}

	public String getLogisticsType() {
		return logisticsType;
	}

	public void setLogisticsType(String logisticsType) {
		this.logisticsType = logisticsType;
	}


	public BigDecimal getLogisticsFee() {
		return logisticsFee;
	}

	public void setLogisticsFee(BigDecimal logisticsFee) {
		this.logisticsFee = logisticsFee;
	}

	public BigDecimal getAgencyFee() {
		return agencyFee;
	}

	public void setAgencyFee(BigDecimal agencyFee) {
		this.agencyFee = agencyFee;
	}

	public BigDecimal getDeliveryAmount() {
		return deliveryAmount;
	}

	public void setDeliveryAmount(BigDecimal deliveryAmount) {
		this.deliveryAmount = deliveryAmount;
	}

	public Integer getOrderlogisticsStatus() {
		return orderlogisticsStatus;
	}

	public void setOrderlogisticsStatus(Integer orderlogisticsStatus) {
		this.orderlogisticsStatus = orderlogisticsStatus;
	}

	public Integer getLogisticsSettlementStatus() {
		return logisticsSettlementStatus;
	}

	public void setLogisticsSettlementStatus(Integer logisticsSettlementStatus) {
		this.logisticsSettlementStatus = logisticsSettlementStatus;
	}

	public String getLogisticsResultLast() {
		return logisticsResultLast;
	}

	public void setLogisticsResultLast(String logisticsResultLast) {
		this.logisticsResultLast = logisticsResultLast;
	}

	public String getLogisticsResult() {
		return logisticsResult;
	}

	public void setLogisticsResult(String logisticsResult) {
		this.logisticsResult = logisticsResult;
	}

	public LocalDateTime getLogisticsCreateTime() {
		return logisticsCreateTime;
	}

	public void setLogisticsCreateTime(LocalDateTime logisticsCreateTime) {
		this.logisticsCreateTime = logisticsCreateTime;
	}

	public LocalDateTime getLogisticsUpdateTime() {
		return logisticsUpdateTime;
	}

	public void setLogisticsUpdateTime(LocalDateTime logisticsUpdateTime) {
		this.logisticsUpdateTime = logisticsUpdateTime;
	}

	public LocalDateTime getLogisticsSettlementTime() {
		return logisticsSettlementTime;
	}

	public void setLogisticsSettlementTime(LocalDateTime logisticsSettlementTime) {
		this.logisticsSettlementTime = logisticsSettlementTime;
	}

	public String getPayChannel() {
		return payChannel;
	}

	public void setPayChannel(String payChannel) {
		this.payChannel = payChannel;
	}

	public String getPayNo() {
		return payNo;
	}

	public void setPayNo(String payNo) {
		this.payNo = payNo;
	}

	public Integer getReconciliationStatus() {
		return reconciliationStatus;
	}

	public void setReconciliationStatus(Integer reconciliationStatus) {
		this.reconciliationStatus = reconciliationStatus;
	}

	public LocalDateTime getReconciliationTime() {
		return reconciliationTime;
	}

	public void setReconciliationTime(LocalDateTime reconciliationTime) {
		this.reconciliationTime = reconciliationTime;
	}


}
