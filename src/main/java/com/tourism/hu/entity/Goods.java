package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 
 * @ClassName: Goods 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:05:09 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_goods")
public class Goods implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增主键ID
     */
    @TableId(value = "goods_id", type = IdType.AUTO)
    private Integer goodsId;

		/**
	 * 所属商户id
	 */
	private Integer merchantId;

    /**
     * 商品编码
     */
    private String goodsCode;

    /**
     * 拼音码
     */
    private String opcode;

    /**
     * 商品名称
     */
    private String goodsname;

    /**
     * 商品规格
     */
    private String goodstype;

    /**
     * 商品型号
     */
    private String goodsmodel;

	/**
	 * 商品头图 地址
	 */
	private String pictureUrl;

    /**
     * 商品单价 销售价
     */
    private BigDecimal price;

    /**
     * 出厂价 低价
     */
    private BigDecimal exitPrice;

    /**
     * 活动价
     */
    private BigDecimal activityPrice;

    /**
     * 商品分类ID
     */
    private Integer goodsclassid;


    /**
     * 优惠额度
     */
    private BigDecimal preferentialQuota;

    /**
     * 状态 关联数据字PUB_BASIC_STATUS
     */
    private Integer status;

    /**
     *  限价标志 0-不限，1-限价
     */
    private Integer priceflag;

    /**
     * 库存上限
     */
    private Integer stupperlimit;

    /**
     * 库存下限
     */
    private Integer stlowerlimit;

    /**
     * 商品条码
     */
    private String barcode;

    /**
     * 进项税
     */
    private BigDecimal invat;

    /**
     * 销项税
     */
    private BigDecimal outvat;

    /**
     * 商品描述
     */
    private String descript;

    /**
     * 一级利润，返点x%
     */
    private Integer primaryProfit;

    /**
     * 二级利润，返点x%
     */
    private Integer secondaryProfit;

    /**
     * 创建人id
     */
    private Integer createUserId;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新人id
     */
    private Integer updateUserId;

    /**
     * 更新人员
     */
    private String updateUser;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    @TableField(exist = false)
	private String  goodsClassName;

    /**
	 * 商户名称
	 */
    @TableField(exist = false)
	private String mName;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Integer getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Integer merchantId) {
		this.merchantId = merchantId;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public Integer getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(Integer goodsId) {
		this.goodsId = goodsId;
	}

	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	public String getOpcode() {
		return opcode;
	}

	public void setOpcode(String opcode) {
		this.opcode = opcode;
	}

	public String getGoodsname() {
		return goodsname;
	}

	public void setGoodsname(String goodsname) {
		this.goodsname = goodsname;
	}

	public String getGoodstype() {
		return goodstype;
	}

	public void setGoodstype(String goodstype) {
		this.goodstype = goodstype;
	}

	public String getGoodsmodel() {
		return goodsmodel;
	}

	public void setGoodsmodel(String goodsmodel) {
		this.goodsmodel = goodsmodel;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getExitPrice() {
		return exitPrice;
	}

	public void setExitPrice(BigDecimal exitPrice) {
		this.exitPrice = exitPrice;
	}

	public BigDecimal getActivityPrice() {
		return activityPrice;
	}

	public void setActivityPrice(BigDecimal activityPrice) {
		this.activityPrice = activityPrice;
	}

	public Integer getGoodsclassid() {
		return goodsclassid;
	}

	public void setGoodsclassid(Integer goodsclassid) {
		this.goodsclassid = goodsclassid;
	}

	public BigDecimal getPreferentialQuota() {
		return preferentialQuota;
	}

	public void setPreferentialQuota(BigDecimal preferentialQuota) {
		this.preferentialQuota = preferentialQuota;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getPriceflag() {
		return priceflag;
	}

	public void setPriceflag(Integer priceflag) {
		this.priceflag = priceflag;
	}

	public Integer getStupperlimit() {
		return stupperlimit;
	}

	public void setStupperlimit(Integer stupperlimit) {
		this.stupperlimit = stupperlimit;
	}

	public Integer getStlowerlimit() {
		return stlowerlimit;
	}

	public void setStlowerlimit(Integer stlowerlimit) {
		this.stlowerlimit = stlowerlimit;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public BigDecimal getInvat() {
		return invat;
	}

	public void setInvat(BigDecimal invat) {
		this.invat = invat;
	}

	public BigDecimal getOutvat() {
		return outvat;
	}

	public void setOutvat(BigDecimal outvat) {
		this.outvat = outvat;
	}

	public String getDescript() {
		return descript;
	}

	public void setDescript(String descript) {
		this.descript = descript;
	}

	public Integer getPrimaryProfit() {
		return primaryProfit;
	}

	public void setPrimaryProfit(Integer primaryProfit) {
		this.primaryProfit = primaryProfit;
	}

	public Integer getSecondaryProfit() {
		return secondaryProfit;
	}

	public void setSecondaryProfit(Integer secondaryProfit) {
		this.secondaryProfit = secondaryProfit;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getCreateTime() {
		return createTime;
	}

	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public LocalDateTime getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(LocalDateTime updateTime) {
		this.updateTime = updateTime;
	}

	public String getGoodsClassName() {
		return goodsClassName;
	}

	public void setGoodsClassName(String goodsClassName) {
		this.goodsClassName = goodsClassName;
	}


	public Goods(Integer goodsId, Integer merchantId, String goodsCode, String opcode, String goodsname, String goodstype, String goodsmodel, BigDecimal price, BigDecimal exitPrice, BigDecimal activityPrice, Integer goodsclassid, BigDecimal preferentialQuota, Integer status, Integer priceflag, Integer stupperlimit, Integer stlowerlimit, String barcode, BigDecimal invat, BigDecimal outvat, String descript, Integer primaryProfit, Integer secondaryProfit, Integer createUserId, String createUser, LocalDateTime createTime, Integer updateUserId, String updateUser, LocalDateTime updateTime, String goodsClassName, String mName) {
		this.goodsId = goodsId;
		this.merchantId = merchantId;
		this.goodsCode = goodsCode;
		this.opcode = opcode;
		this.goodsname = goodsname;
		this.goodstype = goodstype;
		this.goodsmodel = goodsmodel;
		this.price = price;
		this.exitPrice = exitPrice;
		this.activityPrice = activityPrice;
		this.goodsclassid = goodsclassid;
		this.preferentialQuota = preferentialQuota;
		this.status = status;
		this.priceflag = priceflag;
		this.stupperlimit = stupperlimit;
		this.stlowerlimit = stlowerlimit;
		this.barcode = barcode;
		this.invat = invat;
		this.outvat = outvat;
		this.descript = descript;
		this.primaryProfit = primaryProfit;
		this.secondaryProfit = secondaryProfit;
		this.createUserId = createUserId;
		this.createUser = createUser;
		this.createTime = createTime;
		this.updateUserId = updateUserId;
		this.updateUser = updateUser;
		this.updateTime = updateTime;
		this.goodsClassName = goodsClassName;
		this.mName = mName;
	}

	public Goods() {
	}

	@Override
	public String toString() {
		return "Goods{" +
				"goodsId=" + goodsId +
				", merchantId=" + merchantId +
				", goodsCode='" + goodsCode + '\'' +
				", opcode='" + opcode + '\'' +
				", goodsname='" + goodsname + '\'' +
				", goodstype='" + goodstype + '\'' +
				", goodsmodel='" + goodsmodel + '\'' +
				", price=" + price +
				", exitPrice=" + exitPrice +
				", activityPrice=" + activityPrice +
				", goodsclassid=" + goodsclassid +
				", preferentialQuota=" + preferentialQuota +
				", status=" + status +
				", priceflag=" + priceflag +
				", stupperlimit=" + stupperlimit +
				", stlowerlimit=" + stlowerlimit +
				", barcode='" + barcode + '\'' +
				", invat=" + invat +
				", outvat=" + outvat +
				", descript='" + descript + '\'' +
				", primaryProfit=" + primaryProfit +
				", secondaryProfit=" + secondaryProfit +
				", createUserId=" + createUserId +
				", createUser='" + createUser + '\'' +
				", createTime=" + createTime +
				", updateUserId=" + updateUserId +
				", updateUser='" + updateUser + '\'' +
				", updateTime=" + updateTime +
				", goodsClassName='" + goodsClassName + '\'' +
				", mName='" + mName + '\'' +
				'}';
	}
}
