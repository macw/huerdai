package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @ClassName: Address 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:03:57 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_address")
public class Address implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 地址id
     */
    @TableId(value = "address_id",type = IdType.AUTO)
    private Integer addressId;

    /**
     * 地址所属用户ID
     */
    private Integer customerId;

    /**
     * 状态 1启用 0禁用
     */
    private Integer astatus;

    /**
     * 收货人姓名
     */
    private String consignee;

    /**
     * 省份，保存是ID
     */
    private Integer province;

	/**
	 * 省份名称
	 * @return
	 */
	@TableField(exist = false)
	private String provinceName;

    /**
     * 市
     */
    private Integer city;

	/**
	 * 城市名称
	 */
	@TableField(exist = false)
	private String cityName;

    /**
     * 区
     */
    private Integer district;

	/**
	 * 区名称
	 */
	@TableField(exist = false)
	private String districtName;
    /**
     * 街道地址
     */
    private String street;

    /**
     * 邮政编码
     */
    private String zipcode;

    /**
     * 电话
     */
    private String telephone;

    /**
     * 移动电话
     */
    private String mobile;

    /**
     * 准确地址
     */
    private String address;

	/**
	 * 用户昵称
	 */
	@TableField(exist = false)
	private String nickname;



}
