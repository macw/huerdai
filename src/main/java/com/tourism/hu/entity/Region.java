package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @ClassName: Region 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:09:11 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_region")
public class Region implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * 区域主键id
	 */
    private Integer id;

    /**
     * 区域名称
     */
    private String name;

    /**
     * 区域上级标识id
     */
    private Integer pid;

    /**
     * 地名简称
     */
    private String sname;

    /**
     * 区域等级
     */
    private Integer level;

    /**
     * 区域编码
     */
    private String citycode;

    /**
     * 邮政编码
     */
    private String yzcode;

    /**
     * 组合名称
     */
    private String mername;

	/**
	 * 地区经度
	 */
	@TableField("Lng")
    private Float Lng;

	/**
	 * 地区维度
	 */
    @TableField("Lat")
    private Float Lat;

	/**
	 * 地区拼音码
	 */
	private String pinyin;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getCitycode() {
		return citycode;
	}

	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}

	public String getYzcode() {
		return yzcode;
	}

	public void setYzcode(String yzcode) {
		this.yzcode = yzcode;
	}

	public String getMername() {
		return mername;
	}

	public void setMername(String mername) {
		this.mername = mername;
	}

	public Float getLng() {
		return Lng;
	}

	public void setLng(Float lng) {
		Lng = lng;
	}

	public Float getLat() {
		return Lat;
	}

	public void setLat(Float lat) {
		Lat = lat;
	}

	public String getPinyin() {
		return pinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}


}
