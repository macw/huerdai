package com.tourism.hu.entity;

import java.io.Serializable;
import java.util.List;
/**
 * 
 * @ClassName: HmFunctionstree 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:05:38 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
public class HmFunctionstree implements Serializable {
    private Integer id;
    private String label;
    private List<HmFunctionstree> children;
    private boolean checked;


    @Override
    public String toString() {
        return "HmFunctionstree{" +
                "id=" + id +
                ", label='" + label + '\'' +
                ", children=" + children +
                ", checked=" + checked +
                '}';
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public Integer getId()
    {
        return this.id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getLabel()
    {
        return this.label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public List<HmFunctionstree> getChildren() {
        return children;
    }

    public void setChildren(List<HmFunctionstree> children) {
        this.children = children;
    }

    public HmFunctionstree() {
    }

    public HmFunctionstree(Integer id, String label, List<HmFunctionstree> children) {
        this.id = id;
        this.label = label;
        this.children = children;
    }
}
