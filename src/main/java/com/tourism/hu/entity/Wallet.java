package com.tourism.hu.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tourism.hu.util.PageUtil;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @ClassName: Wallet 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:12:36 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_wallet")
public class Wallet  extends PageUtil implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户UID
     */
    @TableId(type=IdType.AUTO,value="id")
    private Integer id;

    /**
     * 用户UID
     */
    private Integer cusId;

    /**
     * 用户钱包余额
     */
    private BigDecimal money;

    /**
     * 支付密码
     */
    private String payPassword;

    /**
     * 提现姓名，不可更改
     */
    private String name;

    /**
     * 身份证号
     */
    private String idcard;

    /**
     * 提现支付宝账号
     */
    private String txAlipay;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建者名称
     */
    private String createdName;

    /**
     * 创建者id
     */
    private Integer createdId;

    /**
     * 最后更新者id
     */
    private Integer lastUpdatedId;

    /**
     * 最后更新日期
     */
    private LocalDateTime lastUpdateDate;

    /**
     * 最后更新人名称
     */
    private String lastUpdateName;

	/**
	 * 付款笔数
	 */
	private Integer orderNumber;

	/**
	 * 成交预估收入
	 */
	private BigDecimal readyProfit;

	/**
	 * 结算预估收入
	 */
	private BigDecimal endProfit;

	/**
	 * 本月消费预估收入
	 */
	private BigDecimal monthProfit;

	/**
	 * 本月结算预估收入
	 */
	private BigDecimal monthEndProfit;

	/**
	 * 上月消费预估收入
	 */
	private BigDecimal frontMonthProfit;

	/**
	 * 上月结算预估收入
	 */
	private BigDecimal frontMonthEndProfit;


}
