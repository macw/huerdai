package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 
 * @ClassName: GoodsPic 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:05:34 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_goods_pic")
public class GoodsPic implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 商品图片id
     */
    @TableId(value = "gp_id", type = IdType.AUTO)
    private Integer gpId;

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 作用类型
     */
    private Integer type;

    /**
     * 备注
     */
    private String memo;

    /**
     * 图片地址
     */
    private String pictureurl;

    /**
     * 图片大小
     */
    private Long size;

    /**
     * 图片级别
     */
    private Integer leavel;

    /**
     * 创建人id
     */
    private Integer createUserId;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新人id
     */
    private Integer updateUserId;

    /**
     * 更新人员
     */
    private String updateUser;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

	public GoodsPic() {
	}

	public GoodsPic(Integer gpId, Integer goodsId, Integer type, String memo, String pictureurl, Long size, Integer leavel, Integer createUserId, String createUser, LocalDateTime createTime, Integer updateUserId, String updateUser, LocalDateTime updateTime) {
		this.gpId = gpId;
		this.goodsId = goodsId;
		this.type = type;
		this.memo = memo;
		this.pictureurl = pictureurl;
		this.size = size;
		this.leavel = leavel;
		this.createUserId = createUserId;
		this.createUser = createUser;
		this.createTime = createTime;
		this.updateUserId = updateUserId;
		this.updateUser = updateUser;
		this.updateTime = updateTime;
	}

	public Integer getGpId() {
		return gpId;
	}

	public void setGpId(Integer gpId) {
		this.gpId = gpId;
	}

	public Integer getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(Integer goodsId) {
		this.goodsId = goodsId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getPictureurl() {
		return pictureurl;
	}

	public void setPictureurl(String pictureurl) {
		this.pictureurl = pictureurl;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public Integer getLeavel() {
		return leavel;
	}

	public void setLeavel(Integer leavel) {
		this.leavel = leavel;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getCreateTime() {
		return createTime;
	}

	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public LocalDateTime getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(LocalDateTime updateTime) {
		this.updateTime = updateTime;
	}


}
