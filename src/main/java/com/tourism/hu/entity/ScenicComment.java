package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

 
/**
 * 
 * @ClassName: ScenicComment 
 * @Description: ( 线路评论表) 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:10:27 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_scenic_comment")
public class ScenicComment implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 评论ID
     */
    @TableId(value = "comment_id", type = IdType.AUTO)
    private Integer commentId;

    /**
     * 上级ID
     */
    private Integer commentParent;

    /**
     * 	线路订单ID
     */
    private Integer residenceOrderId;
    
    /**
     * 	线路ID
     */
    private Integer scenicId;

    /**
     * 用户ID
     */
    private Integer customerId;

    /**
     * 用户名
     */
    private String customerName;

    /**
     * 评论标题
     */
    private String title;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 评论图片
     */
    private String pictureurl;

    /**
     * 商品评分 星级
     */
    @TableField("goodsScore")
    private Float goodsScore;

    /**
     * 服务评分 星级
     */
    @TableField("serviceScore")
    private Float serviceScore;

    /**
     * 审核状态：0未审核，1已审核
     */
    private Integer auditStatus;

    /**
     * 评论时间
     */
    private LocalDateTime auditTime;

    /**
     * 最后修改时间
     */
    private LocalDateTime modifiedTime;

	public Integer getCommentId() {
		return commentId;
	}

	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}

	public Integer getCommentParent() {
		return commentParent;
	}

	public void setCommentParent(Integer commentParent) {
		this.commentParent = commentParent;
	}

	public Integer getResidenceOrderId() {
		return residenceOrderId;
	}

	public void setResidenceOrderId(Integer residenceOrderId) {
		this.residenceOrderId = residenceOrderId;
	}

	public Integer getScenicId() {
		return scenicId;
	}

	public void setScenicId(Integer scenicId) {
		this.scenicId = scenicId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPictureurl() {
		return pictureurl;
	}

	public void setPictureurl(String pictureurl) {
		this.pictureurl = pictureurl;
	}

	public Float getGoodsScore() {
		return goodsScore;
	}

	public void setGoodsScore(Float goodsScore) {
		this.goodsScore = goodsScore;
	}

	public Float getServiceScore() {
		return serviceScore;
	}

	public void setServiceScore(Float serviceScore) {
		this.serviceScore = serviceScore;
	}

	public Integer getAuditStatus() {
		return auditStatus;
	}

	public void setAuditStatus(Integer auditStatus) {
		this.auditStatus = auditStatus;
	}

	public LocalDateTime getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(LocalDateTime auditTime) {
		this.auditTime = auditTime;
	}

	public LocalDateTime getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(LocalDateTime modifiedTime) {
		this.modifiedTime = modifiedTime;
	}


}
