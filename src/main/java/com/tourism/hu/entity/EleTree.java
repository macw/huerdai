package com.tourism.hu.entity;

import java.io.Serializable;
import java.util.List;
/**
 * 
 * @ClassName: EleTree 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:05:03 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
public class EleTree implements Serializable {
    private Integer id;
    private String label;
    private List elem;
    private List othis;


    @Override
    public String toString() {
        return "EleTree{" +
                "id=" + id +
                ", label='" + label + '\'' +
                ", elem=" + elem +
                ", othis=" + othis +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List getElem() {
        return elem;
    }

    public void setElem(List elem) {
        this.elem = elem;
    }

    public List getOthis() {
        return othis;
    }

    public void setOthis(List othis) {
        this.othis = othis;
    }
}
