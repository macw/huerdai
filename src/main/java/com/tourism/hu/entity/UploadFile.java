package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 上传文件记录表
 * </p>
 *
 * @author hu
 * @since 2019-12-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_upload_file")
public class UploadFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 文件guid
     */
    private String guid;

    /**
     * 文件存储路径
     */
    private String filepath;

    /**
     * 文件下载地址
     */
    private String url;

    /**
     * 文件类别
     */
    private String filetype;

    /**
     * 用户id
     */
    @TableField("userId")
    private String userId;

    /**
     * 创建时间
     */
    private LocalDateTime addtime;


}
