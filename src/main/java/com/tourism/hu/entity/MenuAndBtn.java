package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.util.List;


/**
 * 
 * @ClassName: MenuAndBtn 
 * @Description: (该类主要是封装menu 对象和 Operation 对象用来给树形菜单展示数据) 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:06:39 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
public class MenuAndBtn implements Serializable {


    /**
     * 菜单id
     */

    private Integer menuId;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 菜单url
     */
    private String menuUrl;

    /**
     * 父菜单id
     */
    private Integer parentId;

    /**
     * 菜单级别（一级菜单，二级菜单，三级菜单）
     */
    private Integer level;

    /**
     * 菜单状态（1启用,0禁用）
     */
    private Integer menuStatus;


    /**
     * 按钮id
     */

    private Integer btnId;

    /**
     * 按钮编号
     */
    private String btnCode;

    /**
     * 按钮名称
     */
    private String btnName;

    /**
     * 按钮标题
     */
    private String btnTitle;

    /**
     * 菜单id
     */
    private Integer btnMenuId;

    public MenuAndBtn() {
    }

    public MenuAndBtn(Integer menuId, String menuName, String menuUrl, Integer parentId, Integer level, Integer menuStatus, Integer btnId, String btnCode, String btnName, String btnTitle, Integer btnMenuId) {
        this.menuId = menuId;
        this.menuName = menuName;
        this.menuUrl = menuUrl;
        this.parentId = parentId;
        this.level = level;
        this.menuStatus = menuStatus;
        this.btnId = btnId;
        this.btnCode = btnCode;
        this.btnName = btnName;
        this.btnTitle = btnTitle;
        this.btnMenuId = btnMenuId;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getMenuStatus() {
        return menuStatus;
    }

    public void setMenuStatus(Integer menuStatus) {
        this.menuStatus = menuStatus;
    }

    public Integer getBtnId() {
        return btnId;
    }

    public void setBtnId(Integer btnId) {
        this.btnId = btnId;
    }

    public String getBtnCode() {
        return btnCode;
    }

    public void setBtnCode(String btnCode) {
        this.btnCode = btnCode;
    }

    public String getBtnName() {
        return btnName;
    }

    public void setBtnName(String btnName) {
        this.btnName = btnName;
    }

    public String getBtnTitle() {
        return btnTitle;
    }

    public void setBtnTitle(String btnTitle) {
        this.btnTitle = btnTitle;
    }

    public Integer getBtnMenuId() {
        return btnMenuId;
    }

    public void setBtnMenuId(Integer btnMenuId) {
        this.btnMenuId = btnMenuId;
    }
}
