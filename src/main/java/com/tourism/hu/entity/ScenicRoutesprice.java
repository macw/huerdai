package com.tourism.hu.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

 
/**
 * 
 * @ClassName: ScenicRoutesprice 
 * @Description: (旅游线路表) 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:11:08 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_scenic_routesprice")
public class ScenicRoutesprice implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 线路ID
     */
    @TableId(value = "routesprice_id", type = IdType.AUTO)
    private Integer routespriceId;

    /**
     * 线路ID
     */
    private Integer routesId;
    
    /**
     * 线路ID
     */
    private Integer spotId;

    public BigDecimal getFloorPrice() {
		return floorPrice;
	}

	public void setFloorPrice(BigDecimal floorPrice) {
		this.floorPrice = floorPrice;
	}

	public BigDecimal getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(BigDecimal sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	/**
     * 状态 1启用 2禁用
     */
    private Integer status;
    

    /**
     * 备注
     */
    private String memo;

    /**
     * 底价
     */
    private BigDecimal floorPrice;
    /**
     * 活动价
     */
    private BigDecimal activityPrice;
    
    /**
     * 线路销售价格
     */
    private BigDecimal sellingPrice;
    

    /**
     * 一级利润，返点x%
     */
    private Integer primaryProfit;

    /**
     * 二级利润，返点x%
     */
    private Integer secondaryProfit;

    /**
     * 优惠额度
     */
    private BigDecimal preferentialQuota;


    /**
     * 日期价格
     */
    private LocalDateTime priceDate;


    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建者名称
     */
    private String createdName;

    /**
     * 创建者id
     */
    private Integer createdId;

    /**
     * 最后更新者id
     */
    private Integer lastUpdatedId;

    /**
     * 最后更新日期
     */
    private LocalDateTime lastUpdateDate;

    /**
     * 最后更新人名称
     */
    private String lastUpdateName;

	public Integer getRoutespriceId() {
		return routespriceId;
	}

	public void setRoutespriceId(Integer routespriceId) {
		this.routespriceId = routespriceId;
	}

	public Integer getRoutesId() {
		return routesId;
	}

	public void setRoutesId(Integer routesId) {
		this.routesId = routesId;
	}

	public Integer getSpotId() {
		return spotId;
	}

	public void setSpotId(Integer spotId) {
		this.spotId = spotId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public BigDecimal getActivityPrice() {
		return activityPrice;
	}

	public void setActivityPrice(BigDecimal activityPrice) {
		this.activityPrice = activityPrice;
	}

	public Integer getPrimaryProfit() {
		return primaryProfit;
	}

	public void setPrimaryProfit(Integer primaryProfit) {
		this.primaryProfit = primaryProfit;
	}

	public Integer getSecondaryProfit() {
		return secondaryProfit;
	}

	public void setSecondaryProfit(Integer secondaryProfit) {
		this.secondaryProfit = secondaryProfit;
	}

	public BigDecimal getPreferentialQuota() {
		return preferentialQuota;
	}

	public void setPreferentialQuota(BigDecimal preferentialQuota) {
		this.preferentialQuota = preferentialQuota;
	}


	public LocalDateTime getPriceDate() {
		return priceDate;
	}

	public void setPriceDate(LocalDateTime priceDate) {
		this.priceDate = priceDate;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreatedName() {
		return createdName;
	}

	public void setCreatedName(String createdName) {
		this.createdName = createdName;
	}

	public Integer getCreatedId() {
		return createdId;
	}

	public void setCreatedId(Integer createdId) {
		this.createdId = createdId;
	}

	public Integer getLastUpdatedId() {
		return lastUpdatedId;
	}

	public void setLastUpdatedId(Integer lastUpdatedId) {
		this.lastUpdatedId = lastUpdatedId;
	}

	public LocalDateTime getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getLastUpdateName() {
		return lastUpdateName;
	}

	public void setLastUpdateName(String lastUpdateName) {
		this.lastUpdateName = lastUpdateName;
	}


    
}
