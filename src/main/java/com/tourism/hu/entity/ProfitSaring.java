package com.tourism.hu.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tourism.hu.util.PageUtil;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.experimental.Accessors;


/**
 * 
 * @ClassName: ProfitSaring 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:08:41 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Accessors(chain = true)
@TableName("fz_profit_saring")
public class ProfitSaring extends PageUtil implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 分成主键
     */
    @TableId(type=IdType.AUTO,value="ps_id")
    private Integer psId;

    /**
     * 分成名称
     */
    private String psName;

    /**
     * 代理商主键
     */
    private Integer agenId;

	/**
	 * 代理商利润分成百分比
	 */
	private Integer agentPercentage;

	/**
	 * 平台利润分成百分比
	 */
	private Integer platformPercentage;

	/**
	 * 用户利润分成百分比
	 */
	private Integer pPercentage;
	/**
	 * 上级利润分成百分比
	 */
	@TableField(value = "parent_p_percentage")
	private Integer parentPPercentage;

    /**
     * 1普通用户  2会员
     */
    private Integer type;

    /**
     * 旅游线路订单id
     */
    private Integer scenicOrderId;

    /**
     * 状态 1 旅游 2 商品
     */
    private Integer status;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建者名称
     */
    private String createdName;

    /**
     * 创建者id
     */
    private Integer createdId;

    /**
     * 最后更新者id
     */
    private Integer lastUpdatedId;

    /**
     * 最后更新日期
     */
    private LocalDateTime lastUpdateDate;

    /**
     * 最后更新人名称
     */
    private String lastUpdateName;



	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Integer getPsId() {
		return psId;
	}

	public void setPsId(Integer psId) {
		this.psId = psId;
	}

	public String getPsName() {
		return psName;
	}

	public void setPsName(String psName) {
		this.psName = psName;
	}

	public Integer getAgenId() {
		return agenId;
	}

	public void setAgenId(Integer agenId) {
		this.agenId = agenId;
	}

	public Integer getAgentPercentage() {
		return agentPercentage;
	}

	public void setAgentPercentage(Integer agentPercentage) {
		this.agentPercentage = agentPercentage;
	}

	public Integer getPlatformPercentage() {
		return platformPercentage;
	}

	public void setPlatformPercentage(Integer platformPercentage) {
		this.platformPercentage = platformPercentage;
	}

	public Integer getpPercentage() {
		return pPercentage;
	}

	public void setpPercentage(Integer pPercentage) {
		this.pPercentage = pPercentage;
	}

	public Integer getParentPPercentage() {
		return parentPPercentage;
	}

	public void setParentPPercentage(Integer parentPPercentage) {
		this.parentPPercentage = parentPPercentage;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getScenicOrderId() {
		return scenicOrderId;
	}

	public void setScenicOrderId(Integer scenicOrderId) {
		this.scenicOrderId = scenicOrderId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreatedName() {
		return createdName;
	}

	public void setCreatedName(String createdName) {
		this.createdName = createdName;
	}

	public Integer getCreatedId() {
		return createdId;
	}

	public void setCreatedId(Integer createdId) {
		this.createdId = createdId;
	}

	public Integer getLastUpdatedId() {
		return lastUpdatedId;
	}

	public void setLastUpdatedId(Integer lastUpdatedId) {
		this.lastUpdatedId = lastUpdatedId;
	}

	public LocalDateTime getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getLastUpdateName() {
		return lastUpdateName;
	}

	public void setLastUpdateName(String lastUpdateName) {
		this.lastUpdateName = lastUpdateName;
	}


}
