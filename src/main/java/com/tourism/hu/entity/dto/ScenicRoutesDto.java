package com.tourism.hu.entity.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.tourism.hu.entity.ScenicComment;
import com.tourism.hu.entity.ScenicRoutes;
import com.tourism.hu.entity.ScenicRoutesspot;
import com.tourism.hu.entity.ScenicSpot;

/**
 * <p>
 * 旅游线路表
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public class ScenicRoutesDto extends ScenicRoutes implements Serializable {

    private static final long serialVersionUID = 1L;
 
    //路线包含的景点名称
    private String spotNames;
    
    private String spotIds;
    
    private String routesClassName;
    
    private String operatorName;
    
    /**
     * 
     */
    private Integer monthlySales;
    
    /**
     * 	评分
     */
    private float routesScore;
    
    /**
     *    日期集合
     */
    private List<Map<String,Object>> dates;
    
    /**
     *   评论集合
     */
    private List<ScenicComment> commentList;
    
    //路线包含的景点ids
    private List<ScenicSpot> spotList;
    
    //线路景点
    private List<ScenicRoutesspot> routesspotList;
    
    //公司名称
    private String tName;
    
    //线路
    private String line;
    
    //页面的返回金额
    private BigDecimal returnPrice;
    
    
	public BigDecimal getReturnPrice() {
		return returnPrice;
	}

	public void setReturnPrice(BigDecimal returnPrice) {
		this.returnPrice = returnPrice;
	}

 

	public List<Map<String, Object>> getDates() {
		return dates;
	}

	public void setDates(List<Map<String, Object>> dates) {
		this.dates = dates;
	}

	public List<ScenicComment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<ScenicComment> commentList) {
		this.commentList = commentList;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	public String getSpotNames() {
		return spotNames;
	}

	public void setSpotNames(String spotNames) {
		this.spotNames = spotNames;
	}


	public String gettName() {
		return tName;
	}

	public void settName(String tName) {
		this.tName = tName;
	}

	public List<ScenicSpot> getSpotList() {
		return spotList;
	}

	public void setSpotList(List<ScenicSpot> spotList) {
		this.spotList = spotList;
	}

	public String getRoutesClassName() {
		return routesClassName;
	}

	public void setRoutesClassName(String routesClassName) {
		this.routesClassName = routesClassName;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public List<ScenicRoutesspot> getRoutesspotList() {
		return routesspotList;
	}

	public void setRoutesspotList(List<ScenicRoutesspot> routesspotList) {
		this.routesspotList = routesspotList;
	}

	public String getSpotIds() {
		return spotIds;
	}

	public void setSpotIds(String spotIds) {
		this.spotIds = spotIds;
	}

	public Integer getMonthlySales() {
		return monthlySales;
	}

	public void setMonthlySales(Integer monthlySales) {
		this.monthlySales = monthlySales;
	}

	public float getRoutesScore() {
		return routesScore;
	}

	public void setRoutesScore(float routesScore) {
		this.routesScore = routesScore;
	}

}
