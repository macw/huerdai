package com.tourism.hu.entity.dto;

import java.io.Serializable;

import com.tourism.hu.entity.ProfitSaring;


/**
 * 
 * @ClassName: ProfitSaringDto 
 * @Description: (利润分成Dto) 
 * @author 董兴隆
 * @date 2019年10月14日 下午2:59:46 
 *
 */
public class ProfitSaringDto extends ProfitSaring implements Serializable {

    /** 
	 * @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	 */ 
	private static final long serialVersionUID = 1L;
	//代理商名称
	private String agenName;
	//经纪人姓名
	private String customerName;
	//上级经纪人姓名
	private String parentCustomerName;

	public String getAgenName() {
		return agenName;
	}

	public void setAgenName(String agenName) {
		this.agenName = agenName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getParentCustomerName() {
		return parentCustomerName;
	}

	public void setParentCustomerName(String parentCustomerName) {
		this.parentCustomerName = parentCustomerName;
	}
	
	
	
	
	
	

}
