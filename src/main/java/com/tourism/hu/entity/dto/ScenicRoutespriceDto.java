package com.tourism.hu.entity.dto;

import java.io.Serializable;

import com.tourism.hu.entity.ScenicRoutesprice;

/**
 * 
 * @ClassName: ScenicRoutespriceDto
 * @Description: (旅游线路 景点 价格表)
 * @author 董兴隆
 * @date 2019年10月29日 上午9:38:55
 *
 */
public class ScenicRoutespriceDto extends ScenicRoutesprice implements Serializable {

	
	private String tourName;
	
	private String date;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTourName() {
		return tourName;
	}

	public void setTourName(String tourName) {
		this.tourName = tourName;
	}
	

}
