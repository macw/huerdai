package com.tourism.hu.entity.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class SeckillDto implements Serializable {

    /**
     * 秒杀，拼团ID
     */
    private Integer seckillId;

    /**
     * 秒杀，拼团名称
     */
    private String seckillName;


    /**
     * 备注
     */
    private String memo;


    /**
     * 秒杀，拼团价
     */
    private BigDecimal seckillPrice;

    /**
     * 秒杀，拼团商品id,或者景点id
     */
    private Integer goodsId;


    /**
     * 秒杀，拼团开始时间
     */
    private LocalDateTime seckillBegintime;

    /**
     * 秒杀，拼团结束时间
     */
    private LocalDateTime seckillEndtime;

    /**
     * 商品名称
     */
    private String goodsname;

    /**
     * 商品单价
     */
    private BigDecimal price;

    /**
     * 活动价
     */
    private BigDecimal activityPrice;

    /**
     * 图片地址
     */
    private String pictureurl;

    /**
     * 开始时间
     */
    private String begintime;

    /**
     * 结束时间
     */
    private String endtime;


    public Integer getSeckillId() {
        return seckillId;
    }

    public void setSeckillId(Integer seckillId) {
        this.seckillId = seckillId;
    }

    public String getSeckillName() {
        return seckillName;
    }

    public void setSeckillName(String seckillName) {
        this.seckillName = seckillName;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public BigDecimal getSeckillPrice() {
        return seckillPrice;
    }

    public void setSeckillPrice(BigDecimal seckillPrice) {
        this.seckillPrice = seckillPrice;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public LocalDateTime getSeckillBegintime() {
        return seckillBegintime;
    }

    public void setSeckillBegintime(LocalDateTime seckillBegintime) {
        this.seckillBegintime = seckillBegintime;
    }

    public LocalDateTime getSeckillEndtime() {
        return seckillEndtime;
    }

    public void setSeckillEndtime(LocalDateTime seckillEndtime) {
        this.seckillEndtime = seckillEndtime;
    }

    public String getGoodsname() {
        return goodsname;
    }

    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getActivityPrice() {
        return activityPrice;
    }

    public void setActivityPrice(BigDecimal activityPrice) {
        this.activityPrice = activityPrice;
    }

    public String getPictureurl() {
        return pictureurl;
    }

    public void setPictureurl(String pictureurl) {
        this.pictureurl = pictureurl;
    }

    public String getBegintime() {
        return begintime;
    }

    public void setBegintime(String begintime) {
        this.begintime = begintime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public SeckillDto() {
    }

    public SeckillDto(Integer seckillId, String seckillName, String memo, BigDecimal seckillPrice, Integer goodsId, LocalDateTime seckillBegintime, LocalDateTime seckillEndtime, String goodsname, BigDecimal price, BigDecimal activityPrice, String pictureurl, String begintime, String endtime) {
        this.seckillId = seckillId;
        this.seckillName = seckillName;
        this.memo = memo;
        this.seckillPrice = seckillPrice;
        this.goodsId = goodsId;
        this.seckillBegintime = seckillBegintime;
        this.seckillEndtime = seckillEndtime;
        this.goodsname = goodsname;
        this.price = price;
        this.activityPrice = activityPrice;
        this.pictureurl = pictureurl;
        this.begintime = begintime;
        this.endtime = endtime;
    }

    @Override
    public String toString() {
        return "SeckillDto{" +
                "seckillId=" + seckillId +
                ", seckillName='" + seckillName + '\'' +
                ", memo='" + memo + '\'' +
                ", seckillPrice=" + seckillPrice +
                ", goodsId=" + goodsId +
                ", seckillBegintime=" + seckillBegintime +
                ", seckillEndtime=" + seckillEndtime +
                ", goodsname='" + goodsname + '\'' +
                ", price=" + price +
                ", activityPrice=" + activityPrice +
                ", pictureurl='" + pictureurl + '\'' +
                ", begintime='" + begintime + '\'' +
                ", endtime='" + endtime + '\'' +
                '}';
    }
}
