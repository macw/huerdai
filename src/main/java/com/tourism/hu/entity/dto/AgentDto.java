package com.tourism.hu.entity.dto;

import java.io.Serializable;

import com.tourism.hu.entity.Agent;

/**
 * <p>
 * 
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */

public class AgentDto extends Agent implements Serializable {

	
	private static final long serialVersionUID = 1L;

	private String provinceName;

	private String cityName;

	private String districtName;

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}


}
