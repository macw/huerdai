package com.tourism.hu.entity.dto;

import java.io.Serializable;

import com.tourism.hu.entity.ResidenceOrder;



/**
 * 
 * @ClassName: ResidenceOrderDto 
 * @Description: (民宿订单Dto) 
 * @author 董兴隆
 * @date 2019年10月21日 下午3:10:57 
 *
 */
public class ResidenceOrderDto extends ResidenceOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 客栈名称
     */
    private String residenceName;
    
    /**
     * 房间名
     */
    private String residenceroomName;
    
    /**
     * 保险名
     */
    private String insName;
    
    /**
     * 保险内容
     */
    private String content;

	public String getResidenceName() {
		return residenceName;
	}

	public void setResidenceName(String residenceName) {
		this.residenceName = residenceName;
	}

	public String getResidenceroomName() {
		return residenceroomName;
	}

	public void setResidenceroomName(String residenceroomName) {
		this.residenceroomName = residenceroomName;
	}

	public String getInsName() {
		return insName;
	}

	public void setInsName(String insName) {
		this.insName = insName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

    
}
