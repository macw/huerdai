package com.tourism.hu.entity.dto;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tourism.hu.entity.ResidenceRoom;
import com.tourism.hu.util.PageUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @ClassName: ResidenceRoomDto 
 * @Description: (民宿房间Dto) 
 * @author 董兴隆
 * @date 2019年10月17日 下午5:12:16 
 *
 */
public class ResidenceRoomDto extends ResidenceRoom implements Serializable {

	/**
	 * 民宿名称
	 */
	private String residenceName;

	public String getResidenceName() {
		return residenceName;
	}

	public void setResidenceName(String residenceName) {
		this.residenceName = residenceName;
	}
	
	

}
