package com.tourism.hu.entity.dto;

import com.tourism.hu.entity.Invoice;

/**
 * 
 * @ClassName: InvoiceDto 
 * @Description:(发票Dto) 
 * @author 董兴隆
 * @date 2019年9月20日 下午5:14:42 
 *
 */
public class InvoiceDto extends Invoice  {

     
	private String invoiceDateString;

	public String getInvoiceDateString() {
		return invoiceDateString;
	}

	public void setInvoiceDateString(String invoiceDateString) {
		this.invoiceDateString = invoiceDateString;
	}
	
}
