package com.tourism.hu.entity.dto;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tourism.hu.entity.WalletLog;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 钱包变动日志
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-24
 */
/**
 * 
 * @ClassName: WalletLogDto 
 * @Description: (钱包变动日志Dto) 
 * @author 董兴隆
 * @date 2019年10月17日 上午11:06:09 
 *
 */
public class WalletLogDto extends WalletLog implements Serializable {

	private String customerName;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
}
