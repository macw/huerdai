package com.tourism.hu.entity.dto;

import com.tourism.hu.entity.ScenicSpot;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 * @ClassName: ScenicSpotDto 
 * @Description: (旅游景点Dto) 
 * @author 董兴隆
 * @date 2019年9月27日 上午11:35:59 
 *
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class ScenicSpotDto extends ScenicSpot{

    private static final long serialVersionUID = 1L;
 
    /**
     * 	旅游公司名称
     */
    private String tName;
    
    private String provinceName;
    
    private String cityName;
    
    private String districtName;
    
    private String date;
    
	public String gettName() {
		return tName;
	}

	public void settName(String tName) {
		this.tName = tName;
	}
	
	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
    

	 

    
    


}
