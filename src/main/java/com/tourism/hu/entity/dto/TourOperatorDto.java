package com.tourism.hu.entity.dto;

import com.tourism.hu.entity.TourOperator;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class TourOperatorDto extends TourOperator  {

    private static final long serialVersionUID = 1L;

    private String provinceName;
    
    private String cityName;
    
    private String districtName;

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

    
    
}
