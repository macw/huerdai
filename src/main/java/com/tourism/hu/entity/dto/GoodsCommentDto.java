package com.tourism.hu.entity.dto;

import com.tourism.hu.entity.GoodsComment;
import lombok.Data;

@Data
public class GoodsCommentDto extends GoodsComment {
    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 用户真实姓名
     */
    private String customerName;



}
