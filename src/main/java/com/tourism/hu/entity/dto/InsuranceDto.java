package com.tourism.hu.entity.dto;

import java.io.Serializable;

import com.tourism.hu.entity.Insurance;

/**
 * 
 * @ClassName: Insurance 
 * @Description: (保险Dto) 
 * @author 董兴隆
 * @date 2019年10月11日 下午3:40:44 
 *
 */

public class InsuranceDto extends Insurance implements Serializable {

	/** 
	 */ 
	private static final long serialVersionUID = 1L;

	private String expiresStartDate;
	
	private String expiresEndDate;

	public String getExpiresStartDate() {
		return expiresStartDate;
	}

	public void setExpiresStartDate(String expiresStartDate) {
		this.expiresStartDate = expiresStartDate;
	}

	public String getExpiresEndDate() {
		return expiresEndDate;
	}

	public void setExpiresEndDate(String expiresEndDate) {
		this.expiresEndDate = expiresEndDate;
	}


}
