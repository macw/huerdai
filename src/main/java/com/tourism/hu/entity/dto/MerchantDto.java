package com.tourism.hu.entity.dto;

import com.tourism.hu.entity.Merchant;

import lombok.Data;

/**
 * 
 * @ClassName: MerchantDto 
 * @Description: (商户表dto) 
 * @author 董兴隆
 * @date 2019年9月16日 上午10:54:52 
 *
 */
@Data
public class MerchantDto extends Merchant {

    private static final long serialVersionUID = 1L;


}
