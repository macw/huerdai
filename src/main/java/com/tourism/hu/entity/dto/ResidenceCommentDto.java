package com.tourism.hu.entity.dto;

import java.io.Serializable;

import com.tourism.hu.entity.ResidenceComment;

/**
 * <p>
 * 民宿评论表
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */

public class ResidenceCommentDto extends ResidenceComment implements Serializable {

    
    /**
     * 民宿名
     */
    private String residenceName;

	public String getResidenceName() {
		return residenceName;
	}

	public void setResidenceName(String residenceName) {
		this.residenceName = residenceName;
	}
    
    
    

}
