package com.tourism.hu.entity.dto;

import com.tourism.hu.entity.Order;

/**
 * 
 * @ClassName: InvoiceDto 
 * @Description:(发票Dto) 
 * @author 董兴隆
 * @date 2019年9月20日 下午5:14:42 
 *
 */
public class OrderDto extends Order  {

    
	private Integer orderType;

	public Integer getOrderType() {
		return orderType;
	}

	public void setOrderType(Integer orderType) {
		this.orderType = orderType;
	}
	
	
	
}
