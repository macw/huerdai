package com.tourism.hu.entity.dto;

import java.io.Serializable;

import com.tourism.hu.entity.Coupon;

/**
 * 
 * @ClassName: CouponDto 
 * @Description: (优惠券Dto) 
 * @author 董兴隆
 * @date 2019年10月15日 下午2:14:11 
 *
 */
public class CouponDto extends Coupon implements Serializable {

    private static final long serialVersionUID = 1L;

    private String expiresStartDate;
	
	private String expiresEndDate;

	public String getExpiresStartDate() {
		return expiresStartDate;
	}

	public void setExpiresStartDate(String expiresStartDate) {
		this.expiresStartDate = expiresStartDate;
	}

	public String getExpiresEndDate() {
		return expiresEndDate;
	}

	public void setExpiresEndDate(String expiresEndDate) {
		this.expiresEndDate = expiresEndDate;
	}
	
	
	
	
    

}
