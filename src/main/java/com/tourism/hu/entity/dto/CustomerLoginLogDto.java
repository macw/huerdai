package com.tourism.hu.entity.dto;

import com.tourism.hu.entity.CustomerLoginLog;

/**
 * 
 * @ClassName: CustomerLoginLogDto 
 * @Description: (登陆日志dto) 
 * @author 董兴隆
 * @date 2019年9月12日 下午2:52:19 
 *
 */
public class CustomerLoginLogDto extends CustomerLoginLog  {

    
	private static final long serialVersionUID = 1L;
	
	/**登陆人名称*/
	private String customerName;

	/**登陆人名称*/
	public String getCustomerName() {
		return customerName;
	}

	/**登陆人名称*/
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


}
