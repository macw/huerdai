package com.tourism.hu.entity.dto;

import com.tourism.hu.entity.PaymentChannel;

import lombok.Data;


@Data
public class PaymentChannelDto extends PaymentChannel  {

    private static final long serialVersionUID = 1L;

    private String merchantName;
    
    private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
    

}
