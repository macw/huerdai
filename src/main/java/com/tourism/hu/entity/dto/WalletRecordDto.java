package com.tourism.hu.entity.dto;

import java.io.Serializable;

import com.tourism.hu.entity.WalletRecord;

/**
 * 
 * @ClassName: WalletRecord 
 * @Description: (钱包交易Dto) 
 * @author 董兴隆
 * @date 2019年10月16日 下午3:48:28 
 *
 */
public class WalletRecordDto extends WalletRecord implements Serializable {

	
	private String fromUserName;
	
	private String toUserName;

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}
     
	

}
