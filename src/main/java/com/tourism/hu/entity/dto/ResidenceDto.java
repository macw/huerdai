package com.tourism.hu.entity.dto;

import java.io.Serializable;

import com.tourism.hu.entity.Residence;

/**
 * 
 * @ClassName: ResidenceDto
 * @Description: (民宿dto)
 * @author 董兴隆
 * @date 2019年10月17日 下午5:20:07
 *
 */

public class ResidenceDto extends Residence implements Serializable {

	private static final long serialVersionUID = 1L;

}
