package com.tourism.hu.entity.dto;

import java.io.Serializable;

import com.tourism.hu.entity.Wallet;

/**
 * 
 * @ClassName: WalletDto
 * @Description: (用户钱包Dto)
 * @author 董兴隆
 * @date 2019年10月15日 下午5:35:46
 *
 */
public class WalletDto extends Wallet implements Serializable {

	/**
	 * 
	 */
	private String customerName;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

}
