package com.tourism.hu.entity.dto;

import java.io.Serializable;

import com.tourism.hu.entity.ScenicOrder;



public class ScenicOrderDto extends ScenicOrder implements Serializable {

   
	/** 
	 */ 
	private static final long serialVersionUID = 1L;
 
	
	/**
	 * 地址Id
	 */
	private Integer addressId;
	
	
	/**
	 * 数量
	 */
	private Integer num;
	
	
	

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getAddressId() {
		return addressId;
	}

	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}

 

	 
	
	
}
