package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author hu
 * @since 2019-12-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_star")
public class Star implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 达人主键id
     */
    @TableId(value = "star_id", type = IdType.AUTO)
    private Integer starId;

    /**
     * 达人头图
     */
    private String starTitleUrl;

    /**
     * 达人头像
     */
    private String starImageUrl;

    /**
     * 达人姓名
     */
    private String starName;

    /**
     * 达人个性签名
     */
    private String starSign;

    /**
     * 观看数量
     */
    private Integer starVideoCount;

    /**
     * 点赞数量
     */
    private Integer starPriseCount;

    /**
     * 视频路径地址
     */
    private String starVideoUrl;

    /**
     * 视频名称
     */
    private String starVideoTitle;

    /**
     * 视频预览图
     */
    private String starVideoIcon;

    /**
     * 达人简介
     */
    private String starInfo;

    /**
     * 创建人昵称
     */
    private String userName;

    /**
     * 创建人id
     */
    private Integer userId;

    /**
     *排序，越大越靠前
     */
    private Integer starSort;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
