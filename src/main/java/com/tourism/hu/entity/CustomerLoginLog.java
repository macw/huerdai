package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.tourism.hu.util.PageUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @ClassName: CustomerLoginLog 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:04:53 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_customer_login_log")
public class CustomerLoginLog extends PageUtil implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 登陆日志ID
     */
    @TableId(value = "login_id", type = IdType.AUTO)
    private Integer loginId;

    /**
     * 登陆用户ID
     */
    private Integer customerId;

    /**
     * 用户登陆时间
     */
    private LocalDateTime loginTime;

	/**
	 * 访问内容
	 */
	private String logContent;

    /**
     * 登陆IP
     */
    private String loginIp;

    /**
     * 日志类型
     */
    private String loginType;




}
