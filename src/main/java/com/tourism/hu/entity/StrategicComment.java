package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @ClassName: StrategicComment 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:11:51 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_strategic_comment")
public class StrategicComment implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 评论ID
     */
    @TableId(value = "comment_id", type = IdType.AUTO)
    private Integer commentId;

    /**
     * 上级ID
     */
    private Integer commentParent;

    /**
     * 攻略ID
     */
    private Integer strategicId;

    /**
     * 用户ID
     */
    private Integer customerId;

    /**
     * 用户头像
     */
    @TableField(exist = false)
    private String customerIcon;

    /**
     * 用户昵称
     */
    @TableField(exist = false)
    private String nickname;


    /**
     * 评论内容
     */
    private String content;

    /**
     * 审核状态：0未审核，1已审核
     */
    private Integer auditStatus;

    /**
     * 评论时间
     */
    private LocalDateTime auditTime;

    /**
     * 评论时间
     */
    @TableField(exist = false)
    private String createTime;

    /**
     * 最后修改时间
     */
    private LocalDateTime modifiedTime;

    /**
     * 回复的评论
     */
    @TableField(exist = false)
    private List<StrategicComment> strategicCommentList;

}
