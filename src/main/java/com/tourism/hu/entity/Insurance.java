package com.tourism.hu.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tourism.hu.util.PageUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * 
 * @ClassName: Insurance 
 * @Description: (保险表) 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:05:51 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_insurance")
public class Insurance extends PageUtil implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 保险Id
     */
    @TableId(type=IdType.AUTO)
    private Integer insId;
    /**
     * 保险名称
     */
    private String insName;
    /**
     * 状态 1启用 2未启用
     */
    private Integer status;
    /**
     * 保险价格
     */
    private BigDecimal money;
    /**
     * 保险标题
     */
    private String title;
    /**
     * 保险内容
     */
    private String content;
    /**
     * 保额
     */
    private BigDecimal insuredAmount;
    /**
     * 第三方保险接口id 
     */
    private String openid;
    /**
     * 第三方保险接口名称
     */
    private String platform;
    /**
     * 备注
     */
    private String memo;
    /**
     * AccessToken
     */
    private String accessToken;
    /**
     * RefreshToken
     */
    private String refreshToken;
    /**
     * 创建者Id
     */
    private Integer createdId;
    /**
     * 创建人
     */
    private String createdName;
    /**
     * 创建时间
     */
    private LocalDateTime creationDate;
    /**
     * 有效开始时间
     */
    private LocalDateTime expiresBegindate;
    /**
     * 有效结束时间
     */
    private LocalDateTime expiresEnddate;
    /**
     * 最后修改人Id
     */
    private Integer lastUpdatedId;
    /**
     * 最后修改者名称
     */
    private String lastUpdateName;
    /**
     * 最后修改时间
     */
    private LocalDateTime lastUpdateDate;
	public Integer getInsId() {
		return insId;
	}
	public void setInsId(Integer insId) {
		this.insId = insId;
	}
	public String getInsName() {
		return insName;
	}
	public void setInsName(String insName) {
		this.insName = insName;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public BigDecimal getMoney() {
		return money;
	}
	public void setMoney(BigDecimal money) {
		this.money = money;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public BigDecimal getInsuredAmount() {
		return insuredAmount;
	}
	public void setInsuredAmount(BigDecimal insuredAmount) {
		this.insuredAmount = insuredAmount;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	public Integer getCreatedId() {
		return createdId;
	}
	public void setCreatedId(Integer createdId) {
		this.createdId = createdId;
	}
	public String getCreatedName() {
		return createdName;
	}
	public void setCreatedName(String createdName) {
		this.createdName = createdName;
	}
	public LocalDateTime getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}
	public LocalDateTime getExpiresBegindate() {
		return expiresBegindate;
	}
	public void setExpiresBegindate(LocalDateTime expiresBegindate) {
		this.expiresBegindate = expiresBegindate;
	}
	public LocalDateTime getExpiresEnddate() {
		return expiresEnddate;
	}
	public void setExpiresEnddate(LocalDateTime expiresEnddate) {
		this.expiresEnddate = expiresEnddate;
	}
	public Integer getLastUpdatedId() {
		return lastUpdatedId;
	}
	public void setLastUpdatedId(Integer lastUpdatedId) {
		this.lastUpdatedId = lastUpdatedId;
	}
	public String getLastUpdateName() {
		return lastUpdateName;
	}
	public void setLastUpdateName(String lastUpdateName) {
		this.lastUpdateName = lastUpdateName;
	}
	public LocalDateTime getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
    

    
    

}
