package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 
 * @ClassName: Operation 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:07:03 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_operation")
public class Operation implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 按钮id
     */
    @TableId(value = "btn_id", type = IdType.AUTO)
    private Integer btnId;

    /**
     * 按钮编号
     */
    private String btnCode;

    /**
     * 按钮名称
     */
    private String btnName;

    /**
     * 按钮标题
     */
    private String btnTitle;

    /**
     * 菜单id
     */
    private Integer menuId;




	public Integer getBtnId() {
		return btnId;
	}

	public void setBtnId(Integer btnId) {
		this.btnId = btnId;
	}

	public String getBtnCode() {
		return btnCode;
	}

	public void setBtnCode(String btnCode) {
		this.btnCode = btnCode;
	}

	public String getBtnName() {
		return btnName;
	}

	public void setBtnName(String btnName) {
		this.btnName = btnName;
	}

	public String getBtnTitle() {
		return btnTitle;
	}

	public void setBtnTitle(String btnTitle) {
		this.btnTitle = btnTitle;
	}

	public Integer getMenuId() {
		return menuId;
	}

	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}


}
