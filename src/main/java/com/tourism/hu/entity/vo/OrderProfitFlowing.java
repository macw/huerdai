package com.tourism.hu.entity.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.tourism.hu.util.PageUtil;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 16:55
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@Data
public class OrderProfitFlowing extends PageUtil {

    /**
     * 用户头像
     */
    private String customerIcon;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 商品名称
     */
    private String goodsname;

    /**
     * 商品头图 地址
     */
    private String pictureUrl;


    /**
     * 商品订单编号
     */
    private String orderSn;


    /**
     * 订单总金额，=活动价*数量
     */
    private BigDecimal orderPriceAll;


    /**
     * 抵扣总金额
     */
    private BigDecimal subPriceAll;


    /**
     * 商品购买数量
     */
    private Integer number;


    /**
     * 实际支付金额
     */
    private BigDecimal salePrice;

    /**
     * 用户利润金额
     */
    private BigDecimal cusMoney;

    /**
     * 上级利润金额
     */
    private BigDecimal parentcusMoney;

    /**
     * 代理商利润金额
     */
    private BigDecimal agentMoney;





    /**
     * 创建日期
     */
    private LocalDateTime creationDate;


}
