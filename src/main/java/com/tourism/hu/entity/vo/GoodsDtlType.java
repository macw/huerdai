package com.tourism.hu.entity.vo;

import com.tourism.hu.entity.GoodsDtl;

import java.math.BigDecimal;
import java.util.List;

public class GoodsDtlType {

    /**
     * 规格类型名称
     */
    private String name;

    /**
     * 规格类型type
     */
    private Integer type;

    /**
     * 规格列表
     */
    private List<GoodsDtl> dtlVoList;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<GoodsDtl> getDtlVoList() {
        return dtlVoList;
    }

    public void setDtlVoList(List<GoodsDtl> dtlVoList) {
        this.dtlVoList = dtlVoList;
    }
}
