package com.tourism.hu.entity.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import lombok.Data;



/**
 * 
 * @ClassName: makeOrderVo 
 * @Description: (生成订单返回Vo) 
 * @author 董兴隆
 * @date 2019年11月9日 下午4:53:50 
 *
 */
@Data
public class MakeOrderVo  implements Serializable {

	
	private BigDecimal totalPrice;
	
	private Integer payType;
	
	private String orderSn;

	private  Map<String, String> wxConfig;
	
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Integer getPayType() {
		return payType;
	}

	public void setPayType(Integer payType) {
		this.payType = payType;
	}

	public String getOrderSn() {
		return orderSn;
	}

	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}
	
	
}
