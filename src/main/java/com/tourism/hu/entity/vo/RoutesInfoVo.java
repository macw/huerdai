package com.tourism.hu.entity.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class RoutesInfoVo implements Serializable {
	/**
	 * 线路Id
	 */
	private Integer routesId;
	/**
	 * 线路出发城市-结束城市
	 */
	 private String line;
	 /**
	  * 线路图片
	  */
	 private String routesImg;
	 /**
	  * 线路名称
	  */
	 private String routesName;
	 
	 /**
	  * 费用说明
	  */
	 private String routesclassInfo;
	 
	 /**
	  *行程 详情
	  */
	 private String characteristics;
	 /**
	  * 费用说明
	  */
	 private String costDescription;
	 
	 /**
	  *   购买须知
	  */
	 private String instructions;
	 
	 private String city;
	 
	 /**
	  * 销售价
	  */
	 private BigDecimal sellingPrice;
	 
	 
	 /**
	  * 拼团价
	  */
	 private BigDecimal groupPrice;
	 
	 /**
	  * 返回价
	  */
	 private BigDecimal cusMoney;
	 
	 private BigDecimal agentMoney;
	 
	 private BigDecimal parentcusMoney;
	 
	 
	 
	 
	 /**
	  * 活动价格
	  */
	 private BigDecimal activePrice;
	 /**
	  * 日期
	  */
	 private List<Map<String,Object>> dates;
	 
	 /**
	  * 旅游公司Id
	  */
	 private Integer tourId;

	 /**
	  * 旅游公司Name
	  */
	 private String tourName;
	 
	 /**
	  *  旅游多少天
	  */
	 private Integer travelDay;
	 
	 /**
	  * 评论
	  */
	 private List<RoutesCommentVo> comments;
	 
	 
	 private Integer isSeckill;
	 
	 private BigDecimal seckillPrice;
	 
	 private Integer isGroup;
	 
	 
	 /**
	  * 评论条数
	  */
	 private int commentsCount;
	 
	 
	 
	 
	 
	public Integer getRoutesId() {
		return routesId;
	}

	public void setRoutesId(Integer routesId) {
		this.routesId = routesId;
	}

	public int getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(int commentsCount) {
		this.commentsCount = commentsCount;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	public String getRoutesImg() {
		return routesImg;
	}

	public void setRoutesImg(String routesImg) {
		this.routesImg = routesImg;
	}

	public String getRoutesName() {
		return routesName;
	}

	public void setRoutesName(String routesName) {
		this.routesName = routesName;
	}

	public String getRoutesclassInfo() {
		return routesclassInfo;
	}

	public void setRoutesclassInfo(String routesclassInfo) {
		this.routesclassInfo = routesclassInfo;
	}

	public String getCharacteristics() {
		return characteristics;
	}

	public void setCharacteristics(String characteristics) {
		this.characteristics = characteristics;
	}


	public List<Map<String, Object>> getDates() {
		return dates;
	}

	public void setDates(List<Map<String, Object>> dates) {
		this.dates = dates;
	}

	public Integer getTourId() {
		return tourId;
	}

	public void setTourId(Integer tourId) {
		this.tourId = tourId;
	}

	public String getTourName() {
		return tourName;
	}

	public void setTourName(String tourName) {
		this.tourName = tourName;
	}

	public Integer getTravelDay() {
		return travelDay;
	}

	public void setTravelDay(Integer travelDay) {
		this.travelDay = travelDay;
	}

	public List<RoutesCommentVo> getComments() {
		return comments;
	}

	public void setComments(List<RoutesCommentVo> comments) {
		this.comments = comments;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public BigDecimal getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(BigDecimal sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	

	public BigDecimal getGroupPrice() {
		return groupPrice;
	}

	public void setGroupPrice(BigDecimal groupPrice) {
		this.groupPrice = groupPrice;
	}

	public BigDecimal getActivePrice() {
		return activePrice;
	}

	
	public Integer getIsSeckill() {
		return isSeckill;
	}

	public void setIsSeckill(Integer isSeckill) {
		this.isSeckill = isSeckill;
	}

	public BigDecimal getSeckillPrice() {
		return seckillPrice;
	}

	public void setSeckillPrice(BigDecimal seckillPrice) {
		this.seckillPrice = seckillPrice;
	}

	public Integer getIsGroup() {
		return isGroup;
	}

	public void setIsGroup(Integer isGroup) {
		this.isGroup = isGroup;
	}

	public void setActivePrice(BigDecimal activePrice) {
		this.activePrice = activePrice;
	}

	 
	 
	 
	 
}
