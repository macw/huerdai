package com.tourism.hu.entity.vo;

import java.math.BigDecimal;
import java.util.List;

public class GoodsDtlVo {


    /**
     * 商品规格  价格
     */
    private BigDecimal goodsDtlPrice;

    /**
     * 商品规格图片路径
     */
    private String dtlPicUrl;

    /**
     * 商品。规格名称列表
     */
    private List<String> dtlStringList;

    /**
     * 商品所有规格列表
     */
    private List<GoodsDtlType> goodsDtlTypes;

    @Override
    public String toString() {
        return "GoodsDtlVo{" +
                "goodsDtlPrice=" + goodsDtlPrice +
                ", dtlPicUrl='" + dtlPicUrl + '\'' +
                ", dtlStringList=" + dtlStringList +
                ", goodsDtlTypes=" + goodsDtlTypes +
                '}';
    }

    public List<GoodsDtlType> getGoodsDtlTypes() {
        return goodsDtlTypes;
    }

    public void setGoodsDtlTypes(List<GoodsDtlType> goodsDtlTypes) {
        this.goodsDtlTypes = goodsDtlTypes;
    }

    public String getDtlPicUrl() {
        return dtlPicUrl;
    }

    public void setDtlPicUrl(String dtlPicUrl) {
        this.dtlPicUrl = dtlPicUrl;
    }

    public BigDecimal getGoodsDtlPrice() {
        return goodsDtlPrice;
    }

    public void setGoodsDtlPrice(BigDecimal goodsDtlPrice) {
        this.goodsDtlPrice = goodsDtlPrice;
    }

    public List<String> getDtlStringList() {
        return dtlStringList;
    }

    public void setDtlStringList(List<String> dtlStringList) {
        this.dtlStringList = dtlStringList;
    }
}
