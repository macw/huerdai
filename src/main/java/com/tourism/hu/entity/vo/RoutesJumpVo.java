package com.tourism.hu.entity.vo;

import java.io.Serializable;
import java.math.BigDecimal;



/**
 * 
 * @ClassName: makeOrderVo 
 * @Description: (生成订单返回Vo) 
 * @author 董兴隆
 * @date 2019年11月9日 下午4:53:50 
 *
 */
public class RoutesJumpVo  implements Serializable {

	 private String id;
	 
	 private String name;
	 
	 private String remark;
	 
	 private String url;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	 
	 
	 
	 
	
}
