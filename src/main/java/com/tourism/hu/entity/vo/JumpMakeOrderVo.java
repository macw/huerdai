package com.tourism.hu.entity.vo;

import java.io.Serializable;
import java.math.BigDecimal;



/**
 * 
 * @ClassName: jumpMakeOrderVo 
 * @Description: (跳转到生成订单页面的vo) 
 * @author 董兴隆
 * @date 2019年11月9日 上午11:48:37 
 *
 */
public class JumpMakeOrderVo  implements Serializable {

	
	private Integer tourId;
	
	private String tourName;
	
	private Integer num;
	
	private String routesImg;
	
	private Integer routesId;
	
	private String routesName;
	
	private Integer isRefund;
	
	private String phone;
	
	private String name;
	
	private String cardNo;
	
	private String travelTime;
	
	/**
	  * 销售价
	  */
	 private BigDecimal sellingPrice;
	 
	 /**
	  * 返回价
	  */
	 private BigDecimal returnPrice;
	 
	 /**
	  * 活动价格
	  */
	 private BigDecimal activityPrice;

	public Integer getTourId() {
		return tourId;
	}

	public void setTourId(Integer tourId) {
		this.tourId = tourId;
	}
	
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getTourName() {
		return tourName;
	}

	public void setTourName(String tourName) {
		this.tourName = tourName;
	}

	public String getRoutesImg() {
		return routesImg;
	}

	public void setRoutesImg(String routesImg) {
		this.routesImg = routesImg;
	}

	public Integer getRoutesId() {
		return routesId;
	}

	public void setRoutesId(Integer routesId) {
		this.routesId = routesId;
	}

	public String getRoutesName() {
		return routesName;
	}

	public void setRoutesName(String routesName) {
		this.routesName = routesName;
	}

	public Integer getIsRefund() {
		return isRefund;
	}

	public void setIsRefund(Integer isRefund) {
		this.isRefund = isRefund;
	}

	public BigDecimal getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(BigDecimal sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public BigDecimal getReturnPrice() {
		return returnPrice;
	}

	public void setReturnPrice(BigDecimal returnPrice) {
		this.returnPrice = returnPrice;
	}
  
	public BigDecimal getActivityPrice() {
		return activityPrice;
	}

	public void setActivityPrice(BigDecimal activityPrice) {
		this.activityPrice = activityPrice;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTravelTime() {
		return travelTime;
	}

	public void setTravelTime(String travelTime) {
		this.travelTime = travelTime;
	}
	
    
 

	
	
}
