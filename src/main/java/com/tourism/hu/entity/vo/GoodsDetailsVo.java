package com.tourism.hu.entity.vo;

import com.tourism.hu.entity.Equity;
import com.tourism.hu.entity.GoodsPic;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

/**
 * 商品详情信息
 */
@Data
public class GoodsDetailsVo  {

    /**
     * 商品自增主键ID
     */
    private Integer goodsId;
    
    /**
     * 商品是否有规格
     */
    private Integer isDtl;

    /**
     * 商品名称
     */
    private String goodsname;

    /**
     * 商品单价
     */
    private BigDecimal price;

    /**
     * 出厂价 低价
     */
    private BigDecimal exitPrice;

    /**
     * 活动价
     */
    private BigDecimal activityPrice;

    /**
     * 商品实际价格
     */
    private BigDecimal actualPrice;

    /**
     * 省，用户个人返利价格数目
     */
    private BigDecimal savePrice;

    /**
     * 分享赚
     */
    private BigDecimal sharePrice;

    /**
     * 商品头图 地址
     */
    private String pictureUrl;



    /**
     * 查询单个商品顶部头图列表
     */
    private List<GoodsPic> pictureTitelList;

    /**
     * 图片级别
     */
    private Integer leavel;

    /**
     * 红包、代金券名称
     */
    private String couponName;

    /**
     * 优惠券，价值，抵扣金额
     */
    private BigDecimal couponPrice;

    /**
     * 商品够买数量
     */
    private Integer number;

    /**
     * 商品描述
     */
    private String descript;

    /**
     * 优惠额度
     */
    private BigDecimal preferentialQuota;

    /**
     * 商品分类ID
     */
    private Integer goodsclassid;

    /**
     *  限价标志 0-不限，1-限价
     */
    private Integer priceflag;

    /**
     * 一级利润，返点x%
     */
    private Integer primaryProfit;

    /**
     * 二级利润，返点x%
     */
    private Integer secondaryProfit;

    /**
     * 商户主键id
     */

    private Integer merId;

    /**
     * 商户名称
     */
    private String merName;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 商户地址
     */
    private String address;

    /**
     * 省，用户个人 参加拼团 返利价格数目
     */
    private BigDecimal saveGroupPrice;

    /**
     * 商品权益列表
     */
    private List<Equity> equityList;



}
