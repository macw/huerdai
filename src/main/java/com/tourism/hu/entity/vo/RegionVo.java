package com.tourism.hu.entity.vo;

import lombok.Data;

import java.util.List;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 11:01
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@Data
public class RegionVo {

    private String key;

    /**
     * 区域主键id
     */
    private Integer id;

    /**
     * 地名简称
     */
    private String sname;

    private List<RegionVo> regionVos;


}
