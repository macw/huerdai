package com.tourism.hu.entity.vo;

/**
 * 订单状态显示VO
 */
public class OrderStatusVo {

    private String name;

    private Integer number;

    private String link;

    private String picUrl;

    public OrderStatusVo(String name, Integer number, String link, String picUrl) {
        this.name = name;
        this.number = number;
        this.link = link;
        this.picUrl = picUrl;
    }

    public OrderStatusVo() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }
}
