package com.tourism.hu.entity.vo;

import com.tourism.hu.entity.Equity;
import com.tourism.hu.entity.GoodsDtl;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

@Data
public class GoodsOrderDetailsVo extends GoodsVo {

    /**
     * 商品自增主键ID
     */
    private Integer goodsId;

    /**
     * 商品名称
     */
    private String goodsname;

    /**
     * 商品单价
     */
    private BigDecimal price;

    /**
     * 出厂价 低价
     */
    private BigDecimal exitPrice;


    /**
     * 活动价
     */
    private BigDecimal activityPrice;

    /**
     * 商品实际价格
     */
    private BigDecimal actualPrice;

    /**
     * 省，用户个人返利价格数目
     */
    private BigDecimal savePrice;

    /**
     * 商品头图 地址
     */
    private String pictureUrl;

    /**
     * 图片级别
     */
    private Integer leavel;

    /**
     * 红包、代金券名称
     */
    private String couponName;

    /**
     * 优惠券，价值，抵扣金额
     */
    private BigDecimal couponPrice;

    /**
     * 商品够买数量
     */
    private Integer number;


    /**
     * 商户名称
     */
    private String merName;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 商户地址
     */
    private String address;

    /**
     * 订单编号
     */
    private String orderSn;

    /**
     * 订单状态 1已支付 2未支付 3已完成
     */
    private Integer orderStatus;


    /**
     * 商品规格列表
     */
    private String goodsDtlList;

    /**
     * 商品权益列表
     */
    private String equityList;


}
