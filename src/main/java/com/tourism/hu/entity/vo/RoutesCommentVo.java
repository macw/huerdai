package com.tourism.hu.entity.vo;

import java.io.Serializable;
import java.util.List;

public class RoutesCommentVo implements Serializable {

	 private String userName;
	 
	 private String userImg;
	 
	 private String content;
	 
	 private Float grade;
	 
	 private List<RoutesCommentVo> routesComments;

	 private List<String> imgs;
	 
	 
	public List<String> getImgs() {
		return imgs;
	}

	public void setImgs(List<String> imgs) {
		this.imgs = imgs;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserImg() {
		return userImg;
	}

	public void setUserImg(String userImg) {
		this.userImg = userImg;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Float getGrade() {
		return grade;
	}

	public void setGrade(Float grade) {
		this.grade = grade;
	}

	public List<RoutesCommentVo> getRoutesComments() {
		return routesComments;
	}

	public void setRoutesComments(List<RoutesCommentVo> routesComments) {
		this.routesComments = routesComments;
	}
	 
	 
	 
	 
	 
}
