package com.tourism.hu.entity.vo;

import java.io.Serializable;
import java.util.List;

public class GoodsCategoryVo implements Serializable {

    /**
     * 分类ID
     */
    private Integer classId;

    /**
     * 分类名称
     */
    private String className;

    /**
     * 商品列表
     */
    private List<GoodsVo> goodsLists;


    private String link;


    @Override
    public String toString() {
        return "GoodsCategoryVo{" +
                "classId=" + classId +
                ", className='" + className + '\'' +
                ", goodsLists=" + goodsLists +
                ", link='" + link + '\'' +
                '}';
    }



    public GoodsCategoryVo(Integer classId, String className, List<GoodsVo> goodsLists, String link) {
        this.classId = classId;
        this.className = className;
        this.goodsLists = goodsLists;
        this.link = link;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public GoodsCategoryVo() {
    }

    public List<GoodsVo> getGoodsLists() {
        return goodsLists;
    }

    public void setGoodsLists(List<GoodsVo> goodsLists) {
        this.goodsLists = goodsLists;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

}
