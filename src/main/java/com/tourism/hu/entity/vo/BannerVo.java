package com.tourism.hu.entity.vo;

import java.io.Serializable;
import java.math.BigDecimal;



/**
 * 
 * @ClassName: BannerVo 
 * @Description: (Banner图vo) 
 * @author 董兴隆
 * @date 2019年11月10日 上午10:46:45 
 *
 */
public class BannerVo  implements Serializable {

	
	 private String img;
	 
	 private String url;
	 
	 private String remark;

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	 
	 
	 
	
}
