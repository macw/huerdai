package com.tourism.hu.entity.vo;

import com.tourism.hu.util.PageUtil;

import java.math.BigDecimal;

public class GoodsVo extends PageUtil {

    /**
     * 商品自增主键ID
     */
    private Integer goodsId;

    /**
     * 商品名称
     */
    private String goodsname;

    /**
     * 商品头图 地址
     */
    private String pictureUrl;


    /**
     * 商品单价
     */
    private BigDecimal price;

    /**
     * 出厂价 低价
     */
    private BigDecimal exitPrice;

    /**
     * 活动价
     */
    private BigDecimal activityPrice;

    /**
     * 商品实际价格
     */
    private BigDecimal actualPrice;

    /**
     * 省，用户个人返利价格数目
     */
    private BigDecimal savePrice;


    /**
     * 图片级别
     */
    private Integer leavel;


    /**
     * 红包、代金券名称
     */
    private String couponName;

    /**
     * 优惠券，价值，抵扣金额
     */
    private BigDecimal couponPrice;

    /**
     * 商品够买数量
     */
    private Integer number;


    @Override
    public String toString() {
        return "GoodsVo{" +
                "goodsId=" + goodsId +
                ", goodsname='" + goodsname + '\'' +
                ", pictureUrl='" + pictureUrl + '\'' +
                ", price=" + price +
                ", exitPrice=" + exitPrice +
                ", activityPrice=" + activityPrice +
                ", actualPrice=" + actualPrice +
                ", savePrice=" + savePrice +
                ", leavel=" + leavel +
                ", couponName='" + couponName + '\'' +
                ", couponPrice=" + couponPrice +
                ", number=" + number +
                '}';
    }


    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsname() {
        return goodsname;
    }

    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getExitPrice() {
        return exitPrice;
    }

    public void setExitPrice(BigDecimal exitPrice) {
        this.exitPrice = exitPrice;
    }

    public BigDecimal getActivityPrice() {
        return activityPrice;
    }

    public void setActivityPrice(BigDecimal activityPrice) {
        this.activityPrice = activityPrice;
    }

    public BigDecimal getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(BigDecimal actualPrice) {
        this.actualPrice = actualPrice;
    }

    public BigDecimal getSavePrice() {
        return savePrice;
    }

    public void setSavePrice(BigDecimal savePrice) {
        this.savePrice = savePrice;
    }

    public Integer getLeavel() {
        return leavel;
    }

    public void setLeavel(Integer leavel) {
        this.leavel = leavel;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public BigDecimal getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(BigDecimal couponPrice) {
        this.couponPrice = couponPrice;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
}
