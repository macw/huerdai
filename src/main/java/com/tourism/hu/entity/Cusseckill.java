package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @ClassName: Cusseckill 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:04:31 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_cusseckill")
public class Cusseckill implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "fz_cusseckill_id", type = IdType.AUTO)
    private Integer fzCusseckillId;

    /**
     * 状态 1启用 2禁用
     */
    private Integer status;

    /**
     * 备注,1为团长，0为团员
     */
    private String memo;

    /**
     * 拼团组，商品code加iD加uid
     */
    private String seckillgroup;

    /**
     * 拼团秒杀ID
     */
    private Integer spotId;

    /**
     * 用户ID
     */
    private Integer cusId;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建者名称
     */
    private String createdName;

    /**
     * 创建者id
     */
    private Integer createdId;

    /**
     * 最后更新者id
     */
    private Integer lastUpdatedId;

    /**
     * 最后更新日期
     */
    private LocalDateTime lastUpdateDate;

    /**
     * 最后更新人名称
     */
    private String lastUpdateName;

    /**
     * 拼团名称
     */
    @TableField(exist = false)
    private String seckillName;

    /**
     * 用户昵称
     */
    @TableField(exist = false)
    private String nickname;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getFzCusseckillId() {
        return fzCusseckillId;
    }

    public void setFzCusseckillId(Integer fzCusseckillId) {
        this.fzCusseckillId = fzCusseckillId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Integer getSpotId() {
        return spotId;
    }

    public void setSpotId(Integer spotId) {
        this.spotId = spotId;
    }

    public Integer getCusId() {
        return cusId;
    }

    public void setCusId(Integer cusId) {
        this.cusId = cusId;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreatedName() {
        return createdName;
    }

    public void setCreatedName(String createdName) {
        this.createdName = createdName;
    }

    public Integer getCreatedId() {
        return createdId;
    }

    public void setCreatedId(Integer createdId) {
        this.createdId = createdId;
    }

    public Integer getLastUpdatedId() {
        return lastUpdatedId;
    }

    public void setLastUpdatedId(Integer lastUpdatedId) {
        this.lastUpdatedId = lastUpdatedId;
    }

    public LocalDateTime getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getLastUpdateName() {
        return lastUpdateName;
    }

    public void setLastUpdateName(String lastUpdateName) {
        this.lastUpdateName = lastUpdateName;
    }

    public String getSeckillName() {
        return seckillName;
    }

    public void setSeckillName(String seckillName) {
        this.seckillName = seckillName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getSeckillgroup() {
        return seckillgroup;
    }

    public void setSeckillgroup(String seckillgroup) {
        this.seckillgroup = seckillgroup;
    }
}
