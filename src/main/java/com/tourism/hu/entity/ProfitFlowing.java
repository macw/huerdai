package com.tourism.hu.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * 
 * @ClassName: ProfitFlowing 
 * @Description: (利润分成表) 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:08:31 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_profit_flowing")
public class ProfitFlowing implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 分成主键
     */
    @TableId(type=IdType.AUTO,value="pf_id")
    private Integer pfId;

    /**
     * 分成名称
     */
    private String pfName;

    /**
     * 商品订单编号
     */
    private String orderSn;
    /**
     * 支付流水号
     */
    private String serialNumber;

    
    /**
     * 商品原价（销售价）
     */
    private BigDecimal price;

    /**
     * 活动价（秒杀价，拼团价）
     */
    private BigDecimal activityPrice;

    /**
     * 优惠卷抵扣价值
     */
    private BigDecimal couponPrice;

    /**
     * 满减抵扣价值
     */
    private BigDecimal fillPrice;

    /**
     * 其他抵扣1
     */
    private BigDecimal otherPriceOne;

    /**
     * 其他抵扣2
     */
    private BigDecimal otherPriceTwo;

    /**
     * 其他抵扣3
     */
    private BigDecimal otherPriceThree;

	/**
	 * 订单总金额，=活动价*数量
	 */
	private BigDecimal orderPriceAll;


	/**
	 * 抵扣总金额
	 */
	private BigDecimal subPriceAll;

    /**
     * 商品低价
     */
    private BigDecimal exitPrice;

    /**
     * 总利润
     */
    private BigDecimal totalMoney;

    /**
     * 商品购买数量
     */
    private Integer number;
    
    /**
     * 是否已支付
     */
    private Integer isPay;

    /**
     * 实际支付金额
     */
    private BigDecimal salePrice;

    /**
     * 用户利润金额
     */
    private BigDecimal cusMoney;

    /**
     * 上级利润金额
     */
    private BigDecimal parentcusMoney;

    /**
     * 代理商利润金额
     */
    private BigDecimal agentMoney;

    /**
     * 平台利润金额
     */
    private BigDecimal platformMoney;

    /**
     * 1,已审核，0,未审核
     */
    private Integer state;

    /**
     * 用户ID
     */
    private Integer cusId;

    /**
     * 上级ID
     */
    private Integer parentId;

    /**
     * 旅游线路订单id
     */
    private Integer scenicOrderId;

    /**
     * 状态 1启用 2禁用
     */
    private Integer status;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建者名称
     */
    private String createdName;

    /**
     * 创建者id
     */
    private Integer createdId;

    /**
     * 最后更新者id
     */
    private Integer lastUpdatedId;

    /**
     * 最后更新日期
     */
    private LocalDateTime lastUpdateDate;

    /**
     * 最后更新人名称
     */
    private String lastUpdateName;

    /**
     * 1线路订单，2商品订单
     */
    private Integer type;

}
