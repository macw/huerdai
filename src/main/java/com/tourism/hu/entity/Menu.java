package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @ClassName: Menu 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:06:33 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_menu")
public class Menu implements Serializable,Comparable<Menu> {

    private static final long serialVersionUID = 1L;

    /**
     * 菜单id
     */
    @TableId(value = "menu_id", type = IdType.AUTO)
    private Integer menuId;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 菜单url
     */
    private String menuUrl;

    /**
     * 父菜单id
     */
    private Integer parentId;

    /**
     * 菜单级别（一级菜单，二级菜单，三级菜单）
     */
    private Integer level;

    /**
     * 菜单状态（1启用,0禁用）
     */
    private Integer menuStatus;

	/**
	 * 排序依据， 数字越大越靠前
	 */
	private Integer menuOrder;

	/**
	 * 二级菜单列表
	 */
	@TableField(exist = false)
    private List<Menu> menuList;

	/**
	 * 二级菜单下的，按钮列表
	 */
	@TableField(exist = false)
    private List<Operation> operationList;


	public int compareTo(Menu o) {
		return this.menuOrder.compareTo(o.getMenuOrder());
	}




}

