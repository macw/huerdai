package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.List;

import com.tourism.hu.util.PageUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @ClassName: GoodsComment 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:05:24 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_goods_comment")
public class GoodsComment extends PageUtil implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 评论ID
     */
    @TableId(value = "comment_id", type = IdType.AUTO)
    private Integer commentId;

	/**
	 * 回复评论的上级ID
	 */
	private Integer commentParent;

    /**
     * 商品ID
     */
    private Integer goodsId;

    /**
     * 订单ID
     */
    private Integer orderId;

	/**
	 * 订单编号
	 */
	private String orderSn;

    /**
     * 用户ID
     */
    private Integer customerId;

    /**
     * 评论标题
     */
    private String title;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 评论图片
     */
    private String pictureurl;

	/**
	 * 图片列表
	 */
	@TableField(exist = false)
	private List<String> pictureList;

    /**
     * 商品评分 星级
     */
    @TableField("goodsScore")
    private Integer goodsScore;

    /**
     * 服务评分 星级
     */
    @TableField("serviceScore")
    private Integer serviceScore;

    /**
     * 审核状态：0未审核，1已审核
     */
    private Integer auditStatus;

    /**
     * 评论时间
     */
    private LocalDateTime auditTime;

    /**
     * 评论发布时间
     */
    @TableField(exist = false)
    private String createTime;

    /**
     * 最后修改时间
     */
    private LocalDateTime modifiedTime;

	/**
	 * 用户头像
	 */
	@TableField(exist = false)
	private String customerIcon;

	/**
	 * 昵称
	 */
	@TableField(exist = false)
	private String nickname;

	/**
	 * 回复的评论列表
	 */
	@TableField(exist = false)
	private List<GoodsComment> goodsCommentList;
}
