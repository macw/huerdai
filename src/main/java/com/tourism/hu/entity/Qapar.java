package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.tourism.hu.util.PageUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * 
 * @ClassName: Qapar 
 * @Description: (凭证表) 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:08:53 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_qapar")
public class Qapar extends PageUtil implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "qapar_ID", type = IdType.AUTO)
    private Integer qaparId;

    /**
     * 公司段
     */
    @TableField("ORG_TITLE")
    private String orgTitle;

    /**
     * 类型1发票 2其他
     */
    @TableField("INV_REC_FLAG")
    private Integer invRecFlag;

    /**
     * 期间
     */
    @TableField("ACCOUNTING_DATE")
    private Integer accountingDate;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建者名称
     */
    private String createdName;

    /**
     * 创建者id
     */
    private Integer createdId;

    /**
     * 最后更新者id
     */
    private Integer lastUpdatedId;

    /**
     * 最后更新日期
     */
    private LocalDateTime lastUpdateDate;

    /**
     * 最后更新人名称
     */
    private String lastUpdateName;

	public Integer getQaparId() {
		return qaparId;
	}

	public void setQaparId(Integer qaparId) {
		this.qaparId = qaparId;
	}

	public String getOrgTitle() {
		return orgTitle;
	}

	public void setOrgTitle(String orgTitle) {
		this.orgTitle = orgTitle;
	}

	public Integer getInvRecFlag() {
		return invRecFlag;
	}

	public void setInvRecFlag(Integer invRecFlag) {
		this.invRecFlag = invRecFlag;
	}

	public Integer getAccountingDate() {
		return accountingDate;
	}

	public void setAccountingDate(Integer accountingDate) {
		this.accountingDate = accountingDate;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreatedName() {
		return createdName;
	}

	public void setCreatedName(String createdName) {
		this.createdName = createdName;
	}

	public Integer getCreatedId() {
		return createdId;
	}

	public void setCreatedId(Integer createdId) {
		this.createdId = createdId;
	}

	public Integer getLastUpdatedId() {
		return lastUpdatedId;
	}

	public void setLastUpdatedId(Integer lastUpdatedId) {
		this.lastUpdatedId = lastUpdatedId;
	}

	public LocalDateTime getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getLastUpdateName() {
		return lastUpdateName;
	}

	public void setLastUpdateName(String lastUpdateName) {
		this.lastUpdateName = lastUpdateName;
	}


}
