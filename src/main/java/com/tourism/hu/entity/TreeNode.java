package com.tourism.hu.entity;

import java.util.List;
/**
 * 
 * @ClassName: TreeNode 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:12:18 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
public class TreeNode {
    private String id;
    private String title;
    private List<TreeNode> children;
    private String href;
    private boolean spread;
    private boolean checked;
    private boolean disabled;



    @Override
    public String toString() {
        return "TreeNode{" +
                "title='" + title + '\'' +
                ", id='" + id + '\'' +
                ", children=" + children +
                ", href='" + href + '\'' +
                ", spread=" + spread +
                ", checked=" + checked +
                ", disabled=" + disabled +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode> children) {
        this.children = children;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public boolean isSpread() {
        return spread;
    }

    public void setSpread(boolean spread) {
        this.spread = spread;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public TreeNode(String title, String id, List<TreeNode> children) {
        this.title = title;
        this.id = id;
        this.children = children;
    }

    public TreeNode(String title, String id) {
        this.title = title;
        this.id = id;
    }

    public TreeNode() {
    }

    public TreeNode(String title, String id, List<TreeNode> children, String href, boolean spread, boolean checked, boolean disabled) {
        this.title = title;
        this.id = id;
        this.children = children;
        this.href = href;
        this.spread = spread;
        this.checked = checked;
        this.disabled = disabled;
    }
}
