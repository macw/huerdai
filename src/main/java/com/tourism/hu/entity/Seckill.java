package com.tourism.hu.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @ClassName: Seckill 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:11:42 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_seckill")
public class Seckill implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 秒杀，拼团ID
     */
    @TableId(value = "seckill_id", type = IdType.AUTO)
    private Integer seckillId;

    /**
     * 秒杀，拼团名称
     */
    private String seckillName;

    /**
     * 状态 1启用 2禁用
     */
    private Integer status;

    /**
     * 备注
     */
    private String memo;


	/**
     * 秒杀，拼团价
     */
    private BigDecimal seckillPrice;

    /**
     * 秒杀，拼团商品id,或者景点id
     */
    private Integer goodsId;

	/**
	 * 1,商品，0景点
	 */
	private Integer type;

    /**
     * 秒杀，拼团开始时间
     */
    private LocalDateTime seckillBegintime;

    /**
     * 秒杀，拼团结束时间
     */
    private LocalDateTime seckillEndtime;

    /**
     * 拼团人数
     */
    private Integer collageCount;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建者名称
     */
    private String createdName;

    /**
     * 创建者id
     */
    private Integer createdId;

    /**
     * 最后更新者id
     */
    private Integer lastUpdatedId;

    /**
     * 最后更新日期
     */
    private LocalDateTime lastUpdateDate;

    /**
     * 最后更新人名称
     */
    private String lastUpdateName;

	/**
	 * 商品名称
	 */
	@TableField(exist = false)
	private String goodsName;


	/**
	 * 开始时间
	 */
	@TableField(exist = false)
	private String begintime;

	/**
	 * 结束时间
	 */
	@TableField(exist = false)
	private String endtime;

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getBegintime() {
		return begintime;
	}

	public void setBegintime(String begintime) {
		this.begintime = begintime;
	}

	public String getEndtime() {
		return endtime;
	}

	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}

	public Integer getSeckillId() {
		return seckillId;
	}

	public void setSeckillId(Integer seckillId) {
		this.seckillId = seckillId;
	}

	public String getSeckillName() {
		return seckillName;
	}

	public void setSeckillName(String seckillName) {
		this.seckillName = seckillName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public BigDecimal getSeckillPrice() {
		return seckillPrice;
	}

	public void setSeckillPrice(BigDecimal seckillPrice) {
		this.seckillPrice = seckillPrice;
	}

	public Integer getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(Integer goodsId) {
		this.goodsId = goodsId;
	}

	public LocalDateTime getSeckillBegintime() {
		return seckillBegintime;
	}

	public void setSeckillBegintime(LocalDateTime seckillBegintime) {
		this.seckillBegintime = seckillBegintime;
	}

	public LocalDateTime getSeckillEndtime() {
		return seckillEndtime;
	}

	public void setSeckillEndtime(LocalDateTime seckillEndtime) {
		this.seckillEndtime = seckillEndtime;
	}

	public Integer getCollageCount() {
		return collageCount;
	}

	public void setCollageCount(Integer collageCount) {
		this.collageCount = collageCount;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreatedName() {
		return createdName;
	}

	public void setCreatedName(String createdName) {
		this.createdName = createdName;
	}

	public Integer getCreatedId() {
		return createdId;
	}

	public void setCreatedId(Integer createdId) {
		this.createdId = createdId;
	}

	public Integer getLastUpdatedId() {
		return lastUpdatedId;
	}

	public void setLastUpdatedId(Integer lastUpdatedId) {
		this.lastUpdatedId = lastUpdatedId;
	}

	public LocalDateTime getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getLastUpdateName() {
		return lastUpdateName;
	}

	public void setLastUpdateName(String lastUpdateName) {
		this.lastUpdateName = lastUpdateName;
	}


}
