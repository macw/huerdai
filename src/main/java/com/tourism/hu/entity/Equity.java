package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_equity")
public class Equity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 权益表，主键id
     */
    @TableId(value = "equity_id", type = IdType.AUTO)
    private Integer equityId;

    /**
     * 权益名●简称
     */
    private String equityName;

    /**
     * 权益详细信息
     */
    private String equityContext;

    /**
     * 状态 1，启用；0禁用
     */
    private Integer status;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 创建人id
     */
    private Integer createUserId;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 更新时间
     */
    private byte[] updateUser;

    /**
     * 更新人id
     */
    private Integer updateUserId;


}
