package com.tourism.hu.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tourism.hu.util.PageUtil;

 
/**
 * 
 * @ClassName: ScenicRoutes 
 * @Description: (旅游线路表) 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:10:48 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@TableName("fz_scenic_routes")
public class ScenicRoutes extends PageUtil implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 线路ID
     */
    @TableId(value = "routes_id", type = IdType.AUTO)
    private Integer routesId;

    /**
     * 线路名称
     */
    private String routesName;
    /**
     * 线路图片
     */
    private String routesImg;
    
    /**
     * 行程特色
     */
    private String characteristics;
    
    
    /**
     * 购买须知
     */
    private String instructions;
    
    /**
     * 费用说明
     */
    private String costDescription;
    
    /**
     * 详情
     */
    private String routesclassInfo;
    /**
     * 线路分类Id
     */
    private Integer routesclassId;
    /**
     * 状态 1启用 2禁用
     */
    private Integer status;

    private Integer travelDay;
    /**
     * 备注
     */
    private String memo;
    /**
     * 出发城市
     */
    private String city;

    /**
     * 旅游公司主键
     */
    private Integer tId;
    /**
     *  是否可定 1可定 2不可
     */
    private Integer determined;
    /**
     *  是否可退 1可定 2不可
     */
    private Integer type;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建者名称
     */
    private String createdName;

    /**
     * 创建者id
     */
    private Integer createdId;

    /**
     * 最后更新者id
     */
    private Integer lastUpdatedId;

    /**
     * 最后更新日期
     */
    private LocalDateTime lastUpdateDate;

    /**
     * 最后更新人名称
     */
    private String lastUpdateName;
    
    /**
     *  销售价
     */
    private BigDecimal sellingPrice;
    
    /**
     *  底价
     */
    private BigDecimal floorPrice;

	public Integer getRoutesId() {
		return routesId;
	}

	public void setRoutesId(Integer routesId) {
		this.routesId = routesId;
	}

	public String getRoutesName() {
		return routesName;
	}

	public void setRoutesName(String routesName) {
		this.routesName = routesName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer gettId() {
		return tId;
	}

	public void settId(Integer tId) {
		this.tId = tId;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreatedName() {
		return createdName;
	}

	public void setCreatedName(String createdName) {
		this.createdName = createdName;
	}

	public Integer getCreatedId() {
		return createdId;
	}

	public void setCreatedId(Integer createdId) {
		this.createdId = createdId;
	}

	public Integer getLastUpdatedId() {
		return lastUpdatedId;
	}

	public void setLastUpdatedId(Integer lastUpdatedId) {
		this.lastUpdatedId = lastUpdatedId;
	}

	public LocalDateTime getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getLastUpdateName() {
		return lastUpdateName;
	}

	public void setLastUpdateName(String lastUpdateName) {
		this.lastUpdateName = lastUpdateName;
	}

	public Integer getRoutesclassId() {
		return routesclassId;
	}

	public void setRoutesclassId(Integer routesclassId) {
		this.routesclassId = routesclassId;
	}

	public Integer getTravelDay() {
		return travelDay;
	}

	public void setTravelDay(Integer travelDay) {
		this.travelDay = travelDay;
	}

	public BigDecimal getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(BigDecimal sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public BigDecimal getFloorPrice() {
		return floorPrice;
	}

	public void setFloorPrice(BigDecimal floorPrice) {
		this.floorPrice = floorPrice;
	}

	public String getRoutesImg() {
		return routesImg;
	}

	public void setRoutesImg(String routesImg) {
		this.routesImg = routesImg;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getDetermined() {
		return determined;
	}

	public void setDetermined(Integer determined) {
		this.determined = determined;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getCharacteristics() {
		return characteristics;
	}

	public void setCharacteristics(String characteristics) {
		this.characteristics = characteristics;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public String getCostDescription() {
		return costDescription;
	}

	public void setCostDescription(String costDescription) {
		this.costDescription = costDescription;
	}

	public String getRoutesclassInfo() {
		return routesclassInfo;
	}

	public void setRoutesclassInfo(String routesclassInfo) {
		this.routesclassInfo = routesclassInfo;
	}
	
	
	

}
