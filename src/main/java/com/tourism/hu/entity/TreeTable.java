package com.tourism.hu.entity;
/**
 * 
 * @ClassName: TreeTable 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:12:21 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
public class TreeTable {

    private Integer id;
    private String name;
    private Integer pid;
    private String code;


    @Override
    public String toString() {
        return "TreeTable{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pid=" + pid +
                ", code='" + code + '\'' +
                '}';
    }

    public TreeTable() {
    }

    public TreeTable(Integer id, String name, Integer pid, String code) {
        this.id = id;
        this.name = name;
        this.pid = pid;
        this.code = code;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
