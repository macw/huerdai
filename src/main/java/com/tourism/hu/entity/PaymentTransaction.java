package com.tourism.hu.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.TreeMap;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tourism.hu.pay.wxsdk.WXPayUtil;
import com.tourism.hu.pay.wxsdk.WeixinConfig;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * 
 * @ClassName: PaymentTransaction 
 * @Description: (支付交易) 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:07:48 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_payment_transaction")
public class PaymentTransaction implements Serializable {

    private static final long serialVersionUID = 1L;

    
    /**
     * 支付id
     */
    @TableId(value="id",type=IdType.AUTO)
    private Integer id;
    
    /**
     * 支付类型  1现金，2余额，3网银，4支付宝，5微信
     */
    private Integer paymentMethod;

    /**
     * 订单号码
     */
    private String orderSn;
    
    /**
     * 1.线路  2订单
     */
    private Integer orderType;
    /**
     * 程序id
     */
    private String appId;
    /**
     * 时间戳
     */
    
    @TableField("time_stamp")
    private String timeStamp;
    /**
     * 生成订单的随机数
     */
    @TableField("nonce_str")
    private String nonceStr;
    /**
     * 微信生成的预付款信息
     */
    @TableField("prepay_id")
    private String prepayId;
    /**
     * 加密方式
     */
    @TableField("sign_type")
    private String signType;
    /**
     * 支付生成的签名
     */
    @TableField("pay_sign")
    private String paySign;
    
    
    public PaymentTransaction() {
    	
    }
    
    /**
     * 
     *  
     *  
     * @param wxConfig
     * @param orderSn
     * @param paymentMethod  支付类型
     * @param orderType    1  线路  2  商品
     */
	public PaymentTransaction(Map<String,String> wxConfig,String orderSn,Integer paymentMethod,Integer orderType) {
		this.paymentMethod = paymentMethod;
		this.orderSn = orderSn;
		this.orderType=orderType;
		this.appId = wxConfig.get("appId");
		this.timeStamp = wxConfig.get("timeStamp");
		this.nonceStr = wxConfig.get("nonceStr");
		this.prepayId = wxConfig.get("prepayId");
		this.signType = wxConfig.get("signType");
		this.paySign = wxConfig.get("paySign");
	}
 
	 

}
