package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @ClassName: CustomerFriend 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:04:37 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_customer_friend")
public class CustomerFriend implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * 主键 自增id
	 */
	@TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    /**
     * 	用户 ID
     */
    private Integer customerInfId1;

    /**
     * 好友id
     */
    private String customerInfId2;

	/**
	 * 1关注 2好友 3粉丝
	 */
	private Integer relationshipType;

    /**
     * 创建人id
     */
    private Integer createUserId;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

	/**
	 * 用户昵称1
	 */
	@TableField(exist = false)
    private String nickname1;

	/**
	 * 用户昵称2
	 */
	@TableField(exist = false)
	private String nickname2;

	public String getNickname1() {
		return nickname1;
	}

	public void setNickname1(String nickname1) {
		this.nickname1 = nickname1;
	}

	public String getNickname2() {
		return nickname2;
	}

	public void setNickname2(String nickname2) {
		this.nickname2 = nickname2;
	}

	public Integer getRelationshipType() {
		return relationshipType;
	}

	public void setRelationshipType(Integer relationshipType) {
		this.relationshipType = relationshipType;
	}

	public Integer getCustomerInfId1() {
		return customerInfId1;
	}

	public void setCustomerInfId1(Integer customerInfId1) {
		this.customerInfId1 = customerInfId1;
	}

	public String getCustomerInfId2() {
		return customerInfId2;
	}

	public void setCustomerInfId2(String customerInfId2) {
		this.customerInfId2 = customerInfId2;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getCreateTime() {
		return createTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}


}
