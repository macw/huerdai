package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * 
 * @ClassName: PrefixThird 
 * @Description: (第三方登录表) 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:08:16 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_prefix_third")
public class PrefixThird implements Serializable {

    private static final long serialVersionUID = 1L;

	@TableId(value = "id", type = IdType.AUTO)
	private Integer Id;

    /**
     * 会员ID
     */

    private Integer customerId;

    /**
     * 第三方应用
     */
    private String platform;

    /**
     * 第三方唯一ID
     */
    private String openid;

    /**
     * 第三方会员昵称
     */
    private String openname;

    /**
     * AccessToken
     */
    private String accessToken;

    private String refreshToken;

    /**
     * 有效期
     */
    private Integer expiresIn;

    /**
     * 创建时间
     */
    private LocalDateTime createtime;

    /**
     * 更新时间
     */
    private LocalDateTime updatetime;

    /**
     * 登录时间
     */
    private LocalDateTime logintime;

    /**
     * 过期时间
     */
    private LocalDateTime expiretime;


	public PrefixThird(Integer id, Integer customerId, String platform, String openid, String openname, String accessToken, String refreshToken, Integer expiresIn, LocalDateTime createtime, LocalDateTime updatetime, LocalDateTime logintime, LocalDateTime expiretime) {
		Id = id;
		this.customerId = customerId;
		this.platform = platform;
		this.openid = openid;
		this.openname = openname;
		this.accessToken = accessToken;
		this.refreshToken = refreshToken;
		this.expiresIn = expiresIn;
		this.createtime = createtime;
		this.updatetime = updatetime;
		this.logintime = logintime;
		this.expiretime = expiretime;
	}

	public PrefixThird() {
	}

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getOpenname() {
		return openname;
	}

	public void setOpenname(String openname) {
		this.openname = openname;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public Integer getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(Integer expiresIn) {
		this.expiresIn = expiresIn;
	}

	public LocalDateTime getCreatetime() {
		return createtime;
	}

	public void setCreatetime(LocalDateTime createtime) {
		this.createtime = createtime;
	}

	public LocalDateTime getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(LocalDateTime updatetime) {
		this.updatetime = updatetime;
	}

	public LocalDateTime getLogintime() {
		return logintime;
	}

	public void setLogintime(LocalDateTime logintime) {
		this.logintime = logintime;
	}

	public LocalDateTime getExpiretime() {
		return expiretime;
	}

	public void setExpiretime(LocalDateTime expiretime) {
		this.expiretime = expiretime;
	}


}
