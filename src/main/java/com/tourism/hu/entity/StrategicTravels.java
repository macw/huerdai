package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @ClassName: StrategicTravels 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:11:55 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_strategic_travels")
public class StrategicTravels implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value="strage_id",type=IdType.AUTO)
    private Integer strageId;

    /**
     * 标题
     */
    private String strageTitle;

	/**
	 * 攻略头图
	 */
	private String stragePictureUrl;

    /**
     * 内容
     */
    private String strageContent;

    /**
     * 线路ID
     */
    private Integer routesId;

    /**
     * 用户ID
     */
    private Integer customerId;

    /**
     * 攻略分类ID
     */
    private Integer travelsClassId;

    /**
     * 状态 1未审核 2已审核
     */
    private Integer status;

    /**
     * 转发数
     */
    private Integer forwardcount;

    /**
     * 是否是大咖
     */
    private Integer isBigShot;
    
    /**
     * 	是否是热搜
     */
    private Integer isHotSearch;
    
    /**
     * 	点赞人数
     */
    private Integer praisecount;
    
    /**
     * 	喜欢的人数
     */
    private Integer likecount;
    
    /**
     *	 用户预览数
     */
    private Integer readingcount;

    /**
     * 评论数
     */
    private Integer commentcount;

    /**
     * 备注
     */
    private String memo;

    /**
     * 省份，保存是ID
     */
    private Integer province;

    /**
     * 市
     */
    private Integer city;

    /**
     * 区
     */
    private Integer district;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建者名称
     */
    private String createdName;

    /**
     * 创建者id
     */
    private Integer createdId;

    /**
     * 最后更新者id
     */
    private Integer lastUpdatedId;

    /**
     * 最后更新日期
     */
    private LocalDateTime lastUpdateDate;

    /**
     * 最后更新人名称
     */
    private String lastUpdateName;

	/**
	 * 标签列表
	 */
	@TableField(exist = false)
    private List<StrategicLable> lableList;

    /**
     * 昵称
     */
    @TableField(exist = false)
    private String nickname;

    /**
     * 用户头像
     */
    @TableField(exist = false)
    private String customerIcon;



}
