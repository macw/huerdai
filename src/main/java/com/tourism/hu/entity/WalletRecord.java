package com.tourism.hu.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 钱包交易记录表
 * </p>
 *
 * @author hu
 * @since 2019-12-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_wallet_record")
public class WalletRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增ID
     */
    @TableId(value = "record_id", type = IdType.AUTO)
    private Integer recordId;

    /**
     * 用户id
     */
    private Integer customerId;

    /**
     * 钱包id
     */
    private Integer walletId;

    /**
     * 交易类型 1订单收益  2提现  3商品消费 
     */
    private Integer type;

    /**
     * 交易金额
     */
    private BigDecimal money;

    /**
     * 交易时间
     */
    private LocalDateTime payTime;

    /**
     * 备注信息
     */
    private String remark;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;


}
