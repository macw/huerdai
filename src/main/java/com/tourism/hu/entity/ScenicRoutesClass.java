package com.tourism.hu.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.tourism.hu.util.PageUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @ClassName: ScenicRoutesClass 
 * @Description: (旅游线路分类表) 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:10:58 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Accessors(chain = true)
@TableName("fz_scenic_routes_class")
public class ScenicRoutesClass  extends PageUtil implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 线路ID
     */
    @TableId(value = "routesclass_id", type = IdType.AUTO)
    private Integer routesclassId;

    /**
     * 线路名称
     */
    private String routesclassName;

    /**
     * 状态 1启用 2禁用
     */
    private Integer status;

    /**
     * 备注
     */
    private String memo;

    /**
     * 跳转地址
     */
    private String url;
    
    private String img;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建者名称
     */
    private String createdName;

    /**
     * 创建者id
     */
    private Integer createdId;

    /**
     * 最后更新者id
     */
    private Integer lastUpdatedId;

    /**
     * 最后更新日期
     */
    private LocalDateTime lastUpdateDate;

    /**
     * 最后更新人名称
     */
    private String lastUpdateName;

	public Integer getRoutesclassId() {
		return routesclassId;
	}

	public void setRoutesclassId(Integer routesclassId) {
		this.routesclassId = routesclassId;
	}

	public String getRoutesclassName() {
		return routesclassName;
	}

	public void setRoutesclassName(String routesclassName) {
		this.routesclassName = routesclassName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreatedName() {
		return createdName;
	}

	public void setCreatedName(String createdName) {
		this.createdName = createdName;
	}

	public Integer getCreatedId() {
		return createdId;
	}

	public void setCreatedId(Integer createdId) {
		this.createdId = createdId;
	}

	public Integer getLastUpdatedId() {
		return lastUpdatedId;
	}

	public void setLastUpdatedId(Integer lastUpdatedId) {
		this.lastUpdatedId = lastUpdatedId;
	}

	public LocalDateTime getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getLastUpdateName() {
		return lastUpdateName;
	}

	public void setLastUpdateName(String lastUpdateName) {
		this.lastUpdateName = lastUpdateName;
	}

    
    

}
