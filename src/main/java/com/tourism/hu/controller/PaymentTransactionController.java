package com.tourism.hu.controller;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.Invoice;
import com.tourism.hu.entity.PaymentTransaction;
import com.tourism.hu.mapper.InvoiceMapper;
import com.tourism.hu.mapper.PaymentTransactionMapper;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;

/**
 * <p>
 * 支付交易 前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@RestController
@RequestMapping("/paymenttransaction")
public class PaymentTransactionController {

	
	
	

	

	@Resource
    private PaymentTransactionMapper ptMapper;

    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page,int limit){
        Page<PaymentTransaction> page1 = new Page<>(page,limit);
        IPage<PaymentTransaction> IPage = ptMapper.selectPage(page1, null);
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<PaymentTransaction> List = IPage.getRecords();
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }
	
    @RequestMapping("/addpaymentTransaction")
    public MsgUtil add(PaymentTransaction cl){
        int i = ptMapper.insert(cl);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/selectOne")
    public PaymentTransaction selectOne(Integer aid){
    	PaymentTransaction cl = ptMapper.selectOne(new QueryWrapper<PaymentTransaction>().eq("id", aid));
        return cl;
    }

    @RequestMapping("/update")
    public MsgUtil update(PaymentTransaction cl){
        int i = ptMapper.updateById(cl);
        return MsgUtil.flag(i);
    }

    
    @RequestMapping("/delete")
    public MsgUtil deleteOne(Integer aid){
        int i = ptMapper.deleteById(aid);
        return MsgUtil.flag(i);
    
    }
	
	
	
	
	
	
	
	
	
	
}
