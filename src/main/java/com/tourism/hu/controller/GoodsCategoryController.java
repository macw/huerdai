package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.GoodsCategoryConstant;
import com.tourism.hu.entity.GoodsCategory;
import com.tourism.hu.entity.TreeNode;
import com.tourism.hu.entity.User;
import com.tourism.hu.mapper.GoodsCategoryMapper;
import com.tourism.hu.mapper.UserMapper;
import com.tourism.hu.service.IGoodsCategoryService;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 商品分类表 前端控制器
 * </p>
 * @author Huerdai
 * @since 2019-09-09
 */
@RestController
@RequestMapping("/goodscategory")
public class GoodsCategoryController extends BaseController {

	@Resource
    private GoodsCategoryMapper goodsCategoryMapper;

	@Resource
	private IGoodsCategoryService iGoodsCategoryService;

	@Resource
	private RedisTemplate redisTemplate;

	@Resource
	private UserMapper userMapper;

    @RequestMapping("/togoodscategory")
    public ModelAndView hello() {
        ModelAndView mv = new ModelAndView("goods/goodsclass");
        return mv;
    }

    //暂时没用
    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page,int limit){
        Page<GoodsCategory> page1 = new Page<>(page,limit);
        //IPage<GoodsCategory> IPage = goodsCategoryMapper.selectPage(page1, null);
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
      //  List<GoodsCategory> List = IPage.getRecords();
        //获取分页总条数
        //long total = IPage.getTotal();
        //将数据封装到dataUtil对象
      //  dataUtil.setData(treeTableList);
        dataUtil.setCount(1L);
        return dataUtil;
    }

    //是一颗树，
    @RequestMapping("/selectTree")
    public DataUtil selectTree(){
        List<TreeNode> treeNodes = iGoodsCategoryService.selectAll();
        DataUtil dataUtil = DataUtil.success();
        dataUtil.setData(treeNodes);
        return dataUtil;
    }

    @RequestMapping("/selectTreeTable")
    public DataUtil selectTreeTable(){
        List<GoodsCategory> goodsCategoryList = goodsCategoryMapper.selectList(null);
        DataUtil dataUtil = DataUtil.success();
        dataUtil.setMsg("ok");
        dataUtil.setData(goodsCategoryList);
        Integer count = goodsCategoryMapper.selectCount(null);
        long l = Long.parseLong(count.toString());
        dataUtil.setCount(l);
        return dataUtil;
    }


    @RequestMapping("/addGoodsCategory")
    public MsgUtil add(GoodsCategory goodsCategory, Integer parentId2){
        System.out.println(goodsCategory);
        System.out.println(parentId2);
        User user = getUser();
        if (parentId2 !=null){
            goodsCategory.setParentId(parentId2);
        }
        goodsCategory.setClassIdStatus(BaseConstant.USE_STATUS);
        goodsCategory.setCreateUserId(user.getUserId());
        goodsCategory.setCreateUser(user.getName());
        goodsCategory.setCreateTime(LocalDateTime.now());
        int i = goodsCategoryMapper.insert(goodsCategory);
        return MsgUtil.flag(i);
    }



    @RequestMapping("/selectOneLevel")
    public List<GoodsCategory> selectOneLevel(){
        List<GoodsCategory> goodsCategoryList = goodsCategoryMapper.selectList(new QueryWrapper<GoodsCategory>().eq("class_id_level", GoodsCategoryConstant.ONE_LEVEL));
        return goodsCategoryList;
    }

    @RequestMapping("/selectTwoLevel")
    public DataUtil selectTwoLevel(Integer classId){
        List<GoodsCategory> goodsCategoryList = goodsCategoryMapper.selectList(new QueryWrapper<GoodsCategory>().eq("parent_id",classId));
        DataUtil<GoodsCategory> dataUtil = new DataUtil<>();
        dataUtil.setData(goodsCategoryList);
        return dataUtil;
    }



    @RequestMapping("/selectOne")
    public GoodsCategory selectOne(Integer aid){
    	GoodsCategory cl = goodsCategoryMapper.selectOne(new QueryWrapper<GoodsCategory>().eq("class_id", aid));
        return cl;
    }

    @RequestMapping("/updateGoods")
    public MsgUtil update(GoodsCategory cl){
        User user = getUser();
        cl.setUpdateTime(LocalDateTime.now());
        cl.setUpdateUser(user.getName());
        cl.setUpdateUserId(user.getUserId());
        int i = goodsCategoryMapper.updateById(cl);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/updateclassIdStatus")
    public MsgUtil updateclassIdStatus(GoodsCategory goodsCategory){
        int i = goodsCategoryMapper.updateById(goodsCategory);
        return MsgUtil.flag(i);
    }

    
    @RequestMapping("/deletecl")
    public MsgUtil deleteOne(Integer aid){
        int i = goodsCategoryMapper.deleteById(aid);
        return MsgUtil.flag(i);
    
    }
	
	
	
}
