package com.tourism.hu.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.entity.ScenicSpotImg;
import com.tourism.hu.service.IScenicSpotImgService;
import com.tourism.hu.util.GlobalConfigUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * @ClassName: ScenicSpotImgController 
 * @Description: 旅游景点图片表 前端控制器
 * @author 马超伟
 * @date 2019年10月12日 下午1:13:42 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("scenicSpotImg")
public class ScenicSpotImgController {

	@Autowired
	private IScenicSpotImgService imgService;

	@RequestMapping("/selectBySpotId")
	public ResultUtil<List<ScenicSpotImg>> selectBySpotId(Integer spotId) {
		ResultUtil<List<ScenicSpotImg>> result = ResultUtil.error("查询失败");
		List<ScenicSpotImg> list = new ArrayList<>();
		try {
			list = imgService.list(new QueryWrapper<ScenicSpotImg>().eq("spot_id", spotId).eq("status", 1));
//			for (ScenicSpotImg scenicSpotImg : list) {
//				scenicSpotImg.setImgUrl(GlobalConfigUtil.getServiceUrl()+scenicSpotImg.getImgUrl());
//			}
			result = ResultUtil.ok("查询成功", list);
		} catch (Exception e) {

		}
		return result;
	}
	
	@RequestMapping("/deleteBySpotImgId")
	public ResultUtil<ScenicSpotImg> deleteBySpotImgId(Integer spotImgId) {
		if(imgService.removeById(spotImgId)) {
			return ResultUtil.ok("删除成功");
		}else {
			return ResultUtil.error("删除失败");
			
		}
	}
}
