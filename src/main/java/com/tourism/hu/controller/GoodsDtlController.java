package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.entity.Goods;
import com.tourism.hu.entity.GoodsDtl;
import com.tourism.hu.entity.GoodsDtlImg;
import com.tourism.hu.entity.User;
import com.tourism.hu.service.IGoodsDtlImgService;
import com.tourism.hu.service.IGoodsDtlService;
import com.tourism.hu.service.IGoodsService;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 规格描述表 前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-05
 */
@RestController
@RequestMapping("/goodsDtl")
public class GoodsDtlController extends BaseController {


    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IGoodsService iGoodsService;

    @Resource
    private IGoodsDtlService iGoodsDtlService;

    @Resource
    private IGoodsDtlImgService iGoodsDtlImgService;

    @RequestMapping("/togoodsDtl")
    public ModelAndView hello(String goodsId) {
        ModelAndView mv = new ModelAndView("goods/dtl/goodsDtlList");
        mv.addObject("goodsId", goodsId);
        return mv;
    }


    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page, int limit, String spec, String goodsId) {
        Page<GoodsDtl> page1 = new Page<>(page, limit);
        QueryWrapper<GoodsDtl> qw = new QueryWrapper<>();
        qw.eq("goods_id", goodsId);
        if (spec != null) {
            qw.like("spec", spec);
        }

        IPage<GoodsDtl> IPage = iGoodsDtlService.page(page1, qw);
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<GoodsDtl> list = IPage.getRecords();
        Goods goods = iGoodsService.getById(goodsId);
        for (GoodsDtl goodsDtl : list) {
            goodsDtl.setGoodsname(goods.getGoodsname());
            GoodsDtlImg goodsDtlImg = iGoodsDtlImgService.getOne(new QueryWrapper<GoodsDtlImg>().eq("goodsdtl_id", goodsDtl.getGoodsdtlId()));
            if (goodsDtlImg != null) {
                goodsDtl.setClassimgAdress(goodsDtlImg.getClassimgAdress());
            }
        }

        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(list);
        dataUtil.setCount(total);
        return dataUtil;
    }

    @RequestMapping("/togoodsDtlEdit")
    public ModelAndView togoodsDtlEdit(Integer goodsId, Integer goodsdtlId) {
        ModelAndView mv = new ModelAndView("goods/dtl/goodsDtlEdit");
        mv.addObject("goodsId", goodsId);
        if (goodsdtlId != null) {
            GoodsDtl goodsDtl = iGoodsDtlService.getById(goodsdtlId);
            mv.addObject("dtl", goodsDtl);
        }
        return mv;
    }

    @RequestMapping("/addOrUpdate")
    public MsgUtil addConfig(MultipartFile file, GoodsDtl goodsDtl) {
        User user = getUser();
        if (user == null) {
            return MsgUtil.error("获取用户信息失败！");
        }
        MsgUtil msgUtil = null;
        boolean b = false;
        String basePath = "goodsDtl";
        String url = "";
        Boolean f = false;
        if (file != null && file.getSize() > 0) {
        	try {
        		url = aliOSSUpload(file, basePath, goodsDtl.getGoodsId());
			} catch (IOException e1) {
				e1.printStackTrace();
				return MsgUtil.error("图片上传失败！");
			}
//            //获取上传文件的路径
//            try {
//                url = upload(file, basePath, user.getUserId());
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
            logger.debug("url===" + url);
            f = true;
        }

        if (goodsDtl.getGoodsdtlId() == null) {
            //执行添加
            goodsDtl.setCreateTime(LocalDateTime.now());
            goodsDtl.setCreateUser(user.getName());
            goodsDtl.setCreateUserId(user.getUserId());
            b = iGoodsDtlService.save(goodsDtl);
            if (f) {
                //添加规格图片
                GoodsDtlImg goodsDtlImg = new GoodsDtlImg();
                goodsDtlImg.setClassimgAdress(url);
                goodsDtlImg.setCreateUser(user.getName());
                goodsDtlImg.setCreateTime(LocalDateTime.now());
                goodsDtlImg.setCreateUserId(user.getUserId());
                goodsDtlImg.setStatus(BaseConstant.USE_STATUS);
                goodsDtlImg.setGoodsdtlId(goodsDtl.getGoodsdtlId());
                b = iGoodsDtlImgService.save(goodsDtlImg);
            }
            msgUtil = MsgUtil.flagSave(b);
        } else {
            goodsDtl.setUpdateTime(LocalDateTime.now());
            goodsDtl.setUpdateUser(user.getName());
            goodsDtl.setUpdateUserId(user.getUserId());
            b = iGoodsDtlService.updateById(goodsDtl);
            if (f){
                //更新规格图片
                GoodsDtlImg goodsDtlImg = iGoodsDtlImgService.getOne(new QueryWrapper<GoodsDtlImg>().eq("goodsdtl_id", goodsDtl.getGoodsdtlId()));
                goodsDtlImg.setStatus(BaseConstant.USE_STATUS);
                goodsDtlImg.setUpdateTime(LocalDateTime.now());
                goodsDtlImg.setUpdateUser(user.getName());
                goodsDtlImg.setUpdateUserId(user.getUserId());
                goodsDtlImg.setGoodsdtlId(goodsDtl.getGoodsdtlId());
                goodsDtlImg.setClassimgAdress(url);
                b = iGoodsDtlImgService.updateById(goodsDtlImg);
            }
            msgUtil = MsgUtil.flagSave(b);
        }

        return msgUtil;
    }

    @RequestMapping("/deleteOne")
    public MsgUtil deleteOne(Integer id) {
        boolean b = iGoodsDtlService.removeById(id);
        return MsgUtil.flagSave(b);
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids) {
        List list = new ArrayList();
        for (Integer id : ids) {
            list.add(id);
        }
        boolean b = iGoodsDtlService.removeByIds(list);
        return MsgUtil.flagSave(b);
    }


}
