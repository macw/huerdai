package com.tourism.hu.controller;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.OrderCart;
import com.tourism.hu.entity.UserRole;
import com.tourism.hu.mapper.OrderCartMapper;
import com.tourism.hu.mapper.UserRoleMapper;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;

/**
 * 
 * @ClassName: UserRoleController 
 * @Description: 前段控制器 
 * @author 杜东兴
 * @date 2019年10月15日 下午1:05:38 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/userRole")
public class UserRoleController {

	@Resource
    private UserRoleMapper userRoleMapper;

    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page,int limit){
        Page<UserRole> page1 = new Page<>(page,limit);
        IPage<UserRole> IPage = userRoleMapper.selectPage(page1, null);
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<UserRole> List = IPage.getRecords();
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }
    
    
    @RequestMapping("/addordercart")
    public MsgUtil add(UserRole cl){
        int i = userRoleMapper.insert(cl);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/selectOne")
    public UserRole selectOne(Integer aid){
    	UserRole cl = userRoleMapper.selectOne(new QueryWrapper<UserRole>().eq("roleId", aid));
        return cl;
    }

    @RequestMapping("/update")
    public MsgUtil update(UserRole cl){
        int i = userRoleMapper.updateById(cl);
        return MsgUtil.flag(i);
    }

    
    @RequestMapping("/delete")
    public MsgUtil deleteOne(Integer aid){
        int i = userRoleMapper.deleteById(aid);
        return MsgUtil.flag(i);
    
    }
	
	
	

}
