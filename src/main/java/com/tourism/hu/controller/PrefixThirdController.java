package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.PrefixThird;
import com.tourism.hu.mapper.PrefixThirdMapper;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 第三方登录表 前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@RestController
@RequestMapping("/prefixThird")
public class PrefixThirdController {

    @Resource
    private PrefixThirdMapper mapper;

    @RequestMapping("/toprefixThird")
    public ModelAndView hello() {
        ModelAndView mv = new ModelAndView("system/prefixThird");
        return mv;
    }

    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page, int limit, String platform){
        Page<PrefixThird> page1 = new Page<>(page,limit);
        IPage<PrefixThird> IPage = null;
        if (platform == null || platform == ""){
            IPage = mapper.selectPage(page1, null);
        }else {
            IPage = mapper.selectPage(page1, new QueryWrapper<PrefixThird>().like("platform",platform));
        }
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<PrefixThird> List = IPage.getRecords();
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }


    @RequestMapping("/add")
    public MsgUtil addConfig(PrefixThird prefixThird){
        int i = mapper.insert(prefixThird);
        return MsgUtil.flag(i);
    }


    @RequestMapping("/selectOne")
    public PrefixThird selectOne(Integer id){
        PrefixThird prefixThird = mapper.selectOne(new QueryWrapper<PrefixThird>().eq("id", id));
        return prefixThird;
    }

    @RequestMapping("/update")
    public MsgUtil updateOne(PrefixThird prefixThird){
        int i = mapper.updateById(prefixThird);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/deleteOne")
    public MsgUtil deleteOne(Integer id){
        int i = mapper.deleteById(id);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids){
        List list = new ArrayList();
        for (Integer id : ids) {
            list.add(id);
        }
        int i = mapper.deleteBatchIds(list);
        return MsgUtil.flag(i);
    }

}
