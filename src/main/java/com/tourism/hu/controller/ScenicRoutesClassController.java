package com.tourism.hu.controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.ScenicRoutesClass;
import com.tourism.hu.entity.User;
import com.tourism.hu.oss.OSSClientConstants;
import com.tourism.hu.service.IScenicRoutesClassService;
import com.tourism.hu.util.FileUploadUtil;
import com.tourism.hu.util.GlobalConfigUtil;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * 
 * @ClassName: ScenicRoutesClassController
 * @Description: (旅游线路分类表)
 * @author 董兴隆
 * @date 2019年10月24日 下午1:14:02
 * @Copyright: All rights Reserved, Designed By Huerdai  
 *             Copyright:    Copyright(C) 2019-2020 Company       Huerdai
 *             Henan LTD.
 *
 */
@RestController
@RequestMapping("/scenic/routesClass")
public class ScenicRoutesClassController extends BaseController {

	@Autowired
	private IScenicRoutesClassService classService;

	@RequestMapping("page")
	public ModelAndView page() {
		ModelAndView mv = new ModelAndView("scenic/routesClass/routesClassList");
		return mv;
	}

	@RequestMapping("editPage")
	public ModelAndView editPage(ScenicRoutesClass dto) {
		ModelAndView mv = new ModelAndView("scenic/routesClass/routesClassEdit");
		if (dto.getRoutesclassId() != null && dto.getRoutesclassId() > 0) {
			dto = classService.getById(dto.getRoutesclassId());
//			dto.setImg(GlobalConfigUtil.getServiceUrl() + dto.getImg());
		}
		mv.addObject("dto", dto);
		return mv;
	}

	@RequestMapping("selectData")
	public ResultUtil<List<ScenicRoutesClass>> selectData(ScenicRoutesClass dto) {
		PageUtil.initAttr(dto);
		List<ScenicRoutesClass> list = new ArrayList<ScenicRoutesClass>();
		ResultUtil<List<ScenicRoutesClass>> result = ResultUtil.error("查询失败", list);
		try {
			QueryWrapper<ScenicRoutesClass> qw = new QueryWrapper<ScenicRoutesClass>();
			if (!StringUtils.isEmpty(dto.getRoutesclassName())) {
				qw.like("routesclass_name", dto.getRoutesclassName());
			}
			if (!StringUtils.isEmpty(dto.getCreatedName())) {
				qw.and(fn -> fn.like("created_name", dto.getCreatedName()).or().like("last_update_name",
						dto.getCreatedName()));
			}
			qw.and(fn -> fn.eq("status", 1).or().eq("status", 2));
			qw.orderByDesc("creation_date");
			Page<ScenicRoutesClass> page = new Page<ScenicRoutesClass>(dto.getPage(), dto.getLimit());
			IPage<ScenicRoutesClass> ipage = classService.page(page, qw);
			result = ResultUtil.ok("查询成功", list);
			result.setCount(ipage.getTotal());
			list = ipage.getRecords();
//			for (ScenicRoutesClass scenicRoutesClass : list) {
//				scenicRoutesClass.setImg(GlobalConfigUtil.getServiceUrl() + scenicRoutesClass.getImg());
//			}
		} catch (Exception e) {
			result.setMsg(e.getMessage());
		}
		result.setData(list);
		return result;
	}

	@RequestMapping("updateYnByRoutesclassId")
	public ResultUtil<ScenicRoutesClass> updateYnByRoutesclassId(ScenicRoutesClass dto) {
		ScenicRoutesClass routesClass = classService.getById(dto.getRoutesclassId());
		int status = routesClass.getStatus() == 1 ? 2 : 1;
		routesClass.setStatus(status);
		if (classService.updateById(routesClass)) {
			return ResultUtil.ok("修改成功");
		} else {
			return ResultUtil.error("修改失败");
		}
	}

	@RequestMapping("deleteByRoutesclassId")
	public ResultUtil<ScenicRoutesClass> deleteByRoutesclassId(ScenicRoutesClass dto) {
		dto.setStatus(3);
		if (classService.updateById(dto)) {
			return ResultUtil.ok("删除成功");
		} else {
			return ResultUtil.error("删除失败");
		}
	}

	@RequestMapping("saveOrUpdate")
	public ResultUtil<ScenicRoutesClass> saveOrUpdate(MultipartFile file, ScenicRoutesClass dto) {
		ResultUtil<ScenicRoutesClass> result = ResultUtil.error();
		boolean save = true;
		if (file != null && file.getSize() > 0) {
			String path="routesClass";
			String url="";
			try {
				url = aliOSSUpload(file, path);
			} catch (IOException e) {
				e.printStackTrace();
				logger.error("线路分类图片上传失败  异常"+e.getMessage());
			}
			dto.setImg(url);
		}
		User user = getUser();
		if (dto.getRoutesclassId() != null && dto.getRoutesclassId() > 0) {
			save = false;
			dto.setLastUpdatedId(user.getUserId());
			dto.setLastUpdateName(user.getName());
			dto.setLastUpdateDate(LocalDateTime.now());
		} else {
			dto.setCreatedId(user.getUserId());
			dto.setCreatedName(user.getName());
			dto.setCreationDate(LocalDateTime.now());
			dto.setStatus(1);
		}

		if (classService.saveOrUpdate(dto)) {
			if (save) {
				result = ResultUtil.ok("保存成功");
			} else {
				result = ResultUtil.ok("修改成功");
			}
			
		} else {
			if (save) {
				result = ResultUtil.error("保存失败");
			} else {
				result = ResultUtil.error("修改失败");
			}
		}
		
		return result;
	}

}
