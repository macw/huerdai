package com.tourism.hu.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.service.ICustomerPassengerService;


/**
 * 
 * @ClassName: CustomerPassengerController 
 * @Description: (用户乘车人信息表 前端控制器) 
 * @author 董兴隆
 * @date 2019年11月12日 下午3:07:52 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("customer/passenger")
public class CustomerPassengerController extends BaseController {

	@Autowired
	private ICustomerPassengerService passengerService;
	
}
