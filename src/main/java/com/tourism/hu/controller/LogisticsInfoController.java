package com.tourism.hu.controller;


import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.Logistics;
import com.tourism.hu.entity.QaparBusiness;
import com.tourism.hu.entity.dto.LogisticsDto;
import com.tourism.hu.mapper.LogisticsMapper;
import com.tourism.hu.service.ILogisticsService;
import com.tourism.hu.service.IQaparBusinessService;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * 
 * @ClassName: LogisticsController 
 * @Description: (物流信息Controller) 
 * @author 董兴隆
 * @date 2019年9月24日 下午5:42:00 
 *
 */
@RestController
@RequestMapping("/logistics/info")
public class LogisticsInfoController  extends BaseController{

	
	@Resource
    private LogisticsMapper logisticsMapper;

	@Autowired
	private ILogisticsService logisticsService;
	
	@Autowired
	private IQaparBusinessService businessService;
	
	@RequestMapping("/page")
    public ModelAndView page() {
        ModelAndView mv = new ModelAndView("/logistics/info/logisticsInfoList");
        List<QaparBusiness> business=businessService.list(new QueryWrapper<QaparBusiness>().eq("status", 1));
        mv.addObject("bus", business);
        return mv;
    }
	@RequestMapping("/editPage")
	public ModelAndView editPage(LogisticsDto dto) {
		ModelAndView mv = new ModelAndView("/logistics/info/logisticsInfoEdit");
		List<QaparBusiness> business=businessService.list(new QueryWrapper<QaparBusiness>().eq("status", 1));
		mv.addObject("bus", business);
		if(dto.getLogisticsId()!=null && dto.getLogisticsId()>0) {
			Logistics log = logisticsService.getById(dto.getLogisticsId());
			mv.addObject("dto", log);
		}
		return mv;
	}
	
	
    @RequestMapping("/selectData")
    public ResultUtil<List<Logistics>> selectData(LogisticsDto dto){
    	ResultUtil<List<Logistics>> result = ResultUtil.error("查询失败");
    	PageUtil.initAttr(dto);
        try {
        	Page<Logistics> page = new Page<Logistics>(dto.getPage(), dto.getLimit());
        	QueryWrapper<Logistics> qw = new QueryWrapper<Logistics>();
        	if(StringUtils.isNotBlank(dto.getExpressNo())) {
        		qw.and(fn-> fn.like("order_id", dto.getExpressNo()).or().like("express_no", dto.getExpressNo()));
        	}
        	if(StringUtils.isNotBlank(dto.getBusinessName())) {
        		qw.like("business_name", dto.getBusinessName());
        	}
        	if(StringUtils.isNotBlank(dto.getConsigneeTelphone())) {
        		qw.and(fn-> fn.like("consignee_telphone", dto.getConsigneeTelphone()).or().like("consignee_telphone2", dto.getConsigneeTelphone()));
        	}
        	if(StringUtils.isNotBlank(dto.getConsigneeRealname())) {
        		qw.like("consignee_realname", dto.getConsigneeRealname());
        	}
        	if(null!=dto.getReconciliationStatus()) {
        		qw.eq("reconciliation_status", dto.getReconciliationStatus());
        	}
        	if(null!=dto.getState()) {
        		qw.eq("state", dto.getState());
        	}
        	qw.orderByDesc("logistics_create_time");
        	IPage<Logistics> ipage= logisticsService.page(page,qw);
			result=ResultUtil.success("查询成功");
			result.setData(ipage.getRecords());
			result.setCount(ipage.getTotal());
		} catch (Exception e) {
			e.printStackTrace();
		}
        return result;
    }
    
    

    
    @RequestMapping("/deleteById")
    public ResultUtil<Logistics> deleteOne(Integer logisticsId){
        if(logisticsService.removeById(logisticsId)) {
        	return ResultUtil.success("删除成功");
        }else {
        	return ResultUtil.error("删除失败");
        }
    
    }
    
    @RequestMapping("/deleteByIds")
    public ResultUtil<Logistics> deleteByIds(List<Integer> logisticsIds){
        if(logisticsService.removeByIds(logisticsIds)) {
        	return ResultUtil.success("删除成功");
        }else {
        	return ResultUtil.error("删除失败");
        }
    }
    
    @RequestMapping("/updateByLogisticsId")
    public ResultUtil<Logistics> updateByLogisticsId(Logistics dto){
    	if(isNotLogIn()) {
    		return ResultUtil.error("未找到登录用户");
    	}
    	if(logisticsService.updateById(dto)) {
    		return ResultUtil.ok("修改成功");
        }else {
        	return ResultUtil.error("修改失败");
        }
    }
    
    
	
	
	
	
}
