package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.SeckillConstant;
import com.tourism.hu.entity.*;
import com.tourism.hu.mapper.CusseckillMapper;
import com.tourism.hu.mapper.CustomerInfoMapper;
import com.tourism.hu.mapper.SeckillMapper;
import com.tourism.hu.util.DataUtil;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 会员秒杀表 前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-29
 */
@RestController
@RequestMapping("/cusseckill")
public class CusseckillController {

    @Resource
    private CusseckillMapper cusseckillMapper;

    @Resource
    private CustomerInfoMapper customerInfoMapper;

    @Resource
    private SeckillMapper seckillMapper;

    @RequestMapping("/toCusseckill")
    public ModelAndView toCusseckill(Integer seckillId){
        ModelAndView mv = new ModelAndView("/seckill/cusseckill/cusseckillList");
      /*  List<Cusseckill> cusseckillList = cusseckillMapper.selectList(new QueryWrapper<Cusseckill>().eq("spot_id", spotId));
        mv.addObject("cusseckill",cusseckillList);*/
        mv.addObject("seckillId",seckillId);
        return mv;
    }

    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page, int limit,Integer seckillId) {
        Page<Cusseckill> page1 = new Page<>(page, limit);
        IPage<Cusseckill> IPage = cusseckillMapper.selectPage(page1, new QueryWrapper<Cusseckill>().eq("spot_id",seckillId).eq("status", BaseConstant.USE_STATUS));
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<Cusseckill> List = IPage.getRecords();
        for (Cusseckill cusseckill : List) {
            if (cusseckill.getCusId()!=null){
                CustomerInfo customerInfo = customerInfoMapper.selectById(cusseckill.getCusId());
                if (customerInfo!=null){
                    cusseckill.setNickname(customerInfo.getNickname());
                }
            }
            if (cusseckill.getSpotId()!=null){
                Seckill seckill = seckillMapper.selectById(cusseckill.getSpotId());
                if (seckill!=null){
                    cusseckill.setSeckillName(seckill.getSeckillName());
                }
            }
        }
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }

}
