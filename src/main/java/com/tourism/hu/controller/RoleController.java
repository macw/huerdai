package com.tourism.hu.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.constant.AuthorConstant;
import com.tourism.hu.constant.MenuConsant;
import com.tourism.hu.entity.*;
import com.tourism.hu.mapper.RoleMapper;
import com.tourism.hu.mapper.UserRoleMapper;
import com.tourism.hu.service.*;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: RoleController 
 * @Description: 用户角色前端控制器
 * @author 马超伟
 * @date 2019年9月27日 下午3:15:50 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/role")
public class RoleController {

    @Resource
    private RoleMapper roleMapper;

    @Resource
    private UserRoleMapper userRoleMapper;

    @Resource
    private IUserRoleService iUserRoleService;

    @Resource
    private IRoleService iRoleService;

    @Resource
    private IAuthorService iAuthorService;

    @Resource
    private IMenuService iMenuService;

    @Resource
    private IOperationService iOperationService;

    @RequestMapping("/tobackRole")
    public ModelAndView tobackRole() {
        ModelAndView mav = new ModelAndView("users/role/role");
        return mav;
    }

    /**
     * 分页，页面table数据展示
     * @param page
     * @param limit
     * @param roleName
     * @return
     */
    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page,int limit,String roleName){
        Page<Role> rolePage = new Page<>(page,limit);
        //查询分页
        IPage<Role> roleIPage = null;
        if (roleName == null || roleName == "" ){
            roleIPage = roleMapper.selectPage(rolePage, null);
        }else{
            roleIPage = roleMapper.selectPage(rolePage, new QueryWrapper<Role>().like("role_name", roleName));
        }
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<Role> roleList = roleIPage.getRecords();
        //获取分页总条数
        long total = roleIPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(roleList);
        dataUtil.setCount(total);
        return dataUtil;
    }

  /*  @RequestMapping("/selectRoleName")
    public List<Role> selectRoleName(){
        List<Role> roleList = roleMapper.selectList(null);
        return roleList;
    }*/







    /**
     * 根据角色名查询该角色所拥有的权限信息
     * @param roleName
     * @return
     */
    @RequestMapping("/selectEleTreeByRoleName")
    public List<HmFunctionstree> selectEleTreeByRoleName(String roleName){
        List<HmFunctionstree> hmFunctionstrees = iRoleService.selectEleTreeByRoleName(roleName);
        return hmFunctionstrees;
    }

    /**
     * 去添加角色页面
     * @return
     */
    @RequestMapping("/toRoleEdit")
    public ModelAndView toRoleEdit(Integer roleId){
        ModelAndView mv = new ModelAndView("/users/role/roleEdit");
        /**
         * 查询一级，列表
         * 根据一级列表查询二级
         */
        List<Menu> menuList = iMenuService.list(new QueryWrapper<Menu>().eq("level", MenuConsant.ONE_LEVEL));
        for (Menu menu : menuList) {
            List<Menu> menus = iMenuService.list(new QueryWrapper<Menu>().eq("parent_id", menu.getMenuId()));
            if (menus!=null && menus.size()>0){
                menu.setMenuList(menus);
            }
            if (menu.getMenuList()!=null && menu.getMenuList().size()>0){
                List<Menu> menuMenuList = menu.getMenuList();
                for (Menu menu1 : menuMenuList) {
                    List<Operation> operationList = iOperationService.list(new QueryWrapper<Operation>().eq("menu_id", menu1.getMenuId()));
                    if (operationList!=null && operationList.size()>0){
                        menu1.setOperationList(operationList);
                    }
                }
            }
        }
        mv.addObject("menu", menuList);

        if (roleId!=null){
            Role role = iRoleService.getById(roleId);
            mv.addObject("role", role);

            List<Author> authors = iAuthorService.list(new QueryWrapper<Author>().eq("role_id", roleId));
            List<String> list = new ArrayList<>();
            for (Author author : authors) {
                list.add(author.getResourceId().toString());
            }
            System.out.println(list);
            mv.addObject("au", list);
        }

        return mv;
    }

    /**
     * 添加角色和权限信息
     * @return
     * @param role 角色信息
     * @param ids 资源信息主键id 列表
     */
    @RequestMapping(value = "/addOrUpdateRole")
    public MsgUtil addOrUpdateRole(Role role,Integer[] ids){
        boolean save = false;
        if (role.getRoleId()==null) {
            //执行添加角色
            //封装role对象，执行添加
            save = iRoleService.save(role);
            //再查询到刚才添加进去的roleid
            for (Integer id : ids) {
                Author author = new Author(role.getRoleId(), id, AuthorConstant.MENU_RESOURCE_TYPE);
                boolean b = iAuthorService.save(author);
                if (!b) {
                    return MsgUtil.error("权限添加失败！");
                }
            }
        }else {
            //roleID ！=null，执行更新方法
            /**
             * 更新，
             */
            save = iRoleService.updateById(role);
            if (!save){
                return MsgUtil.error("角色修改失败！");
            }
            //先根据 角色id 删除该角色下面的所有权限
            save = iAuthorService.remove(new QueryWrapper<Author>().eq("role_id", role.getRoleId()));
           //再执行一遍权限的添加操作
            for (Integer id : ids) {
                Author author = new Author(role.getRoleId(), id, AuthorConstant.MENU_RESOURCE_TYPE);
                save = iAuthorService.save(author);
            }
        }

        return MsgUtil.flagSave(save);
    }


   /* *//**
     * 添加角色和权限信息 不用了
     * @param tree
     * @param roleName
     * @param remark
     * @return
     *//*
    @RequestMapping(value = "/addRole",method = RequestMethod.POST,consumes = "application/json;charset=UTF-8")
    public MsgUtil addRole(@RequestBody JSONArray tree, String roleName, String remark){
        //封装role对象，执行添加
        Role role = new Role(null,roleName,remark);
        int i = iRoleService.addRole(role);
        //再查询到刚才添加进去的roleid
        Role role1 = roleMapper.selectOne(new QueryWrapper<Role>().eq("role_name",roleName));
        //处理接收的json，转为tree对象
        String s = JSONArray.toJSONString(tree);
        List<EleTree> eleTreeList = JSON.parseArray(s,EleTree.class);
        int i1 = iAuthorService.addAuthor(role1, eleTreeList);
        if (i>0 && i1>0){
            return MsgUtil.flag(i);
        }else {
            return MsgUtil.flag(0);
        }
    }
*/
    /**
     * 修改角色信息和角色权限
     * @param tree
     * @param roleName
     * @param remark
     * @param roleId
     * @return
     */
    @RequestMapping(value = "/updateRole",method = RequestMethod.POST,consumes = "application/json;charset=UTF-8")
    public MsgUtil updateRole(@RequestBody JSONArray tree, String roleName, String remark,Integer roleId){
        //封装role对象，执行添加
        Role role = new Role(roleId,roleName,remark);
        //处理接收的json，转为tree对象
        List<EleTree> eleTreeList = JSON.parseArray(JSONArray.toJSONString(tree),EleTree.class);
        int i = iAuthorService.updateAuthor(role, eleTreeList);
        return MsgUtil.flag(i);
    }

    /**
     * 根据角色id 删除对应的角色信息和 授权表的授权信息
     * @param roleId
     * @return
     */
    @RequestMapping("/deleteRole")
    public MsgUtil deleteRole(Integer roleId){
        List<UserRole> userRoleList = userRoleMapper.selectList(new QueryWrapper<UserRole>().eq("role_id", roleId));
        if (userRoleList!=null && userRoleList.size()>0){
           /* for (UserRole userRole : userRoleList) {
                System.out.println("userRole="+userRole);
                int i = userRoleMapper.deleteById(userRole.getId());
            }*/
            MsgUtil msgUtil = MsgUtil.error();
            msgUtil.setMsg("该角色下有用户关联，删除失败！");
            return msgUtil;
        }
        int i = iRoleService.deleteRole(roleId);

        return MsgUtil.flag(i);
    }

    @RequestMapping("/deleteMany")
    public MsgUtil deleteRole(Integer[] roleId){
        List list = new ArrayList();
        for (Integer id : roleId) {
                List<UserRole> userRoleList = userRoleMapper.selectList(new QueryWrapper<UserRole>().eq("role_id", id));
                if (userRoleList!=null && userRoleList.size()>0){
                   /* for (UserRole userRole : userRoleList) {
                        System.out.println("userRole="+userRole);
                        int i = userRoleMapper.deleteById(userRole.getId());
                    }*/
                    MsgUtil msgUtil = MsgUtil.error();
                    msgUtil.setMsg("该角色下有用户关联，删除失败！");
                    return msgUtil;
                }
            list.add(id);
        }
        int i = iRoleService.deleteMAny(list);
        return MsgUtil.flag(i);
    }




}
