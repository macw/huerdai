package com.tourism.hu.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.entity.Region;
import com.tourism.hu.entity.ScenicSpot;
import com.tourism.hu.entity.TourOperator;
import com.tourism.hu.entity.User;
import com.tourism.hu.entity.dto.TourOperatorDto;
import com.tourism.hu.mapper.TourOperatorMapper;
import com.tourism.hu.service.IRegionService;
import com.tourism.hu.service.ITourOperatorService;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;

/**

 * @ClassName: TourOperatorController
 * @Description: (旅游公司Controller)
 * @author 董兴隆
 * @date 2019年10月5日 下午5:06:25
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/touroperator")
public class TourOperatorController extends BaseController{

	@Resource
	private TourOperatorMapper tourOperatorMapper;
	
	@Autowired
	private ITourOperatorService tourOperatorService;
	
	@Autowired
	private IRegionService regionService;

	@RequestMapping("/page")
	public ModelAndView hello() {
		ModelAndView mv = new ModelAndView("touroperator/touroperatorList");
		return mv;
	}
	
	@RequestMapping("/editPage")
	public ModelAndView editPage(TourOperatorDto dto) {
		ModelAndView mv = new ModelAndView("touroperator/touroperatorEdit");
		List<Region> provinceList = regionService.list(new QueryWrapper<Region>().eq("level", 1));
		mv.addObject("provinceList", provinceList);
		if(dto.getTId()!=null && dto.getTId()>0) {
			TourOperator  operator= tourOperatorService.getById(dto.getTId());
			mv.addObject("dto", operator);
			if(operator!=null &&operator.getProvince()!=null && operator.getProvince()>0) {
				List<Region> cityList = regionService.list(new QueryWrapper<Region>().eq("pid", operator.getProvince()));
				mv.addObject("cityList", cityList);
			}
			if(operator!=null &&operator.getCity()!=null && operator.getCity()>0) {
				List<Region> districtList = regionService.list(new QueryWrapper<Region>().eq("pid", operator.getCity()));
				mv.addObject("districtList", districtList);
			}
		}
		return mv;
	}

	@RequestMapping("/selectData")
	@ResponseBody
	public ResultUtil<List<TourOperatorDto>> selectData(TourOperatorDto dto) {
		PageUtil.initAttr(dto);
		List<TourOperatorDto> dataList = new ArrayList<>();
		ResultUtil<List<TourOperatorDto>> result = ResultUtil.error("查询失败", dataList);
		long count = 0;
		try {
			count = tourOperatorMapper.getCount(dto);
			dataList = tourOperatorMapper.selectPageListData(dto);
			result = ResultUtil.ok("查询成功", dataList);
		} catch (Exception e) {
			result.setMsg(e.getMessage());
		}
		result.setData(dataList);
		result.setCount(count);
		return result;
	}

	
	
	@RequestMapping("/saveOrUpdate")
	@ResponseBody
	public ResultUtil<TourOperatorDto> saveOrUpdate(TourOperatorDto dto) {
		User user = getUser();
		if(dto.getTId()!=null && dto.getTId()>0) {
			dto.setLastUpdateDate(LocalDateTime.now());
			dto.setLastUpdatedId(user.getUserId());
			dto.setLastUpdateName(user.getName());
		}else {
			dto.setCreatedId(user.getUserId());
			dto.setCreatedName(user.getName());
			dto.setCreationDate(LocalDateTime.now());
			dto.setStatus(1);
		}
		if(tourOperatorService.saveOrUpdate(dto)) {
			return ResultUtil.ok("操作成功");
		}else {
			return ResultUtil.error("操作失败");
		}
	}
	
	
	@RequestMapping("/deleteById")
	@ResponseBody
	public ResultUtil<TourOperatorDto> deleteById(TourOperatorDto dto) {
		if(tourOperatorService.removeById(dto.getTId())) {
			return ResultUtil.ok("操作成功");
		}else {
			return ResultUtil.error("操作失败");
		}
	}


}
