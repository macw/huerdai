package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.Order;
import com.tourism.hu.mapper.OrderMapper;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 订单主表 前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    private Logger logger = LoggerFactory.getLogger(getClass());

	@Resource
    private OrderMapper orderMapper;

	@RequestMapping("/toOrder")
	public ModelAndView toOrder(){
        ModelAndView mv = new ModelAndView("order/orderList");
        return mv;
    }

    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page,int limit,Order order){
        Page<Order> page1 = new Page<>(page,limit);
        //创建条件构造器
        QueryWrapper<Order> qw = new QueryWrapper<>();
        if (order.getOrderSn()!=null && order.getOrderSn()!=""){
            qw.eq("order_sn",order.getOrderSn());
        }
        if (order.getCustomerName()!=null && order.getCustomerName() !=""){
            qw.like("customer_name",order.getCustomerName());
        }
        if (order.getPaymentMethod()!=null && order.getPaymentMethod()!=0){
            qw.eq("payment_method",order.getPaymentMethod());
        }
        if (order.getOrderStatus()!=null && order.getOrderStatus()!=0){
            qw.eq("order_status",order.getOrderStatus());
        }
        //创建分页
        IPage<Order> IPage = orderMapper.selectPage(page1, qw);
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<Order> List = IPage.getRecords();
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }
    
    
    @RequestMapping("/addorder")
    public MsgUtil add(Order cl){
        int i = orderMapper.insert(cl);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/selectOne")
    public Order selectOne(Integer aid){
    	Order cl = orderMapper.selectOne(new QueryWrapper<Order>().eq("order_id", aid));
        return cl;
    }

    @RequestMapping("/update")
    public MsgUtil update(Order cl){
        int i = orderMapper.updateById(cl);
        return MsgUtil.flag(i);
    }

    
    @RequestMapping("/delete")
    public MsgUtil deleteOne(Integer aid){
        int i = orderMapper.deleteById(aid);
        return MsgUtil.flag(i);
    
    }
	

	
}
