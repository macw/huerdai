package com.tourism.hu.controller;


import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import com.tourism.hu.constant.*;
import com.tourism.hu.entity.*;
import com.tourism.hu.mapper.*;
import com.tourism.hu.service.ICustomerInfoService;
import com.tourism.hu.service.IRegisterService;
import com.tourism.hu.util.FileUploadUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;

/**
 * <p>
 * 用户信息表 前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@RestController
@RequestMapping("/customerinfo")
public class CustomerInfoController extends BaseController {

	@Resource
    private CustomerInfoMapper customerInfoMapper;
	
	@Autowired
	private ICustomerInfoService customerInfoService;
	
	@Autowired
	private IRegisterService registerService;

	@Resource
	private CustomerLevelMapper customerLevelMapper;

	@Resource
	private CustomerFriendMapper customerFriendMapper;

	@Resource
	private ScoreAccountMapper scoreAccountMapper;

	@Resource
	private AgentMapper agentMapper;

	@Resource
	private TourOperatorMapper tourOperatorMapper;

	@Resource
	private AddressMapper addressMapper;

	@RequestMapping("/tocustomerInfo")
    public ModelAndView hello() {
        ModelAndView mav = new ModelAndView("customer/info/customerInfoList");
        return mav;
    }

	@RequestMapping("/tocustomerInfoEdit")
    public ModelAndView tocustomerInfoEdit() {
        ModelAndView mv = new ModelAndView("customer/info/customerInfoEdit");
        List<CustomerLevel> customerLevelList = customerLevelMapper.selectList(null);
        mv.addObject("cll",customerLevelList);
        List<Agent> agentList = agentMapper.selectList(new QueryWrapper<Agent>().eq("status", BaseConstant.USE_STATUS));
        mv.addObject("ag",agentList);
        List<TourOperator> tourOperatorList = tourOperatorMapper.selectList(new QueryWrapper<TourOperator>().eq("status", BaseConstant.USE_STATUS));
        mv.addObject("op",tourOperatorList);
        return mv;
    }

    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page,int limit,CustomerInfo cs){
        Page<CustomerInfo> page1 = new Page<>(page,limit);
        //创建条件构造器
        QueryWrapper<CustomerInfo> qw = new QueryWrapper<>();
        if (cs.getCustomerName()!=null && cs.getCustomerName()!=""){
            qw.like("customer_name",cs.getCustomerName());
        }
        if (cs.getNickname()!=null && cs.getNickname() !=""){
            qw.like("nickname",cs.getNickname());
        }
        if (cs.getLoginName()!=null && cs.getLoginName()!=""){
            qw.like("login_name",cs.getLoginName());
        }
        if (cs.getMobilePhone()!=null && cs.getMobilePhone()!=""){
            qw.eq("mobile_phone",cs.getMobilePhone());
        }
        if (cs.getRegtype()!=null && cs.getRegtype()!=0){
            qw.eq("regtype",cs.getRegtype());
        }
        //创建分页
        IPage<CustomerInfo> IPage = customerInfoMapper.selectPage(page1, qw);
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<CustomerInfo> List = IPage.getRecords();
        for (CustomerInfo customerInfo : List) {
            java.util.List<ScoreAccount> scoreAccountList = scoreAccountMapper.selectList(new QueryWrapper<ScoreAccount>().eq("uid", customerInfo.getCustomerId()));
            int add=0,low=0;
            for (ScoreAccount scoreAccount : scoreAccountList) {
                if (scoreAccount.getType()== ScoreAccountConstant.ADD){
                    add+=scoreAccount.getScore();
                }
                if (scoreAccount.getType()== ScoreAccountConstant.LOW){
                    low+=scoreAccount.getScore();
                }
            }
            customerInfo.setUserPoint(add-low);

        }

        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }
	
    @RequestMapping("/addOrUpdateCustomerInfo")
    public MsgUtil addaCustomerInf(MultipartFile file, CustomerInfo ci){
	    //如果id为空，则说明是添加操作，否则就是修改
    	boolean save=true;
    	if(ci.getCustomerId()==null) {
			  ci.setRegisterTime(LocalDateTime.now());
	          ci.setRegtype(CustomerInfoConstant.WEB_REG);
    	}else{
    		save=false;
    		ci.setModifiedTime(LocalDateTime.now());
    	}
    	if (ci.getCustomerLevelId()!=null){
            CustomerLevel customerLevel = customerLevelMapper.selectById(ci.getCustomerLevelId());
            ci.setCustomerLevelName(customerLevel.getLevelName());
        }
    	if(!registerService.register(ci)) {
    		 return MsgUtil.flag(0);
    	}
    	 if(file != null && file.getSize() > 0) {
             //获取上传文件的路径
             String basePath = "icon";
             String pictureurl= "";
             try {
            	 pictureurl = aliOSSUpload(file, basePath,ci.getCustomerId());
             } catch (IOException e) {
                 e.printStackTrace();
                 logger.error("头像上传失败  "+e.getMessage());
                 return MsgUtil.flag(0);
             }
             ci.setCustomerIcon(pictureurl);
            if(customerInfoService.updateById(ci)) {
            	return MsgUtil.flag(1);
            }else {
            	return MsgUtil.flag(0);
            }
         }
    	 return MsgUtil.flag(1);
    }

    @RequestMapping("/selectOne")
    public ModelAndView selectOne(Integer aid){
        ModelAndView mv = new ModelAndView("customer/info/customerInfoEdit");
        CustomerInfo cs = customerInfoMapper.selectOne(new QueryWrapper<CustomerInfo>().eq("customer_id", aid));
        mv.addObject("cs",cs);
        List<CustomerLevel> customerLevelList = customerLevelMapper.selectList(null);
        mv.addObject("cll",customerLevelList);
        List<Agent> agentList = agentMapper.selectList(new QueryWrapper<Agent>().eq("status", BaseConstant.USE_STATUS));
        mv.addObject("ag",agentList);
        List<TourOperator> tourOperatorList = tourOperatorMapper.selectList(new QueryWrapper<TourOperator>().eq("status", BaseConstant.USE_STATUS));
        mv.addObject("op",tourOperatorList);
        return mv;
    }

    @RequestMapping("/updatecustomerInfo")
    public MsgUtil updateOne(CustomerInfo cf){
        int i = customerInfoMapper.updateById(cf);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/deletecustomerInfo")
    public MsgUtil deleteOne(Integer aid){
        int i = customerInfoMapper.deleteById(aid);
        List<CustomerFriend> customer_inf_id1 = customerFriendMapper.selectList(new QueryWrapper<CustomerFriend>().eq("customer_inf_id1", aid));
        if (customer_inf_id1!=null && customer_inf_id1.size()>0){
            for (CustomerFriend customerFriend : customer_inf_id1) {
                int i1 = customerFriendMapper.deleteById(customerFriend.getId());
            }
        }
        List<CustomerFriend> customer_inf_id2 = customerFriendMapper.selectList(new QueryWrapper<CustomerFriend>().eq("customer_inf_id2", aid));
        if (customer_inf_id2!=null && customer_inf_id2.size()>0){
            for (CustomerFriend customerFriend : customer_inf_id2) {
                int i1 = customerFriendMapper.deleteById(customerFriend.getId());
            }
        }
        List<Address> addressList = addressMapper.selectList(new QueryWrapper<Address>().eq("customer_id", aid));
        for (Address address : addressList) {
            addressMapper.deleteById(address.getAddressId());
        }

        return MsgUtil.flag(i);
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids) {
        List list = new ArrayList();
        for (Integer id : ids) {
            list.add(id);
            List<CustomerFriend> customer_inf_id1 = customerFriendMapper.selectList(new QueryWrapper<CustomerFriend>().eq("customer_inf_id1", id));
            if (customer_inf_id1!=null && customer_inf_id1.size()>0){
                for (CustomerFriend customerFriend : customer_inf_id1) {
                    int i1 = customerFriendMapper.deleteById(customerFriend.getId());
                }
            }
            List<CustomerFriend> customer_inf_id2 = customerFriendMapper.selectList(new QueryWrapper<CustomerFriend>().eq("customer_inf_id2", id));
            if (customer_inf_id2!=null && customer_inf_id2.size()>0){
                for (CustomerFriend customerFriend : customer_inf_id2) {
                    int i1 = customerFriendMapper.deleteById(customerFriend.getId());
                }
            }
            List<Address> addressList = addressMapper.selectList(new QueryWrapper<Address>().eq("customer_id", id));
            for (Address address : addressList) {
                addressMapper.deleteById(address.getAddressId());
            }
        }
        int i = customerInfoMapper.deleteBatchIds(list);
        return MsgUtil.flag(i);
    }
}
