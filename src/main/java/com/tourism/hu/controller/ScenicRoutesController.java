package com.tourism.hu.controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.web.session.HttpServletSession;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.entity.ScenicRoutes;
import com.tourism.hu.entity.ScenicRoutesClass;
import com.tourism.hu.entity.ScenicRoutesspot;
import com.tourism.hu.entity.ScenicSpot;
import com.tourism.hu.entity.TourOperator;
import com.tourism.hu.entity.User;
import com.tourism.hu.entity.dto.ScenicRoutesDto;
import com.tourism.hu.mapper.ScenicRoutesMapper;
import com.tourism.hu.service.IScenicRoutesClassService;
import com.tourism.hu.service.IScenicRoutesService;
import com.tourism.hu.service.IScenicRoutesspotService;
import com.tourism.hu.service.IScenicSpotService;
import com.tourism.hu.service.ITourOperatorService;
import com.tourism.hu.util.GlobalConfigUtil;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * @ClassName: ScenicRoutesController 
 * @Description: (旅游线路Controller)
 * @author 董兴隆
 * @date 2019年9月30日 下午1:13:34 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/scenic/routes")
public class ScenicRoutesController extends BaseController{

	
	
	
	
	/**
	 *  旅游线路Mapper
	 */
	@Resource
	private ScenicRoutesMapper scenicRoutesMapper;
	/**
	 *  旅游线路service
	 */
	@Autowired
	private IScenicRoutesService scenicRoutesService;
	
	@Autowired
	private IScenicRoutesspotService routesspotService;
	
	@Autowired
	private IScenicRoutesClassService classService;
	
	@Autowired
	private IScenicRoutesspotService scenicRoutesspotService;
	
	/**
	 *  旅游景点service
	 */
	@Autowired
	private IScenicSpotService scenicSpotService;
	
	/**
	 * 旅游公司service
	 */
	@Autowired
	private ITourOperatorService tourOperatorService;

	/**
	 * 
	 * @Title: page
	 * @Description: (旅游线路首页)
	 * @return
	 * @date 2019年9月30日 下午5:55:07
	 * @author 董兴隆
	 */
	@RequestMapping("/page")
	public ModelAndView page() {
		ModelAndView mv = new ModelAndView("scenic/routes/scenicsRoutesList");
		// 所有状态是启用的旅游公司信息 	scenic/routes/scenicsRoutesList
		List<TourOperator> tourOperatorList = tourOperatorService.list(new QueryWrapper<TourOperator>().eq("status", 1));
		mv.addObject("tourOperatorList", tourOperatorList);
		return mv;
	}

	@RequestMapping("/editPage")
	public ModelAndView editPage(ScenicRoutesDto dto,HttpServletRequest req,HttpServletResponse resp) {
		ModelAndView mv = new ModelAndView("scenic/routes/scenicsRoutesEdit");
		StringBuffer sb = new StringBuffer("");
		
		//所有启用的的旅游公司
		List<TourOperator> tourOperatorList = tourOperatorService.list(new QueryWrapper<TourOperator>().eq("status", 1));
		mv.addObject("tourOperatorList", tourOperatorList);
		
		//线路分类
		List<ScenicRoutesClass> routesClassList =classService.list(new QueryWrapper<ScenicRoutesClass>().eq("status", 1));
		mv.addObject("routesClassList", routesClassList);
		
		if(dto.getRoutesId() !=null && dto.getRoutesId()>0) {
			//通过旅游线路Id获得 旅游景点Id 集合
			ScenicRoutes routes = scenicRoutesService.getById(dto.getRoutesId());
			BeanUtils.copyProperties(routes, dto);
//			dto.setRoutesImg(GlobalConfigUtil.getServiceUrl()+dto.getRoutesImg());
			List<ScenicRoutesspot> routesspotList = routesspotService.list(new QueryWrapper<ScenicRoutesspot>().eq("routes_id", dto.getRoutesId()));
			List<Integer> spotIdList = new ArrayList<>();
			for (ScenicRoutesspot scenicRoutesspot : routesspotList) {
				spotIdList.add(scenicRoutesspot.getSpotId());
				sb.append(scenicRoutesspot.getSpotId()+"-");
			}
		}
		mv.addObject("dto", dto);
		Cookie cookie = new Cookie("spotIds",sb.toString());
		cookie.setPath("/");
		resp.addCookie(cookie);
		return mv;
	}
	
	@RequestMapping("/editPricePage")
	public ModelAndView editPricePage(ScenicRoutesDto dto) {
		ModelAndView mv = new ModelAndView("scenic/routes/scenicsRoutesCalendar");
		mv.addObject("dto", dto);
		return mv;
	}

	@RequestMapping("/selectData")
	public ResultUtil<List<ScenicRoutesDto>> selectData(ScenicRoutesDto dto) {
		PageUtil.initAttr(dto);
		List<ScenicRoutesDto> list = new ArrayList<>();
		ResultUtil<List<ScenicRoutesDto>> result = ResultUtil.error("查询失败", list);
		long count = 0;
		boolean isSpotNameEmpty=true;
		try {
			//如果传入的景点名称不为空 则去搜索景点信息 搜索出来景点 通过景点Id获得路线包含的景点 用来做查询
			if(StringUtils.isNotBlank(dto.getSpotNames())) {
				isSpotNameEmpty=false;
				List<ScenicSpot> spotList = scenicSpotService.list(new QueryWrapper<ScenicSpot>().like("spot_name", dto.getSpotNames()));
				dto.setSpotList(spotList);
				List<Integer> spotIds =new ArrayList<>();
				for (ScenicSpot scenicSpot : spotList) {
					spotIds.add(scenicSpot.getSpotId());
				}
				if(!spotIds.isEmpty()) {
					List<ScenicRoutesspot> routesSpotlist =routesspotService.list(new QueryWrapper<ScenicRoutesspot>().in("spot_id", spotIds).orderByDesc("creation_date"));
					dto.setRoutesspotList(routesSpotlist);
				}
			}
			//如果没有传入景点   或者传入了景点信息 并且 路线景点集合表包含该景点
			if(isSpotNameEmpty ||(!isSpotNameEmpty && dto.getRoutesspotList()!=null)) {
				count = scenicRoutesMapper.getCount(dto);
				if(count>0) {
					list = scenicRoutesMapper.selectPageListData(dto);
					for (ScenicRoutesDto scenicRoutesDto : list) { 
						StringBuffer sb = new StringBuffer("");
						List<ScenicRoutesspot> routesspotList = routesspotService.list(new QueryWrapper<ScenicRoutesspot>().eq("routes_id", scenicRoutesDto.getRoutesId()));
						if(routesspotList!=null) {
							for (ScenicRoutesspot spot : routesspotList) {
								ScenicSpot spotInfo = scenicSpotService.getById(spot.getSpotId());
								sb.append(spotInfo.getSpotName()+"->");
							}
						}
						if(StringUtils.isNotBlank(sb.toString())) {
							sb= sb.delete(sb.length()-2, sb.length());
						}
						scenicRoutesDto.setSpotNames(sb.toString());
//						scenicRoutesDto.setRoutesImg(GlobalConfigUtil.getServiceUrl()+scenicRoutesDto.getRoutesImg());
					}
				}
			}
			result = ResultUtil.ok("查询成功", list);
		} catch (Exception e) {
			result.setMsg(e.getMessage());
		}
		result.setData(list);
		result.setCount(count);
		return result;
	}
	
	
	@RequestMapping("/saveOrUpdate")
	public ResultUtil<ScenicRoutesDto> saveOrUpdate(MultipartFile file, ScenicRoutesDto dto,HttpServletRequest req,HttpServletResponse resp) {
		Cookie[] coolies = req.getCookies();
		for (Cookie cookie : coolies) {
			if("spotIds".equals(cookie.getName())) {
				cookie.setValue(null);  
				cookie.setMaxAge(0);// 立即销毁cookie
				resp.addCookie(cookie);
			}
		}
		ResultUtil result = ResultUtil.error("");
		User user = getUser();
		if(user == null || user.getUserId() == null) {
			return ResultUtil.error("未找到用户信息");
		}
		boolean save=true;
		if(dto.getRoutesId()!=null && dto.getRoutesId()>0) {
			save = false;
			dto.setLastUpdateDate(LocalDateTime.now());
			dto.setLastUpdatedId(user.getUserId());
			dto.setLastUpdateName(user.getName());
			if(StringUtils.isEmpty(dto.getMemo())) {
				dto.setMemo(" ");
			}
		}else {
			dto.setCreatedId(user.getUserId());
			dto.setCreatedName(user.getName());
			dto.setCreationDate(LocalDateTime.now());
			dto.setStatus(1);
		}
		if(scenicRoutesService.saveOrUpdate(dto)) {
			if(!StringUtils.isEmpty(dto.getSpotIds())) {
				int routesId = dto.getRoutesId();
				List<String> spotIds = Arrays.asList(dto.getSpotIds().split(","));
				if(!saveRoutesSpot(routesId, spotIds, user)) {
					if(save) {
						result= ResultUtil.ok("添加成功");
					}else {
						result= ResultUtil.ok("修改失败");
					}
				}
			} 
			if(save) {
				result= ResultUtil.ok("添加成功");
			}else {
				result= ResultUtil.ok("修改成功");
			}
		}else {
			if(save) {
				result= ResultUtil.error("添加失败");
			}else {
				result= ResultUtil.error("修改失败");
			}
		}
		if(file != null && file.getSize() > 0 && result.getCode()==0) {
			String routesImg ="";
			try {
				routesImg = aliOSSUpload(file, "routesMainImg", dto.getRoutesId());
			} catch (IOException e) {
				e.printStackTrace();
			}
			dto.setRoutesImg(routesImg);
			if(scenicRoutesService.updateById(dto)) {
				result.setCode(0);
			}else {
				result.setSe(false);
				result.setCode(400);
				result.setMsg("线路图片上传失败");
			}
		}
		return result;
	}
	
	private boolean saveRoutesSpot(Integer routesId,List<String> spotIds,User user) {
		//删除线路中所有景点信心
		scenicRoutesspotService.remove(new QueryWrapper<ScenicRoutesspot>().eq("routes_id", routesId));
		//循环添加景点信心到线路
		List<ScenicRoutesspot> scenicRoutesList = new ArrayList<>();
		for (String spotId : spotIds) {
			ScenicRoutesspot s = new ScenicRoutesspot();
			s.setCreatedId(user.getUserId());
			s.setCreatedName(user.getName());
			s.setCreationDate(LocalDateTime.now());
			s.setRoutesId(routesId);
			s.setSpotId(Integer.parseInt(spotId));
			scenicRoutesList.add(s);
		}
		return scenicRoutesspotService.saveBatch(scenicRoutesList);
	}

	@RequestMapping("/updateYnByRoutesId")
	public ResultUtil<ScenicRoutesDto> updateYnByRoutesId(ScenicRoutesDto dto) {
		User user = getUser();
		if(user == null || user.getUserId() == null) {
			return ResultUtil.error("未找到用户信息");
		}
		ScenicRoutes routes = scenicRoutesService.getById(dto.getRoutesId());
		int yn = routes.getStatus()==1?2:1;
		routes.setLastUpdateDate(LocalDateTime.now());
		routes.setLastUpdatedId(user.getUserId());
		routes.setLastUpdateName(user.getName());
		routes.setStatus(yn);
		if(scenicRoutesService.updateById(routes)) {
			return ResultUtil.ok("修改成功");
		}
		return ResultUtil.error("修改失败");
	}
	@RequestMapping("/deleteByRoutesId")
	public ResultUtil<ScenicRoutesDto> deleteByRoutesId(ScenicRoutesDto dto) {
		User user = getUser();
		if(user == null || user.getUserId() == null) {
			return ResultUtil.error("未找到用户信息");
		}
		dto.setLastUpdateDate(LocalDateTime.now());
		dto.setLastUpdatedId(user.getUserId());
		dto.setLastUpdateName(user.getName());
		dto.setStatus(3);
		if(scenicRoutesService.updateById(dto)) {
			return ResultUtil.ok("删除成功");
		}
		return ResultUtil.error("删除失败");
	}

	/**
	 * 
	 * @Title: editCharacteristics 
	 * @Description: (行程特色) 
	 * @return  
	 * @date 2019年11月14日 下午6:16:43
	 * @author 董兴隆
	 */
	@RequestMapping("editCharacteristics")
	public ModelAndView  editCharacteristics(Integer routesId) {
		ModelAndView mv = new ModelAndView("scenic/routes/characteristics");
		ScenicRoutes routes = scenicRoutesService.getById(routesId);
//		routes.setRoutesImg(GlobalConfigUtil.getServiceUrl()+routes.getRoutesImg());
		mv.addObject("routes", routes);
		return mv;
	}
	/**
	 * 
	 * @Title: editDescBtn 
	 * @Description: (费用详情) 
	 * @param routesId
	 * @return  
	 * @date 2019年11月15日 上午11:10:15
	 * @author 董兴隆
	 */
	@RequestMapping("editDescBtn")
	public ModelAndView  editDescBtn(Integer routesId) {
		ModelAndView mv = new ModelAndView("scenic/routes/description");
		ScenicRoutes routes = scenicRoutesService.getById(routesId);
//		routes.setRoutesImg(GlobalConfigUtil.getServiceUrl()+routes.getRoutesImg());
		mv.addObject("routes", routes);
		return mv;
	}
	/**
	 * 
	 * @Title: editInstructionsBtn 
	 * @Description: (购买须知) 
	 * @param routesId
	 * @return  
	 * @date 2019年11月15日 上午11:10:25
	 * @author 董兴隆
	 */
	@RequestMapping("editInstructionsBtn")
	public ModelAndView  editInstructionsBtn(Integer routesId) {
		ModelAndView mv = new ModelAndView("scenic/routes/instructions");
		ScenicRoutes routes = scenicRoutesService.getById(routesId);
//		routes.setRoutesImg(GlobalConfigUtil.getServiceUrl()+routes.getRoutesImg());
		mv.addObject("routes", routes);
		return mv;
	}
	/**
	 * 
	 * @Title: editInfo 
	 * @Description: (线路详情) 
	 * @param routesId
	 * @return  
	 * @date 2019年11月15日 上午11:10:49
	 * @author 董兴隆
	 */
	@RequestMapping("editInfo")
	public ModelAndView  editInfo(Integer routesId) {
		ModelAndView mv = new ModelAndView("scenic/routes/info");
		ScenicRoutes routes = scenicRoutesService.getById(routesId);
//		routes.setRoutesImg(GlobalConfigUtil.getServiceUrl()+routes.getRoutesImg());
		mv.addObject("routes", routes);
		return mv;
	}

}
