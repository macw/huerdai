package com.tourism.hu.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tourism.hu.controller.BaseController;

/**
 * <p>
 * 上传文件记录表 前端控制器
 * </p>
 *
 * @author hu
 * @since 2019-12-04
 */
@RestController
@RequestMapping("/hu/upload-file")
public class UploadFileController extends BaseController {

}
