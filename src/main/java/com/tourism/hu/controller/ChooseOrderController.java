package com.tourism.hu.controller;


import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.Logistics;
import com.tourism.hu.entity.Order;
import com.tourism.hu.entity.QaparBusiness;
import com.tourism.hu.entity.ScenicOrder;
import com.tourism.hu.entity.dto.LogisticsDto;
import com.tourism.hu.entity.dto.OrderDto;
import com.tourism.hu.mapper.LogisticsMapper;
import com.tourism.hu.mapper.OrderMapper;
import com.tourism.hu.mapper.ScenicOrderMapper;
import com.tourism.hu.service.ILogisticsService;
import com.tourism.hu.service.IOrderService;
import com.tourism.hu.service.IQaparBusinessService;
import com.tourism.hu.service.IScenicOrderService;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * 
 * @ClassName: ChooseOrderController 
 * @Description: (选择订单) 
 * @author 董兴隆
 * @date 2019年9月30日 下午3:49:58 
 *
 */
@RestController
@RequestMapping("/choose/order")
public class ChooseOrderController  extends BaseController{

	
	@Resource
    private OrderMapper orderMapper;
	
	@Resource
	private ScenicOrderMapper scenicOrderMapper;
	
	@Autowired
    private IOrderService orderService;
	
	@Resource
	private IScenicOrderService scenicOrderService;

	 
	
	@RequestMapping("/page")
    public ModelAndView page(OrderDto dto) {
        ModelAndView mv = new ModelAndView("/chooseList/radio/orderChoose");
        if(dto.getOrderType()==null || dto.getOrderType()<=0) {
        	dto.setOrderType(1);
        }
        mv.addObject("dto", dto);
        return mv;
    }
	
	
	
    @RequestMapping("/selectData")
    @ResponseBody
    public ResultUtil<List<OrderDto>> selectData(OrderDto dto){
    	ResultUtil<List<OrderDto>> result = ResultUtil.error("查询失败");
    	List<OrderDto> dtos = new ArrayList<OrderDto>();
    	PageUtil.initAttr(dto);
    	if(dto.getOrderType()==null || dto.getOrderType()<=0) {
    		dto.setOrderType(1);
    	}
        try {
        	long count = 0;
        	if(dto.getOrderType()==1) {
        		IPage<Order> iPage = new Page<Order>(dto.getPage(), dto.getLimit());
        		orderService.page(iPage, new QueryWrapper<Order>().orderByDesc("create_time"));
        		List<Order> orderList = iPage.getRecords();
        		BeanUtils.copyProperties(orderList, dtos, Order.class);
        		count=iPage.getTotal();
        		
        	}else {
        		IPage<ScenicOrder> iPage = new Page<ScenicOrder>(dto.getPage(), dto.getLimit());
        		scenicOrderService.page(iPage, new QueryWrapper<ScenicOrder>().orderByDesc("create_time"));
        		List<ScenicOrder> orderList = iPage.getRecords();
        		for (ScenicOrder scenicOrder : orderList) {
        			OrderDto temp = new OrderDto();
        			 
				}
        		BeanUtils.copyProperties(orderList, dtos, ScenicOrder.class);
        		iPage.getTotal();
        		count=iPage.getTotal();
        	}
        	result=ResultUtil.success("查询成功");
        	result.setData(dtos);
        	result.setCount(count);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return result;
    }
    
}
