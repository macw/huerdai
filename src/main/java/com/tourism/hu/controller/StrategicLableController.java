package com.tourism.hu.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tourism.hu.controller.BaseController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hu
 * @since 2019-11-28
 */
@RestController
@RequestMapping("/hu/strategic-lable")
public class StrategicLableController extends BaseController {

}
