package com.tourism.hu.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tourism.hu.controller.BaseController;

/**
 * <p>
 * 钱包交易记录表 前端控制器
 * </p>
 *
 * @author hu
 * @since 2019-12-13
 */
@RestController
@RequestMapping("/hu/wallet-record")
public class WalletRecordController extends BaseController {

}
