package com.tourism.hu.controller;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.tourism.hu.entity.ScenicRoutes;
import com.tourism.hu.entity.ScenicSpot;
import com.tourism.hu.entity.User;
import com.tourism.hu.entity.dto.ScenicRoutespriceDto;
import com.tourism.hu.entity.dto.ScenicSpotDto;
import com.tourism.hu.mapper.ScenicRoutespriceMapper;
import com.tourism.hu.service.IScenicRoutesService;
import com.tourism.hu.service.IScenicRoutespriceService;
import com.tourism.hu.service.IScenicSpotService;
import com.tourism.hu.util.DateConverterUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * @ClassName: ScenicRoutespriceController 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年10月13日 下午2:14:10 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/scenicroutesprice")
public class ScenicRoutespriceController extends BaseController {

	@Autowired
	private ScenicRoutespriceMapper mapper;
	
	@Autowired
	private IScenicRoutespriceService service;
	
	@Autowired
	private IScenicRoutesService routesService;
	
	@Autowired
	private IScenicSpotService spotService;

	/**
	 * 
	 * @Title: selectRoutesPrice 
	 * @Description: (查询路线价格) 
	 * @param dto
	 * @return  
	 * @date 2019年10月29日 上午11:55:57
	 * @author 董兴隆
	 */
	@RequestMapping("/selectRoutesPrice")
	public ResultUtil<List<ScenicRoutespriceDto>> selectRoutesPrice(ScenicRoutespriceDto dto) {
		//获得线路的默认价格
		String date="";
		ScenicRoutes scenicRoutes = routesService.getById(dto.getRoutesId());
		if(scenicRoutes==null) {
			return ResultUtil.error("未找到该线路信息");
		};
		if(dto.getDate()==null) {
			date = DateConverterUtil.dateFormat(LocalDateTime.now(), DateConverterUtil.month);
		}else if(dto.getDate()!=null && dto.getDate().length()>=7){
			date = dto.getDate().substring(0,7);
		}
		dto.setDate(date);
		List<ScenicRoutespriceDto>  list  = mapper.selectByMonthAndRoutesId(dto);
		for (ScenicRoutespriceDto scenicRoutespriceDto : list) {
			date = DateConverterUtil.dateFormat(scenicRoutespriceDto.getPriceDate(), "dd");
			scenicRoutespriceDto.setDate(date);
		}
		ResultUtil<List<ScenicRoutespriceDto>> result = ResultUtil.ok("查询成功", list);
		result.setMsg(scenicRoutes.getSellingPrice().toString());
		return result;
	}
	
	/**
	 * 
	 * @Title: selectSpotPrice 
	 * @Description: (查询景点价格) 
	 * @param dto
	 * @return  
	 * @date 2019年10月29日 上午11:55:57
	 * @author 董兴隆
	 */
	@RequestMapping("/selectSpotPrice")
	public ResultUtil<List<ScenicRoutespriceDto>> selectSpotPrice(ScenicRoutespriceDto dto) {
		//获得线路的默认价格
		String date="";
		ScenicSpot spot = spotService.getById(dto.getSpotId());
		if(spot==null) {
			return ResultUtil.error("未找到该景点信息");
		};
		if(dto.getDate()==null) {
			date = DateConverterUtil.dateFormat(LocalDateTime.now(), DateConverterUtil.month);
		}else if(dto.getDate()!=null && dto.getDate().length()>=7){
			date = dto.getDate().substring(0,7);
		}
		dto.setDate(date);
		List<ScenicRoutespriceDto>  list  = mapper.selectByMonthAndSpotId(dto);
		for (ScenicRoutespriceDto scenicRoutespriceDto : list) {
			date = DateConverterUtil.dateFormat(scenicRoutespriceDto.getPriceDate(), "dd");
			scenicRoutespriceDto.setDate(date);
		}
		ResultUtil<List<ScenicRoutespriceDto>> result = ResultUtil.ok("查询成功", list);
		result.setMsg(spot.getSellingPrice().toString());
		return result;
	}
	
	
	@RequestMapping("/editByRoutesIdPrice")
	public ModelAndView editByRoutesIdPrice(ScenicRoutespriceDto dto) {
		ModelAndView mv = new ModelAndView("/scenic/routes/scenicsRoutesPriceEdit");
		//传入的日期和旅游线路 未找到数据 返回空给页面     页面保存 保存不到日期  创建vo 实体
		ScenicRoutespriceDto vo= new ScenicRoutespriceDto();
		BeanUtils.copyProperties(dto, vo);
		if (dto.getDate() != null && dto.getRoutesId() != null) {
			dto = mapper.getByDayAndRoutesId(dto);
		}
		if(dto==null) {
			dto = new ScenicRoutespriceDto();
			BigDecimal floorPrice = routesService.getById(vo.getRoutesId()).getFloorPrice();
			dto.setFloorPrice(floorPrice);
		}
		mv.addObject("dto", dto);
		mv.addObject("vo", vo);
		return mv;
	}
	 
	@RequestMapping("/editBySpotIdPrice")
	public ModelAndView editBySpotIdPrice(ScenicRoutespriceDto dto) {
		ModelAndView mv = new ModelAndView("/scenic/spot/scenicsSpotPriceEdit");
		ScenicSpotDto vo= new ScenicSpotDto();
		BeanUtils.copyProperties(dto, vo);
		if (dto.getDate() != null && dto.getSpotId() != null) {
			dto = mapper.getByDayAndSpotId(dto);
		}
		if(dto==null) {
			dto = new ScenicRoutespriceDto();
			BigDecimal floorPrice = spotService.getById(vo.getSpotId()).getFloorPrice();
			dto.setFloorPrice(floorPrice);
		}
		mv.addObject("dto", dto);
		mv.addObject("vo", vo);
		return mv;
	}
	
	@RequestMapping("/saveOrUpdate")
	public ResultUtil<ScenicRoutespriceDto> saveOrUpdate(ScenicRoutespriceDto dto) {
		if(isNotLogIn()) {
			return ResultUtil.error("未找到登录用户信息");
		}
		User user = getUser();
		boolean save=true;
		if(dto.getRoutespriceId()!=null && dto.getRoutespriceId()>0) {
			save=false;
			dto.setLastUpdateDate(LocalDateTime.now());
			dto.setLastUpdatedId(user.getUserId());
			dto.setLastUpdateName(user.getName());
		}else {
			dto.setCreatedId(user.getUserId());
			dto.setCreatedName(user.getName());
			dto.setCreationDate(LocalDateTime.now());
			dto.setPriceDate(DateConverterUtil.setDayZero(dto.getDate().substring(0, 10)));
		}
		BigDecimal floorPrice= dto.getFloorPrice();
		if(floorPrice == null || floorPrice.compareTo(BigDecimal.ZERO)==0) {
			floorPrice = routesService.getById(dto.getRoutesId()).getFloorPrice();
			dto.setFloorPrice(floorPrice);
		}
		/**
		 * 如果商品有活动价格则用活动价格减去底价为利润  如果没有活动价格则 销售价格减去底价为利润
		 */
		BigDecimal preferentialQuota = BigDecimal.ZERO;
		if(dto.getActivityPrice()==null || dto.getActivityPrice().compareTo(BigDecimal.ZERO)==0) {
			preferentialQuota = dto.getSellingPrice().subtract(floorPrice);
		}else {
			preferentialQuota = dto.getActivityPrice().subtract(floorPrice);
		}
		dto.setPreferentialQuota(preferentialQuota);
		if(service.saveOrUpdate(dto)) {
			if(save) {
				return ResultUtil.ok("添加成功");
			}else {
				return ResultUtil.ok("修改成功");
			}
		}else {
			if(save) {
				return ResultUtil.error("添加失败");
			}else {
				return ResultUtil.error("修改失败");
			}
			
		}
	}

}
