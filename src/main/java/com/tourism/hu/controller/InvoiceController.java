package com.tourism.hu.controller;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.Invoice;
import com.tourism.hu.entity.User;
import com.tourism.hu.entity.dto.InvoiceDto;
import com.tourism.hu.mapper.InvoiceMapper;
import com.tourism.hu.service.IInvoiceService;
import com.tourism.hu.util.MsgUtil;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * 
 * @ClassName: InvoiceController
 * @Description: (发票表 前端控制器)
 * @author 董兴隆
 * @date 2019年9月20日 下午5:13:06
 *
 */
@RestController
@RequestMapping("/invoice")
public class InvoiceController extends BaseController {

	@Resource
	private InvoiceMapper invoiceMapper;
	
	@Autowired
	private IInvoiceService invoiceService;

	/**
	 * 
	 * @Title: Page 
	 * @Description: (发票列表页面) 
	 * @return  
	 * @date 2019年9月23日 下午2:40:11
	 * @author 董兴隆
	 */
	@RequestMapping("/page")
	public ModelAndView Page() {
		return new ModelAndView("/invoice/invoiceList");
	}
	/**
	 * 
	 * @Title: editPage 
	 * @Description: (发票添加/编辑页面) 
	 * @return  
	 * @date 2019年9月23日 下午2:39:53
	 * @author 董兴隆
	 */
	@RequestMapping("/editPage")
	public ModelAndView editPage(InvoiceDto dto) {
		ModelAndView mv = new ModelAndView("/invoice/invoiceEdit");
		if(dto.getInvoiceId()!=null && dto.getInvoiceId()>0) {
			Invoice invoice=invoiceMapper.selectById(dto.getInvoiceId());
			if(invoice.getInvoiceDate()!=null) {
				DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
				String invoiceDate = formatter2.format(invoice.getInvoiceDate());
				System.out.println(invoiceDate);
				dto.setInvoiceDateString(invoiceDate);
				
			}
			BeanUtils.copyProperties(invoice, dto);
			mv.addObject("dto", dto);
		}
		return mv;
	}

	/**
	 * 
	 * @Title: selectData
	 * @Description: (发票查询数据)
	 * @param dto
	 * @return
	 * @date 2019年9月20日 下午5:23:53
	 * @author 董兴隆
	 */
	@RequestMapping("/selectData")
	@ResponseBody
	public ResultUtil<List<Invoice>> selectData(InvoiceDto dto) {
		PageUtil.initAttr(dto);
		ResultUtil<List<Invoice>> result = ResultUtil.error("查询失败");
		try {
			Page<Invoice> page = new Page<>(dto.getPage(), dto.getLimit());
			QueryWrapper<Invoice> qw= new QueryWrapper<Invoice>();
			if(!StringUtils.isEmpty(dto.getInvoiceNum())) {
				qw.and(wapper -> wapper.like("invoice_num", dto.getInvoiceNum()).or().like("source", dto.getInvoiceNum()));
				qw.like("invoice_num", dto.getInvoiceNum());
			}
			if(dto.getInvoiceTypeLookupCode()!=null && dto.getInvoiceTypeLookupCode()>0) {
				qw.eq("invoice_type_lookup_code", dto.getInvoiceTypeLookupCode());
			}
			if(dto.getApprovalStatus()!=null && dto.getApprovalStatus()>0) {
				qw.eq("approval_status", dto.getApprovalStatus());
			}
			if(dto.getPaymentStatusFlag()!=null && dto.getPaymentStatusFlag()>0) {
				qw.eq("payment_status_flag", dto.getPaymentStatusFlag());
			}
			if(!StringUtils.isEmpty(dto.getCreatedName())) {
				qw.like("created_name", dto.getCreatedName());
			}
			qw.orderByDesc("creation_date");
			IPage<Invoice> IPage = invoiceMapper.selectPage(page, qw);
			result = ResultUtil.ok("查询成功");
			result.setData(IPage.getRecords());
			result.setCount(IPage.getTotal());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@RequestMapping("/deleteById")
	@ResponseBody
	public ResultUtil<Invoice> deleteById(InvoiceDto dto) {
		ResultUtil<Invoice> result = ResultUtil.error("删除失败");
		try {
			if (invoiceMapper.deleteById(dto.getInvoiceId()) > 0) {
				result = ResultUtil.success("删除成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	
	@RequestMapping("/saveOrUpdate")
	@ResponseBody
	public ResultUtil<Invoice> saveOrUpdate(InvoiceDto dto) {
		if(isNotLogIn()) {
			return ResultUtil.error("未找到登录用户信息");
		}
		boolean save=true;
		User user = getUser();
		if(dto.getInvoiceId()!=null && dto.getInvoiceId()>0) {
			save=false;
			dto.setLastUpdatedId(user.getUserId());
			dto.setLastUpdateName(user.getName());
			dto.setLastUpdateDate(LocalDateTime.now());
		}else {
			dto.setInvoiceCurrencyCode(dto.getPaymentCurrencyCode());
			dto.setApprovalStatus(1);
			dto.setCreatedId(user.getUserId());
			dto.setCreatedName(user.getName());
			dto.setCreationDate(LocalDateTime.now());
		}
		if(invoiceService.saveOrUpdate(dto)) {
			if(save) {
				return ResultUtil.ok("添加成功");
			}else {
				return ResultUtil.ok("修改成功");
			}
		}else {
			if(save) {
				return ResultUtil.error("添加失败");
			}else {
				return ResultUtil.error("修改失败");
			}
		}
	}
	

}
