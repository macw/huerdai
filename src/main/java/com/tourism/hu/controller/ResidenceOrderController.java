package com.tourism.hu.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.servlet.ModelAndView;

import com.tourism.hu.entity.ResidenceOrder;
import com.tourism.hu.entity.User;
import com.tourism.hu.entity.dto.ResidenceOrderDto;
import com.tourism.hu.mapper.ResidenceOrderMapper;
import com.tourism.hu.service.IResidenceOrderService;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * @ClassName: ResidenceOrderController 
 * @Description: (民宿订单表)
 * @author 董兴隆
 * @date 2019年10月17日 下午3:17:16 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 */
@RestController  
@RequestMapping("/residence/order")
public class ResidenceOrderController extends BaseController {

	@Autowired
	private IResidenceOrderService residenceOrderService;
	
	@Autowired
	private ResidenceOrderMapper residenceOrderMapper;

	@RequestMapping("/page")
	public ModelAndView page() {
		ModelAndView mv = new ModelAndView("/residence/order/residenceOrderList");
		return mv;
	}

	@RequestMapping("/editPage")
	public ModelAndView editPage(ResidenceOrderDto dto) {
		ModelAndView mv = new ModelAndView("/residence/order/residenceOrderEdit");
		if(dto.getResidenceOrderId()!=null && dto.getResidenceOrderId()>0) {
			ResidenceOrder res = residenceOrderService.getById(dto.getResidenceOrderId());
			BeanUtils.copyProperties(res, dto);
		}
		mv.addObject("dto", dto);
		return mv;
	}

	@RequestMapping("/selectData")
	public ResultUtil<List<ResidenceOrderDto>> selectData(ResidenceOrderDto dto) {
		PageUtil.initAttr(dto);
		List<ResidenceOrderDto> list = new ArrayList<>();
		ResultUtil<List<ResidenceOrderDto>> result = ResultUtil.error("查询失败", list);
		try {
			long count =residenceOrderMapper.getCount(dto);
			result = ResultUtil.ok("查询成功", list);
			if(count > 0) {
				list = residenceOrderMapper.selectPageDataList(dto);
				result.setCount(count);
			}
		}catch (Exception e) {
			result.setMsg(e.getMessage());
		}
		result.setData(list);
		return result;
	}
	
	@RequestMapping("saveOrUpdate")
	public ResultUtil<ResidenceOrderDto> saveOrUpdate(ResidenceOrderDto dto){
		User user = getUser();
		if (isNotLogIn()) {
			return ResultUtil.error("未找到登录用户");
		}
		boolean save = true;
		if (dto.getResidenceOrderId() != null && dto.getResidenceOrderId() > 0) {
			dto.setLastUpdateDate(LocalDateTime.now());
			dto.setLastUpdatedId(user.getUserId());
			dto.setLastUpdateName(user.getName());
			save = false;
		} else {
			dto.setCreatedId(user.getUserId());
			dto.setCreatedName(user.getName());
			dto.setCreationDate(LocalDateTime.now());
		}
		if (residenceOrderService.saveOrUpdate(dto)) {
			if (save) {
				return ResultUtil.ok("添加成功");
			} else {
				return ResultUtil.ok("修改成功");
			}
		} else {
			if (save) {
				return ResultUtil.error("添加失败");
			} else {
				return ResultUtil.error("修改失败");
			}
		}
	}
	
	@RequestMapping("/deleteByResidenceOrderId")
	public  ResultUtil<ResidenceOrderDto> deleteByResidenceOrderId(ResidenceOrderDto dto){
		if(residenceOrderService.removeById(dto.getResidenceOrderId())) {
			return ResultUtil.ok("删除成功");
		}else {
			return ResultUtil.error("删除失败");
		}
	}

}
