package com.tourism.hu.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: StrategicTravelsController 
 * @Description: 攻略游记表 前端控制器
 * @author 董兴隆
 * @date 2019年10月11日 下午1:08:27 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/strategicTravels")
public class StrategicTravelsController {


}
