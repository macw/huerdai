package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.CustomerInfoConstant;
import com.tourism.hu.constant.OrderConstant;
import com.tourism.hu.entity.*;
import com.tourism.hu.entity.dto.GoodsCommentDto;
import com.tourism.hu.mapper.CustomerInfoMapper;
import com.tourism.hu.mapper.GoodsCommentMapper;
import com.tourism.hu.mapper.OrderDetailMapper;
import com.tourism.hu.mapper.OrderMapper;
import com.tourism.hu.service.IGoodsCommentService;
import com.tourism.hu.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 商品评论表 前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@RestController
@RequestMapping("/goodscomment")
public class GoodsCommentController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

	@Resource
    private GoodsCommentMapper goodsCommentMapper;

	@Resource
	private IGoodsCommentService iGoodsCommentService;

	@Resource
	private OrderMapper orderMapper;

	@Resource
	private OrderDetailMapper orderDetailMapper;

	@Resource
	private CustomerInfoMapper customerInfoMapper;

	@RequestMapping("/toComment")
	public ModelAndView toComment(){
        ModelAndView mv = new ModelAndView("goods/GoodsComment");
        return mv;
    }

    @RequestMapping("/toEditComment")
    public ModelAndView toEditComment(){
        ModelAndView mv = new ModelAndView("goods/GoodsCommentEdit");
        List<Order> orderList = orderMapper.selectList(new QueryWrapper<Order>().eq("order_status", OrderConstant.THREE_SHIPPING));
        mv.addObject("ol",orderList);
        List<CustomerInfo> user_stats = customerInfoMapper.selectList(new QueryWrapper<CustomerInfo>().eq("user_stats", BaseConstant.USE_STATUS));
        mv.addObject("us",user_stats);
        return mv;
    }

    @RequestMapping("/selectProductByOrderSn")
    public DataUtil selectProductByOrderSn(String orderId){
      //  Order order = orderMapper.selectOne(new QueryWrapper<Order>().eq("order_sn", orderSn));
        List<OrderDetail> orderDetailList = orderDetailMapper.selectList(new QueryWrapper<OrderDetail>().eq("order_id", orderId));
        DataUtil<OrderDetail> dataUtil = new DataUtil<>();
        dataUtil.setData(orderDetailList);
        return dataUtil;
    }

    @GetMapping("/selectData")
    public ResultUtil<List<GoodsCommentDto>> selectData(GoodsCommentDto dto){
        System.out.println("dto:"+dto);
        ResultUtil<List<GoodsCommentDto>> result = ResultUtil.success("查询成功");
        PageUtil.initAttr(dto);
        try {
            Long goodsCommentCount = goodsCommentMapper.getGoodsCommentCount(dto);
            List<GoodsCommentDto> goodsCommentDtoList = goodsCommentMapper.selectGoodsCommentDto(dto);
            result.setCode(0);
            result.setCount(goodsCommentCount);
            result.setData(goodsCommentDtoList);
        }catch(Exception e){
            e.printStackTrace();
            result = ResultUtil.error("查询失败");
        }
        return result;
    }

    //暂时没用
    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page,int limit){
        DataUtil dataUtil = iGoodsCommentService.selectAll(page, limit);
        return dataUtil;
    }

    @RequestMapping("/updateStatus")
    public MsgUtil updateStatus(GoodsComment goodsComment){
        int i = goodsCommentMapper.updateById(goodsComment);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/addgoodscomment")
    public MsgUtil add(MultipartFile file, GoodsComment cl){
//        if(file != null && file.getSize() > 0) {
//            //获取上传文件的路径
//            String basePath = "goodsComm";
//            String pictureurl= "";
//            try {
//                pictureurl = upload(file, basePath);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            cl.setCustomerIcon(pictureurl);
//        }
    	if(file!=null && file.getSize()>0) {
    		String basePath = "goodsComm";
    		String url = "";
    		try {
    			url = aliOSSUpload(file, basePath, cl.getGoodsId());
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    		cl.setCustomerIcon(url);
    	}
        cl.setAuditTime(LocalDateTime.now());
        int i = goodsCommentMapper.insert(cl);
        return MsgUtil.flag(i);
    }


    @RequestMapping("/toReply")
    public MsgUtil toReply(GoodsComment goodsComment,Integer cid){
        GoodsComment goodsComment1 = goodsCommentMapper.selectOne(new QueryWrapper<GoodsComment>().eq("comment_id", cid));
        goodsComment.setCommentParent(cid);
        goodsComment.setAuditTime(LocalDateTime.now());
        int i = goodsCommentMapper.insert(goodsComment);
        return MsgUtil.flag(i);
    }

    /**
     * 查看该订单下有无评论
     * @param commentId
     * @return
     */
    @RequestMapping("/testReply")
    public MsgUtil testReply(Integer commentId){
        GoodsComment goodsComment = goodsCommentMapper.selectOne(new QueryWrapper<GoodsComment>().eq("comment_parent", commentId));
        if (goodsComment==null){
            return MsgUtil.error();
        }else {
            return MsgUtil.success();
        }
    }

    @RequestMapping("/toSeeDetail")
    public ModelAndView toSeeDetail(Integer commentId){
        ModelAndView mv = new ModelAndView("goods/GoodsCommentReply");
        GoodsComment goodsComment = goodsCommentMapper.selectOne(new QueryWrapper<GoodsComment>().eq("comment_parent", commentId));
        mv.addObject("gc",goodsComment);
        return mv;
    }

/*
    @RequestMapping("/selectOne")
    public GoodsComment selectOne(Integer aid){
    	GoodsComment cl = goodsCommentMapper.selectOne(new QueryWrapper<GoodsComment>().eq("comment_id", aid));
        return cl;
    }

    @RequestMapping("/updatecl")
    public MsgUtil update(GoodsComment cl){
        int i = goodsCommentMapper.updateById(cl);
        return MsgUtil.flag(i);
    }*/


    @RequestMapping("/deleteReply")
    public MsgUtil deleteReply(Integer aid){
        int i = goodsCommentMapper.deleteById(aid);
        return MsgUtil.flag(i);
    }


    @RequestMapping("/deletecl")
    public MsgUtil deleteOne(Integer aid){
        GoodsComment goodsComment = goodsCommentMapper.selectOne(new QueryWrapper<GoodsComment>().eq("comment_parent", aid));
        if (goodsComment!=null){
            int i1 = goodsCommentMapper.deleteById(goodsComment.getCommentId());
        }        int i = goodsCommentMapper.deleteById(aid);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids) {
        List list = new ArrayList();
        for (Integer id : ids) {
            list.add(id);
            GoodsComment goodsComment = goodsCommentMapper.selectOne(new QueryWrapper<GoodsComment>().eq("comment_parent", id));
            if (goodsComment!=null){
                int i1 = goodsCommentMapper.deleteById(goodsComment.getCommentId());
            }
        }
        int i = goodsCommentMapper.deleteBatchIds(list);
        return MsgUtil.flag(i);
    }
	
}
