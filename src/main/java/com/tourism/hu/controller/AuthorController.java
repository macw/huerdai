package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.MenuConsant;
import com.tourism.hu.entity.Menu;
import com.tourism.hu.entity.Operation;
import com.tourism.hu.mapper.AuthorMapper;
import com.tourism.hu.mapper.MenuMapper;
import com.tourism.hu.mapper.OperationMapper;
import com.tourism.hu.service.IAuthorService;
import com.tourism.hu.util.DataUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-06
 */
@RestController
@RequestMapping("/author")
public class AuthorController {

    @Resource
    private AuthorMapper authorMapper;

    @Resource
    private IAuthorService iAuthorService;

    @Resource
    private MenuMapper menuMapper;

    @Resource
    private OperationMapper operationMapper;

    /**
     * 根据角色名，查询该 角色拥有的权限
     * @return
     */
    @RequestMapping("/selectAllAuthor")
    public DataUtil selectAllAuthor(){
        DataUtil dataUtil = DataUtil.success();
        List<String> authorName = new ArrayList<>();
        //查询二级菜单
        List<Menu> menuList = menuMapper.selectList(new QueryWrapper<Menu>().eq("level", MenuConsant.TWO_LEVEL));
        for (Menu menu : menuList) {
            authorName.add(menu.getMenuName());
        }
        //查询所有按钮名称
        List<Operation> operationList = operationMapper.selectList(null);
        for (Operation operation : operationList) {
            authorName.add(operation.getBtnName());
        }

        dataUtil.setData(authorName);
        return dataUtil;
    }

}
