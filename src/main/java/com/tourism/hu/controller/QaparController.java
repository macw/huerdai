package com.tourism.hu.controller;


import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.OrderCart;
import com.tourism.hu.entity.Qapar;
import com.tourism.hu.entity.User;
import com.tourism.hu.mapper.OrderCartMapper;
import com.tourism.hu.mapper.QaparMapper;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * 
 * @ClassName: QaparController 
 * @Description: (凭证controller) 
 * @author 董兴隆
 * @date 2019年9月23日 下午6:57:13 
 *
 */
@RestController
@RequestMapping("/qapar")
public class QaparController  extends BaseController{

	@Resource
    private QaparMapper qaparMapper;
	
	/**
	 * 
	 * @Title: page 
	 * @Description: (凭证首页) 
	 * @return  
	 * @date 2019年9月24日 下午3:56:02
	 * @author 董兴隆
	 */
	@RequestMapping("/page")
	public ModelAndView page() {
		return new ModelAndView("/qapar/qaparList");
	}
	
	
	@RequestMapping("/editPage")
	public ModelAndView editPage(Qapar qapar) {
		ModelAndView mv = new ModelAndView("/qapar/qaparEdit");
		if(qapar.getQaparId()!=null && 0<qapar.getQaparId()) {
			Qapar qa = qaparMapper.selectById(qaparMapper.selectById(qapar.getQaparId()));
			mv.addObject("dto", qa);
		}
		return mv;
	}
	@RequestMapping("/selectData")
    public ResultUtil<List<Qapar>> selectData(Qapar qapar){
		ResultUtil<List<Qapar>> result = ResultUtil.error("查询失败");
		PageUtil.initAttr(qapar);
        Page<Qapar> page = new Page<>(qapar.getPage(),qapar.getLimit());
		try {
			QueryWrapper<Qapar> qw = new QueryWrapper<Qapar>();
			if(!StringUtils.isEmpty(qapar.getOrgTitle())) {
				qw.like("ORG_TITLE", qapar.getOrgTitle());
			}
			if(qapar.getInvRecFlag() != null && 0<qapar.getInvRecFlag()) {
				qw.like("INV_REC_FLAG", qapar.getInvRecFlag());
			}
			if(!StringUtils.isEmpty(qapar.getCreatedName())) {
				qw.and(wapper -> wapper.like("created_name", qapar.getCreatedName()).or().like("last_update_name", qapar.getCreatedName()));
			}
			qw.orderByDesc("creation_date");
			IPage<Qapar> IPage = qaparMapper.selectPage(page, qw);
			result = ResultUtil.success("查询成功");
			result.setData(IPage.getRecords());
			result.setCount(IPage.getTotal());
		} catch (Exception e) {
			e.printStackTrace();
		}
        //将数据封装到dataUtil对象
        result.setData(result.getData());
        result.setCount(result.getCount());
        return result;
    }
    

    @RequestMapping("/update")
    @ResponseBody
    public ResultUtil<Qapar> update(Qapar qa){
    	return ResultUtil.influenceQuantity(qaparMapper.updateById(qa));        
    }

    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public ResultUtil<Qapar> saveOrUpdate(Qapar qa){
    	User user = getUser();
    	if(qa.getQaparId()!=null && qa.getQaparId()>0) {
    		qa.setLastUpdateDate(LocalDateTime.now());
    		qa.setLastUpdatedId(user.getUserId());
    		qa.setLastUpdateName(user.getName());
    		return ResultUtil.influenceQuantity(qaparMapper.updateById(qa),"修改");    
    	}else {
    		qa.setCreatedId(user.getUserId());
    		qa.setCreatedName(user.getName());
    		qa.setCreationDate(LocalDateTime.now());
    		return ResultUtil.influenceQuantity(qaparMapper.insert(qa),"添加");    
    	}
    	    
    }
    
    @RequestMapping("/deleteById")
    @ResponseBody
    public ResultUtil<Qapar> deleteById(Integer qaparId){
        return ResultUtil.influenceQuantity(qaparMapper.deleteById(qaparId));        
    
    }
	
	
	
	
	
	
	
	
}
