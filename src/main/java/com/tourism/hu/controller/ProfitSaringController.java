package com.tourism.hu.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.Agent;
import com.tourism.hu.entity.ProfitSaring;
import com.tourism.hu.entity.User;
import com.tourism.hu.entity.dto.ProfitSaringDto;
import com.tourism.hu.mapper.ProfitSaringMapper;
import com.tourism.hu.service.IAgentService;
import com.tourism.hu.service.IProfitSaringService;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * 
 * @ClassName: ProfitSaringController
 * @Description: (利润分成)
 * @author 董兴隆
 * @date 2019年10月11日 上午10:46:14
 *
 */
@RestController
@RequestMapping("/profitsaring")
public class ProfitSaringController extends BaseController {

	@Resource
	private ProfitSaringMapper profitSaringMapper;

	
	@Autowired
	private IProfitSaringService profitSaringService;

	@Autowired
	private IAgentService agentService;

	 

	@RequestMapping("/page")
	public ModelAndView page() {
		ModelAndView mv = new ModelAndView("profitsaring/profitsaringList");
		List<Agent> agentList = agentService.list(new QueryWrapper<Agent>().eq("status", 1));
		mv.addObject("agentList", agentList);
		return mv;
	}

	@RequestMapping("/editPage")
	public ModelAndView editPage(Integer psId) {
		ModelAndView mv = new ModelAndView("profitsaring/profitsaringEdit");
		ProfitSaring dto =  profitSaringService.getById(psId);
		mv.addObject("dto", dto);
		return mv;
	}

	 

	@RequestMapping("/selectData")
	@ResponseBody
	public ResultUtil<List<ProfitSaring>> selectData(ProfitSaringDto dto) {
		PageUtil.initAttr(dto);
		List<ProfitSaring> list = new ArrayList<>();
		ResultUtil<List<ProfitSaring>> result = ResultUtil.error("查询失败", list);
		long count = 0;
		Page<ProfitSaring> page= new Page<ProfitSaring>(dto.getPage(), dto.getLimit());
		QueryWrapper<ProfitSaring> qw =new QueryWrapper<ProfitSaring>();
		if(dto.getType()!=null) {
			qw.eq("type", dto.getType());
		}
		if(dto.getStatus()!=null) {
			qw.eq("status", dto.getStatus());
		}
		try {
			IPage<ProfitSaring> iPage = profitSaringService.page(page,qw);
			count = iPage.getTotal();
			list = iPage.getRecords();
			result.setCode(0);
			result.setMsg("查询成功");
		} catch (Exception e) {
			result.setMsg(e.getMessage());
		}
		result.setCount(count);
		result.setData(list);
		return result;
	}

	@RequestMapping("/deleteBySId")
	@ResponseBody
	public ResultUtil<ProfitSaringDto> deleteBySId(ProfitSaringDto dto) {
		User user = getUser();
		if (user == null || user.getUserId() == null) {
			return ResultUtil.error("未找到登录用户");
		}
		if (profitSaringService.removeById(dto.getPsId())) {
			return ResultUtil.ok("删除成功");
		} else {
			return ResultUtil.error("删除失败");
		}
	}

	@RequestMapping("/updateYnByPsId")
	@ResponseBody
	public ResultUtil<ProfitSaringDto> updateYnByPsId(Integer psId) {
		User user = getUser();
		if (user == null || user.getUserId() == null) {
			return ResultUtil.error("未找到登录用户");
		}
		ProfitSaring profitSaring = profitSaringService.getById(psId);
		int status = profitSaring.getStatus() == 1 ? 2 : 1;
		profitSaring.setStatus(status);
		profitSaring.setLastUpdateDate(LocalDateTime.now());
		profitSaring.setLastUpdatedId(user.getUserId());
		profitSaring.setLastUpdateName(user.getName());
		if (profitSaringService.updateById(profitSaring)) {
			if(status == 1) {
				return ResultUtil.ok("启用成功");
			}else {
				return ResultUtil.ok("禁用成功");
			}
		} else {
			if(status == 1) {
				return ResultUtil.ok("启用失败");
			}else {
				return ResultUtil.ok("禁用失败");
			}
		}
	}

	@RequestMapping("/saveOrUpdate")
	@ResponseBody
	public ResultUtil<ProfitSaringDto> saveOrUpdate(ProfitSaringDto dto) {
		User user = getUser();
		if (user == null || user.getUserId() == null) {
			return ResultUtil.error("未找到登录用户");
		}
		boolean save = true;
		if (dto.getPsId() != null && dto.getPsId() > 0) {
			save = false;
			dto.setLastUpdateDate(LocalDateTime.now());
			dto.setLastUpdatedId(user.getUserId());
			dto.setLastUpdateName(user.getName());
		} else {
			dto.setCreatedId(user.getUserId());
			dto.setCreatedName(user.getName());
			dto.setCreationDate(LocalDateTime.now());
			dto.setStatus(1);
		}
		if (profitSaringService.saveOrUpdate(dto)) {
			if (save) {
				return ResultUtil.ok("添加成功");
			} else {
				return ResultUtil.ok("修改成功");
			}
		} else {
			if (save) {
				return ResultUtil.error("添加失败");
			} else {
				return ResultUtil.error("修改失败");
			}
		}
	}
}
