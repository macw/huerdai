package com.tourism.hu.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tourism.hu.controller.BaseController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-06
 */
@RestController
@RequestMapping("/hu/profit-flowing")
public class ProfitFlowingController extends BaseController {

}
