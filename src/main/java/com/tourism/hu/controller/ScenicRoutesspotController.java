package com.tourism.hu.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @ClassName: ScenicRoutesspotController 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月13日 下午1:12:48 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/hu/scenic-routesspot")
public class ScenicRoutesspotController {

}
