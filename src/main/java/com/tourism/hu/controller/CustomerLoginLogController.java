package com.tourism.hu.controller;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.entity.CustomerLoginLog;
import com.tourism.hu.entity.dto.CustomerLoginLogDto;
import com.tourism.hu.mapper.CustomerLoginLogMapper;
import com.tourism.hu.service.ICustomerLoginLogService;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.PageUtil;


/**
 * 
 * @ClassName: CustomerLoginLogController 
 * @Description: (用户登陆日志表 前端控制器) 
 * @author 董兴隆
 * @date 2019年9月12日 上午10:39:51 
 *
 */
@RestController
@RequestMapping("/customerloginlog")
public class CustomerLoginLogController {

	@Resource
    private CustomerLoginLogMapper cliMapper;
	
	
	@Autowired
	private ICustomerLoginLogService customerLoginLogService;

	/**
	 * 
	 * @Title: page 
	 * @Description: (跳转用户登录日志页面) 
	 * @return  
	 * @date 2019年9月12日 下午4:09:44
	 * @author 董兴隆
	 */
	@RequestMapping("/page")
	public ModelAndView page(){
		return new ModelAndView("/customer/customerLoginLogList");
	}
	/**
	 * 
	 * @Title: selectList 
	 * @Description: (获取用户登陆页面数据) 
	 * @param dto
	 * @return  
	 * @date 2019年9月12日 下午4:10:09
	 * @author 董兴隆
	 */
	@RequestMapping("/selectList")
	@ResponseBody
	public DataUtil selectList(CustomerLoginLogDto dto){
		PageUtil.initAttr(dto);
		long  count = customerLoginLogService.selectCustomerLoginLogCount(dto);
		List<CustomerLoginLogDto>  dtos= customerLoginLogService.selectCustomerLoginLogDto(dto);
		DataUtil dataUtil = DataUtil.success();
        dataUtil.setData(dtos);
        dataUtil.setCount(count);
        return dataUtil;
	}
	
	
	@RequestMapping("/addCustomerLoginLog/api")
	@ResponseBody
	public DataUtil addCustomerLoginLog(CustomerLoginLogDto dto){
		DataUtil dataUtil = DataUtil.success();
		if(cliMapper.insert(dto)>0) {
			dataUtil.success();
			dataUtil.setMsg("添加成功");
		}else {
			dataUtil.error();
			dataUtil.setMsg("添加失败");
		}
        return dataUtil;
	}
	
	@RequestMapping("/delCustomerLoginLog/api")
	@ResponseBody
	public DataUtil delCustomerLoginLog(CustomerLoginLogDto dto){
		DataUtil dataUtil = DataUtil.success();
		if(cliMapper.deleteById(dto.getLoginId())>0) {
			dataUtil.success();
			dataUtil.setMsg("删除成功");
		}else {
			dataUtil.error();
			dataUtil.setMsg("删除失败");
		}
        return dataUtil;
	}
	@RequestMapping("/getCustomerLoginLog/api")
	@ResponseBody
	public CustomerLoginLog getCustomerLoginLog(CustomerLoginLogDto dto){
		CustomerLoginLog coust = cliMapper.selectById(dto.getLoginId());
		return coust;
	}
	
	@RequestMapping("/selectCustomerLoginLog/api")
	@ResponseBody
	public DataUtil<CustomerLoginLog> selectCustomerLoginLogBycustomerId(CustomerLoginLogDto dto){
		DataUtil<CustomerLoginLog> dataUtil = DataUtil.success();
		List<CustomerLoginLog> list = cliMapper.selectList(new QueryWrapper<CustomerLoginLog>().eq("customer_id", dto.getCustomerId()));
		dataUtil.success();
		dataUtil.setData(list);
		return dataUtil;
	}
	
}
