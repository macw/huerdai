package com.tourism.hu.controller;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.entity.Agent;
import com.tourism.hu.entity.Region;
import com.tourism.hu.entity.ScenicSpot;
import com.tourism.hu.entity.User;
import com.tourism.hu.entity.dto.AgentDto;
import com.tourism.hu.mapper.AgentMapper;
import com.tourism.hu.service.IAgentService;
import com.tourism.hu.service.IRegionService;
import com.tourism.hu.util.GlobalConfigUtil;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.QRCodeUtils;
import com.tourism.hu.util.ResultUtil;

/**
 * 
 * @ClassName: AgentController
 * @Description: (代理商Controller)
 * @author 董兴隆
 * @date 2019年10月6日 下午5:36:30
 *
 */
@RestController
@RequestMapping("/agent")
public class AgentController extends BaseController{

	@Resource
	private AgentMapper agentMapper;
	
	@Autowired
	private IAgentService agentService;
	
	@Autowired
	private IRegionService regionService;

	
	@RequestMapping("/page")
	public ModelAndView page() {
		ModelAndView mv = new ModelAndView("agent/agentList");
		return mv;
	}

	
	@RequestMapping("/editPage")
	public ModelAndView editPage(AgentDto dto) {
		ModelAndView mv = new ModelAndView("agent/agentEdit");
		List<Region> provinceList = regionService.list(new QueryWrapper<Region>().eq("level", 1));
		mv.addObject("provinceList", provinceList);
		if(dto.getAgenId()!=null && dto.getAgenId()>0) {
			Agent agent= agentService.getById(dto.getAgenId());
			mv.addObject("dto", agent);
			if(agent!=null &&agent.getProvince()!=null && agent.getProvince()>0) {
				List<Region> cityList = regionService.list(new QueryWrapper<Region>().eq("pid", agent.getProvince()));
				mv.addObject("cityList", cityList);
			}
			if(agent!=null &&agent.getCity()!=null && agent.getCity()>0) {
				List<Region> districtList = regionService.list(new QueryWrapper<Region>().eq("pid", agent.getCity()));
				mv.addObject("districtList", districtList);
			}
		}
		return mv;
	}

	@RequestMapping("/selectData")
	@ResponseBody
	public ResultUtil<List<AgentDto>> selectData(AgentDto dto) {
		PageUtil.initAttr(dto);
		List<AgentDto> list = new ArrayList<>();
		ResultUtil<List<AgentDto>> result = ResultUtil.error("查询失败",list);
		long count = 0;
		try {
			count = agentMapper.getCount(dto);
			list = agentMapper.selectPageDataList(dto);
			result = ResultUtil.ok("查询成功",list);
		}catch (Exception e) {
			result.setMsg("查询异常"+e.getMessage());
		}
		result.setCount(count);
		result.setData(list);
		return result;
	}
	
	@RequestMapping("/saveOrUpdate")
	@ResponseBody
	public ResultUtil<AgentDto> saveOrUpdate(AgentDto dto){
		User user = getUser();
		boolean save=true;
		ResultUtil<AgentDto> result = ResultUtil.error("修改失败",dto);
		if(user==null || user.getUserId()==null) {
			result.setMsg("未找到登录的用户信息");
			return result;
		}
		if(dto.getAgenId()!=null && dto.getAgenId()>0) {
			dto.setLastUpdateDate(LocalDateTime.now());
			dto.setLastUpdatedId(user.getUserId());
			dto.setLastUpdateName(user.getName());
			save=false;
		}else {
			dto.setCreatedId(user.getUserId());
			dto.setCreatedName(user.getName());
			dto.setCreationDate(LocalDateTime.now());
			dto.setStatus(1);//添加默认为可用
			result = ResultUtil.error("添加失败",dto);
		}
		dto.setFullAddress(getFullAddress(dto.getProvince(), dto.getCity(), dto.getDistrict(),dto.getAddress()));
		if(agentService.saveOrUpdate(dto)) {
			if(save) {
				dto.setSharecode(dto.getAgenId().toString());
				if(agentService.updateById(dto)) {
					result=ResultUtil.ok("添加成功",dto);
				}
			}else {
				result=ResultUtil.ok("修改成功",dto);
			}
		}
		return result;
	}
	
	@RequestMapping("/deleteById")
	@ResponseBody
	public ResultUtil<AgentDto> deleteById(AgentDto dto){
		User user = getUser();
		ResultUtil<AgentDto> result = ResultUtil.error("删除失败",dto);
		if(user==null || user.getUserId()==null) {
			result.setMsg("未找到登录的用户信息");
			return result;
		}
		dto.setLastUpdateDate(LocalDateTime.now());
		dto.setLastUpdatedId(user.getUserId());
		dto.setLastUpdateName(user.getName());
		dto.setStatus(3);//删除标识
		if(agentService.updateById(dto)) {
			result=ResultUtil.ok("删除成功",dto);
		}
		return result;
	}
	
	
	@RequestMapping("/sharePage")
	public ModelAndView sharePage(AgentDto dto){
		ModelAndView mv = new ModelAndView("agent/agentShare");
		Agent agent = agentService.getById(dto.getAgenId());
		mv.addObject("sharecode", agent.getSharecode());
		String sharecodeUrl = GlobalConfigUtil.getKey("shareAppUrl")+"?sharecode="+isNullNotAppend(agent.getSharecode());
		mv.addObject("sharecodeUrl", sharecodeUrl);
		String shareQRCode=agent.getSharecodeUrl();
		if(StringUtils.isBlank(shareQRCode)) {
			String destPath = GlobalConfigUtil.getUploadUrl();
			try {
				shareQRCode = QRCodeUtils.encode(sharecodeUrl, destPath);
				File file = new File(shareQRCode);
				shareQRCode = aliOSSUpload(file, "qrcode");
				agent.setSharecodeUrl(shareQRCode);
				agentService.updateById(agent);
				file.delete();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		mv.addObject("shareQRCode", shareQRCode);
		return mv;
	}
	

}
