package com.tourism.hu.controller;


import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.Logistics;
import com.tourism.hu.entity.OrderCart;
import com.tourism.hu.entity.QaparBusiness;
import com.tourism.hu.mapper.OrderCartMapper;
import com.tourism.hu.mapper.QaparBusinessMapper;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * 
 * @ClassName: QaparBusinessController 
 * @Description: (物流商家controller) 
 * @author 董兴隆
 * @date 2019年9月23日 上午11:39:04 
 *
 */
@RestController
@RequestMapping("/qaparBusiness")
public class QaparBusinessController {

	
	@Resource
    private QaparBusinessMapper qaparBusinessMapper;

	
	/**
	 * 
	 * @Title: page 
	 * @Description: (物流商家页面) 
	 * @return  
	 * @date 2019年9月23日 上午11:59:53
	 * @author 董兴隆
	 */
	@RequestMapping("/page")
	public ModelAndView page() {
		return new ModelAndView("/logistics/businessList");
	}
	
    @RequestMapping("/selectData")
    public ResultUtil<List<QaparBusiness>> selectAll(QaparBusiness qapar){
    	ResultUtil<List<QaparBusiness>> dataUtil = ResultUtil.error("查询失败");
        PageUtil.initAttr(qapar);
        IPage<QaparBusiness> iPage = null;
        Page<QaparBusiness> page = new Page<>(qapar.getPage(),qapar.getLimit());
        QueryWrapper<QaparBusiness> qw = new QueryWrapper<QaparBusiness>();
        if(!StringUtils.isEmpty(qapar.getBusinessName())) {
        	qw.like("business_name", qapar.getBusinessName());
        }
		try {
			iPage = qaparBusinessMapper.selectPage(page, qw);
			dataUtil = ResultUtil.success();
		} catch (Exception e) {
			e.printStackTrace();
		}
		dataUtil.setData(iPage.getRecords());
		dataUtil.setCount(iPage.getTotal());
        return dataUtil;
    }
    
    
    @RequestMapping("/deleteById")
    @ResponseBody
    public ResultUtil deleteById(Integer id){
        return ResultUtil.influenceQuantity(qaparBusinessMapper.deleteById(id));
    }

    @RequestMapping("/selectOne")
    public QaparBusiness selectOne(Integer aid){
    	QaparBusiness cl = qaparBusinessMapper.selectOne(new QueryWrapper<QaparBusiness>().eq("business_id", aid));
        return cl;
    }

    @RequestMapping("/update")
    public MsgUtil update(QaparBusiness cl){
        int i = qaparBusinessMapper.updateById(cl);
        return MsgUtil.flag(i);
    }

    
    @RequestMapping("/delete")
    public MsgUtil deleteOne(Integer aid){
        int i = qaparBusinessMapper.deleteById(aid);
        return MsgUtil.flag(i);
    
    }
	
	
	
	
	
	
	
	
	
	
	
	
}
