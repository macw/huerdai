package com.tourism.hu.controller;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.tourism.hu.entity.User;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.ScoreAccount;
import com.tourism.hu.mapper.ScoreAccountMapper;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import org.springframework.web.servlet.ModelAndView;

/**
 * @ClassName: ScoreAccountController 
 * @Description: 会员积分账户表 前端控制器
 * @author 马超伟
 * @date 2019年11月1日 下午1:11:21 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/scoreaccount")
public class ScoreAccountController extends BaseController {


	@Resource
    private ScoreAccountMapper scoreAccountMapper;

	@RequestMapping("/toAccount")
	public ModelAndView toAccount(Integer id){
        ModelAndView mv = new ModelAndView("customer/score/customerScoreList");
        mv.addObject("sid",id);
        return mv;
    }

    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page,int limit,Integer id){
        Page<ScoreAccount> page1 = new Page<>(page,limit);
        IPage<ScoreAccount> IPage = scoreAccountMapper.selectPage(page1, new QueryWrapper<ScoreAccount>().eq("uid",id));
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<ScoreAccount> List = IPage.getRecords();
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }

    @RequestMapping("/toAccountEdit")
    public ModelAndView toAccountEdit(Integer id){
        ModelAndView mv = new ModelAndView("customer/score/customerScoreEdit");
        mv.addObject("sid",id);
        return mv;
    }
	

    @RequestMapping("/addOrUpdate")
    public MsgUtil addOrUpdate(ScoreAccount cl){
        User user = getUser();
        if (cl.getId()==null){
	        cl.setCreateUserId(user.getUserId());
	        cl.setCreateUser(user.getName());
	        cl.setCreateTime(LocalDateTime.now());
            int i = scoreAccountMapper.insert(cl);
            return MsgUtil.flag(i);
        }else {
            cl.setUpdateUserId(user.getUserId());
            cl.setUpdateUser(user.getName());
            cl.setUpdateTime(LocalDateTime.now());
            int i = scoreAccountMapper.updateById(cl);
            return MsgUtil.flag(i);
        }

    }

    @RequestMapping("/selectOne")
    public ModelAndView selectOne(Integer id){
    	ScoreAccount cl = scoreAccountMapper.selectOne(new QueryWrapper<ScoreAccount>().eq("id", id));
        ModelAndView mv = new ModelAndView("customer/score/customerScoreEdit");
        mv.addObject("as",cl);
        mv.addObject("aid",cl.getUid());
        return mv;
    }

    
    @RequestMapping("/deleteOne")
    public MsgUtil deleteOne(Integer aid){
        int i = scoreAccountMapper.deleteById(aid);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids) {
        List list = new ArrayList();
        for (Integer id : ids) {
            list.add(id);
        }
        int i = scoreAccountMapper.deleteBatchIds(list);
        return MsgUtil.flag(i);
    }
	

}
