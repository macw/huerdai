package com.tourism.hu.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.aliyun.oss.OSS;
import com.tourism.hu.oss.OSSClientConstants;
import com.tourism.hu.util.FileUploadUtil;
import com.tourism.hu.util.GlobalConfigUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * @ClassName: UploadController 
 * @Description: 上传控制 
 * @author 杜东兴
 * @date 2019年11月13日 下午1:07:23 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
public class UploadController extends BaseController {

	/**
	 * 
	 * @Title: uploadImage 
	 * @Description: (上传名宿房间图片接口) 
	 * @param file
	 * @return  
	 * @date 2019年11月15日 上午10:50:55
	 * @author 董兴隆
	 */
	@RequestMapping("uploadImage")
	public ResultUtil uploadImage(MultipartFile file,Integer residenceroomId) {
		String basePath = "residenceRoom";
		String src ="";
		try {
			src = aliOSSUpload(file, basePath,residenceroomId);
		} catch (IOException e) {
			
		}
		Map<String, String> map = new HashMap<>();
		map.put("src",src);
		return ResultUtil.ok("成功",map);
	}
	/**
	 * 
	 * @Title: saveSpotInfoImg 
	 * @Description: (上传景点详情图片接口) 
	 * @param file
	 * @return  
	 * @date 2019年11月15日 上午10:51:32
	 * @author 董兴隆
	 */
	@RequestMapping("saveSpotInfoImg")
	public ResultUtil saveSpotInfoImg(MultipartFile file,Integer spotId) {
		String basePath = "scenicSpot/spotInfo";
		Map<String, String> map = new HashMap<>();
		String msg = "";
		try {
			String src = aliOSSUpload(file, basePath, spotId);
			map.put("src", src);
			return ResultUtil.ok(msg,map);
		} catch (IOException e1) {
			map.put("src", "");
			map.put("msg", e1.getMessage());
			return ResultUtil.error(msg,map);
		}
	}

	
	@RequestMapping("saveRoutesImg")
	public ResultUtil saveRoutesImg(MultipartFile file,Integer routesId) {
		String basePath = "saveRoutesInfoImg";
		Map<String, String> map = new HashMap<>();
		String msg = "";
		try {
			String src = aliOSSUpload(file, basePath, routesId);
			map.put("src", src);
			return ResultUtil.ok(msg,map);
		} catch (IOException e1) {
			map.put("src", "");
			map.put("msg", e1.getMessage());
			return ResultUtil.error(msg,map);
		}
	}


	/**
	 * 达人/大咖图片上传
	 * @param file
	 * @return
	 */
	@RequestMapping("/saveStarImg")
	public ResultUtil saveStarImg(MultipartFile file) {
		String basePath = "saveStarInfoImg";
		Map<String, String> map = new HashMap<>();
		String msg = "";
		try {
			String src = aliOSSUpload(file, basePath);
			map.put("src", src);
			logger.debug("map-------------------------------"+map);
			return ResultUtil.ok(msg,map);
		} catch (IOException e1) {
			map.put("src", "");
			map.put("msg", e1.getMessage());
			return ResultUtil.error(msg,map);
		}
	}




}
