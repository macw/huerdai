package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.entity.Address;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.Region;
import com.tourism.hu.mapper.AddressMapper;
import com.tourism.hu.mapper.CustomerInfoMapper;
import com.tourism.hu.mapper.RegionMapper;
import com.tourism.hu.service.IRegionService;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@RestController
@RequestMapping("/address")
public class AddressController extends BaseController {

    @Resource
    private AddressMapper addressMapper;

    @Resource
    private CustomerInfoMapper customerInfoMapper;

    @Autowired
    private IRegionService regionService;

    @Resource
    private RegionMapper regionMapper;


    @RequestMapping("/toaddress")
    public ModelAndView hello() {
        ModelAndView mv = new ModelAndView("customer/address/customerAddressList");
        return mv;
    }

    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page, int limit, String nickname) {
        Page<Address> page1 = new Page<>(page, limit);
        IPage<Address> IPage = addressMapper.selectPage(page1, null);
        if (nickname != null) {
            List<CustomerInfo> customerInfoList = customerInfoMapper.selectList(new QueryWrapper<CustomerInfo>().like("nickname", nickname));
            int ids[] = new int[customerInfoList.size()];
            for (int i = 0; i < customerInfoList.size(); i++) {
                ids[i] = customerInfoList.get(i).getCustomerId();
            }
            StringBuffer sb = new StringBuffer();
            for(int i=0;i<ids.length;i++){
                if (i!=ids.length-1){
                    sb.append(ids[i]+",");
                }else {
                    sb.append(ids[i]);
                }
            }
            IPage = addressMapper.selectPage(page1, new QueryWrapper<Address>().inSql("customer_id", sb.toString()));
        }
        //获取分页后的数据
        List<Address> List = IPage.getRecords();
        for (Address address : List) {
            CustomerInfo customer_id = customerInfoMapper.selectOne(new QueryWrapper<CustomerInfo>().eq("customer_id", address.getCustomerId()));
            if (customer_id != null) {
                address.setNickname(customer_id.getNickname());
            }
        }
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        DataUtil dataUtil = DataUtil.success();
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }

    @RequestMapping("/tocustomerAddressEdit")
    public ModelAndView tocustomerAddressEdit(Address address) {
        ModelAndView mv = new ModelAndView("customer/address/customerAddressEdit");
        List<CustomerInfo> user_stats = customerInfoMapper.selectList(new QueryWrapper<CustomerInfo>().eq("user_stats", BaseConstant.USE_STATUS));
        mv.addObject("cs", user_stats);
        List<Region> provinceList = selectProvinceList();
        mv.addObject("provinceList", provinceList);
        if (address.getProvince() != null && address.getProvince() > 0) {
            List<Region> cityList = regionService.list(new QueryWrapper<Region>().eq("pid", address.getProvince()));
            mv.addObject("cityList", cityList);
        }
        if (address.getCity() != null && address.getCity() > 0) {
            List<Region> districtList = regionService.list(new QueryWrapper<Region>().eq("pid", address.getCity()));
            mv.addObject("districtList", districtList);
        }
        return mv;
    }



    @RequestMapping("/selectOne")
    public ModelAndView selectOne(Integer aid) {
        Address address = addressMapper.selectOne(new QueryWrapper<Address>().eq("address_id", aid));
        ModelAndView mv = new ModelAndView("customer/address/customerAddressEdit");
        mv.addObject("ad",address);
        List<CustomerInfo> user_stats = customerInfoMapper.selectList(new QueryWrapper<CustomerInfo>().eq("user_stats", BaseConstant.USE_STATUS));
        mv.addObject("cs", user_stats);
        List<Region> provinceList = selectProvinceList();
        mv.addObject("provinceList", provinceList);
        if (address.getProvince() != null && address.getProvince() > 0) {
            List<Region> cityList = regionService.list(new QueryWrapper<Region>().eq("pid", address.getProvince()));
            mv.addObject("cityList", cityList);
        }
        if (address.getCity() != null && address.getCity() > 0) {
            List<Region> districtList = regionService.list(new QueryWrapper<Region>().eq("pid", address.getCity()));
            mv.addObject("districtList", districtList);
        }
        return mv;
    }


    /**
     * 添加 或者更新收货地址
     * 这里缺少用户id
     * @param address
     * @return
     */
    @RequestMapping("/addOrUpdate")
    public MsgUtil addAddress(Address address) {
        Region province = regionMapper.selectOne(new QueryWrapper<Region>().eq("id", address.getProvince()));
        Region city = regionMapper.selectOne(new QueryWrapper<Region>().eq("id", address.getCity()));
        Region dist = regionMapper.selectOne(new QueryWrapper<Region>().eq("id", address.getDistrict()));
        address.setAddress(province.getName() + city.getName() + dist.getName() + address.getStreet());
//        address.setCustomerId();
        if (address.getAddressId()==null) {
            address.setAstatus(BaseConstant.USE_STATUS);
            int i = addressMapper.insert(address);
            return MsgUtil.flag(i);
        }else {
            int i = addressMapper.updateById(address);
            return MsgUtil.flag(i);
        }
    }

    @RequestMapping("/updateStatus")
    public MsgUtil updateStatus(Address address){
        int i = addressMapper.updateById(address);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/deleteOne")
    public MsgUtil deleteOne(Integer aid){
        int i = addressMapper.deleteById(aid);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids) {
        List list = new ArrayList();
        for (Integer id : ids) {
            list.add(id);
        }
        int i = addressMapper.deleteBatchIds(list);
        return MsgUtil.flag(i);
    }

}
