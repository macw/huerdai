package com.tourism.hu.controller;


import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.entity.Merchant;
import com.tourism.hu.entity.PaymentChannel;
import com.tourism.hu.entity.User;
import com.tourism.hu.entity.dto.PaymentChannelDto;
import com.tourism.hu.mapper.MerchantMapper;
import com.tourism.hu.mapper.PaymentChannelMapper;
import com.tourism.hu.service.IPaymentChannelService;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * 
 * @ClassName: PaymentChannelController 
 * @Description: (支付渠道  前端控制器) 
 * @author 董兴隆
 * @date 2019年9月17日 下午2:55:00 
 *
 */
@RestController
@RequestMapping("/payment/channel")
public class PaymentChannelController  extends BaseController{

	 @Autowired
	 private IPaymentChannelService channelService;
	 
	 @Autowired
	 private PaymentChannelMapper channelMapper;
	 
	 @Autowired
	 private MerchantMapper merchantMapper;
	 
	 
	/**
	 * 
	 * @Title: page 
	 * @Description: (支付管理/支付渠道管理首页)  
	 * @param dto
	 * @return  
	 * @date 2019年9月19日 上午11:15:03
	 * @author 董兴隆
	 */
	 @RequestMapping("/page")
	 public ModelAndView page(PaymentChannelDto dto) {
		 ModelAndView mv = new ModelAndView("/payment/channel/channelList");
		 //去重所有支付类型 用于搜索
		 mv.addObject("payMethods", channelMapper.selectPayMethods());
		 //所有公司列表
		 QueryWrapper<Merchant> merchantQuery = new  QueryWrapper<Merchant>().ne("status", 2);
		 mv.addObject("merchantList", merchantMapper.selectList(merchantQuery));
		 
		 mv.addObject("dto", dto);
		 return mv;
	 }

	 @RequestMapping("/editPage")
	 public ModelAndView editPage(PaymentChannelDto dto) {
		 ModelAndView mv = new ModelAndView("/payment/channel/channelEdit");
		 //所有公司列表
		 QueryWrapper<Merchant> merchantQuery = new  QueryWrapper<Merchant>().ne("status", 2);
		 mv.addObject("merchantList", merchantMapper.selectList(merchantQuery));
		 if(dto.getChannelId()!=null && dto.getChannelId()>0) {
			 PaymentChannel channel =channelMapper.selectById(dto.getChannelId());
			 mv.addObject("channel", channel);
		 }
		 mv.addObject("dto", dto);
		 return mv;
	 }
	 
	 /**
	  * 
	  * @Title: selectData 
	  * @Description: (支付渠道管理首页获取数据接口) 
	  * @param dto
	  * @return  
	  * @date 2019年9月19日 上午11:16:23
	  * @author 董兴隆
	  */
	 @GetMapping("/selectData")
	 @ResponseBody
	 public ResultUtil<List<PaymentChannelDto>> selectData(PaymentChannelDto dto){
		 ResultUtil<List<PaymentChannelDto>> result = ResultUtil.success("查询成功");
		 PageUtil.initAttr(dto);
		 try {
			 long count = channelMapper.getPaymentChannelCount(dto);
			 List<PaymentChannelDto>  paymentChannel=channelMapper.selectPaymentChannel(dto);
			 result.setCount(count);
			 result.setData(paymentChannel);
		 }catch(Exception e){
			 result = ResultUtil.error("查询失败");
		 }
		 return result;
	 }
	 
	/*
	 * public DataUtil<PaymentChannelDto> selectData(PaymentChannelDto dto){
	 * PageUtil.initAttr(dto); long count =
	 * channelMapper.getPaymentChannelCount(dto); List<PaymentChannelDto>
	 * paymentChannel=channelMapper.selectPaymentChannel(dto);
	 * DataUtil<PaymentChannelDto> data = DataUtil.success(); data.setCount(count);
	 * data.setData(paymentChannel); return data; }
	 */
	 
	 
	/**
	  * 
	  * @Title: delById 
	  * @Description: (支付渠道管理首页删除接口) 
	  * @param dto
	  * @return  
	  * @date 2019年9月19日 上午11:16:48
	  * @author 董兴隆
	  */
	 @GetMapping("/w")
	 @ResponseBody
	 public DataUtil<PaymentChannelDto> delByChannel(PaymentChannelDto dto){
		 DataUtil<PaymentChannelDto> data = DataUtil.error();
		 if(channelMapper.deleteById(dto.getChannelId())>0) {
			 data.setCode(0);
			 data.setMsg("删除成功");
		 }
		 return data;
	 }
	 
	 
	 
	 
	 /**
	  * 
	  * @Title: save 
	  * @Description: (支付渠道管理添加接口) 
	  * @param dto
	  * @return  
	  * @date 2019年9月19日 上午11:17:19
	  * @author 董兴隆
	  */
	 @PostMapping("/")
	 @ResponseBody
	 public DataUtil<PaymentChannelDto> save(PaymentChannelDto dto){
		 DataUtil<PaymentChannelDto> data = DataUtil.error();
		 data.setMsg("添加失败");
		 if(channelMapper.insert(dto)>0) {
			 data.setCode(0);
			 data.setMsg("添加成功");
		 }
		 return data;
	 }
	 
	 /**
	  * 
	  * @Title: update 
	  * @Description: (支付渠道管理修改接口) 
	  * @param dto
	  * @return  
	  * @date 2019年9月19日 上午11:17:40
	  * @author 董兴隆
	  */
	 @PutMapping("/")
	 @ResponseBody
	 public ResultUtil<PaymentChannelDto> update(PaymentChannelDto dto){
		 ResultUtil<PaymentChannelDto> data = ResultUtil.error("更新失败");
		 User user = getUser();
		 if(user == null) {
			 return data.error("获取登陆用户失败");
		 }
		 dto.setUpdatedBy(user.getName());
		 dto.setUpdatedTime(LocalDateTime.now());
		 if(channelMapper.updateById(dto)>0) {
			 data.setCode(0);
			 data.setMsg("更新成功");
		 }
		 return data;
	 }
	 @PostMapping("/updateOrSave")
	 @ResponseBody
	 public ResultUtil<PaymentChannelDto> updateOrSave(PaymentChannelDto dto){
		 ResultUtil<PaymentChannelDto> data = ResultUtil.error();
		 User user = getUser();
		 if(user == null) {
			 return data.error("获取登陆用户失败");
		 }
		 if(dto.getChannelId()!=null && dto.getChannelId()>0) {
			 data.setMsg("更新失败");
			 dto.setUpdatedBy(user.getName());
			 dto.setUpdatedTime(LocalDateTime.now());
			 if(channelMapper.updateById(dto)>0) {
				 data.setCode(0);
				 data.setMsg("更新成功");
			 }
		 }else {
			 data.setMsg("添加失败");
			 dto.setCreatedBy(user.getName());
			 dto.setCreatedTime(LocalDateTime.now());
			 if(channelMapper.insert(dto)>0) {
				 data.setCode(0);
				 data.setMsg("添加成功");
			 }
		 }
		 return data;
	 }
	 
	 /**
	  * 
	  * @Title: getById 
	  * @Description: (通过支付渠道Id获得该支付渠道信息) 
	  * @param dto
	  * @return  
	  * @date 2019年9月19日 上午11:18:44
	  * @author 董兴隆
	  */
	 @GetMapping("/getById")
	 @ResponseBody
	 public ResultUtil<PaymentChannel> getById(PaymentChannelDto dto){
		 ResultUtil<PaymentChannel> result = ResultUtil.error();
		 PaymentChannel paymentChannel= channelMapper.selectById(dto.getChannelId());
		 result.setData(paymentChannel);
		 return result;
	 }
	
}
