package com.tourism.hu.controller;


import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.constant.CustomerInfoConstant;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.Region;
import com.tourism.hu.entity.User;
import com.tourism.hu.entity.WechatRegionCode;
import com.tourism.hu.mapper.CustomerInfoMapper;
import com.tourism.hu.mapper.RegionMapper;
import com.tourism.hu.mapper.WechatRegionCodeMapper;
import com.tourism.hu.service.IRegionService;
import com.tourism.hu.service.IWechatRegionCodeService;

/**
 * 
 * @ClassName: WechatRegionCodeController 
 * @Description: 前端控制器
 * @author 马超伟
 * @date 2019年10月19日 下午1:03:26 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/wechatRegionCode")
public class WechatRegionCodeController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());


    @Resource
    private WechatRegionCodeMapper wechatRegionCodeMapper;

    @Autowired
    private IWechatRegionCodeService wechatRegionCodeService;
    
    @Resource
    private RegionMapper regionMapper;

    @Resource
    private CustomerInfoMapper customerInfoMapper;

    @Autowired
    private IRegionService regionService;
    
    @RequestMapping("/towechatAreaList")
    public ModelAndView towechatAreaList(){
        ModelAndView mv = new ModelAndView("wechatArea/wechatAreaList");
        return mv;
    }

    @RequestMapping("/towechatAreaEdit")
    public ModelAndView towechatAreaEdit(Region region){
        ModelAndView mv = new ModelAndView("wechatArea/wechatAreaEdit");
        System.out.println(region);
        List<Region> provinceList = selectProvinceList();
        mv.addObject("provinceList", provinceList);
        if (region.getId()!=null){
            WechatRegionCode wechatRegionCode = wechatRegionCodeMapper.selectOne(new QueryWrapper<WechatRegionCode>().eq("id", region.getId()));
            mv.addObject("wr",wechatRegionCode);
            if(wechatRegionCode.getRegionId()!=null && wechatRegionCode.getRegionId()>0){
                List<Region> cityList = selectByPidRegionList(wechatRegionCode.getRegionId());
                mv.addObject("cityList", cityList);
            }
        }
        List<CustomerInfo> user_stats = customerInfoMapper.selectList(new QueryWrapper<CustomerInfo>().eq("status", BaseConstant.USE_STATUS));
        mv.addObject("us",user_stats);
        return mv;
    }


    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page,int limit,String customerName,String wechatNumber){
        Page<WechatRegionCode> page1 = new Page<>(page,limit);
        IPage<WechatRegionCode> IPage =  wechatRegionCodeMapper.selectPage(page1, null);
        QueryWrapper<WechatRegionCode> qw = new QueryWrapper<>();
        if (customerName!=null && customerName!=""){
            qw.like("customer_name",customerName);
        }
        if (wechatNumber!=null && wechatNumber!=""){
            qw.like("wechat_number",wechatNumber);
        }
        IPage =  wechatRegionCodeMapper.selectPage(page1, qw);
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<WechatRegionCode> List = IPage.getRecords();
        for (WechatRegionCode wechatRegionCode : List) {
            Region region = regionMapper.selectOne(new QueryWrapper<Region>().eq("id", wechatRegionCode.getRegionId()));
            Region region1 = regionMapper.selectOne(new QueryWrapper<Region>().eq("id", wechatRegionCode.getRegionCityId()));
            if (region!=null){
                wechatRegionCode.setProvinceName(region.getSname());
            }
            if (region1!=null){
                wechatRegionCode.setCityName(region1.getSname());
            }
        }
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }


    @RequestMapping("/saveOrUpdate")
    public ResultUtil<WechatRegionCode> saveOrUpdate(MultipartFile file, WechatRegionCode dto, HttpServletRequest request){
        logger.debug("dto=====1"+dto.toString());
    	if(isNotLogIn()) {
    	    logger.debug("未找到登录账号信息");
    		return ResultUtil.error("未找到登录账号信息");
    	}
    	User user = getUser();
        boolean save=true;
        CustomerInfo customerInfo = customerInfoMapper.selectOne(new QueryWrapper<CustomerInfo>().eq("id", dto.getCustomerId()));
        logger.debug("customerInfo========"+customerInfo);
        if(dto.getId()!=null && dto.getId()>0) {
            save=false;
            //更新
            dto.setUpdateId(user.getUserId());
    		dto.setUpdateUser(user.getName());
    		dto.setUpdateTime(LocalDateTime.now());
    	}else {
    	    //添加
    		dto.setCreateId(user.getUserId());
    		dto.setCreateUser(user.getName());
    		dto.setCreateTime(LocalDateTime.now());
    	}
        logger.debug("file====="+file);
    	if(file!=null&&file.getSize()>0) {
            String basePath = "areacode";
            String src="";
            try {
            	src = aliOSSUpload(file, basePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            logger.debug("src===="+src);
            dto.setWechatUrl(src);
    	}
    	if (customerInfo!=null){
    	    dto.setCustomerName(customerInfo.getNickname());
        }
        logger.debug("dto======="+dto.toString());
    	int i;
        if (save){
            i = wechatRegionCodeMapper.insert(dto);
        }
        else {
            i = wechatRegionCodeMapper.updateById(dto);
        }
        logger.debug("i===="+i);
        if (i>0){
            return ResultUtil.ok("保存成功");
        }else {
            return ResultUtil.error("保存失败");
        }
    }
    
    
    @RequestMapping("/deleteById")
    public ResultUtil<WechatRegionCode> deleteById(WechatRegionCode dto){
    	if(wechatRegionCodeService.removeById(dto.getId())) {
    		return ResultUtil.ok("删除成功");
    	}else {
    		return ResultUtil.error("删除失败");
    	}
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids) {
        List list = new ArrayList();
        for (Integer id : ids) {
            list.add(id);
        }
        int i = wechatRegionCodeMapper.deleteBatchIds(list);
        return MsgUtil.flag(i);
    }
    


}
