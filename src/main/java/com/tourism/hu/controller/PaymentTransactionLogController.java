package com.tourism.hu.controller;

import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.entity.Merchant;
import com.tourism.hu.entity.PaymentTransactionLog;
import com.tourism.hu.entity.dto.PaymentTransactionLogDto;
import com.tourism.hu.mapper.MerchantMapper;
import com.tourism.hu.mapper.PaymentChannelMapper;
import com.tourism.hu.mapper.PaymentTransactionLogMapper;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * 
 * @ClassName: PaymentTransactionLogController
 * @Description: (支付交易日志表 前端控制器)
 * @author 董兴隆
 * @date 2019年9月20日 上午10:13:23
 *
 */
@RestController
@RequestMapping("/payment/transLog")
public class PaymentTransactionLogController extends BaseController {

	@Resource
	private PaymentTransactionLogMapper paymentTransactionLogMapper;

	@Autowired
	private PaymentChannelMapper channelMapper;

	@Autowired
	private MerchantMapper merchantMapper;

	/**
	 * 
	 * @Title: page
	 * @Description: (支付交易日志首页)
	 * @return
	 * @date 2019年9月20日 上午10:21:32
	 * @author 董兴隆
	 */
	@RequestMapping("/page")
	public ModelAndView page() {
		ModelAndView mv = new ModelAndView("/payment/transLog/transactionLogList");
		// 去重所有支付类型 用于搜索
		mv.addObject("payMethods", channelMapper.selectPayMethods());
		// 所有公司列表
		QueryWrapper<Merchant> merchantQuery = new QueryWrapper<Merchant>().ne("status", 2);
		mv.addObject("merchantList", merchantMapper.selectList(merchantQuery));
		return mv;
	}

	/**
	 * 
	 * @Title: selectData
	 * @Description: (首页数据)
	 * @param dto
	 * @return
	 * @date 2019年9月20日 上午11:21:24
	 * @author 董兴隆
	 */
	@RequestMapping("/selectData")
	@ResponseBody
	public ResultUtil<List<PaymentTransactionLogDto>> selectData(PaymentTransactionLogDto dto) {
		ResultUtil<List<PaymentTransactionLogDto>> result = ResultUtil.error("查询失败");
		PageUtil.initAttr(dto);
		try {
			long total = paymentTransactionLogMapper.getCount(dto);
			List<PaymentTransactionLogDto> list = paymentTransactionLogMapper.selectData(dto);
			result = ResultUtil.ok("查询成功");
			result.setData(list);
			result.setCount(total);
		} catch (Exception e) {
			result.setMsg(e.getMessage());
		}
		return result;
	}
	
	/**
	 * 
	 * @Title: deleteById 
	 * @Description: (删除日志) 
	 * @param dto
	 * @return  
	 * @date 2019年9月20日 下午5:03:24
	 * @author 董兴隆
	 */
	@RequestMapping("/deleteById")
	@ResponseBody
	public ResultUtil<PaymentTransactionLogDto> deleteById(PaymentTransactionLogDto dto) {
		ResultUtil<PaymentTransactionLogDto> result = ResultUtil.error("删除失败");
		try {
			if(paymentTransactionLogMapper.deleteById(dto.getId())>0) {
				result = ResultUtil.ok("删除成功");
			}
		} catch (Exception e) {
			result.setMsg(e.getMessage());
		}
		return result;
	}
	
	/**
	 * 
	 * @Title: add 
	 * @Description: (添加日志) 
	 * @param transLog
	 * @return  
	 * @date 2019年9月20日 下午5:03:38
	 * @author 董兴隆
	 */
	@RequestMapping("/add")
	@ResponseBody
	public ResultUtil<PaymentTransactionLog> add(PaymentTransactionLog transLog) {
		ResultUtil<PaymentTransactionLog> result = ResultUtil.error("添加失败");
		try {
			transLog.setCreatedTime(LocalDateTime.now());
			if(paymentTransactionLogMapper.insert(transLog)>0) {
				result = ResultUtil.ok("添加成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setMsg(e.getMessage());
		}
		return result;
	}

}
