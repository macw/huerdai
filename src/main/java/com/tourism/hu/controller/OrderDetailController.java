package com.tourism.hu.controller;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.OrderCart;
import com.tourism.hu.entity.OrderDetail;
import com.tourism.hu.mapper.OrderCartMapper;
import com.tourism.hu.mapper.OrderDetailMapper;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;

/**
 * <p>
 * 订单详情表 前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@RestController
@RequestMapping("/orderdetail")
public class OrderDetailController {

	@Resource
    private OrderDetailMapper orderDetailMapper;


	 @RequestMapping("/toorderdetail")
	 public ModelAndView hello(Integer orderId) {
	        ModelAndView mv = new ModelAndView("order/orderDetails");
	        mv.addObject("orderId",orderId);
	        return mv;
	 }
		
    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page,int limit,Integer orderId){
        Page<OrderDetail> page1 = new Page<>(page,limit);
        IPage<OrderDetail> IPage = orderDetailMapper.selectPage(page1, new QueryWrapper<OrderDetail>().eq("order_id",orderId));
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<OrderDetail> List = IPage.getRecords();
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }
    
    
    @RequestMapping("/addorderDetail")
    public MsgUtil add(OrderDetail cl){
        int i = orderDetailMapper.insert(cl);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/selectOne")
    public OrderDetail selectOne(Integer aid){
    	OrderDetail cl = orderDetailMapper.selectOne(new QueryWrapper<OrderDetail>().eq("order_detail_id", aid));
        return cl;
    }

    @RequestMapping("/update")
    public MsgUtil update(OrderDetail cl){
        int i = orderDetailMapper.updateById(cl);
        return MsgUtil.flag(i);
    }

    
    @RequestMapping("/delete")
    public MsgUtil deleteOne(Integer aid){
        int i = orderDetailMapper.deleteById(aid);
        return MsgUtil.flag(i);
    
    }
	
	
	
	
	
	
	
	
	
	
	
}
