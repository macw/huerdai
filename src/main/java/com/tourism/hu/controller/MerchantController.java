package com.tourism.hu.controller;


import java.time.LocalDateTime;

import javax.annotation.Resource;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.Merchant;
import com.tourism.hu.entity.User;
import com.tourism.hu.entity.dto.MerchantDto;
import com.tourism.hu.mapper.MerchantMapper;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.PageUtil;


/**
 * 
 * @ClassName: MerchantController 
 * @Description: (商户表 前端控制器) 
 * @author 董兴隆
 * @date 2019年9月11日 下午6:17:40 
 *
 */
@RestController
@RequestMapping("/merchant")
public class MerchantController extends BaseController{

	@Resource
    private MerchantMapper merchantMapper;
	    

	/**
	 * 
	 * @Title: page 
	 * @Description: (跳转商户表页面) 
	 * @return  
	 * @date 2019年9月16日 下午3:18:49
	 * @author 董兴隆
	 */
	@RequestMapping("/page")
	public ModelAndView page(){
		ModelAndView mv = new ModelAndView("/merchant/merchantList");
		return mv;
	}
	
	/**
	 * 
	 * @Title: selectMerchantList 
	 * @Description: (查询商户表数据) 
	 * @date 2019年9月16日 下午3:18:36
	 * @author 董兴隆
	 */
    @RequestMapping("/selectMerchantList")
    public DataUtil selectMerchantList(MerchantDto dto){
        PageUtil.initAttr(dto);
        Page<Merchant> page = new Page<>(dto.getPage(),dto.getLimit());
        QueryWrapper<Merchant> merchantQuery = new  QueryWrapper<Merchant>().ne("status", 2);
        if(!StringUtils.isEmpty(dto.getMerName())) {
        	merchantQuery.like("m_name", dto.getMerName());
        }
        if(!StringUtils.isEmpty(dto.getPhone())) {
        	merchantQuery.like("phone", dto.getPhone());
        }
        if(!StringUtils.isEmpty(dto.getCreateUser())) {
        	merchantQuery.and(wrapper -> wrapper.like("create_user",  dto.getCreateUser()).or().like("update_user", dto.getCreateUser()));
        }
        merchantQuery.orderByDesc("create_time");
        IPage<Merchant> merchant=merchantMapper.selectPage(page,merchantQuery);
        DataUtil dataUtil = DataUtil.success();
        dataUtil.setData(merchant.getRecords());
        dataUtil.setCount(merchant.getTotal());
        return dataUtil;
    }
    
	/**
	 * 
	 * @Title: update 
	 * @Description: (编辑商户信息) 
	 * @param dto
	 * @return  
	 * @date 2019年9月16日 下午3:18:16
	 * @author 董兴隆
	 */
    @RequestMapping("/update")
    @ResponseBody
    public DataUtil update(MerchantDto dto){
    	User user = getUser();
    	DataUtil dataUtil = DataUtil.success();
    	if(dto.getMerId()==null || dto.getMerId()==0) {
        	dto.setCreateUserId(user.getUserId());
        	dto.setCreateUser(user.getName());
        	dto.setCreateTime(LocalDateTime.now());
        	dto.setStatus(1);//默认设置为启用状态
    		if(merchantMapper.insert(dto)<=0) {
    			dataUtil.setCode(400);
    			dataUtil.setMsg("添加失败");
    		}
    	}else {
        	dto.setUpdateUserId(user.getUserId());
        	dto.setUpdateUser(user.getName());
    		dto.setUpdateTime(LocalDateTime.now());
    		if(merchantMapper.updateById(dto)<=0) {
    			dataUtil.setCode(400);
    			dataUtil.setMsg("修改失败");
    		}
    	}
        return dataUtil;
    }
    /**
     * 
     * @Title: editPage 
     * @Description: (跳转到编辑页面) 
     * @param dto
     * @return  
     * @date 2019年9月16日 下午3:17:30
     * @author 董兴隆
     */
    @RequestMapping("/editPage")
    public ModelAndView editPage(MerchantDto dto){
    	 ModelAndView mv = new ModelAndView("/merchant/merchantEdit");
    	 if(dto.getMerId()!=null && dto.getMerId()>0) {
    		 Merchant merch= merchantMapper.selectById(dto.getMerId());
    		 mv.addObject("merch", merch);
    	 }
    	 return mv;
    	 
    }
    
    /**
     * 
     * @Title: del 
     * @Description: (删除商户表) 
     * @date 2019年9月16日 下午3:17:45
     * @author 董兴隆
     */
    @RequestMapping("/del")
    @ResponseBody
    public DataUtil del(MerchantDto dto){
    	DataUtil dataUtil = DataUtil.success();
    	dto.setUpdateTime(LocalDateTime.now());
    	dto.setStatus(2);
    	User user = getUser();
    	dto.setUpdateUserId(user.getUserId());
    	dto.setUpdateUser(user.getName());
    	if(merchantMapper.deleteById(dto.getMerId())<=0) {
    		dataUtil.setCode(400);
    		dataUtil.setMsg("删除失败");
    	}
    	return dataUtil;
    }
	
	
	
	
}
