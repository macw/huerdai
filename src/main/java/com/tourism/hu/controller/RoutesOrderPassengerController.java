package com.tourism.hu.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tourism.hu.controller.BaseController;

/**
 * 
 * @ClassName: RoutesOrderPassengerController 
 * @Description: 旅游订单乘车人信息表 前端控制器
 * @author 马超伟
 * @date 2019年10月12日 下午4:15:15 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/hu/routes-order-passenger")
public class RoutesOrderPassengerController extends BaseController {

}
