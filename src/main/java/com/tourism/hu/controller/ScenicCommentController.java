package com.tourism.hu.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tourism.hu.controller.BaseController;

/**
 * @ClassName: ScenicCommentController 
 * @Description: 线路评论表 前端控制器
 * @author 马超伟
 * @date 2019年9月25日 下午1:14:50 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/hu/scenic-comment")
public class ScenicCommentController extends BaseController {

}
