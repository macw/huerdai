package com.tourism.hu.controller;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.MegtemplateConstant;
import com.tourism.hu.entity.User;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.Megtemplate;
import com.tourism.hu.mapper.MegtemplateMapper;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@RestController
@RequestMapping("/megtemplate")
public class MegtemplateController extends BaseController {

	@Resource
    private MegtemplateMapper megtemplateMapper;


	@RequestMapping("/tomegtemplate")
    public ModelAndView hello() {
        ModelAndView mv = new ModelAndView("/system/megtemplate");
        return mv;
    }
	
    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page,int limit,String title){
        Page<Megtemplate> page1 = new Page<>(page,limit);
        IPage<Megtemplate> IPage = null;
        if (title == null || title == ""){
            IPage = megtemplateMapper.selectPage(page1, new QueryWrapper<Megtemplate>().eq("status", BaseConstant.USE_STATUS));
        }else {
            IPage = megtemplateMapper.selectPage(page1, new QueryWrapper<Megtemplate>().like("title",title).eq("status", BaseConstant.USE_STATUS));
        }
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<Megtemplate> List = IPage.getRecords();
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }
	

    @RequestMapping("/addmegtemplate")
    public MsgUtil add(Megtemplate cl){
        User user = getUser();
        cl.setCreatedId(user.getUserId());
        cl.setCreatedName(user.getName());
        cl.setCreationDate(LocalDateTime.now());
        cl.setStatus(""+BaseConstant.USE_STATUS);
        int i = megtemplateMapper.insert(cl);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/selectOne")
    public Megtemplate selectOne(Integer aid){
    	Megtemplate cl = megtemplateMapper.selectOne(new QueryWrapper<Megtemplate>().eq("id", aid));
        return cl;
    }

    @RequestMapping("/update")
    public MsgUtil update(Megtemplate cl){
        User user = getUser();
        cl.setLastUpdateDate(LocalDateTime.now());
        cl.setLastUpdatedId(user.getUserId());
        cl.setLastUpdateName(user.getName());
        cl.setStatus(""+BaseConstant.USE_STATUS);
        int i = megtemplateMapper.updateById(cl);
        return MsgUtil.flag(i);
    }

    
    @RequestMapping("/deleteOne")
    public MsgUtil deleteOne(Integer aid){
//        int i = megtemplateMapper.deleteById(aid);
        Megtemplate megtemplate = new Megtemplate();
        megtemplate.setStatus(""+BaseConstant.DISUSE_STATUS);
        megtemplate.setId(aid);
        int i = megtemplateMapper.updateById(megtemplate);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids){
        List list = new ArrayList();
        int i = 0;
        for (Integer id : ids) {
            list.add(id);
            Megtemplate megtemplate = new Megtemplate();
            megtemplate.setStatus(""+BaseConstant.DISUSE_STATUS);
            megtemplate.setId(id);
             i += megtemplateMapper.updateById(megtemplate);
        }
//        int i = megtemplateMapper.deleteBatchIds(list);

        return MsgUtil.flag(i);
    }

	
}
