package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.CouponConstant;
import com.tourism.hu.entity.Coupon;
import com.tourism.hu.entity.Coupongoods;
import com.tourism.hu.entity.Goods;
import com.tourism.hu.entity.User;
import com.tourism.hu.mapper.CouponMapper;
import com.tourism.hu.mapper.CoupongoodsMapper;
import com.tourism.hu.mapper.GoodsMapper;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 优惠券商品表 前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-30
 */
@RestController
@RequestMapping("/coupongoods")
public class CoupongoodsController extends BaseController {

    @Resource
    private CoupongoodsMapper coupongoodsMapper;

    @Resource
    private GoodsMapper goodsMapper;

    @Resource
    private CouponMapper couponMapper;

    @RequestMapping("/toCoupongoods")
    public ModelAndView toCoupongoods(){
        ModelAndView mv = new ModelAndView("coupongoods/coupongoodsList");
        return mv;
    }

    @RequestMapping("/toCoupongoodsEdit")
    public ModelAndView toCoupongoodsEdit(Integer cpId){
        ModelAndView mv = new ModelAndView("coupongoods/coupongoodsEdit");
        Coupongoods coupongoods = coupongoodsMapper.selectById(cpId);
        mv.addObject("cpg",coupongoods);
        List<Coupon> couponList = couponMapper.selectList(new QueryWrapper<Coupon>().eq("status", CouponConstant.THREE_STATUS));
        mv.addObject("cpl",couponList);
        List<Goods> goodsList = goodsMapper.selectList(new QueryWrapper<Goods>().eq("status", BaseConstant.USE_STATUS));
        mv.addObject("gl",goodsList);
        return mv;
    }

    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page, int limit, String goodsname,String couponName) {
        Page<Coupongoods> page1 = new Page<>(page, limit);
        IPage<Coupongoods> IPage = coupongoodsMapper.selectPage(page1, null);
        QueryWrapper<Coupongoods> qw = new QueryWrapper<>();
        if (goodsname!=null){
            List<Goods> goods = goodsMapper.selectList(new QueryWrapper<Goods>().eq("status", BaseConstant.USE_STATUS).like("goodsname", goodsname));
            int ids[] = new int[goods.size()];
            for (int i = 0; i <goods.size() ; i++) {
                ids[i]=goods.get(i).getGoodsId();
            }
            StringBuffer sb = new StringBuffer();
            for(int i=0;i<ids.length;i++){
                if (i!=ids.length-1){
                    sb.append(ids[i]+",");
                }else {
                    sb.append(ids[i]);
                }
            }
            qw.inSql("goods_id",sb.toString());
        }
        if (couponName!=null){
            List<Coupon> coupons = couponMapper.selectList(new QueryWrapper<Coupon>().eq("status", CouponConstant.THREE_STATUS).like("coupon_name", couponName));
            int ids[] = new int[coupons.size()];
            for (int i = 0; i <coupons.size() ; i++) {
                ids[i]=coupons.get(i).getCouponId();
            }
            StringBuffer sb = new StringBuffer();
            for(int i=0;i<ids.length;i++){
                if (i!=ids.length-1){
                    sb.append(ids[i]+",");
                }else {
                    sb.append(ids[i]);
                }
            }
            qw.inSql("coupon_id",sb.toString());
        }
        IPage = coupongoodsMapper.selectPage(page1, qw);
        //获取分页后的数据
        List<Coupongoods> List = IPage.getRecords();
        for (Coupongoods coupongoods : List) {
            if (coupongoods.getGoodsId()!=null){
                Goods goods = goodsMapper.selectById(coupongoods.getGoodsId());
                coupongoods.setGoodsname(goods.getGoodsname());
            }
            if (coupongoods.getCouponId()!=null){
                Coupon coupon = couponMapper.selectById(coupongoods.getCouponId());
                coupongoods.setCouponName(coupon.getCouponName());
            }
        }
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        DataUtil dataUtil = DataUtil.success();
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }

    @RequestMapping("/addOrUpdate")
    public MsgUtil addOrUpdate(Coupongoods coupongoods){
        User user = getUser();
        if (user==null){
            return MsgUtil.error("获取用户信息失败，请重新登录");
        }
        if (coupongoods.getCpId()==null){
            //添加
            coupongoods.setCreatedId(user.getUserId());
            coupongoods.setCreatedName(user.getName());
            coupongoods.setCreationDate(LocalDateTime.now());
            coupongoods.setStatus(BaseConstant.USE_STATUS);
            int i = coupongoodsMapper.insert(coupongoods);
            return MsgUtil.flag(i);
        }else {
            //更新
            coupongoods.setLastUpdateDate(LocalDateTime.now());
            coupongoods.setLastUpdatedId(user.getUserId());
            coupongoods.setLastUpdateName(user.getName());
            int i = coupongoodsMapper.updateById(coupongoods);
            return MsgUtil.flag(i);
        }
    }

    @RequestMapping("/deleteOne")
    public MsgUtil deleteOne(Integer aid){
        int i = coupongoodsMapper.deleteById(aid);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids) {
        List list = new ArrayList();
        for (Integer id : ids) {
            list.add(id);
        }
        int i = coupongoodsMapper.deleteBatchIds(list);
        return MsgUtil.flag(i);
    }

}
