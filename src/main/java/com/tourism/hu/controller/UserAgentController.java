package com.tourism.hu.controller;


import com.alipay.api.domain.Person;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.UserConstant;
import com.tourism.hu.entity.*;
import com.tourism.hu.entity.dto.AgentDto;
import com.tourism.hu.entity.vo.OrderProfitFlowing;
import com.tourism.hu.mapper.ProfitFlowingMapper;
import com.tourism.hu.mapper.UserMapper;
import com.tourism.hu.mapper.UserRoleMapper;
import com.tourism.hu.service.*;
import com.tourism.hu.util.*;

import cn.hutool.http.useragent.UserAgent;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 代理商
 * 省代控制器
 */
@RestController
@RequestMapping("/userAgent")
public class UserAgentController extends BaseController {

    @Resource
    private IUserService iUserService;

    @Resource
    private IUserRoleService iUserRoleService;

    @Resource
    private IRoleService iRoleService;

    @Resource
    private ICustomerInfoService iCustomerInfoService;

    @Resource
    private IProfitFlowingService iProfitFlowingService;

    @Resource
    private ProfitFlowingMapper profitFlowingMapper;

    @Resource
    private IOrderDetailService iOrderDetailService;

    @RequestMapping("/toproAgent")
    public ModelAndView hello() {
        ModelAndView mv = new ModelAndView("agent/proAgent/proAgentList");
        return mv;
    }

    @RequestMapping("/tobackUser")
    public ModelAndView tobackUser() {
        ModelAndView mav = new ModelAndView("users/administrators/userlist");
        return mav;
    }

    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page, int limit, String name) {
        User user = getUser();
        if (user==null){
            return DataUtil.error();
        }
        DataUtil dataUtil = iUserService.selectAgentAll(page, limit, name,user);
        return dataUtil;
    }

    @RequestMapping("/selectOne")
    public User selectOne(Integer aid) {
        User user = iUserService.selectOne(aid);
        return user;
    }

    
    @RequestMapping("/sharePage")
	public ModelAndView sharePage(User dto){
		ModelAndView mv = new ModelAndView("agent/agentShare");
		User user = iUserService.getById(dto.getUserId());
		mv.addObject("sharecode", user.getSharecode());
		String sharecodeUrl = GlobalConfigUtil.getKey("shareAppUrl")+"?sharecode="+isNullNotAppend(user.getSharecode());
		mv.addObject("sharecodeUrl", sharecodeUrl);
		String shareQRCode=user.getSharecodeUrl();
		if(StringUtils.isBlank(shareQRCode)) {
			String destPath = GlobalConfigUtil.getUploadUrl();
			try {
				shareQRCode = QRCodeUtils.encode(sharecodeUrl, destPath);
				File file = new File(shareQRCode);
				shareQRCode = aliOSSUpload(file, "qrcode");
				user.setSharecodeUrl(shareQRCode);
				iUserService.updateById(user);
				file.delete();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		mv.addObject("shareQRCode", shareQRCode);
		return mv;
	}
    
    /**
     * 去添加区域代理商
     * @param userIdAgent
     * @return
     */
    @RequestMapping("/toEditUserAgent")
    public ModelAndView toEditUserAgent(Integer userIdAgent) {
        ModelAndView mv = new ModelAndView("agent/proAgent/proAgentEdit");
        if (userIdAgent!=null) {
            logger.debug("userid====="+userIdAgent);
            User user = iUserService.getOne(new QueryWrapper<User>().eq("user_id", userIdAgent));
            mv.addObject("ua", user);
        }
        return mv;
    }

    /**
     * 省级代理商，去添加代理
     * @param user
     * @return
     */
    @RequestMapping("/updateOrSaveAgent")
    public ResultUtil updateOrSaveAgent(User user) {
        User loginUser = getUser();
        ResultUtil<User> data = ResultUtil.error();
        if(loginUser == null) {
            return data.error("获取登录用户失败");
        }
        user.setParentId(loginUser.getUserId());
        user.setDepartmentId(UserConstant.DEP_THREE);
        boolean save = false;
        
        if(user.getUserId() == null) {
        	save = true;
        	user.setCreateTime(LocalDateTime.now());
            user.setCreateUser(loginUser.getName());
            user.setCreateUserId(loginUser.getUserId());
        }else {
        	 user.setUpdateTime(LocalDateTime.now());
             user.setUpdateUser(loginUser.getName());
             user.setUpdateUserId(loginUser.getUserId());
        }
        if(iUserService.saveOrUpdate(user)) {
        	if(save) {
        		user.setSharecode(user.getUserId().toString());
        		if(iUserService.updateById(user)) {
        			return ResultUtil.ok("添加成功");
        		}else {
        			return ResultUtil.ok("修改分享码失败");
        		}
        	}else {
        		return ResultUtil.ok("修改成功");
        	}
        }else {
        	if(save) {
        		return ResultUtil.error("添加失败");
        	}else {
        		return ResultUtil.error("修改失败");
        	}
        }
        
    }

    @RequestMapping("/deleteOne")
    public MsgUtil deleteOne(Integer aid) {
        MsgUtil msgUtil = iUserService.deleteOne(aid);
        return msgUtil;
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids) {
        MsgUtil msgUtil = iUserService.delectMany(ids);
        return msgUtil;
    }

    @RequestMapping("/toselOrder")
    public ModelAndView toselOrder(Integer userId){
        ModelAndView mv = new ModelAndView("agent/proAgent/selectProfiting");
        mv.addObject("userId", userId);
        return mv;
    }

    @RequestMapping("/selOrder")
    public ResultUtil selOrder( Integer page, Integer limit, Integer userId, String nickname, String goodsname, String orderSn){
        ResultUtil<List<OrderProfitFlowing>> dataUtil = new ResultUtil<>();

        List<OrderProfitFlowing> collect = profitFlowingMapper.getOrderProfit(page,limit,userId,nickname,goodsname,orderSn);

       /* Integer userId = cust.getUserId();
        List<CustomerInfo> customerInfoList = iCustomerInfoService.list(new QueryWrapper<CustomerInfo>().eq("user_id", userId));
        for (CustomerInfo customerInfo : customerInfoList) {
            List<ProfitFlowing> profitFlowingList = iProfitFlowingService.list(new QueryWrapper<ProfitFlowing>().eq("cus_id", customerInfo.getCustomerId()).eq("is_pay", BaseConstant.USE_STATUS).eq("type", 2));
            for (ProfitFlowing profitFlowing : profitFlowingList) {
                logger.debug("profitFlowing===="+profitFlowing.getOrderSn());
                OrderDetail orderDetail = iOrderDetailService.getOne(new QueryWrapper<OrderDetail>().eq("order_sn", profitFlowing.getOrderSn()));
                OrderProfitFlowing flowing = new OrderProfitFlowing();
                if (orderDetail!=null){
                    flowing.setPictureUrl(GlobalConfigUtil.getServiceUrl()+orderDetail.getGoodsPictureUrl());
                    if (orderDetail.getGoodsName()!=null){
                        logger.debug("orderDetail.getGoodsName()==="+orderDetail.getGoodsName());
                        flowing.setGoodsname(orderDetail.getGoodsName());
                    }
                }
                logger.debug("orderDetail==="+orderDetail);
                flowing.setNickname(customerInfo.getNickname());
                flowing.setCustomerIcon(customerInfo.getCustomerIcon());
                flowing.setOrderSn(profitFlowing.getOrderSn());
                //订单总金额
                flowing.setOrderPriceAll(profitFlowing.getOrderPriceAll());
                //抵扣总金额
                flowing.setSubPriceAll(profitFlowing.getSubPriceAll());
                //实际支付金额
                flowing.setSalePrice(profitFlowing.getSalePrice());
                flowing.setNumber(profitFlowing.getNumber());
                //用户返利金额
                flowing.setCusMoney(profitFlowing.getCusMoney());
                //上级用户返利金额
                flowing.setParentcusMoney(profitFlowing.getParentcusMoney());
                //代理商利润金额
                flowing.setAgentMoney(profitFlowing.getAgentMoney());
                flowing.setCreationDate(profitFlowing.getCreationDate());
                collect.add(flowing);
            }
        }
*/
        Long count = profitFlowingMapper.getCount(page,limit,userId,nickname,goodsname,orderSn);
        dataUtil.setCode(0);
        dataUtil.setCount(count);
        dataUtil.setData(collect);
        dataUtil.setMsg("查询成功！");
        return dataUtil;
    }


}
