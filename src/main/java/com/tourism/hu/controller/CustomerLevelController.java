package com.tourism.hu.controller;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.tourism.hu.entity.*;
import com.tourism.hu.mapper.CustomerInfoMapper;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.mapper.CustomerFriendMapper;
import com.tourism.hu.mapper.CustomerLevelMapper;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;

/**
 * <p>
 * 用户信息表 前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@RestController
@RequestMapping("/customerlevel")
public class CustomerLevelController extends BaseController {

	@Resource
    private CustomerLevelMapper customerLevelMapper;

	@Resource
	private CustomerInfoMapper customerInfoMapper;

	@RequestMapping("/tocustomerLevel")
    public ModelAndView hello() {
        ModelAndView mav = new ModelAndView("customer/level/customerLevelList");
        return mav;
    }

    @RequestMapping("/tocustomerLevelEdit")
    public ModelAndView tocustomerInfoEdit() {
        ModelAndView mav = new ModelAndView("customer/level/customerLevelEdit");
        return mav;
    }

    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page,int limit,String levelName){
        Page<CustomerLevel> page1 = new Page<>(page,limit);
        IPage<CustomerLevel> IPage =  customerLevelMapper.selectPage(page1, null);
        if (levelName!=null && levelName!=""){
            IPage =  customerLevelMapper.selectPage(page1, new QueryWrapper<CustomerLevel>().like("level_name",levelName));
        }
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<CustomerLevel> List = IPage.getRecords();
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }

    @RequestMapping("/addOrUpdate")
    public MsgUtil addcoupon(CustomerLevel cl){
        User user = getUser();
        if (cl.getLevelId()==null){
            cl.setCreateUserId(user.getUserId());
            cl.setCreateUser(user.getName());
            cl.setCreateTime(LocalDateTime.now());
            int i = customerLevelMapper.insert(cl);
            return MsgUtil.flag(i);
        }else {
            cl.setUpdateUserId(user.getUserId());
            cl.setUpdateUser(user.getName());
            cl.setUpdateTime(LocalDateTime.now());
            int i = customerLevelMapper.updateById(cl);
            return MsgUtil.flag(i);
        }

    }

    @RequestMapping("/selectOne")
    public ModelAndView selectOne(Integer aid){
        ModelAndView mv = new ModelAndView("customer/level/customerLevelEdit");
        CustomerLevel cl = customerLevelMapper.selectOne(new QueryWrapper<CustomerLevel>().eq("level_id", aid));
    	mv.addObject("cs",cl);
        return mv;
    }

    @RequestMapping("/deleteOne")
    public MsgUtil deleteOne(Integer aid){
        List<CustomerInfo> customerInfoList = customerInfoMapper.selectList(null);
        for (CustomerInfo customerInfo : customerInfoList) {
            if (customerInfo.getCustomerLevelId()==aid){
                MsgUtil msgUtil = MsgUtil.error();
                msgUtil.setMsg("该等级下有用户关联，删除失败！");
                return msgUtil;
            }
        }
        int i = customerLevelMapper.deleteById(aid);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids) {
        List list = new ArrayList();
        List<CustomerInfo> customerInfoList = customerInfoMapper.selectList(null);
        for (Integer id : ids) {
            list.add(id);
            for (CustomerInfo customerInfo : customerInfoList) {
                if (customerInfo.getCustomerLevelId()==id){
                    MsgUtil msgUtil = MsgUtil.error();
                    msgUtil.setMsg("该等级下有用户关联，删除失败！");
                    return msgUtil;
                }
            }
        }
        int i = customerLevelMapper.deleteBatchIds(list);
        return MsgUtil.flag(i);
    }
	

    
}
