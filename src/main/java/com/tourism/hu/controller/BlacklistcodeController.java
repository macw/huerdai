package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.Blacklistcode;
import com.tourism.hu.entity.PrefixThird;
import com.tourism.hu.entity.User;
import com.tourism.hu.mapper.BlacklistcodeMapper;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import com.tourism.hu.util.ResultUtil;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-29
 */
@RestController
@RequestMapping("/blacklistcode")
public class BlacklistcodeController extends BaseController {

    @Resource
    private BlacklistcodeMapper blacklistcodeMapper;

    @RequestMapping("/tobackBlack")
    public ModelAndView tobackRole() {
        ModelAndView mav = new ModelAndView("system/blacklistcode");
        return mav;
    }

    /**
     * 分页，页面table数据展示
     * @param page
     * @param limit
     * @param code
     * @return
     */
    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page, int limit, String code){
        Page<Blacklistcode> blacklistcodePage = new Page<>(page,limit);
        //查询分页
        IPage<Blacklistcode> blacklistcodeIPage = null;
        if (code == null || code == "" ){
            blacklistcodeIPage = blacklistcodeMapper.selectPage(blacklistcodePage, null);
        }else{
            blacklistcodeIPage = blacklistcodeMapper.selectPage(blacklistcodePage, new QueryWrapper<Blacklistcode>().like("code", code));
        }
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<Blacklistcode> blacklistcodeList = blacklistcodeIPage.getRecords();
        //获取分页总条数
        long total = blacklistcodeIPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(blacklistcodeList);
        dataUtil.setCount(total);
        return dataUtil;
    }

    @RequestMapping("/updateStatus")
    public MsgUtil updateStatus(Blacklistcode blacklistcode) {
        int i = blacklistcodeMapper.updateById(blacklistcode);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/selectOne")
    public Blacklistcode selectOne(Integer aid) {
        Blacklistcode blacklistcode = blacklistcodeMapper.selectOne(new QueryWrapper<Blacklistcode>().eq("id", aid));
        return blacklistcode;
    }


    @RequestMapping("/add")
    public MsgUtil addConfig(Blacklistcode blacklistcode){
        User user = getUser();
        blacklistcode.setCreatedId(user.getUserId());
        blacklistcode.setCreatedName(user.getName());
        blacklistcode.setCreationDate(LocalDateTime.now());
        int i = blacklistcodeMapper.insert(blacklistcode);
        return MsgUtil.flag(i);
    }



    @RequestMapping("/update")
    public MsgUtil updateOne(Blacklistcode blacklistcode){
        User user = getUser();
        blacklistcode.setLastUpdatedId(user.getUserId());
        blacklistcode.setLastUpdateName(user.getName());
        blacklistcode.setLastUpdateDate(LocalDateTime.now());
        int i = blacklistcodeMapper.updateById(blacklistcode);
        return MsgUtil.flag(i);
    }


    @RequestMapping("/deleteOne")
    public MsgUtil deleteOne(Integer aid) {
        int i = blacklistcodeMapper.deleteById(aid);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids) {
        List list = new ArrayList();
        for (Integer id : ids) {
            list.add(id);
        }
        int i = blacklistcodeMapper.deleteBatchIds(list);
        return MsgUtil.flag(i);
    }

}
