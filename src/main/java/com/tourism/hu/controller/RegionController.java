package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.Region;
import com.tourism.hu.mapper.RegionMapper;
import com.tourism.hu.service.IRegionService;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import com.tourism.hu.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@RestController
@RequestMapping("/region")
public class RegionController extends BaseController {


    @Resource
    private RegionMapper regionMapper;

    @Autowired
    private IRegionService regionService;


    @RequestMapping("/region")
    public ModelAndView hello(Region region) {
        ModelAndView mv = new ModelAndView("system/regionNew");
        List<Region> provinceList = selectProvinceList();
        mv.addObject("provinceList", provinceList);
        List<Region> cityList = regionService.list(new QueryWrapper<Region>().eq("pid", region.getPid()));
        mv.addObject("cityList", cityList);
        List<Region> districtList = regionService.list(new QueryWrapper<Region>().eq("pid", region.getPid()));
        mv.addObject("districtList", districtList);
        return mv;
    }

    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page, int limit, Integer id) {
        Page<Region> page1 = new Page<>(page, limit);
        IPage<Region> regionIPage = regionMapper.selectPage(page1, null);
        QueryWrapper<Region> qw = new QueryWrapper<>();
        if (id!=null){
            qw.eq("pid",id);
            regionIPage = regionMapper.selectPage(page1,qw);
        }
        List<Region> regionList = regionIPage.getRecords();
        DataUtil dataUtil = DataUtil.success();
        dataUtil.setData(regionList);
        dataUtil.setCount(regionIPage.getTotal());
        return dataUtil;
    }

    /**
     * 树形表格， treeTable
     *
     * @return
     */
    @RequestMapping("/selectTreeTable")
    public DataUtil selectTreeTable() {
        List<Region> regionList = regionMapper.selectList(null);
        DataUtil dataUtil = DataUtil.success();
        dataUtil.setData(regionList);
        Integer count = regionMapper.selectCount(null);
        long l = Long.parseLong(count.toString());
        dataUtil.setCount(l);
        return dataUtil;
    }


    /**
     * @param reg
     * @return
     * @Title: AjaxRegionList
     * @Description: (查询省市区)
     * @date 2019年9月29日 下午2:32:39
     * @author 董兴隆
     */
    @RequestMapping("/ajaxRegionList")
    @ResponseBody
    public ResultUtil<List<Region>> AjaxRegionList(Region reg) {
        ResultUtil<List<Region>> result = ResultUtil.error("查询失败");
        try {
            List<Region> regionList = regionService.list(
                    new QueryWrapper<Region>().eq(reg.getPid() != null && reg.getPid() > 0, "pid", reg.getPid())
                            .eq(!StringUtils.isEmpty(reg.getCitycode()), "citycode", reg.getCitycode())
                            .eq(!StringUtils.isEmpty(reg.getYzcode()), "yzcode", reg.getYzcode()));
            result = ResultUtil.success("查询成功");
            result.setData(regionList);
        } catch (Exception e) {

        }
        return result;
    }

    @RequestMapping("/AjaxCityList")
    @ResponseBody
    public ResultUtil<List<Region>> AjaxCityList(Region reg) {
        ResultUtil<List<Region>> result = ResultUtil.error("查询失败");
        try {
            List<Region> regionList = regionService.list(new QueryWrapper<Region>().eq("pid", reg.getPid()));
            result.setData(regionList);
            result = ResultUtil.success("查询成功");
        } catch (Exception e) {

        }
        return result;
    }




    @RequestMapping("/addregion")
    public MsgUtil add(Region cl) {
        int i = regionMapper.insert(cl);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/selectOne")
    public Region selectOne(Integer aid) {
        Region cl = regionMapper.selectOne(new QueryWrapper<Region>().eq("id", aid));
        return cl;
    }

    @RequestMapping("/update")
    public MsgUtil update(Region cl) {
        int i = regionMapper.updateById(cl);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/delete")
    public MsgUtil deleteOne(Integer aid) {
        int i = regionMapper.deleteById(aid);
        return MsgUtil.flag(i);

    }


}
