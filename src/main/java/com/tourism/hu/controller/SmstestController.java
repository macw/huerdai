package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.Smstest;
import com.tourism.hu.mapper.SmstestMapper;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @ClassName: SmstestController 
 * @Description: 验证码登录表 前端控制器
 * @author 马超伟
 * @date 2019年10月9日 下午4:10:22 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/smstest")
public class SmstestController {

	@Resource
    private SmstestMapper smstestMapper;

    @RequestMapping("/tosmstest")
    public ModelAndView hello() {
        ModelAndView mv = new ModelAndView("system/smsTest");
        return mv;
    }

    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page, int limit, String mobileNumber){
        Page<Smstest> page1 = new Page<>(page,limit);
        IPage<Smstest> IPage = null;
        if (mobileNumber == null || mobileNumber == ""){
            IPage = smstestMapper.selectPage(page1, null);
        }else {
            IPage = smstestMapper.selectPage(page1, new QueryWrapper<Smstest>().like("mobile_number",mobileNumber));
        }
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<Smstest> List = IPage.getRecords();
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }
	

    @RequestMapping("/addsmstest")
    public MsgUtil add(Smstest cl){
        int i = smstestMapper.insert(cl);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/selectOne")
    public Smstest selectOne(Integer id){
    	Smstest cl = smstestMapper.selectOne(new QueryWrapper<Smstest>().eq("msale_sms_id", id));
        return cl;
    }

    @RequestMapping("/update")
    public MsgUtil update(Smstest cl){
        int i = smstestMapper.updateById(cl);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/deleteOne")
    public MsgUtil deleteOne(Integer id){
        int i = smstestMapper.deleteById(id);
        return MsgUtil.flag(i);
    
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids){
        List list = new ArrayList();
        for (Integer id : ids) {
            list.add(id);
        }
        int i = smstestMapper.deleteBatchIds(list);
        return MsgUtil.flag(i);
    }
	
	
	
}
