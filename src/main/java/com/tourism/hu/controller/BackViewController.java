package com.tourism.hu.controller;

import org.apache.jasper.tagplugins.jstl.core.Redirect;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 15:21
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@Controller
public class BackViewController extends BaseController {

    @RequestMapping("/admin")
    public ModelAndView toBack(){
        ModelAndView mv = new ModelAndView();
        if (isNotLogIn()){
            //如果没有登录，跳转到登录页面，
            mv.setViewName("redirect:/views/login.jsp");
        }else {
            //登录成功跳转到后台首页
            mv.setViewName("redirect:/views/index.jsp");
        }
        return mv;
    }

}
