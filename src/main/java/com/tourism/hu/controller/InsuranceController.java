package com.tourism.hu.controller;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.Insurance;
import com.tourism.hu.entity.User;
import com.tourism.hu.entity.dto.InsuranceDto;
import com.tourism.hu.mapper.InsuranceMapper;
import com.tourism.hu.service.IInsuranceService;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * 
 * @ClassName: InsuranceController 
 * @Description: (保险Controller) 
 * @author 董兴隆
 * @date 2019年10月11日 下午3:29:11 
 *
 */
@RestController
@RequestMapping("/insurance")
public class InsuranceController extends BaseController{

	 @Autowired
	 private InsuranceMapper insuranceMapper;
	
	 @Autowired
	 private IInsuranceService insuranceService;
	
	 @RequestMapping("/page")
	 public ModelAndView hello() {
        ModelAndView mv = new ModelAndView("insurance/insuranceList");
        return mv;
	 }
	 
	 @RequestMapping("/editPage")
	 public ModelAndView editPage(InsuranceDto dto) {
        ModelAndView mv = new ModelAndView("insurance/insuranceEdit");
        if(dto.getInsId()!=null && dto.getInsId()>0) {
        	dto.getInsId();
        	Insurance insurance = insuranceService.getById(dto.getInsId());
        	DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        	String expiresDate = "";
        	if(insurance.getExpiresBegindate() != null) {
        		expiresDate = insurance.getExpiresBegindate().format(formatter2);
        		dto.setExpiresStartDate(expiresDate);
        	}
        	if(insurance.getExpiresEnddate() != null) {
        		expiresDate = insurance.getExpiresEnddate().format(formatter2);
        		dto.setExpiresEndDate(expiresDate);
        	}
        	BeanUtils.copyProperties(insurance, dto);
        	 
        }
        mv.addObject("dto", dto);
        return mv;
	 }
	
    @RequestMapping("/selectData")
    @ResponseBody
    public ResultUtil<List<Insurance>> selectData(Insurance ins){
    	List<Insurance> list = new ArrayList<>();
    	long count = 0;
    	ResultUtil<List<Insurance>> result = ResultUtil.error("查询失败",list);
    	PageUtil.initAttr(ins);
    	IPage<Insurance> iPage=null;
	    Page<Insurance> page = new Page<>(ins.getPage(),ins.getLimit());
	    try {
	    	QueryWrapper<Insurance> qw = new QueryWrapper<Insurance>();
	    	if(!StringUtils.isEmpty(ins.getInsName())) {
	    		qw.eq("ins_name", ins.getInsName());
	    	}
	    	if(!StringUtils.isEmpty(ins.getTitle())) {
	    		qw.eq("title", ins.getTitle());
	    	}
	    	if(!StringUtils.isEmpty(ins.getOpenid())) {
	    		qw.and(temp-> temp.eq("openid", ins.getOpenid()).or().eq("platform", ins.getOpenid()));
	    	}
	    	qw.and(temp-> temp.eq("status", 1).or().eq("status", 2));
	    	iPage = insuranceMapper.selectPage(page, qw);
	    	result = ResultUtil.ok("查询成功",list);
	    	count = iPage.getTotal();
	    	list = iPage.getRecords();
	    }catch (Exception e) {
	    	result.setMsg("查询异常"+e.getMessage());
		}
	    result.setCount(count);
	    result.setData(list);
	    return result;
    }
    
    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public ResultUtil<Insurance> saveOrUpdate(Insurance ins){
    	User user = getUser();
    	if(user == null || user.getUserId() == null ) {
    		return ResultUtil.error("未找到登录用户");
    	}
    	boolean save =  true;
    	if(ins.getInsId()!=null && ins.getInsId()>0) {
    		ins.setLastUpdateDate(LocalDateTime.now());
    		ins.setLastUpdatedId(user.getUserId());
    		ins.setLastUpdateName(user.getName());
    		save = false;
    	}else {
    		ins.setCreatedId(user.getUserId());
    		ins.setCreatedName(user.getName());
    		ins.setCreationDate(LocalDateTime.now());
    		ins.setStatus(1);
    	}
    	if(insuranceService.saveOrUpdate(ins)) {
    		if(save) {
    			return ResultUtil.ok("添加成功");
    		}else {
    			return ResultUtil.ok("修改成功");
    		}
    	}else {
			if(save) {
				return ResultUtil.error("添加失败");
    		}else {
    			return ResultUtil.error("修改失败");
    		} 
    	}
    }
    @RequestMapping("/updateYnByInsId")
    @ResponseBody
    public ResultUtil<Insurance> updateYnByInsId(Insurance ins){
    	User user = getUser();
    	if(user == null || user.getUserId() == null ) {
    		return ResultUtil.error("未找到登录用户");
    	}
    	Insurance insurance = insuranceService.getById(ins.getInsId());
    	insurance.setStatus(insurance.getStatus()==1?2:1);
    	if(insuranceService.saveOrUpdate(insurance)) {
    		return ResultUtil.ok("修改成功");
    	}else {
    		return ResultUtil.error("修改失败");
    	}
    }
	
   
    @RequestMapping("/deleteByInsId")
    @ResponseBody
    public ResultUtil<Insurance> deleteByInsId(Insurance ins){
    	User user = getUser();
    	if(user == null || user.getUserId() == null ) {
    		return ResultUtil.error("未找到登录用户");
    	}
    	ins.setStatus(3);//删除
    	ins.setLastUpdateDate(LocalDateTime.now());
    	ins.setLastUpdatedId(user.getUserId());
    	ins.setLastUpdateName(user.getName());
    	if(insuranceService.updateById(ins)) {
    		return ResultUtil.ok("删除成功");
    	}else {
    		return ResultUtil.error("删除失败");
    	}
    }
	
	
	
	
	
}
