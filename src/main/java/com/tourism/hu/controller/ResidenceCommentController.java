package com.tourism.hu.controller;


import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.entity.ResidenceComment;
import com.tourism.hu.entity.User;
import com.tourism.hu.entity.dto.ResidenceCommentDto;
import com.tourism.hu.mapper.ResidenceCommentMapper;
import com.tourism.hu.service.IResidenceCommentService;
import com.tourism.hu.util.FileUploadUtil;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * @ClassName: ResidenceCommentController 
 * @Description: (民宿评论) 
 * @author 董兴隆
 * @date 2019年10月17日 下午4:19:15 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/residence/comment")
public class ResidenceCommentController  extends BaseController{

	
	@Autowired
	private IResidenceCommentService commentService;
	
	@Autowired
	private ResidenceCommentMapper commentMapper;
	
	@RequestMapping("/page")
	public ModelAndView page() {
		ModelAndView mv = new ModelAndView("/residence/comment/residenceCommentList");
		return mv;
	}

	@RequestMapping("/editPage")
	public ModelAndView editPage(ResidenceCommentDto dto) {
		ModelAndView mv = new ModelAndView("/residence/comment/residenceCommentEdit");
		mv.addObject("isDel", false);
		if(dto.getCommentParent()!=null && dto.getCommentParent()>0) {
			ResidenceComment res = commentService.getOne(new QueryWrapper<ResidenceComment>().eq("comment_parent", dto.getCommentParent()),false);
			if(res!=null) {
				BeanUtils.copyProperties(res, dto);
				mv.addObject("isDel", true);
			}
		}
		mv.addObject("dto", dto);
		return mv;
	}

	@RequestMapping("/selectData")
	public ResultUtil<List<ResidenceCommentDto>> selectData(ResidenceCommentDto dto) {
		PageUtil.initAttr(dto);
		List<ResidenceCommentDto> list = new ArrayList<>();
		ResultUtil<List<ResidenceCommentDto>> result = ResultUtil.error("查询失败", list);
		try {
			long count =commentMapper.getCount(dto);
			result = ResultUtil.ok("查询成功", list);
			if(count > 0) {
				list = commentMapper.selectPageDataList(dto);
				result.setCount(count);
			}
		}catch (Exception e) {
			result.setMsg(e.getMessage());
		}
		result.setData(list);
		return result;
	}
	
	@RequestMapping("saveOrUpdate")
	public ResultUtil<ResidenceCommentDto> saveOrUpdate(MultipartFile file,ResidenceCommentDto dto){
		User user = getUser();
		if (isNotLogIn()) {
			return ResultUtil.error("未找到登录用户");
		}
		if(file != null && file.getSize() > 0) {
			String basePath = "residenceComment";
			String url ="";
			try {
				url = aliOSSUpload(file, basePath, dto.getResidenceId());
			} catch (IOException e) {
				e.printStackTrace();
			}
			dto.setPictureurl(url);
		}
		boolean save = true;
		if (dto.getResidenceOrderId() != null && dto.getResidenceOrderId() > 0) {
			dto.setModifiedTime(LocalDateTime.now());
			save = false;
		} else {
			dto.setAuditTime(LocalDateTime.now());
			dto.setCustomerId(user.getUserId());
		}
		if (commentService.saveOrUpdate(dto)) {
			if (save) {
				return ResultUtil.ok("添加成功");
			} else {
				return ResultUtil.ok("修改成功");
			}
		} else {
			if (save) {
				return ResultUtil.error("添加失败");
			} else {
				return ResultUtil.error("修改失败");
			}
		}
	}
	
	/**
	 * 
	 * @Title: saveOrUpdate 
	 * @Description: (保存回复) 
	 * @param dto
	 * @return  
	 * @date 2019年10月23日 下午4:07:39
	 * @author 董兴隆
	 */
	@RequestMapping("saveReply")
	public ResultUtil<ResidenceCommentDto> saveReply(MultipartFile file,ResidenceCommentDto dto){
		User user = getUser();
		if (isNotLogIn()) {
			return ResultUtil.error("未找到登录用户");
		}
		if(file != null && file.getSize() > 0) {
			String basePath = "residenceComment";
			String url ="";
			try {
				url = aliOSSUpload(file, basePath, dto.getResidenceId());
			} catch (IOException e) {
				e.printStackTrace();
			}
			dto.setPictureurl(url);
		}
		if(dto.getCustomerId()==null || dto.getCustomerId()<=0) {
			dto.setCustomerId(user.getUserId());
		}
		if(StringUtils.isEmpty(dto.getCustomerName())) {
			dto.setCustomerName(user.getName());
		}
		dto.setAuditStatus(1);
		dto.setAuditTime(LocalDateTime.now());
		dto.setModifiedTime(LocalDateTime.now());
		if (commentService.saveOrUpdate(dto)) {
			return ResultUtil.ok("添加成功");
		} else {
			return ResultUtil.error("添加失败");
		}
	}
	
	@RequestMapping("/deleteByResidenceCommentId")
	public  ResultUtil<ResidenceCommentDto> deleteByResidenceCommentId(ResidenceCommentDto dto){
		if(commentService.removeById(dto.getCommentId())) {
			return ResultUtil.ok("删除成功");
		}else {
			return ResultUtil.error("删除失败");
		}
	}
	@RequestMapping("/updateYnByResidenceCommentId")
	public ResultUtil<ResidenceCommentDto> updateYnByResidenceCommentId(ResidenceCommentDto dto){
		ResidenceComment res = commentService.getById(dto.getCommentId());
		int status = res.getAuditStatus()==1?0:1;
		res.setAuditStatus(status);
		if(commentService.updateById(res)) {
			return ResultUtil.ok("修改成功");
		}else {
			return ResultUtil.error("修改失败");
		}
	}
	
}
