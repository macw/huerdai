package com.tourism.hu.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tourism.hu.controller.BaseController;

/**
 * <p>
 * 规格描述表 前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-14
 */
@RestController
@RequestMapping("/hu/goods-dtl-img")
public class GoodsDtlImgController extends BaseController {

}
