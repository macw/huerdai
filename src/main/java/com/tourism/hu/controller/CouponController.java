package com.tourism.hu.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.Coupon;
import com.tourism.hu.entity.User;
import com.tourism.hu.entity.dto.CouponDto;
import com.tourism.hu.mapper.CouponMapper;
import com.tourism.hu.service.ICouponService;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.DateConverterUtil;
import com.tourism.hu.util.MsgUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * 
 * @ClassName: CouponController
 * @Description: (红包/优惠券)
 * @author 董兴隆
 * @date 2019年10月15日 上午10:25:58
 *
 */
@RestController
@RequestMapping("/coupon")
public class CouponController extends BaseController {

	@Resource
	private CouponMapper couponMapper;

	@Autowired
	private ICouponService couponService;

	@RequestMapping("/page")
	public ModelAndView page() {
		ModelAndView mv = new ModelAndView("/coupon/couponList");
		return mv;
	}

	@RequestMapping("/editPage")
	public ModelAndView editPage(CouponDto dto) {
		ModelAndView mv = new ModelAndView("/coupon/couponEdit");
		if (dto.getCouponId() != null && dto.getCouponId() > 0) {
			Coupon coupon = couponService.getById(dto.getCouponId());
			if(coupon.getValidityStartdate()!=null) {
				String validityStartdate= DateConverterUtil.dateFormat(coupon.getValidityStartdate(), DateConverterUtil.date);
				dto.setExpiresStartDate(validityStartdate);
			}
			if(coupon.getValidityEnddate()!=null) {
				String validityEnddate= DateConverterUtil.dateFormat(coupon.getValidityEnddate(), DateConverterUtil.date);
				dto.setExpiresEndDate(validityEnddate);
			}
			BeanUtils.copyProperties(coupon, dto);
		}
		mv.addObject("dto", dto);
		return mv;
	}

	@RequestMapping("/selectData")
	@ResponseBody
	public ResultUtil<List<Coupon>> selectData(Coupon dto) {
		List<Coupon> list = new ArrayList<>();
		ResultUtil<List<Coupon>> result = ResultUtil.error("查询失败", list);
		try {
			Page<Coupon> page = new Page<Coupon>(dto.getPage(), dto.getLimit());
			QueryWrapper<Coupon> qw= new QueryWrapper<Coupon>();
			qw.eq(!StringUtils.isEmpty(dto.getCouponName()), "coupon_name", dto.getCouponName());
			
			if(dto.getStatus()!=null && dto.getStatus()>0) {
				qw.eq("status",dto.getStatus());
			}
			if(!StringUtils.isEmpty(dto.getCreatedName())) {
				qw.and(fn -> fn.eq("created_name", dto.getCreatedName()).or().eq("last_update_name", dto.getCreatedName()));
			}
			qw.orderByDesc("creation_date");
			IPage<Coupon> iPage = couponMapper.selectPage(page, qw);
			result = ResultUtil.ok("查询成功", list);
			result.setData(iPage.getRecords());
			result.setCount(iPage.getTotal());
		} catch (Exception e) {
			result.setMsg(e.getMessage());
		}
		return result;
	}

	@RequestMapping("/deleteByCouponId")
	@ResponseBody
	public ResultUtil<Coupon> deleteByCouponId(Coupon dto) {
		if (isNotLogIn()) {
			return ResultUtil.error("未找到登录账号信息");
		}
		if (couponService.removeById(dto.getCouponId())) {
			return ResultUtil.ok("删除成功");
		} else {
			return ResultUtil.error("删除成功");
		}
	}

	@RequestMapping("/saveOrUpdate")
	@ResponseBody
	public ResultUtil<Coupon> saveOrUpdate(CouponDto dto) {
		if (isNotLogIn()) {
			return ResultUtil.error("未找到登录账号信息");
		}
		User user = getUser();
		boolean save = true;
		if (dto.getCouponId() != null && dto.getCouponId() > 0) {
			save = false;
			dto.setLastUpdateDate(LocalDateTime.now());
			dto.setLastUpdatedId(user.getUserId());
			dto.setLastUpdateName(user.getName());
		} else {
			dto.setCreatedId(user.getUserId());
			dto.setCreatedName(user.getName());
			dto.setCreationDate(LocalDateTime.now());
		}
		if(dto.getExpiresStartDate() != null) {
			LocalDateTime validityStartdate =DateConverterUtil.setDayZero(dto.getExpiresStartDate());
			dto.setValidityStartdate(validityStartdate);
		}
		if(dto.getExpiresEndDate() != null) {
			LocalDateTime validityEnddate =DateConverterUtil.setDayLast(dto.getExpiresEndDate());
			dto.setValidityEnddate(validityEnddate);
		}
		if (couponService.saveOrUpdate(dto)) {
			if (save) {
				return ResultUtil.ok("添加成功");
			} else {
				return ResultUtil.ok("修改成功");
			}
		} else {
			if (save) {
				return ResultUtil.error("添加失败");
			} else {
				return ResultUtil.error("修改失败");
			}
		}

	}

}
