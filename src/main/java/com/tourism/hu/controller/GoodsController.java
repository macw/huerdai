package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.GoodsCategoryConstant;
import com.tourism.hu.entity.Goods;
import com.tourism.hu.entity.GoodsCategory;
import com.tourism.hu.entity.Merchant;
import com.tourism.hu.entity.User;
import com.tourism.hu.mapper.GoodsCategoryMapper;
import com.tourism.hu.mapper.GoodsMapper;
import com.tourism.hu.mapper.MerchantMapper;
import com.tourism.hu.service.IGoodsService;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 商品信息表 前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@RestController
@RequestMapping("/goods")
public class GoodsController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private GoodsMapper mapper;

    @Resource
    private IGoodsService iGoodsService;

    @Resource
    private GoodsCategoryMapper goodsCategoryMapper;

    @Resource
    private MerchantMapper merchantMapper;


    @RequestMapping("/togoods")
    public ModelAndView hello() {

        ModelAndView mv = new ModelAndView("goods/goodsList");
        List<GoodsCategory> classIdLevel = goodsCategoryMapper.selectList(new QueryWrapper<GoodsCategory>().eq("class_id_level", GoodsCategoryConstant.ONE_LEVEL));
        mv.addObject("one", classIdLevel);
        return mv;
    }

    @RequestMapping("/togoodsEdit")
    public ModelAndView togoodsEdit() {
        ModelAndView mv = new ModelAndView("goods/goodsEdit");
        List<GoodsCategory> classIdLevel = goodsCategoryMapper.selectList(new QueryWrapper<GoodsCategory>().eq("class_id_level", GoodsCategoryConstant.ONE_LEVEL));
        mv.addObject("one", classIdLevel);
        List<Merchant> merchantList = merchantMapper.selectList(new QueryWrapper<Merchant>().eq("status", BaseConstant.USE_STATUS));
        mv.addObject("ml", merchantList);
        return mv;
    }

    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page, int limit, String goodsname, Integer id) {
        System.out.println("---" + goodsname + "---" + id);
        Page<Goods> page1 = new Page<>(page, limit);
        QueryWrapper<Goods> qw = new QueryWrapper<>();
        if (goodsname != null && goodsname != "") {
            qw.like("goodsname", goodsname);
        }
        if (id != null && id != 0) {
            qw.eq("goodsclassid", id);
        }
        IPage<Goods> IPage = mapper.selectPage(page1, qw);
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<Goods> List = IPage.getRecords();
        for (Goods goods : List) {
            GoodsCategory goodsCategory = goodsCategoryMapper.selectOne(new QueryWrapper<GoodsCategory>().eq("class_id", goods.getGoodsclassid()));
            if (goodsCategory != null) {
                goods.setGoodsClassName(goodsCategory.getClassName());
            }
            logger.debug("goods.getMerchantId()===" + goods.getMerchantId());
            if (goods.getMerchantId() != null) {
                Merchant merchant = merchantMapper.selectById(goods.getMerchantId());
                logger.debug("merchant===" + merchant);
                if (merchant != null) {
                    goods.setmName(merchant.getMerName());
                }
            }
            logger.debug(goods.toString());
        }

        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }

    @RequestMapping("/addOrUpdate")
    public MsgUtil addConfig(MultipartFile file, Goods goods,Integer classId) {
        User user = getUser();
        if (user==null){
            return MsgUtil.error("获取用户信息失败！");
        }
        MsgUtil  result  =  MsgUtil.flagSave(false);
        goods.setGoodsclassid(classId);
        logger.debug("goods================="+goods);
        if (goods.getGoodsId() == null) {
            //将用户的id，name，和当前时间 ，封装进goods对象
            goods.setCreateUserId(user.getUserId());
            goods.setCreateUser(user.getName());
            goods.setCreateTime(LocalDateTime.now());
            goods.setStatus(BaseConstant.USE_STATUS);
            //调用添加方法
            result= MsgUtil.flagSave( iGoodsService.save(goods));
        } else {
            //执行修改
            goods.setUpdateUserId(user.getUserId());
            goods.setUpdateUser(user.getName());
            goods.setUpdateTime(LocalDateTime.now());
            int i = mapper.updateById(goods);
            result= MsgUtil.flag(i);
        }
        if (file != null && file.getSize() > 0) {
            //获取上传文件的路径
            String basePath ="goods/titleImgs";
            String url="";
            try {
                url = aliOSSUpload(file, basePath,goods.getGoodsId());
            } catch (IOException e) {
                e.printStackTrace();
            }
            goods.setPictureUrl(url);
            int i = mapper.updateById(goods);
            result= MsgUtil.flag(i);
        }
        return result;
    }


    @RequestMapping("/selectOne")
    public ModelAndView selectOne(Integer id) {
        ModelAndView mv = new ModelAndView("goods/goodsEdit");
        Goods goods = mapper.selectOne(new QueryWrapper<Goods>().eq("goods_id", id));
        mv.addObject("go", goods);
        List<GoodsCategory> classIdLevel = goodsCategoryMapper.selectList(new QueryWrapper<GoodsCategory>().eq("class_id_level", GoodsCategoryConstant.ONE_LEVEL));
        mv.addObject("one", classIdLevel);
        if (goods.getGoodsclassid() != null) {
            List<GoodsCategory> goodsCategoryList = goodsCategoryMapper.selectList(new QueryWrapper<GoodsCategory>().eq("class_id_level", GoodsCategoryConstant.TWO_LEVEL));
            mv.addObject("two", goodsCategoryList);
            List<GoodsCategory> goodsCategoryList2 = goodsCategoryMapper.selectList(new QueryWrapper<GoodsCategory>().eq("class_id_level", GoodsCategoryConstant.THREE_LEVEL));
            mv.addObject("three", goodsCategoryList2);
        }
        List<Merchant> merchantList = merchantMapper.selectList(new QueryWrapper<Merchant>().eq("status", BaseConstant.USE_STATUS));
        mv.addObject("ml", merchantList);
        return mv;
    }

    @RequestMapping("/deleteOne")
    public MsgUtil deleteOne(Integer id) {
        int i = mapper.deleteById(id);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids) {
        List list = new ArrayList();
        for (Integer id : ids) {
            list.add(id);
        }
        int i = mapper.deleteBatchIds(list);
        return MsgUtil.flag(i);
    }

}
