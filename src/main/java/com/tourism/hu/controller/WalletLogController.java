package com.tourism.hu.controller;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.User;
import com.tourism.hu.entity.WalletLog;
import com.tourism.hu.entity.dto.WalletLogDto;
import com.tourism.hu.entity.dto.WalletLogDto;
import com.tourism.hu.mapper.WalletLogMapper;
import com.tourism.hu.service.IWalletLogService;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;
import com.tourism.hu.util.UUIDUtil;

/**
 * @ClassName: WalletLogController 
 * @Description: 钱包变动日志 前端控制器
 * @author 董兴隆
 * @date 2019年10月16日 下午1:04:37 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/walletlog")
public class WalletLogController extends BaseController{

	
	@Resource
    private WalletLogMapper walletLogMapper;
	
	@Autowired
	private IWalletLogService walletLogService;
	
	
	@RequestMapping("/page")
    public ModelAndView hello() {
        ModelAndView mv = new ModelAndView("/walletlog/walletlogList");
        return mv;
    }

	@RequestMapping("/editPage")
    public ModelAndView editPage(WalletLogDto dto) {
        ModelAndView mv = new ModelAndView("/walletlog/walletlogEdit");
        if(dto.getLogId()!=null && dto.getLogId()>0) {
        	WalletLog walletLog = walletLogMapper.selectById(dto.getLogId());
        	BeanUtils.copyProperties(walletLog, dto);
        }
        mv.addObject("dto", dto);
        return mv;
    }
	
	@RequestMapping("/selectData")
	@ResponseBody
	public ResultUtil<List<WalletLogDto>> selectData(WalletLogDto dto){
		PageUtil.initAttr(dto);
		List<WalletLogDto> list= new ArrayList<>();
		ResultUtil<List<WalletLogDto>> result = ResultUtil.error("查询失败",list);
		try {
			long count = walletLogMapper.getCount(dto);
			if(count >0) {
				list = walletLogMapper.selectPageDataList(dto);
			}
			result = ResultUtil.ok("查询成功",list);
			result.setCount(count);
			result.setData(list);
		}catch (Exception e) {
			result.setMsg(e.getMessage());
		}
		return result;
	}
	@RequestMapping("/deleteByLogId")
	@ResponseBody
	public ResultUtil<WalletLogDto> deleteByLogId(WalletLogDto dto){
		if(walletLogService.removeById(dto.getLogId())) {
			return ResultUtil.ok("删除成功");
		}else {
			return ResultUtil.error("删除失败");
		}
	}
	
	@RequestMapping("/saveOrUpdate")
	@ResponseBody
	public ResultUtil<WalletLogDto> saveOrUpdate(WalletLogDto dto){
		if(isNotLogIn()) {
			return ResultUtil.error("未找到当前登录用户");
		}
		User user = getUser();
		boolean save=true;
		if(dto.getLogId()!=null && dto.getLogId()>0) {
			save=false;
			dto.setLastUpdateDate(LocalDateTime.now());
			dto.setLastUpdatedId(user.getUserId());
			dto.setLastUpdateName(user.getName());
		}else {
			dto.setCreatedId(user.getUserId());
			dto.setCreatedName(user.getName());
			dto.setCreationDate(LocalDateTime.now());
		}
		if(walletLogService.saveOrUpdate(dto)) {
			if(save) {
				return ResultUtil.ok("添加成功");
			}else {
				return ResultUtil.ok("修改成功");
			}
		}else {
			if(save) {
				return ResultUtil.error("添加失败");
			}else {
				return ResultUtil.error("修改失败");
			}
		}
	}
	
	
	
}
