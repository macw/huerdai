package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.GoodsclassPic;
import com.tourism.hu.mapper.GoodsclassPicMapper;
import com.tourism.hu.util.DataUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 商品商品分类图片表 前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@RestController
@RequestMapping("/goodsclasspic")
public class GoodsclassPicController {

	@Resource
    private GoodsclassPicMapper goodsclassPicMapper;

    @RequestMapping("/selectAll")
    public DataUtil selectAll(){
        List<GoodsclassPic> goodsclassPicList = goodsclassPicMapper.selectList(null);
        DataUtil dataUtil = DataUtil.success();
        //将数据封装到dataUtil对象
        dataUtil.setData(goodsclassPicList);
        return dataUtil;
    }

   /* @RequestMapping("/selectAll")
    public DataUtil selectAll(int page,int limit){
        Page<GoodsclassPic> page1 = new Page<>(page,limit);
        IPage<GoodsclassPic> IPage = goodsclassPicMapper.selectPage(page1, null);
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<GoodsclassPic> List = IPage.getRecords();
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }*/


	
	
	
}
