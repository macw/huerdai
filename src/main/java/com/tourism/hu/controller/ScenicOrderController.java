package com.tourism.hu.controller;


import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.OrderCart;
import com.tourism.hu.entity.ScenicOrder;
import com.tourism.hu.entity.dto.ScenicOrderDto;
import com.tourism.hu.mapper.OrderCartMapper;
import com.tourism.hu.mapper.ScenicOrderMapper;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * @ClassName: ScenicOrderController 
 * @Description: (线路订单表)
 * @author 董兴隆
 * @date 2019年10月23日 下午1:14:22 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/scenic/order")
public class ScenicOrderController {

	@Resource
    private ScenicOrderMapper scenicOrderMapper;

	
	@RequestMapping("/page")
	public ModelAndView page() {
		ModelAndView mv = new ModelAndView("/scenic/order/scenicOrderList");
		return mv;
	}
    @RequestMapping("/selectAll")
    public ResultUtil<List<ScenicOrderDto>> selectAll(ScenicOrderDto dto){
        PageUtil.initAttr(dto);
        List<ScenicOrderDto> list= new ArrayList<>();
        ResultUtil<List<ScenicOrderDto>> result = ResultUtil.error("查询失败", list);
        long count = 0;
        try {
        	
        }catch (Exception e) {
		}
        
        return result;
    }
    
    
    @RequestMapping("/addscenicorder")
    public MsgUtil add(ScenicOrder cl){
        int i = scenicOrderMapper.insert(cl);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/selectOne")
    public ScenicOrder selectOne(Integer aid){
    	ScenicOrder cl = scenicOrderMapper.selectOne(new QueryWrapper<ScenicOrder>().eq("scenic_order_id", aid));
        return cl;
    }

    @RequestMapping("/update")
    public MsgUtil update(ScenicOrder cl){
        int i = scenicOrderMapper.updateById(cl);
        return MsgUtil.flag(i);
    }

    
    @RequestMapping("/delete")
    public MsgUtil deleteOne(Integer aid){
        int i = scenicOrderMapper.deleteById(aid);
        return MsgUtil.flag(i);
    
    }
	
	
	
	
}
