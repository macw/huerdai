package com.tourism.hu.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: StrategicCommentController 
 * @Description: 攻略评论表 前端控制器
 * @author 马超伟
 * @date 2019年10月13日 下午3:09:39 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/hu/strategic-comment")
public class StrategicCommentController {

}
