package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.entity.GoodsCategory;
import com.tourism.hu.entity.StrategicTravelsClass;
import com.tourism.hu.entity.User;
import com.tourism.hu.mapper.StrategicTravelsClassMapper;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @ClassName: StrategicTravelsClassController 
 * @Description: 攻略游记分类表 前端控制器
 * @author 马超伟
 * @date 2019年10月13日 下午2:09:06 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/strategicTravelsClass")
public class StrategicTravelsClassController extends BaseController {

    @Resource
    private StrategicTravelsClassMapper strategicTravelsClassMapper;

    @RequestMapping("/toStrategicTravelsClass")
    public ModelAndView hello() {
        ModelAndView mv = new ModelAndView("strategic/class/strategicTravelsClassList");
        return mv;
    }


    @RequestMapping("/selectTreeTable")
    public DataUtil selectTreeTable(){
        List<StrategicTravelsClass> strategicTravelsClassList = strategicTravelsClassMapper.selectList(null);
        DataUtil dataUtil = DataUtil.success();
        dataUtil.setData(strategicTravelsClassList);
        Integer count = strategicTravelsClassMapper.selectCount(null);
        long l = Long.parseLong(count.toString());
        dataUtil.setCount(l);
        return dataUtil;
    }

    @RequestMapping("/toStrategicTravelsClassEdit")
    public ModelAndView toStrategicTravelsClassEdit(Integer travelsClassId){
        ModelAndView mv = new ModelAndView("strategic/class/strategicTravelsClassEdit");
        StrategicTravelsClass travelsClass = strategicTravelsClassMapper.selectOne(new QueryWrapper<StrategicTravelsClass>().eq("travels_class_id", travelsClassId));
        mv.addObject("tc",travelsClass);
        return mv;
    }

    @RequestMapping("/selectTwoLevel")
    public DataUtil selectTwoLevel(Integer classId){
        List<StrategicTravelsClass> strategicTravelsClassList = strategicTravelsClassMapper.selectList(new QueryWrapper<StrategicTravelsClass>().eq("travels_class_parentid",0));
        DataUtil<StrategicTravelsClass> dataUtil = new DataUtil<>();
        dataUtil.setData(strategicTravelsClassList);
        return dataUtil;
    }


    @RequestMapping("/addOrUpdate")
    public MsgUtil addOrUpdate(StrategicTravelsClass strategicTravelsClass){
        User user = getUser();
        if (strategicTravelsClass.getTravelsClassId()==null){
            //添加
            strategicTravelsClass.setCreatedId(user.getUserId());
            strategicTravelsClass.setCreatedName(user.getName());
            strategicTravelsClass.setCreationDate(LocalDateTime.now());
            strategicTravelsClass.setStatus(BaseConstant.USE_STATUS);
            int i = strategicTravelsClassMapper.insert(strategicTravelsClass);
            return MsgUtil.flag(i);
        }else {
            //更新
            strategicTravelsClass.setLastUpdateDate(LocalDateTime.now());
            strategicTravelsClass.setLastUpdatedId(user.getUserId());
            strategicTravelsClass.setLastUpdateName(user.getName());
            int i = strategicTravelsClassMapper.updateById(strategicTravelsClass);
            return MsgUtil.flag(i);
        }
    }



/*






    @RequestMapping("/selectOne")
    public GoodsCategory selectOne(Integer aid){
        GoodsCategory cl = goodsCategoryMapper.selectOne(new QueryWrapper<GoodsCategory>().eq("class_id", aid));
        return cl;
    }


    @RequestMapping("/updateclassIdStatus")
    public MsgUtil updateclassIdStatus(GoodsCategory goodsCategory){
        int i = goodsCategoryMapper.updateById(goodsCategory);
        return MsgUtil.flag(i);
    }


    @RequestMapping("/deletecl")
    public MsgUtil deleteOne(Integer aid){
        int i = goodsCategoryMapper.deleteById(aid);
        return MsgUtil.flag(i);

    }
*/



}
