package com.tourism.hu.realm;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.UserConstant;
import com.tourism.hu.entity.User;
import com.tourism.hu.mapper.UserMapper;
import com.tourism.hu.service.IRoleService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.annotation.Resource;
import java.util.Set;

/**
 * 
 * @ClassName: AuthorRealm 
 * @Description: 用户授权
 * @author 董兴隆
 * @date 2019年10月16日 下午1:00:06 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
public class AuthorRealm extends AuthorizingRealm {

    @Resource
    private IRoleService iRoleService;

    @Resource
    private UserMapper userMapper;

    /**
     * 用户授权
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //根据认证信息获取用户名
        String username = principalCollection.getPrimaryPrincipal().toString();
        //查询数据库，根据用户名查询用户所对应的角色信息和权限信息
        Set<String> allRolesByUsername = iRoleService.getAllRolesByUsername(username);
        Set<String> allPermissionsByUsername = iRoleService.getAllPermissionsByUsername(username);
        //封装INFo，
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        simpleAuthorizationInfo.setRoles(allRolesByUsername);
        simpleAuthorizationInfo.setStringPermissions(allPermissionsByUsername);
        return simpleAuthorizationInfo;
    }


    /**
     * 用户验证
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //根据token获取用户名
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        String username = token.getUsername();
        //查询数据库，
        User user = userMapper.selectOne(new QueryWrapper<User>().eq("login_name", username).eq("user_status", BaseConstant.USE_STATUS));
        //封装info
        if (user!=null) {
            SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(user.getLoginName(), user.getPassword(), this.getName());
            return info;
        }else {
            return null;
        }
    }
}
