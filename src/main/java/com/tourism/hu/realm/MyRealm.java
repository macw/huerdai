package com.tourism.hu.realm;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.entity.User;
import com.tourism.hu.mapper.UserMapper;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

/**
 * @ClassName: MyRealm 
 * @Description: 用户授权
 * @author 董兴隆
 * @date 2019年10月18日 下午1:01:03 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
public class MyRealm extends AuthenticatingRealm {

    @Resource
    private UserMapper userMapper;

    /**
     * 记录日志
     */
    Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //1，获取令牌中的数据，账号
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        String username = token.getUsername();
        logger.info("username : "+username);
        //2，通过账号查询获取数据库中对应的账号信息
        User user = userMapper.selectOne(new QueryWrapper<User>().eq("login_name", username));
        logger.info("---"+user);
        Subject subject = SecurityUtils.getSubject();
        //将查询的对象放进去subject自带的session中去
        subject.getSession().setAttribute("user", user);
        //如果有数据，对象为非null，说明查询到了数据，封装account返回
        if (user != null){
            SimpleAccount simpleAccount = new SimpleAccount(user.getLoginName(),user.getPassword(),this.getName());
            return simpleAccount;
        }else {
            //如果对象为空，return null就会抛出账户不存在异常
            return null;
        }
    }
}
