package com.tourism.hu.util;

import java.security.MessageDigest;

import org.apache.commons.codec.binary.Base64;

/**
 * Base64(MD5(str)) Created by liuhaiyin on 2016/6/20.
 */
public class MD5Util {
	public static final String CHARSET_UTF8 = "UTF-8";
	public static final String CHARSET_GBK = "GBK";

	public final static byte[] md5(String s, String charset) {
		byte[] md = null;
		try {
			byte[] btInput = s.getBytes(charset);
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			mdInst.update(btInput);
			md = mdInst.digest();
		} catch (Exception e) {
			// e.printStackTrace();
			return null;
		}
		return md;
	}

	/**
	 * 生成签名
	 *
	 * @param params
	 * @return
	 */
	public static String sign(String params) {
		return md5AndBase64(params);
	}

	public static String base64Encode(byte[] b) {
		return Base64.encodeBase64String(b);
	}

	public static byte[] base64Decode(String b) {
		try {
			return Base64.decodeBase64(b);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Base64(MD5(str))
	 *
	 * @param s
	 * @return
	 */
	public static String md5AndBase64(String s) {
		return md5AndBase64(s, CHARSET_UTF8);
	}

	/**
	 * Base64(MD5(str))
	 *
	 * @param s
	 * @param charset 字符串编码集，默认UTF-8
	 * @return
	 */
	public static String md5AndBase64(String s, String charset) {
		return base64Encode(md5(s, charset));
	}
}
