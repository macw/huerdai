package com.tourism.hu.util;

public class IntegerUtil {

	/**
	 * 
	 * @Title: isNotNullAndZero 
	 * @Description: (判断这个Integer 不为空 也不为0) 
	 * @param id
	 * @return  
	 * @date 2019年12月7日 下午2:03:48
	 * @author 董兴隆
	 */
	public static boolean isNotNullAndZero(Integer id) {
		if(id!=null && id !=0) {
			return true;
		}else {
			return false;
		}
	}
}
