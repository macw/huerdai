package com.tourism.hu.util;

import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @ClassName: DataUtil 
 * @Description: 
 * @author 马超伟
 * @date 2019年10月13日  13:50:21 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
public class DataUtil<T> {


    private int code;
    private String msg;
    private Long count;
    private List<T> data;
    private Map<String, Object> map = new HashMap<String, Object>();



    public static DataUtil success(){
        DataUtil msg = new DataUtil();
        msg.setCode(0);
        msg.setMsg("");
        msg.setCount(1000L);
        return msg;
    }

    public static DataUtil error(){
        DataUtil msg = new DataUtil();
        msg.setCode(400);
        msg.setMsg("失败");
        return msg;
    }

    public DataUtil add(String key, Object value) {
        map.put(key, value);
        return this;
    }
}
