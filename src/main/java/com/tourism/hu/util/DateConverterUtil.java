package com.tourism.hu.util;

import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;

/**
 * 自定义日期格式化转换器
 */
public class DateConverterUtil implements Converter<String, LocalDateTime> {

	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	
	public static String dateTime = "yyyy-MM-dd HH:mm:ss";

	public static String date = "yyyy-MM-dd";

	public static String month = "yyyy-MM";

	
	
	public static SimpleDateFormat getSimpleDateFormat(String format){
		SimpleDateFormat sdf=new SimpleDateFormat(format);
		return sdf;
	}
	
	@Override
	public LocalDateTime convert(String s) {
		if ("".equals(s) || s == null) {
			return null;
		}
		try {
			Date date = simpleDateFormat.parse(s);
			Instant instant = date.toInstant();// An instantaneous point on the time-line.(时间线上的一个瞬时点。)
			ZoneId zoneId = ZoneId.systemDefault();// A time-zone ID, such as {@code Europe/Paris}.(时区)
			LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zoneId);
			return localDateTime;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String dateFormat(LocalDateTime dateTime, String format) {
		if (dateTime == null) {
			return null;
		}
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		return dateTime.format(formatter);
	}

	private static int secureGet(TemporalAccessor temporalAccessor, ChronoField chronoField) {
		if (temporalAccessor.isSupported(chronoField)) {
			return temporalAccessor.get(chronoField);
		}
		return 0;
	}

	/**
	 * @param date
	 * @return
	 * @Title: setDayZero
	 * @Description: (获得一天的开始日期)
	 * @date 2019年10月15日 下午3:42:27
	 * @author 董兴隆
	 */
	public static LocalDateTime setDayZero(String date) {
		TemporalAccessor temporalAccessor = DateTimeFormatter.ISO_DATE.parse(date);
		LocalDateTime localDateTime = LocalDateTime.of(secureGet(temporalAccessor, ChronoField.YEAR),
				secureGet(temporalAccessor, ChronoField.MONTH_OF_YEAR),
				secureGet(temporalAccessor, ChronoField.DAY_OF_MONTH),
				secureGet(temporalAccessor, ChronoField.HOUR_OF_DAY),
				secureGet(temporalAccessor, ChronoField.MINUTE_OF_HOUR),
				secureGet(temporalAccessor, ChronoField.SECOND_OF_MINUTE),
				secureGet(temporalAccessor, ChronoField.NANO_OF_SECOND));
		return localDateTime;
	}





	/**
	 * @param date
	 * @return
	 * @Title: setDayLast
	 * @Description: (获得一天的结束日期)
	 * @date 2019年10月15日 下午3:42:52
	 * @author 董兴隆
	 */
	public static LocalDateTime setDayLast(String date) {
		TemporalAccessor temporalAccessor = DateTimeFormatter.ISO_DATE.parse(date);
		LocalDateTime localDateTime = LocalDateTime.of(secureGet(temporalAccessor, ChronoField.YEAR),
				secureGet(temporalAccessor, ChronoField.MONTH_OF_YEAR),
				secureGet(temporalAccessor, ChronoField.DAY_OF_MONTH), 23, 59, 59, 0);
		return localDateTime;
	}

	/**
	 * 
	 * @Title: getDay 
	 * @Description: (获得当前日期的前后 多少天  -1 位前一天) 
	 * @param day
	 * @return  
	 * @date 2019年11月6日 下午4:18:25
	 * @author 董兴隆
	 */
	public static  String getDay(int day) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, day);
		return frormat(calendar.getTime(), DateConverterUtil.date);
	}

    /**
     * 获得当期日期之后的多少天
     * @param day
     * @return
     */
	public static LocalDateTime getTime(int day) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, day);
		String frormat = frormat(calendar.getTime(), DateConverterUtil.date);
		LocalDate localda = LocalDate.parse(frormat);
		LocalTime localTime = LocalTime.now();
		LocalDateTime localDateTime = LocalDateTime.of(localda, localTime);
		return localDateTime;
	}

    /**
     * 获得指定日期，之后的多少天
     * @param day
     * @return
     */
    public static LocalDateTime getTime(LocalDateTime localTime,int day) {
		LocalDate localDate = localTime.toLocalDate();
		long day1 = localDate.toEpochDay();
		LocalDateTime now = LocalDateTime.now();
		long l = now.toLocalDate().toEpochDay();

		int i = (int) day1 - (int) l;

		System.out.println("day1==="+i);
		Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, day+i);
        String frormat = frormat(calendar.getTime(), DateConverterUtil.date);
        LocalDate localda = LocalDate.parse(frormat);
        LocalTime localTimeNow = LocalTime.now();
        LocalDateTime localDateTimes = LocalDateTime.of(localda, localTimeNow);
        return localDateTimes;
    }



	public static String frormat(Date date, String format) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		String format1 = simpleDateFormat.format(date);
		return format1;
	}

	/**
	 * 获得当前日期的 月份的第一天的零时零刻
	 * @param date
	 * @return
	 */
	public static LocalDateTime getStartMonthDay(LocalDateTime date){
		LocalDateTime firstday = date.with(TemporalAdjusters.firstDayOfMonth());
		String format = DateConverterUtil.dateFormat(firstday, DateConverterUtil.date);
		LocalDateTime localDateTime = setDayZero(format);
		return localDateTime;
	}

	/**
	 * 获取某个月的 月、份开始时间的第一天的零时零刻
	 * @param number
	 * number为正数： 上number个月的第一天
	 * number为0，当前月的第一天
	 * number为负数：下number月的第一天
	 * @return
	 */
	public static LocalDateTime getNumberStartMonthDay(Integer number){
		LocalDateTime localDateTime1 = LocalDateTime.now().minusMonths(number);
		LocalDateTime startMonthDay = getStartMonthDay(localDateTime1);
		return startMonthDay;
	}


	public static void main(String[] args) throws InterruptedException {

		int day = LocalDateTime.now().getDayOfMonth();
		System.out.println(day);

		/*DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDateTime date = LocalDateTime.now();
		LocalDateTime firstday = date.with(TemporalAdjusters.firstDayOfMonth());

		LocalDateTime lastDay = date.with(TemporalAdjusters.lastDayOfMonth());
		String format = firstday.format(fmt);
		LocalDateTime localDateTime = setDayZero(format);
		System.out.println("firstday:" + firstday.format(fmt)+"----"+localDateTime);
		System.out.println("lastDay:" + lastDay.format(fmt)+"----"+setDayLast(lastDay.format(fmt)));*/
	}


	/**
	 * 获取当前时间，x分钟之后的时间
	 * @return
	 */
	public static LocalDateTime getAfterFiveMinutes(int minuts){
		DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, minuts);
		String date = DateConverterUtil.frormat(calendar.getTime(), DateConverterUtil.dateTime);
		return LocalDateTime.parse(date,df);
	}



}
