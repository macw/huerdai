package com.tourism.hu.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: MsgUtil 
 * @Description: 短信
 * @author 马超伟
 * @date 2019年11月18日  15:52:38 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
public class MsgUtil<T> {


    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public MsgUtil() {
    }

    public MsgUtil(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private Map<String, Object> map = new HashMap<String, Object>();


    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

    @Override
    public String toString() {
        return "MsgUtil{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", map=" + map +
                '}';
    }

    public static MsgUtil flag(int i){
        MsgUtil msg = null;
        if (i>=1){
            msg = MsgUtil.success();
        }else {
            msg = MsgUtil.error();
        }
        return msg;
    }

    public static MsgUtil flagSave(Boolean b){
        MsgUtil msg = null;
        if (b){
            msg = MsgUtil.success();
        }else {
            msg = MsgUtil.error();
        }
        return msg;
    }

    public static MsgUtil success(){
        MsgUtil msg = new MsgUtil();
        msg.setCode(200);
        msg.setMsg("成功");
        return msg;
    }

    public static MsgUtil error(){
        MsgUtil msg = new MsgUtil();
        msg.setCode(400);
        msg.setMsg("失败");
        return msg;
    }

    public static MsgUtil error(String msg){
        MsgUtil msgUtil = new MsgUtil();
        msgUtil.setCode(400);
        msgUtil.setMsg(msg);
        return msgUtil;
    }


    public MsgUtil add(String key, Object value) {
        map.put(key, value);
        return this;
    }
}
