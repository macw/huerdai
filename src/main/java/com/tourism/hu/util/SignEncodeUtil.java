package com.tourism.hu.util;

import java.security.MessageDigest;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;

/**
 * 
 * @ClassName: SignEncodeUtil 
 * @Description: (生成签名帮助类) 
 * @author 董兴隆
 * @date 2019年11月28日 下午4:53:07 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
public class SignEncodeUtil {

	public static final String CHARSET_UTF8 = "UTF-8";
	public static final String CHARSET_GBK = "GBK";

	/**
	 * 
	 * @Title: getSignByMapAddSeparatorASC
	 * @Description: (将map组装成一个String字符串 每个map对象)
	 * @param data  
	 * @return
	 * @date 2019年11月28日 下午4:41:00
	 * @author 董兴隆
	 */
	public static String getSignByMapASC(Map<String, String> data) {
		StringBuffer sb = new StringBuffer("");
		for (String key : data.keySet()) {
			sb.append(key + "=" + data.get(key));
		}
		return sb.toString();
	}

	public final static byte[] md5(String s, String charset) {
		byte[] md = null;
		try {
			byte[] btInput = s.getBytes(charset);
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			mdInst.update(btInput);
			md = mdInst.digest();
		} catch (Exception e) {
			return null;
		}
		return md;
	}

	/**
	 * 生成签名
	 *
	 * @param params
	 * @return
	 */
	public static String sign(String params) {
		return md5AndBase64(params);
	}

	public static String base64Encode(byte[] b) {
		return Base64.encodeBase64String(b);
	}

	public static byte[] base64Decode(String b) {
		try {
			return Base64.decodeBase64(b);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Base64(MD5(str))
	 *
	 * @param s
	 * @return
	 */
	public static String md5AndBase64(String s) {
		return md5AndBase64(s, CHARSET_UTF8);
	}

	/**
	 * Base64(MD5(str))
	 *
	 * @param s
	 * @param charset 字符串编码集，默认UTF-8
	 * @return
	 */
	public static String md5AndBase64(String s, String charset) {
		return base64Encode(md5(s, charset));
	}
}
