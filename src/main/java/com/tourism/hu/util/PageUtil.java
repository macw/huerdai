package com.tourism.hu.util;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;

import lombok.Data;
/**
 * @ClassName: PageUtil 
 * @Description: 数据分页
 * @author 马超伟
 * @date 2019年11月8日  15:55:04 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
public class PageUtil implements Serializable{

	private static final long serialVersionUID = 1L;

	@TableField(exist = false)
	private Integer page;
	
	@TableField(exist = false)
	private Integer start;
	
	@TableField(exist = false)
	private Integer limit;

	
	public static PageUtil initAttr(PageUtil page) {
		if(page==null) {
			page = new PageUtil();
		}
		if(page.getPage()==null || page.getPage()<1) {
			page.setPage(1);
		}
		if(page.getLimit()==null) {
			page.setLimit(10);
		}
		page.setStart((page.getPage()-1)*page.getLimit());
		return page;
	}
	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	
	
	
}
