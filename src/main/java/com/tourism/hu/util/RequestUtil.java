package com.tourism.hu.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;

public class RequestUtil {

	/**
	 * 
	 * @Title: getPostStr 
	 * @Description: (将Http中的request 请求转换成String) 
	 * @param request
	 * @return  
	 * @date 2019年11月21日 下午4:31:03
	 * @author 董兴隆
	 */
	public static String getPostStr(HttpServletRequest request) {
		StringBuffer sb = new StringBuffer();
		try {
			InputStream is = request.getInputStream();
			InputStreamReader isr = new InputStreamReader(is, "UTF-8");
			BufferedReader br = new BufferedReader(isr);
			String s = "";

			while ((s = br.readLine()) != null) {
				sb.append(s);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString(); // 次即为接收到微信端发送过来的xml数据
	}

}
