package com.tourism.hu.util;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.TreeMap;

import com.alibaba.fastjson.JSONObject;

/**
 * 
 * @ClassName: Shang365 
 * @Description: (商旅365的方法) 
 * @author 董兴隆
 * @date 2019年11月29日 下午1:49:24 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
public class Shang365 {
	
	
	public static Map<String,String> getShang365Map(){
		String timestamp = DateConverterUtil.dateFormat(LocalDateTime.now(), "yyyy/MM/dd HH:mm:ss");
		Map<String,String> map = new TreeMap<String, String>();
		map.put("apiKey", GlobalConfigUtil.getKey("365apikey"));
		map.put("Timestamp", timestamp);
		map.put("Sign", getSgin(timestamp));
		return map;
	}
	
	public static JSONObject getShang365Json(){
		String timestamp = DateConverterUtil.dateFormat(LocalDateTime.now(), "yyyy/MM/dd HH:mm:ss");
		JSONObject obj = new JSONObject();
		obj.put("apiKey", GlobalConfigUtil.getKey("365apikey"));
		obj.put("Timestamp", timestamp);
		obj.put("Sign", getSgin(timestamp));
		return obj;
	}
	
	private static String getSgin(String timestamp){
		Map<String,String> data = new TreeMap<String, String>();
		data.put("apiKey", GlobalConfigUtil.getKey("365apikey"));
		data.put("secretkey", GlobalConfigUtil.getKey("365secretkey"));
		data.put("timestamp", timestamp);
		String sgin= SignEncodeUtil.getSignByMapASC(data);
		return SignEncodeUtil.md5AndBase64(sgin);
	}
	
	public static void main(String[] args) {
		System.out.println(getShang365Map());
	}

}
