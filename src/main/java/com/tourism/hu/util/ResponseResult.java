package com.tourism.hu.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

public class ResponseResult<T> {

    //返回状态码
    private int code;

    //提示信息
    private String msg;

    //数据类型
    private T data;



    //0表示正常
    public static final int SUCCESS_CODE = 0;

    public static final int ERROR_CODE = 1;
    //错误信息提示
    public static final int ERROR_MSGCODE = 400;

    //状态码
    public static final int STATE_CODE = 2;

    
  //状态码
    public static final String SUCCESS_MSG = "成功";
    
    public static final String ERROR_MSG = "失败";
    //成功
    public static final ResponseResult SUCCESS = new ResponseResult(SUCCESS_CODE, "SUCCESS");
    //失败
    public static final ResponseResult ERROR = new ResponseResult(ERROR_CODE, "ERROR");

    public static final ResponseResult ERROR(String msg){
        return new ResponseResult(ERROR_CODE, msg);};

    public static final ResponseResult SUCCESS(String msg){
        return new ResponseResult(SUCCESS_CODE, msg);};

    /**
     * 返回状态码
     */
    public static ResponseResult STATUS(String msg) {
        return new ResponseResult(STATE_CODE, msg);
    }

    /**
     * 返回，插入/更新状态
     */
    public static ResponseResult flag(int i) {
        if (i >= 1) {
            return SUCCESS;
        } else {
            return ERROR;
        }
    }


    /**
     * 静态Request请求方法
     * @return
     */
    public static HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
    }

    /**
     * 静态Response返回响应方法
     * @return
     */
    public static HttpServletResponse getResponse() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
    }

    /**
     * jsonp对象 封装返回响应方法，返回类型json
     * @param responseResult
     */
    public static void Step(ResponseResult responseResult) {
        try {
            getResponse().setContentType("text/plain");
            getResponse().setHeader("Pragma", "No-cache");
            getResponse().setCharacterEncoding("utf-8");
            getResponse().setHeader("Cache-Control", "no-cache");
            getResponse().setDateHeader("Expires", 0);
            PrintWriter out = getResponse().getWriter();
            JSONObject resultJSON = JSONObject.parseObject(responseResult.toString()); //根据需要拼装json
            
            String jsonpCallback = getRequest().getParameter("jsonpCallback");//客户端请求参数
            out.println(jsonpCallback + "(" + resultJSON.toJSONString(responseResult,SerializerFeature.PrettyFormat,
                    SerializerFeature.WriteMapNullValue, SerializerFeature.WriteNullStringAsEmpty,
                    SerializerFeature.DisableCircularReferenceDetect,
                    SerializerFeature.WriteNullListAsEmpty) + ")");//返回jsonp格式数据
            //out.println(resultJSON.toString(1, 1));//返回jsonp格式数据
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * jsonp对象 封装返回响应方法，返回类型Map
     * @param map
     */
    public static void StepMap(Map<String,Object> map) {
        try {
            getResponse().setContentType("text/plain");
            getResponse().setHeader("Pragma", "No-cache");
            getResponse().setCharacterEncoding("utf-8");
            getResponse().setHeader("Cache-Control", "no-cache");
            getResponse().setDateHeader("Expires", 0);
            PrintWriter out = getResponse().getWriter();
            JSONObject resultJSON = JSONObject.parseObject(map.toString()); //根据需要拼装json
            String jsonpCallback = getRequest().getParameter("jsonpCallback");//客户端请求参数
            out.println(jsonpCallback + "(" + resultJSON.toString(SerializerFeature.PrettyFormat,
                    SerializerFeature.WriteMapNullValue, SerializerFeature.WriteNullStringAsEmpty,
                    SerializerFeature.DisableCircularReferenceDetect,
                    SerializerFeature.WriteNullListAsEmpty) + ")");//返回jsonp格式数据
			/*
			 * JSONObject resultJSON = JSONObject.fromObject(map); //根据需要拼装json String
			 * jsonpCallback = getRequest().getParameter("jsonpCallback");//客户端请求参数
			 * out.println(jsonpCallback + "(" + resultJSON.toString(1, 1) +
			 * ")");//返回jsonp格式数据
			 */            
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 无参构造
     */
    public ResponseResult() {

    }


    /**
     * 有参构造
     * @param code
     * @param msg
     */
    public ResponseResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public ResponseResult(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static int getSuccessCode() {
        return SUCCESS_CODE;
    }

    public static int getErrorCode() {
        return ERROR_CODE;
    }

    public static int getErrorMsgcode() {
        return ERROR_MSGCODE;
    }

    public static int getStateCode() {
        return STATE_CODE;
    }

    public static ResponseResult getSUCCESS() {
        return SUCCESS;
    }

    public static ResponseResult getERROR() {
        return ERROR;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setErrorMsg(String msg) {
        this.msg = msg;
        this.code = ERROR_MSGCODE;
    }
    
    public void setErrorMsg(String msg,Integer code) {
    	this.msg = msg;
    	this.code = code;
    }
    
    public void setOkMsg(String msg) {
        this.msg = msg;
        this.code = 0;
    }
    public void setOkMsg(T data) {
        this.msg = getSuccessMsg();
        this.code = 0;
        this.data=data;
    }

    public static String getSuccessMsg() {
        return SUCCESS_MSG;
    }

    public static String getErrorMsg() {
        return ERROR_MSG;
    }

    public String toString() {
        return JSON.toJSONString(this);
    }
}
