package com.tourism.hu.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.aliyun.oss.OSS;
import com.tourism.hu.oss.OSSClientConstants;

/**
 * 
 * @ClassName: FileUploadUtil 
 * @Description: 文件上传工具类 
 * @author 马超伟
 * @date 2019年11月13日 上午11:03:51 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
public class FileUploadUtil {
	
	public static String upload(MultipartFile file) {
		try {
			String extName = file.getOriginalFilename();
			// 获取文件后缀
			if (extName.lastIndexOf(".") <= 0) {
				throw new RuntimeException("不支持该文件类型");
			}
			extName = extName.substring(extName.lastIndexOf("."));
			String webUrl = getWebUrl();
			String fileName = getFileName();
			String sysPath = System.getProperty("catalina.home") + "/webapps";
			// 获取文件名字
			fileName = getFileName() + extName;
			// 获取文件地址
			String filePath = "/content/" + fileName;
			String Url = sysPath +"/content/";
			File file2 = new File(Url);
			if (!file2.exists()) {
				file2.mkdirs();
			}
			file.transferTo(new File(sysPath + filePath));
			return webUrl + filePath;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	/**
	 * 
	 * @Title: upload 
	 * @Description: (将文件保存到指定的路径下) 
	 * @param file

	 * @return  
	 * @date 2019年9月30日 上午10:22:31
	 * @author 董兴隆
	 */
	public static String upload(MultipartFile file,String specifiedPath) {
		try {
			String extName = file.getOriginalFilename();
			// 获取文件后缀
			if (extName.lastIndexOf(".") <= 0) {
				throw new RuntimeException("不支持该文件类型");
			}
			extName = extName.substring(extName.lastIndexOf("."));
			String fileName = getFileName();
			// 获取文件名字
			fileName = getFileName() + extName;
			File file2 = new File(specifiedPath);
			if (!file2.exists()) {
				file2.mkdirs();
			}
			file.transferTo(new File(specifiedPath + File.separator+ fileName));
			return fileName;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 上传方法， 用这个
	 * @param file
	 * @param specifiedPath
	 * @return
	 * @throws IOException
	 */
	public static String baseUpload(MultipartFile file,String specifiedPath,Integer userId) throws IOException {
			String extName = file.getOriginalFilename();
			// 获取文件后缀
			if (extName.lastIndexOf(".") <= 0) {
				throw new RuntimeException("不支持该文件类型");
			}
			extName = extName.substring(extName.lastIndexOf("."));
			String fileName = getFileName();
			// 获取文件名字
			fileName = getFileName() + extName;

			/*File file2 = new File(createFileBySysTime(specifiedPath));
			if (!file2.exists()) {
				file2.mkdirs();
			}*/
			file.transferTo(new File(createFileBySysTime(specifiedPath) + File.separator+ fileName));
			System.out.println("存入路径=======================》"+createFileByCusId(specifiedPath,userId) + File.separator+ fileName);
			return "/"+getFileByCusId(userId)+"/"+fileName;
	}

	public static String baseUpload(MultipartFile file,String specifiedPath) throws IOException {
		String extName = file.getOriginalFilename();
		// 获取文件后缀
		if (extName.lastIndexOf(".") <= 0) {
			throw new RuntimeException("不支持该文件类型");
		}
		extName = extName.substring(extName.lastIndexOf("."));
		String fileName = getFileName();
		// 获取文件名字
		fileName = getFileName() + extName;

		File filedir = new File(specifiedPath + File.separator);
        if (!filedir.exists()) {
        	filedir.mkdirs();
        }
		file.transferTo(new File(specifiedPath + File.separator+ fileName));
		System.out.println("存入路径=======================》"+specifiedPath + File.separator+ fileName);
		return "/"+fileName;
	}

	/**
	 * 
	 * @Title: getFileName 
	 * @Description: (获得文件名称) 
	 * @param file
	 * @return  
	 * @date 2019年12月6日 下午5:48:20
	 * @author 董兴隆
	 */
	public static String getFileName(MultipartFile file) {
		String extName = file.getOriginalFilename();
		// 获取文件后缀
		if (extName.lastIndexOf(".") <= 0) {
			throw new RuntimeException("不支持该文件类型");
		}
		extName = extName.substring(extName.lastIndexOf("."));
		// 获取文件名字
		return  getFileName() + extName;
	}
	
	public static String getFileName(File file) {
		String extName = file.getName();
		// 获取文件后缀
		if (extName.lastIndexOf(".") <= 0) {
			throw new RuntimeException("不支持该文件类型");
		}
		extName = extName.substring(extName.lastIndexOf("."));
		// 获取文件名字
		return  getFileName() + extName;
	}
	/**
	 * 
	 * @Title: getFileName 
	 * @Description: (获得文件名称) 
	 * @param file
	 * @return  
	 * @date 2019年12月6日 下午5:48:20
	 * @author 董兴隆
	 */
	public static String getFileExtName(MultipartFile file) {
		String extName = file.getOriginalFilename();
		// 获取文件后缀
		if (extName.lastIndexOf(".") <= 0) {
			throw new RuntimeException("不支持该文件类型");
		}
		extName = extName.substring(extName.lastIndexOf("."));
		return extName;
	}
	
	public static File MultipartFileConverFile(MultipartFile file) throws IOException {
		 String fileName = file.getOriginalFilename();
		 String prefix=fileName.substring(fileName.lastIndexOf("."));
	     // 用uuid作为文件名，防止生成的临时文件重复
         File excelFile = File.createTempFile(UUIDUtil.getUUID(), prefix);
	     // MultipartFile to File
         file.transferTo(excelFile);
         return excelFile;

	}
	
	public static void main(String[] args) throws FileNotFoundException {
		OSSClientConstants  oss = new OSSClientConstants();
		OSS ossClient = oss.getOssClient();
		long startTime=System.currentTimeMillis();
		OSSClientConstants.uploadByInputStream(ossClient, new FileInputStream("E:\\2019-12-22.mp4"), OSSClientConstants.bucketName, "2019-12-11.mp4");
		 long endTime=System.currentTimeMillis();
		 System.out.println("程序运行时间： "+(endTime - startTime)+"ms");
	}
	/**
	 * 获取文件名
	 * @return
	 */
	public static String getFileName() {
		String uuid = UUID.randomUUID().toString();
		uuid = uuid.replace("-", "");
		return uuid.toLowerCase();
	}
	
	public static String getWebUrl() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		return  request.getServletContext().getRealPath("/img");
	}
	
	public static String getWebProUrl() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() +request.getContextPath();
	}

	/**
	 * 根据用户id 自动创建文件夹
	 * @param path
	 * @return
	 */
	public static String createFileByCusId(String path,Integer userId) {
		String fileName = getFileByCusId(userId);
		// 3. 创建文件夹
		String newPath = path +"\\"+ fileName;
		File file = new File(newPath);
		//如果文件目录不存在则创建目录
		if (!file.exists()) {
			if (!file.mkdir()) {
				System.out.println("当前路径不存在，创建失败");
				return null;
			}
		}
		System.out.println("创建成功" + newPath);
		return newPath;
	}

	/**
	 * 根据当前用户id 创建文件名
	 * @param userId
	 * @return
	 */
	public static String getFileByCusId(Integer userId) {
		userId = userId % 1000;
		String fileName = ""+userId; //获取系统当前用户id并将其转换为string类型,fileName即文件名
		System.out.println("fileName=====================>"+fileName);
		// 3. 创建文件夹
		return fileName;
	}

	/**
	 * 
	 * @Title: getFiledir 
	 * @Description: (通过Id获得文件路径 如果没有则创建文件夹) 
	 * @param path
	 * @param id
	 * @return  
	 * @date 2019年12月9日 上午10:27:51
	 * @author 董兴隆
	 */
	public static String getFiledir(String path,Integer id) {
		int temp = 1000;
		id= id%temp;
		return path+"/"+id+"/";
		
	}
	public static String getFiledir(String path) {
		return path+"/";
		
	}
 

	/**
	 * 根据系统时间 自动创建文件夹  暂时不用
	 * @param path
	 * @return
	 */
	public static String createFileBySysTime(String path) {
		String fileName = getFileBaneBySysTime();
		// 3. 创建文件夹
		String newPath = path +"\\"+ fileName;
		File file = new File(newPath);
		//如果文件目录不存在则创建目录
		if (!file.exists()) {
			if (!file.mkdir()) {
				System.out.println("当前路径不存在，创建失败");
				return null;
			}
		}
		System.out.println("创建成功" + newPath);
		return newPath;
	}

    /**
     * 获取当前日期名  暂时没用
     * @return
     */
    public static String getFileBaneBySysTime() {

        // 1. 读取系统时间
        Calendar calendar = Calendar.getInstance();
        Date time = calendar.getTime();

        // 2. 格式化系统时间
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String fileName = format.format(time); //获取系统当前时间并将其转换为string类型,fileName即文件名
        return fileName;
    }


}
