package com.tourism.hu.util;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;

public class HttpClientsUtil {

	
	
	 private static  final Gson gson = new Gson();
	 private static  CloseableHttpClient httpClient =  null;
	 private static  int timeout =  5000;
    
	
 

    /**
     * get方法
     * @param url
     * @return
     */
    public static Map<String,Object> doGetToMap(String url){
    	Map<String,Object> map = new HashMap<String, Object>();
    	String jsonResult = doGetToString(url,timeout);
        map = gson.fromJson(jsonResult,map.getClass());
        return map;
    }
    
    public static String doGetToString(String url,int timeout){
    	httpClient =  HttpClients.createDefault();
    	String result="";
    	RequestConfig requestConfig =  RequestConfig.custom().setConnectTimeout(timeout) //连接超时
    			.setConnectionRequestTimeout(timeout)//请求超时
    			.setSocketTimeout(timeout)
    			.setRedirectsEnabled(true)  //允许自动重定向
    			.build();
    	
    	HttpGet httpGet = new HttpGet(url);
    	httpGet.setConfig(requestConfig);
    	
    	try{
    		HttpResponse httpResponse = httpClient.execute(httpGet);
    		if(httpResponse.getStatusLine().getStatusCode() == 200){
    			result =  EntityUtils.toString(httpResponse.getEntity());
    			return result;
    		}
    	}catch (Exception e){
    		e.printStackTrace();
    	}finally {
    		try {
    			httpClient.close();
    		}catch (Exception e){
    			e.printStackTrace();
    		}
    	}
    	return result;
    }
	/**
     * 	封装post
     * @return
     */
    public static String doPost(String url, String data,int timeout){
        httpClient =  HttpClients.createDefault();
        //超时设置

        RequestConfig requestConfig =  RequestConfig.custom().setConnectTimeout(timeout) //连接超时
                .setConnectionRequestTimeout(timeout)//请求超时
                .setSocketTimeout(timeout)
                .setRedirectsEnabled(true)  //允许自动重定向
                .build();


        HttpPost httpPost  = new HttpPost(url);
        httpPost.setConfig(requestConfig);
        httpPost.addHeader("Content-Type","text/html; chartset=UTF-8");

        if(data != null && data instanceof  String){ //使用字符串传参
            StringEntity stringEntity = new StringEntity(data,"UTF-8");
            httpPost.setEntity(stringEntity);
        }
        try{
            CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            if(httpResponse.getStatusLine().getStatusCode() == 200){
                String result = EntityUtils.toString(httpEntity,"UTF-8");
                return result;
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                httpClient.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return null;
    }
    
    
    public static String postJson(String url,String jsonStr) {
    	String ticket="";
    	httpClient = HttpClients.createDefault();
		ResponseHandler<String> responseHandler = new BasicResponseHandler();
    	HttpPost method  = new HttpPost(url);
    	method.addHeader("Content-type","application/json; charset=utf-8");
		method.setHeader("Accept", "application/json");
		method.setEntity(new StringEntity(jsonStr, Charset.forName("UTF-8")));
		try {
			ticket = httpClient.execute(method,responseHandler);
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}finally {
			try {
				httpClient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return ticket;
    }
}
