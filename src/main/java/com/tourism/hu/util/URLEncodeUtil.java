package com.tourism.hu.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 10:06
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
public class URLEncodeUtil {

    /**
     * urlEncode 转码
     * @param url
     * @return
     */
    public static String urlEncode(String url) {
        if(url == null) {
            return null;
        }

        final String reserved_char = ";/?:@=&";
        String ret = "";
        for(int i=0; i < url.length(); i++) {
            String cs = String.valueOf( url.charAt(i) );
            if(reserved_char.contains(cs)){
                ret += cs;
            }else{
                try {
                    ret += URLEncoder.encode(cs, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
        return ret.replace("+", "%20");
    }
}
