package com.tourism.hu.util;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;
@Data
public class ResultUtil<T> {
	
	private Integer code;
	
	private String msg; 
	/** se  成功或者失败*/
	private boolean se;
	
	private T data;
	
	private Long count=0l;
	
	private static final  int errorCode=400;
	
	private static final  int successCode=0;
	
	private static final String successMsg="操作成功";
	
	private static final String errorMsg="操作失败";
	
	private Map<String, Object> map = new HashMap<String, Object>();
	
	
	public static <T>ResultUtil<T> success() {
		return ResultUtil.ok(successMsg);
	}
	public static <T>ResultUtil<T>  success(T data) {
		return ResultUtil.ok(successMsg,data);
	}
	public static <T>ResultUtil<T>  success(String msg) {
		return ResultUtil.ok(msg);
	}
	public static <T>ResultUtil<T>  success(String msg,T data) {
		return ResultUtil.ok(msg,data);
	}
	
	
	public static <T>ResultUtil<T> error() {
		return ResultUtil.error(errorMsg);
	}
	public static <T>ResultUtil<T> error(T data) {
		return ResultUtil.error(errorMsg,data);
	}
	
	public static <T>ResultUtil<T> error(String msg, T data) {
		ResultUtil<T> result = new ResultUtil<T>();
		result.setCode(errorCode);
		result.setSe(false);
		result.setMsg(msg);
		result.setData(data);
		return result;
	}
	public static <T>ResultUtil<T> error(String msg) {
		ResultUtil<T> result = new ResultUtil();
		result.setCode(errorCode);
		result.setSe(false);
		result.setMsg(msg);
		return result;
	}
	public static <T>ResultUtil<T> ok(String msg) {
		ResultUtil result = new ResultUtil();
		result.setCode(successCode);
		result.setSe(true);
		result.setMsg(msg);
		return result;
	}
	
	public static <T>ResultUtil<T> ok(String msg,T data) {
		ResultUtil<T> result = new ResultUtil<T>();
		result.setCode(successCode);
		result.setSe(true);
		result.setMsg(msg);
		result.setData(data);
		return result;
	}



	
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public boolean isSe() {
		return se;
	}

	public void setSe(boolean se) {
		this.se = se;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Map<String, Object> getMap() {
		return map;
	}

	public void setMap(Map<String, Object> map) {
		this.map = map;
	}
	/**
	 * 
	 * @Title: flag 
	 * @Description: (判断受影响行数大于successCode返回成功 否则返回false) 
	 * @param number
	 * @return  
	 * @date 2successCode19年9月23日 下午12:successCode3:54
	 * @author 董兴隆
	 */
	public static ResultUtil influenceQuantity(Integer number) {
		if(number>0) {
			return ResultUtil.success();
		}else {
			return ResultUtil.error();
		}
	}
	/**
	 * 
	 * @Title: flag 
	 * @Description: (判断受影响行数大于successCode返回成功 否则返回false) 
	 * @param number
	 * @return  
	 * @date 2successCode19年9月23日 下午12:successCode3:54
	 * @author 董兴隆
	 */
	public static ResultUtil influenceQuantity(Integer number,String msg) {
		if(number>0) {
			return ResultUtil.success(msg+successMsg);
		}else {
			return ResultUtil.error(msg+errorMsg);
		}
	}
	
	
	
	

}
