package com.tourism.hu.schedule;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.entity.Logistics;
import com.tourism.hu.service.ILogisticsService;
import com.tourism.hu.util.KdniaoTrackQueryAPI;

@Component
@Configuration // 1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling // 2.开启定时任务
public class SaticScheduleTask {
	
	
	@Autowired
	private ILogisticsService logisticsService;


	
	// 3.添加定时任务
	// @Scheduled(cron = "0/5 * * * * ?") //每5秒执行一次
	// 或直接指定时间间隔，例如：5秒
	// @Scheduled(fixedRate=5000)
	
	
	private void configureTasks() {
		System.err.println("执行静态定时任务时间: " + LocalDateTime.now());
	}

	/**
	 * 
	 * @Title: logistics
	 * @Description: (每天2点更新未完成的物流接口)
	 * @date 2019年11月16日 下午1:39:33
	 * @author 董兴隆
	 */
	@Scheduled(cron = "0 0 2 * * ? ")
	//@Scheduled(fixedRate = 5000*100)
	private void updateLogistics() {
		//不查询成功的和问题件物流
		List<Logistics> logisticsList = logisticsService.list(new QueryWrapper<Logistics>().ne("state", 3).ne("state", 4));
		for (Logistics logistics : logisticsList) {
			try {
				JSONObject logisticsJson = KdniaoTrackQueryAPI.getOrderTracesByJson(logistics.getBusinessAlias(),logistics.getExpressNo());
				if(logisticsJson!=null && logisticsJson.getBooleanValue("Success")) {
					String logisticsResult = logisticsJson.getJSONArray("Traces").toJSONString();
					//更新物流信息
					logistics.setLogisticsResult(logisticsResult);
					//更新物流状态
					Integer state = logisticsJson.getInteger("State");
					logistics.setState(state);
					logistics.setLastUpdateTime(LocalDateTime.now());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		logisticsService.updateBatchById(logisticsList);
	}



}