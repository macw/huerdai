package com.tourism.hu.service;

import com.tourism.hu.entity.ScoreAccount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员积分账户表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface IScoreAccountService extends IService<ScoreAccount> {

}
