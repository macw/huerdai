package com.tourism.hu.service;

import com.tourism.hu.entity.DedeSysconfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统参数配置表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface IDedeSysconfigService extends IService<DedeSysconfig> {

}
