package com.tourism.hu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tourism.hu.entity.GoodsCategory;
import com.tourism.hu.entity.TreeNode;

import java.util.List;

/**
 * <p>
 * 商品分类表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface IGoodsCategoryService extends IService<GoodsCategory> {

    /**
     * 查询商品分类表的一级 二级 目录
     * @return
     */
    List<TreeNode> selectAll();

}
