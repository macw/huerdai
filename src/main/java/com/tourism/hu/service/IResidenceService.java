package com.tourism.hu.service;

import com.tourism.hu.entity.Residence;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 民宿表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface IResidenceService extends IService<Residence> {

}
