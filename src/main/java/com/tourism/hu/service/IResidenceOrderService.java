package com.tourism.hu.service;

import com.tourism.hu.entity.ResidenceOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 民宿订单表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface IResidenceOrderService extends IService<ResidenceOrder> {

}
