package com.tourism.hu.service;

import com.tourism.hu.entity.GoodsDtl;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tourism.hu.entity.vo.GoodsDtlVo;

/**
 * <p>
 * 规格描述表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-05
 */
public interface IGoodsDtlService extends IService<GoodsDtl> {

//    GoodsDtlVo get

}
