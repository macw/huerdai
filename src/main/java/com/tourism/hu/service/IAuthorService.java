package com.tourism.hu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tourism.hu.entity.Author;
import com.tourism.hu.entity.EleTree;
import com.tourism.hu.entity.Role;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-06
 */
public interface IAuthorService extends IService<Author> {


    /**
     * 根据角色id，查询菜单id
     * @param roleId
     * @return
     */
    List<Author> selectMenuIdByRoleId(Integer roleId);

    /**
     * 根据角色id 去获取 授权表对象的按钮id
     * @param roleId
     * @return
     */
    List<Author> selectBtnIdByRoleId(Integer roleId);

    /**
     * 添加， 授权
      * @param role
     * @param eleTreeList
     * @return
     */
    int addAuthor(Role role, List<EleTree> eleTreeList);

    /**
     * 更新角色信息  和授权信息
     * @param role
     * @param eleTreeList
     * @return
     */
    int updateAuthor(Role role, List<EleTree> eleTreeList);




}
