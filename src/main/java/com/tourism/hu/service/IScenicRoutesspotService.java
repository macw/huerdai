package com.tourism.hu.service;

import com.tourism.hu.entity.ScenicRoutesspot;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 线路景点关联表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-25
 */
public interface IScenicRoutesspotService extends IService<ScenicRoutesspot> {

}
