package com.tourism.hu.service;

import com.tourism.hu.entity.PaymentChannel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 支付渠道  服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface IPaymentChannelService extends IService<PaymentChannel> {

}
