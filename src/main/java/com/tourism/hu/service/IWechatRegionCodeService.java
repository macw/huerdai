package com.tourism.hu.service;

import com.tourism.hu.entity.WechatRegionCode;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-19
 */
public interface IWechatRegionCodeService extends IService<WechatRegionCode> {

}
