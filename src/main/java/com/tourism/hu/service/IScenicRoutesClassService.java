package com.tourism.hu.service;

import com.tourism.hu.entity.ScenicRoutesClass;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 旅游线路分类表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-24
 */
public interface IScenicRoutesClassService extends IService<ScenicRoutesClass> {

}
