package com.tourism.hu.service;

import com.tourism.hu.entity.TourOperator;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface ITourOperatorService extends IService<TourOperator> {

}
