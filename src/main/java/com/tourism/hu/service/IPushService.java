package com.tourism.hu.service;

import com.tourism.hu.entity.Push;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface IPushService extends IService<Push> {

}
