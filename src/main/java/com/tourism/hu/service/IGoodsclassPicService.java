package com.tourism.hu.service;

import com.tourism.hu.entity.GoodsclassPic;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品商品分类图片表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface IGoodsclassPicService extends IService<GoodsclassPic> {

}
