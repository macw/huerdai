package com.tourism.hu.service;

import com.tourism.hu.entity.PrefixThird;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 第三方登录表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface IPrefixThirdService extends IService<PrefixThird> {

}
