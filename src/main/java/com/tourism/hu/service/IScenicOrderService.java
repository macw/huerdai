package com.tourism.hu.service;

import com.tourism.hu.entity.ScenicOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 旅游线路订单表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface IScenicOrderService extends IService<ScenicOrder> {

}
