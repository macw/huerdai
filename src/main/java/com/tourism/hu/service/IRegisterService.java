package com.tourism.hu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tourism.hu.entity.CustomerInfo;

/**
 * 
 * @ClassName: IRegisterService 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年12月12日 下午5:42:34 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
public interface IRegisterService extends IService<CustomerInfo> {

	
	  boolean register(CustomerInfo customerInfo);
}
