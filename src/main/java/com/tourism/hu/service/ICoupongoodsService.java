package com.tourism.hu.service;

import com.tourism.hu.entity.Coupongoods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 优惠券商品表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-30
 */
public interface ICoupongoodsService extends IService<Coupongoods> {

}
