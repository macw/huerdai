package com.tourism.hu.service;

import com.tourism.hu.entity.GoodsComment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tourism.hu.util.DataUtil;

/**
 * <p>
 * 商品评论表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface IGoodsCommentService extends IService<GoodsComment> {

    DataUtil selectAll(int page,int limit);

}
