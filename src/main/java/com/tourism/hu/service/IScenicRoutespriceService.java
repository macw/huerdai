package com.tourism.hu.service;

import com.tourism.hu.entity.ScenicRoutesprice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 旅游线路表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-19
 */
public interface IScenicRoutespriceService extends IService<ScenicRoutesprice> {

}
