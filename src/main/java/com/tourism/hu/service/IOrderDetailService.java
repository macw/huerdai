package com.tourism.hu.service;

import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.Goods;
import com.tourism.hu.entity.Order;
import com.tourism.hu.entity.OrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tourism.hu.entity.vo.GoodsDtlVo;

/**
 * <p>
 * 订单详情表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface IOrderDetailService extends IService<OrderDetail> {

    /**
     * 创建订单详情
     * @param orderDetail
     * @param order
     * @param goodsId
     * @param num
     * @param goodsDtlVo
     * @param goods
     * @param customerInfo
     * @return
     */
    boolean createOrderDetail( OrderDetail orderDetail,Order order, Integer goodsId, Integer num, GoodsDtlVo goodsDtlVo, Goods goods, CustomerInfo customerInfo);
}
