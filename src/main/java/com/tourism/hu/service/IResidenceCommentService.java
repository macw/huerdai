package com.tourism.hu.service;

import com.tourism.hu.entity.ResidenceComment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 民宿评论表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface IResidenceCommentService extends IService<ResidenceComment> {

}
