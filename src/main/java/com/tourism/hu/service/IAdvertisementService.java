package com.tourism.hu.service;

import com.tourism.hu.entity.Advertisement;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-19
 */
public interface IAdvertisementService extends IService<Advertisement> {

}
