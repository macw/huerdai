package com.tourism.hu.service.impl;

import com.tourism.hu.entity.StrategicTravels;
import com.tourism.hu.mapper.StrategicTravelsMapper;
import com.tourism.hu.service.IStrategicTravelsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 攻略游记表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
@Service
public class StrategicTravelsServiceImpl extends ServiceImpl<StrategicTravelsMapper, StrategicTravels> implements IStrategicTravelsService {

}
