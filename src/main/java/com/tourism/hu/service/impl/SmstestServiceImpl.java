package com.tourism.hu.service.impl;

import com.tourism.hu.entity.Smstest;
import com.tourism.hu.mapper.SmstestMapper;
import com.tourism.hu.service.ISmstestService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 验证码登录表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class SmstestServiceImpl extends ServiceImpl<SmstestMapper, Smstest> implements ISmstestService {

}
