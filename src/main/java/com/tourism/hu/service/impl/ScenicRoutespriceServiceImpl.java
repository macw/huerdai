package com.tourism.hu.service.impl;

import com.tourism.hu.entity.ScenicRoutesprice;
import com.tourism.hu.mapper.ScenicRoutespriceMapper;
import com.tourism.hu.service.IScenicRoutespriceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 旅游线路表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-19
 */
@Service
public class ScenicRoutespriceServiceImpl extends ServiceImpl<ScenicRoutespriceMapper, ScenicRoutesprice> implements IScenicRoutespriceService {

}
