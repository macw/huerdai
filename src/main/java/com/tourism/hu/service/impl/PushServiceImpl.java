package com.tourism.hu.service.impl;

import com.tourism.hu.entity.Push;
import com.tourism.hu.mapper.PushMapper;
import com.tourism.hu.service.IPushService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
@Service
public class PushServiceImpl extends ServiceImpl<PushMapper, Push> implements IPushService {

}
