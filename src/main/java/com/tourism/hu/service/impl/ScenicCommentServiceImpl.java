package com.tourism.hu.service.impl;

import com.tourism.hu.entity.ScenicComment;
import com.tourism.hu.mapper.ScenicCommentMapper;
import com.tourism.hu.service.IScenicCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 线路评论表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-05
 */
@Service
public class ScenicCommentServiceImpl extends ServiceImpl<ScenicCommentMapper, ScenicComment> implements IScenicCommentService {

}
