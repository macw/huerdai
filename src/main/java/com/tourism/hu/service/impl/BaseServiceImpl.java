package com.tourism.hu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.ProfitSaringConstant;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.ProfitSaring;
import com.tourism.hu.mapper.CustomerInfoMapper;
import com.tourism.hu.service.IBaseService;
import com.tourism.hu.service.IProfitFlowingService;
import com.tourism.hu.service.IProfitSaringService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 13:45
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@Service
public class BaseServiceImpl implements IBaseService {

    @Resource
    private IProfitFlowingService iProfitFlowingService;

    @Resource
    private IProfitSaringService iProfitSaringService;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private CustomerInfoMapper customerInfoMapper;


    @Override
    public HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
    }

    @Override
    public CustomerInfo getCustomerInfo() {
        String sessionid = getRequest().getSession().getId();
        Object obj = redisTemplate.opsForValue().get(sessionid);
        String customerId = "";
        if(obj!=null) {
            customerId=obj.toString();
        }
        CustomerInfo customerInfo = customerInfoMapper.selectOne(new QueryWrapper<CustomerInfo>().eq("id", customerId));
        return customerInfo;
    }

    @Override
    public boolean isVIP() {
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo!=null){
            if (customerInfo.getIsCuslevel()!=null && customerInfo.getIsCuslevel()==1){
                return true;
            }else {
                return false;
            }
        }else {
            return false;
        }
    }

    @Override
    public BigDecimal getPercentage(BigDecimal price, BigDecimal exitPrice, BigDecimal activePrice) {
        BigDecimal profits = getProfits(price, exitPrice, activePrice);
        int isvip= ProfitSaringConstant.ORD_TYPE;
        //判断是否为VIP
        if (isVIP()){
            isvip=ProfitSaringConstant.MEM_TYPE;
        }
        ProfitSaring profitSaring = iProfitSaringService.getOne(new QueryWrapper<ProfitSaring>().eq("type", isvip).eq("status", ProfitSaringConstant.GOODS_STATUS));
        //用户分成百分比
        if(profitSaring==null) {
            return BigDecimal.ZERO;
        }
        double getpPercentage = profitSaring.getpPercentage()*0.01;
        BigDecimal num = new BigDecimal(getpPercentage);
        //计算节省金额
        BigDecimal multiply = profits.multiply(num);
        //四舍五入只保留两位小数，如，2.35变成2.4
        return  multiply.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    @Override
    public BigDecimal getPercentage(Integer status, BigDecimal price, BigDecimal exitPrice, BigDecimal activePrice, Integer userType) {


        BigDecimal profits = getProfits(price, exitPrice, activePrice);
        int isvip= ProfitSaringConstant.ORD_TYPE;
        //判断是否为VIP
        if (isVIP()){
            isvip= ProfitSaringConstant.MEM_TYPE;
        }
        ProfitSaring profitSaring = iProfitSaringService.getOne(new QueryWrapper<ProfitSaring>().eq("type", isvip).eq("status", status));
        if(profitSaring==null) {
            return BigDecimal.ZERO;
        }
        //用户分成百分比
        double getpPercentage = 0d;
        if (userType== BaseConstant.AGENT_TYPE && profitSaring.getAgentPercentage()!=null && profitSaring.getAgentPercentage()>0){
            getpPercentage=profitSaring.getAgentPercentage()*0.01;
        }else if(userType==BaseConstant.USER_TYPE && profitSaring.getpPercentage()!=null && profitSaring.getpPercentage()>0){
            getpPercentage=profitSaring.getpPercentage()*0.01;
        }else if (userType==BaseConstant.PLAT_TYPE && profitSaring.getPlatformPercentage()!=null && profitSaring.getPlatformPercentage()>0){
            getpPercentage=profitSaring.getPlatformPercentage()*0.01;
        }else if(userType==BaseConstant.PARE_TYPE && profitSaring.getParentPPercentage()!=null && profitSaring.getParentPPercentage()>0){
            getpPercentage=profitSaring.getParentPPercentage()*0.01;
        }
        //计算百分比
        BigDecimal percent = new BigDecimal(getpPercentage);
        //计算节省金额
        BigDecimal multiply = profits.multiply(percent);
        //四舍五入只保留两位小数，如，2.35变成2.4
        return  multiply.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    @Override
    public BigDecimal getProfits(BigDecimal price, BigDecimal exitPrice, BigDecimal activePrice) {
        /**
         * 价格与0做比较，
         * 大于0，priceUP==1
         * 等于0，priceUP==0
         * 小于0，priceUP==-1
         */
        if(activePrice!=null && activePrice.compareTo(BigDecimal.ZERO)==1){
            return activePrice.subtract(exitPrice);
        }else if(price!=null &&  price.compareTo(BigDecimal.ZERO)==1){
            return price.subtract(exitPrice);
        }else{
            return BigDecimal.ZERO;
        }
    }
}
