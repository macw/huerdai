package com.tourism.hu.service.impl;

import com.tourism.hu.entity.ProfitSaring;
import com.tourism.hu.mapper.ProfitSaringMapper;
import com.tourism.hu.service.IProfitSaringService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class ProfitSaringServiceImpl extends ServiceImpl<ProfitSaringMapper, ProfitSaring> implements IProfitSaringService {

}
