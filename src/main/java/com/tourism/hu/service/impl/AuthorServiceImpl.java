package com.tourism.hu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tourism.hu.constant.AuthorConstant;
import com.tourism.hu.constant.MenuConsant;
import com.tourism.hu.entity.*;
import com.tourism.hu.mapper.AuthorMapper;
import com.tourism.hu.mapper.MenuMapper;
import com.tourism.hu.mapper.OperationMapper;
import com.tourism.hu.mapper.RoleMapper;
import com.tourism.hu.service.IAuthorService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-06
 */
@Service
public class AuthorServiceImpl extends ServiceImpl<AuthorMapper, Author> implements IAuthorService {

    @Resource
    private AuthorMapper authorMapper;

    @Resource
    private MenuMapper menuMapper;

    @Resource
    private RoleMapper roleMapper;

    @Resource
    private OperationMapper operationMapper;

    @Override
    public List<Author> selectMenuIdByRoleId(Integer roleId) {
        List<Author> authors = authorMapper.selectList(new QueryWrapper<Author>().eq("role_id", roleId).eq("resource_type", AuthorConstant.MENU_RESOURCE_TYPE));
        return authors;
    }

    @Override
    public List<Author> selectBtnIdByRoleId(Integer roleId) {
        List<Author> authorsBtn = authorMapper.selectList(new QueryWrapper<Author>().eq("role_id", roleId).eq("resource_type", AuthorConstant.BTN_RESOURCE_TYPE));
        return authorsBtn;
    }

    @Override
    public int addAuthor(Role role, List<EleTree> eleTreeList) {
        int i = 0;
        //遍历获取到的资源集合
        for (EleTree eleTree : eleTreeList) {
            Menu menu = menuMapper.selectOne(new QueryWrapper<Menu>().eq("menu_id", eleTree.getId()).eq("menu_name", eleTree.getLabel()));
            Operation operation = operationMapper.selectOne(new QueryWrapper<Operation>().eq("btn_id", eleTree.getId()).eq("btn_name", eleTree.getLabel()));
            //不为空，说明这个资源是个菜单,然后将这个菜单添加到授权
            if (menu!=null && operation==null){
                //过滤一级目录，只存二级目录
                if (menu.getLevel()!= MenuConsant.ONE_LEVEL){
                    Author author = new Author(null,role.getRoleId(),menu.getMenuId(),AuthorConstant.MENU_RESOURCE_TYPE);
                    i+= authorMapper.insert(author);
                }
            }else if (menu==null && operation!=null){
                Author author = new Author(null,role.getRoleId(),operation.getBtnId(),AuthorConstant.BTN_RESOURCE_TYPE);
                i+= authorMapper.insert(author);
            }
        }
        return i;
    }

    @Override
    public int updateAuthor(Role role, List<EleTree> eleTreeList) {
        //先更新角色信息
        int update = roleMapper.updateById(role);
        //更新授权信息
        //先删除该角色拥有的所有权限信息，再根据传入的的资源进行添加
        authorMapper.delete(new QueryWrapper<Author>().eq("role_id", role.getRoleId()));
        //再进行添加权限
        //遍历获取到的资源集合
        int i = 0;
        for (EleTree eleTree : eleTreeList) {
            Menu menu = menuMapper.selectOne(new QueryWrapper<Menu>().eq("menu_id", eleTree.getId()).eq("menu_name", eleTree.getLabel()));
            Operation operation = operationMapper.selectOne(new QueryWrapper<Operation>().eq("btn_id", eleTree.getId()).eq("btn_name", eleTree.getLabel()));
            //不为空，说明这个资源是个菜单,然后将这个菜单添加到授权
            if (menu!=null && operation==null){
                //过滤一级目录，只存二级目录
                if (menu.getLevel()!= MenuConsant.ONE_LEVEL){
                    Author author = new Author(null,role.getRoleId(),menu.getMenuId(),AuthorConstant.MENU_RESOURCE_TYPE);
                    i+= authorMapper.insert(author);
                }
            }else if (menu==null && operation!=null){
                Author author = new Author(null,role.getRoleId(),operation.getBtnId(),AuthorConstant.BTN_RESOURCE_TYPE);
                i+= authorMapper.insert(author);
            }
        }
        if (update==1 && i>0){
            return i;
        }
        else {
            return 0;
        }
    }


}
