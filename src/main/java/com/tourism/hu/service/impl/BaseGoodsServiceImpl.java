package com.tourism.hu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.GoodsDtlConstant;
import com.tourism.hu.entity.Goods;
import com.tourism.hu.entity.GoodsDtl;
import com.tourism.hu.entity.GoodsDtlImg;
import com.tourism.hu.entity.vo.GoodsDtlType;
import com.tourism.hu.entity.vo.GoodsDtlVo;
import com.tourism.hu.service.*;
import com.tourism.hu.util.ResponseResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 11:48
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@Service
public class BaseGoodsServiceImpl implements IBaseGoods {

    @Resource
    private IGoodsDtlService iGoodsDtlService;

    @Resource
    private IGoodsService iGoodsService;

    @Resource
    private IGoodsDtlImgService iGoodsDtlImgService;

    @Resource
    private IOrderService iOrderService;

    @Resource
    private IOrderDetailService iOrderDetailService;

    @Resource
    private IBaseGoods iBaseGoods;


    @Override
    public BigDecimal getActualPrice(BigDecimal getPrice, BigDecimal getActivityPrice) {
        if (getActivityPrice != null) {
            return getActivityPrice;
        } else {
            return getPrice;
        }
    }

    @Override
    public GoodsDtlVo getGoodsDtlVo(Integer goodsId, Integer[] dtlIdArray) {
        //创建返回实体
        GoodsDtlVo goodsDtlVo = new GoodsDtlVo();

        /**
         * 计算商品的实际价格
         */
        Goods goods = iGoodsService.getById(goodsId);
        BigDecimal actualPrice = iBaseGoods.getActualPrice(goods.getPrice(), goods.getActivityPrice());
        List<String> stringList = new ArrayList<>();
        List<GoodsDtl> dtlList = new ArrayList<>();
        //如果没选择规格，无数据
        if (dtlIdArray == null || dtlIdArray.length==0){
            goodsDtlVo.setGoodsDtlPrice(actualPrice);
            goodsDtlVo.setDtlPicUrl(goods.getPictureUrl());
            return goodsDtlVo;
        }


        for (Integer id : dtlIdArray) {
            //根据商品id 和 商品规格主键id 查询商品规格
            GoodsDtl goodsDtl = iGoodsDtlService.getOne(new QueryWrapper<GoodsDtl>().eq("goods_id", goodsId).eq("goodsdtl_id", id));
            if (goodsDtl != null) {
                dtlList.add(goodsDtl);
            }
        }
        for (GoodsDtl goodsDtl : dtlList) {
            /**
             * 颜色，选择图片，和价格
             * 型号，选择价格
             */
            if (goodsDtl.getType() == GoodsDtlConstant.COLOR_TYPE) {
                GoodsDtlImg goodsDtlImg = iGoodsDtlImgService.getOne(new QueryWrapper<GoodsDtlImg>().eq("goodsdtl_id", goodsDtl.getGoodsdtlId()));
                //设置返回图片
                if (goodsDtlImg != null) {
                    goodsDtlVo.setDtlPicUrl(goodsDtlImg.getClassimgAdress());
                }
                //实际价格 + 商品规格加价
                actualPrice = actualPrice.add(goodsDtl.getDtlPrice());
                stringList.add(goodsDtl.getSpec());

            } else if (goodsDtl.getType() == GoodsDtlConstant.MODEL_TYPE) {
                actualPrice = actualPrice.add(goodsDtl.getDtlPrice());
                stringList.add(goodsDtl.getSpec());
            }
        }
        goodsDtlVo.setGoodsDtlPrice(actualPrice);
        goodsDtlVo.setDtlStringList(stringList);
        return goodsDtlVo;
    }
}
