package com.tourism.hu.service.impl;

import com.tourism.hu.entity.DedeSysconfig;
import com.tourism.hu.mapper.DedeSysconfigMapper;
import com.tourism.hu.service.IDedeSysconfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 系统参数配置表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class DedeSysconfigServiceImpl extends ServiceImpl<DedeSysconfigMapper, DedeSysconfig> implements IDedeSysconfigService {

}
