package com.tourism.hu.service.impl;

import com.tourism.hu.entity.Coupongoods;
import com.tourism.hu.mapper.CoupongoodsMapper;
import com.tourism.hu.service.ICoupongoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠券商品表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-30
 */
@Service
public class CoupongoodsServiceImpl extends ServiceImpl<CoupongoodsMapper, Coupongoods> implements ICoupongoodsService {

}
