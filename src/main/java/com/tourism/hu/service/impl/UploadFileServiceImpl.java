package com.tourism.hu.service.impl;

import com.tourism.hu.entity.UploadFile;
import com.tourism.hu.mapper.UploadFileMapper;
import com.tourism.hu.service.IUploadFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 上传文件记录表 服务实现类
 * </p>
 *
 * @author hu
 * @since 2019-12-04
 */
@Service
public class UploadFileServiceImpl extends ServiceImpl<UploadFileMapper, UploadFile> implements IUploadFileService {

}
