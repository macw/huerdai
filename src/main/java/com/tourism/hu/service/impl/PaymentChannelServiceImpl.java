package com.tourism.hu.service.impl;

import com.tourism.hu.entity.PaymentChannel;
import com.tourism.hu.mapper.PaymentChannelMapper;
import com.tourism.hu.service.IPaymentChannelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支付渠道  服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class PaymentChannelServiceImpl extends ServiceImpl<PaymentChannelMapper, PaymentChannel> implements IPaymentChannelService {

}
