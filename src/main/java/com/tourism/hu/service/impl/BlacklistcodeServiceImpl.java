package com.tourism.hu.service.impl;

import com.tourism.hu.entity.Blacklistcode;
import com.tourism.hu.mapper.BlacklistcodeMapper;
import com.tourism.hu.service.IBlacklistcodeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-29
 */
@Service
public class BlacklistcodeServiceImpl extends ServiceImpl<BlacklistcodeMapper, Blacklistcode> implements IBlacklistcodeService {

}
