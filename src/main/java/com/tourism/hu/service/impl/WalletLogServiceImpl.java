package com.tourism.hu.service.impl;

import com.tourism.hu.entity.WalletLog;
import com.tourism.hu.mapper.WalletLogMapper;
import com.tourism.hu.service.IWalletLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 钱包变动日志 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-24
 */
@Service
public class WalletLogServiceImpl extends ServiceImpl<WalletLogMapper, WalletLog> implements IWalletLogService {

}
