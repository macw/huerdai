package com.tourism.hu.service.impl;

import com.tourism.hu.entity.ScenicSpotImg;
import com.tourism.hu.mapper.ScenicSpotImgMapper;
import com.tourism.hu.service.IScenicSpotImgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 旅游景点图片表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-31
 */
@Service
public class ScenicSpotImgServiceImpl extends ServiceImpl<ScenicSpotImgMapper, ScenicSpotImg> implements IScenicSpotImgService {

}
