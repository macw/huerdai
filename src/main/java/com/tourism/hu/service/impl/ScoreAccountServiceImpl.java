package com.tourism.hu.service.impl;

import com.tourism.hu.entity.ScoreAccount;
import com.tourism.hu.mapper.ScoreAccountMapper;
import com.tourism.hu.service.IScoreAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员积分账户表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class ScoreAccountServiceImpl extends ServiceImpl<ScoreAccountMapper, ScoreAccount> implements IScoreAccountService {

}
