package com.tourism.hu.service.impl;

import com.tourism.hu.entity.TourOperator;
import com.tourism.hu.mapper.TourOperatorMapper;
import com.tourism.hu.service.ITourOperatorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class TourOperatorServiceImpl extends ServiceImpl<TourOperatorMapper, TourOperator> implements ITourOperatorService {

}
