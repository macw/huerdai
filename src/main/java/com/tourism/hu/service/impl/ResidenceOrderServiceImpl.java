package com.tourism.hu.service.impl;

import com.tourism.hu.entity.ResidenceOrder;
import com.tourism.hu.mapper.ResidenceOrderMapper;
import com.tourism.hu.service.IResidenceOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 民宿订单表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
@Service
public class ResidenceOrderServiceImpl extends ServiceImpl<ResidenceOrderMapper, ResidenceOrder> implements IResidenceOrderService {

}
