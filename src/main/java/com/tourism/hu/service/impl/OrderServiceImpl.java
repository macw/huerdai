package com.tourism.hu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.OrderConstant;
import com.tourism.hu.constant.SeckillConstant;
import com.tourism.hu.entity.*;
import com.tourism.hu.entity.vo.GoodsDtlVo;
import com.tourism.hu.mapper.OrderMapper;
import com.tourism.hu.service.IAddressService;
import com.tourism.hu.service.IOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tourism.hu.service.ISeckillService;
import com.tourism.hu.util.OrderCodeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 订单主表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
@Transactional
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IAddressService iAddressService;


    @Resource
    private IOrderService iOrderService;

    @Override
    public boolean createOrder(Order order, CustomerInfo customerInfo, Integer addressId,ProfitFlowing profitFlowing) {
        order.setOrderSn(profitFlowing.getOrderSn());

        //下单人
        order.setCustomerName(customerInfo.getNickname());
        order.setCustomerId(customerInfo.getCustomerId());
        //根据用户id查询默认收货地址 ，地址状态，1为默认，0为非默认
        Address address = iAddressService.getById(addressId);
        order.setAddress(address.getAddress());
        //购买人，收货人姓名
        order.setShippingUser(address.getConsignee());
        //收货人手机号
        order.setCusPhone(address.getTelephone());
        //订单状态
        order.setOrderStatus(OrderConstant.ONE_NO_PAY);
        //时间戳
        order.setOrderTime(LocalDateTime.now());
        order.setCreateTime(LocalDateTime.now());
        order.setCreateUserId(customerInfo.getCustomerId());
        order.setCreateUser(customerInfo.getNickname());

        order.setOrderMoney(profitFlowing.getOrderPriceAll());
        order.setDistrictMoney(profitFlowing.getSubPriceAll());
        order.setPaymentMoney(profitFlowing.getSalePrice());
        logger.debug("创建订单：order=="+order);
        //创建订单
        return iOrderService.save(order);
    }
}
