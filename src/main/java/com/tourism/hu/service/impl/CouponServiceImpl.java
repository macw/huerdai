package com.tourism.hu.service.impl;

import com.tourism.hu.entity.Coupon;
import com.tourism.hu.mapper.CouponMapper;
import com.tourism.hu.service.ICouponService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-24
 */
@Service
public class CouponServiceImpl extends ServiceImpl<CouponMapper, Coupon> implements ICouponService {

}
