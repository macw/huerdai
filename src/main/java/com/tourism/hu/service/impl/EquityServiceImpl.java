package com.tourism.hu.service.impl;

import com.tourism.hu.entity.Equity;
import com.tourism.hu.mapper.EquityMapper;
import com.tourism.hu.service.IEquityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-15
 */
@Service
public class EquityServiceImpl extends ServiceImpl<EquityMapper, Equity> implements IEquityService {

}
