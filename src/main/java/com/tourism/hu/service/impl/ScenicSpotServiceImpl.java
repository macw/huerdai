package com.tourism.hu.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tourism.hu.entity.ScenicSpot;
import com.tourism.hu.mapper.ScenicSpotMapper;
import com.tourism.hu.service.IScenicSpotService;

/**
 * <p>
 * 旅游景点表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class ScenicSpotServiceImpl extends ServiceImpl<ScenicSpotMapper, ScenicSpot> implements IScenicSpotService {

}
