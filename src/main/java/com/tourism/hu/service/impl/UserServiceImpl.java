package com.tourism.hu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.constant.UserConstant;
import com.tourism.hu.entity.Role;
import com.tourism.hu.entity.User;
import com.tourism.hu.entity.UserRole;
import com.tourism.hu.mapper.RoleMapper;
import com.tourism.hu.mapper.UserMapper;
import com.tourism.hu.mapper.UserRoleMapper;
import com.tourism.hu.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import com.tourism.hu.util.ResultUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-05
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private IUserService iUserService;

    @Resource
    private UserRoleMapper userRoleMapper;

    @Resource
    private RoleMapper roleMapper;

    @Override
    public DataUtil selectAll(int page, int limit, String name) {
        Page<User> page1 = new Page<>(page, limit);
        IPage<User> IPage = null;
        if (name == null || name == "") {
            IPage = iUserService.page(page1, new QueryWrapper<>());
        } else {
            IPage = userMapper.selectPage(page1, new QueryWrapper<User>().like("name", name));
        }
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<User> userList = IPage.getRecords();

        for (User user : userList) {
            UserRole userRole = userRoleMapper.selectOne(new QueryWrapper<UserRole>().eq("user_id", user.getUserId()));
            if (userRole!=null){
                Role role = roleMapper.selectOne(new QueryWrapper<Role>().eq("role_id", userRole.getRoleId()));
                user.setRoleName(role.getRoleName());
            }
        }
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(userList);
        dataUtil.setCount(total);
        return dataUtil;
    }

    @Override
    public DataUtil selectAgentAll(int page, int limit, String name,User users) {
        Page<User> page1 = new Page<>(page, limit);
        IPage<User> IPage = null;
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("department_id", UserConstant.DEP_THREE).eq("parent_id", users.getUserId());
        if (name == null || name == "") {
            IPage = userMapper.selectPage(page1, queryWrapper);

        } else {
            queryWrapper.like("name", name);
            IPage = userMapper.selectPage(page1, queryWrapper);
        }
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<User> userList = IPage.getRecords();

        for (User user : userList) {
            UserRole userRole = userRoleMapper.selectOne(new QueryWrapper<UserRole>().eq("user_id", user.getUserId()));
            if (userRole!=null){
                Role role = roleMapper.selectOne(new QueryWrapper<Role>().eq("role_id", userRole.getRoleId()));
                user.setRoleName(role.getRoleName());
            }
        }
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(userList);
        dataUtil.setCount(total);
        return dataUtil;
    }

    @Override
    public User selectOne(Integer aid) {
        User user = userMapper.selectOne(new QueryWrapper<User>().eq("user_id", aid));
        UserRole userRole = userRoleMapper.selectOne(new QueryWrapper<UserRole>().eq("user_id", user.getUserId()));
        if (userRole!=null){
            Role role = roleMapper.selectOne(new QueryWrapper<Role>().eq("role_id", userRole.getRoleId()));
            user.setRoleName(role.getRoleName());
        }
        return user;
    }


    /**
     *
     * @param user 前台传过来的用户信息
     * @param loginUser  当前登录用户
     * @return
     */
    @Override
    public ResultUtil updateOrSave(User user,User loginUser) {
        ResultUtil<User> data = ResultUtil.error();
        if(loginUser == null) {
            return data.error("获取登录用户失败");
        }
        //如果有id，则执行更新操作
        if (user.getUserId()!=null){
            data.setMsg("更新失败");
            user.setUpdateTime(LocalDateTime.now());
            user.setUpdateUser(loginUser.getName());
            user.setUpdateUserId(loginUser.getUserId());
            int i = userMapper.updateById(user);
            int j = 0;
            if (i>0) {
                UserRole userRole = userRoleMapper.selectOne(new QueryWrapper<UserRole>().eq("user_id", user.getUserId()));
                Role role = roleMapper.selectOne(new QueryWrapper<Role>().eq("role_name", user.getRoleName()));
                if (userRole==null && role!=null){
                    //说明该用户没有角色信息，就添加
                    UserRole uRole = new UserRole(null,role.getRoleId(),user.getUserId());
                    userRoleMapper.insert(uRole);
                }
                if (user.getRoleName()==null || user.getRoleName()=="") {
                    //说明该用户没有角色信息
                    int id = userRoleMapper.delete(new QueryWrapper<UserRole>().eq("user_id", user.getUserId()));
                }
                if (userRole!=null && role!=null){
                    userRole.setRoleId(role.getRoleId());
                    j = userRoleMapper.updateById(userRole);
                }
                //否则就执行更新
            }
            if (i>0 && j>0){
                data.setCode(0);
                data.setMsg("更新成功");
            }
            //没有用户id，则添加
        }else{
            data.setMsg("添加失败");
            user.setCreateTime(LocalDateTime.now());
            user.setCreateUser(loginUser.getName());
            user.setCreateUserId(loginUser.getUserId());
            if (user.getRoleName()!=null && user.getRoleName()=="directorAdmin"){
                user.setDepartmentId(UserConstant.DEP_TWO);
            }else if (user.getRoleName()!=null && user.getRoleName()=="agentadmin"){
                user.setDepartmentId(UserConstant.DEP_THREE);
            }
            int i = userMapper.insert(user);
            int j = 0;
            if (i>0){
            Role role = roleMapper.selectOne(new QueryWrapper<Role>().eq("role_name", user.getRoleName()));
            UserRole userRole = new UserRole(null,role.getRoleId(),user.getUserId());
              j = userRoleMapper.insert(userRole);
            }
            if (i>0 && j>0){
                data.setCode(0);
                data.setMsg("添加成功");
            }
        }
        return data;
    }


    @Override
    public ModelAndView toEditUser(Integer userId) {
        ModelAndView mv = new ModelAndView("users/administrators/edituserlist");
        User user = userMapper.selectOne(new QueryWrapper<User>().eq("user_id", userId));
        UserRole userRole = null;
        if (user !=null){
            userRole = userRoleMapper.selectOne(new QueryWrapper<UserRole>().eq("user_id", user.getUserId()));
        }
        if (userRole!=null){
            Role role = roleMapper.selectOne(new QueryWrapper<Role>().eq("role_id", userRole.getRoleId()));
            user.setRoleName(role.getRoleName());
        }

        List<Role> roleList = roleMapper.selectList(null);
        mv.addObject("roleList",roleList);
        mv.addObject("userBack",user);
        return mv;
    }

    @Override
    public MsgUtil deleteOne(Integer aid) {
        UserRole userRole = userRoleMapper.selectOne(new QueryWrapper<UserRole>().eq("user_id", aid));
        if (userRole!=null){
            userRoleMapper.delete(new QueryWrapper<UserRole>().eq("user_id",aid));
        }
        int i = userMapper.deleteById(aid);
        return MsgUtil.flag(i);
    }

    @Override
    public MsgUtil delectMany(Integer[] ids) {
        List list = new ArrayList();
        for (Integer id : ids) {
            UserRole userRole = userRoleMapper.selectOne(new QueryWrapper<UserRole>().eq("user_id", id));
            if (userRole!=null){
                userRoleMapper.delete(new QueryWrapper<UserRole>().eq("user_id",id));
            }
            list.add(id);
        }
        int i = userMapper.deleteBatchIds(list);
        return MsgUtil.flag(i);
    }
}
