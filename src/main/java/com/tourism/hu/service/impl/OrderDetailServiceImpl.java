package com.tourism.hu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.entity.*;
import com.tourism.hu.entity.vo.GoodsDtlVo;
import com.tourism.hu.mapper.OrderDetailMapper;
import com.tourism.hu.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 订单详情表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
@Transactional
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements IOrderDetailService {

    @Resource
    private IOrderDetailService iOrderDetailService;

    @Resource
    private IGoodsEquityService iGoodsEquityService;

    @Resource
    private IEquityService iEquityService;

    @Resource
    private IMerchantService iMerchantService;

    @Override
    public boolean createOrderDetail( OrderDetail orderDetail,Order order, Integer goodsId, Integer num, GoodsDtlVo goodsDtlVo, Goods goods, CustomerInfo customerInfo) {
        orderDetail.setOrderId(order.getOrderId());
        orderDetail.setGoodsId(goodsId);
        orderDetail.setProductCnt(num);
        if (goodsDtlVo.getDtlStringList()!=null && goodsDtlVo.getDtlStringList().size()>0){
            orderDetail.setGoodsDtlDetail(goodsDtlVo.getDtlStringList().toString().replace("[", "").replace("]", ""));
        }
        orderDetail.setOrderSn(order.getOrderSn());
        Merchant merchant = iMerchantService.getById(goods.getMerchantId());
        orderDetail.setMerName(merchant.getMerName());
        orderDetail.setMerPhone(merchant.getPhone());
        orderDetail.setMerId(merchant.getMerId());
        //成本价格，也就是出厂价， 售价
        orderDetail.setGoodsName(goods.getGoodsname());
        orderDetail.setGoodsId(goods.getGoodsId());
        orderDetail.setGoodsCode(goods.getGoodsCode());
        orderDetail.setGoodsPictureUrl(goods.getPictureUrl());
        //设置 商品信息的 权益 列表
        List<String> list = new ArrayList<>();
        List<GoodsEquity> equityList = iGoodsEquityService.list(new QueryWrapper<GoodsEquity>().eq("goods_id", goodsId));
        for (GoodsEquity goodsEquity : equityList) {
            Equity equity = iEquityService.getOne(new QueryWrapper<Equity>().eq("equity_id", goodsEquity.getEquityId()).eq("status", BaseConstant.USE_STATUS));
            list.add(equity.getEquityName());
        }
        if (list!=null && list.size()>0){
            orderDetail.setGoodsEquityDetail(list.toString().replace("[", "").replace("]", ""));
        }
        //时间戳
        orderDetail.setCreateTime(LocalDateTime.now());
        orderDetail.setCreateUser(customerInfo.getNickname());
        orderDetail.setCreateUserId(customerInfo.getCustomerId());

        //执行 插入订单详情表
        return iOrderDetailService.save(orderDetail);

    }
}
