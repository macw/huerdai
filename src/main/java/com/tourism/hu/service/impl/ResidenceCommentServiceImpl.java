package com.tourism.hu.service.impl;

import com.tourism.hu.entity.ResidenceComment;
import com.tourism.hu.mapper.ResidenceCommentMapper;
import com.tourism.hu.service.IResidenceCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 民宿评论表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
@Service
public class ResidenceCommentServiceImpl extends ServiceImpl<ResidenceCommentMapper, ResidenceComment> implements IResidenceCommentService {

}
