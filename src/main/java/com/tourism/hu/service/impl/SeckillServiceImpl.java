package com.tourism.hu.service.impl;

import com.tourism.hu.entity.Seckill;
import com.tourism.hu.entity.dto.SeckillDto;
import com.tourism.hu.mapper.SeckillMapper;
import com.tourism.hu.service.ISeckillService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 秒杀表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
@Service
public class SeckillServiceImpl extends ServiceImpl<SeckillMapper, Seckill> implements ISeckillService {


}
