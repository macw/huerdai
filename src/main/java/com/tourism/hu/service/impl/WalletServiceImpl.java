package com.tourism.hu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.OrderConstant;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.Order;
import com.tourism.hu.entity.ProfitFlowing;
import com.tourism.hu.entity.Wallet;
import com.tourism.hu.mapper.WalletMapper;
import com.tourism.hu.service.ICustomerInfoService;
import com.tourism.hu.service.IOrderService;
import com.tourism.hu.service.IProfitFlowingService;
import com.tourism.hu.service.IWalletService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tourism.hu.util.DateConverterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 用户钱包 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-20
 */
@Service
public class WalletServiceImpl extends ServiceImpl<WalletMapper, Wallet> implements IWalletService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IWalletService iWalletService;

    @Resource
    private IProfitFlowingService iProfitFlowingService;

    @Resource
    private IOrderService iOrderService;

    @Resource
    private ICustomerInfoService iCustomerInfoService;


    /**
     * 该用户下的所有商品订单，收益计算
     * @param customerId
     * @return
     */
    @Override
    public Wallet selectGoodsOrderProfit(Integer customerId) {
        Wallet wallet = new Wallet();
        //查询该用户下的所有订单
        List<Order> orderList = iOrderService.list(new QueryWrapper<Order>().eq("customer_id", customerId).ge("order_status", OrderConstant.TWO_YES_PAY));
        if (orderList!=null && orderList.size()>0){
            //设置总付款笔数
            wallet.setOrderNumber(orderList.size());
        }
        logger.debug("总付款笔数:=-------------------------"+wallet.getOrderNumber());
        //查询该用户下订单的所有利润分成信息
        BigDecimal cusMoney = BigDecimal.ZERO;
        for (Order order : orderList) {
            ProfitFlowing profitFlowing = iProfitFlowingService.getOne(new QueryWrapper<ProfitFlowing>().eq("order_sn", order.getOrderSn()));
            cusMoney = cusMoney.add(profitFlowing.getCusMoney());
        }

        //查询该用户所推荐的下级用户列表
        List<CustomerInfo> customerInfoList = iCustomerInfoService.list(new QueryWrapper<CustomerInfo>().eq("parent_id", customerId));
        for (CustomerInfo customerInfo : customerInfoList) {
            //查询下级用户的所有订单信息
            List<Order> orderList1 = iOrderService.list(new QueryWrapper<Order>().eq("customer_id", customerInfo.getCustomerId()).ge("order_status", OrderConstant.TWO_YES_PAY));
            for (Order order : orderList1) {
                ProfitFlowing profitFlowing = iProfitFlowingService.getOne(new QueryWrapper<ProfitFlowing>().eq("order_sn", order.getOrderSn()));
                BigDecimal parentcusMoney = profitFlowing.getParentcusMoney();
                //将下级用户的上级返利 加到 该用户的总收益中
                cusMoney = cusMoney.add(parentcusMoney);
            }
        }
        //成交预估总收益
        wallet.setReadyProfit(cusMoney);
        return wallet;
    }

    /**
     * 获取当前用户某月的 消费预估收入
     * @param customerId
     * @return
     */
    @Override
    public BigDecimal getLocalMonthConsume(Integer customerId,Integer number) {

        BigDecimal totalPrice = BigDecimal.ZERO;
        //查询该用户下的所有订单
        List<Order> orderList = iOrderService.list(new QueryWrapper<Order>().eq("customer_id", customerId)
                .in("order_status", OrderConstant.FOUR_DONE,OrderConstant.FIVE_DONE)
                .ge("confirm_time", DateConverterUtil.getNumberStartMonthDay(number))
                .lt("confirm_time", DateConverterUtil.getNumberStartMonthDay(number+1)));
        if (orderList!=null && orderList.size()>0){
            for (Order order : orderList) {
                ProfitFlowing profitFlowing = iProfitFlowingService.getOne(new QueryWrapper<ProfitFlowing>().eq("order_sn", order.getOrderSn()));
                //将订单的个人收益加到消费预估收益中
                if (profitFlowing!=null){
                    totalPrice = totalPrice.add(profitFlowing.getCusMoney());
                }
            }
        }
        //查询该用户所推荐的下级用户列表
        List<CustomerInfo> customerInfoList = iCustomerInfoService.list(new QueryWrapper<CustomerInfo>().eq("parent_id", customerId));
        for (CustomerInfo customerInfo : customerInfoList) {
            //查询下级用户的所有订单信息
            List<Order> orderList1 = iOrderService.list(new QueryWrapper<Order>().eq("customer_id", customerInfo.getCustomerId())
                    .in("order_status", OrderConstant.FOUR_DONE,OrderConstant.FIVE_DONE)
                    .ge("confirm_time", DateConverterUtil.getStartMonthDay(LocalDateTime.now())));
            for (Order order : orderList1) {
                ProfitFlowing profitFlowing = iProfitFlowingService.getOne(new QueryWrapper<ProfitFlowing>().eq("order_sn", order.getOrderSn()));
                //将订单的个人收益加到消费预估收益中
                if (profitFlowing!=null){
                    totalPrice = totalPrice.add(profitFlowing.getCusMoney());
                }
            }
        }
        return totalPrice;
    }




}
