package com.tourism.hu.service.impl;

import com.tourism.hu.entity.ResidenceRoom;
import com.tourism.hu.mapper.ResidenceRoomMapper;
import com.tourism.hu.service.IResidenceRoomService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 民宿房间表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
@Service
public class ResidenceRoomServiceImpl extends ServiceImpl<ResidenceRoomMapper, ResidenceRoom> implements IResidenceRoomService {

}
