package com.tourism.hu.service.impl;

import com.tourism.hu.entity.WalletRecord;
import com.tourism.hu.mapper.WalletRecordMapper;
import com.tourism.hu.service.IWalletRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 钱包交易记录表 服务实现类
 * </p>
 *
 * @author hu
 * @since 2019-12-13
 */
@Service
public class WalletRecordServiceImpl extends ServiceImpl<WalletRecordMapper, WalletRecord> implements IWalletRecordService {

}
