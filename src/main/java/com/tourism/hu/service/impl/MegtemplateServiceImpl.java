package com.tourism.hu.service.impl;

import com.tourism.hu.entity.Megtemplate;
import com.tourism.hu.mapper.MegtemplateMapper;
import com.tourism.hu.service.IMegtemplateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class MegtemplateServiceImpl extends ServiceImpl<MegtemplateMapper, Megtemplate> implements IMegtemplateService {

}
