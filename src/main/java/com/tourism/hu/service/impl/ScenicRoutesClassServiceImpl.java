package com.tourism.hu.service.impl;

import com.tourism.hu.entity.ScenicRoutesClass;
import com.tourism.hu.mapper.ScenicRoutesClassMapper;
import com.tourism.hu.service.IScenicRoutesClassService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 旅游线路分类表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-24
 */
@Service
public class ScenicRoutesClassServiceImpl extends ServiceImpl<ScenicRoutesClassMapper, ScenicRoutesClass> implements IScenicRoutesClassService {

}
