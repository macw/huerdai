package com.tourism.hu.service.impl;

import com.tourism.hu.entity.StrategicComment;
import com.tourism.hu.mapper.StrategicCommentMapper;
import com.tourism.hu.service.IStrategicCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 攻略评论表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
@Service
public class StrategicCommentServiceImpl extends ServiceImpl<StrategicCommentMapper, StrategicComment> implements IStrategicCommentService {

}
