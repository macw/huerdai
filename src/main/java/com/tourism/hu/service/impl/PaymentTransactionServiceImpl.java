package com.tourism.hu.service.impl;

import com.tourism.hu.entity.PaymentTransaction;
import com.tourism.hu.mapper.PaymentTransactionMapper;
import com.tourism.hu.service.IPaymentTransactionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支付交易 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class PaymentTransactionServiceImpl extends ServiceImpl<PaymentTransactionMapper, PaymentTransaction> implements IPaymentTransactionService {

}
