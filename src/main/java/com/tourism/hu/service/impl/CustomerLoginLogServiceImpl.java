package com.tourism.hu.service.impl;

import com.tourism.hu.entity.CustomerLoginLog;
import com.tourism.hu.entity.dto.CustomerLoginLogDto;
import com.tourism.hu.mapper.CustomerLoginLogMapper;
import com.tourism.hu.service.ICustomerLoginLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 用户登陆日志表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class CustomerLoginLogServiceImpl extends ServiceImpl<CustomerLoginLogMapper, CustomerLoginLog> implements ICustomerLoginLogService {

	@Autowired
	private CustomerLoginLogMapper customerLoginLogMapper;
	
	@Override
	public List<CustomerLoginLogDto> selectCustomerLoginLogDto(CustomerLoginLogDto dto){
		return customerLoginLogMapper.selectCustomerLoginLogDto(dto);
	}

	public Long selectCustomerLoginLogCount(CustomerLoginLogDto dto) {
		return customerLoginLogMapper.selectCustomerLoginLogCount(dto);
	}
}
