package com.tourism.hu.service.impl;

import com.tourism.hu.entity.GoodsPic;
import com.tourism.hu.mapper.GoodsPicMapper;
import com.tourism.hu.service.IGoodsPicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 商品图片表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class GoodsPicServiceImpl extends ServiceImpl<GoodsPicMapper, GoodsPic> implements IGoodsPicService {

}
