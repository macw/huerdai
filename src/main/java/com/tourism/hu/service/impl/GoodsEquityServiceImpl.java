package com.tourism.hu.service.impl;

import com.tourism.hu.entity.GoodsEquity;
import com.tourism.hu.mapper.GoodsEquityMapper;
import com.tourism.hu.service.IGoodsEquityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-15
 */
@Service
public class GoodsEquityServiceImpl extends ServiceImpl<GoodsEquityMapper, GoodsEquity> implements IGoodsEquityService {

}
