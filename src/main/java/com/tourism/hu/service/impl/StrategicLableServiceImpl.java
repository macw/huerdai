package com.tourism.hu.service.impl;

import com.tourism.hu.entity.StrategicLable;
import com.tourism.hu.mapper.StrategicLableMapper;
import com.tourism.hu.service.IStrategicLableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hu
 * @since 2019-11-28
 */
@Service
public class StrategicLableServiceImpl extends ServiceImpl<StrategicLableMapper, StrategicLable> implements IStrategicLableService {

}
