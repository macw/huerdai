package com.tourism.hu.service.impl;

import com.tourism.hu.entity.WechatRegionCode;
import com.tourism.hu.mapper.WechatRegionCodeMapper;
import com.tourism.hu.service.IWechatRegionCodeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-19
 */
@Service
public class WechatRegionCodeServiceImpl extends ServiceImpl<WechatRegionCodeMapper, WechatRegionCode> implements IWechatRegionCodeService {

}
