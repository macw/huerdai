package com.tourism.hu.service.impl;

import com.tourism.hu.entity.PaymentTransactionLog;
import com.tourism.hu.mapper.PaymentTransactionLogMapper;
import com.tourism.hu.service.IPaymentTransactionLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支付交易日志表  服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class PaymentTransactionLogServiceImpl extends ServiceImpl<PaymentTransactionLogMapper, PaymentTransactionLog> implements IPaymentTransactionLogService {

}
