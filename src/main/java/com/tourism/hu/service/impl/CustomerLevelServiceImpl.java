package com.tourism.hu.service.impl;

import com.tourism.hu.entity.CustomerLevel;
import com.tourism.hu.mapper.CustomerLevelMapper;
import com.tourism.hu.service.ICustomerLevelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class CustomerLevelServiceImpl extends ServiceImpl<CustomerLevelMapper, CustomerLevel> implements ICustomerLevelService {

}
