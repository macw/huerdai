package com.tourism.hu.service.impl;

import com.tourism.hu.entity.StrategicTravelsClass;
import com.tourism.hu.mapper.StrategicTravelsClassMapper;
import com.tourism.hu.service.IStrategicTravelsClassService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 攻略游记分类表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
@Service
public class StrategicTravelsClassServiceImpl extends ServiceImpl<StrategicTravelsClassMapper, StrategicTravelsClass> implements IStrategicTravelsClassService {

}
