package com.tourism.hu.service.impl;

import com.tourism.hu.entity.Star;
import com.tourism.hu.mapper.StarMapper;
import com.tourism.hu.service.IStarService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hu
 * @since 2019-12-09
 */
@Service
public class StarServiceImpl extends ServiceImpl<StarMapper, Star> implements IStarService {

}
