package com.tourism.hu.service.impl;

import com.tourism.hu.entity.UserRole;
import com.tourism.hu.mapper.UserRoleMapper;
import com.tourism.hu.service.IUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-06
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

}
