package com.tourism.hu.service.impl;

import com.tourism.hu.entity.ScenicOrder;
import com.tourism.hu.mapper.ScenicOrderMapper;
import com.tourism.hu.service.IScenicOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 旅游线路订单表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class ScenicOrderServiceImpl extends ServiceImpl<ScenicOrderMapper, ScenicOrder> implements IScenicOrderService {

}
