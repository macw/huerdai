package com.tourism.hu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.GoodsCategoryConstant;
import com.tourism.hu.entity.GoodsCategory;
import com.tourism.hu.entity.TreeNode;
import com.tourism.hu.mapper.GoodsCategoryMapper;
import com.tourism.hu.service.IGoodsCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 商品分类表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class GoodsCategoryServiceImpl extends ServiceImpl<GoodsCategoryMapper, GoodsCategory> implements IGoodsCategoryService {

    @Resource
    private GoodsCategoryMapper goodsCategoryMapper;


    @Override
    public List<TreeNode> selectAll() {
//        List<TreeNode> treeNodeList = goodsCategoryMapper.selectAll();
      /* List<TreeNode> treeNodeList = new ArrayList<>();
        List<GoodsCategory> goodsCategoryList = goodsCategoryMapper.selectList(new QueryWrapper<GoodsCategory>().eq("class_id_level", GoodsCategoryConstant.ONE_LEVEL));
        //遍历一级菜单
        for (GoodsCategory goodsCategoryOne : goodsCategoryList) {
            TreeNode treeNode = new TreeNode();
            treeNode.setId(goodsCategoryOne.getClassId().toString());
            treeNode.setTitle(goodsCategoryOne.getClassName());
            //查询二级菜单的父id = 一级菜单的分类id
            List<GoodsCategory> goodsCategoryList2 = goodsCategoryMapper.selectList(new QueryWrapper<GoodsCategory>().eq("parent_id",goodsCategoryOne.getClassId()));
            //遍历二级菜单
            List<TreeNode> treeNodeList2 = new ArrayList<>();
            for (GoodsCategory twoCategory : goodsCategoryList2) {
                if (goodsCategoryOne.getClassId() == twoCategory.getParentId()){
                    TreeNode treeNode2 = new TreeNode();
                    treeNode2.setId(twoCategory.getClassId().toString());
                    treeNode2.setTitle(twoCategory.getClassName());
                    treeNodeList2.add(treeNode2);

                }
            }
            treeNode.setChildren(treeNodeList2);

        }*/
        List<TreeNode> treeNodeList = goodsCategoryMapper.selectAll();


        return treeNodeList;
    }
}
