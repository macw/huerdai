package com.tourism.hu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.Goods;
import com.tourism.hu.entity.GoodsComment;
import com.tourism.hu.entity.dto.GoodsCommentDto;
import com.tourism.hu.mapper.CustomerInfoMapper;
import com.tourism.hu.mapper.GoodsCommentMapper;
import com.tourism.hu.mapper.GoodsMapper;
import com.tourism.hu.service.IGoodsCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tourism.hu.util.DataUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 商品评论表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class GoodsCommentServiceImpl extends ServiceImpl<GoodsCommentMapper, GoodsComment> implements IGoodsCommentService {

    @Resource
    private GoodsCommentMapper goodsCommentMapper;

    @Resource
    private GoodsMapper goodsMapper;

    @Resource
    private CustomerInfoMapper customerInfoMapper;

    @Override
    public DataUtil selectAll(int page, int limit) {
        Page<GoodsComment> page1 = new Page<>(page,limit);
        IPage<GoodsComment> IPage = goodsCommentMapper.selectPage(page1, null);
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<GoodsComment> list = IPage.getRecords();

        List<GoodsCommentDto> goodsCommentDtoList = new ArrayList<>();

        BeanUtils.copyProperties(goodsCommentDtoList,list,GoodsCommentDto.class);

        System.out.println();



        for (GoodsComment goodsComment : list) {
            Goods goods = goodsMapper.selectOne(new QueryWrapper<Goods>().eq("goods_id", goodsComment.getGoodsId()));
            if (goods!=null){
                //goodsComment.setGoodsName(goods.getGoodsname());
            }
            //customerInfoMapper.selectOne(new QueryWrapper<CustomerInfo>().eq(""))
        }

        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(list);
        dataUtil.setCount(total);
        return dataUtil;
    }
}
