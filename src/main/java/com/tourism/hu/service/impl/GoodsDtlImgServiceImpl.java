package com.tourism.hu.service.impl;

import com.tourism.hu.entity.GoodsDtlImg;
import com.tourism.hu.mapper.GoodsDtlImgMapper;
import com.tourism.hu.service.IGoodsDtlImgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 规格描述表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-14
 */
@Service
public class GoodsDtlImgServiceImpl extends ServiceImpl<GoodsDtlImgMapper, GoodsDtlImg> implements IGoodsDtlImgService {

}
