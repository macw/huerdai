package com.tourism.hu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tourism.hu.constant.AuthorConstant;
import com.tourism.hu.constant.MenuConsant;
import com.tourism.hu.entity.*;
import com.tourism.hu.mapper.MenuMapper;
import com.tourism.hu.mapper.OperationMapper;
import com.tourism.hu.mapper.RoleMapper;
import com.tourism.hu.service.IAuthorService;
import com.tourism.hu.service.IMenuService;
import com.tourism.hu.service.IUserRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-04
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

    @Resource
    private IMenuService iMenuService;


    @Resource
    private RoleMapper roleMapper;

    @Resource
    private IAuthorService iAuthorService;

    @Resource
    private OperationMapper operationMapper;

    @Resource
    private IUserRoleService iUserRoleService;


    @Override
    public List<Menu> getMenu(Integer userId) {
        List<Menu> menuList = new ArrayList<>();
        //根据当前用户id 查询当前用户的角色
        List<UserRole> userRoleList = iUserRoleService.list(new QueryWrapper<UserRole>().eq("user_id", userId));
        for (UserRole userRole : userRoleList) {
            //根据用户角色信息查询出角色id
            Integer roleId = userRole.getRoleId();
            //根据角色id查询授权信息
            List<Author> authorList = iAuthorService.list(new QueryWrapper<Author>().eq("role_id", roleId).eq("resource_type", AuthorConstant.MENU_RESOURCE_TYPE));
            //根据授权信息查询资源（菜单）id等数据
            for (Author author : authorList) {
                //根据资源id查询出菜单对象
                Menu menu = iMenuService.getById(author.getResourceId());
                //将菜单对象添加到返回结果集中
                menuList.add(menu);
            }
        }
        //处理菜单返回结果集，封装一级 菜单集合
        List<Menu> menus = new ArrayList<>();

        if (menuList!=null && menuList.size()>0) {
            for (Menu menu : menuList) {
                if (MenuConsant.ONE_LEVEL == menu.getLevel()) {
                    menus.add(menu);
                }
            }
            //处理二级结果集
            //遍历一级结果集
            for (Menu menu : menus) {
                //再次从总结果集中抽出资源
                List<Menu> list2 = new ArrayList<>();
                for (Menu menu1 : menuList) {
                    if (menu1.getParentId() == menu.getMenuId()) {
                        //将二级结果集放进去
                        list2.add(menu1);
                    }
                }
                if (list2 != null && list2.size() > 0) {
                    menu.setMenuList(list2);
                }
            }
        }
        return menus;
    }
}
