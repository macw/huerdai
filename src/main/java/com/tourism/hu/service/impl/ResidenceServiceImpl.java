package com.tourism.hu.service.impl;

import com.tourism.hu.entity.Residence;
import com.tourism.hu.mapper.ResidenceMapper;
import com.tourism.hu.service.IResidenceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 民宿表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
@Service
public class ResidenceServiceImpl extends ServiceImpl<ResidenceMapper, Residence> implements IResidenceService {

}
