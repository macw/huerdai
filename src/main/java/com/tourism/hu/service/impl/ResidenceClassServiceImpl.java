package com.tourism.hu.service.impl;

import com.tourism.hu.entity.ResidenceClass;
import com.tourism.hu.mapper.ResidenceClassMapper;
import com.tourism.hu.service.IResidenceClassService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 民宿分类表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
@Service
public class ResidenceClassServiceImpl extends ServiceImpl<ResidenceClassMapper, ResidenceClass> implements IResidenceClassService {

}
