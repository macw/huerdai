package com.tourism.hu.service.impl;

import com.tourism.hu.entity.ScenicRoutes;
import com.tourism.hu.mapper.ScenicRoutesMapper;
import com.tourism.hu.service.IScenicRoutesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 旅游线路表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class ScenicRoutesServiceImpl extends ServiceImpl<ScenicRoutesMapper, ScenicRoutes> implements IScenicRoutesService {

}
