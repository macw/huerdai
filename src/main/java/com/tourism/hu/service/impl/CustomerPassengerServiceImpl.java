package com.tourism.hu.service.impl;

import com.tourism.hu.entity.CustomerPassenger;
import com.tourism.hu.mapper.CustomerPassengerMapper;
import com.tourism.hu.service.ICustomerPassengerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户乘车人信息表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-12
 */
@Service
public class CustomerPassengerServiceImpl extends ServiceImpl<CustomerPassengerMapper, CustomerPassenger> implements ICustomerPassengerService {

}
