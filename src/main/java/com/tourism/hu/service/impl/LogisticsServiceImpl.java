package com.tourism.hu.service.impl;

import com.tourism.hu.entity.Logistics;
import com.tourism.hu.entity.dto.LogisticsDto;
import com.tourism.hu.mapper.LogisticsMapper;
import com.tourism.hu.service.ILogisticsService;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 物流信息表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class LogisticsServiceImpl extends ServiceImpl<LogisticsMapper, Logistics> implements ILogisticsService {

	@Autowired
	private LogisticsMapper logisticsMapper;

	public List<LogisticsDto> selectPageDate(LogisticsDto dto) {
		return logisticsMapper.selectPageDataList(dto);
	}

	public Long getCount(LogisticsDto dto) {
		return logisticsMapper.getCount(dto);
	}

}
