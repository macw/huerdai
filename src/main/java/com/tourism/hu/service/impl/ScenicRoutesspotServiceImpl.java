package com.tourism.hu.service.impl;

import com.tourism.hu.entity.ScenicRoutesspot;
import com.tourism.hu.mapper.ScenicRoutesspotMapper;
import com.tourism.hu.service.IScenicRoutesspotService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 线路景点关联表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-25
 */
@Service
public class ScenicRoutesspotServiceImpl extends ServiceImpl<ScenicRoutesspotMapper, ScenicRoutesspot> implements IScenicRoutesspotService {

}
