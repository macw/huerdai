package com.tourism.hu.service.impl;

import com.tourism.hu.entity.CustomerFriend;
import com.tourism.hu.mapper.CustomerFriendMapper;
import com.tourism.hu.service.ICustomerFriendService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class CustomerFriendServiceImpl extends ServiceImpl<CustomerFriendMapper, CustomerFriend> implements ICustomerFriendService {

}
