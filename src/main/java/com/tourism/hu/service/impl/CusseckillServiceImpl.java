package com.tourism.hu.service.impl;

import com.tourism.hu.entity.Cusseckill;
import com.tourism.hu.mapper.CusseckillMapper;
import com.tourism.hu.service.ICusseckillService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员秒杀表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-29
 */
@Service
public class CusseckillServiceImpl extends ServiceImpl<CusseckillMapper, Cusseckill> implements ICusseckillService {

}
