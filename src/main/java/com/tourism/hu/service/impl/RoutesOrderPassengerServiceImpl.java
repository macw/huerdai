package com.tourism.hu.service.impl;

import com.tourism.hu.entity.RoutesOrderPassenger;
import com.tourism.hu.mapper.RoutesOrderPassengerMapper;
import com.tourism.hu.service.IRoutesOrderPassengerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 旅游订单乘车人信息表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-12
 */
@Service
public class RoutesOrderPassengerServiceImpl extends ServiceImpl<RoutesOrderPassengerMapper, RoutesOrderPassenger> implements IRoutesOrderPassengerService {

}
