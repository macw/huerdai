package com.tourism.hu.service.impl;

import com.tourism.hu.entity.GoodsclassPic;
import com.tourism.hu.mapper.GoodsclassPicMapper;
import com.tourism.hu.service.IGoodsclassPicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 商品商品分类图片表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@Service
public class GoodsclassPicServiceImpl extends ServiceImpl<GoodsclassPicMapper, GoodsclassPic> implements IGoodsclassPicService {

}
