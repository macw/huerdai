package com.tourism.hu.service;

import com.tourism.hu.entity.StrategicTravelsClass;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 攻略游记分类表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface IStrategicTravelsClassService extends IService<StrategicTravelsClass> {

}
