package com.tourism.hu.service;

import com.tourism.hu.entity.WalletRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 钱包交易记录表 服务类
 * </p>
 *
 * @author hu
 * @since 2019-12-13
 */
public interface IWalletRecordService extends IService<WalletRecord> {

}
