package com.tourism.hu.service;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tourism.hu.entity.Logistics;
import com.tourism.hu.entity.dto.LogisticsDto;

/**
 * <p>
 * 物流信息表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface ILogisticsService extends IService<Logistics> {

	 List<LogisticsDto> selectPageDate(LogisticsDto dto);
	 
	 Long getCount(LogisticsDto dto);
}
