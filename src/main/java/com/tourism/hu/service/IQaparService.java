package com.tourism.hu.service;

import com.tourism.hu.entity.Qapar;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 凭证表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface IQaparService extends IService<Qapar> {

}
