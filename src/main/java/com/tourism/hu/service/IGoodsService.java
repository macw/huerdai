package com.tourism.hu.service;

import com.tourism.hu.entity.Goods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品信息表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface IGoodsService extends IService<Goods> {

}
