package com.tourism.hu.service;

import com.tourism.hu.entity.Smstest;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 验证码登录表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface ISmstestService extends IService<Smstest> {

}
