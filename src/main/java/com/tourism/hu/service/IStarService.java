package com.tourism.hu.service;

import com.tourism.hu.entity.Star;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hu
 * @since 2019-12-09
 */
public interface IStarService extends IService<Star> {

}
