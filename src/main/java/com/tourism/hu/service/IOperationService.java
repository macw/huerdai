package com.tourism.hu.service;

import com.tourism.hu.entity.Operation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-06
 */
public interface IOperationService extends IService<Operation> {

}
