package com.tourism.hu.service;

import com.tourism.hu.entity.StrategicTravels;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 攻略游记表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface IStrategicTravelsService extends IService<StrategicTravels> {

}
