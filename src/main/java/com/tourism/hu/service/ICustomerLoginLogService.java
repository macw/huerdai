package com.tourism.hu.service;

import com.tourism.hu.entity.CustomerLoginLog;
import com.tourism.hu.entity.dto.CustomerLoginLogDto;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户登陆日志表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface ICustomerLoginLogService extends IService<CustomerLoginLog> {

	List<CustomerLoginLogDto> selectCustomerLoginLogDto(CustomerLoginLogDto dto);

	Long selectCustomerLoginLogCount(CustomerLoginLogDto dto);
}
