package com.tourism.hu.service;

import com.tourism.hu.entity.ResidenceRoom;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 民宿房间表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface IResidenceRoomService extends IService<ResidenceRoom> {

}
