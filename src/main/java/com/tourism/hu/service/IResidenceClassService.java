package com.tourism.hu.service;

import com.tourism.hu.entity.ResidenceClass;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 民宿分类表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface IResidenceClassService extends IService<ResidenceClass> {

}
