package com.tourism.hu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tourism.hu.entity.HmFunctionstree;
import com.tourism.hu.entity.Role;

import java.util.List;
import java.util.Set;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-06
 */
public interface IRoleService extends IService<Role> {

    /**
     * 根据用户名查询该用户所有的角色信息
     * @param username
     * @return
     */
    Set<String> getAllRolesByUsername(String username);

    /**
     * 根据用户名查询该用户拥有的所有权限
     * @param username
     * @return
     */
    Set<String> getAllPermissionsByUsername(String username);

    /**
     * 根据角色名查询角色信息和授权信息
     * @param roleName
     * @return
     */
    List<HmFunctionstree> selectEleTreeByRoleName(String roleName);

    /**
     * 添加管理员角色
     * @param role
     * @return
     */
    int addRole(Role role);

    /**
     * 根据角色id删除角色信息和授权信息
     * @param roleId
     * @return
     */
    int deleteRole(Integer roleId);

    int deleteMAny(List list);

}
