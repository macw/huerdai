package com.tourism.hu.service;

import com.tourism.hu.entity.GoodsDtlImg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 规格描述表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-14
 */
public interface IGoodsDtlImgService extends IService<GoodsDtlImg> {

}
