package com.tourism.hu.service;

import com.tourism.hu.entity.UploadFile;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 上传文件记录表 服务类
 * </p>
 *
 * @author hu
 * @since 2019-12-04
 */
public interface IUploadFileService extends IService<UploadFile> {

}
