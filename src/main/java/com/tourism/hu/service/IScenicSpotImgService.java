package com.tourism.hu.service;

import com.tourism.hu.entity.ScenicSpotImg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 旅游景点图片表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-31
 */
public interface IScenicSpotImgService extends IService<ScenicSpotImg> {

}
