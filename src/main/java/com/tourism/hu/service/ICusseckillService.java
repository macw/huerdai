package com.tourism.hu.service;

import com.tourism.hu.entity.Cusseckill;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员秒杀表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-29
 */
public interface ICusseckillService extends IService<Cusseckill> {

}
