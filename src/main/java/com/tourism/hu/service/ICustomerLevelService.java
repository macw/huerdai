package com.tourism.hu.service;

import com.tourism.hu.entity.CustomerLevel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface ICustomerLevelService extends IService<CustomerLevel> {

}
