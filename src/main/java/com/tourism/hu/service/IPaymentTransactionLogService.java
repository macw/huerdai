package com.tourism.hu.service;

import com.tourism.hu.entity.PaymentTransactionLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 支付交易日志表  服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface IPaymentTransactionLogService extends IService<PaymentTransactionLog> {

}
