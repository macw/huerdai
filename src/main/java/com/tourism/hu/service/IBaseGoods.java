package com.tourism.hu.service;

import com.tourism.hu.entity.vo.GoodsDtlVo;
import com.tourism.hu.util.ResponseResult;

import java.math.BigDecimal;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 11:48
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
public interface IBaseGoods {

    /**
     * 根据活动价，商品单价，判断实际价格
     * 如果活动价为空，返回商品单价，
     * 有活动价，返回活动价
     * @param getPrice
     * @param getActivityPrice
     * @return
     */
    BigDecimal getActualPrice(BigDecimal getPrice,BigDecimal getActivityPrice);

    GoodsDtlVo getGoodsDtlVo(Integer goodsId, Integer[] dtlIdArray);

}
