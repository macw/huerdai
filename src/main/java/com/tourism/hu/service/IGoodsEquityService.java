package com.tourism.hu.service;

import com.tourism.hu.entity.GoodsEquity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-15
 */
public interface IGoodsEquityService extends IService<GoodsEquity> {

}
