package com.tourism.hu.service;

import com.tourism.hu.entity.Seckill;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tourism.hu.entity.dto.SeckillDto;

import java.util.List;

/**
 * <p>
 * 秒杀表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface ISeckillService extends IService<Seckill> {


}
