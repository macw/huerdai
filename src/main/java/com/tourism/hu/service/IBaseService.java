package com.tourism.hu.service;

import com.tourism.hu.entity.CustomerInfo;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 13:44
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */

public interface IBaseService {

    /**
     * 获取Request对象
     * @return
     */
    HttpServletRequest getRequest();

    /**
     * 获取前台登录用户
     * @return
     */
    CustomerInfo getCustomerInfo();

    /**
     * 是否为VIP， true，VIP 。
     * false 普通用户
     * @return
     */
    boolean isVIP();

    /**
     * 用户对于某个商品的省钱价格
     * @return
     */
    BigDecimal getPercentage(BigDecimal price, BigDecimal exitPrice, BigDecimal activePrice);

    /**
     *
     * @param status 1 旅游商品 2  商场商品
     * @param price	 销售价   （销售价  活动 必选一个）
     * @param exitPrice  底价   （不能为空）
     * @param activePrice 活动价
     * @param userType
     * 返佣类型
     * AGENT_TYPE = 1 代理商分成 百分比 int
     * USER_TYPE = 2; 用户分成
     * PLAT_TYPE = 3; 平台分成
     * PARE_TYPE = 4; 上级分成
     * @return
     */
    BigDecimal getPercentage(Integer status,BigDecimal price,  BigDecimal exitPrice,  BigDecimal activePrice,Integer userType);

    /**
     * 计算利润
     * 商品单价，低价(出厂价)、活动价
     * 计算方法：
     * 如果有活动价，就活动价减去低价，
     * 没有活动价就商品单价减去低价
     *
     * @return
     */
    BigDecimal getProfits( BigDecimal price,  BigDecimal exitPrice,  BigDecimal activePrice);
}
