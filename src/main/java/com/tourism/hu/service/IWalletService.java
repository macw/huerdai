package com.tourism.hu.service;

import com.tourism.hu.entity.Wallet;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;

/**
 * <p>
 * 用户钱包 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-20
 */
public interface IWalletService extends IService<Wallet> {
    /**
     * 获取总订单数和总成交预估收益
     * @param customerId
     * @return
     */
    Wallet selectGoodsOrderProfit(Integer customerId);

    /**
     * 获取当前用户某的 消费预估收入
     * @param customerId
     * @param number
     * number==0: 为本月
     * number==-1：为上个月
     * number==1，下个月
     *
     * @return
     */
    BigDecimal getLocalMonthConsume(Integer customerId, Integer number);



}
