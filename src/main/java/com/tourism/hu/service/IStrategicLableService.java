package com.tourism.hu.service;

import com.tourism.hu.entity.StrategicLable;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hu
 * @since 2019-11-28
 */
public interface IStrategicLableService extends IService<StrategicLable> {

}
