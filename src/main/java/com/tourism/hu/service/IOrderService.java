package com.tourism.hu.service;

import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.Goods;
import com.tourism.hu.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tourism.hu.entity.ProfitFlowing;
import com.tourism.hu.entity.vo.GoodsDtlVo;

/**
 * <p>
 * 订单主表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface IOrderService extends IService<Order> {

    /**
     * 商品，创建订单
     * @param order
     * @param customerInfo
     * @param addressId
     * @return
     */
    boolean createOrder(Order order, CustomerInfo customerInfo,  Integer addressId,ProfitFlowing profitFlowing);

}
