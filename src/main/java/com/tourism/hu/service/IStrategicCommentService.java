package com.tourism.hu.service;

import com.tourism.hu.entity.StrategicComment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 攻略评论表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-10-11
 */
public interface IStrategicCommentService extends IService<StrategicComment> {

}
