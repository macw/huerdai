package com.tourism.hu.service;

import com.tourism.hu.entity.Coupon;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-24
 */
public interface ICouponService extends IService<Coupon> {

}
