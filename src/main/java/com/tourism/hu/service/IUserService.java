package com.tourism.hu.service;

import com.tourism.hu.entity.Role;
import com.tourism.hu.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import com.tourism.hu.util.ResultUtil;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-05
 */
public interface IUserService extends IService<User> {

    /**
     * 分页查询， 所有用户
     * @param page
     * @param limit
     * @param name
     * @return
     */
    DataUtil selectAll(int page, int limit, String name);

    DataUtil selectAgentAll(int page, int limit, String name,User user);

    /**
     * 查询单个后台用户
     * @param aid
     * @return
     */
    User selectOne(Integer aid);

    /**
     * 执行更新或者添加操作，  根据传入的user对象有无userid
     * @param user
     * @param loginUser
     * @return
     */
    ResultUtil updateOrSave(User user,User loginUser);


    /**
     * 跳转到编辑页面
     * @param userId
     * @return
     */
    ModelAndView toEditUser(Integer userId);

    /**
     * 删除单个
     * @param aid
     * @return
     */
    MsgUtil deleteOne(Integer aid);

    /**
     * 批量删除
     * @param ids
     * @return
     */
    MsgUtil delectMany(Integer[] ids);

}
