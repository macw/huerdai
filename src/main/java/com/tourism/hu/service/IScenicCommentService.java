package com.tourism.hu.service;

import com.tourism.hu.entity.ScenicComment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 线路评论表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-05
 */
public interface IScenicCommentService extends IService<ScenicComment> {

}
