package com.tourism.hu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tourism.hu.entity.Menu;
import com.tourism.hu.entity.MenuAndBtn;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-04
 */
public interface IMenuService extends IService<Menu> {

    List<Menu> getMenu(Integer userId);




}
