package com.tourism.hu.service;

import com.tourism.hu.entity.CustomerPassenger;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户乘车人信息表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-12
 */
public interface ICustomerPassengerService extends IService<CustomerPassenger> {

}
