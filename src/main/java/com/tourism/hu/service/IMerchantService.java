package com.tourism.hu.service;

import com.tourism.hu.entity.Merchant;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商户表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface IMerchantService extends IService<Merchant> {

}
