package com.tourism.hu.service;

import com.tourism.hu.entity.QaparBusiness;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 
 * @ClassName: IQaparBusinessService 
 * @Description: (物流商家service) 
 * @author 董兴隆
 * @date 2019年9月30日 下午3:38:14 
 *
 */
public interface IQaparBusinessService extends IService<QaparBusiness> {

}
