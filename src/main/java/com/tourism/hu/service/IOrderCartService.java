package com.tourism.hu.service;

import com.tourism.hu.entity.OrderCart;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 购物车表 服务类
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
public interface IOrderCartService extends IService<OrderCart> {

}
