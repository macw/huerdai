package com.tourism.fzll.controller;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.fzll.entity.Residence;
import com.tourism.fzll.entity.ResidenceRoom;
import com.tourism.fzll.entity.User;
import com.tourism.fzll.entity.dto.ResidenceRoomDto;
import com.tourism.fzll.mapper.ResidenceMapper;
import com.tourism.fzll.mapper.ResidenceRoomMapper;
import com.tourism.fzll.service.IResidenceRoomService;
import com.tourism.fzll.service.IResidenceService;
import com.tourism.fzll.util.PageUtil;
import com.tourism.fzll.util.ResultUtil;

/**
 * @ClassName: ResidenceRoomController 
 * @Description: (民宿房间) 
 * @author 董兴隆
 * @date 2019年10月17日 下午5:16:53 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/residence/room")
public class ResidenceRoomController extends BaseController{

	
	@Autowired
	private ResidenceRoomMapper residenceRoomMapper;
	
	@Autowired
	private IResidenceRoomService residenceRoomService;
	
	@Autowired
	private IResidenceService residenceService;
	
	/**
	 * 
	 * @Title: page
	 * @Description: (民宿房间页面)
	 * @return
	 * @date 2019年10月17日 下午3:54:22
	 * @author 董兴隆
	 */
	@RequestMapping("/page")
	public ModelAndView page() {
		return new ModelAndView("/residence/room/residenceRoomList");
	}

	/**
	 * 
	 * @Title: editPage
	 * @Description: (民宿房间编辑页面)
	 * @param dto
	 * @return
	 * @date 2019年10月17日 下午3:55:02
	 * @author 董兴隆
	 */
	@RequestMapping("/editPage")
	public ModelAndView editPage(ResidenceRoom dto) {
		ModelAndView mv = new ModelAndView("/residence/room/residenceRoomEdit");
		//启用状态下的民宿列表
		List<Residence> residenceList = residenceService.list(new QueryWrapper<Residence>().eq("status",1)); 
		mv.addObject("residenceList", residenceList);
		if (dto.getResidenceroomId() != null && dto.getResidenceroomId() > 0) {
			 dto = residenceRoomService.getById(dto.getResidenceroomId());
		}
		mv.addObject("dto", dto);
		return mv;
	}
	@RequestMapping("/roomInfo")
	public ModelAndView roomInfo(ResidenceRoom dto) {
		ModelAndView mv = new ModelAndView("/residence/room/residenceRoomInfo");
		if (dto.getResidenceroomId() != null && dto.getResidenceroomId() > 0) {
			dto = residenceRoomService.getById(dto.getResidenceroomId());
		}
		mv.addObject("dto", dto);
		return mv;
	}

	/**
	 * 
	 * @Title: selectData
	 * @Description: (民宿房间获取数据接口)
	 * @param dto
	 * @return
	 * @date 2019年10月17日 下午3:55:17
	 * @author 董兴隆
	 */
	@RequestMapping("/selectData")
	public ResultUtil<List<ResidenceRoomDto>> selectData(ResidenceRoomDto dto) {
		List<ResidenceRoomDto> list = new ArrayList<>();
		ResultUtil<List<ResidenceRoomDto>> result = ResultUtil.error("查询失败", list);
		PageUtil.initAttr(dto);
		try {
			long count = residenceRoomMapper.getCount(dto);
			if(count>0) {
				list = residenceRoomMapper.selectPageDataList(dto);
			}
			result = ResultUtil.ok("查询成功", list);
			result.setCount(count);
		} catch (Exception e) {
			result.setMsg(e.getMessage());
		}
		result.setData(list);
		return result;
	}

	/**
	 * 
	 * @Title: saveOrUpdate 
	 * @Description: (添加或者修改接口) 
	 * @param res
	 * @return  
	 * @date 2019年10月17日 下午4:07:24
	 * @author 董兴隆
	 */
	@RequestMapping("/saveOrUpdate")
	@ResponseBody
	public ResultUtil<ResidenceRoom> saveOrUpdate(ResidenceRoom res) {
		User user = getUser();
		if (isNotLogIn()) {
			return ResultUtil.error("未找到登录用户");
		}
		boolean save = true;
		if (res.getResidenceroomId() != null && res.getResidenceroomId() > 0) {
			res.setLastUpdateDate(LocalDateTime.now());
			res.setLastUpdatedId(user.getUserId());
			res.setLastUpdateName(user.getName());
			save = false;
		} else {
			res.setCreatedId(user.getUserId());
			res.setCreatedName(user.getName());
			res.setCreationDate(LocalDateTime.now());
			res.setStatus(1);
		}
		if (residenceRoomService.saveOrUpdate(res)) {
			if (save) {
				return ResultUtil.ok("添加成功");
			} else {
				return ResultUtil.ok("修改成功");
			}
		} else {
			if (save) {
				return ResultUtil.error("添加失败");
			} else {
				return ResultUtil.error("修改失败");
			}
		}
	}

	/**
	 * 
	 * @Title: updateYnByResidenceRoomId 
	 * @Description: (修改分类可用状态) 
	 * @param ins
	 * @return  
	 * @date 2019年10月17日 下午4:07:41
	 * @author 董兴隆
	 */
	@RequestMapping("/updateYnByResidenceRoomId")
	@ResponseBody
	public ResultUtil<ResidenceRoom> updateYnByResidenceRoomId(ResidenceRoom ins) {

		User user = getUser();
		if (user == null || user.getUserId() == null) {
			return ResultUtil.error("未找到登录用户");
		}
		ResidenceRoom ResidenceRoom = residenceRoomService.getById(ins.getResidenceroomId());
		ResidenceRoom.setStatus(ResidenceRoom.getStatus() == 1 ? 2 : 1);
		if (residenceRoomService.saveOrUpdate(ResidenceRoom)) {
			return ResultUtil.ok("修改成功");
		} else {
			return ResultUtil.error("修改失败");
		}
	}

	/**
	 * 
	 * @Title: deleteByResidenceRoomId 
	 * @Description: (删除民宿分类) 
	 * @param ins
	 * @return  
	 * @date 2019年10月17日 下午4:08:08
	 * @author 董兴隆
	 */
	@RequestMapping("/deleteByResidenceRoomId")
	@ResponseBody
	public ResultUtil<ResidenceRoom> deleteByResidenceRoomId(ResidenceRoom ins) {
		User user = getUser();
		if (user == null || user.getUserId() == null) {
			return ResultUtil.error("未找到登录用户");
		}
		ins.setStatus(3);// 删除
		ins.setLastUpdateDate(LocalDateTime.now());
		ins.setLastUpdatedId(user.getUserId());
		ins.setLastUpdateName(user.getName());
		if (residenceRoomService.updateById(ins)) {
			return ResultUtil.ok("删除成功");
		} else {
			return ResultUtil.error("删除失败");
		}
	}
	
	
	
}
