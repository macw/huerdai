package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.*;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.*;
import com.tourism.hu.entity.vo.GoodsOrderDetailsVo;
import com.tourism.hu.entity.vo.OrderStatusVo;
import com.tourism.hu.mapper.AddressMapper;
import com.tourism.hu.mapper.GoodsMapper;
import com.tourism.hu.mapper.OrderMapper;
import com.tourism.hu.mapper.SeckillMapper;
import com.tourism.hu.service.*;
import com.tourism.hu.util.OrderCodeUtil;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 董兴隆
 * @ClassName: OrderApiController
 * @Description: ()
 * @date 2019年11月11日 下午5:02:34
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/api/orderApi")
public class OrderApiController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private OrderMapper orderMapper;

    @Resource
    private IOrderService iOrderService;

    @Resource
    private IOrderDetailService iOrderDetailService;

    @Resource
    private AddressMapper addressMapper;

    @Resource
    private IAddressService iAddressService;

    @Resource
    private GoodsMapper goodsMapper;

    @Resource
    private IGoodsService iGoodsService;

    @Resource
    private IGoodsPicService iGoodsPicService;

    @Resource
    private SeckillMapper seckillMapper;

    @Resource
    private IProfitFlowingService iProfitFlowingService;

    @Resource
    private IMerchantService iMerchantService;

    @Resource
    private IGoodsEquityService iGoodsEquityService;

    @Resource
    private IEquityService iEquityService;

    /**
     * 获取用户个人中心 订单，待付款、带发货，待收货，带评价，退款中个数统计接口
     */
    @RequestMapping("/getOrderNumberByCustomer")
    public void getOrderNumberByCustomer() {
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录"));
            return;
        }
        try {
            Integer customerId = customerInfo.getCustomerId();
            Map<String, Object> map = new HashMap();
//            Map<String, Map<String, Object>> statusMap = new HashMap();
            List<OrderStatusVo> voList = new ArrayList<>();
            //获取用户个人中心 订单，待付款数量
            List<Order> order_status = orderMapper.selectList(new QueryWrapper<Order>().eq("customer_id", customerId).eq("order_status", OrderConstant.ONE_NO_PAY));
            if (order_status != null) {
                OrderStatusVo vo = new OrderStatusVo("待付款", order_status.size(), BaseConstant.OS_PATH + "api/orderApi/selectOrderByStatus/" + 1, null);
                voList.add(vo);
            }
            //获取用户个人中心 订单，待发货数量
            List<Order> order_status2 = orderMapper.selectList(new QueryWrapper<Order>().eq("customer_id", customerId).eq("order_status", OrderConstant.TWO_YES_PAY));
            if (order_status2 != null) {
                OrderStatusVo vo = new OrderStatusVo("待发货", order_status2.size(), BaseConstant.OS_PATH + "api/orderApi/selectOrderByStatus/" + 2, null);
                voList.add(vo);
            }
            //获取用户，待使用数量
            List<Order> orders21 = orderMapper.selectList(new QueryWrapper<Order>().eq("customer_id", customerId).eq("order_status", OrderConstant.TWOONE_NO_USE));
            if ((orders21 != null)) {
                OrderStatusVo vo = new OrderStatusVo("待使用", orders21.size(), BaseConstant.OS_PATH + "api/orderApi/selectOrderByStatus/" + 21, null);
                voList.add(vo);
            }
            //获取用户个人中心 订单，待收货数量
            List<Order> order_status3 = orderMapper.selectList(new QueryWrapper<Order>().eq("customer_id", customerId).eq("order_status", OrderConstant.FOUR_DONE));
            if (order_status3 != null) {
                OrderStatusVo vo = new OrderStatusVo("待收货", order_status3.size(), BaseConstant.OS_PATH + "api/orderApi/selectOrderByStatus/" + 3, null);
                voList.add(vo);
            }
            //获取用户个人中心 订单，待评价数量
            List<Order> order_status4 = orderMapper.selectList(new QueryWrapper<Order>().eq("customer_id", customerId).eq("order_status", OrderConstant.THREE_SHIPPING));
            if (order_status4 != null) {
                OrderStatusVo vo = new OrderStatusVo("待评价", order_status4.size(), BaseConstant.OS_PATH + "api/orderApi/selectOrderByStatus/" + 4, null);
                voList.add(vo);
            }
            //获取用户个人中心 订单，退款中数量
            List<Order> order_status5 = orderMapper.selectList(new QueryWrapper<Order>().eq("customer_id", customerId).eq("order_status", OrderConstant.FIVE_EXIT));
            if (order_status5 != null) {
                OrderStatusVo vo = new OrderStatusVo("退款中", order_status5.size(), BaseConstant.OS_PATH + "api/orderApi/selectOrderByStatus/" + 5, null);
                voList.add(vo);
            }
            ResponseResult<List<OrderStatusVo>> result = new ResponseResult<>();
            result.setData(voList);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("customerinfo===" + customerInfo.toString());
            logger.debug("Exception===" + e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
        }

    }

    /**
     * 查询订单页面下的商品信息
     * <p>
     * 问题：
     * 商户不需要头像
     *
     * @param goodsId    商品主键id
     * @param type       1,单独购买，2拼团购买，3秒杀
     * @param dtlIdArray 理论上，dtlIdArray应不能为空，格式例如： 2,5
     */
    @RequestMapping("/selectOrderGoodsDetails")
    public void selectOrderGoodsDetails(Integer goodsId, Integer type, Integer[] dtlIdArray) {
        logger.debug("" + goodsId);
        logger.debug("dtlIdArray===" + dtlIdArray);
        try {
            GoodsOrderDetailsVo goodsOrderDetails = null;
            if (dtlIdArray != null) {
                List list = new ArrayList();
                for (Integer id : dtlIdArray) {
                    list.add(id);
                }
                goodsOrderDetails = orderMapper.selectGoodsOrderDetails(goodsId, list);
            }

            /**
             * 价格计算
             *  如果有活动价，则将活动价赋值为销售价，再将活动价社设为0，
             *  只返回一个销售价 price
             */
            if (goodsOrderDetails.getActivityPrice() != null && goodsOrderDetails.getActivityPrice().compareTo(BigDecimal.ZERO) == 1) {
                goodsOrderDetails.setPrice(goodsOrderDetails.getActivityPrice());
            }
            //再判断是否是拼团或者秒杀，是的话重新赋值
            if (type == 2) {
                Seckill seckill = seckillMapper.selectOne(new QueryWrapper<Seckill>().ge("collage_count", 2).eq("type", SeckillConstant.GOODSID).eq("goods_id", goodsId).eq("status", BaseConstant.USE_STATUS));
                goodsOrderDetails.setPrice(seckill.getSeckillPrice());
            } else if (type == 3) {
                /**
                 * 这个会有问题,如果一个商品有多个秒杀信息会报错
                 */
                //  Seckill seckill = seckillMapper.selectOne(new QueryWrapper<Seckill>().eq("collage_count", 0).eq("type", SeckillConstant.GOODSID).eq("goods_id", goodsId).eq("status", BaseConstant.USE_STATUS));
                //goodsOrderDetails.setPrice(seckill.getSeckillPrice());
            }
            //将活动价 和出厂价设为 0
            goodsOrderDetails.setActivityPrice(BigDecimal.ZERO);
            goodsOrderDetails.setExitPrice(BigDecimal.ZERO);

            /**
             * 根据 商品价格
             * 计算 加规格之后的 价格
             */
            List<GoodsDtl> goodsDtlList = goodsOrderDetails.getGoodsDtlList();
            for (GoodsDtl goodsDtl : goodsDtlList) {
                BigDecimal subtract = goodsOrderDetails.getPrice().add(goodsDtl.getDtlPrice());
                goodsOrderDetails.setPrice(subtract);
            }

            //设置 商品信息的 权益
            List<Equity> list = new ArrayList<>();
            List<GoodsEquity> equityList = iGoodsEquityService.list(new QueryWrapper<GoodsEquity>().eq("goods_id", goodsId));
            for (GoodsEquity goodsEquity : equityList) {
                Equity equity = iEquityService.getOne(new QueryWrapper<Equity>().eq("equity_id", goodsEquity.getEquityId()).eq("status", BaseConstant.USE_STATUS));
                list.add(equity);
            }
            goodsOrderDetails.setEquityList(list);

            ResponseResult<GoodsOrderDetailsVo> result = new ResponseResult<>();
            result.setData(goodsOrderDetails);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
        }
    }

    /**
     * 查询订单页面下的商品信息
     * 没有商品规格信息的版本
     *
     * @param goodsId
     * @param type    1,单独购买，2拼团购买，3秒杀
     */
    @RequestMapping("/selectOrderGoodsDetailsNoDtl")
    public void selectOrderGoodsDetailsNoDtl(Integer goodsId, Integer type) {
        logger.debug("" + goodsId);

        try {
            GoodsOrderDetailsVo goodsOrderDetails = orderMapper.selectGoodsOrderDetailsNoDtl(goodsId);
            //如果有活动价，则将活动价赋值为销售价，再将活动价社设为0，
            //只返回一个销售价
            if (goodsOrderDetails.getActivityPrice() != null && goodsOrderDetails.getActivityPrice().compareTo(BigDecimal.ZERO) == 1) {
                goodsOrderDetails.setPrice(goodsOrderDetails.getActivityPrice());
            }
            //再判断是否是拼团或者秒杀，是的话重新赋值
            if (type == 2) {
                Seckill seckill = seckillMapper.selectOne(new QueryWrapper<Seckill>().ge("collage_count", 2).eq("type", SeckillConstant.GOODSID).eq("goods_id", goodsId).eq("status", BaseConstant.USE_STATUS));
                goodsOrderDetails.setPrice(seckill.getSeckillPrice());
            } else if (type == 3) {
                /**
                 * 这个会有问题,如果一个商品有多个秒杀信息会报错
                 */
                //  Seckill seckill = seckillMapper.selectOne(new QueryWrapper<Seckill>().eq("collage_count", 0).eq("type", SeckillConstant.GOODSID).eq("goods_id", goodsId).eq("status", BaseConstant.USE_STATUS));
                //goodsOrderDetails.setPrice(seckill.getSeckillPrice());
            }

            goodsOrderDetails.setActivityPrice(BigDecimal.ZERO);
            ResponseResult<GoodsOrderDetailsVo> result = new ResponseResult<>();
            result.setData(goodsOrderDetails);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
        }
    }


    /**
     * 不参与拼团，
     * 直接购买，创建订单
     * <p>
     * 订单表不存手机号，直接从地址表获取
     * 不用返回用户可用支付方式
     * 商品规格也不要
     *
     * @param addressId 地址id
     * @param goodsId   商品id
     * @param num       购买数量
     * @param type      1,单独购买，2拼团购买，3秒杀
     */
    @RequestMapping("/toCreateOrder")
    public void toCreateOrder(Integer addressId, Integer goodsId, Integer num, Integer type) {
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录"));
        } else {
            int i = 0;
            //订单编号
            String orderCode = null;
            Order order = new Order();
            Goods goods = null;
            OrderDetail orderDetail = null;
            try {
                //生成订单编号，根据登录用户id和当前时间戳
                orderCode = OrderCodeUtil.getOrderCode(customerInfo.getCustomerId().longValue());
                order.setOrderSn(orderCode);
                //下单人
                order.setCustomerName(customerInfo.getNickname());
                order.setCustomerId(customerInfo.getCustomerId());
                //根据用户id查询默认收货地址 ，地址状态，1为默认，0为非默认
                Address address = addressMapper.selectById(addressId);
                order.setAddress(address.getAddress());
                //购买人，收货人姓名
                order.setShippingUser(address.getConsignee());
                //收货人手机号
                order.setCusPhone(address.getTelephone());
                //订单状态
                order.setOrderStatus(OrderConstant.ONE_NO_PAY);
                //时间戳
                order.setOrderTime(LocalDateTime.now());
                order.setCreateTime(LocalDateTime.now());
                order.setCreateUserId(customerInfo.getCustomerId());
                order.setCreateUser(customerInfo.getNickname());
                //执行
                goods = goodsMapper.selectById(goodsId);
                BigDecimal totalPrice;
                //如果是有活动价，就活动价 乘以数量
                if (goods.getActivityPrice() != null) {
                    totalPrice = goods.getActivityPrice().multiply(new BigDecimal(num));
                } else {
                    totalPrice = goods.getPrice().multiply(new BigDecimal(num));
                }
                //如果是拼团，就拼团价 乘以数量
                if (type == 2) {
                    Seckill seckill = seckillMapper.selectOne(new QueryWrapper<Seckill>().ge("collage_count", 2).eq("type", SeckillConstant.GOODSID).eq("goods_id", goodsId).eq("status", BaseConstant.USE_STATUS));
                    //并且将拼团价设置为活动价
                    goods.setActivityPrice(seckill.getSeckillPrice());
                    totalPrice = seckill.getSeckillPrice().multiply(new BigDecimal(num));
                }
                order.setOrderMoney(totalPrice);
                //创建订单
                boolean save = iOrderService.save(order);
                if (!save) {
                    ResponseResult.Step(ResponseResult.ERROR("创建失败！"));
                    return;
                } else {


                    //如果订单创建成功，则添加订单详情
                    orderDetail = new OrderDetail();
                    orderDetail.setOrderId(order.getOrderId());
                    orderDetail.setGoodsId(goodsId);
                    orderDetail.setProductCnt(num);
                    if (goods != null) {
                        //设置购买商品实际价格
                        if (goods.getActivityPrice() != null) {
                            orderDetail.setProductPrice(goods.getActivityPrice());
                        } else {
                            orderDetail.setProductPrice(goods.getPrice());
                        }

                        //成本价格，也就是出厂价， 售价
                        orderDetail.setAverageCost(goods.getExitPrice());
                        orderDetail.setGoodsName(goods.getGoodsname());
                        orderDetail.setGoodsId(goods.getGoodsId());
                        orderDetail.setGoodsCode(goods.getGoodsCode());
                        //时间戳
                        orderDetail.setCreateTime(LocalDateTime.now());
                        orderDetail.setCreateUser(customerInfo.getNickname());
                        orderDetail.setCreateUserId(customerInfo.getCustomerId());
                    }
                    //执行 插入订单详情表
                    boolean save1 = iOrderDetailService.save(orderDetail);
                    if (save1) {
                        //如果订单创建成功，则先把当前商品的库存减 1
                        Integer stupperlimit = (goods.getStupperlimit() - 1);
                        goods.setStupperlimit(stupperlimit);
                        goodsMapper.updateById(goods);
                    }
                    //创建返回
                    ResponseResult flag = ResponseResult.flag(save1 ? 1 : 0);
                    flag.setData(order.getOrderMoney());
                    ResponseResult.Step(flag);
                }

                //创建订单流水
                ProfitFlowing profitFlowing = new ProfitFlowing();
                profitFlowing.setOrderSn(order.getOrderSn());
                profitFlowing.setIsPay(ProfitFlowingConstant.NO_PAY);
                profitFlowing.setPrice(goods.getPrice());
                profitFlowing.setActivityPrice(goods.getActivityPrice());

                /**
                 * 设置优惠券，满减，等其他抵扣值
                 * 后期有具体数值后再更新这里，前期设为 0
                 */
                profitFlowing.setCouponPrice(BigDecimal.ZERO);
                profitFlowing.setFillPrice(BigDecimal.ZERO);
                profitFlowing.setOtherPriceOne(BigDecimal.ZERO);
                profitFlowing.setOtherPriceTwo(BigDecimal.ZERO);
                profitFlowing.setOtherPriceThree(BigDecimal.ZERO);

                //计算所有抵扣金额总数
                BigDecimal add = profitFlowing.getCouponPrice().add(profitFlowing.getFillPrice()).add(profitFlowing.getOtherPriceOne()).add(profitFlowing.getOtherPriceTwo()).add(profitFlowing.getOtherPriceThree());
                BigDecimal subtract = null;  // 实际支付金额
                if (profitFlowing.getActivityPrice() != null) {
                    //如果活动价不为空， 则让活动价 减去 抵扣总数
                    subtract = profitFlowing.getActivityPrice().subtract(add);
                } else {
                    //否则，让销售单价  减去  抵扣总额
                    subtract = profitFlowing.getPrice().subtract(add);
                }

                //实际支付金额
                profitFlowing.setSalePrice(subtract);
                //商品低价
                profitFlowing.setExitPrice(goods.getExitPrice());
                //商品购买数量
                profitFlowing.setNumber(orderDetail.getProductCnt());

                //获取总利润 ,商品支付金额，乘以 数量， 再减去  低价 乘以数量
                BigDecimal totalMoney = profitFlowing.getSalePrice().multiply(new BigDecimal(profitFlowing.getNumber())).subtract(profitFlowing.getExitPrice().multiply(new BigDecimal(profitFlowing.getNumber())));
                //如果 总利润  小于 0 ，则返回创建 失败
                if (totalMoney.compareTo(BigDecimal.ZERO) == -1) {
                    ResponseResult.Step(ResponseResult.ERROR("价格设置错误，创建失败！"));
                    return;
                }
                //设置总利润
                profitFlowing.setTotalMoney(totalMoney);


                logger.debug("getPrice===" + profitFlowing.getPrice().multiply(new BigDecimal(profitFlowing.getNumber())));
                logger.debug("getExitPrice===" + profitFlowing.getExitPrice().multiply(new BigDecimal(profitFlowing.getNumber())));
                logger.debug("getActivityPrice===" + profitFlowing.getActivityPrice().multiply(new BigDecimal(profitFlowing.getNumber())));


                //代理商利润金额
                BigDecimal AGENT_TYPE = getPercentage(2, profitFlowing.getPrice().multiply(new BigDecimal(profitFlowing.getNumber())), profitFlowing.getExitPrice().multiply(new BigDecimal(profitFlowing.getNumber())), profitFlowing.getActivityPrice().multiply(new BigDecimal(profitFlowing.getNumber())), BaseConstant.AGENT_TYPE);
                //上级利润金额
                BigDecimal PARE_TYPE = getPercentage(2, profitFlowing.getPrice().multiply(new BigDecimal(profitFlowing.getNumber())), profitFlowing.getExitPrice().multiply(new BigDecimal(profitFlowing.getNumber())), profitFlowing.getActivityPrice().multiply(new BigDecimal(profitFlowing.getNumber())), BaseConstant.PARE_TYPE);
                //平台利润金额
                BigDecimal PLAT_TYPE = getPercentage(2, profitFlowing.getPrice().multiply(new BigDecimal(profitFlowing.getNumber())), profitFlowing.getExitPrice().multiply(new BigDecimal(profitFlowing.getNumber())), profitFlowing.getActivityPrice().multiply(new BigDecimal(profitFlowing.getNumber())), BaseConstant.PLAT_TYPE);
                //购买用户利润金额
                BigDecimal USER_TYPE = getPercentage(2, profitFlowing.getPrice().multiply(new BigDecimal(profitFlowing.getNumber())), profitFlowing.getExitPrice().multiply(new BigDecimal(profitFlowing.getNumber())), profitFlowing.getActivityPrice().multiply(new BigDecimal(profitFlowing.getNumber())), BaseConstant.USER_TYPE);

                profitFlowing.setCusMoney(USER_TYPE);
                profitFlowing.setParentcusMoney(PARE_TYPE);
                profitFlowing.setAgentMoney(AGENT_TYPE);
                profitFlowing.setPlatformMoney(PLAT_TYPE);

                profitFlowing.setState(ProfitFlowingConstant.NO_STATUS);
                profitFlowing.setCusId(customerInfo.getCustomerId());

                profitFlowing.setCreationDate(LocalDateTime.now());
                profitFlowing.setCreatedId(customerInfo.getCustomerId());
                profitFlowing.setCreatedName(customerInfo.getNickname());

                boolean save1 = iProfitFlowingService.save(profitFlowing);
                if (!save1) {
                    ResponseResult.Step(ResponseResult.ERROR("流水创建失败，请重新操作！"));
                }

            } catch (Exception e) {
                e.printStackTrace();
                ResponseResult.Step(ResponseResult.ERROR("创建失败，请重新操作！"));
            }
        }
    }

    /**
     * 支付成功后执行 这个回调
     * 更新订单表支付状态 和  流水表支付状态
     *
     * @param orderSn 订单编号
     */
    @RequestMapping("/successPay")
    public void successPay(String orderSn) {
        try {
            Order order = iOrderService.getOne(new QueryWrapper<Order>().eq("order_sn", orderSn));
            ProfitFlowing profitFlowing = iProfitFlowingService.getOne(new QueryWrapper<ProfitFlowing>().eq("order_sn", orderSn));
            //订单表状态设为已支付
            order.setOrderStatus(OrderConstant.TWO_YES_PAY);
            //流水表设为已支付
            profitFlowing.setIsPay(ProfitFlowingConstant.YES_PAY);
            boolean b = iProfitFlowingService.updateById(profitFlowing);
            boolean b1 = iOrderService.updateById(order);
            if (b && b1) {
                ResponseResult.Step(ResponseResult.SUCCESS);
            } else {
                ResponseResult.Step(ResponseResult.ERROR("订单状态更新失败！"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("orderSn===" + orderSn);
            logger.debug("Exception===" + e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("异常错误！"));
        }

    }


    /**
     * 根据订单状态，
     * 查询该状态下的订单商品详情
     */
    @RequestMapping("/selectOrderByStatus/{status}")
    public void selectOrderByStatus(@PathVariable Integer status) {
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录"));
            return;
        }
        try {
            List<Order> orderList = iOrderService.list(new QueryWrapper<Order>().eq("order_status", status).eq("customer_id", customerInfo.getCustomerId()));
            List<GoodsOrderDetailsVo> goodsOrderDetailsVos = new ArrayList<>();
            for (Order order : orderList) {
                GoodsOrderDetailsVo goodsOrderDetailsVo = new GoodsOrderDetailsVo();
                logger.debug("getOrderId===" + order.getOrderId());
                OrderDetail orderDetail = iOrderDetailService.getOne(new QueryWrapper<OrderDetail>().eq("order_id", order.getOrderId()));
                if (orderDetail == null) {
                    ResponseResult.Step(ResponseResult.ERROR("查询订单详情失败！"));
                    return;
                }
                //商品 实际金额
                goodsOrderDetailsVo.setActivityPrice(orderDetail.getProductPrice());
                goodsOrderDetailsVo.setNumber(orderDetail.getProductCnt());
                goodsOrderDetailsVo.setGoodsId(orderDetail.getGoodsId());
                goodsOrderDetailsVo.setGoodsname(orderDetail.getGoodsName());
                //订单实际支付金额
                goodsOrderDetailsVo.setPrice(order.getOrderMoney());


                Goods goods = iGoodsService.getById(orderDetail.getGoodsId());
                Merchant merchant = iMerchantService.getById(goods.getMerchantId());
                goodsOrderDetailsVo.setmName(merchant.getmName());
                goodsOrderDetailsVo.setOrderSn(order.getOrderSn());
                goodsOrderDetailsVos.add(goodsOrderDetailsVo);

            }
            ResponseResult<List<GoodsOrderDetailsVo>> result = new ResponseResult<>();
            result.setData(goodsOrderDetailsVos);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("status===" + status);
            logger.debug("Exception===" + e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("获取订单列表失败！"));
        }

    }


    /**
     * 根据订单状态，
     * 查询 当前登录用户的 全部订单
     * 已删除的不算，根据订单创建时间排序
     */
    @RequestMapping("/selectOrderAll")
    public void selectOrderAll() {
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录"));
            return;
        }
        try {
            List<Order> orderList = iOrderService.list(new QueryWrapper<Order>().ne("order_status", OrderConstant.DELETE_STATE).eq("customer_id", customerInfo.getCustomerId()).orderByDesc("order_time"));
            List<GoodsOrderDetailsVo> goodsOrderDetailsVos = new ArrayList<>();
            for (Order order : orderList) {
                GoodsOrderDetailsVo goodsOrderDetailsVo = new GoodsOrderDetailsVo();
                logger.debug("getOrderId===" + order.getOrderId());
                OrderDetail orderDetail = iOrderDetailService.getOne(new QueryWrapper<OrderDetail>().eq("order_id", order.getOrderId()));
                if (orderDetail == null) {
                    ResponseResult.Step(ResponseResult.ERROR("查询订单详情失败！"));
                    return;
                }
                //商品 实际金额
                goodsOrderDetailsVo.setActivityPrice(orderDetail.getProductPrice());
                goodsOrderDetailsVo.setNumber(orderDetail.getProductCnt());
                goodsOrderDetailsVo.setGoodsId(orderDetail.getGoodsId());
                goodsOrderDetailsVo.setGoodsname(orderDetail.getGoodsName());
                //订单实际支付金额
                goodsOrderDetailsVo.setPrice(order.getOrderMoney());

                Goods goods = iGoodsService.getById(orderDetail.getGoodsId());
                Merchant merchant = iMerchantService.getById(goods.getMerchantId());
                goodsOrderDetailsVo.setPictureUrl(goods.getPictureUrl());
                goodsOrderDetailsVo.setmName(merchant.getmName());
                goodsOrderDetailsVo.setOrderSn(order.getOrderSn());
                goodsOrderDetailsVo.setOrderStatus(order.getOrderStatus());
                goodsOrderDetailsVos.add(goodsOrderDetailsVo);

            }
            ResponseResult<List<GoodsOrderDetailsVo>> result = new ResponseResult<>();
            result.setData(goodsOrderDetailsVos);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("customerinfo id===" + customerInfo.getCustomerId());
            logger.debug("Exception===" + e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("获取订单列表失败！"));
        }

    }


    /**
     * 订单待付款时，
     * 更新 订单的收货地址
     */
    @RequestMapping("/updateAddressByOne")
    public void updateAddressByOne(String orderSn, String addressId) {
        try {
            Order order = iOrderService.getOne(new QueryWrapper<Order>().eq("order_sn", orderSn).eq("order_status", OrderConstant.ONE_NO_PAY));
            if (order == null) {
                ResponseResult.Step(ResponseResult.ERROR("失败，订单不存在或者状态错误！"));
                return;
            }
            Address address = iAddressService.getById(addressId);
            order.setAddress(address.getAddress());
            order.setShippingUser(address.getConsignee());
            order.setCusPhone(address.getTelephone());

            boolean b = iOrderService.updateById(order);
            if (b) {
                ResponseResult.Step(ResponseResult.SUCCESS("修改地址成功！"));
            } else {
                ResponseResult.Step(ResponseResult.ERROR("修改失败！"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("orderSn===" + orderSn);
            logger.debug("addressId===" + addressId);
            logger.debug("Exception===" + e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("系统异常！"));
        }

    }

    /**
     * 取消订单，
     */
    @RequestMapping("/cancelOrder")
    public void cancelOrder(String orderSn) {
        try {
            Order order = iOrderService.getOne(new QueryWrapper<Order>().eq("order_sn", orderSn).eq("order_status", OrderConstant.ONE_NO_PAY));
            if (order == null) {
                ResponseResult.Step(ResponseResult.ERROR("失败，订单不存在或者状态错误！"));
                return;
            }
            //将订单状态设为0
            order.setOrderStatus(OrderConstant.NO_USE);
            boolean b = iOrderService.updateById(order);
            if (b) {
                ResponseResult.Step(ResponseResult.SUCCESS("订单取消成功！"));
            } else {
                ResponseResult.Step(ResponseResult.ERROR("订单取消失败！"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("orderSn===" + orderSn);
            logger.debug("Exception===" + e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("系统异常！"));
        }

    }


    /**
     * 删除订单，
     */
    @RequestMapping("/deleteOrder")
    public void deleteOrder(String orderSn) {
        try {
            Order order = iOrderService.getOne(new QueryWrapper<Order>().eq("order_sn", orderSn).eq("order_status", OrderConstant.NO_USE));
            if (order == null) {
                ResponseResult.Step(ResponseResult.ERROR("失败，订单不存在或者状态错误！"));
                return;
            }
            //将订单状态设为0
            order.setOrderStatus(OrderConstant.DELETE_STATE);
            boolean b = iOrderService.updateById(order);
            if (b) {
                ResponseResult.Step(ResponseResult.SUCCESS("订单删除成功！"));
            } else {
                ResponseResult.Step(ResponseResult.ERROR("订单删除失败！"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("orderSn===" + orderSn);
            logger.debug("Exception===" + e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("系统异常！"));
        }

    }


    /**
     * 订单详情，
     * 页面的订单编号，下单时间，支付时间
     *
     * @param orderSn
     */
    @RequestMapping("/orderDetailVo")
    public void orderDetailVo(String orderSn) {

        ResponseResult<List<Map<String, Object>>> result = new ResponseResult<>();
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        QueryWrapper<Order> eq = queryWrapper.select("order_time orderTime", "pay_time payTime", "order_sn orderSn").eq("order_sn", orderSn);
        List<Map<String, Object>> maps = new ArrayList<>();
        try {
            maps = iOrderService.listMaps(eq);
            result.setOkMsg("查询成功");
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("订单信息 查询失败");
            result.setErrorMsg("查询失败");
        }
        result.setData(maps);
        ResponseResult.Step(result);

    }


}


