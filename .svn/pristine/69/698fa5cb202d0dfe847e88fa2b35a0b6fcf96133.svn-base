package com.tourism.hu.apicontroller;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.CustomerPassenger;
import com.tourism.hu.service.ICustomerPassengerService;
import com.tourism.hu.util.ResponseResult;


/**
 * 
 * @ClassName: CustomerPassengerController 
 * @Description: (用户乘车人信息表 前端控制器) 
 * @author 董兴隆
 * @date 2019年11月12日 下午3:07:52 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("api/customerPassengerApi")
public class CustomerPassengerApiController extends BaseController {

	private Logger logger  = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private ICustomerPassengerService passengerService;
	
	/**
	 * @Title: savePassenger
	 * @Description: (用户添加乘车人)   
	 * @date 2019年11月12日 下午3:09:53
	 * @author 董兴隆
	 */
	@RequestMapping("/saveOrUpdate")
	public void saveOrUpdate(CustomerPassenger pass) {
		ResponseResult<List<Map<String,Object>>> reuslt =  new ResponseResult<List<Map<String,Object>>>();
		List<Map<String,Object>> maps = new ArrayList<>();
		CustomerInfo user = getSessionUser();
		if(user == null) {
			reuslt.setErrorMsg("未找到登录用户信息");
			reuslt.setData(maps);
			ResponseResult.Step(reuslt);
			return ;
		}
		//身份证信息 必填 证件类型   证件号码   姓名
		if(pass.getCardType()!=null && StringUtils.isNotBlank(pass.getCardNo())&& StringUtils.isNotBlank(pass.getName())) {
			try {
				//如果当前设置为本人 将该用户其他角色设置为非本人
				if(pass.getIsMyself()!=null && pass.getIsMyself()==1) {
					passengerService.update(new UpdateWrapper<CustomerPassenger>().eq("card_type", pass.getCardType()).eq("customer_id", pass.getCustomerId()).set("is_myself", 0));
				}
				pass.setCustomerId(user.getCustomerId());
				if(passengerService.saveOrUpdate(pass)) {
					reuslt.setOkMsg("成功");
					reuslt.setData(maps);
					ResponseResult.Step(reuslt);
					return ;
				}
			}catch (Exception e) {
				logger.debug("savePassenger 异常--------->"+e.getMessage());
				logger.debug("参数--------->"+pass.toString());
			}
			reuslt.setErrorMsg("失败");
			reuslt.setData(maps);
			ResponseResult.Step(reuslt);
		}else {
			reuslt.setErrorMsg("请检查必填信息    证件类型   证件号码   姓名");
			reuslt.setData(maps);
			ResponseResult.Step(reuslt);
		}
		
	}
	
	/**
	 * 
	 * @Title: selectPassenger 
	 * @Description: (获得多个乘车人信息)   
	 * @date 2019年11月12日 下午3:25:22
	 * @author 董兴隆
	 */
	@RequestMapping("selectPassenger")
	public void selectPassenger(Integer cardType) {
		ResponseResult<List<Map<String,Object>>> result =  new ResponseResult<List<Map<String,Object>>>();
		List<Map<String,Object>> maps = new ArrayList<>();
		CustomerInfo user = getSessionUser();
		if(user == null) {
			result.setErrorMsg("未找到登录用户信息");
			result.setData(maps);
			ResponseResult.Step(result);
			return ;
		}
		QueryWrapper<CustomerPassenger> qw = new QueryWrapper<CustomerPassenger>();
		qw.select("id","is_myself isMyself","card_no cardNo","name","phone","memo","start_time startTime","end_time endTime").eq("status", 1).eq("card_type", cardType).eq("customer_id", user.getCustomerId());
		maps =passengerService.listMaps(qw);
		result.setOkMsg("查询成功");
		result.setData(maps);
		ResponseResult.Step(result);
	}
	
	/**
	 * 
	 * @Title: deltePassenger 
	 * @Description: (删除乘车人信息) 
	 * @param id  
	 * @date 2019年11月12日 下午3:41:08
	 * @author 董兴隆
	 */
	@RequestMapping("deltePassenger")
	public void deltePassenger(Integer id) {
		ResponseResult<List<Map<String,Object>>> result =  new ResponseResult<List<Map<String,Object>>>();
		List<Map<String,Object>> maps = new ArrayList<>();
		CustomerInfo user = getSessionUser();
		if(user == null) {
			result.setErrorMsg("未找到登录用户信息");
			result.setData(maps);
			ResponseResult.Step(result);
			return ;
		}
		CustomerPassenger pass = passengerService.getById(id);
		if(pass == null) {
			result.setErrorMsg("未找到用户信息");
			result.setData(maps);
			ResponseResult.Step(result);
			return ;
		}
		pass.setStatus(3);
		result.setErrorMsg("删除失败");
		try {
			if(passengerService.updateById(pass)) {
				result.setOkMsg("删除成功");
			}
		}catch (Exception e) {
			logger.debug("deltePassenger 异常--------->"+e.getMessage());
			logger.debug("id 参数--------->"+id);
		}
		result.setData(maps);
		ResponseResult.Step(result);
	}
	
	/**
	 * 
	 * @Title: getPassenger 
	 * @Description: (获得指定乘车人信息) 
	 * @param id  
	 * @date 2019年11月12日 下午6:34:28
	 * @author 董兴隆
	 */
	@RequestMapping("getPassenger")
	public void getPassenger(Integer id) {
		ResponseResult<Map<String,Object>> result =  new ResponseResult<Map<String,Object>>();
		Map<String,Object> map = new HashMap<String, Object>();
		CustomerInfo user = getSessionUser();
		if(user == null) {
			result.setErrorMsg("未找到登录用户信息");
			result.setData(map);
			ResponseResult.Step(result);
			return ;
		}
		map = passengerService.getMap(new QueryWrapper<CustomerPassenger>().eq("id", id));
		if(map!=null) {
			map.remove("status");
		}
		result.setData(map);
		ResponseResult.Step(result);
	}
	
	
}
