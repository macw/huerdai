package com.tourism.hu.util;

import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.awt.Window;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

/**
 * @ClassName: GlobalConfigUtil 
 * @Description: 系统全局变量参数设置
 * @author JoshZhu
 * @date 2019年11月16日 下午2:47:24 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */

public class GlobalConfigUtil {
	
	
	public static Map<String,String> map = new HashMap<String, String>();
	/**
	 * 快递密钥
	 */
	public static String KD_EBUSINESSID = "EBusinessID";
	
	/**
	 * 快递商户户
	 */
	public static String KD_APIKEY = "AppKey";
	
	/**
	 * 快递请求地址
	 */
	public static String KD_REQURL  = "ReqURL";
	
	/**
	 * 同一快递查询间隔时间
	 */
	public static String KD_SEARCH_TIME = "SearchTime";
	
	
	/**
	 *  服务商APPID
	 */
	public static String WX_PAY_APPID = "appId";
	/**
	 *  服务商商户号
	 */
	public static String WX_PAY_MCHID = "mchId";
	/**
	 *  支付商户号
	 */
	public static String WX_PAY_PARTER = "partner";
	/**
	 *  支付商户密码
	 */
	public static String WX_PAY_PARTNERKEY = "partnerKey" ;
	/**
	 *  公众号秘钥
	 */
	public static String WX_PAY_APPSECRET = "appSecret";
	/**
	 *  编码格式
	 */
	public static String WX_PAY_CHARSET = "CHARSET";
	public static String WX_PAY_SIGNTYPE = "SIGNTYPE";
	/**
	 *  通知地址
	 */
	public static String WX_CERTFILE = "certFile";
	public static String WX_PAYNOTIFY = "payNotify";
	public static String WX_REFUNDNOTIFY = "refundNotify";
	
	/**
	 * 阿里根据IP获取省市接口
	 */
	public static String ALI_CITY_URL = "getCityUrl";

	
	 /**
	  *  默认加载全部配置
	  */
	 static{
		Properties properties=new Properties();
		try {
			InputStream is = ClassLoader.getSystemResourceAsStream("GlobalConfig.properties");
			properties.load(is);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Set<Object> keys=properties.keySet();
		for (Object k : keys) {
			map.put((String)k, (String)properties.get(k));
		}
	}
	
	
	 /**
	  * 
	  * @Title: getUploadUrl 
	  * @Description: (上传路径) 
	  * @return  
	  * @date 2019年11月26日 下午2:16:39
	  * @author 董兴隆
	  */
	 public  static String getUploadUrl(){
		if (isTest()) {
			return map.get("testUploadUrl");
		}else {
			return map.get("uploadUrl");
		}
	 }
	 /**
	  * 
	  * @Title: getServiceUrl 
	  * @Description: (访问路径) 
	  * @return  
	  * @date 2019年11月26日 下午2:17:50
	  * @author 董兴隆
	  */
	 public  static String getServiceUrl(){
		 if(isTest()) {
			 return map.get("testServiceUrl");
		 }else {
			 return map.get("serviceUrl");
		 }
	 }
	 
	 /**
	  * 
	  * @Title: isTest 
	  * @Description: (是否是测试) 
	  * @return  
	  * @date 2019年11月26日 下午2:16:30
	  * @author 董兴隆
	  */
	 public static boolean isTest(){
		if("1".equals(map.get("isTest"))) {
			return true;
		}else {
			return false;
		}
	}
	 
	
	 
	 /**
	  * 
	  * @Title: getKey 
	  * @Description: (通过Key获得对应的值) 
	  * @param key
	  * @return  
	  * @date 2019年11月26日 下午2:08:54
	  * @author 董兴隆
	  */
	public static String getKey(String key) {
		return map.get(key);
	}
	
	

  
}
