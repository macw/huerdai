package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.OrderConstant;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.*;
import com.tourism.hu.entity.vo.GoodsDtlVo;
import com.tourism.hu.entity.vo.GoodsOrderDetailsVo;
import com.tourism.hu.entity.vo.OrderStatusVo;
import com.tourism.hu.mapper.AddressMapper;
import com.tourism.hu.mapper.GoodsMapper;
import com.tourism.hu.mapper.OrderMapper;
import com.tourism.hu.mapper.SeckillMapper;
import com.tourism.hu.pay.wxsdk.TradeType;
import com.tourism.hu.pay.wxsdk.WXPay;
import com.tourism.hu.pay.wxsdk.WXPayUtil;
import com.tourism.hu.service.*;
import com.tourism.hu.util.DateConverterUtil;
import com.tourism.hu.util.GlobalConfigUtil;
import com.tourism.hu.util.OrderCodeUtil;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 董兴隆
 * @ClassName: OrderApiController
 * @Description: ()
 * @date 2019年11月11日 下午5:02:34
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/api/orderApi")
public class OrderApiController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private OrderMapper orderMapper;

    @Resource
    private IOrderService iOrderService;

    @Resource
    private IOrderDetailService iOrderDetailService;

    @Resource
    private AddressMapper addressMapper;

    @Resource
    private IAddressService iAddressService;

    @Resource
    private GoodsMapper goodsMapper;

    @Resource
    private IGoodsService iGoodsService;

    @Resource
    private IGoodsPicService iGoodsPicService;

    @Resource
    private SeckillMapper seckillMapper;

    @Resource
    private IProfitFlowingService iProfitFlowingService;

    @Resource
    private IMerchantService iMerchantService;

    @Resource
    private IGoodsEquityService iGoodsEquityService;

    @Resource
    private IEquityService iEquityService;

    @Resource
    private IBaseGoods iBaseGoods;


    /**
     * 获取用户个人中心 订单，待付款、带发货，待收货，带评价，退款中个数统计接口
     */
    @RequestMapping("/getOrderNumberByCustomer")
    public void getOrderNumberByCustomer() {
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录"));
            return;
        }
        try {
            Integer customerId = customerInfo.getCustomerId();
            List<OrderStatusVo> voList = new ArrayList<>();
            //获取用户个人中心 订单，待付款数量
            List<Order> order_status = orderMapper.selectList(new QueryWrapper<Order>().eq("customer_id", customerId).eq("order_status", OrderConstant.ONE_NO_PAY));
            if (order_status != null) {
                OrderStatusVo vo = new OrderStatusVo("待付款", order_status.size(), GlobalConfigUtil.getKey("projectUrl") + "api/orderApi/selectOrderByStatus/" + 1, null);
                voList.add(vo);
            }
            //获取用户个人中心 订单，待发货数量
            List<Order> order_status2 = orderMapper.selectList(new QueryWrapper<Order>().eq("customer_id", customerId).eq("order_status", OrderConstant.TWO_YES_PAY));
            if (order_status2 != null) {
                OrderStatusVo vo = new OrderStatusVo("待发货", order_status2.size(), GlobalConfigUtil.getKey("projectUrl") + "api/orderApi/selectOrderByStatus/" + 2, null);
                voList.add(vo);
            }
          /*  //获取用户，待使用数量
            List<Order> orders21 = orderMapper.selectList(new QueryWrapper<Order>().eq("customer_id", customerId).eq("order_status", OrderConstant.TWOONE_NO_USE));
            if ((orders21 != null)) {
                OrderStatusVo vo = new OrderStatusVo("待使用", orders21.size(), BaseConstant.OS_PATH + "api/orderApi/selectOrderByStatus/" + 21, null);
                voList.add(vo);
            }*/
            //获取用户个人中心 订单，待收货数量
            List<Order> order_status3 = orderMapper.selectList(new QueryWrapper<Order>().eq("customer_id", customerId).eq("order_status", OrderConstant.FOUR_DONE));
            if (order_status3 != null) {
                OrderStatusVo vo = new OrderStatusVo("待收货", order_status3.size(), GlobalConfigUtil.getKey("projectUrl") + "api/orderApi/selectOrderByStatus/" + 3, null);
                voList.add(vo);
            }
            //获取用户个人中心 订单，待评价数量
            List<Order> order_status4 = orderMapper.selectList(new QueryWrapper<Order>().eq("customer_id", customerId).eq("order_status", OrderConstant.THREE_SHIPPING));
            if (order_status4 != null) {
                OrderStatusVo vo = new OrderStatusVo("待评价", order_status4.size(), GlobalConfigUtil.getKey("projectUrl") + "api/orderApi/selectOrderByStatus/" + 4, null);
                voList.add(vo);
            }
          /*  //获取用户个人中心 订单，退款中数量
            List<Order> order_status5 = orderMapper.selectList(new QueryWrapper<Order>().eq("customer_id", customerId).eq("order_status", OrderConstant.FIVE_EXIT));
            if (order_status5 != null) {
                OrderStatusVo vo = new OrderStatusVo("退款中", order_status5.size(), BaseConstant.OS_PATH + "api/orderApi/selectOrderByStatus/" + 5, null);
                voList.add(vo);
            }*/
            ResponseResult<List<OrderStatusVo>> result = new ResponseResult<>();
            result.setData(voList);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("customerinfo===" + customerInfo.toString());
            logger.debug("Exception===" + e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
        }
    }


    /**
     * 创建订单
     * <p>
     * 价格全部存流水表
     * 基本信息存订单表
     * 商品信息存详情表
     * <p>
     * 1，是否登录
     * 2，判断库存
     * 3. 查询商品规格信息
     * 4，先创建流水表，
     * 5，再创建订单表，
     * 6，再创建详情表
     * 7,去支付
     *
     * @param addressId 地址id
     * @param goodsId   商品id
     * @param num       购买数量
     * @param type      1,单独购买，2拼团购买，3秒杀
     */
    @RequestMapping("/toCreateOrder")
    public void toCreateOrder(Integer addressId, Integer goodsId, Integer num, Integer type, @RequestParam("dtlIdArray[]") Integer[] dtlIdArray) {
        logger.debug("dtlIdArray==" + dtlIdArray.length + "---" + dtlIdArray.toString() + "---" + dtlIdArray[0] + "----" + dtlIdArray[1]);
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录"));
            return;
        }
        //订单编号
        String orderCode;
        Order order = new Order();
        OrderDetail orderDetail = new OrderDetail();
        //根据商品id 查询商品
        Goods goods = iGoodsService.getById(goodsId);
        if (goods == null) {
            ResponseResult.Step(ResponseResult.ERROR("商品不存在或者已下架！"));
            return;
        } else if (goods.getStupperlimit() - goods.getStlowerlimit() == 0 || goods.getStupperlimit() - goods.getStlowerlimit() < 0) {
            ResponseResult.Step(ResponseResult.ERROR("商品库存不足！"));
            return;
        }
        GoodsDtlVo goodsDtlVo = null;
        try {
            goodsDtlVo = iBaseGoods.getGoodsDtlVo(goodsId, dtlIdArray);
            if (goodsDtlVo == null) {
                logger.debug("goodsDtlVo===null,商品规格查询异常");
                ResponseResult.Step(ResponseResult.ERROR("商品规格查询异常！"));
                return;
            }
            logger.debug("1111111111111111");
            //生成订单编号，根据登录用户id和当前时间戳
            orderCode = OrderCodeUtil.getOrderCode(customerInfo.getCustomerId().longValue());

            // 4，创建订单流水
            ProfitFlowing profitFlowing = new ProfitFlowing();
            ResponseResult responseResult = iProfitFlowingService.createProfitFlowing(profitFlowing, orderCode, goods, goodsDtlVo, num, customerInfo);
            if (responseResult.getCode() == 1) {
                ResponseResult.Step(ResponseResult.ERROR(responseResult.getMsg()));
                return;
            }
            // 5 ，创建订单
            boolean save = iOrderService.createOrder(order, customerInfo, addressId, profitFlowing);
            if (!save) {
                ResponseResult.Step(ResponseResult.ERROR("创建失败！"));
                return;
            }
            //6， 如果订单创建成功，则添加订单详情
            boolean save1 = iOrderDetailService.createOrderDetail(orderDetail, order, goodsId, num, goodsDtlVo, goods, customerInfo);
            logger.debug("55555555555555555");
            if (save1) {
                //如果订单创建成功，则先把当前商品的库存减 去购买的数量
                Integer stupperlimit = (goods.getStupperlimit() - num);
                goods.setStupperlimit(stupperlimit);
                if (!iGoodsService.updateById(goods)) {
                    ResponseResult.Step(ResponseResult.ERROR("库存更新失败！"));
                    return;
                }
            } else {
                ResponseResult.Step(ResponseResult.ERROR("订单详情创建失败！"));
                return;
            }

            logger.debug("6666666666666666666");
            //到这里都没有异常，则返回创建成功！
            //别返回成功了，去支付
            if (customerInfo.getOpenid() == null) {
                ResponseResult.Step(ResponseResult.ERROR("您还没有进行微信授权，无法进行支付！"));
                return;
            }
            String wxResult = WXPay.toPay(order.getOrderSn(), "购买" + orderDetail.getGoodsName(), profitFlowing.getSalePrice(), "http://api.huerdai.net/hu/notify/successPay", TradeType.JSAPI, customerInfo.getOpenid());
            logger.info("wxResult========>" + wxResult);
            Map<String, String> wxConfig = WXPayUtil.xmlToMap(wxResult);
            if (wxConfig != null) {
                if ("SUCCESS".equals(wxConfig.get("result_code"))) {
                    WXPay.createJSAPIWxConfig(wxConfig);
                    logger.info("wxConfig==============>" + wxConfig.toString());
                } else {
                    wxConfig.put("result_code", "FAIL");
                }
            }

            ResponseResult<Map<String, Object>> result = new ResponseResult<>();
            Map<String, Object> map = new HashMap<>();
            map.put("price", profitFlowing.getSalePrice());
            map.put("orderSn", order.getOrderSn());
            map.put("wxConfig", wxConfig);
            result.setData(map);
            result.setMsg("订单创建成功!");
            result.setCode(0);
            ResponseResult.Step(result);
            return;
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("goodsId=" + goodsId);
            logger.debug("goodsDtlvo===" + goodsDtlVo);
            logger.debug("Exception===" + e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("创建失败，请重新操作！"));
        }
    }


    /**
     * 根据订单状态，
     * 查询该状态下的订单商品 列表信息
     */
    @RequestMapping("/selectOrderByStatus/{status}")
    public void selectOrderByStatus(@PathVariable Integer status) {
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录"));
            return;
        }
        try {
            List<Order> orderList = iOrderService.list(new QueryWrapper<Order>().eq("order_status", status).eq("customer_id", customerInfo.getCustomerId()));
            List<GoodsOrderDetailsVo> goodsOrderDetailsVos = new ArrayList<>();
            for (Order order : orderList) {
                GoodsOrderDetailsVo goodsOrderDetailsVo = new GoodsOrderDetailsVo();
                logger.debug("getOrderId===" + order.getOrderId());
                OrderDetail orderDetail = iOrderDetailService.getOne(new QueryWrapper<OrderDetail>().eq("order_sn", order.getOrderSn()));
                ProfitFlowing profitFlowing = iProfitFlowingService.getOne(new QueryWrapper<ProfitFlowing>().eq("order_sn", order.getOrderSn()));
                if (orderDetail == null) {
                    ResponseResult.Step(ResponseResult.ERROR("查询订单详情失败！"));
                    return;
                }
                //商品 实际金额
                goodsOrderDetailsVo.setActivityPrice(profitFlowing.getActivityPrice());
                goodsOrderDetailsVo.setNumber(orderDetail.getProductCnt());
                goodsOrderDetailsVo.setGoodsId(orderDetail.getGoodsId());
                goodsOrderDetailsVo.setGoodsname(orderDetail.getGoodsName());
                //订单实际支付金额
                goodsOrderDetailsVo.setPrice(order.getOrderMoney());
                goodsOrderDetailsVo.setMerName(orderDetail.getMerName());
                goodsOrderDetailsVo.setOrderSn(order.getOrderSn());
                goodsOrderDetailsVo.setOrderStatus(order.getOrderStatus());
                goodsOrderDetailsVo.setGoodsDtlList(orderDetail.getGoodsDtlDetail());
                goodsOrderDetailsVo.setEquityList(orderDetail.getGoodsEquityDetail());
                goodsOrderDetailsVo.setPictureUrl(orderDetail.getGoodsPictureUrl());
                goodsOrderDetailsVos.add(goodsOrderDetailsVo);
            }
            ResponseResult<List<GoodsOrderDetailsVo>> result = new ResponseResult<>();
            result.setData(goodsOrderDetailsVos);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("status===" + status);
            logger.debug("Exception===" + e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("获取订单列表失败！"));
        }

    }


    /**
     * 根据订单状态，
     * 查询 当前登录用户的 全部订单 列表
     * 已删除的不算，根据订单创建时间排序
     */
    @RequestMapping("/selectOrderAll")
    public void selectOrderAll() {
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录"));
            return;
        }
        try {
            List<Order> orderList = iOrderService.list(new QueryWrapper<Order>().ne("order_status", OrderConstant.DELETE_STATE).eq("customer_id", customerInfo.getCustomerId()).orderByDesc("order_time"));
            List<GoodsOrderDetailsVo> goodsOrderDetailsVos = new ArrayList<>();
            for (Order order : orderList) {
                GoodsOrderDetailsVo goodsOrderDetailsVo = new GoodsOrderDetailsVo();

                logger.debug("getOrderId===" + order.getOrderId());
                OrderDetail orderDetail = iOrderDetailService.getOne(new QueryWrapper<OrderDetail>().eq("order_sn", order.getOrderSn()));
                if (orderDetail == null) {
                    ResponseResult.Step(ResponseResult.ERROR("查询订单详情失败！"));
                    return;
                }
                //商品 实际金额
                ProfitFlowing profitFlowing = iProfitFlowingService.getOne(new QueryWrapper<ProfitFlowing>().eq("order_sn", order.getOrderSn()));
                goodsOrderDetailsVo.setActivityPrice(profitFlowing.getActivityPrice());
                goodsOrderDetailsVo.setNumber(orderDetail.getProductCnt());
                goodsOrderDetailsVo.setGoodsId(orderDetail.getGoodsId());
                goodsOrderDetailsVo.setGoodsname(orderDetail.getGoodsName());
                //订单实际支付金额
                goodsOrderDetailsVo.setPrice(order.getOrderMoney());
                goodsOrderDetailsVo.setPictureUrl(orderDetail.getGoodsPictureUrl());
                goodsOrderDetailsVo.setMerName(orderDetail.getMerName());
                goodsOrderDetailsVo.setOrderSn(order.getOrderSn());
                goodsOrderDetailsVo.setOrderStatus(order.getOrderStatus());

                goodsOrderDetailsVos.add(goodsOrderDetailsVo);

            }
            ResponseResult<List<GoodsOrderDetailsVo>> result = new ResponseResult<>();
            result.setData(goodsOrderDetailsVos);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("customerinfo id===" + customerInfo.getCustomerId());
            logger.debug("Exception===" + e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("获取订单列表失败！"));
        }

    }


    /**
     * 订单待付款时，
     * 更新 订单的收货地址
     */
    @RequestMapping("/updateAddressByOne")
    public void updateAddressByOne(String orderSn, String addressId) {
        try {
            Order order = iOrderService.getOne(new QueryWrapper<Order>().eq("order_sn", orderSn).eq("order_status", OrderConstant.ONE_NO_PAY));
            if (order == null) {
                ResponseResult.Step(ResponseResult.ERROR("失败，订单不存在或者状态错误！"));
                return;
            }
            Address address = iAddressService.getById(addressId);
            order.setAddress(address.getAddress());
            order.setShippingUser(address.getConsignee());
            order.setCusPhone(address.getTelephone());

            boolean b = iOrderService.updateById(order);
            if (b) {
                ResponseResult.Step(ResponseResult.SUCCESS("修改地址成功！"));
            } else {
                ResponseResult.Step(ResponseResult.ERROR("修改失败！"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("orderSn===" + orderSn);
            logger.debug("addressId===" + addressId);
            logger.debug("Exception===" + e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("系统异常！"));
        }
    }

    /**
     * 查询订单详情
     * @param orderSn
     */
    @RequestMapping("/selectOrderDetail")
    public void selectOrderDetail(String orderSn){
        Map<String, Object> map = new HashMap<>();
        Order order = iOrderService.getOne(new QueryWrapper<Order>().eq("order_sn", orderSn));
        if (order==null){
            ResponseResult.Step(ResponseResult.ERROR("订单查询失败！"));
            return;
        }
        OrderDetail orderDetail = iOrderDetailService.getOne(new QueryWrapper<OrderDetail>().eq("order_sn", order.getOrderSn()));
        if (orderDetail==null){
            ResponseResult.Step(ResponseResult.ERROR("订单详情查询失败！"));
            return;
        }
        ProfitFlowing profitFlowing = iProfitFlowingService.getOne(new QueryWrapper<ProfitFlowing>().eq("order_sn", orderSn));
        if (profitFlowing==null){
            ResponseResult.Step(ResponseResult.ERROR("订单流水查询失败！"));
            return;
        }
        try {
            map.put("orderSn",orderSn);
            map.put("orderStatus", order.getOrderStatus());
            map.put("address",order.getAddress());
            map.put("shippingUser",order.getShippingUser());
            map.put("cusPhone",order.getCusPhone());
            //支付方式
            map.put("paymentMethod",order.getPaymentMethod());
            //订单总金额
            map.put("orderMoney",order.getOrderMoney());
            //商户名称
            map.put("merName",orderDetail.getMerName());
            map.put("picUrl", orderDetail.getGoodsPictureUrl());
            map.put("goodsName", orderDetail.getGoodsName());
            map.put("activePrice", profitFlowing.getActivityPrice());
            map.put("number", profitFlowing.getNumber());
            //订单总价格
            map.put("orderPriceAll",profitFlowing.getOrderPriceAll());
            //优惠总额
            map.put("subPriceAll", profitFlowing.getSubPriceAll());
            //实际支付价格
            map.put("salePrice", profitFlowing.getSalePrice());
            map.put("goodsDtlDetail", orderDetail.getGoodsDtlDetail());
            map.put("goodsEquityDetail", orderDetail.getGoodsEquityDetail());
            map.put("merPhone",orderDetail.getMerPhone());
            //下单时间
            map.put("orderTime", DateConverterUtil.dateFormat(order.getOrderTime(),DateConverterUtil.dateTime));
            map.put("payTime", DateConverterUtil.dateFormat(order.getPayTime(),DateConverterUtil.dateTime));
            ResponseResult result = ResponseResult.SUCCESS("查询成功！");
            result.setData(map);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
            logger.debug("orderSn==="+orderSn);
            logger.debug("订单详情查询失败====error："+e.getMessage());
        }

    }


    /**
     * 取消订单，
     */
    @RequestMapping("/cancelOrder")
    public void cancelOrder(String orderSn) {
        try {
            Order order = iOrderService.getOne(new QueryWrapper<Order>().eq("order_sn", orderSn).eq("order_status", OrderConstant.ONE_NO_PAY));
            if (order == null) {
                ResponseResult.Step(ResponseResult.ERROR("失败，订单不存在或者状态错误！"));
                return;
            }
            //将订单状态设为0
            order.setOrderStatus(OrderConstant.NO_USE);
            //查询该订单下的商品详情
            OrderDetail orderDetail = iOrderDetailService.getOne(new QueryWrapper<OrderDetail>().eq("order_id", order.getOrderId()));
            if (orderDetail == null) {
                ResponseResult.Step(ResponseResult.ERROR("订单详情为空！"));
                return;
            }
            //更新商品的库存， 库存加上本单的 购买数量
            Goods goods = iGoodsService.getById(orderDetail.getGoodsId());
            goods.setStupperlimit(goods.getStupperlimit() + orderDetail.getProductCnt());
            boolean update = iGoodsService.updateById(goods);
            if (!update) {
                ResponseResult.Step(ResponseResult.ERROR("商品库存更新失败"));
                return;
            }
            boolean b = iOrderService.updateById(order);
            if (b) {
                ResponseResult.Step(ResponseResult.SUCCESS("订单取消成功！"));
            } else {
                ResponseResult.Step(ResponseResult.ERROR("订单取消失败！"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("orderSn===" + orderSn);
            logger.debug("Exception===" + e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("系统异常！"));
        }

    }


    /**
     * 删除订单，
     */
    @RequestMapping("/deleteOrder")
    public void deleteOrder(String orderSn) {
        try {
            Order order = iOrderService.getOne(new QueryWrapper<Order>().eq("order_sn", orderSn).eq("order_status", OrderConstant.NO_USE));
            if (order == null) {
                ResponseResult.Step(ResponseResult.ERROR("失败，订单不存在或者状态错误！"));
                return;
            }
            //将订单状态设为0
            order.setOrderStatus(OrderConstant.DELETE_STATE);
            boolean b = iOrderService.updateById(order);
            if (b) {
                ResponseResult.Step(ResponseResult.SUCCESS("订单删除成功！"));
            } else {
                ResponseResult.Step(ResponseResult.ERROR("订单删除失败！"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("orderSn===" + orderSn);
            logger.debug("Exception===" + e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("系统异常！"));
        }

    }


    /**
     * 订单详情，
     * 页面的订单编号，下单时间，支付时间
     *
     * @param orderSn
     */
    @RequestMapping("/orderDetailVo")
    public void orderDetailVo(String orderSn) {

        ResponseResult<List<Map<String, Object>>> result = new ResponseResult<>();
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        QueryWrapper<Order> eq = queryWrapper.select("order_time orderTime", "pay_time payTime", "order_sn orderSn").eq("order_sn", orderSn);
        List<Map<String, Object>> maps = new ArrayList<>();
        try {
            maps = iOrderService.listMaps(eq);
            result.setOkMsg("查询成功");
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("订单信息 查询失败");
            result.setErrorMsg("查询失败");
        }
        result.setData(maps);
        ResponseResult.Step(result);
    }

    /**
     * 确认收货
     * @param orderSn
     */
    @RequestMapping("/confirmReceive")
    public void confirmReceive(String orderSn){
        try {
            Order order = iOrderService.getOne(new QueryWrapper<Order>().eq("order_sn", orderSn).eq("order_status", OrderConstant.THREE_SHIPPING));
            if (order == null) {
                ResponseResult.Step(ResponseResult.ERROR("失败，订单不存在或者状态错误！"));
                return;
            }
            //将订单状态设为0
            order.setOrderStatus(OrderConstant.FOUR_DONE);
            boolean b = iOrderService.updateById(order);
            if (b) {
                ResponseResult.Step(ResponseResult.SUCCESS("确认收货成功！"));
            } else {
                ResponseResult.Step(ResponseResult.ERROR("确认收货失败！"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("orderSn===" + orderSn);
            logger.debug("Exception===" + e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("系统异常！"));
        }
    }


}


