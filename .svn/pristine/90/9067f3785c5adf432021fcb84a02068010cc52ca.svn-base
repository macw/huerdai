package com.tourism.hu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.ProfitFlowingConstant;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.Goods;
import com.tourism.hu.entity.Order;
import com.tourism.hu.entity.ProfitFlowing;
import com.tourism.hu.entity.vo.GoodsDtlVo;
import com.tourism.hu.mapper.ProfitFlowingMapper;
import com.tourism.hu.service.IBaseService;
import com.tourism.hu.service.IProfitFlowingService;

import com.tourism.hu.util.MsgUtil;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 利润分成表 服务实现类
 * </p>
 *
 * @author Huerdai
 * @since 2019-11-11
 */
@Service
public class ProfitFlowingServiceImpl extends ServiceImpl<ProfitFlowingMapper, ProfitFlowing> implements IProfitFlowingService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IProfitFlowingService iProfitFlowingService;

    @Resource
    private IBaseService iBaseService;

    @Override
    public ResponseResult createProfitFlowing(ProfitFlowing profitFlowing, Order order, Goods goods, GoodsDtlVo goodsDtlVo, Integer num, CustomerInfo customerInfo) {

        profitFlowing.setOrderSn(order.getOrderSn());
        profitFlowing.setIsPay(ProfitFlowingConstant.NO_PAY);
        profitFlowing.setPrice(goods.getPrice());
        //这里的活动价，就是商品的实际规格价
        profitFlowing.setActivityPrice(goodsDtlVo.getGoodsDtlPrice());
        /**
         * 设置优惠券，满减，等其他抵扣值
         * 后期有具体数值后再更新这里，前期设为 0
         */
        profitFlowing.setCouponPrice(BigDecimal.ZERO);
        profitFlowing.setFillPrice(BigDecimal.ZERO);
        profitFlowing.setOtherPriceOne(BigDecimal.ZERO);
        profitFlowing.setOtherPriceTwo(BigDecimal.ZERO);
        profitFlowing.setOtherPriceThree(BigDecimal.ZERO);

        //计算所有抵扣金额总数
        BigDecimal add = profitFlowing.getCouponPrice().add(profitFlowing.getFillPrice()).add(profitFlowing.getOtherPriceOne()).add(profitFlowing.getOtherPriceTwo()).add(profitFlowing.getOtherPriceThree());
        // 实际支付金额, 活动价 减去 抵扣总额
        BigDecimal subtract =goodsDtlVo.getGoodsDtlPrice().subtract(add);
        //实际支付金额
        profitFlowing.setSalePrice(subtract);
        //商品低价
        profitFlowing.setExitPrice(goods.getExitPrice());
        //商品购买数量
        profitFlowing.setNumber(num);

        //代理商利润金额
        BigDecimal AGENT_TYPE = iBaseService.getPercentage(2, profitFlowing.getPrice().multiply(new BigDecimal(profitFlowing.getNumber())), profitFlowing.getExitPrice().multiply(new BigDecimal(profitFlowing.getNumber())), profitFlowing.getActivityPrice().multiply(new BigDecimal(profitFlowing.getNumber())), BaseConstant.AGENT_TYPE);
        //上级利润金额
        BigDecimal PARE_TYPE = iBaseService.getPercentage(2, profitFlowing.getPrice().multiply(new BigDecimal(profitFlowing.getNumber())), profitFlowing.getExitPrice().multiply(new BigDecimal(profitFlowing.getNumber())), profitFlowing.getActivityPrice().multiply(new BigDecimal(profitFlowing.getNumber())), BaseConstant.PARE_TYPE);
        //平台利润金额
        BigDecimal PLAT_TYPE = iBaseService.getPercentage(2, profitFlowing.getPrice().multiply(new BigDecimal(profitFlowing.getNumber())), profitFlowing.getExitPrice().multiply(new BigDecimal(profitFlowing.getNumber())), profitFlowing.getActivityPrice().multiply(new BigDecimal(profitFlowing.getNumber())), BaseConstant.PLAT_TYPE);
        //购买用户利润金额
        BigDecimal USER_TYPE = iBaseService.getPercentage(2, profitFlowing.getPrice().multiply(new BigDecimal(profitFlowing.getNumber())), profitFlowing.getExitPrice().multiply(new BigDecimal(profitFlowing.getNumber())), profitFlowing.getActivityPrice().multiply(new BigDecimal(profitFlowing.getNumber())), BaseConstant.USER_TYPE);


        //获取总利润 ,商品支付金额，乘以 数量， 再减去  低价 乘以数量
        BigDecimal totalMoney = profitFlowing.getSalePrice().multiply(new BigDecimal(profitFlowing.getNumber())).subtract(profitFlowing.getExitPrice().multiply(new BigDecimal(profitFlowing.getNumber())));
        //如果 总利润  小于 0 ，则返回创建 失败
        if (totalMoney.compareTo(BigDecimal.ZERO) == -1) {
//            ResponseResult.Step(ResponseResult.ERROR("价格设置错误，创建失败！"));
            return ResponseResult.ERROR("价格设置错误，创建失败！");
        }
        //设置总利润
        profitFlowing.setTotalMoney(totalMoney);



        logger.debug("getPrice===" + profitFlowing.getPrice().multiply(new BigDecimal(profitFlowing.getNumber())));
        logger.debug("getExitPrice===" + profitFlowing.getExitPrice().multiply(new BigDecimal(profitFlowing.getNumber())));
        logger.debug("getActivityPrice===" + profitFlowing.getActivityPrice().multiply(new BigDecimal(profitFlowing.getNumber())));

        profitFlowing.setCusMoney(USER_TYPE);
        profitFlowing.setParentcusMoney(PARE_TYPE);
        profitFlowing.setAgentMoney(AGENT_TYPE);
        profitFlowing.setPlatformMoney(PLAT_TYPE);
        profitFlowing.setState(ProfitFlowingConstant.NO_STATUS);
        profitFlowing.setCusId(customerInfo.getCustomerId());
        profitFlowing.setCreationDate(LocalDateTime.now());
        profitFlowing.setCreatedId(customerInfo.getCustomerId());
        profitFlowing.setCreatedName(customerInfo.getNickname());
        boolean save1 = iProfitFlowingService.save(profitFlowing);
        if (save1){
            return ResponseResult.SUCCESS("订单流水创建成功！");
        }else {
            return ResponseResult.ERROR("创建失败！");
        }

    }
}
