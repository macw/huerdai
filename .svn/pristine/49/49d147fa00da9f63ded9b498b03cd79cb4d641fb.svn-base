package com.tourism.hu.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * 
 * @ClassName: GoodsDtl 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:05:29 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_goods_dtl")
public class GoodsDtl implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增主键ID
     */
    @TableId(value = "goodsdtl_id", type = IdType.AUTO)
    private Integer goodsdtlId;

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 编码
     */
    private Integer specid;

    /**
     * 规格描述
     */
    private String spec;

    /**
     * 价格
     */
    private BigDecimal dtlPrice;

    /**
     * 描述类型 1    2   3   4  5
     */
    private Integer type;

	/**
	 * 商品规格图片路径
	 */
	private String dtlImg;

    /**
     * 创建人id
     */
    private Integer createUserId;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新人id
     */
    private Integer updateUserId;

    /**
     * 更新人员
     */
    private String updateUser;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 图片地址
     */
    @TableField(exist = false)
    private String classimgAdress;

	/**
	 * 商品名称
	 */
	@TableField(exist = false)
    private String goodsname;


	public String getGoodsname() {
		return goodsname;
	}

	public void setGoodsname(String goodsname) {
		this.goodsname = goodsname;
	}

	public String getDtlImg() {
		return dtlImg;
	}

	public void setDtlImg(String dtlImg) {
		this.dtlImg = dtlImg;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Integer getGoodsdtlId() {
		return goodsdtlId;
	}

	public void setGoodsdtlId(Integer goodsdtlId) {
		this.goodsdtlId = goodsdtlId;
	}

	public Integer getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(Integer goodsId) {
		this.goodsId = goodsId;
	}

	public Integer getSpecid() {
		return specid;
	}

	public void setSpecid(Integer specid) {
		this.specid = specid;
	}

	public String getSpec() {
		return spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}

	public BigDecimal getDtlPrice() {
		return dtlPrice;
	}

	public void setDtlPrice(BigDecimal dtlPrice) {
		this.dtlPrice = dtlPrice;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public LocalDateTime getCreateTime() {
		return createTime;
	}

	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}

	public Integer getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public LocalDateTime getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(LocalDateTime updateTime) {
		this.updateTime = updateTime;
	}

	public String getClassimgAdress() {
		return classimgAdress;
	}

	public void setClassimgAdress(String classimgAdress) {
		this.classimgAdress = classimgAdress;
	}



}
