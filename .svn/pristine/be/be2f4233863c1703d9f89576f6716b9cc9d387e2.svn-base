package com.tourism.hu.apicontroller;

import ch.qos.logback.core.pattern.util.RegularEscapeUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.StrategicTravels;
import com.tourism.hu.service.ICustomerInfoService;
import com.tourism.hu.service.IStrategicTravelsService;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 15:32
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/api/strategicTravelsApi")
public class StrategicTravelsApiController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IStrategicTravelsService iStrategicTravelsService;

    @Resource
    private ICustomerInfoService iCustomerInfoService;

    /**
     * 添加旅游攻略
     * 参数：
     * sTitle 标题
     * sContent 内容
     *
     * @param strategicTravels
     */
    @RequestMapping("/addOrUpdateStrategicTravels")
    public void addOrUpdateStrategicTravels(MultipartFile file, StrategicTravels strategicTravels) {
        CustomerInfo customerInfo = getCustomerInfo();
        if (customerInfo == null) {
            ResponseResult.Step(ResponseResult.ERROR("获取用户信息失败，请重新登录！"));
            return;
        }
        if (file != null && file.getSize() > 0) {
            //获取上传文件的路径
            String realPath = "strategic/travels";
            String url = "";
            try {
                url = upload(file, realPath, customerInfo.getCustomerId());
            } catch (IOException e) {
                e.printStackTrace();
            }
            strategicTravels.setStragePictureUrl(url);
        }
        strategicTravels.setCustomerId(customerInfo.getCustomerId());
        strategicTravels.setStatus(BaseConstant.DISUSE_STATUS);

        if (strategicTravels.getStrageId() == null) {

            strategicTravels.setCreatedId(customerInfo.getCustomerId());
            strategicTravels.setCreationDate(LocalDateTime.now());
            strategicTravels.setCreatedName(customerInfo.getCustomerName());
            if (iStrategicTravelsService.save(strategicTravels)) {
                ResponseResult.Step(ResponseResult.SUCCESS("发布成功！"));
            } else {
                ResponseResult.Step(ResponseResult.ERROR("发布失败！"));
            }
        }else {
            strategicTravels.setLastUpdateDate(LocalDateTime.now());
            strategicTravels.setLastUpdatedId(customerInfo.getCustomerId());
            strategicTravels.setLastUpdateName(customerInfo.getNickname());
            if (iStrategicTravelsService.updateById(strategicTravels)){
                ResponseResult.Step(ResponseResult.SUCCESS("更新成功！"));
            } else {
                ResponseResult.Step(ResponseResult.ERROR("更新失败！"));
            }
        }
    }

    /**
     * 查询所有的攻略
     */
    @RequestMapping("/selectStrategicTravels")
    public void selectStrategicTravels() {
        try {
            List<StrategicTravels> strategicTravelsList = iStrategicTravelsService.list(new QueryWrapper<StrategicTravels>().eq("status", BaseConstant.USE_STATUS).orderByDesc("last_update_date", "creation_date"));
            ResponseResult<List<StrategicTravels>> result = new ResponseResult<>();
            result.setData(strategicTravelsList);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Exception==="+e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
        }
    }

    /**
     * 查询单个攻略
     * 详情
     * @param strageId
     */
    @RequestMapping("/selectDetailTravels")
    public void selectDetailTravels(Integer strageId){
        StrategicTravels strategicTravels = iStrategicTravelsService.getById(strageId);
        if (strategicTravels.getCustomerId()!=null){
            CustomerInfo customerInfo = iCustomerInfoService.getById(strategicTravels.getCustomerId());
            strategicTravels.setNickname(customerInfo.getNickname());
            strategicTravels.setCustomerIcon(customerInfo.getCustomerIcon());
        }
        ResponseResult<StrategicTravels> result = new ResponseResult<>();
        result.setData(strategicTravels);
        ResponseResult.Step(result);
    }


}
