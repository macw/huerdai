package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.GoodsConstant;
import com.tourism.hu.constant.SeckillConstant;
import com.tourism.hu.entity.Goods;
import com.tourism.hu.entity.ScenicSpot;
import com.tourism.hu.entity.Seckill;
import com.tourism.hu.entity.User;
import com.tourism.hu.mapper.GoodsMapper;
import com.tourism.hu.mapper.ScenicSpotMapper;
import com.tourism.hu.mapper.SeckillMapper;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.DateConverterUtil;
import com.tourism.hu.util.MsgUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.swing.text.StringContent;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: SeckillController 
 * @Description: 秒杀表 前端控制器
 * @author 马超伟
 * @date 2019年10月11日 下午2:10:51 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/seckill")
public class SeckillController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private SeckillMapper seckillMapper;

    @Resource
    private GoodsMapper goodsMapper;

    @Resource
    private ScenicSpotMapper scenicSpotMapper;

    /**
     * 去秒杀页面
     *
     * @return
     */
    @RequestMapping("/toseckillList")
    public ModelAndView toseckillList() {
        ModelAndView mv = new ModelAndView("seckill/seckill/seckillList");
        return mv;
    }

    /**
     * 去拼团页面
     *
     * @return
     */
    @RequestMapping("/toseckillGroupList")
    public ModelAndView toseckillGroupList() {
        ModelAndView mv = new ModelAndView("seckill/group/seckillGroupList");
        return mv;
    }

    /**
     * 去秒杀编辑页面
     *
     * @return
     */
    @RequestMapping("/toseckillEdit")
    public ModelAndView toseckillEdit(Integer aid) {
        ModelAndView mv = new ModelAndView("seckill/seckill/seckillEdit");
        Seckill seckill = seckillMapper.selectOne(new QueryWrapper<Seckill>().eq("seckill_id", aid));
        if (seckill != null) {
            if (seckill.getSeckillBegintime() != null) {
                String validityStartdate = DateConverterUtil.dateFormat(seckill.getSeckillBegintime(), DateConverterUtil.dateTime);
                seckill.setBegintime(validityStartdate);
            }
            if (seckill.getSeckillEndtime() != null) {
                String validityEnddate = DateConverterUtil.dateFormat(seckill.getSeckillBegintime(), DateConverterUtil.dateTime);
                seckill.setEndtime(validityEnddate);
            }
        }
        mv.addObject("sk", seckill);
        List<Goods> goodsList = goodsMapper.selectList(new QueryWrapper<Goods>().eq("status", BaseConstant.USE_STATUS));
        mv.addObject("goods", goodsList);
        List<ScenicSpot> scenicSpotList = scenicSpotMapper.selectList(new QueryWrapper<ScenicSpot>().eq("status", BaseConstant.USE_STATUS));
        mv.addObject("spot", scenicSpotList);
        return mv;
    }


    /**
     * 秒杀功能的展示，查询所有秒杀功能
     * @param page
     * @param limit
     * @param seckillName
     * @return
     */
    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page, int limit, String seckillName) {
        Page<Seckill> page1 = new Page<>(page, limit);
        IPage<Seckill> IPage = seckillMapper.selectPage(page1, new QueryWrapper<Seckill>().eq("collage_count", 0));
        if (seckillName != null) {
            IPage = seckillMapper.selectPage(page1, new QueryWrapper<Seckill>().like("seckill_name", seckillName));
        }
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<Seckill> List = IPage.getRecords();
        for (Seckill seckill : List) {
            if (seckill.getType()== SeckillConstant.GOODSID){
                if (seckill.getGoodsId() != null) {
                    Goods goods = goodsMapper.selectById(seckill.getGoodsId());
                    if (goods!=null){
                        seckill.setGoodsName(goods.getGoodsname());
                    }
                }
            }
            if (seckill.getType() == SeckillConstant.SPOTID) {
                ScenicSpot scenicSpot = scenicSpotMapper.selectById(seckill.getGoodsId());
                if (scenicSpot!=null){
                    seckill.setGoodsName(scenicSpot.getSpotName());
                }
            }
        }
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }

    /**
     * 秒杀功能的新增，或者修改操作
     * @param seckill
     * @param spotId
     * @return
     */
    @RequestMapping("/addOrUpdate")
    public MsgUtil addOrUpdate(Seckill seckill,Integer spotId) {
        logger.debug("seckill===="+seckill);
        logger.debug("spotId===="+spotId);
        User user = getUser();
        if (seckill.getType()==SeckillConstant.SPOTID){
            seckill.setGoodsId(spotId);
            logger.debug("goodsId===="+seckill.getGoodsId());
        }
        if (seckill.getSeckillId() == null) {
            //添加
            seckill.setCreatedId(user.getUserId());
            seckill.setCreatedName(user.getName());
            seckill.setCreationDate(LocalDateTime.now());
            seckill.setStatus(BaseConstant.USE_STATUS);
            seckill.setCollageCount(0);
            int i = seckillMapper.insert(seckill);
            return MsgUtil.flag(i);
        } else {
            //更新
            seckill.setLastUpdatedId(user.getUserId());
            seckill.setLastUpdateDate(LocalDateTime.now());
            seckill.setLastUpdateName(user.getName());
            logger.debug("seckill2==="+seckill);
            int i = seckillMapper.updateById(seckill);
            return MsgUtil.flag(i);
        }
    }


    @RequestMapping("/deleteOne")
    public MsgUtil deleteOne(Integer aid) {
        int i = seckillMapper.deleteById(aid);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids) {
        List list = new ArrayList();
        for (Integer id : ids) {
            list.add(id);
        }
        int i = seckillMapper.deleteBatchIds(list);
        return MsgUtil.flag(i);
    }













    /**
     * 拼团。查询所有，拼团数量大于2
     * @param page
     * @param limit
     * @param seckillName
     * @return
     */
    @RequestMapping("/selectGroupAll")
    public DataUtil selectGroupAll(int page, int limit, String seckillName) {
        Page<Seckill> page1 = new Page<>(page, limit);
        IPage<Seckill> IPage = seckillMapper.selectPage(page1, new QueryWrapper<Seckill>().ge("collage_count",2));
        if (seckillName != null) {
            IPage = seckillMapper.selectPage(page1, new QueryWrapper<Seckill>().ge("collage_count",2).like("seckill_name", seckillName));
        }
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<Seckill> List = IPage.getRecords();
        for (Seckill seckill : List) {
            if (seckill.getType()== SeckillConstant.GOODSID){
                if (seckill.getGoodsId() != null) {
                    Goods goods = goodsMapper.selectById(seckill.getGoodsId());
                    if (goods!=null){
                        seckill.setGoodsName(goods.getGoodsname());
                    }
                }
            }
            if (seckill.getType() == SeckillConstant.SPOTID) {
                ScenicSpot scenicSpot = scenicSpotMapper.selectById(seckill.getGoodsId());
                if (scenicSpot!=null){
                    seckill.setGoodsName(scenicSpot.getSpotName());
                }
            }
        }
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }
    /**
     * 去秒杀编辑页面
     *
     * @return
     */
    @RequestMapping("/toGroupEdit")
    public ModelAndView toGroupEdit(Integer aid) {
        ModelAndView mv = new ModelAndView("seckill/group/seckillGroupEdit");
        Seckill seckill = seckillMapper.selectOne(new QueryWrapper<Seckill>().eq("seckill_id", aid));
        if (seckill != null) {
            if (seckill.getSeckillBegintime() != null) {
                String validityStartdate = DateConverterUtil.dateFormat(seckill.getSeckillBegintime(), DateConverterUtil.dateTime);
                seckill.setBegintime(validityStartdate);
            }
            if (seckill.getSeckillEndtime() != null) {
                String validityEnddate = DateConverterUtil.dateFormat(seckill.getSeckillBegintime(), DateConverterUtil.dateTime);
                seckill.setEndtime(validityEnddate);
            }
        }
        mv.addObject("sk", seckill);
        List<Goods> goodsList = goodsMapper.selectList(new QueryWrapper<Goods>().eq("status", BaseConstant.USE_STATUS));
        mv.addObject("goods", goodsList);
        List<ScenicSpot> scenicSpotList = scenicSpotMapper.selectList(new QueryWrapper<ScenicSpot>().eq("status", BaseConstant.USE_STATUS));
        mv.addObject("spot", scenicSpotList);
        return mv;
    }

    /**
     * 拼团功能的添加或者更新实现
     * @param seckill
     * @param spotId
     * @return
     */
    @RequestMapping("/addOrUpdateGroup")
    public MsgUtil addOrUpdateGroup(Seckill seckill,Integer spotId) {
        logger.debug("seckill===="+seckill);
        logger.debug("spotId===="+spotId);
        User user = getUser();
        if (seckill.getType()==SeckillConstant.SPOTID){
            seckill.setGoodsId(spotId);
            logger.debug("goodsId===="+seckill.getGoodsId());
        }
        if (seckill.getSeckillId() == null) {
            //添加
            seckill.setCreatedId(user.getUserId());
            seckill.setCreatedName(user.getName());
            seckill.setCreationDate(LocalDateTime.now());
            seckill.setStatus(BaseConstant.USE_STATUS);
            int i = seckillMapper.insert(seckill);
            return MsgUtil.flag(i);
        } else {
            //更新
            seckill.setLastUpdatedId(user.getUserId());
            seckill.setLastUpdateDate(LocalDateTime.now());
            seckill.setLastUpdateName(user.getName());
            logger.debug("seckill2==="+seckill);
            int i = seckillMapper.updateById(seckill);
            return MsgUtil.flag(i);
        }
    }


}
