package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.User;
import com.tourism.hu.entity.Wallet;
import com.tourism.hu.entity.dto.WalletDto;
import com.tourism.hu.mapper.WalletMapper;
import com.tourism.hu.service.ICustomerInfoService;
import com.tourism.hu.service.IWalletService;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: WalletController 
 * @Description: 钱包
 * @author 董兴隆
 * @date 2019年10月15日 下午1:04:57 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/wallet")
public class WalletController extends BaseController{
	

	@Resource
    private WalletMapper walletMapper;
	
	@Autowired
	private IWalletService walletService;
	
	/**
	 * 用户信息service
	 */
	@Autowired
	private ICustomerInfoService customerInfoService;
	
	
	
	
	@RequestMapping("/page")
    public ModelAndView page() {
        ModelAndView mv = new ModelAndView("/wallet/walletList");
        return mv;
    }
	@RequestMapping("/editPage")
	public ModelAndView editPage(Wallet dto) {
		ModelAndView mv = new ModelAndView("/wallet/walletEdit");
		List<CustomerInfo> customerInfoList = customerInfoService.list();
		mv.addObject("customerInfoList", customerInfoList);
		if(dto.getId()!=null && dto.getId()>0) {
			Wallet wallet = walletService.getById(dto.getId());
			mv.addObject("dto", wallet);
		}
		return mv;
	}
	
	

	@RequestMapping("/selectData")
	@ResponseBody
    public ResultUtil<List<WalletDto>> selectData(WalletDto dto){
		PageUtil.initAttr(dto);
		List<WalletDto> list = new ArrayList<WalletDto>();
		ResultUtil<List<WalletDto>> result = ResultUtil.error("查询失败",list);
		
		
        QueryWrapper<Wallet> qw = new QueryWrapper<Wallet>();
        qw.eq(!StringUtils.isEmpty(dto.getName()), "name", dto.getName());
        qw.orderByDesc("creation_date");
        try {
        	long count = walletMapper.getCount(dto);
        	if(count>0) {
        		 list = walletMapper.selectPageDataList(dto);
        	}
        	result = ResultUtil.ok("查询成功",list);
        	result.setData(list);
        	result.setCount(count);
        }catch (Exception e) {
        	result.setMsg(e.getMessage());
		}
        return result;
    }
    
	
	@RequestMapping("/deleteById")
	@ResponseBody
	public ResultUtil<Wallet> deleteById(Wallet dto){
		if(walletService.removeById(dto.getId())) {
			return ResultUtil.ok("删除成功");
		}else {
			return ResultUtil.error("删除失败");
		}
	}
	
	@RequestMapping("/saveOrUpdate")
	@ResponseBody
	public ResultUtil<Wallet> saveOrUpdate(Wallet dto){
		if(isNotLogIn()) {
			return ResultUtil.error("未找到当前登录用户");
		}
		User user = getUser();
		boolean save=true;
		if(dto.getId()!=null && dto.getId()>0) {
			save=false;
			dto.setLastUpdateDate(LocalDateTime.now());
			dto.setLastUpdatedId(user.getUserId());
			dto.setLastUpdateName(user.getName());
		}else {
			dto.setCreatedId(user.getUserId());
			dto.setCreatedName(user.getName());
			dto.setCreationDate(LocalDateTime.now());
		}
		if(walletService.saveOrUpdate(dto)) {
			if(save) {
				return ResultUtil.ok("添加成功");
			}else {
				return ResultUtil.ok("修改成功");
			}
		}else {
			if(save) {
				return ResultUtil.error("添加失败");
			}else {
				return ResultUtil.error("修改失败");
			}
		}
	}
}
	
