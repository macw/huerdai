package com.tourism.hu.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.ProfitSaringConstant;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.ProfitSaring;
import com.tourism.hu.entity.Region;
import com.tourism.hu.entity.User;
import com.tourism.hu.mapper.CustomerInfoMapper;
import com.tourism.hu.mapper.ProfitSaringMapper;
import com.tourism.hu.mapper.RegionMapper;
import com.tourism.hu.mapper.UserMapper;
import com.tourism.hu.util.FileUploadUtil;
import com.tourism.hu.util.GlobalConfigUtil;

@RestController
public class BaseController {

	Logger logger  = LoggerFactory.getLogger(getClass());

	@Resource
	private UserMapper userMapper;

	@Resource
	private RedisTemplate redisTemplate;

	@Resource
	private CustomerInfoMapper customerInfoMapper;

	@Resource
	private RegionMapper regionMapper;

    @Resource
    private ProfitSaringMapper profitSaringMapper;


    public  HttpServletRequest getRequest() {
		return ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
	}

    public String isNullNotAppend(Object obj){
    	if(obj == null || StringUtils.isBlank(obj.toString())) {
    		return "";
    	}
    	return obj.toString();
    }
    
    public String isNullNotAppend(String str){
    	if(str == null || StringUtils.isBlank(str.toString())) {
    		return "";
    	}
    	return str.toString();
    }
	/**
	 * 获取后台登录用户
	 * @return
	 */
	public User getUser() {
		String sessionid = getRequest().getSession().getId();
    	Object obj = redisTemplate.opsForValue().get(sessionid);
		String userid = "";
    	if(obj!=null) {
    		userid=obj.toString();
    	}
		User user = userMapper.selectOne(new QueryWrapper<User>().eq("user_id", userid));
		return user;
	}

	/**
	 * 获取前台登录用户
	 * @return
	 */
	public  CustomerInfo getCustomerInfo() {
		String sessionid = getRequest().getSession().getId();
		Object obj = redisTemplate.opsForValue().get(sessionid);
		String customerId = "";
    	if(obj!=null) {
			customerId=obj.toString();
    	}
		CustomerInfo customerInfo = customerInfoMapper.selectOne(new QueryWrapper<CustomerInfo>().eq("id", customerId));
		return customerInfo;
	}

	/**
	 * 是否为VIP， true，VIP 。
	 * false 普通用户
	 * @return
	 */
	public   boolean isVIP(){
		CustomerInfo customerInfo = getCustomerInfo();
		if (customerInfo!=null){
			if (customerInfo.getIsCuslevel()!=null && customerInfo.getIsCuslevel()==1){
				return true;
			}else {
				return false;
			}
		}else {
			return false;
		}
	}

	/**
	 * 从shiro中取用户信息
	 * @return
	 */
	public Session shiroSession() {
		Subject subject = SecurityUtils.getSubject();
		Session session = subject.getSession();
		return session;
	}
	
	/**
	 * 
	 * @Title: getSessionUser 
	 * @Description: (从shrio 中获取用户信息) 
	 * @return  
	 * @date 2019年11月9日 上午11:33:20
	 * @author 董兴隆
	 */
	public CustomerInfo getSessionUser() {
		Session session = shiroSession();
		CustomerInfo cust =  (CustomerInfo) session.getAttribute("LOGIN_USER");//Constant.LOGIN_USER
		if(cust==null) {
			cust = new CustomerInfo();
			cust.setCustomerId(1);
			cust.setCustomerName("测试用户");
			session.setAttribute("LOGIN_USER", cust);
		}
		return  cust;
	}
	/**
	 * 
	 * @Title: isLogIn 
	 * @Description: (如果账号登录返回true) 
	 * @return  
	 * @date 2019年10月15日 上午11:06:01
	 * @author 董兴隆
	 */
	public boolean isLogIn() {
		User user = getUser();
		if (user != null && user.getUserId() != null && user.getUserId() > 0) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * @Title: isNotLogIn 
	 * @Description: (如果账号未登录返回true) 
	 * @return  
	 * @date 2019年10月15日 上午11:05:40
	 * @author 董兴隆
	 */
	public boolean isNotLogIn() {
		User user = getUser();
		if (user != null && user.getUserId() != null && user.getUserId() > 0) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 
	 * @Title: getFullAddress
	 * @Description: (通过省市区Id 获得 地址)
	 * @param province
	 * @param city
	 * @param district
	 * @return
	 * @date 2019年10月11日 上午11:27:31
	 * @author 董兴隆
	 */
	public String getFullAddress(Integer province, Integer city, Integer district) {
		Region reg = new Region();
		String address = "";
		if (province != null && province > 0) {
			reg = regionMapper.selectById(province);
			if (reg != null) {
				address = reg.getName();
			}
		}
		if (city != null && city > 0) {
			reg = regionMapper.selectById(city);
			if (reg != null) {
				address += reg.getName();
			}
		}
		if (district != null && district > 0) {
			reg = regionMapper.selectById(district);
			if (reg != null) {
				address += reg.getName();
			}
		}
		return address;
	}

	/**
	 * 获取全地址
	 * @param province
	 * @param city
	 * @param district
	 * @param address
	 * @return
	 */
	public String getFullAddress(Integer province, Integer city, Integer district, String address) {
		String fullAddress = getFullAddress(province, city, district);
		return fullAddress + address;
	}

	/**
	 * 获取中国全部省份名
	 * @return
	 */
	public List<Region> selectProvinceList() {
		return regionMapper.selectList(new QueryWrapper<Region>().eq("pid", 100000)); // 100000 中国
	}
	/**
	 * 
	 * @Title: getRegion 
	 * @Description: (通过地区Id获得地区详情) 
	 * @param regionId
	 * @return  
	 * @date 2019年11月9日 下午3:03:45
	 * @author 董兴隆
	 */
	public Region getRegion(Integer regionId) {
		return regionMapper.selectById(regionId);
	}

	/**
	 * 通过pid 获取下级地址集合
	 * @param pid
	 * @return
	 */
	public List<Region> selectByPidRegionList(Integer pid) {
		return regionMapper.selectList(new QueryWrapper<Region>().eq("pid", pid)); 
	}

	/**
	 * 获取本地项目路径
	 * @param path
	 * @return
	 */
	public String getRealPath(String path) {
		if(path!=null && path.length()>0) {
			if(path.substring(0, 1).equals("/")) {
				path=path.substring(1,path.length());
			}
		}
		return GlobalConfigUtil.getUploadUrl()+path;
	}

	/**
	 * 获取web访问路径
	 * @return
	 */
	public String getWebUrl() {
		return GlobalConfigUtil.getServiceUrl();

	}

	public String getUploadUrl() {
		return GlobalConfigUtil.getUploadUrl();
	}
	/**
	 * 上传方法
	 * @param file
	 * @param basePath
	 * @return
	 * @throws IOException
	 */
	public String upload(MultipartFile file,String basePath) throws IOException {
		String realPath = getRealPath(basePath);
		logger.info("传入参数路径realPath===============>"+realPath);
		String url = FileUploadUtil.baseUpload(file,realPath);
		logger.info("生成图片路径url===============>"+url);
		url = basePath+url;
		logger.info("存入数据库路径returnUrl===============>"+url);
		return url;
	}
	public String upload(MultipartFile file,String basePath,Integer userId) throws IOException {
		String realPath = getRealPath(basePath);
		logger.info("传入参数路径realPath===============>"+realPath);
		String url = FileUploadUtil.baseUpload(file,realPath,userId);
		logger.info("生成图片路径url===============>"+url);
		url = basePath+url;
		logger.info("存入数据库路径returnUrl===============>"+url);
		return url;
	}

	/**
	 * 获取处理后的UUID
	 * @return
	 */
	public static String getUUID() {
		String uuid = UUID.randomUUID().toString();
		uuid = uuid.replace("-", "");
		return uuid.toLowerCase();
	}

    /**
     * 计算利润
     * 商品单价，低价(出厂价)、活动价
     * 计算方法：
     * 如果有活动价，就活动价减去低价，
     * 没有活动价就商品单价减去低价
     *
     * @return
     */
    public static BigDecimal getProfits( BigDecimal price,  BigDecimal exitPrice,  BigDecimal activePrice){


        /**
         * 价格与0做比较，
         * 大于0，priceUP==1
         * 等于0，priceUP==0
         * 小于0，priceUP==-1
         */
        if(activePrice!=null && activePrice.compareTo(BigDecimal.ZERO)==1){
            return activePrice.subtract(exitPrice);
        }else if(price!=null &&  price.compareTo(BigDecimal.ZERO)==1){
            return price.subtract(exitPrice);
        }else{
            return BigDecimal.ZERO;
        }
    }

    /**
     * 用户对于某个商品的省钱价格
     * @return
     */
    public  BigDecimal getPercentage(BigDecimal price,  BigDecimal exitPrice,  BigDecimal activePrice){
		BigDecimal profits = getProfits(price, exitPrice, activePrice);
		int isvip= ProfitSaringConstant.ORD_TYPE;
		//判断是否为VIP
		if (isVIP()){
			isvip=ProfitSaringConstant.MEM_TYPE;
		}
		ProfitSaring profitSaring = profitSaringMapper.selectOne(new QueryWrapper<ProfitSaring>().eq("type", isvip).eq("status", ProfitSaringConstant.GOODS_STATUS));
		//用户分成百分比
		if(profitSaring==null) {
			return BigDecimal.ZERO;
		}
		double getpPercentage = profitSaring.getpPercentage()*0.01;
		BigDecimal num = new BigDecimal(getpPercentage);
		//计算节省金额
		BigDecimal multiply = profits.multiply(num);
		//四舍五入只保留两位小数，如，2.35变成2.4
		return  multiply.setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 *
	 * @param status 1 旅游商品 2  商场商品
	 * @param price	 销售价   （销售价  活动 必选一个）
	 * @param exitPrice  底价   （不能为空）
	 * @param activePrice 活动价
	 * @param userType
	 * 返佣类型
	 * AGENT_TYPE = 1 代理商分成 百分比 int
	 * USER_TYPE = 2; 用户分成
	 * PLAT_TYPE = 3; 平台分成
	 * PARE_TYPE = 4; 上级分成
	 * @return
	 */
	public  BigDecimal getPercentage(Integer status,BigDecimal price,  BigDecimal exitPrice,  BigDecimal activePrice,Integer userType){
		BigDecimal profits = getProfits(price, exitPrice, activePrice);
		int isvip= ProfitSaringConstant.ORD_TYPE;
		//判断是否为VIP
		if (isVIP()){
			isvip=ProfitSaringConstant.MEM_TYPE;
		}
		ProfitSaring profitSaring = profitSaringMapper.selectOne(new QueryWrapper<ProfitSaring>().eq("type", isvip).eq("status", status));
		if(profitSaring==null) {
			return BigDecimal.ZERO;
		}
		//用户分成百分比
		double getpPercentage = 0d;
		if (userType== BaseConstant.AGENT_TYPE && profitSaring.getAgentPercentage()!=null && profitSaring.getAgentPercentage()>0){
			getpPercentage=profitSaring.getAgentPercentage()*0.01;
		}else if(userType==BaseConstant.USER_TYPE && profitSaring.getpPercentage()!=null && profitSaring.getpPercentage()>0){
			getpPercentage=profitSaring.getpPercentage()*0.01;
		}else if (userType==BaseConstant.PLAT_TYPE && profitSaring.getPlatformPercentage()!=null && profitSaring.getPlatformPercentage()>0){
			getpPercentage=profitSaring.getPlatformPercentage()*0.01;
		}else if(userType==BaseConstant.PARE_TYPE && profitSaring.getParentPPercentage()!=null && profitSaring.getParentPPercentage()>0){
			getpPercentage=profitSaring.getParentPPercentage()*0.01;
		}
		//计算百分比
		BigDecimal percent = new BigDecimal(getpPercentage);
		//计算节省金额
		BigDecimal multiply = profits.multiply(percent);
		//四舍五入只保留两位小数，如，2.35变成2.4
		return  multiply.setScale(2, BigDecimal.ROUND_HALF_UP);
	}



	
}
