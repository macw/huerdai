package com.tourism.hu.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.tourism.hu.util.PageUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @ClassName: CustomerInfo 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:04:42 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tqk_user")
public class CustomerInfo extends PageUtil implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增主键ID
     * customer_login表的自增ID
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer customerId;

    /**
     * 用户真实姓名
     */
    @TableField("realname")
    private String customerName;

    /**
     * 用户头像
     */
    @TableField("avatar")
    private String customerIcon;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 用户登录账号
     */
    @TableField("username")
    private String loginName;

    /**
     * md5加密的密码
     */
    private String password;

    /**
     * 用户状态
     */
    @TableField("status")
    private Integer userStats;

    /**
     * 注册类型 1邀请注册链接 2网页注册 3扫码注册
     */
    private Integer regtype;

    /**
		 * 邀请码
     */
    @TableField("invocode")
    private String invitationcode;


    /**
     * 手机号
     */
    @TableField("phone")
    private String mobilePhone;

    /**
     * 邮箱
     */
    @TableField("email")
    private String customerEmail;

    /**
     * 性别（1,男，2女，0未知）
     */
    private String gender;

    /**
     * 用户积分
     */
    @TableField("score")
    private Integer userPoint;

    /**
     * 注册时间
     */
    private LocalDateTime registerTime;

	/**
	 * 会员到期时间
	 */
	private LocalDateTime endTime;

	/**
	 * 注册时间
	 */
	private Integer regTime;

    /**
     * 会员级id
     */
    private Integer customerLevelId;

	/**
	 * 是否会员 1是 2不是
	 */
	private Integer isCuslevel;


    /**
     * 会员级别
     */
    private String customerLevelName;

    /**
     * 用户余额
     */
    @TableField("money")
    private BigDecimal userMoney;

    /**
     * 最后登录时间
     */
    private LocalDateTime modifiedTime;
	/**
	 * 最后登录时间
	 */
	private Integer lastTime;


	/**
	 * 关注数量
	 */
	private Integer followcount;

	/**
	 * 粉丝数量
	 */
	private Integer fanscount;

	/**
	 * 达人描述
	 */
	private String darenMemo;

	/**
	 *微信openId
	 */
	private String openid;

	/**
	 * 用户所在省份
	 */
	private String province;

	/**
	 * 用户所在城市
	 */
	private String city;

	/**
	 * 用户所在国家
	 */
	private String country;

	/**
	 * 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
	 */
	private String unionid;

	/**
	 * 冻结资金
	 */
	private Double freezeFee;

	/**
	 * 微信账号
	 */
	private String wechat;

	/**
	 * qq账号
	 */
	private String qq;

	/**
	 * 地址
	 */
	private String address;

	/**
	 * 注册ip
	 */
	private String regIp;

	/**
	 * 登录ip
	 */
	private String lastIp;

	/**
	 * 登录次数
	 */
	private Integer loginCount;

	/**
	 * 创建时间，暂时弃用
	 */
	private Integer createTime;

	/**
	 *
	 */
	private int state;

	/**
	 * 预估收入
	 */
	private Double frozen;

	private Integer webmaster;

	private String webmasterPid;

	private Integer webmasterRate;

	/**
	 * 拼多多联盟id
	 */
	private String pddPid;

	/**
	 * 1级用户id
	 */
	private String oid;

	/**
	 * 2级用户id
	 */
	private Integer fuid;

	/**
	 * 3级用户id
	 */
	private Integer guid;

	/**
	 * 支付宝账号
	 */
	private String alipay;

	private String relationId;

	private String specialId;

	/**
	 * 淘宝昵称
	 */
	private String tbname;

	/**
	 * 代理商id， 关联后台用户主键id
	 */
	private Integer userId;

	/**
	 * 上级邀请用户id
	 */
	private Integer parentId;



}
