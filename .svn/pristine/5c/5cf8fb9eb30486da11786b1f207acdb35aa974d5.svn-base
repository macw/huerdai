package com.tourism.hu.controller;

import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.QaparBusiness;
import com.tourism.hu.entity.User;
import com.tourism.hu.mapper.QaparBusinessMapper;
import com.tourism.hu.service.IQaparBusinessService;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * 
 * @ClassName: LogisticsBusinessController
 * @Description: (物流商家controller)
 * @author 董兴隆
 * @date 2019年9月26日 下午3:00:33
 *
 */
@RestController
@RequestMapping("/logistics/business")
public class LogisticsBusinessController extends BaseController{

	@Resource
	private QaparBusinessMapper qaparBusinessMapper;
	
	@Autowired
	private IQaparBusinessService businessService;

	/**
	 * 
	 * @Title: page
	 * @Description: (物流商家页面)
	 * @return
	 * @date 2019年9月23日 上午11:59:53
	 * @author 董兴隆
	 */
	@RequestMapping("/page")
	public ModelAndView page() {
		return new ModelAndView("/logistics/business/logisticsBusinessList");
	}

	/**
	 * 
	 * @Title: editPage 
	 * @Description: (添加编辑页面) 
	 * @return  
	 * @date 2019年9月26日 下午3:07:22
	 * @author 董兴隆
	 */
	@RequestMapping("/editPage")
	public ModelAndView editPage(QaparBusiness business) {
		ModelAndView mv = new ModelAndView("/logistics/business/logisticsBusinessEdit");
		if(business!=null && business.getBusinessId()!=null&& business.getBusinessId()>0) {
			business= businessService.getById(business.getBusinessId());
			mv.addObject("dto", business);
		}
		return mv;
	}
	
	@RequestMapping("/selectData")
	public ResultUtil<List<QaparBusiness>> selectAll(QaparBusiness qapar) {
		ResultUtil<List<QaparBusiness>> dataUtil = ResultUtil.error("查询失败");
		PageUtil.initAttr(qapar);
		IPage<QaparBusiness> iPage = null;
		Page<QaparBusiness> page = new Page<>(qapar.getPage(), qapar.getLimit());
		QueryWrapper<QaparBusiness> qw = new QueryWrapper<QaparBusiness>();
		if (!StringUtils.isEmpty(qapar.getBusinessName())) {
			qw.like("business_name", qapar.getBusinessName());
		}
		if(!StringUtils.isEmpty(qapar.getCreatedName())) {
			qw.and(wrapper -> wrapper.like("created_name",qapar.getCreatedName()).or().like("last_update_name", qapar.getCreatedName()));
		}
		qw.orderByDesc("business_id");
		try {
			//iPage = qaparBusinessMapper.selectPage(page, qw);
			iPage = businessService.page(page,qw);
			dataUtil = ResultUtil.success();
		} catch (Exception e) {
			e.printStackTrace();
		}
		dataUtil.setData(iPage.getRecords());
		dataUtil.setCount(iPage.getTotal());
		return dataUtil;
	}

	@RequestMapping("/deleteById")
	@ResponseBody
	public ResultUtil deleteById(@RequestParam(value="businessId")Integer businessId) {
		if(businessService.removeById(businessId)) {
			return ResultUtil.success("删除成功");
		}else {
			return ResultUtil.error("删除失败");
		}
	}
	@RequestMapping("/saveOrUpdate")
	@ResponseBody
	public ResultUtil saveOrUpdate(QaparBusiness business) {
		User user = getUser();
		if(business!=null && business.getBusinessId() != null && business.getBusinessId()>0) {
			business.setLastUpdateDate(LocalDateTime.now());
			business.setLastUpdatedId(user.getUserId());
			business.setLastUpdateName(user.getName());
		}else {
			business.setCreationDate(LocalDateTime.now());
			business.setCreatedId(user.getUserId());
			business.setCreatedName(user.getName());
			business.setStatus(1);
		}
		if(businessService.saveOrUpdate(business)) {
			return ResultUtil.success("操作成功");
		}else {
			return ResultUtil.error("操作失败");
		}
	}

}
