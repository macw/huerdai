package com.tourism.fzll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @ClassName: Push 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:08:48 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_push")
public class Push implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 推送ID
     */
    @TableId(type=IdType.AUTO,value="push_id")
    private Integer pushId;

    /**
     * 推送标题
     */
    private String pushTitle;

    /**
     * 推送内容
     */
    private String pushContent;

    /**
     * 状态 1启用 2禁用
     */
    private String status;

    /**
     * 备注
     */
    private String memo;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建者名称
     */
    private String createdName;

    /**
     * 创建者id
     */
    private Integer createdId;

    /**
     * 最后更新者id
     */
    private Integer lastUpdatedId;

    /**
     * 最后更新日期
     */
    private LocalDateTime lastUpdateDate;

    /**
     * 最后更新人名称
     */
    private String lastUpdateName;


}
