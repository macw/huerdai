package com.tourism.hu.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @ClassName: CustomerInfo 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:04:42 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_customer_info")
public class CustomerInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增主键ID
     * customer_login表的自增ID
	 */
	@TableId(value = "customer_id", type = IdType.AUTO)
	private Integer customerId;

    /**
     * 用户真实姓名
     */
    private String customerName;

    /**
     * 用户头像
     */
    private String customerIcon;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 用户登录名
     */
    private String loginName;

    /**
     * md5加密的密码
     */
    private String password;

    /**
     * 用户状态
     */
    private Integer userStats;

    /**
     * 注册类型 1邀请注册链接 2网页注册 3扫码注册
     */
    private Integer regtype;

    /**
     * 邀请码
     */
    private String invitationcode;

    /**
     * 证件类型：1 身份证，2 军官证，3 护照
     */
    private Integer identityCardType;

    /**
     * 证件号码
     */
    private String identityCardNo;

    /**
     * 手机号
     */
    private String mobilePhone;

    /**
     * 邮箱
     */
    private String customerEmail;

    /**
     * 性别
     */
    private String gender;

    /**
     * 用户积分
     */
    private Integer userPoint;

    /**
     * 注册时间
     */
    private LocalDateTime registerTime;

    /**
     * 会员生日
     */
    private LocalDateTime birthday;

    /**
     * 会员级id
     */
    private Integer customerLevelId;

	/**
	 * 是否会员 1是 2不是
	 */
	private Integer isCuslevel;


    /**
     * 会员级别
     */
    private String customerLevelName;

    /**
     * 用户余额
     */
    private BigDecimal userMoney;

    /**
     * 最后修改时间
     */
    private LocalDateTime modifiedTime;
    
    /**
     * 是否是经纪人
     */
    private Integer isBroker;

	/**
	 * 是否代理商
	 */
	private Integer isAgent;

	/**
	 * 代理商主键
	 */
	private Integer agenId;

	/**
	 * 是否旅游公司
	 */
    private Integer isOperator;

	/**
	 * 旅游公司主键
	 */
	private Integer tId;

	/**
	 * 关注数量
	 */
	private Integer followcount;

	/**
	 * 粉丝数量
	 */
	private Integer fanscount;

	/**
	 * 达人描述
	 */
	private String darenMemo;

	/**
	 *微信openId
	 */
	private String openid;

	/**
	 * 用户所在省份
	 */
	private String province;

	/**
	 * 用户所在城市
	 */
	private String city;

	/**
	 * 用户所在国家
	 */
	private String country;

	/**
	 * 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
	 */
	private String unionid;


	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

	public Integer getIsAgent() {
		return isAgent;
	}

	public void setIsAgent(Integer isAgent) {
		this.isAgent = isAgent;
	}

	public Integer getAgenId() {
		return agenId;
	}

	public void setAgenId(Integer agenId) {
		this.agenId = agenId;
	}

	public Integer getIsOperator() {
		return isOperator;
	}

	public void setIsOperator(Integer isOperator) {
		this.isOperator = isOperator;
	}

	public Integer gettId() {
		return tId;
	}

	public void settId(Integer tId) {
		this.tId = tId;
	}

	public Integer getFollowcount() {
		return followcount;
	}

	public void setFollowcount(Integer followcount) {
		this.followcount = followcount;
	}

	public Integer getFanscount() {
		return fanscount;
	}

	public void setFanscount(Integer fanscount) {
		this.fanscount = fanscount;
	}

	public String getDarenMemo() {
		return darenMemo;
	}

	public void setDarenMemo(String darenMemo) {
		this.darenMemo = darenMemo;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerIcon() {
		return customerIcon;
	}

	public void setCustomerIcon(String customerIcon) {
		this.customerIcon = customerIcon;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getUserStats() {
		return userStats;
	}

	public void setUserStats(Integer userStats) {
		this.userStats = userStats;
	}

	public Integer getRegtype() {
		return regtype;
	}

	public void setRegtype(Integer regtype) {
		this.regtype = regtype;
	}

	public String getInvitationcode() {
		return invitationcode;
	}

	public void setInvitationcode(String invitationcode) {
		this.invitationcode = invitationcode;
	}

	public Integer getIdentityCardType() {
		return identityCardType;
	}

	public void setIdentityCardType(Integer identityCardType) {
		this.identityCardType = identityCardType;
	}

	public String getIdentityCardNo() {
		return identityCardNo;
	}

	public void setIdentityCardNo(String identityCardNo) {
		this.identityCardNo = identityCardNo;
	}

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getUserPoint() {
		return userPoint;
	}

	public void setUserPoint(Integer userPoint) {
		this.userPoint = userPoint;
	}

	public LocalDateTime getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(LocalDateTime registerTime) {
		this.registerTime = registerTime;
	}

	public LocalDateTime getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDateTime birthday) {
		this.birthday = birthday;
	}

	public Integer getCustomerLevelId() {
		return customerLevelId;
	}

	public void setCustomerLevelId(Integer customerLevelId) {
		this.customerLevelId = customerLevelId;
	}

	public String getCustomerLevelName() {
		return customerLevelName;
	}

	public void setCustomerLevelName(String customerLevelName) {
		this.customerLevelName = customerLevelName;
	}

	public BigDecimal getUserMoney() {
		return userMoney;
	}

	public void setUserMoney(BigDecimal userMoney) {
		this.userMoney = userMoney;
	}

	public LocalDateTime getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(LocalDateTime modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	public Integer getIsBroker() {
		return isBroker;
	}

	public void setIsBroker(Integer isBroker) {
		this.isBroker = isBroker;
	}

	public Integer getIsCuslevel() {
		return isCuslevel;
	}

	public void setIsCuslevel(Integer isCuslevel) {
		this.isCuslevel = isCuslevel;
	}
}
