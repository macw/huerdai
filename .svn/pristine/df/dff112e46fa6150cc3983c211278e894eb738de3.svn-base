package com.tourism.fzll.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tourism.fzll.util.PageUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

 
/**
 * 
 * @ClassName: ResidenceRoom 
 * @Description: (民宿房间表) 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:10:11 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_residence_room")
public class ResidenceRoom extends PageUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 民宿房间ID
	 */
	@TableId(value = "residenceroom_id", type = IdType.AUTO)
	private Integer residenceroomId;

	/**
	 * 民宿房间名称
	 */
	private String residenceroomName;
	
	/**
	 * 民宿Id
	 */
	private Integer residenceId;
	
	
	/**
	 * 房间价
	 */
	private BigDecimal price;

	/**
	 * 状态 1启用 2禁用
	 */
	private Integer status;

	/**
	 * 备注
	 */
	private String memo;

	/**
	 * 房间信息
	 */
	private String roomInfo;
	/**
	 * 创建日期
	 */
	private LocalDateTime creationDate;

	/**
	 * 创建者名称
	 */
	private String createdName;

	/**
	 * 创建者id
	 */
	private Integer createdId;

	/**
	 * 最后更新者id
	 */
	private Integer lastUpdatedId;

	/**
	 * 最后更新日期
	 */
	private LocalDateTime lastUpdateDate;

	/**
	 * 最后更新人名称
	 */
	private String lastUpdateName;

	public Integer getResidenceroomId() {
		return residenceroomId;
	}

	public void setResidenceroomId(Integer residenceroomId) {
		this.residenceroomId = residenceroomId;
	}

	public String getResidenceroomName() {
		return residenceroomName;
	}

	public void setResidenceroomName(String residenceroomName) {
		this.residenceroomName = residenceroomName;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreatedName() {
		return createdName;
	}

	public void setCreatedName(String createdName) {
		this.createdName = createdName;
	}

	public Integer getCreatedId() {
		return createdId;
	}

	public void setCreatedId(Integer createdId) {
		this.createdId = createdId;
	}

	public Integer getLastUpdatedId() {
		return lastUpdatedId;
	}

	public void setLastUpdatedId(Integer lastUpdatedId) {
		this.lastUpdatedId = lastUpdatedId;
	}

	public LocalDateTime getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getLastUpdateName() {
		return lastUpdateName;
	}

	public void setLastUpdateName(String lastUpdateName) {
		this.lastUpdateName = lastUpdateName;
	}

	public Integer getResidenceId() {
		return residenceId;
	}

	public void setResidenceId(Integer residenceId) {
		this.residenceId = residenceId;
	}

	public String getRoomInfo() {
		return roomInfo;
	}

	public void setRoomInfo(String roomInfo) {
		this.roomInfo = roomInfo;
	}
	
	

}
