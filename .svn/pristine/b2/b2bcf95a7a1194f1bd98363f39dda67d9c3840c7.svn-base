package com.tourism.hu.apicontroller;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.CustomerPassenger;
import com.tourism.hu.entity.ProfitFlowing;
import com.tourism.hu.entity.Region;
import com.tourism.hu.entity.RoutesOrderPassenger;
import com.tourism.hu.entity.ScenicComment;
import com.tourism.hu.entity.ScenicOrder;
import com.tourism.hu.entity.ScenicRoutes;
import com.tourism.hu.entity.ScenicRoutesprice;
import com.tourism.hu.entity.ScenicRoutesspot;
import com.tourism.hu.entity.ScenicSpot;
import com.tourism.hu.entity.Seckill;
import com.tourism.hu.entity.TourOperator;
import com.tourism.hu.entity.dto.ScenicRoutesDto;
import com.tourism.hu.entity.vo.JumpMakeOrderVo;
import com.tourism.hu.entity.vo.MakeOrderVo;
import com.tourism.hu.entity.vo.RoutesCommentVo;
import com.tourism.hu.entity.vo.RoutesInfoVo;
import com.tourism.hu.pay.wxsdk.TradeType;
import com.tourism.hu.pay.wxsdk.WXPay;
import com.tourism.hu.pay.wxsdk.WXPayUtil;
import com.tourism.hu.pay.wxsdk.WeixinConfig;
import com.tourism.hu.service.ICustomerInfoService;
import com.tourism.hu.service.ICustomerPassengerService;
import com.tourism.hu.service.IProfitFlowingService;
import com.tourism.hu.service.IRegionService;
import com.tourism.hu.service.IRoutesOrderPassengerService;
import com.tourism.hu.service.IScenicCommentService;
import com.tourism.hu.service.IScenicOrderService;
import com.tourism.hu.service.IScenicRoutesService;
import com.tourism.hu.service.IScenicRoutespriceService;
import com.tourism.hu.service.IScenicRoutesspotService;
import com.tourism.hu.service.IScenicSpotService;
import com.tourism.hu.service.ISeckillService;
import com.tourism.hu.service.ITourOperatorService;
import com.tourism.hu.util.DateConverterUtil;
import com.tourism.hu.util.GlobalConfigUtil;
import com.tourism.hu.util.OrderCodeUtil;
import com.tourism.hu.util.ResponseResult;
import com.tourism.hu.util.ResultUtil;
import com.tourism.hu.util.SignEncodeUtil;

/**
 * 
 * @ClassName: ScenicRoutesApiController 
 * @Description: (这里用一句话描述这个类的作用) 
 * @author 董兴隆
 * @date 2019年11月11日 下午4:56:01 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RequestMapping("api/scenicRoutesApi")
@RestController
public class ScenicRoutesApiController extends BaseController {

	 private  Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 *  线路Service
	 */
	@Autowired
	private IScenicRoutesService routesService;
	
	/**
	 *  线路订单Service
	 */
	@Autowired
	private IScenicOrderService orderService;
	
	/**
	 * 线路包含的景点Service
	 */
	@Autowired
	private IScenicRoutesspotService routesspotService;
	
	/**
	 * 线路包含的景点Service
	 */
	@Autowired
	private IScenicSpotService scenicSpotService;
	
	/**
	 *  区域service
	 */
	@Autowired
	private IRegionService regionService;
	
	/**
	 *  线路景点 价格 service
	 */
	@Autowired
	private IScenicRoutespriceService routespriceService;
 
	/**
	 *   旅游公司价格 service
	 */
	@Autowired
	private ITourOperatorService tourCompanyService;
	/**
	 *   旅游公司价格 service
	 */
	@Autowired
	private IRoutesOrderPassengerService orderPassengerService;
	
	
	@Autowired
	private ICustomerInfoService customerInfoService;
	/**
	 *   旅游线路评论 service
	 */
	@Autowired
	private IScenicCommentService commentService;
	
	/**
	 *   秒杀拼团 service
	 */
	@Autowired
	private ISeckillService seckillService;
	
	/**
	 *   利润分成service
	 */
	@Autowired
	private IProfitFlowingService flowingService;
	
	
	/**
	 *   用户乘客人service
	 */
	@Autowired
	private ICustomerPassengerService passengerService;
	
	
	@RequestMapping("/selectRoutesPriceByMonth")
	 public void selectRoutesPriceByMonth(Integer routesId,String month) {
		 ResponseResult<List<Map<String,Object>>> result = new ResponseResult<>(0,"暂无数据");
		 List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		 if(routesId==null || routesId<=0) {
			 result.setOkMsg("请确定线路Id");
			 ResponseResult.Step(result);
			 return ;
		 }
		 if(StringUtils.isBlank(month)) {
			 result.setOkMsg("请选择月份");
			 ResponseResult.Step(result);
			 return ;
		 }
		 ScenicRoutes scenicRoutes= routesService.getById(routesId);
		 if(scenicRoutes==null) {
			 result.setOkMsg("未找到线路信息");
			 ResponseResult.Step(result);
			 return ;
		 }
		 
		 String[] data =  month.split("-");
		 Integer y = Integer.valueOf(data[0]);
		 Integer m = Integer.valueOf(data[1]);
		 LocalDate monthStart = LocalDate.of(y, m, 1);
		 LocalDate monthEnd =null;
		 if(m==12) {
			 monthEnd = LocalDate.of(y+1, 1, 1);
		 }else {
			 monthEnd = LocalDate.of(y, m+1, 1);
		 }
		 //活动价格
		 List<ScenicRoutesprice> routesPriceList = routespriceService.list(new QueryWrapper<ScenicRoutesprice>().between("price_date", monthStart, monthEnd).eq("routes_id", routesId).eq("status", 1));
		 long days =  monthEnd.toEpochDay()-monthStart.toEpochDay();
		 for (int i = 0; i < days; i++) {
			 Map<String,Object> map = new HashMap<String,Object>();
			 LocalDate localData=LocalDate.of(y, m, i+1);
			 map.put("date", localData);
			 map.put("value", scenicRoutes.getSellingPrice());
			//原价
			 // localData.format(dataTimeFormat);
			 for (ScenicRoutesprice routesPrice : routesPriceList) {
				 LocalDate priceDate=routesPrice.getPriceDate().toLocalDate();
				 if(localData.isEqual(priceDate)) {
					 if(routesPrice.getActivityPrice()!=null && routesPrice.getActivityPrice().compareTo(BigDecimal.ZERO)==1) {
						 map.put("value", routesPrice.getActivityPrice());
						 break;
					 }else if(routesPrice.getSellingPrice()!=null && routesPrice.getSellingPrice().compareTo(BigDecimal.ZERO)==1) {
						 map.put("value", routesPrice.getSellingPrice());
						 break;
					 }
				 }
			}
			list.add(map);
		 }
		 //当前时间
//		 String now = DateConverterUtil.dateFormat(LocalDateTime.now(), DateConverterUtil.dateTime);
//		 //秒杀价格
//		 Seckill seckill = seckillService.getOne(new QueryWrapper<Seckill>().eq("goods_id", routesId).isNull("collage_count").apply("seckill_begintime<={0}", now).apply("seckill_endtime>{0}", now).last("limit 1"));
//		 if(seckill != null) {
//			 int daysNum = Period.between(seckill.getSeckillBegintime().toLocalDate() , seckill.getSeckillEndtime().toLocalDate()).getDays();
//			 LocalDate startDate = seckill.getSeckillBegintime().toLocalDate();
//			 for (int i = 0; i <= daysNum; i++) {
//				 for (Map<LocalDate,BigDecimal> monthPrice : monthPriceList) {
//					 for (LocalDate key : monthPrice.keySet()) {
//						 if(startDate.isEqual(key)) {
//							 monthPrice.put(key, seckill.getSeckillPrice());
//							 break;
//						 }
//					 }
//				 }
//			 }
//		 }
//		 List<Map<String,Object>> resultData = new ArrayList<Map<String,Object>>();
//		 for (Map<LocalDate,BigDecimal> monthPrice : monthPriceList) {
//			 Map<String,Object> map =new HashMap<String, Object>();
//			 Set<Map.Entry<LocalDate,BigDecimal>> entity = monthPrice.entrySet();
//			 Iterator<Entry<LocalDate, BigDecimal>>  it = entity.iterator();
//			 if(it.hasNext()) {
//				 Entry<LocalDate, BigDecimal> i =  it.next();
//				 map.put("date", i.getKey());
//				 map.put("value", i.getValue());
//			 }
//			 resultData.add(map);
//		 }
		 result.setData(list);
		 result.setOkMsg("查询成功");
		 ResponseResult.Step(result);
		 
		 
		 
		 
		 
		 
		 
	 }
	 
	 /**
	  * 
	  * @Title: getInfo 
	  * @Description: (通过旅游线路获得线路详情) 
	  * @param scenicRoutesId  
	  * @date 2019年11月6日 下午3:33:12
	  * @author 董兴隆
	  */
	 @RequestMapping("/getScenicRoutesInfo")
	 public void getScenicRoutesInfo(Integer routesId) {
		 ResponseResult<RoutesInfoVo> result = new ResponseResult<>(0,"暂无数据");
		 RoutesInfoVo vo = new RoutesInfoVo();
		 ScenicRoutes scenicRoutes= routesService.getById(routesId);
		 if(scenicRoutes==null) {
			 ResponseResult.Step(result);
			 return ;
		 }
		 ScenicRoutesDto dto = new ScenicRoutesDto();
		 BeanUtils.copyProperties(scenicRoutes, dto);
		 
		 List<ScenicRoutesspot> routesSpotList = routesspotService.list(new QueryWrapper<ScenicRoutesspot>().eq("routes_id", scenicRoutes.getRoutesId()));
		 if(routesSpotList==null || routesSpotList.isEmpty()) {
			 result.setErrorMsg("未找到线路的相关景点信息", ResponseResult.ERROR_CODE);
			 ResponseResult.Step(result);
			 return ;
		 }
		 //最后一个景点地址
		 ScenicSpot lastSpot = scenicSpotService.getById(routesSpotList.get(routesSpotList.size()-1).getSpotId());
		 if(lastSpot==null) {
			 result.setErrorMsg("未找到线路的最后一个景点信息", ResponseResult.ERROR_CODE);
			 ResponseResult.Step(result);
			 return ;
		 }
		 //旅游公司信息
		 TourOperator touOpertor = tourCompanyService.getById(scenicRoutes.gettId());
		 if(touOpertor==null) {
			 result.setErrorMsg("未找到商家信息", ResponseResult.ERROR_CODE);
			 ResponseResult.Step(result);
			 return ;
		 } 
		 vo.setTourId(touOpertor.getTId());
		 vo.setTourName(touOpertor.gettName());
		 Region region = regionService.getById(lastSpot.getCity());
		 if(region==null) {
			 result.setErrorMsg("未找到景点地址信息", ResponseResult.ERROR_CODE);
			 ResponseResult.Step(result);
			 return ;
		 }
		 //线路  出发城市  -  最后一个景点城市地址
		 vo.setLine(scenicRoutes.getCity()+"-"+region.getSname());
		 vo.setRoutesImg(GlobalConfigUtil.getServiceUrl()+scenicRoutes.getRoutesImg());
		 vo.setRoutesName(scenicRoutes.getRoutesName());
		 vo.setRoutesId(scenicRoutes.getRoutesId());
		 
		 String end = DateConverterUtil.getDay(5);
		 String today = DateConverterUtil.getDay(0);
		 ScenicRoutesprice routesPrice = routespriceService.getOne(new QueryWrapper<ScenicRoutesprice>().eq("routes_id", scenicRoutes.getRoutesId()).apply("date_format(price_date,'%Y-%m-%d') = {0}",today),false);
		 //底价  及 活动价
		 BigDecimal floorPrice = scenicRoutes.getFloorPrice();
		 BigDecimal activityPrice = BigDecimal.ZERO;
		 BigDecimal sellingPrice = scenicRoutes.getSellingPrice();
		 if(routesPrice!=null) {
			 if(routesPrice.getActivityPrice()!=null && routesPrice.getActivityPrice().compareTo(BigDecimal.ZERO)==1) {
				 activityPrice = routesPrice.getActivityPrice();
			 }else if(routesPrice.getSellingPrice()!=null && routesPrice.getSellingPrice().compareTo(BigDecimal.ZERO)==1) {
				 sellingPrice=routesPrice.getSellingPrice();
			 }
			 if(routesPrice.getFloorPrice()!=null && routesPrice.getFloorPrice().compareTo(BigDecimal.ZERO)==1) {
				 floorPrice = routesPrice.getFloorPrice();
			 }
			 
		 }
		 
		 //当前时间
//		 String now = DateConverterUtil.dateFormat(LocalDateTime.now(), DateConverterUtil.dateTime);
//		 //拼团价格
//		 Seckill group = seckillService.getOne(new QueryWrapper<Seckill>().eq("goods_id", routesId).gt("collage_count", 1).apply("seckill_begintime<={0}", now).apply("seckill_endtime>{0}", now).last("limit 1"));
//		 //秒杀价格
//		 Seckill seckill = seckillService.getOne(new QueryWrapper<Seckill>().eq("goods_id", routesId).isNull("collage_count").apply("seckill_begintime<={0}", now).apply("seckill_endtime>{0}", now).last("limit 1"));
		 BigDecimal groupPrice = BigDecimal.ZERO;
		 BigDecimal seckillPrice = BigDecimal.ZERO;
		 int isGroup=0;
		 int isSeckill=0;
//		 if(group!=null && group.getSeckillPrice()!=null && group.getSeckillPrice().compareTo(BigDecimal.ZERO)==1) {
//			 groupPrice=group.getSeckillPrice();
//			 isGroup=1;
//		 }
//		 if(seckill!=null && seckill.getSeckillPrice()!=null && seckill.getSeckillPrice().compareTo(BigDecimal.ZERO)==1) {
//			 seckillPrice=seckill.getSeckillPrice();
//			 isSeckill=1;
//		 }
		 vo.setIsGroup(isGroup);
		 vo.setGroupPrice(groupPrice);
		 vo.setIsSeckill(isSeckill);
		 vo.setSeckillPrice(seckillPrice);
		 vo.setActivePrice(activityPrice);
		 vo.setSellingPrice(sellingPrice);
		 BigDecimal returnPrice = getPercentage(1, sellingPrice, floorPrice, activityPrice, BaseConstant.USER_TYPE);
		 
		 BigDecimal parePrice = getPercentage(1, sellingPrice, floorPrice, activityPrice, BaseConstant.PARE_TYPE);
		 
		 vo.setReturnPrice(returnPrice);
		 vo.setParePrice(parePrice);
		 
		 //设置5天的默认价格
		 Calendar calendar =Calendar.getInstance();
		 String start = DateConverterUtil.frormat(calendar.getTime(), DateConverterUtil.date);
		 Map<String,Object> map = new LinkedHashMap<String,Object>();
		 List<Map<String,Object>> dates = new ArrayList<>();
		 for (int i=0;i<=5; i++) {
			 Map<String,Object> date=  new LinkedHashMap<String,Object>();
			 String key = DateConverterUtil.frormat(calendar.getTime(), DateConverterUtil.date);
			 date.put("date", key);
			 date.put("price", scenicRoutes.getSellingPrice());
			 map.put(key, scenicRoutes.getSellingPrice());
			 dates.add(date);
			 calendar.add(Calendar.DATE, 1);
		 }
		 /**
		  * 线路5天内的价格  如果有活动价 则使用活动价格  如果没有活动价 有销售价 则使用销售价
		  */
		 List<ScenicRoutesprice> scenicRoutesPriceList = routespriceService.list(new QueryWrapper<ScenicRoutesprice>().eq("routes_id", scenicRoutes.getRoutesId()).between("price_date", start, end));
		 for (ScenicRoutesprice scenicRoutesprice : scenicRoutesPriceList) {
			 String day = DateConverterUtil.dateFormat(scenicRoutesprice.getPriceDate(), DateConverterUtil.date);
			 for (Map<String,Object> date : dates) {
				 if(date.get("date").equals(day)) {
					 if(scenicRoutesprice.getActivityPrice()!=null && scenicRoutesprice.getActivityPrice().compareTo(BigDecimal.ZERO)==1) {
						 date.put("price", scenicRoutesprice.getActivityPrice());
					 }else if(scenicRoutesprice.getSellingPrice()!=null && scenicRoutesprice.getSellingPrice().compareTo(BigDecimal.ZERO)==1) {
						 date.put("price", scenicRoutesprice.getSellingPrice());
					 }
				 }
			 }
		}
//		 if(seckill != null) {
//			 int daysNum = Period.between(seckill.getSeckillBegintime().toLocalDate() , seckill.getSeckillEndtime().toLocalDate()).getDays();
//			 for (int i = 0; i <= daysNum; i++) {
//				 String day = DateConverterUtil.dateFormat(seckill.getSeckillBegintime().plusDays(i), DateConverterUtil.date);
//				 for (Map<String,Object> date : dates) {
//					 if(date.get("date").equals(day)) {
//						 date.put("price", seckill.getSeckillPrice());
//						 break;
//					 }
//				 }
//			 }
//		 }
		vo.setDates(dates);
		int comment =  commentService.count(new QueryWrapper<ScenicComment>().eq("scenic_id",scenicRoutes.getRoutesId()).eq("comment_parent", 0));
		vo.setCommentsCount(comment);
		List<ScenicComment> commentList = new ArrayList<>();
		List<RoutesCommentVo> RoutesCommentList = new ArrayList<>();
		if(comment > 0) {
			commentList = commentService.list(new QueryWrapper<ScenicComment>().eq("scenic_id",scenicRoutes.getRoutesId()).last("limit 2"));
			for (ScenicComment scenicComment : commentList) {
				RoutesCommentVo commentVo= new RoutesCommentVo();
				CustomerInfo customerInfo = customerInfoService.getById(scenicComment.getCustomerId());
				if(customerInfo!=null&&customerInfo.getCustomerId()!=null) {
					commentVo.setUserImg(GlobalConfigUtil.getServiceUrl()+customerInfo.getCustomerIcon());
					commentVo.setContent(scenicComment.getContent());
					commentVo.setGrade(scenicComment.getGoodsScore());
					commentVo.setUserName(scenicComment.getCustomerName());
					List<String> imgs = new ArrayList<>();
					imgs.add(scenicComment.getPictureurl());
					commentVo.setImgs(imgs);
				}
				RoutesCommentList.add(commentVo);
			}
		}
		vo.setComments(RoutesCommentList);
		result.setData(vo);
		result.setOkMsg("查询成功");
		ResponseResult.Step(result);
	 }
	 
	 /**
	  * 
	  * @Title: jumpMakeOrderPage 
	  * @Description: (跳转到生成订单页面)   
	  * @param routesId  线路Id
	  * @param num   数量
	  * @param travelTime   出行时间
	  * @date 2019年11月9日 上午11:41:21
	  * @author 董兴隆
	  */
	 @RequestMapping("jumpMakeOrderPage")
	 public void jumpMakeOrderPage(Integer routesId,Integer num,String travelTime) {
		 ResponseResult<JumpMakeOrderVo> result = new ResponseResult<>(404,"未找到登陆用户");
		 CustomerInfo user = getCustomerInfo();
		 if(user==null) {
			 ResponseResult.Step(result);
			 return ;
		 }
		 ScenicRoutes routes = routesService.getById(routesId);
		 if(routes == null || routes.getRoutesId()==null) {
			 result.setErrorMsg("未找到线路信息");
			 ResponseResult.Step(result);
			 return ;
		 }
		 if(routes.gettId()==null || routes.gettId()<=0) {
			 result.setErrorMsg("未找到商家信息");
			 ResponseResult.Step(result);
			 return ;
		 }
		 
		 TourOperator tourCompany = tourCompanyService.getById(routes.gettId());
		 if(tourCompany == null || tourCompany.getTId()==null || tourCompany.getTId()<=0) {
			 result.setErrorMsg("未找到商家信息");
			 ResponseResult.Step(result);
			 return ;
		 }
		 ScenicRoutesprice routesPrice = routespriceService.getOne(new QueryWrapper<ScenicRoutesprice>().eq("routes_id", routes.getRoutesId()).apply("date_format(price_date,'%Y-%m-%d') = {0}",travelTime),false);
		 //底价  及 活动价
		 BigDecimal floorPrice = routes.getFloorPrice();
		 BigDecimal activityPrice = BigDecimal.ZERO;
		 BigDecimal sellingPrice = routes.getSellingPrice();
		 if(routesPrice!=null) {
			 if(routesPrice.getActivityPrice()!=null && routesPrice.getActivityPrice().compareTo(BigDecimal.ZERO)==1) {
				 activityPrice = routesPrice.getActivityPrice();
			 }else if(routesPrice.getSellingPrice()!=null && routesPrice.getSellingPrice().compareTo(BigDecimal.ZERO)==1) {
				 sellingPrice=routesPrice.getSellingPrice();
			 }
			 if(routesPrice.getFloorPrice()!=null && routesPrice.getFloorPrice().compareTo(BigDecimal.ZERO)==1) {
				 floorPrice = routesPrice.getFloorPrice();
			 }
		 }
		 
		 BigDecimal returnPrice = getPercentage(1, sellingPrice, floorPrice, activityPrice, BaseConstant.USER_TYPE);
		 JumpMakeOrderVo  vo  = new JumpMakeOrderVo();
		 vo.setReturnPrice(returnPrice);
		 vo.setActivityPrice(activityPrice);
//		 String now = DateConverterUtil.dateFormat(LocalDateTime.now(), DateConverterUtil.dateTime);
//		 Seckill seckill = seckillService.getOne(new QueryWrapper<Seckill>().eq("goods_id", routesId).isNull("collage_count").apply("seckill_begintime<={0}", now).apply("seckill_endtime>{0}", now).last("limit 1"));
//		 if(seckill!=null) {
//			 vo.setActivityPrice(seckill.getSeckillPrice());
//		 }
		 CustomerPassenger pass = passengerService.getOne(new QueryWrapper<CustomerPassenger>().eq("customer_id", user.getCustomerId()),false);	
		 if(pass!=null) {
			 vo.setPhone(pass.getPhone());
			 vo.setCardNo(pass.getCardNo());
			 vo.setName(pass.getName());
		 }
		 vo.setTravelTime(travelTime);
		 vo.setSellingPrice(sellingPrice);
		 vo.setIsRefund(routes.getType());
		 vo.setNum(num);
		 vo.setRoutesName(routes.getRoutesName());
		 vo.setRoutesImg(GlobalConfigUtil.getServiceUrl()+routes.getRoutesImg());
		 vo.setRoutesId(routes.getRoutesId());
		 vo.setTourId(tourCompany.getTId());
		 vo.setTourName(tourCompany.gettName());
		 result.setData(vo);
		 result.setOkMsg("查询成功");
		 ResponseResult.Step(result);
		 
	 }
	 
	 /**
	  * 
	  * @Title: makeOrder 
	  * @Description: (生成线路订单) 
	  * @param routesId  线路Id
	  * @param num  数量
	  * @param travelTime  出行时间
	  * @param payMethod  支付方式
	  * @param phone   联系手机号
	  * @param memo   备注信息
	  * @param passengerIds   出行人Id 集合
	  * @date 2019年11月14日 上午10:12:05
	  * @author 董兴隆
	  */
	 @RequestMapping("makeOrder")
	 public void makeOrder(Integer routesId,Integer num,String travelTime,Integer payMethod,String phone,@RequestParam(required = false, value="passengerIds[]") List<Integer> passengerIds,String memo) {
		 logger.info("进入线路生成订单接口------>  scenicRoutesApi/makeOrder");
		 logger.info("routesId---------->"+routesId);
		 logger.info("num---------->"+num);
		 logger.info("travelTime---------->"+travelTime);
		 ResponseResult<MakeOrderVo> result = new ResponseResult<>(404,"未找到登陆用户");
		//如果未输入联系人手机号 返回
		 if(StringUtils.isBlank(phone)) {
			 result.setErrorMsg("请输入手机号");
			 ResponseResult.Step(result);
			 logger.info(result.toString());
			 return ;
		 }
		 //如果没传入出行时间 返回
		 if(StringUtils.isBlank(travelTime)) {
			 result.setErrorMsg("请选择出行时间");
			 ResponseResult.Step(result);
			 logger.info(result.toString());
			 return ;
		 }
		 //如果出行人数量为0 返回选择出行人
		 if(passengerIds==null || passengerIds.isEmpty()) {
			 result.setErrorMsg("请选择出行人");
			 ResponseResult.Step(result);
			 logger.info(result.toString());
			 return ;
		 }
		//如果未选择数量默认为1
		 if(num==null || num<=0) {
			 num=1;
		 }
		 CustomerInfo user = getCustomerInfo();
		 if(user==null) {
			 result.setErrorMsg("未找到登陆用户");
			 ResponseResult.Step(result);
			 logger.info(result.toString());
			 return ;
		 }
		 //如果未选择支付方式 默认为微信支付
		 if(payMethod==null || payMethod>=0) {
			 payMethod = 5;
		 }
		 if(routesId==null || routesId<=0) {
			 result.setErrorMsg("未找到该线路");
			 ResponseResult.Step(result);
			 logger.info(result.toString());
			 return ;
		 }
		 ScenicRoutes routes = routesService.getById(routesId);
		 if(routes == null || routes.getRoutesId()==null) {
			 result.setErrorMsg("未找到线路信息");
			 ResponseResult.Step(result);
			 logger.info(result.toString());
			 return ;
		 }
		
		 ScenicRoutesprice routesPrice = routespriceService.getOne(new QueryWrapper<ScenicRoutesprice>().eq("routes_id", routesId).apply("date_format(price_date,'%Y-%m-%d') = {0}",travelTime),false);
		 //底价  及 活动价
		 BigDecimal activityPrice = BigDecimal.ZERO;
		 BigDecimal floorPrice = routes.getFloorPrice();
		 //实际支付价格
		 BigDecimal payPrice = routes.getSellingPrice();
		 if(routesPrice!=null) {
			 if(routesPrice.getActivityPrice()!=null && routesPrice.getActivityPrice().compareTo(BigDecimal.ZERO)==1) {
				 activityPrice = routesPrice.getActivityPrice();
				 payPrice = routesPrice.getActivityPrice();
			 }else if(routesPrice.getSellingPrice()!=null && routesPrice.getSellingPrice().compareTo(BigDecimal.ZERO)==1) {
				 payPrice = routesPrice.getActivityPrice();
			 }
			 if(routesPrice.getFloorPrice()!=null && routesPrice.getFloorPrice().compareTo(BigDecimal.ZERO)==1) {
				 floorPrice=routesPrice.getFloorPrice();
			 }
		 }
//		 if(activityPrice.compareTo(BigDecimal.ZERO)==1) {
//			 sellingPrice = activityPrice;
//		 }
//		 String now = DateConverterUtil.dateFormat(LocalDateTime.now(), DateConverterUtil.dateTime);
//		 Seckill seckill = seckillService.getOne(new QueryWrapper<Seckill>().eq("goods_id", routesId).isNull("collage_count").apply("seckill_begintime<={0}", now).apply("seckill_endtime>{0}", now).last("limit 1"));
		 //用户  上级  代理商  返利
		 BigDecimal userRebate = BigDecimal.ZERO;
		 BigDecimal superiorRebate  = BigDecimal.ZERO;
		 BigDecimal agentRebate  = BigDecimal.ZERO;
		 //如果为秒杀的情况下 销售价格修改为秒杀价
//		 if(seckill!=null && seckill.getSeckillPrice()!=null && seckill.getSeckillPrice().compareTo(BigDecimal.ZERO)==1) {
//			 sellingPrice = seckill.getSeckillPrice();
//		 }else {
			 //不为秒杀 生成返利
			 userRebate = getPercentage(1, payPrice.subtract(new BigDecimal(num)), floorPrice.subtract(new BigDecimal(num)), activityPrice.subtract(new BigDecimal(num)), 2);
			 superiorRebate = getPercentage(1, payPrice.subtract(new BigDecimal(num)), floorPrice.subtract(new BigDecimal(num)), activityPrice.subtract(new BigDecimal(num)), 4);
			 agentRebate = getPercentage(1, payPrice.subtract(new BigDecimal(num)), floorPrice.subtract(new BigDecimal(num)), activityPrice.subtract(new BigDecimal(num)), 1);
//		 }
		 ScenicOrder orderDo = new ScenicOrder();
		 String orderSn = OrderCodeUtil.getOrderCode(user.getCustomerId().longValue());
		 //生成线路订单信息
		 createRoutesOrder(orderDo, orderSn,routes, num, travelTime, payMethod, phone, memo, user, payPrice);
		 //返回页面数据
		 MakeOrderVo vo = new MakeOrderVo();
		 vo.setPayType(payMethod);
		 vo.setTotalPrice(payPrice.multiply(new BigDecimal(num)));
		 vo.setOrderSn(orderSn);
		 
		 try {
			 //生成线路乘客信息
//			 Collection<CustomerPassenger>  passList = passengerService.listByIds(Arrays.asList(passengerIds));
			 Collection<CustomerPassenger>  passList = passengerService.listByIds(passengerIds);
			 List<RoutesOrderPassenger> orderPassList = new ArrayList<>();
			 for (CustomerPassenger customerPassenger : passList) {
				 RoutesOrderPassenger p = new RoutesOrderPassenger();
				 BeanUtils.copyProperties(customerPassenger, p);
				 p.setOrderSn(orderSn);
				 orderPassList.add(p);
			 }
			 // 如果出行人和数量不一致 返回数量不一致
			 if(orderPassList.size()!=num) {
				 result.setErrorMsg("出行人和数量不一致");
				 ResponseResult.Step(result);
				 logger.info(result.toString());
				 return ;
			 }
			 orderPassengerService.saveBatch(orderPassList);
			 /**
			  * 生成订单流水信息
			  */
			 createRoutesOrderProfitFlowing(num, user, payPrice, activityPrice, floorPrice, userRebate, superiorRebate,
					agentRebate, orderDo, orderSn);
			 
			 if(orderService.save(orderDo)) {																												//user.getOpenid()
				 logger.info("订单生成成功");
				 String wxResult = WXPay.toPay(orderSn, "购买"+routes.getRoutesName(), orderDo.getPaymentMoney(), "http://m.huerdai.net:8888/hu/notify/routesOrder", TradeType.JSAPI, user.getOpenid());
				 logger.info("wxResult========>"+wxResult);
				 Map<String, String> wxConfig = WXPayUtil.xmlToMap(wxResult);
				 if(wxConfig!=null) {
					if("SUCCESS".equals(wxConfig.get("result_code"))) {
						WXPay.createJSAPIWxConfig(wxConfig);
						logger.info("wxConfig==============>"+wxConfig.toString());
					 }else {
						 wxConfig.put("result_code", "FAIL");
					 }
				 }
				 vo.setWxConfig(wxConfig);
				 result.setOkMsg("生成订单成功");
			 }else {
				 result.setErrorMsg("生成订单失败");
			 }
		 }catch (Exception e) {
			 result.setErrorMsg("生成订单失败");
		}
		result.setData(vo);
		logger.info(result.toString());
		ResponseResult.Step(result);
	 }

	private void createRoutesOrderProfitFlowing(Integer num, CustomerInfo user, BigDecimal payPrice,
			BigDecimal activityPrice, BigDecimal floorPrice, BigDecimal userRebate, BigDecimal superiorRebate,
			BigDecimal agentRebate, ScenicOrder orderDo, String orderSn) throws Exception {
		//生成线路乘客信息结束  生成订单流水信息
		 ProfitFlowing pf = new ProfitFlowing();
		 pf.setOrderSn(orderSn);
		 pf.setPrice(payPrice);
		 pf.setActivityPrice(activityPrice);
		 pf.setExitPrice(floorPrice);
		 //总利润   商品支付价格 -  商品的底价 * 数量  以后做折扣再重新计算
		 pf.setTotalMoney(orderDo.getPaymentMoney().subtract(floorPrice.multiply(new BigDecimal(num))));
		 pf.setNumber(num);
		 pf.setSalePrice(orderDo.getPaymentMoney());
		 //没有秒杀则生成返利
//			 if(seckill==null || seckill.getSeckillId()==null) {
			 pf.setCusMoney(userRebate);
			 pf.setAgentMoney(agentRebate);
			 pf.setParentcusMoney(superiorRebate);
//			 }
		 pf.setCusId(user.getCustomerId());
		 //pf.setParentId(user.get)  用户上级Id
		 pf.setCreatedId(user.getCustomerId());
		 pf.setCreatedName(user.getCustomerName());
		 pf.setCreationDate(LocalDateTime.now());
		 flowingService.save(pf);
	}
	 
	 /**
	  * 
	  * @Title: createRoutesOrder 
	  * @Description: (生成线路订单) 
	  * @param orderDo  订单实体
	  * @param orderSn   订单号
	  * @param routes  线路id
	  * @param num   购买数量
	  * @param travelTime  出行时间
	  * @param payMethod  支付方式
	  * @param phone   手机号
	  * @param memo  备注
	  * @param user  用户
	  * @param payPrice  支付金额
	  * @date 2019年11月22日 上午9:23:57
	  * @author 董兴隆
	  */
	private void createRoutesOrder(ScenicOrder orderDo, String orderSn,ScenicRoutes routes, Integer num, String travelTime, Integer payMethod, String phone,
			String memo, CustomerInfo user, BigDecimal payPrice) {
		 orderDo.setOrderSn(orderSn);
		 orderDo.setOrderCount(num);
		 orderDo.setRoutesId(routes.getRoutesId());
		 orderDo.setRoutesName(routes.getRoutesName());
		 orderDo.setRoutesImg(routes.getRoutesImg());
		 orderDo.setTravelTime(DateConverterUtil.setDayZero(travelTime));
		 orderDo.setPaymentMethod(payMethod);
		 orderDo.setRoutesPrice(payPrice);
		 orderDo.setPaymentMoney(payPrice.multiply(new BigDecimal(num)));
		 orderDo.setCustomerId(user.getCustomerId());
		 orderDo.setCustomerName(user.getCustomerName());
		 orderDo.setCustomerPhone(phone);
		 orderDo.setCreatedId(user.getCustomerId());
		 orderDo.setCreatedName(user.getCustomerName());
		 orderDo.setCreationDate(LocalDateTime.now());
		 orderDo.setOrderStatus(2);  // 1已支付 2未支付 3已完成
		 orderDo.setOrderTime(LocalDateTime.now());
		 orderDo.setOrderMoney(payPrice.multiply(new BigDecimal(num)));
		 orderDo.setMemo(memo);
	}


}
