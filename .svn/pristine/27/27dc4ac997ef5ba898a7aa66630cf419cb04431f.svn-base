package com.tourism.hu.schedule;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.OrderConstant;
import com.tourism.hu.constant.WalletRecordConstant;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.Order;
import com.tourism.hu.entity.Wallet;
import com.tourism.hu.entity.WalletRecord;
import com.tourism.hu.service.*;
import com.tourism.hu.util.DateConverterUtil;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author 马超伟
 * @PROJECT_NAME: fzll
 * @Description:
 * @date 17:36
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@Component
@Configuration // 1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling // 2.开启定时任务
public class WalltScheduleTask  {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private IWalletService iWalletService;

    @Resource
    private IWalletRecordService iWalletRecordService;

    @Resource
    private IOrderService iOrderService;

    @Resource
    private ICustomerInfoService iCustomerInfoService;

    /**
     * 每日0点
     * 发货之后大于等于15天的订单自动确认收货
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void confirmGoods() {
        //查询支付时间距离今天0点大于等于15天的订单
        List<Order> orderList = iOrderService.list(new QueryWrapper<Order>().le("send_time", DateConverterUtil.getTime(-15)));
        for (Order order : orderList) {
            logger.debug("自动确认收货的订单编号为：" + order.getOrderSn());
            order.setOrderStatus(OrderConstant.FOUR_DONE);
        }
        boolean updateBatchById = iOrderService.updateBatchById(orderList);
        if (updateBatchById) {
            logger.debug("自动确认收货成功");
        } else {
            logger.debug("自动确认收货失败！" + orderList);
        }

    }


    /**
     * 每月20号
     * 每一个人
     * 自动将上个月的消费预算金额转入到用户余额中
     * 并且累加到总结算收益中
     */
    @Scheduled(cron = "0 0 0 20 * ?")
    public void toPayCount() {
        List<CustomerInfo> customerInfoList = iCustomerInfoService.list(new QueryWrapper<CustomerInfo>().eq("status", BaseConstant.USE_STATUS));
        for (CustomerInfo customerInfo : customerInfoList) {
            Wallet wallet = iWalletService.getOne(new QueryWrapper<Wallet>().eq("cus_id", customerInfo.getCustomerId()));
            if (wallet == null) {
                wallet = new Wallet();
                wallet.setCusId(customerInfo.getCustomerId());
                wallet.setCusId(customerInfo.getCustomerId());
                wallet.setCreatedName(customerInfo.getCustomerName());
                wallet.setCreationDate(LocalDateTime.now());
                iWalletService.save(wallet);
                return;
            }
            //计算上月预估消费收入
            BigDecimal localMonthConsume1 = iWalletService.getLocalMonthConsume(customerInfo.getCustomerId(), -1);
            //转入用户余额中
            wallet.setMoney(wallet.getMoney().add(localMonthConsume1));
            //累加到中结算收益余额中
            wallet.setEndProfit(wallet.getEndProfit().add(localMonthConsume1));
            //更新用户钱包表
            boolean save = iWalletService.save(wallet);
            if (save){
                WalletRecord walletRecord = new WalletRecord();
                walletRecord.setCreationDate(LocalDateTime.now());
                walletRecord.setMoney(localMonthConsume1);
                walletRecord.setPayTime(LocalDateTime.now());
                walletRecord.setType(WalletRecordConstant.ORDER_PROFIT);
                walletRecord.setRemark("上月自动转入的收益");
                walletRecord.setCustomerId(customerInfo.getCustomerId());
                walletRecord.setWalletId(wallet.getId());
                iWalletRecordService.save(walletRecord);
                logger.debug("用户："+customerInfo.getCustomerId()+"自动转入到余额："+localMonthConsume1+"元");
            }else {
                logger.debug("失败！用户："+customerInfo.getCustomerId()+"转入到余额："+localMonthConsume1+"元");
            }
        }
    }
}
