package com.tourism.hu.controller;


import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.entity.Region;
import com.tourism.hu.entity.ScenicRoutesspot;
import com.tourism.hu.entity.ScenicSpot;
import com.tourism.hu.entity.ScenicSpotImg;
import com.tourism.hu.entity.TourOperator;
import com.tourism.hu.entity.User;
import com.tourism.hu.entity.dto.ScenicSpotDto;
import com.tourism.hu.mapper.ScenicSpotMapper;
import com.tourism.hu.service.IScenicRoutesspotService;
import com.tourism.hu.service.IScenicSpotImgService;
import com.tourism.hu.service.IScenicSpotService;
import com.tourism.hu.service.ITourOperatorService;
import com.tourism.hu.util.PageUtil;
import com.tourism.hu.util.ResultUtil;

/**
 * @ClassName: ScenicSpotController 
 * @Description: (风景地点)
 * @author 董兴隆
 * @date 2019年9月27日 下午1:12:14 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/scenic/spot")
public class ScenicSpotController extends BaseController{

	 private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Resource
    private ScenicSpotMapper scenicSpotMapper;
	
	
	/**
	 * 线路service
	 */
	@Autowired
	private IScenicSpotService scenicSpotService;
	
	 
	/**
	 *  线路景点service
	 */

	@Autowired
	private IScenicRoutesspotService routesspotService;
	/**
	 * 旅游公司service
	 * **/
	@Autowired
	private ITourOperatorService tourOperatorService;
	
	/**
	 *  景点缩略图service
	 */
	@Autowired
	private IScenicSpotImgService spotImgService;
	
	
	
	/**
	 * 
	 * @Title: page 
	 * @Description: (风景地点首页) 
	 * @return  
	 * @date 2019年9月27日 下午4:06:32
	 * @author 董兴隆
	 */
	@RequestMapping("/page")
	public ModelAndView page() {
		ModelAndView mv = new ModelAndView("/scenic/spot/scenicSpotList");
		//所有状态是启用的旅游公司信息
		List<TourOperator> tourOperatorList = tourOperatorService.list(new QueryWrapper<TourOperator>().eq("status", 1));
		mv.addObject("tourOperatorList", tourOperatorList);
		List<Region> provinceList = selectProvinceList();
		mv.addObject("provinceList", provinceList);
		return mv;
	}
	
	/**
	 * 
	 * @Title: editPage 
	 * @Description: (风景地点编辑页面  包含 新增页面) 
	 * @param dto
	 * @return  
	 * @date 2019年9月27日 下午4:06:51
	 * @author 董兴隆
	 */
	@RequestMapping("/editPage")
	public ModelAndView editPage(ScenicSpotDto dto) {
		ModelAndView mv = new ModelAndView("/scenic/spot/scenicSpotEdit");
		//所有状态是启用的旅游公司信息
		List<TourOperator> tourOperatorList = tourOperatorService.list(new QueryWrapper<TourOperator>().eq("status", 1));
		mv.addObject("tourOperatorList", tourOperatorList);
		List<Region> provinceList = selectProvinceList();
		mv.addObject("provinceList", provinceList);
		if(dto.getSpotId()!=null && dto.getSpotId()>0) {
			ScenicSpot spot = scenicSpotService.getById(dto.getSpotId());
			mv.addObject("dto", spot);
			//如果景点Id不为空则查看景点的缩略图是否存在
			List<ScenicSpotImg> thumbnailsList = spotImgService.list(new QueryWrapper<ScenicSpotImg>().eq("spot_id", dto.getSpotId()));
			mv.addObject("thumbnailsList", thumbnailsList);
			if(spot!=null &&spot.getProvince()!=null && spot.getProvince()>0) {
				List<Region> cityList = selectByPidRegionList(spot.getProvince());
				mv.addObject("cityList", cityList);
			}
			if(spot!=null &&spot.getCity()!=null && spot.getCity()>0) {
				List<Region> districtList = selectByPidRegionList(spot.getCity());
				mv.addObject("districtList", districtList);
			}
		}
		return mv;
	}
	@RequestMapping("/editSpotCalendarPage")
	public ModelAndView editSpotCalendarPage(ScenicSpotDto dto) {
		ModelAndView mv = new ModelAndView("/scenic/spot/scenicsSpotCalendar");
		mv.addObject("dto", dto);
		return mv;
	}
	
	/**
	 * 
	 * @Title: selectData 
	 * @Description: (风景地点首页数据接口) 
	 * @param dto
	 * @return  
	 * @date 2019年9月27日 下午4:07:15
	 * @author 董兴隆
	 */
	@RequestMapping("/selectData")
	public ResultUtil<List<ScenicSpotDto>> selectData(ScenicSpotDto dto){
		PageUtil.initAttr(dto);
		ResultUtil<List<ScenicSpotDto>> dataUtil=null;
		dataUtil = ResultUtil.error("查询失败",new ArrayList<ScenicSpotDto>());
		long count = scenicSpotMapper.getCount(dto);
		List<ScenicSpotDto> list = scenicSpotMapper.selectPageListData(dto);
		dataUtil=ResultUtil.success("查询成功",new ArrayList<ScenicSpotDto>());
		//将数据封装到dataUtil对象
		dataUtil.setData(list);
		dataUtil.setCount(count);
		return dataUtil;
	}
	
	@RequestMapping("/deleteById")
	public ResultUtil<ScenicSpot> deleteById(@RequestParam(value="spotId") Integer spotId){
		if(scenicSpotService.removeById(spotId)) {
			return ResultUtil.success();
		}else {
			return ResultUtil.error();
		}
	}
	
	 
	@RequestMapping("saveOrUpdate")
	public ResultUtil<ScenicSpotDto> saveOrUpdate(MultipartFile file,MultipartFile[] files, ScenicSpotDto dto) {
		if(isNotLogIn()) {
			return ResultUtil.error("未找到登录用户");
		}
		//景点主图片   景点展示图
		String basePath="img/scenicSpot/primaryPic";
		if(file != null && file.getSize() > 0) {
			 String url=null;
			 try {
				url = upload(file, basePath);
			} catch (IOException e) {
				logger.debug("scenic/spot/saveOrUpdate 图片上传失败---->"+e.getMessage());
				return ResultUtil.error("图片上传失败");
			}
			dto.setPictureurl(url);
		}
		boolean save = true;
		User user= getUser();
		if(dto.getSpotId()!=null && dto.getSpotId()>0) {
			save=false;
			dto.setLastUpdatedId(user.getUserId());
			dto.setLastUpdateName(user.getName());
			dto.setLastUpdateDate(LocalDateTime.now());
		}else {
			dto.setCreatedId(user.getUserId());
			dto.setCreatedName(user.getName());
			dto.setCreationDate(LocalDateTime.now());
			dto.setStatus(1);
		}
		//获取完整地址
		dto.setFulladdress(getFullAddress(dto.getProvince(), dto.getCity(), dto.getDistrict(), dto.getAddress()));
		dto.setPreferentialQuota(dto.getSellingPrice().subtract(dto.getFloorPrice()));
		if(scenicSpotService.saveOrUpdate(dto)) {
			// 判断是否添加了缩略图  如果添加了缩略图   删除 其他缩略图  及保存缩略图
			if(files!=null && files.length>0) {
				basePath="img/scenicSpot/thumbnails";
				List<ScenicSpotImg> spotImgList = new ArrayList<>();
				for (MultipartFile thumbnailFile : files) {
					if(thumbnailFile != null && thumbnailFile.getSize() > 0) {
						ScenicSpotImg spotImg = new ScenicSpotImg();
						spotImg.setSpotId(dto.getSpotId());
						String url=null;
						try {
							url = upload(thumbnailFile, basePath);
						} catch (IOException e) {
							logger.debug("scenic/spot/saveOrUpdate 缩略图片上传失败---->"+e.getMessage());
							return ResultUtil.error("图片上传失败");
						}
						spotImg.setImgUrl(url);
						spotImg.setStatus(1);
						spotImgList.add(spotImg);
					}
				}
				if(!spotImgList.isEmpty()) {
					spotImgService.saveBatch(spotImgList);
				}
			}
			if(save) {
				return ResultUtil.ok("添加成功");
			}else {
				return ResultUtil.ok("修改成功");
			}
		}else {
			if(save) {
				return ResultUtil.error("添加失败");
			}else {
				return ResultUtil.error("修改失败");
			}
		}
	}
	
	
	@RequestMapping("saveSpotInfo")
	public ResultUtil<ScenicSpotDto> saveSpotInfo(ScenicSpotDto dto) {
		if(isNotLogIn()) {
			return ResultUtil.error("未找到登录用户");
		}
		User user= getUser();
		dto.setLastUpdatedId(user.getUserId());
		dto.setLastUpdateName(user.getName());
		dto.setLastUpdateDate(LocalDateTime.now());
		if(scenicSpotService.updateById(dto)) {
			return ResultUtil.ok("修改成功");
		}else {
			return ResultUtil.error("修改失败");
		}
	}
	
	@RequestMapping(value="multiple/choosePage")
	public ModelAndView choosePage(HttpServletRequest request,HttpServletResponse resp,Integer routesId) {
		
		StringBuffer sb = new StringBuffer();
		if(null!=routesId) {
			//线路景点集合
			List<ScenicRoutesspot> routesSpotList = routesspotService.list(new QueryWrapper<ScenicRoutesspot>().eq("routes_id", routesId));
			if(routesSpotList != null && !routesSpotList.isEmpty()) {
				routesSpotList.forEach(fn->sb.append(fn.getSpotId()+"-"));
			}
			
		}
		Cookie cookie = new Cookie("spotIds",sb.toString());
		cookie.setPath("/");
		resp.addCookie(cookie);
		ModelAndView mv = new ModelAndView("/chooseList/multiple/scenic/spot/scenicSpotList");
		//所有状态是启用的旅游公司信息
		List<TourOperator> tourOperatorList = tourOperatorService.list(new QueryWrapper<TourOperator>().eq("status", 1));
		mv.addObject("tourOperatorList", tourOperatorList);
		List<Region> provinceList = selectProvinceList();
		mv.addObject("provinceList", provinceList);
		return mv;
	}
	
	@RequestMapping(value="spotInfo")
	public ModelAndView spotInfo(ScenicSpotDto dto) {
		ModelAndView mv = new ModelAndView("/scenic/spot/spotInfo");
		if(dto.getSpotId()!=null && dto.getSpotId()>0) {
			ScenicSpot spot = scenicSpotService.getById(dto.getSpotId());
			BeanUtils.copyProperties(spot, dto);
		}
		mv.addObject("dto", dto);
		return mv;
	}
	
 
	
	
}
