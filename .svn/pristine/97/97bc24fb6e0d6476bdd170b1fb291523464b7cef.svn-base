package com.tourism.hu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tourism.hu.constant.AuthorConstant;
import com.tourism.hu.entity.*;
import com.tourism.hu.mapper.*;
import com.tourism.hu.service.IAuthorService;
import com.tourism.hu.service.IRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hu
 * @since 2019-09-06
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private UserRoleMapper userRoleMapper;

    @Resource
    private RoleMapper roleMapper;

    @Resource
    private MenuMapper menuMapper;

    @Resource
    private OperationMapper operationMapper;

    @Resource
    private AuthorMapper authorMapper;

    @Resource
    private IAuthorService iAuthorService;

    @Override
    public Set<String> getAllRolesByUsername(String username) {
        //根据用户名查询用户对象
        User user = userMapper.selectOne(new QueryWrapper<User>().eq("login_name", username));
        //根据用户id获取到该用户所拥有的所有角色信息
        List<UserRole> userRoleList = userRoleMapper.selectList(new QueryWrapper<UserRole>().eq("user_id", user.getUserId()));
        //再根据角色id获取到角色名
        Set<String> roles = new HashSet<>();
        for (UserRole userRole : userRoleList) {
            //根据UserRole的角色id，获取到角色对象
            Role role = roleMapper.selectOne(new QueryWrapper<Role>().eq("role_id", userRole.getRoleId()));
            //再根据角色名得到角色对象放进set集合中
            roles.add(role.getRoleName());
        }
        //最后返回set集合
        return roles;
    }

    @Override
    public Set<String> getAllPermissionsByUsername(String username) {
        //先查询该用户所有的角色名
        Set<String> allRolesByUsername = getAllRolesByUsername(username);
        Set<String> permissions = new HashSet<>();
        //遍历角色，得到每一个角色所拥有的权限
        for (String roleName : allRolesByUsername) {
            //根据角色名，获取到角色对象
            Role role = roleMapper.selectOne(new QueryWrapper<Role>().eq("role_name", roleName));

            //根据角色id，查询授权表的资源类型是菜单的资源id，
            List<Author> authors = authorMapper.selectList(new QueryWrapper<Author>().eq("role_id", role.getRoleId()).eq("resource_type", AuthorConstant.MENU_RESOURCE_TYPE));
            //遍历授权表，拿到授权表的resource_id去查询menu表的name
            for (Author author : authors) {
                List<Menu> menuList = menuMapper.selectList(new QueryWrapper<Menu>().eq("menu_id", author.getResourceId()));
                for (Menu menu : menuList) {
                    permissions.add(menu.getMenuName());
                }
            }

            //根据角色id 查询该角色所拥有的按钮权限，
            List<Author> authorsBtn = authorMapper.selectList(new QueryWrapper<Author>().eq("role_id", role.getRoleId()).eq("resource_type", AuthorConstant.BTN_RESOURCE_TYPE));
            //遍历授权表，拿到授权表的resource_id去查询按钮表的按钮name
            for (Author author : authorsBtn) {
                List<Operation> btnId = operationMapper.selectList(new QueryWrapper<Operation>().eq("btn_id", author.getResourceId()));
                for (Operation operation : btnId) {
                    permissions.add(operation.getBtnName());
                }
            }
        }

        return permissions;
    }

    @Override
    public List<HmFunctionstree> selectEleTreeByRoleName(String roleName) {
        //通过角色名，查询角色对象
        Role role = roleMapper.selectOne(new QueryWrapper<Role>().eq("role_name", roleName));
        List<HmFunctionstree> hmFunctionstreeList = new ArrayList<>();
        List<Author> authorList = new ArrayList<>();
        //通过角色id查询授权表，返回该角色所拥有的所有权限
        if (role !=null){
            authorList = iAuthorService.selectMenuIdByRoleId(role.getRoleId());
        }
        //如果角色权限为空，则应查询所有的菜单和按钮
        if (role == null || authorList.size() == 0) {
            hmFunctionstreeList = menuMapper.selectAlleleTree();
            for (HmFunctionstree hmFunctionstree : hmFunctionstreeList) {
                List<HmFunctionstree> children = hmFunctionstree.getChildren();
                for (HmFunctionstree child : children) {
                    List<Operation> operations = operationMapper.selectList(new QueryWrapper<Operation>().eq("menu_id", child.getId()));
                    if (operations.size() > 0) {
                        //只有集合中有数据，就设为true；
                        //遍历按钮表，将按钮表数据封装到TreeNode对象中，病添加到树集合
                        List<HmFunctionstree> children1 = new ArrayList<>();
                        for (Operation operation : operations) {
                            HmFunctionstree hmFunctionstree1 = new HmFunctionstree(operation.getBtnId(),operation.getBtnName(),null);
                            children1.add(hmFunctionstree1);
                        }
//                        将TreeNode集合放到二级菜单的children属性中，将按钮封装为三级菜单
                        child.setChildren(children1);
                    }

                }

            }
        } else {
            //else,权限表中有这个对象的话，基本上和上面一样，但是需要添加个有权限的默认选中
            //查询所有一级二级菜单，病封装到MenuAndBtn对象的treeNodeList集合中
            hmFunctionstreeList = menuMapper.selectAlleleTree();
            for (HmFunctionstree hmFunctionstree : hmFunctionstreeList) {
                List<HmFunctionstree> children = hmFunctionstree.getChildren();
                //遍历二级菜单
                for (HmFunctionstree child : children) {
                    //遍历授权表，判断是否有这个权限
                    for (Author author : authorList) {
                        //如果菜单表的树二级id  和 授权表 的resource id 一致，说明有这个权限，就默认选中
                        if (child.getId().equals(author.getResourceId())) {
                            child.setChecked(true);
                        }
                    }

                    List<Operation> operations = operationMapper.selectList(new QueryWrapper<Operation>().eq("menu_id", child.getId()));
                    if (operations.size() > 0) {
                        List<HmFunctionstree> children1 = new ArrayList<>();

                        System.out.println("operations " + operations);
                        for (Operation operation : operations) {
                            HmFunctionstree hmFunctionstree1 = new HmFunctionstree(operation.getBtnId(),operation.getBtnName(),null);
                            children1.add(hmFunctionstree1);
                        }
                        child.setChildren(children1);
                    }

                }
            }

        }
        return hmFunctionstreeList;
    }

    @Override
    public int addRole(Role role) {
        int i = roleMapper.insert(role);
        return i;
    }

    @Override
    public int deleteRole(Integer roleId) {
        //先删除角色信息
        int i = roleMapper.deleteById(roleId);
        //再根据该角色id删除该角色对应的权限id
        int role_id = authorMapper.delete(new QueryWrapper<Author>().eq("role_id", roleId));
        return i;
    }

    @Override
    public int deleteMAny(List list) {
        int i = roleMapper.deleteBatchIds(list);
        int role_id =0;
        for (Object o : list) {
            role_id += authorMapper.delete(new QueryWrapper<Author>().eq("role_id", o));
        }
        return i;
    }


}
