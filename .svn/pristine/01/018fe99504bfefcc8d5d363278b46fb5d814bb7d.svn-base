package com.tourism.hu.apicontroller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.ScenicOrder;
import com.tourism.hu.service.IScenicOrderService;
import com.tourism.hu.util.GlobalConfigUtil;
import com.tourism.hu.util.ResponseResult;

/**
 * 
 * @ClassName: RoutesOrderApiController 
 * @Description: (线路订单) 
 * @author 董兴隆
 * @date 2019年12月5日 下午2:00:57 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RequestMapping("api/routesOrderApi")
@RestController
public class RoutesOrderApiController extends BaseController {

	private Logger logger = LoggerFactory.getLogger(getClass());

	 
	@Autowired
	private IScenicOrderService orderService;
	
	
	@RequestMapping("selectRoutesOrderList")
	public void selectRoutesOrderList(){
		ResponseResult<Map<String,Object>> result = ResponseResult.ERROR("请登录");
		CustomerInfo customer = getCustomerInfo();
		if(customer == null) {
			ResponseResult.Step(result);
			return ;
		}
		int allCount = 0;
		Map<String,Object> resultMap = new HashMap<String, Object>();
		//全部订单
		QueryWrapper<ScenicOrder> qw =new QueryWrapper<ScenicOrder>().eq("customer_id", customer.getCustomerId()).eq("is_del", 0).orderByDesc("scenic_order_id").and(fn-> fn.eq("order_status", 1).or().eq("order_status", 2).or().eq("order_status", 3))
				.select("order_sn orderSn","travel_time travelTime","routes_id routesId","order_count orderCount","payment_money paymentMoney","is_use isUse","order_status orderStatus","routes_price routesPrice","routes_name routesName","routes_img routesImg","set_out_city setOutCity","tour_company_id tourCompanyId","tour_company_name tourCompanyName");
		List<Map<String,Object>> listMaps =orderService.listMaps(qw);
		for (Map<String, Object> map : listMaps) {
			map.put("travelTime", map.get("travelTime").toString().substring(0,10));
//			map.put("routesImg", GlobalConfigUtil.getServiceUrl()+map.get("routesImg").toString());
		}
		if(listMaps!=null && !listMaps.isEmpty()) {
			allCount=listMaps.size();
		}
		resultMap.put("allList", listMaps);
		resultMap.put("allCount", allCount);
		//代付款
		int waitCount = 0;
		QueryWrapper<ScenicOrder> waitPayQw =new QueryWrapper<ScenicOrder>().eq("customer_id", customer.getCustomerId()).eq("is_del", 0).orderByDesc("scenic_order_id").eq("order_status", 2)
				.select("order_sn orderSn","travel_time travelTime","routes_id routesId","order_count orderCount","payment_money paymentMoney","is_use isUse","order_status orderStatus","routes_price routesPrice","routes_name routesName","routes_img routesImg","set_out_city setOutCity","tour_company_id tourCompanyId","tour_company_name tourCompanyName");
		List<Map<String,Object>> waitListMaps=orderService.listMaps(waitPayQw);
		for (Map<String, Object> map : waitListMaps) {
			map.put("travelTime", map.get("travelTime").toString().substring(0,10));
//			map.put("routesImg", GlobalConfigUtil.getServiceUrl()+map.get("routesImg").toString());
		}
		if(waitListMaps!=null && !waitListMaps.isEmpty()) {
			waitCount = waitListMaps.size();
		}
		resultMap.put("waitCount", waitCount);
		resultMap.put("waitList", waitListMaps);
		
		
		//已付款
		int payCount = 0;
		QueryWrapper<ScenicOrder> payCountQw =new QueryWrapper<ScenicOrder>().eq("customer_id", customer.getCustomerId()).eq("is_del", 0).orderByDesc("scenic_order_id").and(fn-> fn.eq("order_status", 1).or().eq("order_status", 3))
				.select("order_sn orderSn","travel_time travelTime","routes_id routesId","order_count orderCount","payment_money paymentMoney","is_use isUse","order_status orderStatus","routes_price routesPrice","routes_name routesName","routes_img routesImg","set_out_city setOutCity","tour_company_id tourCompanyId","tour_company_name tourCompanyName");
		List<Map<String,Object>> payListMaps=orderService.listMaps(payCountQw);
		for (Map<String, Object> map : payListMaps) {
			map.put("travelTime", map.get("travelTime").toString().substring(0,10));
			map.put("routesImg", GlobalConfigUtil.getServiceUrl()+map.get("routesImg").toString());
		}
		if(payListMaps!=null && !payListMaps.isEmpty()) {
			payCount = payListMaps.size();
		}
		resultMap.put("payCount", payCount);
		resultMap.put("payList", payListMaps);
		
		result=ResponseResult.SUCCESS("查询成功");
		result.setData(resultMap);
		ResponseResult.Step(result);
	}
	
	@RequestMapping("getRoutesOrderInfo")
	public void getRoutesOrderInfo(String orderSn){
		ResponseResult<Map<String,Object>> result = ResponseResult.ERROR("请登录");
		List<Map<String,Object>> listMaps =new ArrayList<>();
		CustomerInfo customer = getCustomerInfo();
		if(customer == null) {
			ResponseResult.Step(result);
			return ;
		}
		QueryWrapper<ScenicOrder> qw =new QueryWrapper<ScenicOrder>().eq("order_sn", orderSn).eq("is_del", 0)
				.select("order_sn orderSn","travel_time travelTime","routes_id routesId","order_count orderCount","payment_money paymentMoney","is_use isUse","order_status orderStatus","routes_price routesPrice","routes_name routesName","routes_img routesImg","set_out_city setOutCity","tour_company_id tourCompanyId","tour_company_name tourCompanyName","order_time orderTime","pay_time payTime","payment_method paymentMethod");
		Map<String, Object> map =orderService.getMap(qw);
		if(map==null) {
			result = ResponseResult.ERROR("未找到订单信息");
			ResponseResult.Step(result);
			return ;
		}
		if(map!=null && !map.isEmpty()) {
			map.put("travelTime", map.get("travelTime").toString().substring(0,10));
			map.put("orderTime", map.get("orderTime").toString().substring(0,10));
			Object payTime = map.get("payTime");
			if(payTime!=null && StringUtils.isNotBlank(payTime.toString())) {
				map.put("payTime", map.get("payTime").toString().substring(0,10));
			}
		}
		result=ResponseResult.SUCCESS("查询成功");
		result.setData(map);
		ResponseResult.Step(result);
	}
	
	
	@RequestMapping("deleteByOrderSn")
	public void deleteByOrderSn(String orderSn){
		ResponseResult result = ResponseResult.ERROR("请登录");
		CustomerInfo customer = getCustomerInfo();
		if(customer == null) {
			ResponseResult.Step(result);
			return ;
		}
		UpdateWrapper<ScenicOrder> uw = new UpdateWrapper<ScenicOrder>().set("is_del", 1).eq("order_sn", orderSn);
		if(orderService.update(uw)) {
			result=ResponseResult.SUCCESS("删除成功");
		}else {
			result = ResponseResult.ERROR("删除失败");
		}
		ResponseResult.Step(result);
	}
	 
	
	 
	

}
