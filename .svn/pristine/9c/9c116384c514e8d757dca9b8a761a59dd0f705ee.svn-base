package com.tourism.fzll.controller;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.fzll.entity.Region;
import com.tourism.fzll.entity.Residence;
import com.tourism.fzll.entity.User;
import com.tourism.fzll.service.IResidenceService;
import com.tourism.fzll.util.PageUtil;
import com.tourism.fzll.util.ResultUtil;

/**
 * @ClassName: ResidenceController 
 * @Description:  (民宿信息) 
 * @author 董兴隆
 * @date 2019年10月17日 下午1:17:49 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/residence")
public class ResidenceController extends BaseController{
	
	@Autowired
	private IResidenceService residenceService;
	
	/**
	 * 
	 * @Title: page
	 * @Description: (民宿页面)
	 * @return
	 * @date 2019年10月17日 下午3:54:22
	 * @author 董兴隆
	 */
	@RequestMapping("/page")
	public ModelAndView page() {
		ModelAndView mv = new ModelAndView("/residence/index/residenceList");
		List<Region> provinceList = selectProvinceList();
		mv.addObject("provinceList", provinceList);
		return mv;
	}

	/**
	 * 
	 * @Title: editPage
	 * @Description: (民宿编辑页面)
	 * @param dto
	 * @return
	 * @date 2019年10月17日 下午3:55:02
	 * @author 董兴隆
	 */
	@RequestMapping("/editPage")
	public ModelAndView editPage(Residence dto) {
		ModelAndView mv = new ModelAndView("/residence/index/residenceEdit");
		List<Region> provinceList = selectProvinceList();
		mv.addObject("provinceList", provinceList);
		if (dto.getResidenceId() != null && dto.getResidenceId() > 0) {
			dto = residenceService.getById(dto.getResidenceId());
			
			if(dto!=null&& dto.getProvince()!=null&& dto.getProvince()>0) {
				List<Region> cityList = selectByPidRegionList(dto.getProvince());
				mv.addObject("cityList", cityList);
			}
			if(dto!=null &&dto.getCity()!=null && dto.getCity()>0) {
				List<Region> districtList = selectByPidRegionList(dto.getCity());
				mv.addObject("districtList", districtList);
			}
		}
		mv.addObject("dto", dto);
		return mv;
	}

	/**
	 * 
	 * @Title: selectData
	 * @Description: (民宿获取数据接口)
	 * @param dto
	 * @return
	 * @date 2019年10月17日 下午3:55:17
	 * @author 董兴隆
	 */
	@RequestMapping("/selectData")
	@ResponseBody
	public ResultUtil<List<Residence>> selectData(Residence dto) {
		List<Residence> list = new ArrayList<>();
		ResultUtil<List<Residence>> result = ResultUtil.error("查询失败", list);
		PageUtil.initAttr(dto);
		try {
			Page<Residence> page = new Page<Residence>(dto.getPage(), dto.getLimit());
			QueryWrapper<Residence> qw = new QueryWrapper<Residence>();
			qw.and(fn-> fn.eq("status", 1).or().eq("status", 2))
			.like(!StringUtils.isEmpty(dto.getResidenceName()), "residence_name", dto.getResidenceName())
			.like(!StringUtils.isEmpty(dto.getFulladdress()), "fulladdress", dto.getFulladdress());
			if(!StringUtils.isEmpty(dto.getCreatedName())) {
				qw.and(fn-> fn.like("created_name", dto.getCreatedName()).or().like("last_update_name", dto.getCreatedName()));
			}
			if(dto.getProvince()!=null && dto.getProvince()>0) {
				qw.eq("province", dto.getProvince());
			}
			if(dto.getCity()!=null && dto.getCity()>0) {
				qw.eq("city", dto.getCity());
			}
			if(dto.getDistrict()!=null && dto.getDistrict()>0) {
				qw.eq("district", dto.getDistrict());
			}
			qw.orderByDesc("creation_date");
			IPage<Residence> iPage = residenceService.page(page, qw);
			result = ResultUtil.ok("查询成功", list);
			list = iPage.getRecords();
			result.setCount(iPage.getTotal());
		} catch (Exception e) {
			result.setMsg(e.getMessage());
		}
		result.setData(list);
		return result;
	}

	/**
	 * 
	 * @Title: saveOrUpdate 
	 * @Description: (添加或者修改接口) 
	 * @param dto
	 * @return  
	 * @date 2019年10月17日 下午4:07:24
	 * @author 董兴隆
	 */
	@RequestMapping("/saveOrUpdate")
	@ResponseBody
	public ResultUtil<Residence> saveOrUpdate(Residence dto) {
		User user = getUser();
		if (isNotLogIn()) {
			return ResultUtil.error("未找到登录用户");
		}
		boolean save = true;
		if (dto.getResidenceId() != null && dto.getResidenceId() > 0) {
			dto.setLastUpdateDate(LocalDateTime.now());
			dto.setLastUpdatedId(user.getUserId());
			dto.setLastUpdateName(user.getName());
			if(StringUtils.isEmpty(dto.getMemo())) {
				dto.setMemo(" ");
			}
			save = false;
		} else {
			dto.setCreatedId(user.getUserId());
			dto.setCreatedName(user.getName());
			dto.setCreationDate(LocalDateTime.now());
			dto.setStatus(1);
		}
		String fullAddress= getFullAddress(dto.getProvince(), dto.getCity(), dto.getDistrict(), dto.getAddress());
		dto.setFulladdress(fullAddress);
		if (residenceService.saveOrUpdate(dto)) {
			if (save) {
				return ResultUtil.ok("添加成功");
			} else {
				return ResultUtil.ok("修改成功");
			}
		} else {
			if (save) {
				return ResultUtil.error("添加失败");
			} else {
				return ResultUtil.error("修改失败");
			}
		}
	}

	/**
	 * 
	 * @Title: updateYnByResidenceId 
	 * @Description: (修改分类可用状态) 
	 * @param ins
	 * @return  
	 * @date 2019年10月17日 下午4:07:41
	 * @author 董兴隆
	 */
	@RequestMapping("/updateYnByResidenceId")
	@ResponseBody
	public ResultUtil<Residence> updateYnByResidenceId(Residence ins) {
		User user = getUser();
		if (user == null || user.getUserId() == null) {
			return ResultUtil.error("未找到登录用户");
		}
		Residence Residence = residenceService.getById(ins.getResidenceId());
		Residence.setStatus(Residence.getStatus() == 1 ? 2 : 1);
		if (residenceService.saveOrUpdate(Residence)) {
			return ResultUtil.ok("修改成功");
		} else {
			return ResultUtil.error("修改失败");
		}
	}

	/**
	 * 
	 * @Title: deleteByResidenceId 
	 * @Description: (删除民宿分类) 
	 * @param ins
	 * @return  
	 * @date 2019年10月17日 下午4:08:08
	 * @author 董兴隆
	 */
	@RequestMapping("/deleteByResidenceId")
	@ResponseBody
	public ResultUtil<Residence> deleteByResidenceId(Residence ins) {
		User user = getUser();
		if (user == null || user.getUserId() == null) {
			return ResultUtil.error("未找到登录用户");
		}
		ins.setStatus(3);// 删除
		ins.setLastUpdateDate(LocalDateTime.now());
		ins.setLastUpdatedId(user.getUserId());
		ins.setLastUpdateName(user.getName());
		if (residenceService.updateById(ins)) {
			return ResultUtil.ok("删除成功");
		} else {
			return ResultUtil.error("删除失败");
		}
	}
}
