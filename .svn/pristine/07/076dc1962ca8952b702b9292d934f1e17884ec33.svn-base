package com.tourism.fzll.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.fzll.entity.ResidenceClass;
import com.tourism.fzll.entity.User;
import com.tourism.fzll.service.IResidenceClassService;
import com.tourism.fzll.util.PageUtil;
import com.tourism.fzll.util.ResultUtil;

/**
 * @ClassName: ResidenceClassController 
 * @Description: (民宿分类) 
 * @author 董兴隆
 * @date 2019年10月17日 下午1:19:46 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/residence/class")
public class ResidenceClassController extends BaseController {

	@Autowired
	private IResidenceClassService classService;

	/**
	 * 
	 * @Title: page
	 * @Description: (民宿首页页面)
	 * @return
	 * @date 2019年10月17日 下午3:54:22
	 * @author 董兴隆
	 */
	@RequestMapping("/page")
	public ModelAndView page() {
		return new ModelAndView("/residence/class/residenceClassList");
	}

	/**
	 * 
	 * @Title: editPage
	 * @Description: (民宿编辑页面)
	 * @param dto
	 * @return
	 * @date 2019年10月17日 下午3:55:02
	 * @author 董兴隆
	 */
	@RequestMapping("/editPage")
	public ModelAndView editPage(ResidenceClass dto) {
		ModelAndView mv = new ModelAndView("/residence/class/residenceClassEdit");
		if (dto.getResidenceclassId() != null && dto.getResidenceclassId() > 0) {
			ResidenceClass res = classService.getById(dto.getResidenceclassId());
			BeanUtils.copyProperties(res, dto);
		}
		mv.addObject("dto", dto);
		return mv;
	}

	/**
	 * 
	 * @Title: selectData
	 * @Description: (民宿获取数据接口)
	 * @param dto
	 * @return
	 * @date 2019年10月17日 下午3:55:17
	 * @author 董兴隆
	 */
	@RequestMapping("/selectData")
	@ResponseBody
	public ResultUtil<List<ResidenceClass>> selectData(ResidenceClass dto) {
		List<ResidenceClass> list = new ArrayList<>();
		ResultUtil<List<ResidenceClass>> result = ResultUtil.error("查询失败", list);
		PageUtil.initAttr(dto);
		Page<ResidenceClass> page = new Page<>(dto.getPage(), dto.getLimit());
		QueryWrapper<ResidenceClass> qw = new QueryWrapper<ResidenceClass>();
		qw.and(fn -> fn.eq("status", 1).or().eq("status", 2));
		if (!StringUtils.isEmpty(dto.getResidenceclassName())) {
			qw.like("residenceclass_name", dto.getResidenceclassName());
		}
		if (dto.getStatus() != null && dto.getStatus() > 0) {
			qw.eq("status", dto.getStatus());
		}
		if (!StringUtils.isEmpty(dto.getCreatedName())) {
			qw.and(fn -> fn.eq("created_name", dto.getCreatedName()).or().eq("last_update_name", dto.getCreatedName()));
		}
		qw.orderByDesc("creation_date");
		try {
			IPage<ResidenceClass> IPage = classService.page(page, qw);
			result = ResultUtil.ok("查询成功", list);
			list = IPage.getRecords();
			result.setCount(IPage.getTotal());
		} catch (Exception e) {
			result.setMsg(e.getMessage());
		}
		result.setData(list);
		return result;
	}

	/**
	 * 
	 * @Title: saveOrUpdate 
	 * @Description: (添加或者修改接口) 
	 * @param res
	 * @return  
	 * @date 2019年10月17日 下午4:07:24
	 * @author 董兴隆
	 */
	@RequestMapping("/saveOrUpdate")
	@ResponseBody
	public ResultUtil<ResidenceClass> saveOrUpdate(ResidenceClass res) {
		User user = getUser();
		if (isNotLogIn()) {
			return ResultUtil.error("未找到登录用户");
		}
		boolean save = true;
		if (res.getResidenceclassId() != null && res.getResidenceclassId() > 0) {
			res.setLastUpdateDate(LocalDateTime.now());
			res.setLastUpdatedId(user.getUserId());
			res.setLastUpdateName(user.getName());
			save = false;
		} else {
			res.setCreatedId(user.getUserId());
			res.setCreatedName(user.getName());
			res.setCreationDate(LocalDateTime.now());
			res.setStatus(1);
		}
		if (classService.saveOrUpdate(res)) {
			if (save) {
				return ResultUtil.ok("添加成功");
			} else {
				return ResultUtil.ok("修改成功");
			}
		} else {
			if (save) {
				return ResultUtil.error("添加失败");
			} else {
				return ResultUtil.error("修改失败");
			}
		}
	}

	/**
	 * 
	 * @Title: updateYnByResidenceclassId 
	 * @Description: (修改分类可用状态) 
	 * @param ins
	 * @return  
	 * @date 2019年10月17日 下午4:07:41
	 * @author 董兴隆
	 */
	@RequestMapping("/updateYnByResidenceclassId")
	@ResponseBody
	public ResultUtil<ResidenceClass> updateYnByResidenceclassId(ResidenceClass ins) {
		User user = getUser();
		if (user == null || user.getUserId() == null) {
			return ResultUtil.error("未找到登录用户");
		}
		ResidenceClass ResidenceClass = classService.getById(ins.getResidenceclassId());
		ResidenceClass.setStatus(ResidenceClass.getStatus() == 1 ? 2 : 1);
		if (classService.saveOrUpdate(ResidenceClass)) {
			return ResultUtil.ok("修改成功");
		} else {
			return ResultUtil.error("修改失败");
		}
	}

	/**
	 * 
	 * @Title: deleteByResidenceclassId 
	 * @Description: (删除民宿分类) 
	 * @param ins
	 * @return  
	 * @date 2019年10月17日 下午4:08:08
	 * @author 董兴隆
	 */
	@RequestMapping("/deleteByResidenceclassId")
	@ResponseBody
	public ResultUtil<ResidenceClass> deleteByResidenceclassId(ResidenceClass ins) {
		User user = getUser();
		if (user == null || user.getUserId() == null) {
			return ResultUtil.error("未找到登录用户");
		}
		ins.setStatus(3);// 删除
		ins.setLastUpdateDate(LocalDateTime.now());
		ins.setLastUpdatedId(user.getUserId());
		ins.setLastUpdateName(user.getName());
		if (classService.updateById(ins)) {
			return ResultUtil.ok("删除成功");
		} else {
			return ResultUtil.error("删除失败");
		}
	}
}
