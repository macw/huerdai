package com.tourism.hu.apicontroller;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.CustomerInfoConstant;
import com.tourism.hu.constant.SmstestConstant;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.CustomerInfo;
import com.tourism.hu.entity.Smstest;
import com.tourism.hu.mapper.CustomerInfoMapper;
import com.tourism.hu.service.ICustomerInfoService;
import com.tourism.hu.service.ISmstestService;
import com.tourism.hu.util.*;
import net.sf.json.JSONObject;
import org.apache.shiro.session.InvalidSessionException;
import org.apache.shiro.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 董兴隆
 * @ClassName: CustomerInfoApiController
 * @Description: ()
 * @date 2019年11月11日 下午5:02:01
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/api/customerInfoApi")
public class CustomerInfoApiController extends BaseController {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Resource
    private CustomerInfoMapper customerInfoMapper;

    @Resource
    private ICustomerInfoService iCustomerInfoService;

    @Resource
    private ISmstestService iSmstestService;

    @Resource
    private RedisTemplate redisTemplate;


    @GetMapping("/toRegister")
    public ModelAndView toRegister() {
        ModelAndView mv = new ModelAndView("wechatArea/agentRegister");
        return mv;
    }

    /**
     * 前台用户登录接口
     *
     * @param loginname
     * @param mobilePhone
     * @param password
     */
    @GetMapping("/userCustomerInfoLogin")
    public void userCustomerInfoLogin(String loginname, String mobilePhone, String password) {
        if (password != null) {
            password = DigestUtils.md5(password);
        }

        CustomerInfo customerInfo = null;
        try {
            customerInfo = customerInfoMapper.selectOne(new QueryWrapper<CustomerInfo>().eq("username", loginname).eq("password", password));
            String sessionid = getRequest().getSession().getId();
            ResponseResult<Object> result = ResponseResult.ERROR;
            Session session = shiroSession();
            if (customerInfo != null) {
                redisTemplate.opsForValue().set(sessionid, customerInfo.getCustomerId());
                session.setAttribute("LOGIN_USER", customerInfo);
                result.setMsg("登录成功！");
                result.setData(sessionid);
                ResponseResult.Step(result);
                return;
            } else {
                customerInfo = customerInfoMapper.selectOne(new QueryWrapper<CustomerInfo>().eq("phone", mobilePhone).eq("password", password));
                if (customerInfo != null) {
                    redisTemplate.opsForValue().set(sessionid, customerInfo.getCustomerId());
                    session.setAttribute("LOGIN_USER", customerInfo);
                    result.setMsg("登录成功！");
                    result.setData(sessionid);
                    ResponseResult.Step(result);
                    return;
                } else {
                    ResponseResult.Step(ResponseResult.ERROR("登录失败，请联系系统管理员"));
                    return;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            log.debug("传参 loginname= " + loginname);
            log.debug("传参 mobilePhone= " + mobilePhone);
            log.debug("传参 password= " + password);
            log.debug(e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("登录失败，请联系系统管理员"));
            return;
        }
    }

    /**
     * 使用手机验证码进行登录
     *
     * @param mobilePhone
     * @param code
     */
    @RequestMapping("/usePhoneLogin")
    public void usePhoneLogin(String mobilePhone, String code) {
        Smstest smstest = null;
        try {
            smstest = iSmstestService.getOne(new QueryWrapper<Smstest>().eq("mobile_number", mobilePhone).eq("usable", SmstestConstant.YES_USABLE).eq("sended", SmstestConstant.YES_SEND));
            //如果输入的验证码和 发送的验证码相同，则登录成功
            if (code.equals(smstest.getValidateCode())) {
                CustomerInfo customerInfo = iCustomerInfoService.getOne(new QueryWrapper<CustomerInfo>().eq("phone", mobilePhone));
                String sessionid = getRequest().getSession().getId();
                Session session = shiroSession();
                redisTemplate.opsForValue().set(sessionid, customerInfo.getCustomerId());
                session.setAttribute("LOGIN_USER", customerInfo);
                //并将这条信息设为没用
                smstest.setUsable(SmstestConstant.NO_USABLE);
                iSmstestService.updateById(smstest);
                ResponseResult<Object> result = new ResponseResult<>();
                result.setMsg("登录成功！");
                result.setData(sessionid);
                ResponseResult.Step(result);
            } else {
                //并将这条信息设为没用
                smstest.setUsable(SmstestConstant.NO_USABLE);
                iSmstestService.updateById(smstest);
                ResponseResult.Step(ResponseResult.ERROR("验证码错误，登录失败！"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.debug("mobilePhone===" + mobilePhone);
            log.debug("Exception===" + e.getMessage());
        } finally {
            if (smstest != null) {
                //并将这条信息设为没用
                smstest.setUsable(SmstestConstant.NO_USABLE);
                iSmstestService.updateById(smstest);
            }
        }

    }

    /**
     * 注册，或者  登录 方法
     * 使用手机验证码进行登录
     * 如果没有这个用户，就注册，有的话，就登录
     *
     * @param mobilePhone
     * @param code
     */
    @RequestMapping("/usePhoneLoginOrRegister")
    public void usePhoneLoginOrRegister(String mobilePhone, String code) {
        CustomerInfo customerInfo1 = customerInfoMapper.selectOne(new QueryWrapper<CustomerInfo>().eq("phone", mobilePhone));

        Smstest smstest = iSmstestService.getOne(new QueryWrapper<Smstest>().eq("mobile_number", mobilePhone).eq("usable", SmstestConstant.YES_USABLE).eq("sended", SmstestConstant.YES_SEND).gt("dead_line", LocalDateTime.now()));
        if (smstest==null){
            ResponseResult.Step(ResponseResult.ERROR("验证码错误或者失效，请重新获取！"));
        }
        //验证手机号是否已存在
        //不存在， 则执行注册
        if (customerInfo1 == null) {
            try {
                //如果输入的验证码和 发送的验证码相同，则注册成功
                if (code.equals(smstest.getValidateCode())) {
                    CustomerInfo customerInfo = new CustomerInfo();
                    customerInfo.setNickname("户二代_"+UUIDUtil.getUUID().substring(0,5));
                    customerInfo.setUserStats(CustomerInfoConstant.USE_STATUS);
                    customerInfo.setRegisterTime(LocalDateTime.now());
                    customerInfo.setRegtype(CustomerInfoConstant.MB_REG);
                    customerInfo.setMobilePhone(mobilePhone);
                    customerInfo.setIsCuslevel(CustomerInfoConstant.NO_CUSLEVEL);
                    boolean save = iCustomerInfoService.save(customerInfo);
                    String sessionid = getRequest().getSession().getId();
                    Session session = shiroSession();
                    if (save) {
                        //如果注册成功，则直接登录
                        redisTemplate.opsForValue().set(sessionid, customerInfo.getCustomerId());
                        session.setAttribute("LOGIN_USER", customerInfo);
                        //并将这条信息设为没用
                        smstest.setUsable(SmstestConstant.NO_USABLE);
                        iSmstestService.updateById(smstest);
                        ResponseResult<Object> result = new ResponseResult<>();
                        result.setMsg("注册成功");
                        ResponseResult.Step(result);
                        return;
                    } else {
                        ResponseResult.Step(ResponseResult.ERROR("注册失败！"));
                        return;
                    }
                } else {
                    ResponseResult.Step(ResponseResult.ERROR("失败！验证码错误或超时"));
                    return;
                }
            } catch (InvalidSessionException e) {
                e.printStackTrace();
                log.debug("mobilePhone===" + mobilePhone);
                log.debug("InvalidSessionException===" + e.getMessage());
                ResponseResult.Step(ResponseResult.ERROR("注册失败！"));
            } catch (Exception e) {
                log.debug("mobilePhone===" + mobilePhone);
                log.debug("Exception===" + e.getMessage());
                ResponseResult.Step(ResponseResult.ERROR("注册失败！"));
            }
        } else {
            //执行登录
            try {

                //如果输入的验证码和 发送的验证码相同，则登录成功
                if (smstest.getValidateCode().equals(code)) {
                    CustomerInfo customerInfo = iCustomerInfoService.getOne(new QueryWrapper<CustomerInfo>().eq("phone", mobilePhone));
                    String sessionid = getRequest().getSession().getId();
                    Session session = shiroSession();
                    redisTemplate.opsForValue().set(sessionid, customerInfo.getCustomerId());
                    session.setAttribute("LOGIN_USER", customerInfo);
                    //并将这条信息设为没用
                    smstest.setUsable(SmstestConstant.NO_USABLE);
                    iSmstestService.updateById(smstest);
                    ResponseResult<Object> result = new ResponseResult<>();
                    result.setMsg("登录成功！");
                    result.setData(sessionid);
                    ResponseResult.Step(result);
                } else {
                    ResponseResult.Step(ResponseResult.ERROR("验证码错误或超时，登录失败！"));
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.debug("mobilePhone===" + mobilePhone);
                log.debug("Exception===" + e.getMessage());
                ResponseResult.Step(ResponseResult.ERROR("登录失败！"));
            }
        }
    }


    /**
     * 前台 用户 用户名密码注册接口
     *
     * @param customerInfo
     * @return
     */
    @GetMapping("/customerInfoReg")
    public void customerInfoReg(CustomerInfo customerInfo) {
        log.info(customerInfo.toString());
        //验证手机号是否已存在
        if (customerInfo.getMobilePhone() != null) {
            CustomerInfo customerInfo1 = customerInfoMapper.selectOne(new QueryWrapper<CustomerInfo>().eq("phone", customerInfo.getMobilePhone()));
            if (customerInfo1 != null) {
                ResponseResult.Step(ResponseResult.ERROR("注册失败，该手机号已存在"));
                return;
            }
        }
        //验证用户名是否已存在
        if (customerInfo.getLoginName() != null) {
            CustomerInfo customerInfo1 = customerInfoMapper.selectOne(new QueryWrapper<CustomerInfo>().eq("username", customerInfo.getLoginName()));
            if (customerInfo1 != null) {
                ResponseResult.Step(ResponseResult.ERROR("注册失败，该用户名已存在"));
                return;
            }
        }
        //MD5加密
        String password = null;
        if (customerInfo.getPassword() == null) {
            ResponseResult.Step(ResponseResult.ERROR("密码不能为空！"));
            return;
        } else {
            password = DigestUtils.md5(customerInfo.getPassword());
        }
        customerInfo.setPassword(password);
        //设置默认用户昵称
        if (customerInfo.getNickname() == null) {
            customerInfo.setNickname("户二代_"+UUIDUtil.getUUID().substring(0,5));
        }
        customerInfo.setUserStats(CustomerInfoConstant.USE_STATUS);
        customerInfo.setRegisterTime(LocalDateTime.now());
        try {
            int i = customerInfoMapper.insert(customerInfo);
            ResponseResult.Step(ResponseResult.flag(i));
        } catch (Exception e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR("注册失败，请联系系统管理员"));
        }
    }


    /**
     * 发送验证码
     *
     * @param phone
     */
    @RequestMapping("/sendSMS")
    public void sendSMS(String phone) {
        String smsCode = MemberRulesUtil.createRandom(true, 6);
        SendSmsResponse sms = MoblieMessageUtil.sendSms(phone, smsCode);
        String code = sms.getCode();
        System.out.println(code);
        try {
            if (code.equals("OK")) {
                Smstest smstest = new Smstest();
                smstest.setCustomerId(null);
                smstest.setMobileNumber(phone);
                smstest.setValidateCode(smsCode);
                smstest.setUsable(SmstestConstant.YES_USABLE);
                smstest.setSended(SmstestConstant.YES_SEND);
                smstest.setDeadLine(DateConverterUtil.getAfterFiveMinutes(1));
                boolean save = iSmstestService.save(smstest);
                if (save) {
                    ResponseResult.Step(ResponseResult.SUCCESS);
                    return;
                }
                ResponseResult.Step(ResponseResult.ERROR("系统错误！"));
            } else {
                log.debug("phone===" + phone);
                ResponseResult.Step(ResponseResult.ERROR("请勿重复发送或系统故障！"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR("异常，发送失败！！！"));
        }
    }


    /**
     * 手机验证码注册
     * 验证 验证码是否正确
     *
     * @param mobilePhone
     * @param code
     */
    @RequestMapping("/mobilePhoneReg")
    public void mobilePhoneReg(String mobilePhone, String code) {
        //验证手机号是否已存在
        if (mobilePhone != null) {
            CustomerInfo customerInfo1 = customerInfoMapper.selectOne(new QueryWrapper<CustomerInfo>().eq("phone", mobilePhone));
            if (customerInfo1 != null) {
                ResponseResult.Step(ResponseResult.ERROR("注册失败，该手机号已存在"));
                return;
            }
        }
        Smstest smstest = iSmstestService.getOne(new QueryWrapper<Smstest>().eq("mobile_number", mobilePhone).eq("usable", SmstestConstant.YES_USABLE).eq("sended", SmstestConstant.YES_SEND).gt("dead_line", LocalDateTime.now()));
        if (smstest==null){
            ResponseResult.Step(ResponseResult.ERROR("验证码错误或者失效，请重新获取！"));
        }
        try {
            //如果输入的验证码和 发送的验证码相同，则注册成功
            if (code.equals(smstest.getValidateCode())) {
                CustomerInfo customerInfo = new CustomerInfo();
                customerInfo.setNickname("户二代，行动者");
                customerInfo.setUserStats(CustomerInfoConstant.USE_STATUS);
                customerInfo.setRegisterTime(LocalDateTime.now());
                customerInfo.setRegtype(CustomerInfoConstant.MB_REG);
                customerInfo.setMobilePhone(mobilePhone);
                customerInfo.setIsCuslevel(CustomerInfoConstant.NO_CUSLEVEL);
                boolean save = iCustomerInfoService.save(customerInfo);
                String sessionid = getRequest().getSession().getId();
                Session session = shiroSession();
                if (save) {
                    //如果注册成功，则直接登录
                    redisTemplate.opsForValue().set(sessionid, customerInfo.getCustomerId());
                    session.setAttribute("LOGIN_USER", customerInfo);
                    //并将这条信息设为没用
                    smstest.setUsable(SmstestConstant.NO_USABLE);
                    iSmstestService.updateById(smstest);
                    ResponseResult.Step(ResponseResult.SUCCESS);
                    return;
                } else {
                    ResponseResult.Step(ResponseResult.ERROR("注册失败！"));
                    return;

                }
            }
        } catch (InvalidSessionException e) {
            e.printStackTrace();
            log.debug("mobilePhone===" + mobilePhone);
            log.debug("InvalidSessionException===" + e.getMessage());
        } catch (Exception e) {
            log.debug("mobilePhone===" + mobilePhone);
            log.debug("Exception===" + e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("失败！"));
        }
    }


    /**
     * 前台，用户注册唯一性校验规则
     *
     * @param loginname
     * @param mobilePhone
     * @return
     */
    @GetMapping("/userValCustomerName")
    public void userValCustomerName(String loginname, String mobilePhone) {
        CustomerInfo customerInfo = null;
        if (loginname != null && loginname != "") {
            customerInfo = customerInfoMapper.selectOne(new QueryWrapper<CustomerInfo>().eq("username", loginname));
        }
        if (mobilePhone != null && mobilePhone != "") {
            customerInfo = customerInfoMapper.selectOne(new QueryWrapper<CustomerInfo>().eq("phone", mobilePhone));

        }
        ResponseResult<CustomerInfo> result = ResponseResult.ERROR;
        if (customerInfo == null) {
            result = ResponseResult.SUCCESS;
        }
        ResponseResult.Step(result);
    }

    /**
     * 校验，验证码是否有效
     * @param mobilePhone
     * @param code
     */
    @RequestMapping("/validCode")
    public void validCode(String mobilePhone, String code) {
        try {
            Smstest smstest = iSmstestService.getOne(new QueryWrapper<Smstest>().eq("mobile_number", mobilePhone).eq("usable", SmstestConstant.YES_USABLE).eq("sended", SmstestConstant.YES_SEND).gt("dead_line", LocalDateTime.now()));
            if (smstest == null) {
                ResponseResult.Step(ResponseResult.ERROR("验证码失败或已失效！"));
                return;
            }
            if (smstest.getValidateCode().equals(code)) {
                ResponseResult.Step(ResponseResult.SUCCESS);
            } else {
                ResponseResult.Step(ResponseResult.ERROR("验证码错误,请重新获取！"));
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.debug("Exception===" + e.getMessage());
            log.debug("mobilePhone===" + mobilePhone);
            log.debug("code===" + code);
            ResponseResult.Step(ResponseResult.ERROR("失败！"));
        }
    }

    /**
     * 更新密码
     */
    @RequestMapping("/updatePwd")
    public void updatePwd(String mobilePhone, String password) {
        try {
            CustomerInfo customerInfo = iCustomerInfoService.getOne(new QueryWrapper<CustomerInfo>().eq("phone", mobilePhone));
            if (customerInfo == null) {
                ResponseResult.Step(ResponseResult.ERROR("用户不存在！"));
                return;
            }
            customerInfo.setPassword(DigestUtils.md5(password));
            if (iCustomerInfoService.updateById(customerInfo)) {
                ResponseResult.Step(ResponseResult.SUCCESS);
                return;
            } else {
                ResponseResult.Step(ResponseResult.ERROR("更新密码失败！"));
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.debug("Exception===" + e.getMessage());
            log.debug("mobilePhone===" + mobilePhone);
            log.debug("password===" + password);
            ResponseResult.Step(ResponseResult.ERROR("失败！"));
        }
    }


    /**
     * 微信群管理功能的用户登录
     *
     * @param loginname
     * @param password
     * @return
     */
    @PostMapping("/customerInfoLogin")
    public ResponseResult<CustomerInfo> customerInfoLogin(String loginname, String password) {
        if (password != null) {
            password = DigestUtils.md5(password);
        }
        CustomerInfo customerInfo = customerInfoMapper.selectOne(new QueryWrapper<CustomerInfo>().eq("username", loginname).eq("password", password));
        CustomerInfo customerInfo2 = customerInfoMapper.selectOne(new QueryWrapper<CustomerInfo>().eq("phone", loginname).eq("password", password));
        String sessionid = getRequest().getSession().getId();
        if (customerInfo != null) {
            //如果用户审核未通过返回提醒
           /* if ( customerInfo.getUserStats()==CustomerInfoConstant.DISUSE_STATUS){
                return ResponseResult.STATUS;
            }*/
            redisTemplate.opsForValue().set(sessionid, customerInfo.getCustomerId());
            return ResponseResult.SUCCESS;
        }
        if (customerInfo2 != null) {
            //如果用户审核未通过返回提醒
          /*  if ( customerInfo.getUserStats()==CustomerInfoConstant.DISUSE_STATUS){
                return ResponseResult.STATUS;
            }*/
            redisTemplate.opsForValue().set(sessionid, customerInfo2.getCustomerId());
            return ResponseResult.SUCCESS;
        }
        return ResponseResult.ERROR;
    }

    /**
     * 微信群管理，唯一性验证规则
     *
     * @param loginname
     * @param mobilePhone
     * @return
     */
    @RequestMapping("/valCustomerName")
    public ResponseResult valCustomerName(String loginname, String mobilePhone) {
        CustomerInfo customerInfo = null;
        if (loginname != null && loginname != "") {
            customerInfo = customerInfoMapper.selectOne(new QueryWrapper<CustomerInfo>().eq("username", loginname));
        }
        if (mobilePhone != null && mobilePhone != "") {
            customerInfo = customerInfoMapper.selectOne(new QueryWrapper<CustomerInfo>().eq("phone", mobilePhone));

        }
        if (customerInfo == null) {
            return ResponseResult.SUCCESS;
        } else {
            return ResponseResult.ERROR;
        }

    }

    /**
     * 微信群管理注册
     *
     * @param customerInfo
     * @return
     */
    @PostMapping("/wechatGroupReg")
    public ResponseResult<CustomerInfo> WechatReg(CustomerInfo customerInfo) {
        String password = DigestUtils.md5(customerInfo.getPassword());
        customerInfo.setPassword(password);
        customerInfo.setRegtype(CustomerInfoConstant.WECHAT_REG);
        customerInfo.setUserStats(CustomerInfoConstant.DISUSE_STATUS);
        customerInfo.setRegisterTime(LocalDateTime.now());
        int i = customerInfoMapper.insert(customerInfo);
        return ResponseResult.flag(i);
    }


    /**
     * 微信群管理注册, 跨域注册
     *
     * @param customerInfo
     * @return
     */
    @RequestMapping("/wechatGroupReg222")
    public void exchangeJson(CustomerInfo customerInfo, HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setContentType("text/plain");
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);
            Map<String, Integer> map = new HashMap<String, Integer>();

            String password = DigestUtils.md5(customerInfo.getPassword());
            customerInfo.setPassword(password);
            customerInfo.setRegtype(CustomerInfoConstant.WECHAT_REG);
            customerInfo.setRegisterTime(LocalDateTime.now());
            int i = customerInfoMapper.insert(customerInfo);

            map.put("result", i);
            PrintWriter out = response.getWriter();
            JSONObject resultJSON = JSONObject.fromObject(map); //根据需要拼装json
            String jsonpCallback = request.getParameter("jsonpCallback");//客户端请求参数
            log.info("jsonpCallback==" + jsonpCallback);
            out.println(jsonpCallback + "(" + resultJSON.toString(1, 1) + ")");//返回jsonp格式数据
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
