package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.entity.*;
import com.tourism.hu.service.IStarService;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.GlobalConfigUtil;
import com.tourism.hu.util.MsgUtil;
import com.tourism.hu.util.ResultUtil;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.tourism.hu.controller.BaseController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hu
 * @since 2019-12-09
 */
@RestController
@RequestMapping("/star")
public class StarController extends BaseController {

    @Resource
    private IStarService iStarService;


    @RequestMapping("/toStarList")
    public ModelAndView hello() {
        ModelAndView mv = new ModelAndView("star/starList");
        return mv;
    }

    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page, int limit) {
        Page<Star> page1 = new Page<>(page, limit);
        IPage<Star> IPage = iStarService.page(page1, new QueryWrapper<Star>().orderByDesc("star_sort"));
        //获取分页后的数据
        List<Star> List = IPage.getRecords();
        //获取分页总条数
        long total = IPage.getTotal();
        //将数据封装到dataUtil对象
        DataUtil dataUtil = DataUtil.success();
        dataUtil.setData(List);
        dataUtil.setCount(total);
        return dataUtil;
    }

    @RequestMapping("/toStarEdit")
    public ModelAndView toStarEdit(Integer starId) {
        ModelAndView mv = new ModelAndView("star/starEdit");
        if (starId!=null){
            Star star = iStarService.getById(starId);
            mv.addObject("st", star);
        }
        return mv;
    }


    /**
     * 去富文本编辑器页面
     * @param starId
     * @return
     */
    @RequestMapping("/editInfo")
    public ModelAndView  editInfo(Integer starId) {
        ModelAndView mv = new ModelAndView("star/info");
        Star star = iStarService.getById(starId);
        mv.addObject("sta", star);
        return mv;
    }

    /**
     * 添加 或者更新达人介绍
     * @param star
     * @return
     */
    @RequestMapping("/addOrUpdate")
    public MsgUtil addStar(MultipartFile starTitle,MultipartFile starImage,MultipartFile starVo,MultipartFile starVideo, Star star) {
        String basePath = "starImg";
        //简介头图上传
        if(starTitle != null && starTitle.getSize() > 0) {
            String url=null;
            try {
                url = aliOSSUpload(starTitle, basePath);
            } catch (IOException e) {
                logger.debug("starTitle 图片上传失败---->"+e.getMessage());
                return MsgUtil.error("图片上传失败");
            }
            star.setStarTitleUrl(url);
        }
        //简介头像上传
        if(starImage != null && starImage.getSize() > 0) {
            String url=null;
            try {
                url = aliOSSUpload(starImage, basePath);
            } catch (IOException e) {
                logger.debug("star/starImage 图片上传失败---->"+e.getMessage());
                return MsgUtil.error("图片上传失败");
            }
            star.setStarImageUrl(url);
        }
        //视频预览图上传
        if(starVo != null && starVo.getSize() > 0) {
            String url=null;
            try {
                url = aliOSSUpload(starVo, basePath);
            } catch (IOException e) {
                logger.debug("star/starVo 图片上传失败---->"+e.getMessage());
                return MsgUtil.error("图片上传失败");
            }
            star.setStarVideoIcon(url);
        }
        //视频上传
        if(starVideo != null && starVideo.getSize() > 0) {
            String url=null;
            try {
                url = aliOSSUpload(starVideo, basePath);
            } catch (IOException e) {
                logger.debug("star/starVideo 视频上传失败---->"+e.getMessage());
                return MsgUtil.error("视频上传失败");
            }
            star.setStarVideoUrl(url);
        }
        User user = getUser();

        if (star.getStarId()==null) {
            star.setUserId(user.getUserId());
            star.setUserName(user.getName());
            star.setCreateTime(LocalDateTime.now());
            boolean save = iStarService.save(star);
            return MsgUtil.flagSave(save);
        }else {
            star.setUpdateTime(LocalDateTime.now());
            boolean save = iStarService.updateById(star);
            return MsgUtil.flagSave(save);
        }
    }


    @RequestMapping("/deleteOne")
    public MsgUtil deleteOne(Integer aid){
        boolean b = iStarService.removeById(aid);
        return MsgUtil.flagSave(b);
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids) {
        List list = new ArrayList();
        for (Integer id : ids) {
            list.add(id);
        }
        boolean i = iStarService.removeByIds(list);
        return MsgUtil.flagSave(i);
    }

}
