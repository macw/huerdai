package com.tourism.hu.apicontroller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tourism.hu.constant.BaseConstant;
import com.tourism.hu.constant.GoodsDtlConstant;
import com.tourism.hu.constant.GoodsPicConstant;
import com.tourism.hu.constant.SeckillConstant;
import com.tourism.hu.controller.BaseController;
import com.tourism.hu.entity.*;
import com.tourism.hu.entity.vo.GoodsCategoryVo;
import com.tourism.hu.entity.vo.GoodsDetailsVo;
import com.tourism.hu.entity.vo.GoodsDtlVo;
import com.tourism.hu.entity.vo.GoodsVo;
import com.tourism.hu.mapper.GoodsCategoryMapper;
import com.tourism.hu.mapper.GoodsMapper;
import com.tourism.hu.service.*;
import com.tourism.hu.util.ResponseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 董兴隆
 * @ClassName: GoodsApiController
 * @Description: ()
 * @date 2019年11月11日 下午5:02:06
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020
 * Company       Huerdai Henan LTD.
 */
@RestController
@RequestMapping("/api/goodsApi")
public class GoodsApiController extends BaseController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private GoodsMapper goodsMapper;

    @Resource
    private IGoodsService iGoodsService;

    @Resource
    private GoodsCategoryMapper goodsCategoryMapper;

    @Resource
    private IGoodsCategoryService iGoodsCategoryService;

    @Resource
    private IGoodsEquityService iGoodsEquityService;

    @Resource
    private IEquityService iEquityService;

    @Resource
    private ISeckillService iSeckillService;

    @Resource
    private IGoodsDtlService iGoodsDtlService;

    @Resource
    private IGoodsDtlImgService iGoodsDtlImgService;

    @Resource
    private IGoodsPicService iGoodsPicService;

    /**
     * 商品分类列表展示接口 （暂时不用）
     * <p>
     * 预留优惠券数据
     * 预留商品返利，返利价格显示
     * 预留正在拼团数量数据显示
     * 预留拼团人数头像数据显示
     */
    @RequestMapping("/GoodsCategoryList")
    public void GoodsCategoryList() {
        try {
            List<GoodsCategoryVo> GoodsCategoryVos = goodsCategoryMapper.selectGoodsCategoryList();
            for (GoodsCategoryVo GoodsCategoryVo : GoodsCategoryVos) {
                List<GoodsVo> GoodsVos = GoodsCategoryVo.getGoodsLists();
                for (GoodsVo goodsList : GoodsVos) {
                    //计算用户的省钱数值
                    BigDecimal percentage = getPercentage(goodsList.getPrice(), goodsList.getExitPrice(), goodsList.getActivityPrice());
                    //赋值,并四色五入只保留两位小数，)     四舍五入如，2.35变成2.4
                    goodsList.setSavePrice(percentage);
                }
            }

            ResponseResult<List<GoodsCategoryVo>> result = new ResponseResult<>();
            result.setData(GoodsCategoryVos);
            result.setMsg("查询成功！");
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
        }
    }

    /**
     * 查询商品分类
     */
    @RequestMapping("/selectGoodsCategory")
    public void selectGoodsCategory() {
        try {
            List<GoodsCategoryVo> goodsCategoryVoList = goodsCategoryMapper.selectCategoryList();
            for (GoodsCategoryVo goodsCategoryVo : goodsCategoryVoList) {
                goodsCategoryVo.setLink(BaseConstant.OS_PATH + "api/goodsApi/selectGoodsListByCategory/" + goodsCategoryVo.getClassId());
            }
            ResponseResult<List<GoodsCategoryVo>> result = new ResponseResult<>();
            result.setData(goodsCategoryVoList);
            result.setMsg("查询成功！");
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug(e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
        }
    }

    /**
     * 根据分类id查询，该分类下的商品
     *
     * @param classId
     */
    @RequestMapping("/selectGoodsListByCategory/{classId}")
    public void selectGoodsListByCategory(@PathVariable Integer classId) {
        try {
            List<GoodsVo> goodsVos = goodsMapper.selectGoodsListByCategory(classId);
            for (GoodsVo goodsVo : goodsVos) {
                //计算用户的省钱数值
                BigDecimal percentage = getPercentage(goodsVo.getPrice(), goodsVo.getExitPrice(), goodsVo.getActivityPrice());
                //赋值,并四色五入只保留两位小数，)     四舍五入如，2.35变成2.4
                goodsVo.setSavePrice(percentage);
                //将出厂价设为0
                goodsVo.setExitPrice(BigDecimal.ZERO);
            }
            ResponseResult<List<GoodsVo>> result = new ResponseResult<>();
            result.setData(goodsVos);
            result.setMsg("查询成功！");
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("classId===" + classId);
            logger.debug(e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("查询失败，请重试"));
        }
    }


    /**
     * 查询单个商品详情信息
     *
     * @param goodsId
     */
    @RequestMapping("/GoodsDetails")
    public void GoodsDetails(Integer goodsId) {
        try {
            GoodsDetailsVo goodsDetails = goodsMapper.selectGoodsDetails(goodsId);
            //获取用户对于该商品的返利价格,   商品单价-出厂价
            BigDecimal percentage = getPercentage(goodsDetails.getPrice(), goodsDetails.getExitPrice(), goodsDetails.getActivityPrice());
            goodsDetails.setSavePrice(percentage);
            //计算拼团节省价格
            Seckill seckill = iSeckillService.getOne(new QueryWrapper<Seckill>().ge("collage_count", 2).eq("type", SeckillConstant.GOODSID).eq("goods_id", goodsId).eq("status", BaseConstant.USE_STATUS));
            BigDecimal percentageGroup = null;
            if (seckill != null) {
                percentageGroup = getPercentage(goodsDetails.getPrice(), goodsDetails.getExitPrice(), seckill.getSeckillPrice());
            }
            //设置 商品信息的 权益
            List<Equity> list = new ArrayList<>();
            List<GoodsEquity> equityList = iGoodsEquityService.list(new QueryWrapper<GoodsEquity>().eq("goods_id", goodsId));
            for (GoodsEquity goodsEquity : equityList) {
                Equity equity = iEquityService.getOne(new QueryWrapper<Equity>().eq("equity_id", goodsEquity.getEquityId()).eq("status", BaseConstant.USE_STATUS));
                list.add(equity);
            }
            goodsDetails.setEquityList(list);

            goodsDetails.setSaveGroupPrice(percentageGroup);
            ResponseResult result = new ResponseResult();
            result.setData(goodsDetails);
            result.setMsg("查询成功！");
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
        }
    }

    /**
     * 查询商品的规格
     *
     * @param goodsId
     * @param dtlIdArray
     */
    @RequestMapping("/selectGoodsDtlVo")
    public void selectGoodsDtlVo(Integer goodsId, Integer[] dtlIdArray) {
        logger.debug("ssssssssssss");
        //创建返回实体
        GoodsDtlVo goodsDtlVo = new GoodsDtlVo();

        /**
         * 计算商品的实际价格
         */
        BigDecimal actualPrice = null;
        Goods goods = iGoodsService.getById(goodsId);

        if (goods == null) {
            ResponseResult.Step(ResponseResult.ERROR("商品信息不存在"));
        }
        if (goods.getActivityPrice() != null) {
            actualPrice = goods.getActivityPrice();
        } else {
            actualPrice = goods.getPrice();
        }
        List<String> stringList = new ArrayList<>();
        //如果没选择规格，无数据

        if (dtlIdArray == null || dtlIdArray.length==0){
            goodsDtlVo.setGoodsDtlPrice(actualPrice);
            GoodsPic goodsPic = iGoodsPicService.getOne(new QueryWrapper<GoodsPic>().eq("goods_id", goodsId).eq("type", GoodsPicConstant.LOGO_TYPE));
            goodsDtlVo.setDtlPicUrl(goodsPic.getPictureurl());
            stringList.add("请选择：型号，颜色");
            goodsDtlVo.setDtlStringList(stringList);
            ResponseResult<GoodsDtlVo> result = new ResponseResult<>();
            result.setData(goodsDtlVo);
            result.setMsg("请选择型号");
            ResponseResult.Step(result);
        }


        List<GoodsDtl> dtlList = new ArrayList<>();
        try {
            for (Integer id : dtlIdArray) {
                //根据商品id 和 商品规格主键id 查询商品规格
                GoodsDtl goodsDtl = iGoodsDtlService.getOne(new QueryWrapper<GoodsDtl>().eq("goods_id", goodsId).eq("goodsdtl_id", id));
                if (goodsDtl!=null){
                    dtlList.add(goodsDtl);
                }
            }

            for (GoodsDtl goodsDtl : dtlList) {
                /***
                 * 颜色，选择图片，和价格
                 * 型号，选择价格
                 */
                if (goodsDtl.getType() == GoodsDtlConstant.COLOR_TYPE) {
                    GoodsDtlImg goodsDtlImg = iGoodsDtlImgService.getOne(new QueryWrapper<GoodsDtlImg>().eq("goodsdtl_id", goodsDtl.getGoodsdtlId()));
                    //设置返回图片
                    if (goodsDtlImg != null) {
                        goodsDtlVo.setDtlPicUrl(goodsDtlImg.getClassimgAdress());
                    }
                    //实际价格 + 商品规格加价
                    actualPrice= actualPrice.add(goodsDtl.getDtlPrice());
                    stringList.add(goodsDtl.getSpec());

                } else if (goodsDtl.getType() == GoodsDtlConstant.MODEL_TYPE) {
                    actualPrice= actualPrice.add(goodsDtl.getDtlPrice());
                    stringList.add(goodsDtl.getSpec());
                }
            }
            goodsDtlVo.setGoodsDtlPrice(actualPrice);
            goodsDtlVo.setDtlStringList(stringList);
            ResponseResult<GoodsDtlVo> result = new ResponseResult<>();
            result.setData(goodsDtlVo);
            ResponseResult.Step(result);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("goodsId==="+goodsId);
            logger.debug("dtlIdArray==="+dtlIdArray.length);
            logger.debug("dtlIdArray==="+dtlIdArray.toString());
            logger.debug("Exception==="+e.getMessage());
            ResponseResult.Step(ResponseResult.ERROR("查询失败！"));
        }
    }
}
