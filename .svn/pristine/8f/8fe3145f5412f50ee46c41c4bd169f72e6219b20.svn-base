package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.entity.GoodsPic;
import com.tourism.hu.entity.User;
import com.tourism.hu.mapper.GoodsPicMapper;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.FileUploadUtil;
import com.tourism.hu.util.MsgUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 商品图片表 前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-09
 */
@RestController
@RequestMapping("/goodspic")
public class GoodsPicController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private GoodsPicMapper goodsPicMapper;

    @RequestMapping("/toGoodsPic")
    public ModelAndView toGoodsPic(Integer goodsId) {
        ModelAndView mv = new ModelAndView("goods/pic/goods_pic");
        mv.addObject("goodsId", goodsId);
        return mv;
    }

    @RequestMapping("/toGoodsPicEdit")
    public ModelAndView toGoodsPicEdit(Integer gpId,Integer goodsId) {
        logger.debug("gpid==="+gpId+"---goodsId==="+goodsId);
        ModelAndView mv = new ModelAndView("goods/pic/goodsPicEdit");
        GoodsPic goodsPic = goodsPicMapper.selectById(gpId);
        mv.addObject("goods", goodsPic);
        mv.addObject("goodsId", goodsId);
        return mv;
    }




    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page, int limit) {
        Page<GoodsPic> page1 = new Page<>(page, limit);
        IPage<GoodsPic> roleIPage = goodsPicMapper.selectPage(page1, null);
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<GoodsPic> GoodsPicList = roleIPage.getRecords();
        //获取分页总条数
        long total = roleIPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(GoodsPicList);
        dataUtil.setCount(total);
        return dataUtil;
    }

    @RequestMapping("/selectAllByGoodsId")
    public DataUtil selectAllByGoodsId(int page, int limit, Integer goodsid) {
        Page<GoodsPic> page1 = new Page<>(page, limit);
        IPage<GoodsPic> roleIPage = goodsPicMapper.selectPage(page1, new QueryWrapper<GoodsPic>().eq("goods_id", goodsid));
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<GoodsPic> GoodsPicList = roleIPage.getRecords();
        //获取分页总条数
        long total = roleIPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(GoodsPicList);
        dataUtil.setCount(total);
        return dataUtil;
    }

    @RequestMapping("/addOrUpdate")
    public MsgUtil addOrUpdate(MultipartFile file, GoodsPic goodsPic,Integer goodsId) {
        User user = getUser();
        if (user==null){
            return MsgUtil.error("获取用户信息失败，请重新登录");
        }
        //获取session对象
        HttpSession session = getRequest().getSession();
        if (file != null && file.getSize() > 0) {
            //获取上传文件的路径
            String path = "/img/pic";
            String realPath = session.getServletContext().getRealPath(path);
            logger.info("--------------------------------" + realPath);
            String baseUpload = null;
            try {
                baseUpload = FileUploadUtil.baseUpload(file, realPath);
            } catch (IOException e) {
                e.printStackTrace();
                return MsgUtil.error("图片上传失败！");
            }
            baseUpload = FileUploadUtil.getWebProUrl() + path + "/" + baseUpload;
            long size = file.getSize();
            logger.debug("baseUpload===" + baseUpload);
            goodsPic.setPictureurl(baseUpload);
            goodsPic.setSize(size);
        }
        if (goodsId!=null){
            goodsPic.setGoodsId(goodsId);
        }
        if (goodsPic.getGpId() == null) {
            //添加
            goodsPic.setCreateUserId(user.getUserId());
            goodsPic.setCreateUser(user.getName());
            goodsPic.setCreateTime(LocalDateTime.now());
            int insert = goodsPicMapper.insert(goodsPic);
            MsgUtil msgUtil = MsgUtil.flag(insert);
            return msgUtil;
        } else {
            //更新
            goodsPic.setUpdateTime(LocalDateTime.now());
            goodsPic.setUpdateUser(user.getName());
            goodsPic.setUpdateUserId(user.getUserId());
            int update = goodsPicMapper.updateById(goodsPic);
            MsgUtil msgUtil = MsgUtil.flag(update);
            return msgUtil;
        }
    }



    @RequestMapping("/deleteOne")
    public MsgUtil deleteOne(Integer id) {
        int i = goodsPicMapper.deleteById(id);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids) {
        List list = new ArrayList();
        for (Integer id : ids) {
            list.add(id);
        }
        int i = goodsPicMapper.deleteBatchIds(list);
        return MsgUtil.flag(i);
    }


}
