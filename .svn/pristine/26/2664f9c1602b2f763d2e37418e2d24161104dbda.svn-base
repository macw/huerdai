package com.tourism.fzll.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tourism.fzll.util.PageUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 
 * @ClassName: ScenicSpot 
 * @Description: () 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:11:23 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_scenic_spot")
public class ScenicSpot extends PageUtil implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 景点ID
     */
    @TableId(value = "spot_id", type = IdType.AUTO)
    private Integer spotId;

    /**
     * 景点名称
     */
    private String spotName;

    /**
     * 状态 1启用 2禁用
     */
    private Integer status;

    /**
     * 景点图片
     */
    private String pictureurl;

    /**
     * 景点详情
     */
    private String spotInfo;
    /**
     * 优先级
     */
    private Integer leavel;

    /**
     * 门票价格
     */
    private BigDecimal sellingPrice;

    /**
     * 出厂价
     */
    private BigDecimal floorPrice;

    /**
     * 活动价
     */
    private BigDecimal activityPrice;

    /**
     * 一级利润，返点x%
     */
    private Integer primaryProfit;

    /**
     * 二级利润，返点x%
     */
    private Integer secondaryProfit;

    /**
     * 优惠额度
     */
    private BigDecimal preferentialQuota;

    /**
     * 备注
     */
    private String memo;

    /**
     * 省份，保存是ID
     */
    private Integer province;

    /**
     * 市
     */
    private Integer city;

    /**
     * 区
     */
    private Integer district;

    /**
     * 详细地址
     */
    private String address;
    
    
    
    private String fulladdress;

    /**
     * 旅游公司主键
     */
    private Integer tId;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建者名称
     */
    private String createdName;

    /**
     * 创建者id
     */
    private Integer createdId;

    /**
     * 最后更新者id
     */
    private Integer lastUpdatedId;

    /**
     * 最后更新日期
     */
    private LocalDateTime lastUpdateDate;

    /**
     * 最后更新人名称
     */
    private String lastUpdateName;

	public Integer getSpotId() {
		return spotId;
	}

	public void setSpotId(Integer spotId) {
		this.spotId = spotId;
	}

	public String getSpotName() {
		return spotName;
	}

	public void setSpotName(String spotName) {
		this.spotName = spotName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getPictureurl() {
		return pictureurl;
	}

	public void setPictureurl(String pictureurl) {
		this.pictureurl = pictureurl;
	}

	public Integer getLeavel() {
		return leavel;
	}

	public void setLeavel(Integer leavel) {
		this.leavel = leavel;
	}


	public BigDecimal getActivityPrice() {
		return activityPrice;
	}

	public void setActivityPrice(BigDecimal activityPrice) {
		this.activityPrice = activityPrice;
	}

	public Integer getPrimaryProfit() {
		return primaryProfit;
	}

	public void setPrimaryProfit(Integer primaryProfit) {
		this.primaryProfit = primaryProfit;
	}

	public Integer getSecondaryProfit() {
		return secondaryProfit;
	}

	public void setSecondaryProfit(Integer secondaryProfit) {
		this.secondaryProfit = secondaryProfit;
	}

	public BigDecimal getPreferentialQuota() {
		return preferentialQuota;
	}

	public void setPreferentialQuota(BigDecimal preferentialQuota) {
		this.preferentialQuota = preferentialQuota;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer getProvince() {
		return province;
	}

	public void setProvince(Integer province) {
		this.province = province;
	}

	public Integer getCity() {
		return city;
	}

	public void setCity(Integer city) {
		this.city = city;
	}

	public Integer getDistrict() {
		return district;
	}

	public void setDistrict(Integer district) {
		this.district = district;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer gettId() {
		return tId;
	}

	public void settId(Integer tId) {
		this.tId = tId;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreatedName() {
		return createdName;
	}

	public void setCreatedName(String createdName) {
		this.createdName = createdName;
	}

	public Integer getCreatedId() {
		return createdId;
	}

	public void setCreatedId(Integer createdId) {
		this.createdId = createdId;
	}

	public Integer getLastUpdatedId() {
		return lastUpdatedId;
	}

	public void setLastUpdatedId(Integer lastUpdatedId) {
		this.lastUpdatedId = lastUpdatedId;
	}

	public LocalDateTime getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getLastUpdateName() {
		return lastUpdateName;
	}

	public void setLastUpdateName(String lastUpdateName) {
		this.lastUpdateName = lastUpdateName;
	}

	public String getFulladdress() {
		return fulladdress;
	}

	public void setFulladdress(String fulladdress) {
		this.fulladdress = fulladdress;
	}

	public String getSpotInfo() {
		return spotInfo;
	}

	public void setSpotInfo(String spotInfo) {
		this.spotInfo = spotInfo;
	}

	public BigDecimal getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(BigDecimal sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public BigDecimal getFloorPrice() {
		return floorPrice;
	}

	public void setFloorPrice(BigDecimal floorPrice) {
		this.floorPrice = floorPrice;
	}

	

}
