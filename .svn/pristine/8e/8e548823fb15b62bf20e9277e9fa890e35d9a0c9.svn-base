package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.constant.MegtemplateConstant;
import com.tourism.hu.entity.*;
import com.tourism.hu.entity.dto.PaymentChannelDto;
import com.tourism.hu.mapper.UserMapper;
import com.tourism.hu.mapper.UserRoleMapper;
import com.tourism.hu.service.IUserService;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import com.tourism.hu.util.ResultUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @ClassName: UserController 
 * @Description: 前端控制器
 * @author 杜东兴
 * @date 2019年10月16日 下午1:06:40 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

    @Resource
    private UserMapper userMapper;

    @Resource
    private IUserService iUserService;

    @Autowired
    RedisTemplate redisTemplate;

    @RequestMapping("/tologin")
    public ModelAndView hello() {
        ModelAndView mav = new ModelAndView("login");
        return mav;
    }


    @RequestMapping("/login")
    @ResponseBody
    public MsgUtil userLogin(String username, String password, HttpServletRequest re) {
        System.out.println(username + "\t" + password);
//        1.将用户输入的账号密码 封装在token中
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
//        2.获取Subject
        Subject subject = SecurityUtils.getSubject();
//        3.通过Subject 的login方法 完成登录
        try {
            //到这里如果没有异常说明登录成功，
            subject.login(token);
            //将用户对象放到session中
            User user = userMapper.selectOne(new QueryWrapper<User>().eq("login_name", username));
            //subject.getSession().setAttribute("user",user);

                  /*   //测试,该用户是否有超级管理员权限
                    boolean superadmin = subject.hasRole("superadmin");
                    System.out.println(superadmin);
                    //测试该用户是否有该按钮权限
                    boolean permitted = subject.isPermitted("用户管理按钮");
                    System.out.println("permitted :"+permitted);*/

            String sessionid = re.getSession().getId();
            redisTemplate.opsForValue().set(sessionid, user.getUserId());
            MsgUtil mu = new MsgUtil();
            mu.setCode(0);
            mu.setMsg(sessionid);
            return mu;
        } catch (Exception e) {
            //有异常说明登录失败，重定向到登录页面
            return MsgUtil.error();
        }
    }


    @RequestMapping({"/loginuser"})
    @ResponseBody
    public MsgUtil loginforredis(HttpServletRequest re) {
        MsgUtil mg = new MsgUtil();
        String sessionid = re.getSession().getId();
        if ((sessionid != null) && (!sessionid.equals(""))) {
            if (redisTemplate.opsForValue().get(sessionid) != null) {
                mg.setCode(100);
                String userid = redisTemplate.opsForValue().get(sessionid).toString();
                User user = userMapper.selectOne(new QueryWrapper<User>().eq("user_id", userid));
                mg.setMsg(user.getLoginName());
                return mg;
            }
        }

        return mg.error();
    }

    @RequestMapping("/tobackUser")
    public ModelAndView tobackUser() {
        ModelAndView mav = new ModelAndView("users/administrators/userlist");
        return mav;
    }

    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page, int limit, String name) {
        DataUtil dataUtil = iUserService.selectAll(page, limit, name);
        return dataUtil;
    }

    @RequestMapping("/updateStatus")
    public MsgUtil updateStatus(User user) {
        int i = userMapper.updateById(user);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/selectOne")
    public User selectOne(Integer aid) {
        User user = iUserService.selectOne(aid);
        return user;
    }

    @RequestMapping("/toEditUser")
    public ModelAndView toEditUser(Integer userId){
        ModelAndView modelAndView = iUserService.toEditUser(userId);
        return modelAndView;
    }


    @RequestMapping("/updateOrSave")
    public ResultUtil updateOrSave(User user){
        User user1 = getUser();
        ResultUtil resultUtil = iUserService.updateOrSave(user,user1);
        return resultUtil;
    }

    @RequestMapping("/deleteOne")
    public MsgUtil deleteOne(Integer aid) {
        MsgUtil msgUtil = iUserService.deleteOne(aid);

        return msgUtil;
    }

    @RequestMapping("/deleteMany")
    public MsgUtil delectMany(Integer[] ids) {
        MsgUtil msgUtil = iUserService.delectMany(ids);
        return msgUtil;
    }


}
