package com.tourism.fzll.util;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @ClassName: FileUploadUtil 
 * @Description: 文件上传工具类 
 * @author 马超伟
 * @date 2019年11月13日 上午11:03:51 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
public class FileUploadUtil {
	
	public static String upload(MultipartFile file) {
		try {
			String extName = file.getOriginalFilename();
			// 获取文件后缀
			if (extName.lastIndexOf(".") <= 0) {
				throw new RuntimeException("不支持该文件类型");
			}
			extName = extName.substring(extName.lastIndexOf("."));
			String webUrl = getWebUrl();
			String fileName = getFileName();
			String sysPath = System.getProperty("catalina.home") + "/webapps";
			// 获取文件名字
			fileName = getFileName() + extName;
			// 获取文件地址
			String filePath = "/content/" + fileName;
			String Url = sysPath +"/content/";
			File file2 = new File(Url);
			if (!file2.exists()) {
				file2.mkdirs();
			}
			file.transferTo(new File(sysPath + filePath));
			return webUrl + filePath;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	/**
	 * 
	 * @Title: upload 
	 * @Description: (将文件保存到指定的路径下) 
	 * @param file

	 * @return  
	 * @date 2019年9月30日 上午10:22:31
	 * @author 董兴隆
	 */
	public static String upload(MultipartFile file,String specifiedPath) {
		try {
			String extName = file.getOriginalFilename();
			// 获取文件后缀
			if (extName.lastIndexOf(".") <= 0) {
				throw new RuntimeException("不支持该文件类型");
			}
			extName = extName.substring(extName.lastIndexOf("."));
			String fileName = getFileName();
			// 获取文件名字
			fileName = getFileName() + extName;
			File file2 = new File(specifiedPath);
			if (!file2.exists()) {
				file2.mkdirs();
			}
			file.transferTo(new File(specifiedPath + File.separator+ fileName));
			return fileName;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	public static String baseUpload(MultipartFile file,String specifiedPath) throws IOException {
			String extName = file.getOriginalFilename();
			// 获取文件后缀
			if (extName.lastIndexOf(".") <= 0) {
				throw new RuntimeException("不支持该文件类型");
			}
			extName = extName.substring(extName.lastIndexOf("."));
			String fileName = getFileName();
			// 获取文件名字
			fileName = getFileName() + extName;
			File file2 = new File(specifiedPath);
			if (!file2.exists()) {
				file2.mkdirs();
			}
			file.transferTo(new File(specifiedPath + File.separator+ fileName));
			return fileName;
	}

	/**
	 * 获取文件名
	 * @return
	 */
	public static String getFileName() {
		String uuid = UUID.randomUUID().toString();
		uuid = uuid.replace("-", "");
		return uuid.toLowerCase();
	}
	
	public static String getWebUrl() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		return  request.getServletContext().getRealPath("/img");
	}
	
	public static String getWebProUrl() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() +request.getContextPath();
	}
	
}
