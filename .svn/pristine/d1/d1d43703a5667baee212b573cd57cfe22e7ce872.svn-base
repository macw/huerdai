package com.tourism.hu.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tourism.hu.util.PageUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * 
 * @ClassName: Invoice 
 * @Description: (发票表) 
 * @author 董兴隆
 * @date 2019年11月11日 下午5:06:03 
 * @Copyright: All rights Reserved, Designed By Huerdai  
 * Copyright:    Copyright(C) 2019-2020 
 * Company       Huerdai Henan LTD.
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fz_invoice")
public class Invoice extends PageUtil implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 发票ID
     */
    @TableId(value = "invoice_id", type = IdType.AUTO)
    private Integer invoiceId;

    /**
     * 发票编号
     */
    private String invoiceNum;

    /**
     * 发票币种 常用CNY
     */
    private String invoiceCurrencyCode;

    /**
     * 付款币种
     */
    private String paymentCurrencyCode;

    /**
     * 发票金额
     */
    private BigDecimal invoiceAmount;

    /**
     * 已付金额
     */
    private BigDecimal amountPaid;

    /**
     * 发票日期
     */
    private LocalDateTime invoiceDate;

    /**
     * 发票来源
     */
    private String source;

    /**
     * 发票类型 1信用：贷项通知单 2借方：借项通知单 3费用报告：费用报表 4混合：混合 5先付：预付款发票 6标准：标准发票
     */
    private Integer invoiceTypeLookupCode;

    /**
     * 发票摘要
     */
    private String description;

    /**
     * 付款状态 1全额付款 2未付款 3部分付款
     */
    private Integer paymentStatusFlag;

    /**
     * 凭证编号
     */
    private String docSequenceValue;

    /**
     * 审批状态
     */
    private Integer approvalStatus;

    /**
     * 创建日期
     */
    private LocalDateTime creationDate;

    /**
     * 创建者名称
     */
    private String createdName;

    /**
     * 创建者id
     */
    private Integer createdId;

    /**
     * 最后更新者id
     */
    private Integer lastUpdatedId;

    /**
     * 最后更新日期
     */
    private LocalDateTime lastUpdateDate;

    /**
     * 最后更新人名称
     */
    private String lastUpdateName;

    /**
     * 取消人名称
     */
    private Integer cancelledId;

    /**
     * 取消人名称
     */
    private String cancelledName;

    /**
     * 取消金额
     */
    private BigDecimal cancelledAmount;

    /**
     * 取消日期
     */
    private LocalDateTime cancelledDate;

	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getInvoiceNum() {
		return invoiceNum;
	}

	public void setInvoiceNum(String invoiceNum) {
		this.invoiceNum = invoiceNum;
	}

	public String getInvoiceCurrencyCode() {
		return invoiceCurrencyCode;
	}

	public void setInvoiceCurrencyCode(String invoiceCurrencyCode) {
		this.invoiceCurrencyCode = invoiceCurrencyCode;
	}

	public String getPaymentCurrencyCode() {
		return paymentCurrencyCode;
	}

	public void setPaymentCurrencyCode(String paymentCurrencyCode) {
		this.paymentCurrencyCode = paymentCurrencyCode;
	}

	public BigDecimal getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(BigDecimal invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public BigDecimal getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(BigDecimal amountPaid) {
		this.amountPaid = amountPaid;
	}

	public LocalDateTime getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(LocalDateTime invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Integer getInvoiceTypeLookupCode() {
		return invoiceTypeLookupCode;
	}

	public void setInvoiceTypeLookupCode(Integer invoiceTypeLookupCode) {
		this.invoiceTypeLookupCode = invoiceTypeLookupCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPaymentStatusFlag() {
		return paymentStatusFlag;
	}

	public void setPaymentStatusFlag(Integer paymentStatusFlag) {
		this.paymentStatusFlag = paymentStatusFlag;
	}

	public String getDocSequenceValue() {
		return docSequenceValue;
	}

	public void setDocSequenceValue(String docSequenceValue) {
		this.docSequenceValue = docSequenceValue;
	}

	public Integer getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(Integer approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreatedName() {
		return createdName;
	}

	public void setCreatedName(String createdName) {
		this.createdName = createdName;
	}

	public Integer getCreatedId() {
		return createdId;
	}

	public void setCreatedId(Integer createdId) {
		this.createdId = createdId;
	}

	public Integer getLastUpdatedId() {
		return lastUpdatedId;
	}

	public void setLastUpdatedId(Integer lastUpdatedId) {
		this.lastUpdatedId = lastUpdatedId;
	}

	public LocalDateTime getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getLastUpdateName() {
		return lastUpdateName;
	}

	public void setLastUpdateName(String lastUpdateName) {
		this.lastUpdateName = lastUpdateName;
	}

	public Integer getCancelledId() {
		return cancelledId;
	}

	public void setCancelledId(Integer cancelledId) {
		this.cancelledId = cancelledId;
	}

	public String getCancelledName() {
		return cancelledName;
	}

	public void setCancelledName(String cancelledName) {
		this.cancelledName = cancelledName;
	}

	public BigDecimal getCancelledAmount() {
		return cancelledAmount;
	}

	public void setCancelledAmount(BigDecimal cancelledAmount) {
		this.cancelledAmount = cancelledAmount;
	}

	public LocalDateTime getCancelledDate() {
		return cancelledDate;
	}

	public void setCancelledDate(LocalDateTime cancelledDate) {
		this.cancelledDate = cancelledDate;
	}


}
