package com.tourism.hu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tourism.hu.constant.AuthorConstant;
import com.tourism.hu.constant.MenuConsant;
import com.tourism.hu.entity.*;
import com.tourism.hu.mapper.MenuMapper;
import com.tourism.hu.service.*;
import com.tourism.hu.util.DataUtil;
import com.tourism.hu.util.MsgUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author Huerdai
 * @since 2019-09-04
 */
@Controller
@RequestMapping("/menu")
public class MenuController extends BaseController {

    @Resource
    private IMenuService iMenuService;

    @Resource
    private IUserRoleService iUserRoleService;

    @Resource
    private IUserService iUserService;

    @Resource
    private IRoleService iRoleService;

    @Resource
    private IAuthorService iAuthorService;

    @Resource
    private MenuMapper menuMapper;

    private  Logger logger = LoggerFactory.getLogger(getClass());

    @RequestMapping("/toMenuBack")
    public ModelAndView toMenuBack(){
        ModelAndView modelAndView = new ModelAndView("users/functionresources/list");
        return modelAndView;
    }

    @RequestMapping("/selectAllList")
    @ResponseBody
    public DataUtil selectAllList(String menuName){
        List<Menu> menuList = null;
        if (menuName == null || menuName == ""){
            menuList = menuMapper.selectList(null);
        }else {
            menuList = menuMapper.selectList(new QueryWrapper<Menu>().eq("menu_name",menuName));
        }
        DataUtil dataUtil = DataUtil.success();
        dataUtil.setData(menuList);
        dataUtil.setCount(menuMapper.selectCount(null).longValue());
        return dataUtil;
    }

    @RequestMapping("/updateStatus")
    @ResponseBody
    public MsgUtil updateStatus(Menu menu){
        int update = menuMapper.updateById(menu);
        return MsgUtil.flag(update);
    }

    @RequestMapping("/selectOneLevel")
    @ResponseBody
    public List<Menu> selectOneLevel(){
        List<Menu> menuList = menuMapper.selectList(new QueryWrapper<Menu>().eq("level", MenuConsant.ONE_LEVEL));
        return menuList;
    }

    @RequestMapping("/addMenu")
    @ResponseBody
    public MsgUtil addMenu(Menu menu){
//        logger.info(menu.toString());
        if (menu.getParentId()==null){
            menu.setParentId(0);
        }
        menu.setMenuStatus(MenuConsant.USE_STATE);
        int i = menuMapper.insert(menu);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/selectOne")
    @ResponseBody
    public Menu selectOne(Integer aid){
        Menu menu = menuMapper.selectOne(new QueryWrapper<Menu>().eq("menu_id", aid));
        return menu;
    }

    @RequestMapping("/updateMenu")
    @ResponseBody
    public MsgUtil updateMenu(Menu menu){
        int i = menuMapper.updateById(menu);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/deleteOne")
    @ResponseBody
    public MsgUtil deleteOne(Integer aid){
        int i = menuMapper.deleteById(aid);
        return MsgUtil.flag(i);
    }

    @RequestMapping("/deleteMany")
    public MsgUtil deleteMany(Integer[] ids) {
        List list = new ArrayList();
        for (Integer id : ids) {
            list.add(id);
        }
        int i = menuMapper.deleteBatchIds(list);
        return MsgUtil.flag(i);
    }



    /**
     * 去后台首页
     * @return
     */
    @RequestMapping("/index")
    public ModelAndView index(){
        User user = getUser();
        if (user==null){
            return new ModelAndView("login");
        }
        Integer userId = user.getUserId();
        logger.debug("userid==="+userId);
        ModelAndView mv = new ModelAndView("index");
        List<Menu> menuList = new ArrayList<>();
        //根据当前用户id 查询当前用户的角色
        List<UserRole> userRoleList = iUserRoleService.list(new QueryWrapper<UserRole>().eq("user_id", userId));
        for (UserRole userRole : userRoleList) {
            //根据用户角色信息查询出角色id
            Integer roleId = userRole.getRoleId();
            //根据角色id查询授权信息
            List<Author> authorList = iAuthorService.list(new QueryWrapper<Author>().eq("role_id", roleId).eq("resource_type", AuthorConstant.MENU_RESOURCE_TYPE));
            //根据授权信息查询资源（菜单）id等数据
            for (Author author : authorList) {
                //根据资源id查询出菜单对象
                Menu menu = iMenuService.getById(author.getResourceId());
                //将菜单对象添加到返回结果集中
                menuList.add(menu);
            }
        }
        //处理菜单返回结果集，封装一级 菜单集合
        List<Menu> menus = new ArrayList<>();

        if (menuList!=null && menuList.size()>0) {
            for (Menu menu : menuList) {
                logger.debug("menu.getLevel()===="+menu.getLevel());
                if (MenuConsant.ONE_LEVEL == menu.getLevel()) {
                    menus.add(menu);
                }
            }
            //处理二级结果集
            //遍历一级结果集
            for (Menu menu : menus) {
                //再次从总结果集中抽出资源
                List<Menu> list2 = new ArrayList<>();
                for (Menu menu1 : menuList) {
                    if (menu1.getParentId() == menu.getMenuId()) {
                        //将二级结果集放进去
                        list2.add(menu1);
                    }
                }
                if (list2 != null && list2.size() > 0) {
                    menu.setMenuList(list2);
                }
            }
        }

        /**
         *  Collections.sort(menus,new Comparator<Menu>() {
         *             @Override
         *             public int compare(Menu o1, Menu o2) {
         *                 //降序
         *                 return o2.getMenuOrder().compareTo(o1.getMenuOrder());
         *             }
         *         });
         */

       /* Collections.sort(menus, (o1, o2) -> {
            //降序
            return o2.getMenuOrder().compareTo(o1.getMenuOrder());
        });   //降序排列*/
        mv.addObject("menu", menus);
        return mv;
    }







/*
    @RequestMapping("/selectAll")
    public DataUtil selectAll(int page, int limit, String menuName){
        Page<Menu> menuPage = new Page<>(page,limit);
        //查询分页
        IPage<Menu> menuIPage = null;
        if (menuName == null || menuName == "" ){
            menuIPage = menuMapper.selectPage(menuPage, null);
        }else{
            menuIPage = menuMapper.selectPage(menuPage, new QueryWrapper<Menu>().like("menu_name", menuName));
        }
        DataUtil dataUtil = DataUtil.success();
        //获取分页后的数据
        List<Menu> menuList = menuIPage.getRecords();
        //获取分页总条数
        long total = menuIPage.getTotal();
        //将数据封装到dataUtil对象
        dataUtil.setData(menuList);
        dataUtil.setCount(total);
        return dataUtil;
    }*/






}
